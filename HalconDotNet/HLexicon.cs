using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HLexicon : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HLexicon()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HLexicon(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HLexicon obj)
		{
			obj = new HLexicon(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HLexicon[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HLexicon[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HLexicon(hTuple[i].IP);
			}
			return err;
		}

		public HLexicon(string name, string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(670);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.StoreS(proc, 1, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HLexicon(string name, HTuple words)
		{
			IntPtr proc = HalconAPI.PreCall(671);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.Store(proc, 1, words);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(words);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public string SuggestLexicon(string word, out int numCorrections)
		{
			IntPtr proc = HalconAPI.PreCall(667);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, word);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out numCorrections);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int LookupLexicon(string word)
		{
			IntPtr proc = HalconAPI.PreCall(668);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, word);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple InspectLexicon()
		{
			IntPtr proc = HalconAPI.PreCall(669);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ImportLexicon(string name, string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(670);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.StoreS(proc, 1, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateLexicon(string name, HTuple words)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(671);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.Store(proc, 1, words);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(words);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(666);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
