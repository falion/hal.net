using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HMatrix : HTool, ISerializable, ICloneable
	{
		public double this[int row, int column]
		{
			get
			{
				return this.GetValueMatrix(row, column);
			}
			set
			{
				this.SetValueMatrix(row, column, value);
			}
		}

		public int NumRows
		{
			get
			{
				int result = 0;
				int num = 0;
				this.GetSizeMatrix(out result, out num);
				return result;
			}
		}

		public int NumColumns
		{
			get
			{
				int num = 0;
				int result = 0;
				this.GetSizeMatrix(out num, out result);
				return result;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMatrix()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMatrix(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMatrix obj)
		{
			obj = new HMatrix(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMatrix[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HMatrix[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HMatrix(hTuple[i].IP);
			}
			return err;
		}

		public HMatrix(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(842);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMatrix(int rows, int columns, HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(897);
			HalconAPI.StoreI(proc, 0, rows);
			HalconAPI.StoreI(proc, 1, columns);
			HalconAPI.Store(proc, 2, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(value);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMatrix(int rows, int columns, double value)
		{
			IntPtr proc = HalconAPI.PreCall(897);
			HalconAPI.StoreI(proc, 0, rows);
			HalconAPI.StoreI(proc, 1, columns);
			HalconAPI.StoreD(proc, 2, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeMatrix();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMatrix(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeMatrix(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeMatrix().Serialize(stream);
		}

		public static HMatrix Deserialize(Stream stream)
		{
			HMatrix hMatrix = new HMatrix();
			hMatrix.DeserializeMatrix(HSerializedItem.Deserialize(stream));
			return hMatrix;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HMatrix Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeMatrix();
			HMatrix hMatrix = new HMatrix();
			hMatrix.DeserializeMatrix(hSerializedItem);
			hSerializedItem.Dispose();
			return hMatrix;
		}

		public static HMatrix operator -(HMatrix matrix)
		{
			return matrix.ScaleMatrix(-1.0);
		}

		public static HMatrix operator +(HMatrix matrix1, HMatrix matrix2)
		{
			return matrix1.AddMatrix(matrix2);
		}

		public static HMatrix operator -(HMatrix matrix1, HMatrix matrix2)
		{
			return matrix1.SubMatrix(matrix2);
		}

		public static HMatrix operator *(HMatrix matrix1, HMatrix matrix2)
		{
			return matrix1.MultMatrix(matrix2, "AB");
		}

		public static HMatrix operator *(double factor, HMatrix matrix)
		{
			return matrix.ScaleMatrix(factor);
		}

		public static HMatrix operator *(HMatrix matrix, double factor)
		{
			return matrix.ScaleMatrix(factor);
		}

		public static HMatrix operator /(HMatrix matrix1, HMatrix matrix2)
		{
			return matrix2.SolveMatrix("general", 0.0, matrix1);
		}

		public static implicit operator HTuple(HMatrix matrix)
		{
			return matrix.GetFullMatrix();
		}

		public void DeserializeMatrix(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(840);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(841);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadMatrix(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(842);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteMatrix(string fileFormat, string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(843);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileFormat);
			HalconAPI.StoreS(proc, 2, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix OrthogonalDecomposeMatrix(string decompositionType, string outputMatricesType, string computeOrthogonal, out HMatrix matrixTriangularID)
		{
			IntPtr proc = HalconAPI.PreCall(844);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, decompositionType);
			HalconAPI.StoreS(proc, 2, outputMatricesType);
			HalconAPI.StoreS(proc, 3, computeOrthogonal);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			err = HMatrix.LoadNew(proc, 1, err, out matrixTriangularID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix DecomposeMatrix(string matrixType, out HMatrix matrix2ID)
		{
			IntPtr proc = HalconAPI.PreCall(845);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			err = HMatrix.LoadNew(proc, 1, err, out matrix2ID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix SvdMatrix(string SVDType, string computeSingularVectors, out HMatrix matrixSID, out HMatrix matrixVID)
		{
			IntPtr proc = HalconAPI.PreCall(846);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, SVDType);
			HalconAPI.StoreS(proc, 2, computeSingularVectors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			err = HMatrix.LoadNew(proc, 1, err, out matrixSID);
			err = HMatrix.LoadNew(proc, 2, err, out matrixVID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GeneralizedEigenvaluesGeneralMatrix(HMatrix matrixBID, string computeEigenvectors, out HMatrix eigenvaluesRealID, out HMatrix eigenvaluesImagID, out HMatrix eigenvectorsRealID, out HMatrix eigenvectorsImagID)
		{
			IntPtr proc = HalconAPI.PreCall(847);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.StoreS(proc, 2, computeEigenvectors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HMatrix.LoadNew(proc, 0, err, out eigenvaluesRealID);
			err = HMatrix.LoadNew(proc, 1, err, out eigenvaluesImagID);
			err = HMatrix.LoadNew(proc, 2, err, out eigenvectorsRealID);
			err = HMatrix.LoadNew(proc, 3, err, out eigenvectorsImagID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix GeneralizedEigenvaluesSymmetricMatrix(HMatrix matrixBID, string computeEigenvectors, out HMatrix eigenvectorsID)
		{
			IntPtr proc = HalconAPI.PreCall(848);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.StoreS(proc, 2, computeEigenvectors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			err = HMatrix.LoadNew(proc, 1, err, out eigenvectorsID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return result;
		}

		public void EigenvaluesGeneralMatrix(string computeEigenvectors, out HMatrix eigenvaluesRealID, out HMatrix eigenvaluesImagID, out HMatrix eigenvectorsRealID, out HMatrix eigenvectorsImagID)
		{
			IntPtr proc = HalconAPI.PreCall(849);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, computeEigenvectors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HMatrix.LoadNew(proc, 0, err, out eigenvaluesRealID);
			err = HMatrix.LoadNew(proc, 1, err, out eigenvaluesImagID);
			err = HMatrix.LoadNew(proc, 2, err, out eigenvectorsRealID);
			err = HMatrix.LoadNew(proc, 3, err, out eigenvectorsImagID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMatrix EigenvaluesSymmetricMatrix(string computeEigenvectors, out HMatrix eigenvectorsID)
		{
			IntPtr proc = HalconAPI.PreCall(850);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, computeEigenvectors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			err = HMatrix.LoadNew(proc, 1, err, out eigenvectorsID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix SolveMatrix(string matrixLHSType, double epsilon, HMatrix matrixRHSID)
		{
			IntPtr proc = HalconAPI.PreCall(851);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixLHSType);
			HalconAPI.StoreD(proc, 2, epsilon);
			HalconAPI.Store(proc, 3, matrixRHSID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixRHSID);
			return result;
		}

		public double DeterminantMatrix(string matrixType)
		{
			IntPtr proc = HalconAPI.PreCall(852);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void InvertMatrixMod(string matrixType, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(853);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.StoreD(proc, 2, epsilon);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix InvertMatrix(string matrixType, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(854);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.StoreD(proc, 2, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void TransposeMatrixMod()
		{
			IntPtr proc = HalconAPI.PreCall(855);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix TransposeMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(856);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix MaxMatrix(string maxType)
		{
			IntPtr proc = HalconAPI.PreCall(857);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, maxType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix MinMatrix(string minType)
		{
			IntPtr proc = HalconAPI.PreCall(858);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, minType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void PowMatrixMod(string matrixType, HTuple power)
		{
			IntPtr proc = HalconAPI.PreCall(859);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.Store(proc, 2, power);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(power);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void PowMatrixMod(string matrixType, double power)
		{
			IntPtr proc = HalconAPI.PreCall(859);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.StoreD(proc, 2, power);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix PowMatrix(string matrixType, HTuple power)
		{
			IntPtr proc = HalconAPI.PreCall(860);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.Store(proc, 2, power);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(power);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix PowMatrix(string matrixType, double power)
		{
			IntPtr proc = HalconAPI.PreCall(860);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.StoreD(proc, 2, power);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void PowElementMatrixMod(HMatrix matrixExpID)
		{
			IntPtr proc = HalconAPI.PreCall(861);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixExpID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixExpID);
		}

		public HMatrix PowElementMatrix(HMatrix matrixExpID)
		{
			IntPtr proc = HalconAPI.PreCall(862);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixExpID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixExpID);
			return result;
		}

		public void PowScalarElementMatrixMod(HTuple power)
		{
			IntPtr proc = HalconAPI.PreCall(863);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, power);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(power);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void PowScalarElementMatrixMod(double power)
		{
			IntPtr proc = HalconAPI.PreCall(863);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, power);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix PowScalarElementMatrix(HTuple power)
		{
			IntPtr proc = HalconAPI.PreCall(864);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, power);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(power);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix PowScalarElementMatrix(double power)
		{
			IntPtr proc = HalconAPI.PreCall(864);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, power);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SqrtMatrixMod()
		{
			IntPtr proc = HalconAPI.PreCall(865);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix SqrtMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(866);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AbsMatrixMod()
		{
			IntPtr proc = HalconAPI.PreCall(867);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix AbsMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(868);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double NormMatrix(string normType)
		{
			IntPtr proc = HalconAPI.PreCall(869);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, normType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix MeanMatrix(string meanType)
		{
			IntPtr proc = HalconAPI.PreCall(870);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, meanType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix SumMatrix(string sumType)
		{
			IntPtr proc = HalconAPI.PreCall(871);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, sumType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DivElementMatrixMod(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(872);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix DivElementMatrix(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(873);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return result;
		}

		public void MultElementMatrixMod(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(874);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix MultElementMatrix(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(875);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return result;
		}

		public void ScaleMatrixMod(HTuple factor)
		{
			IntPtr proc = HalconAPI.PreCall(876);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, factor);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(factor);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ScaleMatrixMod(double factor)
		{
			IntPtr proc = HalconAPI.PreCall(876);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, factor);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix ScaleMatrix(HTuple factor)
		{
			IntPtr proc = HalconAPI.PreCall(877);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, factor);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(factor);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix ScaleMatrix(double factor)
		{
			IntPtr proc = HalconAPI.PreCall(877);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, factor);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SubMatrixMod(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(878);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix SubMatrix(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(879);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return result;
		}

		public void AddMatrixMod(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(880);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix AddMatrix(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(881);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return result;
		}

		public void MultMatrixMod(HMatrix matrixBID, string multType)
		{
			IntPtr proc = HalconAPI.PreCall(882);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.StoreS(proc, 2, multType);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix MultMatrix(HMatrix matrixBID, string multType)
		{
			IntPtr proc = HalconAPI.PreCall(883);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.StoreS(proc, 2, multType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return result;
		}

		public void GetSizeMatrix(out int rows, out int columns)
		{
			IntPtr proc = HalconAPI.PreCall(884);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out rows);
			err = HalconAPI.LoadI(proc, 1, err, out columns);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMatrix RepeatMatrix(int rows, int columns)
		{
			IntPtr proc = HalconAPI.PreCall(885);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, rows);
			HalconAPI.StoreI(proc, 2, columns);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HMatrix CopyMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(886);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetDiagonalMatrix(HMatrix vectorID, int diagonal)
		{
			IntPtr proc = HalconAPI.PreCall(887);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, vectorID);
			HalconAPI.StoreI(proc, 2, diagonal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(vectorID);
		}

		public HMatrix GetDiagonalMatrix(int diagonal)
		{
			IntPtr proc = HalconAPI.PreCall(888);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, diagonal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetSubMatrix(HMatrix matrixSubID, int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(889);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixSubID);
			HalconAPI.StoreI(proc, 2, row);
			HalconAPI.StoreI(proc, 3, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixSubID);
		}

		public HMatrix GetSubMatrix(int row, int column, int rowsSub, int columnsSub)
		{
			IntPtr proc = HalconAPI.PreCall(890);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, rowsSub);
			HalconAPI.StoreI(proc, 4, columnsSub);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HMatrix result = null;
			err = HMatrix.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetFullMatrix(HTuple values)
		{
			IntPtr proc = HalconAPI.PreCall(891);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, values);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(values);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetFullMatrix(double values)
		{
			IntPtr proc = HalconAPI.PreCall(891);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, values);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetFullMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(892);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetValueMatrix(HTuple row, HTuple column, HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(893);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(value);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetValueMatrix(int row, int column, double value)
		{
			IntPtr proc = HalconAPI.PreCall(893);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreD(proc, 3, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetValueMatrix(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(894);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double GetValueMatrix(int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(894);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void CreateMatrix(int rows, int columns, HTuple value)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(897);
			HalconAPI.StoreI(proc, 0, rows);
			HalconAPI.StoreI(proc, 1, columns);
			HalconAPI.Store(proc, 2, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(value);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateMatrix(int rows, int columns, double value)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(897);
			HalconAPI.StoreI(proc, 0, rows);
			HalconAPI.StoreI(proc, 1, columns);
			HalconAPI.StoreD(proc, 2, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(896);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
