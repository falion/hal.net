namespace VisionConfig
{
	internal class HDevInputParamTuple : HDevInputParam
	{
		protected HTuple mTuple;

		public HDevInputParamTuple(HTuple tuple)
		{
			this.mTuple = tuple.Clone();
		}

		public override HTuple GetCtrlParamTuple()
		{
			return this.mTuple;
		}
	}
}
