using System;

namespace VisionConfig
{
	public class HMisc
	{
		public static void WriteTuple(HTuple tuple, string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(219);
			HalconAPI.Store(proc, 0, tuple);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(tuple);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void WriteTuple(double tuple, string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(219);
			HalconAPI.StoreD(proc, 0, tuple);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static HTuple ReadTuple(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(220);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void CloseAllSerials()
		{
			IntPtr proc = HalconAPI.PreCall(312);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void CloseAllOcvs()
		{
			IntPtr proc = HalconAPI.PreCall(644);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void CloseAllOcrs()
		{
			IntPtr proc = HalconAPI.PreCall(724);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void ConcatOcrTrainf(HTuple singleFiles, string composedFile)
		{
			IntPtr proc = HalconAPI.PreCall(728);
			HalconAPI.Store(proc, 0, singleFiles);
			HalconAPI.StoreS(proc, 1, composedFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(singleFiles);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void ConcatOcrTrainf(string singleFiles, string composedFile)
		{
			IntPtr proc = HalconAPI.PreCall(728);
			HalconAPI.StoreS(proc, 0, singleFiles);
			HalconAPI.StoreS(proc, 1, composedFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static HTuple ReadOcrTrainfNamesProtected(HTuple trainingFile, HTuple password, out HTuple characterCount)
		{
			IntPtr proc = HalconAPI.PreCall(731);
			HalconAPI.Store(proc, 0, trainingFile);
			HalconAPI.Store(proc, 1, password);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(password);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out characterCount);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static string ReadOcrTrainfNamesProtected(string trainingFile, string password, out int characterCount)
		{
			IntPtr proc = HalconAPI.PreCall(731);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.StoreS(proc, 1, password);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out characterCount);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple ReadOcrTrainfNames(HTuple trainingFile, out HTuple characterCount)
		{
			IntPtr proc = HalconAPI.PreCall(732);
			HalconAPI.Store(proc, 0, trainingFile);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out characterCount);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static string ReadOcrTrainfNames(string trainingFile, out int characterCount)
		{
			IntPtr proc = HalconAPI.PreCall(732);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out characterCount);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void CloseAllMeasures()
		{
			IntPtr proc = HalconAPI.PreCall(826);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void ConvertPoint3dSpherToCart(HTuple longitude, HTuple latitude, HTuple radius, string equatPlaneNormal, string zeroMeridian, out HTuple x, out HTuple y, out HTuple z)
		{
			IntPtr proc = HalconAPI.PreCall(1046);
			HalconAPI.Store(proc, 0, longitude);
			HalconAPI.Store(proc, 1, latitude);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.StoreS(proc, 3, equatPlaneNormal);
			HalconAPI.StoreS(proc, 4, zeroMeridian);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(longitude);
			HalconAPI.UnpinTuple(latitude);
			HalconAPI.UnpinTuple(radius);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out z);
			HalconAPI.PostCall(proc, err);
		}

		public static void ConvertPoint3dSpherToCart(double longitude, double latitude, double radius, string equatPlaneNormal, string zeroMeridian, out double x, out double y, out double z)
		{
			IntPtr proc = HalconAPI.PreCall(1046);
			HalconAPI.StoreD(proc, 0, longitude);
			HalconAPI.StoreD(proc, 1, latitude);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.StoreS(proc, 3, equatPlaneNormal);
			HalconAPI.StoreS(proc, 4, zeroMeridian);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out x);
			err = HalconAPI.LoadD(proc, 1, err, out y);
			err = HalconAPI.LoadD(proc, 2, err, out z);
			HalconAPI.PostCall(proc, err);
		}

		public static HTuple ConvertPoint3dCartToSpher(HTuple x, HTuple y, HTuple z, string equatPlaneNormal, string zeroMeridian, out HTuple latitude, out HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(1047);
			HalconAPI.Store(proc, 0, x);
			HalconAPI.Store(proc, 1, y);
			HalconAPI.Store(proc, 2, z);
			HalconAPI.StoreS(proc, 3, equatPlaneNormal);
			HalconAPI.StoreS(proc, 4, zeroMeridian);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(x);
			HalconAPI.UnpinTuple(y);
			HalconAPI.UnpinTuple(z);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out latitude);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out radius);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static double ConvertPoint3dCartToSpher(double x, double y, double z, string equatPlaneNormal, string zeroMeridian, out double latitude, out double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1047);
			HalconAPI.StoreD(proc, 0, x);
			HalconAPI.StoreD(proc, 1, y);
			HalconAPI.StoreD(proc, 2, z);
			HalconAPI.StoreS(proc, 3, equatPlaneNormal);
			HalconAPI.StoreS(proc, 4, zeroMeridian);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out latitude);
			err = HalconAPI.LoadD(proc, 2, err, out radius);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple ReadKalman(string fileName, out HTuple model, out HTuple measurement, out HTuple prediction)
		{
			IntPtr proc = HalconAPI.PreCall(1105);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out model);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out measurement);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out prediction);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple UpdateKalman(string fileName, HTuple dimensionIn, HTuple modelIn, HTuple measurementIn, out HTuple modelOut, out HTuple measurementOut)
		{
			IntPtr proc = HalconAPI.PreCall(1106);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, dimensionIn);
			HalconAPI.Store(proc, 2, modelIn);
			HalconAPI.Store(proc, 3, measurementIn);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(dimensionIn);
			HalconAPI.UnpinTuple(modelIn);
			HalconAPI.UnpinTuple(measurementIn);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out modelOut);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out measurementOut);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple FilterKalman(HTuple dimension, HTuple model, HTuple measurement, HTuple predictionIn, out HTuple estimate)
		{
			IntPtr proc = HalconAPI.PreCall(1107);
			HalconAPI.Store(proc, 0, dimension);
			HalconAPI.Store(proc, 1, model);
			HalconAPI.Store(proc, 2, measurement);
			HalconAPI.Store(proc, 3, predictionIn);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(dimension);
			HalconAPI.UnpinTuple(model);
			HalconAPI.UnpinTuple(measurement);
			HalconAPI.UnpinTuple(predictionIn);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out estimate);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void CreateRectificationGrid(double width, int numSquares, string gridFile)
		{
			IntPtr proc = HalconAPI.PreCall(1157);
			HalconAPI.StoreD(proc, 0, width);
			HalconAPI.StoreI(proc, 1, numSquares);
			HalconAPI.StoreS(proc, 2, gridFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static HImage GenArbitraryDistortionMap(int gridSpacing, HTuple row, HTuple column, int gridWidth, int imageWidth, int imageHeight, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1160);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, gridWidth);
			HalconAPI.StoreI(proc, 4, imageWidth);
			HalconAPI.StoreI(proc, 5, imageHeight);
			HalconAPI.StoreS(proc, 6, mapType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void ProjectionPl(HTuple row, HTuple column, HTuple row1, HTuple column1, HTuple row2, HTuple column2, out HTuple rowProj, out HTuple colProj)
		{
			IntPtr proc = HalconAPI.PreCall(1338);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, row1);
			HalconAPI.Store(proc, 3, column1);
			HalconAPI.Store(proc, 4, row2);
			HalconAPI.Store(proc, 5, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowProj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out colProj);
			HalconAPI.PostCall(proc, err);
		}

		public static void ProjectionPl(double row, double column, double row1, double column1, double row2, double column2, out double rowProj, out double colProj)
		{
			IntPtr proc = HalconAPI.PreCall(1338);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, row1);
			HalconAPI.StoreD(proc, 3, column1);
			HalconAPI.StoreD(proc, 4, row2);
			HalconAPI.StoreD(proc, 5, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out rowProj);
			err = HalconAPI.LoadD(proc, 1, err, out colProj);
			HalconAPI.PostCall(proc, err);
		}

		public static void GetPointsEllipse(HTuple angle, double row, double column, double phi, double radius1, double radius2, out HTuple rowPoint, out HTuple colPoint)
		{
			IntPtr proc = HalconAPI.PreCall(1339);
			HalconAPI.Store(proc, 0, angle);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreD(proc, 4, radius1);
			HalconAPI.StoreD(proc, 5, radius2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angle);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowPoint);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out colPoint);
			HalconAPI.PostCall(proc, err);
		}

		public static void GetPointsEllipse(double angle, double row, double column, double phi, double radius1, double radius2, out double rowPoint, out double colPoint)
		{
			IntPtr proc = HalconAPI.PreCall(1339);
			HalconAPI.StoreD(proc, 0, angle);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreD(proc, 4, radius1);
			HalconAPI.StoreD(proc, 5, radius2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out rowPoint);
			err = HalconAPI.LoadD(proc, 1, err, out colPoint);
			HalconAPI.PostCall(proc, err);
		}

		public static void IntersectionLl(HTuple rowA1, HTuple columnA1, HTuple rowA2, HTuple columnA2, HTuple rowB1, HTuple columnB1, HTuple rowB2, HTuple columnB2, out HTuple row, out HTuple column, out HTuple isParallel)
		{
			IntPtr proc = HalconAPI.PreCall(1340);
			HalconAPI.Store(proc, 0, rowA1);
			HalconAPI.Store(proc, 1, columnA1);
			HalconAPI.Store(proc, 2, rowA2);
			HalconAPI.Store(proc, 3, columnA2);
			HalconAPI.Store(proc, 4, rowB1);
			HalconAPI.Store(proc, 5, columnB1);
			HalconAPI.Store(proc, 6, rowB2);
			HalconAPI.Store(proc, 7, columnB2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowA1);
			HalconAPI.UnpinTuple(columnA1);
			HalconAPI.UnpinTuple(rowA2);
			HalconAPI.UnpinTuple(columnA2);
			HalconAPI.UnpinTuple(rowB1);
			HalconAPI.UnpinTuple(columnB1);
			HalconAPI.UnpinTuple(rowB2);
			HalconAPI.UnpinTuple(columnB2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out isParallel);
			HalconAPI.PostCall(proc, err);
		}

		public static void IntersectionLl(double rowA1, double columnA1, double rowA2, double columnA2, double rowB1, double columnB1, double rowB2, double columnB2, out double row, out double column, out int isParallel)
		{
			IntPtr proc = HalconAPI.PreCall(1340);
			HalconAPI.StoreD(proc, 0, rowA1);
			HalconAPI.StoreD(proc, 1, columnA1);
			HalconAPI.StoreD(proc, 2, rowA2);
			HalconAPI.StoreD(proc, 3, columnA2);
			HalconAPI.StoreD(proc, 4, rowB1);
			HalconAPI.StoreD(proc, 5, columnB1);
			HalconAPI.StoreD(proc, 6, rowB2);
			HalconAPI.StoreD(proc, 7, columnB2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadI(proc, 2, err, out isParallel);
			HalconAPI.PostCall(proc, err);
		}

		public static HTuple AngleLx(HTuple row1, HTuple column1, HTuple row2, HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1370);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static double AngleLx(double row1, double column1, double row2, double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1370);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple AngleLl(HTuple rowA1, HTuple columnA1, HTuple rowA2, HTuple columnA2, HTuple rowB1, HTuple columnB1, HTuple rowB2, HTuple columnB2)
		{
			IntPtr proc = HalconAPI.PreCall(1371);
			HalconAPI.Store(proc, 0, rowA1);
			HalconAPI.Store(proc, 1, columnA1);
			HalconAPI.Store(proc, 2, rowA2);
			HalconAPI.Store(proc, 3, columnA2);
			HalconAPI.Store(proc, 4, rowB1);
			HalconAPI.Store(proc, 5, columnB1);
			HalconAPI.Store(proc, 6, rowB2);
			HalconAPI.Store(proc, 7, columnB2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowA1);
			HalconAPI.UnpinTuple(columnA1);
			HalconAPI.UnpinTuple(rowA2);
			HalconAPI.UnpinTuple(columnA2);
			HalconAPI.UnpinTuple(rowB1);
			HalconAPI.UnpinTuple(columnB1);
			HalconAPI.UnpinTuple(rowB2);
			HalconAPI.UnpinTuple(columnB2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static double AngleLl(double rowA1, double columnA1, double rowA2, double columnA2, double rowB1, double columnB1, double rowB2, double columnB2)
		{
			IntPtr proc = HalconAPI.PreCall(1371);
			HalconAPI.StoreD(proc, 0, rowA1);
			HalconAPI.StoreD(proc, 1, columnA1);
			HalconAPI.StoreD(proc, 2, rowA2);
			HalconAPI.StoreD(proc, 3, columnA2);
			HalconAPI.StoreD(proc, 4, rowB1);
			HalconAPI.StoreD(proc, 5, columnB1);
			HalconAPI.StoreD(proc, 6, rowB2);
			HalconAPI.StoreD(proc, 7, columnB2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void DistanceSl(HTuple rowA1, HTuple columnA1, HTuple rowA2, HTuple columnA2, HTuple rowB1, HTuple columnB1, HTuple rowB2, HTuple columnB2, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1372);
			HalconAPI.Store(proc, 0, rowA1);
			HalconAPI.Store(proc, 1, columnA1);
			HalconAPI.Store(proc, 2, rowA2);
			HalconAPI.Store(proc, 3, columnA2);
			HalconAPI.Store(proc, 4, rowB1);
			HalconAPI.Store(proc, 5, columnB1);
			HalconAPI.Store(proc, 6, rowB2);
			HalconAPI.Store(proc, 7, columnB2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowA1);
			HalconAPI.UnpinTuple(columnA1);
			HalconAPI.UnpinTuple(rowA2);
			HalconAPI.UnpinTuple(columnA2);
			HalconAPI.UnpinTuple(rowB1);
			HalconAPI.UnpinTuple(columnB1);
			HalconAPI.UnpinTuple(rowB2);
			HalconAPI.UnpinTuple(columnB2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
		}

		public static void DistanceSl(double rowA1, double columnA1, double rowA2, double columnA2, double rowB1, double columnB1, double rowB2, double columnB2, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1372);
			HalconAPI.StoreD(proc, 0, rowA1);
			HalconAPI.StoreD(proc, 1, columnA1);
			HalconAPI.StoreD(proc, 2, rowA2);
			HalconAPI.StoreD(proc, 3, columnA2);
			HalconAPI.StoreD(proc, 4, rowB1);
			HalconAPI.StoreD(proc, 5, columnB1);
			HalconAPI.StoreD(proc, 6, rowB2);
			HalconAPI.StoreD(proc, 7, columnB2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
		}

		public static void DistanceSs(HTuple rowA1, HTuple columnA1, HTuple rowA2, HTuple columnA2, HTuple rowB1, HTuple columnB1, HTuple rowB2, HTuple columnB2, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1373);
			HalconAPI.Store(proc, 0, rowA1);
			HalconAPI.Store(proc, 1, columnA1);
			HalconAPI.Store(proc, 2, rowA2);
			HalconAPI.Store(proc, 3, columnA2);
			HalconAPI.Store(proc, 4, rowB1);
			HalconAPI.Store(proc, 5, columnB1);
			HalconAPI.Store(proc, 6, rowB2);
			HalconAPI.Store(proc, 7, columnB2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowA1);
			HalconAPI.UnpinTuple(columnA1);
			HalconAPI.UnpinTuple(rowA2);
			HalconAPI.UnpinTuple(columnA2);
			HalconAPI.UnpinTuple(rowB1);
			HalconAPI.UnpinTuple(columnB1);
			HalconAPI.UnpinTuple(rowB2);
			HalconAPI.UnpinTuple(columnB2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
		}

		public static void DistanceSs(double rowA1, double columnA1, double rowA2, double columnA2, double rowB1, double columnB1, double rowB2, double columnB2, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1373);
			HalconAPI.StoreD(proc, 0, rowA1);
			HalconAPI.StoreD(proc, 1, columnA1);
			HalconAPI.StoreD(proc, 2, rowA2);
			HalconAPI.StoreD(proc, 3, columnA2);
			HalconAPI.StoreD(proc, 4, rowB1);
			HalconAPI.StoreD(proc, 5, columnB1);
			HalconAPI.StoreD(proc, 6, rowB2);
			HalconAPI.StoreD(proc, 7, columnB2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
		}

		public static void DistancePs(HTuple row, HTuple column, HTuple row1, HTuple column1, HTuple row2, HTuple column2, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1374);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, row1);
			HalconAPI.Store(proc, 3, column1);
			HalconAPI.Store(proc, 4, row2);
			HalconAPI.Store(proc, 5, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
		}

		public static void DistancePs(double row, double column, double row1, double column1, double row2, double column2, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1374);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, row1);
			HalconAPI.StoreD(proc, 3, column1);
			HalconAPI.StoreD(proc, 4, row2);
			HalconAPI.StoreD(proc, 5, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
		}

		public static HTuple DistancePl(HTuple row, HTuple column, HTuple row1, HTuple column1, HTuple row2, HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1375);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, row1);
			HalconAPI.Store(proc, 3, column1);
			HalconAPI.Store(proc, 4, row2);
			HalconAPI.Store(proc, 5, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static double DistancePl(double row, double column, double row1, double column1, double row2, double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1375);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, row1);
			HalconAPI.StoreD(proc, 3, column1);
			HalconAPI.StoreD(proc, 4, row2);
			HalconAPI.StoreD(proc, 5, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple DistancePp(HTuple row1, HTuple column1, HTuple row2, HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1376);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static double DistancePp(double row1, double column1, double row2, double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1376);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static int InfoSmooth(string filter, double alpha, out HTuple coeffs)
		{
			IntPtr proc = HalconAPI.PreCall(1419);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out coeffs);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple GaussDistribution(double sigma)
		{
			IntPtr proc = HalconAPI.PreCall(1443);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple SpDistribution(HTuple percentSalt, HTuple percentPepper)
		{
			IntPtr proc = HalconAPI.PreCall(1444);
			HalconAPI.Store(proc, 0, percentSalt);
			HalconAPI.Store(proc, 1, percentPepper);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(percentSalt);
			HalconAPI.UnpinTuple(percentPepper);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple SpDistribution(double percentSalt, double percentPepper)
		{
			IntPtr proc = HalconAPI.PreCall(1444);
			HalconAPI.StoreD(proc, 0, percentSalt);
			HalconAPI.StoreD(proc, 1, percentPepper);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void DeserializeFftOptimizationData(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1535);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(serializedItemHandle);
		}

		public static HSerializedItem SerializeFftOptimizationData()
		{
			IntPtr proc = HalconAPI.PreCall(1536);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void ReadFftOptimizationData(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1537);
			HalconAPI.StoreS(proc, 0, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void WriteFftOptimizationData(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1538);
			HalconAPI.StoreS(proc, 0, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void OptimizeRftSpeed(int width, int height, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1539);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.StoreS(proc, 2, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void OptimizeFftSpeed(int width, int height, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1540);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.StoreS(proc, 2, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static int InfoEdges(string filter, string mode, double alpha, out HTuple coeffs)
		{
			IntPtr proc = HalconAPI.PreCall(1565);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.StoreD(proc, 2, alpha);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out coeffs);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void CopyFile(string sourceFile, string destinationFile)
		{
			IntPtr proc = HalconAPI.PreCall(1638);
			HalconAPI.StoreS(proc, 0, sourceFile);
			HalconAPI.StoreS(proc, 1, destinationFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void SetCurrentDir(string dirName)
		{
			IntPtr proc = HalconAPI.PreCall(1639);
			HalconAPI.StoreS(proc, 0, dirName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static string GetCurrentDir()
		{
			IntPtr proc = HalconAPI.PreCall(1640);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void RemoveDir(string dirName)
		{
			IntPtr proc = HalconAPI.PreCall(1641);
			HalconAPI.StoreS(proc, 0, dirName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void MakeDir(string dirName)
		{
			IntPtr proc = HalconAPI.PreCall(1642);
			HalconAPI.StoreS(proc, 0, dirName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static HTuple ListFiles(string directory, HTuple options)
		{
			IntPtr proc = HalconAPI.PreCall(1643);
			HalconAPI.StoreS(proc, 0, directory);
			HalconAPI.Store(proc, 1, options);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(options);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple ListFiles(string directory, string options)
		{
			IntPtr proc = HalconAPI.PreCall(1643);
			HalconAPI.StoreS(proc, 0, directory);
			HalconAPI.StoreS(proc, 1, options);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void DeleteFile(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1644);
			HalconAPI.StoreS(proc, 0, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static int FileExists(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1645);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void CloseAllFiles()
		{
			IntPtr proc = HalconAPI.PreCall(1666);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void SelectLinesLongest(HTuple rowBeginIn, HTuple colBeginIn, HTuple rowEndIn, HTuple colEndIn, int num, out HTuple rowBeginOut, out HTuple colBeginOut, out HTuple rowEndOut, out HTuple colEndOut)
		{
			IntPtr proc = HalconAPI.PreCall(1736);
			HalconAPI.Store(proc, 0, rowBeginIn);
			HalconAPI.Store(proc, 1, colBeginIn);
			HalconAPI.Store(proc, 2, rowEndIn);
			HalconAPI.Store(proc, 3, colEndIn);
			HalconAPI.StoreI(proc, 4, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowBeginIn);
			HalconAPI.UnpinTuple(colBeginIn);
			HalconAPI.UnpinTuple(rowEndIn);
			HalconAPI.UnpinTuple(colEndIn);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out rowBeginOut);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out colBeginOut);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out rowEndOut);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out colEndOut);
			HalconAPI.PostCall(proc, err);
		}

		public static void PartitionLines(HTuple rowBeginIn, HTuple colBeginIn, HTuple rowEndIn, HTuple colEndIn, HTuple feature, string operation, HTuple min, HTuple max, out HTuple rowBeginOut, out HTuple colBeginOut, out HTuple rowEndOut, out HTuple colEndOut, out HTuple failRowBOut, out HTuple failColBOut, out HTuple failRowEOut, out HTuple failColEOut)
		{
			IntPtr proc = HalconAPI.PreCall(1737);
			HalconAPI.Store(proc, 0, rowBeginIn);
			HalconAPI.Store(proc, 1, colBeginIn);
			HalconAPI.Store(proc, 2, rowEndIn);
			HalconAPI.Store(proc, 3, colEndIn);
			HalconAPI.Store(proc, 4, feature);
			HalconAPI.StoreS(proc, 5, operation);
			HalconAPI.Store(proc, 6, min);
			HalconAPI.Store(proc, 7, max);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowBeginIn);
			HalconAPI.UnpinTuple(colBeginIn);
			HalconAPI.UnpinTuple(rowEndIn);
			HalconAPI.UnpinTuple(colEndIn);
			HalconAPI.UnpinTuple(feature);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out rowBeginOut);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out colBeginOut);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out rowEndOut);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out colEndOut);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out failRowBOut);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out failColBOut);
			err = HTuple.LoadNew(proc, 6, HTupleType.INTEGER, err, out failRowEOut);
			err = HTuple.LoadNew(proc, 7, HTupleType.INTEGER, err, out failColEOut);
			HalconAPI.PostCall(proc, err);
		}

		public static void PartitionLines(HTuple rowBeginIn, HTuple colBeginIn, HTuple rowEndIn, HTuple colEndIn, string feature, string operation, string min, string max, out HTuple rowBeginOut, out HTuple colBeginOut, out HTuple rowEndOut, out HTuple colEndOut, out HTuple failRowBOut, out HTuple failColBOut, out HTuple failRowEOut, out HTuple failColEOut)
		{
			IntPtr proc = HalconAPI.PreCall(1737);
			HalconAPI.Store(proc, 0, rowBeginIn);
			HalconAPI.Store(proc, 1, colBeginIn);
			HalconAPI.Store(proc, 2, rowEndIn);
			HalconAPI.Store(proc, 3, colEndIn);
			HalconAPI.StoreS(proc, 4, feature);
			HalconAPI.StoreS(proc, 5, operation);
			HalconAPI.StoreS(proc, 6, min);
			HalconAPI.StoreS(proc, 7, max);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowBeginIn);
			HalconAPI.UnpinTuple(colBeginIn);
			HalconAPI.UnpinTuple(rowEndIn);
			HalconAPI.UnpinTuple(colEndIn);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out rowBeginOut);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out colBeginOut);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out rowEndOut);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out colEndOut);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out failRowBOut);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out failColBOut);
			err = HTuple.LoadNew(proc, 6, HTupleType.INTEGER, err, out failRowEOut);
			err = HTuple.LoadNew(proc, 7, HTupleType.INTEGER, err, out failColEOut);
			HalconAPI.PostCall(proc, err);
		}

		public static void SelectLines(HTuple rowBeginIn, HTuple colBeginIn, HTuple rowEndIn, HTuple colEndIn, HTuple feature, string operation, HTuple min, HTuple max, out HTuple rowBeginOut, out HTuple colBeginOut, out HTuple rowEndOut, out HTuple colEndOut)
		{
			IntPtr proc = HalconAPI.PreCall(1738);
			HalconAPI.Store(proc, 0, rowBeginIn);
			HalconAPI.Store(proc, 1, colBeginIn);
			HalconAPI.Store(proc, 2, rowEndIn);
			HalconAPI.Store(proc, 3, colEndIn);
			HalconAPI.Store(proc, 4, feature);
			HalconAPI.StoreS(proc, 5, operation);
			HalconAPI.Store(proc, 6, min);
			HalconAPI.Store(proc, 7, max);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowBeginIn);
			HalconAPI.UnpinTuple(colBeginIn);
			HalconAPI.UnpinTuple(rowEndIn);
			HalconAPI.UnpinTuple(colEndIn);
			HalconAPI.UnpinTuple(feature);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out rowBeginOut);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out colBeginOut);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out rowEndOut);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out colEndOut);
			HalconAPI.PostCall(proc, err);
		}

		public static void SelectLines(HTuple rowBeginIn, HTuple colBeginIn, HTuple rowEndIn, HTuple colEndIn, string feature, string operation, string min, string max, out HTuple rowBeginOut, out HTuple colBeginOut, out HTuple rowEndOut, out HTuple colEndOut)
		{
			IntPtr proc = HalconAPI.PreCall(1738);
			HalconAPI.Store(proc, 0, rowBeginIn);
			HalconAPI.Store(proc, 1, colBeginIn);
			HalconAPI.Store(proc, 2, rowEndIn);
			HalconAPI.Store(proc, 3, colEndIn);
			HalconAPI.StoreS(proc, 4, feature);
			HalconAPI.StoreS(proc, 5, operation);
			HalconAPI.StoreS(proc, 6, min);
			HalconAPI.StoreS(proc, 7, max);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowBeginIn);
			HalconAPI.UnpinTuple(colBeginIn);
			HalconAPI.UnpinTuple(rowEndIn);
			HalconAPI.UnpinTuple(colEndIn);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out rowBeginOut);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out colBeginOut);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out rowEndOut);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out colEndOut);
			HalconAPI.PostCall(proc, err);
		}

		public static void LinePosition(HTuple rowBegin, HTuple colBegin, HTuple rowEnd, HTuple colEnd, out HTuple rowCenter, out HTuple colCenter, out HTuple length, out HTuple phi)
		{
			IntPtr proc = HalconAPI.PreCall(1739);
			HalconAPI.Store(proc, 0, rowBegin);
			HalconAPI.Store(proc, 1, colBegin);
			HalconAPI.Store(proc, 2, rowEnd);
			HalconAPI.Store(proc, 3, colEnd);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowBegin);
			HalconAPI.UnpinTuple(colBegin);
			HalconAPI.UnpinTuple(rowEnd);
			HalconAPI.UnpinTuple(colEnd);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowCenter);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out colCenter);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out length);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out phi);
			HalconAPI.PostCall(proc, err);
		}

		public static void LinePosition(int rowBegin, int colBegin, int rowEnd, int colEnd, out double rowCenter, out double colCenter, out double length, out double phi)
		{
			IntPtr proc = HalconAPI.PreCall(1739);
			HalconAPI.StoreI(proc, 0, rowBegin);
			HalconAPI.StoreI(proc, 1, colBegin);
			HalconAPI.StoreI(proc, 2, rowEnd);
			HalconAPI.StoreI(proc, 3, colEnd);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out rowCenter);
			err = HalconAPI.LoadD(proc, 1, err, out colCenter);
			err = HalconAPI.LoadD(proc, 2, err, out length);
			err = HalconAPI.LoadD(proc, 3, err, out phi);
			HalconAPI.PostCall(proc, err);
		}

		public static HTuple LineOrientation(HTuple rowBegin, HTuple colBegin, HTuple rowEnd, HTuple colEnd)
		{
			IntPtr proc = HalconAPI.PreCall(1740);
			HalconAPI.Store(proc, 0, rowBegin);
			HalconAPI.Store(proc, 1, colBegin);
			HalconAPI.Store(proc, 2, rowEnd);
			HalconAPI.Store(proc, 3, colEnd);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowBegin);
			HalconAPI.UnpinTuple(colBegin);
			HalconAPI.UnpinTuple(rowEnd);
			HalconAPI.UnpinTuple(colEnd);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static double LineOrientation(double rowBegin, double colBegin, double rowEnd, double colEnd)
		{
			IntPtr proc = HalconAPI.PreCall(1740);
			HalconAPI.StoreD(proc, 0, rowBegin);
			HalconAPI.StoreD(proc, 1, colBegin);
			HalconAPI.StoreD(proc, 2, rowEnd);
			HalconAPI.StoreD(proc, 3, colEnd);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void ApproxChainSimple(HTuple row, HTuple column, out HTuple arcCenterRow, out HTuple arcCenterCol, out HTuple arcAngle, out HTuple arcBeginRow, out HTuple arcBeginCol, out HTuple lineBeginRow, out HTuple lineBeginCol, out HTuple lineEndRow, out HTuple lineEndCol, out HTuple order)
		{
			IntPtr proc = HalconAPI.PreCall(1741);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out arcCenterRow);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out arcCenterCol);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out arcAngle);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out arcBeginRow);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out arcBeginCol);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out lineBeginRow);
			err = HTuple.LoadNew(proc, 6, HTupleType.INTEGER, err, out lineBeginCol);
			err = HTuple.LoadNew(proc, 7, HTupleType.INTEGER, err, out lineEndRow);
			err = HTuple.LoadNew(proc, 8, HTupleType.INTEGER, err, out lineEndCol);
			err = HTuple.LoadNew(proc, 9, HTupleType.INTEGER, err, out order);
			HalconAPI.PostCall(proc, err);
		}

		public static void ApproxChain(HTuple row, HTuple column, double minWidthCoord, double maxWidthCoord, double threshStart, double threshEnd, double threshStep, double minWidthSmooth, double maxWidthSmooth, int minWidthCurve, int maxWidthCurve, double weight1, double weight2, double weight3, out HTuple arcCenterRow, out HTuple arcCenterCol, out HTuple arcAngle, out HTuple arcBeginRow, out HTuple arcBeginCol, out HTuple lineBeginRow, out HTuple lineBeginCol, out HTuple lineEndRow, out HTuple lineEndCol, out HTuple order)
		{
			IntPtr proc = HalconAPI.PreCall(1742);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreD(proc, 2, minWidthCoord);
			HalconAPI.StoreD(proc, 3, maxWidthCoord);
			HalconAPI.StoreD(proc, 4, threshStart);
			HalconAPI.StoreD(proc, 5, threshEnd);
			HalconAPI.StoreD(proc, 6, threshStep);
			HalconAPI.StoreD(proc, 7, minWidthSmooth);
			HalconAPI.StoreD(proc, 8, maxWidthSmooth);
			HalconAPI.StoreI(proc, 9, minWidthCurve);
			HalconAPI.StoreI(proc, 10, maxWidthCurve);
			HalconAPI.StoreD(proc, 11, weight1);
			HalconAPI.StoreD(proc, 12, weight2);
			HalconAPI.StoreD(proc, 13, weight3);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out arcCenterRow);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out arcCenterCol);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out arcAngle);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out arcBeginRow);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out arcBeginCol);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out lineBeginRow);
			err = HTuple.LoadNew(proc, 6, HTupleType.INTEGER, err, out lineBeginCol);
			err = HTuple.LoadNew(proc, 7, HTupleType.INTEGER, err, out lineEndRow);
			err = HTuple.LoadNew(proc, 8, HTupleType.INTEGER, err, out lineEndCol);
			err = HTuple.LoadNew(proc, 9, HTupleType.INTEGER, err, out order);
			HalconAPI.PostCall(proc, err);
		}

		public static void CloseAllClassBox()
		{
			IntPtr proc = HalconAPI.PreCall(1900);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void GenCaltab(int XNum, int YNum, double markDist, double diameterRatio, string calPlateDescr, string calPlatePSFile)
		{
			IntPtr proc = HalconAPI.PreCall(1926);
			HalconAPI.StoreI(proc, 0, XNum);
			HalconAPI.StoreI(proc, 1, YNum);
			HalconAPI.StoreD(proc, 2, markDist);
			HalconAPI.StoreD(proc, 3, diameterRatio);
			HalconAPI.StoreS(proc, 4, calPlateDescr);
			HalconAPI.StoreS(proc, 5, calPlatePSFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void CreateCaltab(int numRows, int marksPerRow, double diameter, HTuple finderRow, HTuple finderColumn, string polarity, string calPlateDescr, string calPlatePSFile)
		{
			IntPtr proc = HalconAPI.PreCall(1927);
			HalconAPI.StoreI(proc, 0, numRows);
			HalconAPI.StoreI(proc, 1, marksPerRow);
			HalconAPI.StoreD(proc, 2, diameter);
			HalconAPI.Store(proc, 3, finderRow);
			HalconAPI.Store(proc, 4, finderColumn);
			HalconAPI.StoreS(proc, 5, polarity);
			HalconAPI.StoreS(proc, 6, calPlateDescr);
			HalconAPI.StoreS(proc, 7, calPlatePSFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(finderRow);
			HalconAPI.UnpinTuple(finderColumn);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void CreateCaltab(int numRows, int marksPerRow, double diameter, int finderRow, int finderColumn, string polarity, string calPlateDescr, string calPlatePSFile)
		{
			IntPtr proc = HalconAPI.PreCall(1927);
			HalconAPI.StoreI(proc, 0, numRows);
			HalconAPI.StoreI(proc, 1, marksPerRow);
			HalconAPI.StoreD(proc, 2, diameter);
			HalconAPI.StoreI(proc, 3, finderRow);
			HalconAPI.StoreI(proc, 4, finderColumn);
			HalconAPI.StoreS(proc, 5, polarity);
			HalconAPI.StoreS(proc, 6, calPlateDescr);
			HalconAPI.StoreS(proc, 7, calPlatePSFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void CaltabPoints(string calPlateDescr, out HTuple x, out HTuple y, out HTuple z)
		{
			IntPtr proc = HalconAPI.PreCall(1928);
			HalconAPI.StoreS(proc, 0, calPlateDescr);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out z);
			HalconAPI.PostCall(proc, err);
		}

		public static void CloseAllBgEsti()
		{
			IntPtr proc = HalconAPI.PreCall(2009);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void CloseAllFramegrabbers()
		{
			IntPtr proc = HalconAPI.PreCall(2035);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}
	}
}
