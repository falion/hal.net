using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HDataCode2D : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDataCode2D()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDataCode2D(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDataCode2D obj)
		{
			obj = new HDataCode2D(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDataCode2D[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HDataCode2D[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HDataCode2D(hTuple[i].IP);
			}
			return err;
		}

		public HDataCode2D(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1774);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDataCode2D(string symbolType, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1778);
			HalconAPI.StoreS(proc, 0, symbolType);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDataCode2D(string symbolType, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1778);
			HalconAPI.StoreS(proc, 0, symbolType);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeDataCode2dModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDataCode2D(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeDataCode2dModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeDataCode2dModel().Serialize(stream);
		}

		public static HDataCode2D Deserialize(Stream stream)
		{
			HDataCode2D hDataCode2D = new HDataCode2D();
			hDataCode2D.DeserializeDataCode2dModel(HSerializedItem.Deserialize(stream));
			return hDataCode2D;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HDataCode2D Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeDataCode2dModel();
			HDataCode2D hDataCode2D = new HDataCode2D();
			hDataCode2D.DeserializeDataCode2dModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDataCode2D;
		}

		public HObject GetDataCode2dObjects(HTuple candidateHandle, string objectName)
		{
			IntPtr proc = HalconAPI.PreCall(1766);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, candidateHandle);
			HalconAPI.StoreS(proc, 2, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(candidateHandle);
			HObject result = null;
			err = HObject.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HObject GetDataCode2dObjects(int candidateHandle, string objectName)
		{
			IntPtr proc = HalconAPI.PreCall(1766);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, candidateHandle);
			HalconAPI.StoreS(proc, 2, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HObject result = null;
			err = HObject.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetDataCode2dResults(HTuple candidateHandle, HTuple resultNames)
		{
			IntPtr proc = HalconAPI.PreCall(1767);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, candidateHandle);
			HalconAPI.Store(proc, 2, resultNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(candidateHandle);
			HalconAPI.UnpinTuple(resultNames);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetDataCode2dResults(string candidateHandle, string resultNames)
		{
			IntPtr proc = HalconAPI.PreCall(1767);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, candidateHandle);
			HalconAPI.StoreS(proc, 2, resultNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont FindDataCode2d(HImage image, HTuple genParamName, HTuple genParamValue, out HTuple resultHandles, out HTuple decodedDataStrings)
		{
			IntPtr proc = HalconAPI.PreCall(1768);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out resultHandles);
			err = HTuple.LoadNew(proc, 1, err, out decodedDataStrings);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HXLDCont FindDataCode2d(HImage image, string genParamName, int genParamValue, out int resultHandles, out string decodedDataStrings)
		{
			IntPtr proc = HalconAPI.PreCall(1768);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreI(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HalconAPI.LoadI(proc, 0, err, out resultHandles);
			err = HalconAPI.LoadS(proc, 1, err, out decodedDataStrings);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void SetDataCode2dParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1769);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDataCode2dParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1769);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetDataCode2dParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1770);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetDataCode2dParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1770);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryDataCode2dParams(string queryName)
		{
			IntPtr proc = HalconAPI.PreCall(1771);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, queryName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializeDataCode2dModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1772);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeDataCode2dModel()
		{
			IntPtr proc = HalconAPI.PreCall(1773);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadDataCode2dModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1774);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteDataCode2dModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1775);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateDataCode2dModel(string symbolType, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1778);
			HalconAPI.StoreS(proc, 0, symbolType);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDataCode2dModel(string symbolType, string genParamName, string genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1778);
			HalconAPI.StoreS(proc, 0, symbolType);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1777);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
