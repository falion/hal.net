using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HColorTransLUT : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HColorTransLUT()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HColorTransLUT(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HColorTransLUT obj)
		{
			obj = new HColorTransLUT(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HColorTransLUT[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HColorTransLUT[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HColorTransLUT(hTuple[i].IP);
			}
			return err;
		}

		public HColorTransLUT(string colorSpace, string transDirection, int numBits)
		{
			IntPtr proc = HalconAPI.PreCall(1579);
			HalconAPI.StoreS(proc, 0, colorSpace);
			HalconAPI.StoreS(proc, 1, transDirection);
			HalconAPI.StoreI(proc, 2, numBits);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage ApplyColorTransLut(HImage image1, HImage image2, HImage image3, out HImage imageResult2, out HImage imageResult3)
		{
			IntPtr proc = HalconAPI.PreCall(1578);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 3, image3);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imageResult2);
			err = HImage.LoadNew(proc, 3, err, out imageResult3);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			GC.KeepAlive(image3);
			return result;
		}

		public void CreateColorTransLut(string colorSpace, string transDirection, int numBits)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1579);
			HalconAPI.StoreS(proc, 0, colorSpace);
			HalconAPI.StoreS(proc, 1, transDirection);
			HalconAPI.StoreI(proc, 2, numBits);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1577);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
