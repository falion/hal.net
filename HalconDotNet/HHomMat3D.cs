using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HHomMat3D : HData, ISerializable, ICloneable
	{
		private const int FIXEDSIZE = 12;

		public HHomMat3D(HTuple tuple)
			: base(tuple)
		{
		}

		internal HHomMat3D(HData data)
			: base(data)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, HTupleType type, int err, out HHomMat3D obj)
		{
			HTuple t = null;
			err = HTuple.LoadNew(proc, parIndex, err, out t);
			obj = new HHomMat3D(new HData(t));
			return err;
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HHomMat3D obj)
		{
			return HHomMat3D.LoadNew(proc, parIndex, HTupleType.MIXED, err, out obj);
		}

		internal static HHomMat3D[] SplitArray(HTuple data)
		{
			int num = data.Length / 12;
			HHomMat3D[] array = new HHomMat3D[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = new HHomMat3D(new HData(data.TupleSelectRange(i * 12, (i + 1) * 12 - 1)));
			}
			return array;
		}

		public HHomMat3D()
		{
			IntPtr proc = HalconAPI.PreCall(253);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeHomMat3d();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HHomMat3D(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeHomMat3d(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeHomMat3d().Serialize(stream);
		}

		public static HHomMat3D Deserialize(Stream stream)
		{
			HHomMat3D hHomMat3D = new HHomMat3D();
			hHomMat3D.DeserializeHomMat3d(HSerializedItem.Deserialize(stream));
			return hHomMat3D;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HHomMat3D Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeHomMat3d();
			HHomMat3D hHomMat3D = new HHomMat3D();
			hHomMat3D.DeserializeHomMat3d(hSerializedItem);
			hSerializedItem.Dispose();
			return hHomMat3D;
		}

		public void DeserializeHomMat3d(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(233);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeHomMat3d()
		{
			IntPtr proc = HalconAPI.PreCall(234);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ProjectiveTransHomPoint3d(HTuple px, HTuple py, HTuple pz, HTuple pw, out HTuple qy, out HTuple qz, out HTuple qw)
		{
			IntPtr proc = HalconAPI.PreCall(239);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.Store(proc, 3, pz);
			HalconAPI.Store(proc, 4, pw);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(pz);
			HalconAPI.UnpinTuple(pw);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out qy);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out qz);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out qw);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double ProjectiveTransHomPoint3d(double px, double py, double pz, double pw, out double qy, out double qz, out double qw)
		{
			IntPtr proc = HalconAPI.PreCall(239);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.StoreD(proc, 3, pz);
			HalconAPI.StoreD(proc, 4, pw);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out qy);
			err = HalconAPI.LoadD(proc, 2, err, out qz);
			err = HalconAPI.LoadD(proc, 3, err, out qw);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ProjectiveTransPoint3d(HTuple px, HTuple py, HTuple pz, out HTuple qy, out HTuple qz)
		{
			IntPtr proc = HalconAPI.PreCall(240);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.Store(proc, 3, pz);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(pz);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out qy);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out qz);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double ProjectiveTransPoint3d(double px, double py, double pz, out double qy, out double qz)
		{
			IntPtr proc = HalconAPI.PreCall(240);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.StoreD(proc, 3, pz);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out qy);
			err = HalconAPI.LoadD(proc, 2, err, out qz);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple AffineTransPoint3d(HTuple px, HTuple py, HTuple pz, out HTuple qy, out HTuple qz)
		{
			IntPtr proc = HalconAPI.PreCall(241);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.Store(proc, 3, pz);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(pz);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out qy);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out qz);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double AffineTransPoint3d(double px, double py, double pz, out double qy, out double qz)
		{
			IntPtr proc = HalconAPI.PreCall(241);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.StoreD(proc, 3, pz);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out qy);
			err = HalconAPI.LoadD(proc, 2, err, out qz);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void VectorToHomMat3d(string transformationType, HTuple px, HTuple py, HTuple pz, HTuple qx, HTuple qy, HTuple qz)
		{
			IntPtr proc = HalconAPI.PreCall(242);
			HalconAPI.StoreS(proc, 0, transformationType);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.Store(proc, 3, pz);
			HalconAPI.Store(proc, 4, qx);
			HalconAPI.Store(proc, 5, qy);
			HalconAPI.Store(proc, 6, qz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(pz);
			HalconAPI.UnpinTuple(qx);
			HalconAPI.UnpinTuple(qy);
			HalconAPI.UnpinTuple(qz);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public double HomMat3dDeterminant()
		{
			IntPtr proc = HalconAPI.PreCall(243);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dTranspose()
		{
			IntPtr proc = HalconAPI.PreCall(244);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dInvert()
		{
			IntPtr proc = HalconAPI.PreCall(245);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dCompose(HHomMat3D homMat3DRight)
		{
			IntPtr proc = HalconAPI.PreCall(246);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, homMat3DRight);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(homMat3DRight);
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dRotateLocal(HTuple phi, HTuple axis)
		{
			IntPtr proc = HalconAPI.PreCall(247);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, phi);
			HalconAPI.Store(proc, 2, axis);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(axis);
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dRotateLocal(double phi, string axis)
		{
			IntPtr proc = HalconAPI.PreCall(247);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, phi);
			HalconAPI.StoreS(proc, 2, axis);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dRotate(HTuple phi, HTuple axis, HTuple px, HTuple py, HTuple pz)
		{
			IntPtr proc = HalconAPI.PreCall(248);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, phi);
			HalconAPI.Store(proc, 2, axis);
			HalconAPI.Store(proc, 3, px);
			HalconAPI.Store(proc, 4, py);
			HalconAPI.Store(proc, 5, pz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(axis);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(pz);
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dRotate(double phi, string axis, double px, double py, double pz)
		{
			IntPtr proc = HalconAPI.PreCall(248);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, phi);
			HalconAPI.StoreS(proc, 2, axis);
			HalconAPI.StoreD(proc, 3, px);
			HalconAPI.StoreD(proc, 4, py);
			HalconAPI.StoreD(proc, 5, pz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dScaleLocal(HTuple sx, HTuple sy, HTuple sz)
		{
			IntPtr proc = HalconAPI.PreCall(249);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, sx);
			HalconAPI.Store(proc, 2, sy);
			HalconAPI.Store(proc, 3, sz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(sx);
			HalconAPI.UnpinTuple(sy);
			HalconAPI.UnpinTuple(sz);
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dScaleLocal(double sx, double sy, double sz)
		{
			IntPtr proc = HalconAPI.PreCall(249);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, sx);
			HalconAPI.StoreD(proc, 2, sy);
			HalconAPI.StoreD(proc, 3, sz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dScale(HTuple sx, HTuple sy, HTuple sz, HTuple px, HTuple py, HTuple pz)
		{
			IntPtr proc = HalconAPI.PreCall(250);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, sx);
			HalconAPI.Store(proc, 2, sy);
			HalconAPI.Store(proc, 3, sz);
			HalconAPI.Store(proc, 4, px);
			HalconAPI.Store(proc, 5, py);
			HalconAPI.Store(proc, 6, pz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(sx);
			HalconAPI.UnpinTuple(sy);
			HalconAPI.UnpinTuple(sz);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(pz);
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dScale(double sx, double sy, double sz, double px, double py, double pz)
		{
			IntPtr proc = HalconAPI.PreCall(250);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, sx);
			HalconAPI.StoreD(proc, 2, sy);
			HalconAPI.StoreD(proc, 3, sz);
			HalconAPI.StoreD(proc, 4, px);
			HalconAPI.StoreD(proc, 5, py);
			HalconAPI.StoreD(proc, 6, pz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dTranslateLocal(HTuple tx, HTuple ty, HTuple tz)
		{
			IntPtr proc = HalconAPI.PreCall(251);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, tx);
			HalconAPI.Store(proc, 2, ty);
			HalconAPI.Store(proc, 3, tz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(tx);
			HalconAPI.UnpinTuple(ty);
			HalconAPI.UnpinTuple(tz);
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dTranslateLocal(double tx, double ty, double tz)
		{
			IntPtr proc = HalconAPI.PreCall(251);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, tx);
			HalconAPI.StoreD(proc, 2, ty);
			HalconAPI.StoreD(proc, 3, tz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dTranslate(HTuple tx, HTuple ty, HTuple tz)
		{
			IntPtr proc = HalconAPI.PreCall(252);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, tx);
			HalconAPI.Store(proc, 2, ty);
			HalconAPI.Store(proc, 3, tz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(tx);
			HalconAPI.UnpinTuple(ty);
			HalconAPI.UnpinTuple(tz);
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D HomMat3dTranslate(double tx, double ty, double tz)
		{
			IntPtr proc = HalconAPI.PreCall(252);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, tx);
			HalconAPI.StoreD(proc, 2, ty);
			HalconAPI.StoreD(proc, 3, tz);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void HomMat3dIdentity()
		{
			IntPtr proc = HalconAPI.PreCall(253);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HHomMat2D HomMat3dProject(HTuple principalPointRow, HTuple principalPointCol, HTuple focus)
		{
			IntPtr proc = HalconAPI.PreCall(254);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, principalPointRow);
			HalconAPI.Store(proc, 2, principalPointCol);
			HalconAPI.Store(proc, 3, focus);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(principalPointRow);
			HalconAPI.UnpinTuple(principalPointCol);
			HalconAPI.UnpinTuple(focus);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat3dProject(double principalPointRow, double principalPointCol, double focus)
		{
			IntPtr proc = HalconAPI.PreCall(254);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, principalPointRow);
			HalconAPI.StoreD(proc, 2, principalPointCol);
			HalconAPI.StoreD(proc, 3, focus);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ProjectHomPointHomMat3d(HTuple px, HTuple py, HTuple pz, HTuple pw, out HTuple qy, out HTuple qw)
		{
			IntPtr proc = HalconAPI.PreCall(1930);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.Store(proc, 3, pz);
			HalconAPI.Store(proc, 4, pw);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(pz);
			HalconAPI.UnpinTuple(pw);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out qy);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out qw);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double ProjectHomPointHomMat3d(double px, double py, double pz, double pw, out double qy, out double qw)
		{
			IntPtr proc = HalconAPI.PreCall(1930);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.StoreD(proc, 3, pz);
			HalconAPI.StoreD(proc, 4, pw);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out qy);
			err = HalconAPI.LoadD(proc, 2, err, out qw);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ProjectPointHomMat3d(HTuple px, HTuple py, HTuple pz, out HTuple qy)
		{
			IntPtr proc = HalconAPI.PreCall(1931);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.Store(proc, 3, pz);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(pz);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out qy);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double ProjectPointHomMat3d(double px, double py, double pz, out double qy)
		{
			IntPtr proc = HalconAPI.PreCall(1931);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.StoreD(proc, 3, pz);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out qy);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HPose HomMat3dToPose()
		{
			IntPtr proc = HalconAPI.PreCall(1934);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}
	}
}
