using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HBgEsti : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBgEsti()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBgEsti(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HBgEsti obj)
		{
			obj = new HBgEsti(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HBgEsti[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HBgEsti[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HBgEsti(hTuple[i].IP);
			}
			return err;
		}

		public HBgEsti(HImage initializeImage, double syspar1, double syspar2, string gainMode, double gain1, double gain2, string adaptMode, double minDiff, int statNum, double confidenceC, double timeC)
		{
			IntPtr proc = HalconAPI.PreCall(2008);
			HalconAPI.Store(proc, 1, initializeImage);
			HalconAPI.StoreD(proc, 0, syspar1);
			HalconAPI.StoreD(proc, 1, syspar2);
			HalconAPI.StoreS(proc, 2, gainMode);
			HalconAPI.StoreD(proc, 3, gain1);
			HalconAPI.StoreD(proc, 4, gain2);
			HalconAPI.StoreS(proc, 5, adaptMode);
			HalconAPI.StoreD(proc, 6, minDiff);
			HalconAPI.StoreI(proc, 7, statNum);
			HalconAPI.StoreD(proc, 8, confidenceC);
			HalconAPI.StoreD(proc, 9, timeC);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(initializeImage);
		}

		public HImage GiveBgEsti()
		{
			IntPtr proc = HalconAPI.PreCall(2003);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void UpdateBgEsti(HImage presentImage, HRegion upDateRegion)
		{
			IntPtr proc = HalconAPI.PreCall(2004);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, presentImage);
			HalconAPI.Store(proc, 2, upDateRegion);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(presentImage);
			GC.KeepAlive(upDateRegion);
		}

		public HRegion RunBgEsti(HImage presentImage)
		{
			IntPtr proc = HalconAPI.PreCall(2005);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, presentImage);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(presentImage);
			return result;
		}

		public double GetBgEstiParams(out double syspar2, out string gainMode, out double gain1, out double gain2, out string adaptMode, out double minDiff, out int statNum, out double confidenceC, out double timeC)
		{
			IntPtr proc = HalconAPI.PreCall(2006);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out syspar2);
			err = HalconAPI.LoadS(proc, 2, err, out gainMode);
			err = HalconAPI.LoadD(proc, 3, err, out gain1);
			err = HalconAPI.LoadD(proc, 4, err, out gain2);
			err = HalconAPI.LoadS(proc, 5, err, out adaptMode);
			err = HalconAPI.LoadD(proc, 6, err, out minDiff);
			err = HalconAPI.LoadI(proc, 7, err, out statNum);
			err = HalconAPI.LoadD(proc, 8, err, out confidenceC);
			err = HalconAPI.LoadD(proc, 9, err, out timeC);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetBgEstiParams(double syspar1, double syspar2, string gainMode, double gain1, double gain2, string adaptMode, double minDiff, int statNum, double confidenceC, double timeC)
		{
			IntPtr proc = HalconAPI.PreCall(2007);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, syspar1);
			HalconAPI.StoreD(proc, 2, syspar2);
			HalconAPI.StoreS(proc, 3, gainMode);
			HalconAPI.StoreD(proc, 4, gain1);
			HalconAPI.StoreD(proc, 5, gain2);
			HalconAPI.StoreS(proc, 6, adaptMode);
			HalconAPI.StoreD(proc, 7, minDiff);
			HalconAPI.StoreI(proc, 8, statNum);
			HalconAPI.StoreD(proc, 9, confidenceC);
			HalconAPI.StoreD(proc, 10, timeC);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateBgEsti(HImage initializeImage, double syspar1, double syspar2, string gainMode, double gain1, double gain2, string adaptMode, double minDiff, int statNum, double confidenceC, double timeC)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2008);
			HalconAPI.Store(proc, 1, initializeImage);
			HalconAPI.StoreD(proc, 0, syspar1);
			HalconAPI.StoreD(proc, 1, syspar2);
			HalconAPI.StoreS(proc, 2, gainMode);
			HalconAPI.StoreD(proc, 3, gain1);
			HalconAPI.StoreD(proc, 4, gain2);
			HalconAPI.StoreS(proc, 5, adaptMode);
			HalconAPI.StoreD(proc, 6, minDiff);
			HalconAPI.StoreI(proc, 7, statNum);
			HalconAPI.StoreD(proc, 8, confidenceC);
			HalconAPI.StoreD(proc, 9, timeC);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(initializeImage);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(2002);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
