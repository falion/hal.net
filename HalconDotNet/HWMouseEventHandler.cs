using System.Windows.Input;

namespace VisionConfig
{
	internal delegate void HWMouseEventHandler(int x, int y, MouseButton? button, int delta);
}
