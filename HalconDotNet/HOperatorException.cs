using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HOperatorException : HalconException
	{
		public HOperatorException(int err, string sInfo, Exception inner)
			: base(err, (sInfo == "") ? HalconAPI.GetErrorMessage(err) : sInfo, inner)
		{
		}

		public HOperatorException(int err, string sInfo)
			: this(err, sInfo, null)
		{
		}

		public HOperatorException(int err)
			: this(err, "")
		{
		}

		[Obsolete("GetErrorText is deprecated, please use GetErrorMessage instead.")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new string GetErrorText()
		{
			return HalconAPI.GetErrorMessage(base.GetErrorCode());
		}

		public new string GetErrorMessage()
		{
			return HalconAPI.GetErrorMessage(base.GetErrorCode());
		}

		public long GetExtendedErrorCode()
		{
			HTuple hTuple = null;
			HTuple hTuple2 = null;
			HTuple hTuple3 = null;
			HOperatorSet.GetExtendedErrorInfo(out hTuple, out hTuple2, out hTuple3);
			if (hTuple2.Length > 0)
			{
				return hTuple2[0].L;
			}
			return 0L;
		}

		public string GetExtendedErrorMessage()
		{
			HTuple hTuple = null;
			HTuple hTuple2 = null;
			HTuple hTuple3 = null;
			HOperatorSet.GetExtendedErrorInfo(out hTuple, out hTuple2, out hTuple3);
			if (hTuple3.Length > 0)
			{
				return hTuple3[0];
			}
			return "";
		}

		public static void throwOperator(int err, string logicalName)
		{
			if (HalconAPI.IsFailure(err))
			{
				throw new HOperatorException(err, HalconAPI.GetErrorMessage(err) + " in operator " + logicalName);
			}
		}

		internal static void throwOperator(int err, int procIndex)
		{
			if (HalconAPI.IsFailure(err))
			{
				string logicalName = HalconAPI.GetLogicalName(procIndex);
				throw new HOperatorException(err, HalconAPI.GetErrorMessage(err) + " in operator " + logicalName);
			}
		}

		public static void throwInfo(int err, string sInfo)
		{
			throw new HOperatorException(err, sInfo + ":\n" + HalconAPI.GetErrorMessage(err) + "\n");
		}
	}
}
