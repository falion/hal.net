using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HScatteredDataInterpolator : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HScatteredDataInterpolator()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HScatteredDataInterpolator(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HScatteredDataInterpolator obj)
		{
			obj = new HScatteredDataInterpolator(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HScatteredDataInterpolator[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HScatteredDataInterpolator[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HScatteredDataInterpolator(hTuple[i].IP);
			}
			return err;
		}

		public HScatteredDataInterpolator(string method, HTuple rows, HTuple columns, HTuple values, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(292);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.Store(proc, 1, rows);
			HalconAPI.Store(proc, 2, columns);
			HalconAPI.Store(proc, 3, values);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(columns);
			HalconAPI.UnpinTuple(values);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple InterpolateScatteredData(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(291);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double InterpolateScatteredData(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(291);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void CreateScatteredDataInterpolator(string method, HTuple rows, HTuple columns, HTuple values, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(292);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.Store(proc, 1, rows);
			HalconAPI.Store(proc, 2, columns);
			HalconAPI.Store(proc, 3, values);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(columns);
			HalconAPI.UnpinTuple(values);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(290);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
