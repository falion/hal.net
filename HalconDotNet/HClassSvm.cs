using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HClassSvm : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassSvm()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassSvm(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassSvm obj)
		{
			obj = new HClassSvm(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassSvm[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HClassSvm[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HClassSvm(hTuple[i].IP);
			}
			return err;
		}

		public HClassSvm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1846);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassSvm(int numFeatures, string kernelType, double kernelParam, double nu, int numClasses, string mode, string preprocessing, int numComponents)
		{
			IntPtr proc = HalconAPI.PreCall(1861);
			HalconAPI.StoreI(proc, 0, numFeatures);
			HalconAPI.StoreS(proc, 1, kernelType);
			HalconAPI.StoreD(proc, 2, kernelParam);
			HalconAPI.StoreD(proc, 3, nu);
			HalconAPI.StoreI(proc, 4, numClasses);
			HalconAPI.StoreS(proc, 5, mode);
			HalconAPI.StoreS(proc, 6, preprocessing);
			HalconAPI.StoreI(proc, 7, numComponents);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeClassSvm();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassSvm(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeClassSvm(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeClassSvm().Serialize(stream);
		}

		public static HClassSvm Deserialize(Stream stream)
		{
			HClassSvm hClassSvm = new HClassSvm();
			hClassSvm.DeserializeClassSvm(HSerializedItem.Deserialize(stream));
			return hClassSvm;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HClassSvm Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeClassSvm();
			HClassSvm hClassSvm = new HClassSvm();
			hClassSvm.DeserializeClassSvm(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassSvm;
		}

		public HRegion ClassifyImageClassSvm(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(433);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void AddSamplesImageClassSvm(HImage image, HRegion classRegions)
		{
			IntPtr proc = HalconAPI.PreCall(434);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 2, classRegions);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(classRegions);
		}

		public HClassTrainData GetClassTrainDataSvm()
		{
			IntPtr proc = HalconAPI.PreCall(1791);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HClassTrainData result = null;
			err = HClassTrainData.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AddClassTrainDataSvm(HClassTrainData classTrainDataHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1792);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, classTrainDataHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
		}

		public HTuple SelectFeatureSetSvm(HClassTrainData classTrainDataHandle, string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1800);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return result;
		}

		public HTuple SelectFeatureSetSvm(HClassTrainData classTrainDataHandle, string selectionMethod, string genParamName, double genParamValue, out HTuple score)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1800);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return result;
		}

		public HClassLUT CreateClassLutSvm(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1821);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HClassLUT result = null;
			err = HClassLUT.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static void ClearSamplesClassSvm(HClassSvm[] SVMHandle)
		{
			HTuple hTuple = HTool.ConcatArray(SVMHandle);
			IntPtr proc = HalconAPI.PreCall(1843);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(SVMHandle);
		}

		public void ClearSamplesClassSvm()
		{
			IntPtr proc = HalconAPI.PreCall(1843);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeClassSvm(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1844);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeClassSvm()
		{
			IntPtr proc = HalconAPI.PreCall(1845);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadClassSvm(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1846);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteClassSvm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1847);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadSamplesClassSvm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1848);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteSamplesClassSvm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1849);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple EvaluateClassSvm(HTuple features)
		{
			IntPtr proc = HalconAPI.PreCall(1850);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ClassifyClassSvm(HTuple features, HTuple num)
		{
			IntPtr proc = HalconAPI.PreCall(1851);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.Store(proc, 2, num);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(num);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HClassSvm ReduceClassSvm(string method, int minRemainingSV, double maxError)
		{
			IntPtr proc = HalconAPI.PreCall(1852);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.StoreI(proc, 2, minRemainingSV);
			HalconAPI.StoreD(proc, 3, maxError);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HClassSvm result = null;
			err = HClassSvm.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void TrainClassSvm(double epsilon, HTuple trainMode)
		{
			IntPtr proc = HalconAPI.PreCall(1853);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, epsilon);
			HalconAPI.Store(proc, 2, trainMode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainMode);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TrainClassSvm(double epsilon, string trainMode)
		{
			IntPtr proc = HalconAPI.PreCall(1853);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, epsilon);
			HalconAPI.StoreS(proc, 2, trainMode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetPrepInfoClassSvm(string preprocessing, out HTuple cumInformationCont)
		{
			IntPtr proc = HalconAPI.PreCall(1854);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, preprocessing);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cumInformationCont);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetSupportVectorNumClassSvm(out HTuple numSVPerSVM)
		{
			IntPtr proc = HalconAPI.PreCall(1855);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out numSVPerSVM);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double GetSupportVectorClassSvm(HTuple indexSupportVector)
		{
			IntPtr proc = HalconAPI.PreCall(1856);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, indexSupportVector);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(indexSupportVector);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetSampleNumClassSvm()
		{
			IntPtr proc = HalconAPI.PreCall(1857);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetSampleClassSvm(int indexSample, out int target)
		{
			IntPtr proc = HalconAPI.PreCall(1858);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, indexSample);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out target);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AddSampleClassSvm(HTuple features, HTuple classVal)
		{
			IntPtr proc = HalconAPI.PreCall(1859);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.Store(proc, 2, classVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(classVal);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AddSampleClassSvm(HTuple features, int classVal)
		{
			IntPtr proc = HalconAPI.PreCall(1859);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.StoreI(proc, 2, classVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int GetParamsClassSvm(out string kernelType, out double kernelParam, out double nu, out int numClasses, out string mode, out string preprocessing, out int numComponents)
		{
			IntPtr proc = HalconAPI.PreCall(1860);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadS(proc, 1, err, out kernelType);
			err = HalconAPI.LoadD(proc, 2, err, out kernelParam);
			err = HalconAPI.LoadD(proc, 3, err, out nu);
			err = HalconAPI.LoadI(proc, 4, err, out numClasses);
			err = HalconAPI.LoadS(proc, 5, err, out mode);
			err = HalconAPI.LoadS(proc, 6, err, out preprocessing);
			err = HalconAPI.LoadI(proc, 7, err, out numComponents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void CreateClassSvm(int numFeatures, string kernelType, double kernelParam, double nu, int numClasses, string mode, string preprocessing, int numComponents)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1861);
			HalconAPI.StoreI(proc, 0, numFeatures);
			HalconAPI.StoreS(proc, 1, kernelType);
			HalconAPI.StoreD(proc, 2, kernelParam);
			HalconAPI.StoreD(proc, 3, nu);
			HalconAPI.StoreI(proc, 4, numClasses);
			HalconAPI.StoreS(proc, 5, mode);
			HalconAPI.StoreS(proc, 6, preprocessing);
			HalconAPI.StoreI(proc, 7, numComponents);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1842);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
