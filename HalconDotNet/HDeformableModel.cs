using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HDeformableModel : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableModel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDeformableModel obj)
		{
			obj = new HDeformableModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDeformableModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HDeformableModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HDeformableModel(hTuple[i].IP);
			}
			return err;
		}

		public HDeformableModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(965);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDeformableModel(HXLDCont contours, HCamPar camParam, HPose referencePose, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(976);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public HDeformableModel(HXLDCont contours, HCamPar camParam, HPose referencePose, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(976);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public HDeformableModel(HXLDCont contours, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(977);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public HDeformableModel(HXLDCont contours, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(977);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public HDeformableModel(HImage template, HCamPar camParam, HPose referencePose, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(979);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.Store(proc, 14, contrast);
			HalconAPI.Store(proc, 15, minContrast);
			HalconAPI.Store(proc, 16, genParamName);
			HalconAPI.Store(proc, 17, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HDeformableModel(HImage template, HCamPar camParam, HPose referencePose, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, HTuple contrast, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(979);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.Store(proc, 14, contrast);
			HalconAPI.StoreI(proc, 15, minContrast);
			HalconAPI.Store(proc, 16, genParamName);
			HalconAPI.Store(proc, 17, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HDeformableModel(HImage template, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(980);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.Store(proc, 13, minContrast);
			HalconAPI.Store(proc, 14, genParamName);
			HalconAPI.Store(proc, 15, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HDeformableModel(HImage template, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, HTuple contrast, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(980);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.StoreI(proc, 13, minContrast);
			HalconAPI.Store(proc, 14, genParamName);
			HalconAPI.Store(proc, 15, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeDeformableModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeDeformableModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeDeformableModel().Serialize(stream);
		}

		public static HDeformableModel Deserialize(Stream stream)
		{
			HDeformableModel hDeformableModel = new HDeformableModel();
			hDeformableModel.DeserializeDeformableModel(HSerializedItem.Deserialize(stream));
			return hDeformableModel;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HDeformableModel Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeDeformableModel();
			HDeformableModel hDeformableModel = new HDeformableModel();
			hDeformableModel.DeserializeDeformableModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDeformableModel;
		}

		public void GetDeformableModelOrigin(out double row, out double column)
		{
			IntPtr proc = HalconAPI.PreCall(957);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SetDeformableModelOrigin(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(958);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDeformableModelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(959);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetDeformableModelParams(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(960);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetDeformableModelParams(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(960);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont GetDeformableModelContours(int level)
		{
			IntPtr proc = HalconAPI.PreCall(961);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, level);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializeDeformableModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(963);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeDeformableModel()
		{
			IntPtr proc = HalconAPI.PreCall(964);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadDeformableModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(965);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteDeformableModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(966);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HImage FindLocalDeformableModel(HImage image, out HImage vectorField, out HXLDCont deformedContours, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, int numLevels, double greediness, HTuple resultType, HTuple genParamName, HTuple genParamValue, out HTuple score, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(969);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.StoreI(proc, 10, numLevels);
			HalconAPI.StoreD(proc, 11, greediness);
			HalconAPI.Store(proc, 12, resultType);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultType);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out vectorField);
			err = HXLDCont.LoadNew(proc, 3, err, out deformedContours);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HPose[] FindPlanarCalibDeformableModel(HImage image, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, HTuple numLevels, double greediness, HTuple genParamName, HTuple genParamValue, out HTuple covPose, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(970);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.Store(proc, 10, numLevels);
			HalconAPI.StoreD(proc, 11, greediness);
			HalconAPI.Store(proc, 12, genParamName);
			HalconAPI.Store(proc, 13, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HPose FindPlanarCalibDeformableModel(HImage image, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, int numLevels, double greediness, HTuple genParamName, HTuple genParamValue, out HTuple covPose, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(970);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.StoreI(proc, 10, numLevels);
			HalconAPI.StoreD(proc, 11, greediness);
			HalconAPI.Store(proc, 12, genParamName);
			HalconAPI.Store(proc, 13, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HHomMat2D[] FindPlanarUncalibDeformableModel(HImage image, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, HTuple numLevels, double greediness, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(971);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.Store(proc, 10, numLevels);
			HalconAPI.StoreD(proc, 11, greediness);
			HalconAPI.Store(proc, 12, genParamName);
			HalconAPI.Store(proc, 13, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			HHomMat2D[] result = HHomMat2D.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HHomMat2D FindPlanarUncalibDeformableModel(HImage image, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, int numLevels, double greediness, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(971);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.StoreI(proc, 10, numLevels);
			HalconAPI.StoreD(proc, 11, greediness);
			HalconAPI.Store(proc, 12, genParamName);
			HalconAPI.Store(proc, 13, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void SetLocalDeformableModelMetric(HImage image, HImage vectorField, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(972);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 2, vectorField);
			HalconAPI.StoreS(proc, 1, metric);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(vectorField);
		}

		public void SetPlanarCalibDeformableModelMetric(HImage image, HPose pose, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(973);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, pose);
			HalconAPI.StoreS(proc, 2, metric);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void SetPlanarUncalibDeformableModelMetric(HImage image, HHomMat2D homMat2D, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(974);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, homMat2D);
			HalconAPI.StoreS(proc, 2, metric);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void CreateLocalDeformableModelXld(HXLDCont contours, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(975);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreateLocalDeformableModelXld(HXLDCont contours, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(975);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreatePlanarCalibDeformableModelXld(HXLDCont contours, HCamPar camParam, HPose referencePose, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(976);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreatePlanarCalibDeformableModelXld(HXLDCont contours, HCamPar camParam, HPose referencePose, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(976);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreatePlanarUncalibDeformableModelXld(HXLDCont contours, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(977);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreatePlanarUncalibDeformableModelXld(HXLDCont contours, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(977);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreateLocalDeformableModel(HImage template, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(978);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.Store(proc, 13, minContrast);
			HalconAPI.Store(proc, 14, genParamName);
			HalconAPI.Store(proc, 15, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreateLocalDeformableModel(HImage template, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, HTuple contrast, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(978);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.StoreI(proc, 13, minContrast);
			HalconAPI.Store(proc, 14, genParamName);
			HalconAPI.Store(proc, 15, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreatePlanarCalibDeformableModel(HImage template, HCamPar camParam, HPose referencePose, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(979);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.Store(proc, 14, contrast);
			HalconAPI.Store(proc, 15, minContrast);
			HalconAPI.Store(proc, 16, genParamName);
			HalconAPI.Store(proc, 17, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreatePlanarCalibDeformableModel(HImage template, HCamPar camParam, HPose referencePose, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, HTuple contrast, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(979);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.Store(proc, 14, contrast);
			HalconAPI.StoreI(proc, 15, minContrast);
			HalconAPI.Store(proc, 16, genParamName);
			HalconAPI.Store(proc, 17, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreatePlanarUncalibDeformableModel(HImage template, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(980);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.Store(proc, 13, minContrast);
			HalconAPI.Store(proc, 14, genParamName);
			HalconAPI.Store(proc, 15, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreatePlanarUncalibDeformableModel(HImage template, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, HTuple contrast, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(980);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.StoreI(proc, 13, minContrast);
			HalconAPI.Store(proc, 14, genParamName);
			HalconAPI.Store(proc, 15, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(968);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
