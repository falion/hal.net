using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HDeformableSurfaceModel : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceModel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDeformableSurfaceModel obj)
		{
			obj = new HDeformableSurfaceModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDeformableSurfaceModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HDeformableSurfaceModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HDeformableSurfaceModel(hTuple[i].IP);
			}
			return err;
		}

		public HDeformableSurfaceModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1024);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1031);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public HDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1031);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeDeformableSurfaceModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeDeformableSurfaceModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeDeformableSurfaceModel().Serialize(stream);
		}

		public static HDeformableSurfaceModel Deserialize(Stream stream)
		{
			HDeformableSurfaceModel hDeformableSurfaceModel = new HDeformableSurfaceModel();
			hDeformableSurfaceModel.DeserializeDeformableSurfaceModel(HSerializedItem.Deserialize(stream));
			return hDeformableSurfaceModel;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HDeformableSurfaceModel Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeDeformableSurfaceModel();
			HDeformableSurfaceModel hDeformableSurfaceModel = new HDeformableSurfaceModel();
			hDeformableSurfaceModel.DeserializeDeformableSurfaceModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDeformableSurfaceModel;
		}

		public void DeserializeDeformableSurfaceModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1022);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeDeformableSurfaceModel()
		{
			IntPtr proc = HalconAPI.PreCall(1023);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadDeformableSurfaceModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1024);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteDeformableSurfaceModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1025);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple RefineDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HObjectModel3D initialDeformationObjectModel3D, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1026);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, initialDeformationObjectModel3D);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			GC.KeepAlive(initialDeformationObjectModel3D);
			return result;
		}

		public double RefineDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HObjectModel3D initialDeformationObjectModel3D, string genParamName, string genParamValue, out HDeformableSurfaceMatchingResult deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1026);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, initialDeformationObjectModel3D);
			HalconAPI.StoreS(proc, 4, genParamName);
			HalconAPI.StoreS(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			GC.KeepAlive(initialDeformationObjectModel3D);
			return result;
		}

		public HTuple FindDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HTuple minScore, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1027);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public double FindDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, double minScore, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1027);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HTuple GetDeformableSurfaceModelParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1028);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetDeformableSurfaceModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1028);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple AddDeformableSurfaceModelReferencePoint(HTuple referencePointX, HTuple referencePointY, HTuple referencePointZ)
		{
			IntPtr proc = HalconAPI.PreCall(1029);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, referencePointX);
			HalconAPI.Store(proc, 2, referencePointY);
			HalconAPI.Store(proc, 3, referencePointZ);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(referencePointX);
			HalconAPI.UnpinTuple(referencePointY);
			HalconAPI.UnpinTuple(referencePointZ);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int AddDeformableSurfaceModelReferencePoint(double referencePointX, double referencePointY, double referencePointZ)
		{
			IntPtr proc = HalconAPI.PreCall(1029);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, referencePointX);
			HalconAPI.StoreD(proc, 2, referencePointY);
			HalconAPI.StoreD(proc, 3, referencePointZ);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AddDeformableSurfaceModelSample(HObjectModel3D[] objectModel3D)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1030);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public void AddDeformableSurfaceModelSample(HObjectModel3D objectModel3D)
		{
			IntPtr proc = HalconAPI.PreCall(1030);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public void CreateDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1031);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public void CreateDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, string genParamName, string genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1031);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1021);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
