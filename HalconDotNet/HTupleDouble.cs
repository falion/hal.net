using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace VisionConfig
{
	internal class HTupleDouble : HTupleImplementation
	{
		protected double[] d;

		public override double[] DArr
		{
			get
			{
				return this.d;
			}
			set
			{
				base.SetArray(value, false);
			}
		}

		public override HTupleType Type
		{
			get
			{
				return HTupleType.DOUBLE;
			}
		}

		protected override Array CreateArray(int size)
		{
			return new double[size];
		}

		protected override void NotifyArrayUpdate()
		{
			this.d = (double[])base.data;
		}

		public HTupleDouble(double d)
		{
			base.SetArray(new double[1]
			{
				d
			}, false);
		}

		public HTupleDouble(double[] d, bool copy)
		{
			base.SetArray(d, copy);
		}

		public HTupleDouble(float[] f)
		{
			base.SetArray(f, true);
		}

		internal override void PinTuple()
		{
			Monitor.Enter(this);
			if (base.pinCount == 0)
			{
				base.pinHandle = GCHandle.Alloc(this.d, GCHandleType.Pinned);
			}
			base.pinCount++;
			Monitor.Exit(this);
		}

		public override HTupleElements GetElement(int index, HTuple parent)
		{
			return new HTupleElements(parent, this, index);
		}

		public override HTupleElements GetElements(int[] indices, HTuple parent)
		{
			if (indices != null && indices.Length != 0)
			{
				return new HTupleElements(parent, this, indices);
			}
			return new HTupleElements();
		}

		public override void SetElements(int[] indices, HTupleElements elements)
		{
			if (indices == null)
			{
				return;
			}
			if (indices.Length == 0)
			{
				return;
			}
			double[] dArr = elements.DArr;
			if (dArr.Length == indices.Length)
			{
				for (int i = 0; i < indices.Length; i++)
				{
					this.d[indices[i]] = dArr[i];
				}
				return;
			}
			if (dArr.Length == 1)
			{
				for (int j = 0; j < indices.Length; j++)
				{
					this.d[indices[j]] = dArr[0];
				}
				return;
			}
			throw new HTupleAccessException(this, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
		}

		public override double[] ToDArr()
		{
			return (double[])base.ToArray(base.typeD);
		}

		public override float[] ToFArr()
		{
			float[] array = new float[base.iLength];
			for (int i = 0; i < base.iLength; i++)
			{
				array[i] = (float)this.d[i];
			}
			return array;
		}

		public override void Store(IntPtr proc, int parIndex)
		{
			IntPtr tuple = default(IntPtr);
			HalconAPI.HCkP(proc, HalconAPI.GetInputTuple(proc, parIndex, out tuple));
			this.StoreData(proc, tuple);
		}

		protected override void StoreData(IntPtr proc, IntPtr tuple)
		{
			this.PinTuple();
			HalconAPI.SetDArrPtr(tuple, this.d, base.iLength);
		}

		public static int Load(IntPtr tuple, out HTupleDouble data)
		{
			int num = 0;
			HalconAPI.GetTupleLength(tuple, out num);
			double[] doubleArray = new double[num];
			int dArr = HalconAPI.GetDArr(tuple, doubleArray);
			data = new HTupleDouble(doubleArray, false);
			return dArr;
		}
	}
}
