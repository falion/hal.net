namespace VisionConfig
{
	internal class HTupleElementsInt32 : HTupleElementsImplementation
	{
		internal HTupleElementsInt32(HTupleInt32 source, int index)
			: base(source, index)
		{
		}

		internal HTupleElementsInt32(HTupleInt32 source, int[] indices)
			: base(source, indices)
		{
		}

		public override int[] getI()
		{
			if (base.indices == null)
			{
				return null;
			}
			int[] array = new int[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = base.source.IArr[base.indices[i]];
			}
			return array;
		}

		public override void setI(int[] i)
		{
			if (i != null)
			{
				if (i.Length > 1)
				{
					if (i.Length == base.indices.Length)
					{
						for (int j = 0; j < i.Length; j++)
						{
							base.source.IArr[base.indices[j]] = i[j];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int k = 0; k < i.Length; k++)
				{
					base.source.IArr[base.indices[k]] = i[0];
				}
			}
		}

		public override long[] getL()
		{
			if (base.indices == null)
			{
				return null;
			}
			long[] array = new long[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = base.source.IArr[base.indices[i]];
			}
			return array;
		}

		public override void setL(long[] l)
		{
			if (l != null)
			{
				if (l.Length > 1)
				{
					if (l.Length == base.indices.Length)
					{
						for (int i = 0; i < l.Length; i++)
						{
							base.source.IArr[base.indices[i]] = (int)l[i];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int j = 0; j < l.Length; j++)
				{
					base.source.IArr[base.indices[j]] = (int)l[0];
				}
			}
		}

		public override double[] getD()
		{
			if (base.indices == null)
			{
				return null;
			}
			double[] array = new double[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = (double)base.source.IArr[base.indices[i]];
			}
			return array;
		}

		public override object[] getO()
		{
			if (base.indices == null)
			{
				return null;
			}
			object[] array = new object[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = base.source.IArr[base.indices[i]];
			}
			return array;
		}

		public override HTupleType getType()
		{
			return HTupleType.INTEGER;
		}
	}
}
