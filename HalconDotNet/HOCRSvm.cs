using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HOCRSvm : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRSvm()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRSvm(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRSvm obj)
		{
			obj = new HOCRSvm(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRSvm[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HOCRSvm[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HOCRSvm(hTuple[i].IP);
			}
			return err;
		}

		public HOCRSvm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(676);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCRSvm(int widthCharacter, int heightCharacter, string interpolation, HTuple features, HTuple characters, string kernelType, double kernelParam, double nu, string mode, string preprocessing, int numComponents)
		{
			IntPtr proc = HalconAPI.PreCall(689);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.Store(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.StoreS(proc, 5, kernelType);
			HalconAPI.StoreD(proc, 6, kernelParam);
			HalconAPI.StoreD(proc, 7, nu);
			HalconAPI.StoreS(proc, 8, mode);
			HalconAPI.StoreS(proc, 9, preprocessing);
			HalconAPI.StoreI(proc, 10, numComponents);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(characters);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCRSvm(int widthCharacter, int heightCharacter, string interpolation, string features, HTuple characters, string kernelType, double kernelParam, double nu, string mode, string preprocessing, int numComponents)
		{
			IntPtr proc = HalconAPI.PreCall(689);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.StoreS(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.StoreS(proc, 5, kernelType);
			HalconAPI.StoreD(proc, 6, kernelParam);
			HalconAPI.StoreD(proc, 7, nu);
			HalconAPI.StoreS(proc, 8, mode);
			HalconAPI.StoreS(proc, 9, preprocessing);
			HalconAPI.StoreI(proc, 10, numComponents);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(characters);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeOcrClassSvm();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRSvm(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeOcrClassSvm(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeOcrClassSvm().Serialize(stream);
		}

		public static HOCRSvm Deserialize(Stream stream)
		{
			HOCRSvm hOCRSvm = new HOCRSvm();
			hOCRSvm.DeserializeOcrClassSvm(HSerializedItem.Deserialize(stream));
			return hOCRSvm;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HOCRSvm Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeOcrClassSvm();
			HOCRSvm hOCRSvm = new HOCRSvm();
			hOCRSvm.DeserializeOcrClassSvm(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRSvm;
		}

		public HTuple SelectFeatureSetTrainfSvmProtected(HTuple trainingFile, HTuple password, HTuple featureList, string selectionMethod, int width, int height, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(663);
			HalconAPI.Store(proc, 0, trainingFile);
			HalconAPI.Store(proc, 1, password);
			HalconAPI.Store(proc, 2, featureList);
			HalconAPI.StoreS(proc, 3, selectionMethod);
			HalconAPI.StoreI(proc, 4, width);
			HalconAPI.StoreI(proc, 5, height);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(password);
			HalconAPI.UnpinTuple(featureList);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple SelectFeatureSetTrainfSvmProtected(string trainingFile, string password, string featureList, string selectionMethod, int width, int height, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(663);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.StoreS(proc, 1, password);
			HalconAPI.StoreS(proc, 2, featureList);
			HalconAPI.StoreS(proc, 3, selectionMethod);
			HalconAPI.StoreI(proc, 4, width);
			HalconAPI.StoreI(proc, 5, height);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple SelectFeatureSetTrainfSvm(HTuple trainingFile, HTuple featureList, string selectionMethod, int width, int height, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(664);
			HalconAPI.Store(proc, 0, trainingFile);
			HalconAPI.Store(proc, 1, featureList);
			HalconAPI.StoreS(proc, 2, selectionMethod);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(featureList);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple SelectFeatureSetTrainfSvm(string trainingFile, string featureList, string selectionMethod, int width, int height, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(664);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.StoreS(proc, 1, featureList);
			HalconAPI.StoreS(proc, 2, selectionMethod);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializeOcrClassSvm(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(674);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeOcrClassSvm()
		{
			IntPtr proc = HalconAPI.PreCall(675);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadOcrClassSvm(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(676);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteOcrClassSvm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(677);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetFeaturesOcrClassSvm(HImage character, string transform)
		{
			IntPtr proc = HalconAPI.PreCall(678);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.StoreS(proc, 1, transform);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			return result;
		}

		public HTuple DoOcrWordSvm(HRegion character, HImage image, string expression, int numAlternatives, int numCorrections, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(679);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadS(proc, 1, err, out word);
			err = HalconAPI.LoadD(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public HTuple DoOcrMultiClassSvm(HRegion character, HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(680);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public HTuple DoOcrSingleClassSvm(HRegion character, HImage image, HTuple num)
		{
			IntPtr proc = HalconAPI.PreCall(681);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public HOCRSvm ReduceOcrClassSvm(string method, int minRemainingSV, double maxError)
		{
			IntPtr proc = HalconAPI.PreCall(682);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.StoreI(proc, 2, minRemainingSV);
			HalconAPI.StoreD(proc, 3, maxError);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HOCRSvm result = null;
			err = HOCRSvm.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void TrainfOcrClassSvmProtected(HTuple trainingFile, HTuple password, double epsilon, HTuple trainMode)
		{
			IntPtr proc = HalconAPI.PreCall(683);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, trainingFile);
			HalconAPI.Store(proc, 2, password);
			HalconAPI.StoreD(proc, 3, epsilon);
			HalconAPI.Store(proc, 4, trainMode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(password);
			HalconAPI.UnpinTuple(trainMode);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TrainfOcrClassSvmProtected(string trainingFile, string password, double epsilon, string trainMode)
		{
			IntPtr proc = HalconAPI.PreCall(683);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, trainingFile);
			HalconAPI.StoreS(proc, 2, password);
			HalconAPI.StoreD(proc, 3, epsilon);
			HalconAPI.StoreS(proc, 4, trainMode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TrainfOcrClassSvm(HTuple trainingFile, double epsilon, HTuple trainMode)
		{
			IntPtr proc = HalconAPI.PreCall(684);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, trainingFile);
			HalconAPI.StoreD(proc, 2, epsilon);
			HalconAPI.Store(proc, 3, trainMode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(trainMode);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TrainfOcrClassSvm(string trainingFile, double epsilon, string trainMode)
		{
			IntPtr proc = HalconAPI.PreCall(684);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, trainingFile);
			HalconAPI.StoreD(proc, 2, epsilon);
			HalconAPI.StoreS(proc, 3, trainMode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetPrepInfoOcrClassSvm(HTuple trainingFile, string preprocessing, out HTuple cumInformationCont)
		{
			IntPtr proc = HalconAPI.PreCall(685);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, trainingFile);
			HalconAPI.StoreS(proc, 2, preprocessing);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cumInformationCont);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetPrepInfoOcrClassSvm(string trainingFile, string preprocessing, out HTuple cumInformationCont)
		{
			IntPtr proc = HalconAPI.PreCall(685);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, trainingFile);
			HalconAPI.StoreS(proc, 2, preprocessing);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cumInformationCont);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetSupportVectorNumOcrClassSvm(out HTuple numSVPerSVM)
		{
			IntPtr proc = HalconAPI.PreCall(686);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out numSVPerSVM);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double GetSupportVectorOcrClassSvm(HTuple indexSupportVector)
		{
			IntPtr proc = HalconAPI.PreCall(687);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, indexSupportVector);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(indexSupportVector);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetParamsOcrClassSvm(out int widthCharacter, out int heightCharacter, out string interpolation, out HTuple features, out HTuple characters, out string kernelType, out double kernelParam, out double nu, out string mode, out string preprocessing, out int numComponents)
		{
			IntPtr proc = HalconAPI.PreCall(688);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			HalconAPI.InitOCT(proc, 10);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out widthCharacter);
			err = HalconAPI.LoadI(proc, 1, err, out heightCharacter);
			err = HalconAPI.LoadS(proc, 2, err, out interpolation);
			err = HTuple.LoadNew(proc, 3, err, out features);
			err = HTuple.LoadNew(proc, 4, err, out characters);
			err = HalconAPI.LoadS(proc, 5, err, out kernelType);
			err = HalconAPI.LoadD(proc, 6, err, out kernelParam);
			err = HalconAPI.LoadD(proc, 7, err, out nu);
			err = HalconAPI.LoadS(proc, 8, err, out mode);
			err = HalconAPI.LoadS(proc, 9, err, out preprocessing);
			err = HalconAPI.LoadI(proc, 10, err, out numComponents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetParamsOcrClassSvm(out int widthCharacter, out int heightCharacter, out string interpolation, out string features, out HTuple characters, out string kernelType, out double kernelParam, out double nu, out string mode, out string preprocessing, out int numComponents)
		{
			IntPtr proc = HalconAPI.PreCall(688);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			HalconAPI.InitOCT(proc, 10);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out widthCharacter);
			err = HalconAPI.LoadI(proc, 1, err, out heightCharacter);
			err = HalconAPI.LoadS(proc, 2, err, out interpolation);
			err = HalconAPI.LoadS(proc, 3, err, out features);
			err = HTuple.LoadNew(proc, 4, err, out characters);
			err = HalconAPI.LoadS(proc, 5, err, out kernelType);
			err = HalconAPI.LoadD(proc, 6, err, out kernelParam);
			err = HalconAPI.LoadD(proc, 7, err, out nu);
			err = HalconAPI.LoadS(proc, 8, err, out mode);
			err = HalconAPI.LoadS(proc, 9, err, out preprocessing);
			err = HalconAPI.LoadI(proc, 10, err, out numComponents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcrClassSvm(int widthCharacter, int heightCharacter, string interpolation, HTuple features, HTuple characters, string kernelType, double kernelParam, double nu, string mode, string preprocessing, int numComponents)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(689);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.Store(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.StoreS(proc, 5, kernelType);
			HalconAPI.StoreD(proc, 6, kernelParam);
			HalconAPI.StoreD(proc, 7, nu);
			HalconAPI.StoreS(proc, 8, mode);
			HalconAPI.StoreS(proc, 9, preprocessing);
			HalconAPI.StoreI(proc, 10, numComponents);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(characters);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcrClassSvm(int widthCharacter, int heightCharacter, string interpolation, string features, HTuple characters, string kernelType, double kernelParam, double nu, string mode, string preprocessing, int numComponents)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(689);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.StoreS(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.StoreS(proc, 5, kernelType);
			HalconAPI.StoreD(proc, 6, kernelParam);
			HalconAPI.StoreD(proc, 7, nu);
			HalconAPI.StoreS(proc, 8, mode);
			HalconAPI.StoreS(proc, 9, preprocessing);
			HalconAPI.StoreI(proc, 10, numComponents);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(characters);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(673);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
