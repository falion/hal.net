using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HCameraSetupModel : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCameraSetupModel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCameraSetupModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCameraSetupModel obj)
		{
			obj = new HCameraSetupModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCameraSetupModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HCameraSetupModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HCameraSetupModel(hTuple[i].IP);
			}
			return err;
		}

		public HCameraSetupModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1954);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HCameraSetupModel(int numCameras)
		{
			IntPtr proc = HalconAPI.PreCall(1958);
			HalconAPI.StoreI(proc, 0, numCameras);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeCameraSetupModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCameraSetupModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeCameraSetupModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeCameraSetupModel().Serialize(stream);
		}

		public static HCameraSetupModel Deserialize(Stream stream)
		{
			HCameraSetupModel hCameraSetupModel = new HCameraSetupModel();
			hCameraSetupModel.DeserializeCameraSetupModel(HSerializedItem.Deserialize(stream));
			return hCameraSetupModel;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HCameraSetupModel Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeCameraSetupModel();
			HCameraSetupModel hCameraSetupModel = new HCameraSetupModel();
			hCameraSetupModel.DeserializeCameraSetupModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hCameraSetupModel;
		}

		public HStereoModel CreateStereoModel(string method, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(527);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HStereoModel result = null;
			err = HStereoModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HStereoModel CreateStereoModel(string method, string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(527);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HStereoModel result = null;
			err = HStereoModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HSerializedItem SerializeCameraSetupModel()
		{
			IntPtr proc = HalconAPI.PreCall(1951);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializeCameraSetupModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1952);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public void WriteCameraSetupModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1953);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadCameraSetupModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1954);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetCameraSetupParam(HTuple cameraIdx, string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1955);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetCameraSetupParam(int cameraIdx, string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1955);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetCameraSetupParam(HTuple cameraIdx, string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1956);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCameraSetupParam(int cameraIdx, string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1956);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCameraSetupCamParam(HTuple cameraIdx, HTuple cameraType, HCamPar cameraParam, HTuple cameraPose)
		{
			IntPtr proc = HalconAPI.PreCall(1957);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.Store(proc, 2, cameraType);
			HalconAPI.Store(proc, 3, cameraParam);
			HalconAPI.Store(proc, 4, cameraPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraType);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(cameraPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCameraSetupCamParam(HTuple cameraIdx, string cameraType, HCamPar cameraParam, HTuple cameraPose)
		{
			IntPtr proc = HalconAPI.PreCall(1957);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, cameraType);
			HalconAPI.Store(proc, 3, cameraParam);
			HalconAPI.Store(proc, 4, cameraPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(cameraPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateCameraSetupModel(int numCameras)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1958);
			HalconAPI.StoreI(proc, 0, numCameras);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1950);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
