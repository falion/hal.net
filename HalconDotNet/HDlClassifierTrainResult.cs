using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HDlClassifierTrainResult : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifierTrainResult()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifierTrainResult(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlClassifierTrainResult obj)
		{
			obj = new HDlClassifierTrainResult(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlClassifierTrainResult[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HDlClassifierTrainResult[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HDlClassifierTrainResult(hTuple[i].IP);
			}
			return err;
		}

		public HDlClassifierTrainResult(HImage batchImages, HDlClassifier DLClassifierHandle, HTuple batchLabels)
		{
			IntPtr proc = HalconAPI.PreCall(2131);
			HalconAPI.Store(proc, 1, batchImages);
			HalconAPI.Store(proc, 0, DLClassifierHandle);
			HalconAPI.Store(proc, 1, batchLabels);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(batchLabels);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(batchImages);
			GC.KeepAlive(DLClassifierHandle);
		}

		public HTuple GetDlClassifierTrainResult(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2116);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetDlClassifierTrainResult(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2116);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void TrainDlClassifierBatch(HImage batchImages, HDlClassifier DLClassifierHandle, HTuple batchLabels)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2131);
			HalconAPI.Store(proc, 1, batchImages);
			HalconAPI.Store(proc, 0, DLClassifierHandle);
			HalconAPI.Store(proc, 1, batchLabels);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(batchLabels);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(batchImages);
			GC.KeepAlive(DLClassifierHandle);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(2105);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
