using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HComponentModel : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComponentModel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComponentModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HComponentModel obj)
		{
			obj = new HComponentModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HComponentModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HComponentModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HComponentModel(hTuple[i].IP);
			}
			return err;
		}

		public HComponentModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1002);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HComponentModel(HImage modelImage, HRegion componentRegions, HTuple variationRow, HTuple variationColumn, HTuple variationAngle, double angleStart, double angleExtent, HTuple contrastLowComp, HTuple contrastHighComp, HTuple minSizeComp, HTuple minContrastComp, HTuple minScoreComp, HTuple numLevelsComp, HTuple angleStepComp, string optimizationComp, HTuple metricComp, HTuple pregenerationComp, out HTuple rootRanking)
		{
			IntPtr proc = HalconAPI.PreCall(1004);
			HalconAPI.Store(proc, 1, modelImage);
			HalconAPI.Store(proc, 2, componentRegions);
			HalconAPI.Store(proc, 0, variationRow);
			HalconAPI.Store(proc, 1, variationColumn);
			HalconAPI.Store(proc, 2, variationAngle);
			HalconAPI.StoreD(proc, 3, angleStart);
			HalconAPI.StoreD(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, contrastLowComp);
			HalconAPI.Store(proc, 6, contrastHighComp);
			HalconAPI.Store(proc, 7, minSizeComp);
			HalconAPI.Store(proc, 8, minContrastComp);
			HalconAPI.Store(proc, 9, minScoreComp);
			HalconAPI.Store(proc, 10, numLevelsComp);
			HalconAPI.Store(proc, 11, angleStepComp);
			HalconAPI.StoreS(proc, 12, optimizationComp);
			HalconAPI.Store(proc, 13, metricComp);
			HalconAPI.Store(proc, 14, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(variationRow);
			HalconAPI.UnpinTuple(variationColumn);
			HalconAPI.UnpinTuple(variationAngle);
			HalconAPI.UnpinTuple(contrastLowComp);
			HalconAPI.UnpinTuple(contrastHighComp);
			HalconAPI.UnpinTuple(minSizeComp);
			HalconAPI.UnpinTuple(minContrastComp);
			HalconAPI.UnpinTuple(minScoreComp);
			HalconAPI.UnpinTuple(numLevelsComp);
			HalconAPI.UnpinTuple(angleStepComp);
			HalconAPI.UnpinTuple(metricComp);
			HalconAPI.UnpinTuple(pregenerationComp);
			err = base.Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out rootRanking);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelImage);
			GC.KeepAlive(componentRegions);
		}

		public HComponentModel(HImage modelImage, HRegion componentRegions, int variationRow, int variationColumn, double variationAngle, double angleStart, double angleExtent, int contrastLowComp, int contrastHighComp, int minSizeComp, int minContrastComp, double minScoreComp, int numLevelsComp, double angleStepComp, string optimizationComp, string metricComp, string pregenerationComp, out int rootRanking)
		{
			IntPtr proc = HalconAPI.PreCall(1004);
			HalconAPI.Store(proc, 1, modelImage);
			HalconAPI.Store(proc, 2, componentRegions);
			HalconAPI.StoreI(proc, 0, variationRow);
			HalconAPI.StoreI(proc, 1, variationColumn);
			HalconAPI.StoreD(proc, 2, variationAngle);
			HalconAPI.StoreD(proc, 3, angleStart);
			HalconAPI.StoreD(proc, 4, angleExtent);
			HalconAPI.StoreI(proc, 5, contrastLowComp);
			HalconAPI.StoreI(proc, 6, contrastHighComp);
			HalconAPI.StoreI(proc, 7, minSizeComp);
			HalconAPI.StoreI(proc, 8, minContrastComp);
			HalconAPI.StoreD(proc, 9, minScoreComp);
			HalconAPI.StoreI(proc, 10, numLevelsComp);
			HalconAPI.StoreD(proc, 11, angleStepComp);
			HalconAPI.StoreS(proc, 12, optimizationComp);
			HalconAPI.StoreS(proc, 13, metricComp);
			HalconAPI.StoreS(proc, 14, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			err = HalconAPI.LoadI(proc, 1, err, out rootRanking);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelImage);
			GC.KeepAlive(componentRegions);
		}

		public HComponentModel(HComponentTraining componentTrainingID, double angleStart, double angleExtent, HTuple minContrastComp, HTuple minScoreComp, HTuple numLevelsComp, HTuple angleStepComp, string optimizationComp, HTuple metricComp, HTuple pregenerationComp, out HTuple rootRanking)
		{
			IntPtr proc = HalconAPI.PreCall(1005);
			HalconAPI.Store(proc, 0, componentTrainingID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, minContrastComp);
			HalconAPI.Store(proc, 4, minScoreComp);
			HalconAPI.Store(proc, 5, numLevelsComp);
			HalconAPI.Store(proc, 6, angleStepComp);
			HalconAPI.StoreS(proc, 7, optimizationComp);
			HalconAPI.Store(proc, 8, metricComp);
			HalconAPI.Store(proc, 9, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minContrastComp);
			HalconAPI.UnpinTuple(minScoreComp);
			HalconAPI.UnpinTuple(numLevelsComp);
			HalconAPI.UnpinTuple(angleStepComp);
			HalconAPI.UnpinTuple(metricComp);
			HalconAPI.UnpinTuple(pregenerationComp);
			err = base.Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out rootRanking);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(componentTrainingID);
		}

		public HComponentModel(HComponentTraining componentTrainingID, double angleStart, double angleExtent, int minContrastComp, double minScoreComp, int numLevelsComp, double angleStepComp, string optimizationComp, string metricComp, string pregenerationComp, out int rootRanking)
		{
			IntPtr proc = HalconAPI.PreCall(1005);
			HalconAPI.Store(proc, 0, componentTrainingID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreI(proc, 3, minContrastComp);
			HalconAPI.StoreD(proc, 4, minScoreComp);
			HalconAPI.StoreI(proc, 5, numLevelsComp);
			HalconAPI.StoreD(proc, 6, angleStepComp);
			HalconAPI.StoreS(proc, 7, optimizationComp);
			HalconAPI.StoreS(proc, 8, metricComp);
			HalconAPI.StoreS(proc, 9, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			err = HalconAPI.LoadI(proc, 1, err, out rootRanking);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(componentTrainingID);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeComponentModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComponentModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeComponentModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeComponentModel().Serialize(stream);
		}

		public static HComponentModel Deserialize(Stream stream)
		{
			HComponentModel hComponentModel = new HComponentModel();
			hComponentModel.DeserializeComponentModel(HSerializedItem.Deserialize(stream));
			return hComponentModel;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HComponentModel Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeComponentModel();
			HComponentModel hComponentModel = new HComponentModel();
			hComponentModel.DeserializeComponentModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hComponentModel;
		}

		public HRegion GetFoundComponentModel(HTuple modelStart, HTuple modelEnd, HTuple rowComp, HTuple columnComp, HTuple angleComp, HTuple scoreComp, HTuple modelComp, int modelMatch, string markOrientation, out HTuple rowCompInst, out HTuple columnCompInst, out HTuple angleCompInst, out HTuple scoreCompInst)
		{
			IntPtr proc = HalconAPI.PreCall(994);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, modelStart);
			HalconAPI.Store(proc, 2, modelEnd);
			HalconAPI.Store(proc, 3, rowComp);
			HalconAPI.Store(proc, 4, columnComp);
			HalconAPI.Store(proc, 5, angleComp);
			HalconAPI.Store(proc, 6, scoreComp);
			HalconAPI.Store(proc, 7, modelComp);
			HalconAPI.StoreI(proc, 8, modelMatch);
			HalconAPI.StoreS(proc, 9, markOrientation);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(modelStart);
			HalconAPI.UnpinTuple(modelEnd);
			HalconAPI.UnpinTuple(rowComp);
			HalconAPI.UnpinTuple(columnComp);
			HalconAPI.UnpinTuple(angleComp);
			HalconAPI.UnpinTuple(scoreComp);
			HalconAPI.UnpinTuple(modelComp);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowCompInst);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnCompInst);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angleCompInst);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scoreCompInst);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion GetFoundComponentModel(int modelStart, int modelEnd, double rowComp, double columnComp, double angleComp, double scoreComp, int modelComp, int modelMatch, string markOrientation, out double rowCompInst, out double columnCompInst, out double angleCompInst, out double scoreCompInst)
		{
			IntPtr proc = HalconAPI.PreCall(994);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, modelStart);
			HalconAPI.StoreI(proc, 2, modelEnd);
			HalconAPI.StoreD(proc, 3, rowComp);
			HalconAPI.StoreD(proc, 4, columnComp);
			HalconAPI.StoreD(proc, 5, angleComp);
			HalconAPI.StoreD(proc, 6, scoreComp);
			HalconAPI.StoreI(proc, 7, modelComp);
			HalconAPI.StoreI(proc, 8, modelMatch);
			HalconAPI.StoreS(proc, 9, markOrientation);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HalconAPI.LoadD(proc, 0, err, out rowCompInst);
			err = HalconAPI.LoadD(proc, 1, err, out columnCompInst);
			err = HalconAPI.LoadD(proc, 2, err, out angleCompInst);
			err = HalconAPI.LoadD(proc, 3, err, out scoreCompInst);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple FindComponentModel(HImage image, HTuple rootComponent, HTuple angleStartRoot, HTuple angleExtentRoot, double minScore, int numMatches, double maxOverlap, string ifRootNotFound, string ifComponentNotFound, string posePrediction, HTuple minScoreComp, HTuple subPixelComp, HTuple numLevelsComp, HTuple greedinessComp, out HTuple modelEnd, out HTuple score, out HTuple rowComp, out HTuple columnComp, out HTuple angleComp, out HTuple scoreComp, out HTuple modelComp)
		{
			IntPtr proc = HalconAPI.PreCall(995);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, rootComponent);
			HalconAPI.Store(proc, 2, angleStartRoot);
			HalconAPI.Store(proc, 3, angleExtentRoot);
			HalconAPI.StoreD(proc, 4, minScore);
			HalconAPI.StoreI(proc, 5, numMatches);
			HalconAPI.StoreD(proc, 6, maxOverlap);
			HalconAPI.StoreS(proc, 7, ifRootNotFound);
			HalconAPI.StoreS(proc, 8, ifComponentNotFound);
			HalconAPI.StoreS(proc, 9, posePrediction);
			HalconAPI.Store(proc, 10, minScoreComp);
			HalconAPI.Store(proc, 11, subPixelComp);
			HalconAPI.Store(proc, 12, numLevelsComp);
			HalconAPI.Store(proc, 13, greedinessComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rootComponent);
			HalconAPI.UnpinTuple(angleStartRoot);
			HalconAPI.UnpinTuple(angleExtentRoot);
			HalconAPI.UnpinTuple(minScoreComp);
			HalconAPI.UnpinTuple(subPixelComp);
			HalconAPI.UnpinTuple(numLevelsComp);
			HalconAPI.UnpinTuple(greedinessComp);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out modelEnd);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rowComp);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out columnComp);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out angleComp);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out scoreComp);
			err = HTuple.LoadNew(proc, 7, HTupleType.INTEGER, err, out modelComp);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public int FindComponentModel(HImage image, int rootComponent, double angleStartRoot, double angleExtentRoot, double minScore, int numMatches, double maxOverlap, string ifRootNotFound, string ifComponentNotFound, string posePrediction, double minScoreComp, string subPixelComp, int numLevelsComp, double greedinessComp, out int modelEnd, out double score, out double rowComp, out double columnComp, out double angleComp, out double scoreComp, out int modelComp)
		{
			IntPtr proc = HalconAPI.PreCall(995);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 1, rootComponent);
			HalconAPI.StoreD(proc, 2, angleStartRoot);
			HalconAPI.StoreD(proc, 3, angleExtentRoot);
			HalconAPI.StoreD(proc, 4, minScore);
			HalconAPI.StoreI(proc, 5, numMatches);
			HalconAPI.StoreD(proc, 6, maxOverlap);
			HalconAPI.StoreS(proc, 7, ifRootNotFound);
			HalconAPI.StoreS(proc, 8, ifComponentNotFound);
			HalconAPI.StoreS(proc, 9, posePrediction);
			HalconAPI.StoreD(proc, 10, minScoreComp);
			HalconAPI.StoreS(proc, 11, subPixelComp);
			HalconAPI.StoreI(proc, 12, numLevelsComp);
			HalconAPI.StoreD(proc, 13, greedinessComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out modelEnd);
			err = HalconAPI.LoadD(proc, 2, err, out score);
			err = HalconAPI.LoadD(proc, 3, err, out rowComp);
			err = HalconAPI.LoadD(proc, 4, err, out columnComp);
			err = HalconAPI.LoadD(proc, 5, err, out angleComp);
			err = HalconAPI.LoadD(proc, 6, err, out scoreComp);
			err = HalconAPI.LoadI(proc, 7, err, out modelComp);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HRegion GetComponentModelTree(out HRegion relations, HTuple rootComponent, HTuple image, out HTuple startNode, out HTuple endNode, out HTuple row, out HTuple column, out HTuple phi, out HTuple length1, out HTuple length2, out HTuple angleStart, out HTuple angleExtent)
		{
			IntPtr proc = HalconAPI.PreCall(998);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, rootComponent);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rootComponent);
			HalconAPI.UnpinTuple(image);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HRegion.LoadNew(proc, 2, err, out relations);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out startNode);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out endNode);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out phi);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out length1);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out length2);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out angleStart);
			err = HTuple.LoadNew(proc, 8, HTupleType.DOUBLE, err, out angleExtent);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion GetComponentModelTree(out HRegion relations, int rootComponent, string image, out int startNode, out int endNode, out double row, out double column, out double phi, out double length1, out double length2, out double angleStart, out double angleExtent)
		{
			IntPtr proc = HalconAPI.PreCall(998);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, rootComponent);
			HalconAPI.StoreS(proc, 2, image);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HRegion.LoadNew(proc, 2, err, out relations);
			err = HalconAPI.LoadI(proc, 0, err, out startNode);
			err = HalconAPI.LoadI(proc, 1, err, out endNode);
			err = HalconAPI.LoadD(proc, 2, err, out row);
			err = HalconAPI.LoadD(proc, 3, err, out column);
			err = HalconAPI.LoadD(proc, 4, err, out phi);
			err = HalconAPI.LoadD(proc, 5, err, out length1);
			err = HalconAPI.LoadD(proc, 6, err, out length2);
			err = HalconAPI.LoadD(proc, 7, err, out angleStart);
			err = HalconAPI.LoadD(proc, 8, err, out angleExtent);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetComponentModelParams(out HTuple rootRanking, out HShapeModel[] shapeModelIDs)
		{
			IntPtr proc = HalconAPI.PreCall(999);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out rootRanking);
			err = HShapeModel.LoadNew(proc, 2, err, out shapeModelIDs);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double GetComponentModelParams(out int rootRanking, out HShapeModel shapeModelIDs)
		{
			IntPtr proc = HalconAPI.PreCall(999);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out rootRanking);
			err = HShapeModel.LoadNew(proc, 2, err, out shapeModelIDs);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializeComponentModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1000);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeComponentModel()
		{
			IntPtr proc = HalconAPI.PreCall(1001);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadComponentModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1002);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteComponentModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1003);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple CreateComponentModel(HImage modelImage, HRegion componentRegions, HTuple variationRow, HTuple variationColumn, HTuple variationAngle, double angleStart, double angleExtent, HTuple contrastLowComp, HTuple contrastHighComp, HTuple minSizeComp, HTuple minContrastComp, HTuple minScoreComp, HTuple numLevelsComp, HTuple angleStepComp, string optimizationComp, HTuple metricComp, HTuple pregenerationComp)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1004);
			HalconAPI.Store(proc, 1, modelImage);
			HalconAPI.Store(proc, 2, componentRegions);
			HalconAPI.Store(proc, 0, variationRow);
			HalconAPI.Store(proc, 1, variationColumn);
			HalconAPI.Store(proc, 2, variationAngle);
			HalconAPI.StoreD(proc, 3, angleStart);
			HalconAPI.StoreD(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, contrastLowComp);
			HalconAPI.Store(proc, 6, contrastHighComp);
			HalconAPI.Store(proc, 7, minSizeComp);
			HalconAPI.Store(proc, 8, minContrastComp);
			HalconAPI.Store(proc, 9, minScoreComp);
			HalconAPI.Store(proc, 10, numLevelsComp);
			HalconAPI.Store(proc, 11, angleStepComp);
			HalconAPI.StoreS(proc, 12, optimizationComp);
			HalconAPI.Store(proc, 13, metricComp);
			HalconAPI.Store(proc, 14, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(variationRow);
			HalconAPI.UnpinTuple(variationColumn);
			HalconAPI.UnpinTuple(variationAngle);
			HalconAPI.UnpinTuple(contrastLowComp);
			HalconAPI.UnpinTuple(contrastHighComp);
			HalconAPI.UnpinTuple(minSizeComp);
			HalconAPI.UnpinTuple(minContrastComp);
			HalconAPI.UnpinTuple(minScoreComp);
			HalconAPI.UnpinTuple(numLevelsComp);
			HalconAPI.UnpinTuple(angleStepComp);
			HalconAPI.UnpinTuple(metricComp);
			HalconAPI.UnpinTuple(pregenerationComp);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelImage);
			GC.KeepAlive(componentRegions);
			return result;
		}

		public int CreateComponentModel(HImage modelImage, HRegion componentRegions, int variationRow, int variationColumn, double variationAngle, double angleStart, double angleExtent, int contrastLowComp, int contrastHighComp, int minSizeComp, int minContrastComp, double minScoreComp, int numLevelsComp, double angleStepComp, string optimizationComp, string metricComp, string pregenerationComp)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1004);
			HalconAPI.Store(proc, 1, modelImage);
			HalconAPI.Store(proc, 2, componentRegions);
			HalconAPI.StoreI(proc, 0, variationRow);
			HalconAPI.StoreI(proc, 1, variationColumn);
			HalconAPI.StoreD(proc, 2, variationAngle);
			HalconAPI.StoreD(proc, 3, angleStart);
			HalconAPI.StoreD(proc, 4, angleExtent);
			HalconAPI.StoreI(proc, 5, contrastLowComp);
			HalconAPI.StoreI(proc, 6, contrastHighComp);
			HalconAPI.StoreI(proc, 7, minSizeComp);
			HalconAPI.StoreI(proc, 8, minContrastComp);
			HalconAPI.StoreD(proc, 9, minScoreComp);
			HalconAPI.StoreI(proc, 10, numLevelsComp);
			HalconAPI.StoreD(proc, 11, angleStepComp);
			HalconAPI.StoreS(proc, 12, optimizationComp);
			HalconAPI.StoreS(proc, 13, metricComp);
			HalconAPI.StoreS(proc, 14, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			int result = 0;
			err = HalconAPI.LoadI(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelImage);
			GC.KeepAlive(componentRegions);
			return result;
		}

		public HTuple CreateTrainedComponentModel(HComponentTraining componentTrainingID, double angleStart, double angleExtent, HTuple minContrastComp, HTuple minScoreComp, HTuple numLevelsComp, HTuple angleStepComp, string optimizationComp, HTuple metricComp, HTuple pregenerationComp)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1005);
			HalconAPI.Store(proc, 0, componentTrainingID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, minContrastComp);
			HalconAPI.Store(proc, 4, minScoreComp);
			HalconAPI.Store(proc, 5, numLevelsComp);
			HalconAPI.Store(proc, 6, angleStepComp);
			HalconAPI.StoreS(proc, 7, optimizationComp);
			HalconAPI.Store(proc, 8, metricComp);
			HalconAPI.Store(proc, 9, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minContrastComp);
			HalconAPI.UnpinTuple(minScoreComp);
			HalconAPI.UnpinTuple(numLevelsComp);
			HalconAPI.UnpinTuple(angleStepComp);
			HalconAPI.UnpinTuple(metricComp);
			HalconAPI.UnpinTuple(pregenerationComp);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(componentTrainingID);
			return result;
		}

		public int CreateTrainedComponentModel(HComponentTraining componentTrainingID, double angleStart, double angleExtent, int minContrastComp, double minScoreComp, int numLevelsComp, double angleStepComp, string optimizationComp, string metricComp, string pregenerationComp)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1005);
			HalconAPI.Store(proc, 0, componentTrainingID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreI(proc, 3, minContrastComp);
			HalconAPI.StoreD(proc, 4, minScoreComp);
			HalconAPI.StoreI(proc, 5, numLevelsComp);
			HalconAPI.StoreD(proc, 6, angleStepComp);
			HalconAPI.StoreS(proc, 7, optimizationComp);
			HalconAPI.StoreS(proc, 8, metricComp);
			HalconAPI.StoreS(proc, 9, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			int result = 0;
			err = HalconAPI.LoadI(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(componentTrainingID);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(997);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
