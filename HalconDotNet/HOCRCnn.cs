using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HOCRCnn : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRCnn()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRCnn(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRCnn obj)
		{
			obj = new HOCRCnn(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRCnn[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HOCRCnn[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HOCRCnn(hTuple[i].IP);
			}
			return err;
		}

		public HOCRCnn(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(2082);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeOcrClassCnn();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRCnn(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeOcrClassCnn(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeOcrClassCnn().Serialize(stream);
		}

		public static HOCRCnn Deserialize(Stream stream)
		{
			HOCRCnn hOCRCnn = new HOCRCnn();
			hOCRCnn.DeserializeOcrClassCnn(HSerializedItem.Deserialize(stream));
			return hOCRCnn;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HOCRCnn Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeOcrClassCnn();
			HOCRCnn hOCRCnn = new HOCRCnn();
			hOCRCnn.DeserializeOcrClassCnn(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRCnn;
		}

		public void DeserializeOcrClassCnn(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2053);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HTuple DoOcrMultiClassCnn(HRegion character, HImage image, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2056);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public string DoOcrMultiClassCnn(HRegion character, HImage image, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2056);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public HTuple DoOcrSingleClassCnn(HRegion character, HImage image, HTuple num, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2057);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public string DoOcrSingleClassCnn(HRegion character, HImage image, HTuple num, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2057);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public HTuple DoOcrWordCnn(HRegion character, HImage image, string expression, int numAlternatives, int numCorrections, out HTuple confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(2058);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public string DoOcrWordCnn(HRegion character, HImage image, string expression, int numAlternatives, int numCorrections, out double confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(2058);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public HTuple GetParamsOcrClassCnn(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2072);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetParamsOcrClassCnn(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2072);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryParamsOcrClassCnn()
		{
			IntPtr proc = HalconAPI.PreCall(2081);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadOcrClassCnn(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2082);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem SerializeOcrClassCnn()
		{
			IntPtr proc = HalconAPI.PreCall(2093);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(2046);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
