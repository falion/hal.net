using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HImage : HObject, ISerializable, ICloneable
	{
		public new HImage this[HTuple index]
		{
			get
			{
				return this.SelectObj(index);
			}
		}

		public HImage()
			: base(HObjectBase.UNDEF, false)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HImage(IntPtr key)
			: this(key, true)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HImage(IntPtr key, bool copy)
			: base(key, copy)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HImage(HObject obj)
			: base(obj)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		private void AssertObjectClass()
		{
			HalconAPI.AssertObjectClass(base.key, "image");
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, int err, out HImage obj)
		{
			obj = new HImage(HObjectBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		public HImage(string type, int width, int height, IntPtr pixelPointer)
		{
			IntPtr proc = HalconAPI.PreCall(606);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.StoreIP(proc, 3, pixelPointer);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage(string type, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(607);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage(HTuple fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1658);
			HalconAPI.Store(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fileName);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1658);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeImage();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HImage(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeImage(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			this.SerializeImage().Serialize(stream);
		}

		public new static HImage Deserialize(Stream stream)
		{
			HImage hImage = new HImage();
			hImage.DeserializeImage(HSerializedItem.Deserialize(stream));
			return hImage;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public new HImage Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeImage();
			HImage hImage = new HImage();
			hImage.DeserializeImage(hSerializedItem);
			hSerializedItem.Dispose();
			return hImage;
		}

		public static HImage operator -(HImage image)
		{
			return image.InvertImage();
		}

		public static HImage operator +(HImage image1, HImage image2)
		{
			return image1.AddImage(image2, 1.0, 0.0);
		}

		public static HImage operator -(HImage image1, HImage image2)
		{
			return image1.SubImage(image2, 1.0, 0.0);
		}

		public static HImage operator *(HImage image1, HImage image2)
		{
			return image1.MultImage(image2, 1.0, 0.0);
		}

		public static HImage operator +(HImage image, double add)
		{
			return image.ScaleImage(1.0, add);
		}

		public static HImage operator +(double add, HImage image)
		{
			return image.ScaleImage(1.0, add);
		}

		public static HImage operator -(HImage image, double sub)
		{
			return image.ScaleImage(1.0, 0.0 - sub);
		}

		public static HImage operator *(HImage image, double mult)
		{
			return image.ScaleImage(mult, 0.0);
		}

		public static HImage operator *(double mult, HImage image)
		{
			return image.ScaleImage(mult, 0.0);
		}

		public static HImage operator /(HImage image, double div)
		{
			return image.ScaleImage(1.0 / div, 0.0);
		}

		public static HRegion operator >=(HImage image1, HImage image2)
		{
			return image1.DynThreshold(image2, 0.0, "light");
		}

		public static HRegion operator <=(HImage image1, HImage image2)
		{
			return image1.DynThreshold(image2, 0.0, "dark");
		}

		public static HRegion operator >=(HImage image, double threshold)
		{
			return image.Threshold(threshold, 1.7976931348623157E+308);
		}

		public static HRegion operator <=(HImage image, double threshold)
		{
			return image.Threshold(-1.7976931348623157E+308, threshold);
		}

		public static HRegion operator >=(double threshold, HImage image)
		{
			return image.Threshold(-1.7976931348623157E+308, threshold);
		}

		public static HRegion operator <=(double threshold, HImage image)
		{
			return image.Threshold(threshold, 1.7976931348623157E+308);
		}

		public static HImage operator &(HImage image, HRegion region)
		{
			return image.ReduceDomain(region);
		}

		public static implicit operator HRegion(HImage image)
		{
			return image.GetDomain();
		}

		public HImage WienerFilterNi(HImage psf, HRegion noiseRegion, int maskWidth, int maskHeight)
		{
			IntPtr proc = HalconAPI.PreCall(75);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, psf);
			HalconAPI.Store(proc, 3, noiseRegion);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(psf);
			GC.KeepAlive(noiseRegion);
			return result;
		}

		public HImage WienerFilter(HImage psf, HImage filteredImage)
		{
			IntPtr proc = HalconAPI.PreCall(76);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, psf);
			HalconAPI.Store(proc, 3, filteredImage);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(psf);
			GC.KeepAlive(filteredImage);
			return result;
		}

		public void GenPsfMotion(int PSFwidth, int PSFheight, double blurring, int angle, int type)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(77);
			HalconAPI.StoreI(proc, 0, PSFwidth);
			HalconAPI.StoreI(proc, 1, PSFheight);
			HalconAPI.StoreD(proc, 2, blurring);
			HalconAPI.StoreI(proc, 3, angle);
			HalconAPI.StoreI(proc, 4, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage SimulateMotion(double blurring, int angle, int type)
		{
			IntPtr proc = HalconAPI.PreCall(78);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, blurring);
			HalconAPI.StoreI(proc, 1, angle);
			HalconAPI.StoreI(proc, 2, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GenPsfDefocus(int PSFwidth, int PSFheight, double blurring)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(79);
			HalconAPI.StoreI(proc, 0, PSFwidth);
			HalconAPI.StoreI(proc, 1, PSFheight);
			HalconAPI.StoreD(proc, 2, blurring);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage SimulateDefocus(double blurring)
		{
			IntPtr proc = HalconAPI.PreCall(80);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, blurring);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion CompareExtVariationModel(HVariationModel modelID, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(87);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public HRegion CompareVariationModel(HVariationModel modelID)
		{
			IntPtr proc = HalconAPI.PreCall(88);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public void TrainVariationModel(HVariationModel modelID)
		{
			IntPtr proc = HalconAPI.PreCall(91);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public HHomMat2D ProjMatchPointsDistortionRansacGuided(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, HHomMat2D homMat2DGuide, double kappaGuide, double distanceTolerance, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out double kappa, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(256);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.Store(proc, 6, homMat2DGuide);
			HalconAPI.StoreD(proc, 7, kappaGuide);
			HalconAPI.StoreD(proc, 8, distanceTolerance);
			HalconAPI.Store(proc, 9, matchThreshold);
			HalconAPI.StoreS(proc, 10, estimationMethod);
			HalconAPI.Store(proc, 11, distanceThreshold);
			HalconAPI.StoreI(proc, 12, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(homMat2DGuide);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out kappa);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D ProjMatchPointsDistortionRansacGuided(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, HHomMat2D homMat2DGuide, double kappaGuide, double distanceTolerance, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out double kappa, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(256);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.Store(proc, 6, homMat2DGuide);
			HalconAPI.StoreD(proc, 7, kappaGuide);
			HalconAPI.StoreD(proc, 8, distanceTolerance);
			HalconAPI.StoreI(proc, 9, matchThreshold);
			HalconAPI.StoreS(proc, 10, estimationMethod);
			HalconAPI.StoreD(proc, 11, distanceThreshold);
			HalconAPI.StoreI(proc, 12, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(homMat2DGuide);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out kappa);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D ProjMatchPointsDistortionRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out double kappa, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(257);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.Store(proc, 10, rotation);
			HalconAPI.Store(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.Store(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out kappa);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D ProjMatchPointsDistortionRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out double kappa, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(257);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.StoreD(proc, 10, rotation);
			HalconAPI.StoreI(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.StoreD(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out kappa);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D ProjMatchPointsRansacGuided(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, HHomMat2D homMat2DGuide, double distanceTolerance, HTuple matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(258);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.Store(proc, 6, homMat2DGuide);
			HalconAPI.StoreD(proc, 7, distanceTolerance);
			HalconAPI.Store(proc, 8, matchThreshold);
			HalconAPI.StoreS(proc, 9, estimationMethod);
			HalconAPI.StoreD(proc, 10, distanceThreshold);
			HalconAPI.StoreI(proc, 11, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(homMat2DGuide);
			HalconAPI.UnpinTuple(matchThreshold);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D ProjMatchPointsRansacGuided(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, HHomMat2D homMat2DGuide, double distanceTolerance, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(258);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.Store(proc, 6, homMat2DGuide);
			HalconAPI.StoreD(proc, 7, distanceTolerance);
			HalconAPI.StoreI(proc, 8, matchThreshold);
			HalconAPI.StoreS(proc, 9, estimationMethod);
			HalconAPI.StoreD(proc, 10, distanceThreshold);
			HalconAPI.StoreI(proc, 11, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(homMat2DGuide);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D ProjMatchPointsRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(259);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.Store(proc, 10, rotation);
			HalconAPI.Store(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.StoreD(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D ProjMatchPointsRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(259);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.StoreD(proc, 10, rotation);
			HalconAPI.StoreI(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.StoreD(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public void ReceiveImage(HSocket socket)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(325);
			HalconAPI.Store(proc, 0, socket);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(socket);
		}

		public void SendImage(HSocket socket)
		{
			IntPtr proc = HalconAPI.PreCall(326);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, socket);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(socket);
		}

		public HImage BinocularDistanceMs(HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, HPose relPoseRect, int minDisparity, int maxDisparity, int surfaceSmoothing, int edgeSmoothing, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(346);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreI(proc, 3, minDisparity);
			HalconAPI.StoreI(proc, 4, maxDisparity);
			HalconAPI.StoreI(proc, 5, surfaceSmoothing);
			HalconAPI.StoreI(proc, 6, edgeSmoothing);
			HalconAPI.Store(proc, 7, genParamName);
			HalconAPI.Store(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDistanceMs(HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, HPose relPoseRect, int minDisparity, int maxDisparity, int surfaceSmoothing, int edgeSmoothing, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(346);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreI(proc, 3, minDisparity);
			HalconAPI.StoreI(proc, 4, maxDisparity);
			HalconAPI.StoreI(proc, 5, surfaceSmoothing);
			HalconAPI.StoreI(proc, 6, edgeSmoothing);
			HalconAPI.StoreS(proc, 7, genParamName);
			HalconAPI.StoreS(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDisparityMs(HImage imageRect2, out HImage score, int minDisparity, int maxDisparity, int surfaceSmoothing, int edgeSmoothing, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(347);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.StoreI(proc, 0, minDisparity);
			HalconAPI.StoreI(proc, 1, maxDisparity);
			HalconAPI.StoreI(proc, 2, surfaceSmoothing);
			HalconAPI.StoreI(proc, 3, edgeSmoothing);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDisparityMs(HImage imageRect2, out HImage score, int minDisparity, int maxDisparity, int surfaceSmoothing, int edgeSmoothing, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(347);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.StoreI(proc, 0, minDisparity);
			HalconAPI.StoreI(proc, 1, maxDisparity);
			HalconAPI.StoreI(proc, 2, surfaceSmoothing);
			HalconAPI.StoreI(proc, 3, edgeSmoothing);
			HalconAPI.StoreS(proc, 4, genParamName);
			HalconAPI.StoreS(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDistanceMg(HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, HPose relPoseRect, double grayConstancy, double gradientConstancy, double smoothness, double initialGuess, string calculateScore, HTuple MGParamName, HTuple MGParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(348);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreD(proc, 3, grayConstancy);
			HalconAPI.StoreD(proc, 4, gradientConstancy);
			HalconAPI.StoreD(proc, 5, smoothness);
			HalconAPI.StoreD(proc, 6, initialGuess);
			HalconAPI.StoreS(proc, 7, calculateScore);
			HalconAPI.Store(proc, 8, MGParamName);
			HalconAPI.Store(proc, 9, MGParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HalconAPI.UnpinTuple(MGParamName);
			HalconAPI.UnpinTuple(MGParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDistanceMg(HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, HPose relPoseRect, double grayConstancy, double gradientConstancy, double smoothness, double initialGuess, string calculateScore, string MGParamName, string MGParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(348);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreD(proc, 3, grayConstancy);
			HalconAPI.StoreD(proc, 4, gradientConstancy);
			HalconAPI.StoreD(proc, 5, smoothness);
			HalconAPI.StoreD(proc, 6, initialGuess);
			HalconAPI.StoreS(proc, 7, calculateScore);
			HalconAPI.StoreS(proc, 8, MGParamName);
			HalconAPI.StoreS(proc, 9, MGParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDisparityMg(HImage imageRect2, out HImage score, double grayConstancy, double gradientConstancy, double smoothness, double initialGuess, string calculateScore, HTuple MGParamName, HTuple MGParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(349);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.StoreD(proc, 0, grayConstancy);
			HalconAPI.StoreD(proc, 1, gradientConstancy);
			HalconAPI.StoreD(proc, 2, smoothness);
			HalconAPI.StoreD(proc, 3, initialGuess);
			HalconAPI.StoreS(proc, 4, calculateScore);
			HalconAPI.Store(proc, 5, MGParamName);
			HalconAPI.Store(proc, 6, MGParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(MGParamName);
			HalconAPI.UnpinTuple(MGParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDisparityMg(HImage imageRect2, out HImage score, double grayConstancy, double gradientConstancy, double smoothness, double initialGuess, string calculateScore, string MGParamName, string MGParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(349);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.StoreD(proc, 0, grayConstancy);
			HalconAPI.StoreD(proc, 1, gradientConstancy);
			HalconAPI.StoreD(proc, 2, smoothness);
			HalconAPI.StoreD(proc, 3, initialGuess);
			HalconAPI.StoreS(proc, 4, calculateScore);
			HalconAPI.StoreS(proc, 5, MGParamName);
			HalconAPI.StoreS(proc, 6, MGParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage GenBinocularProjRectification(HHomMat2D FMatrix, HTuple covFMat, int width1, int height1, int width2, int height2, HTuple subSampling, string mapping, out HTuple covFMatRect, out HHomMat2D h1, out HHomMat2D h2)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(351);
			HalconAPI.Store(proc, 0, FMatrix);
			HalconAPI.Store(proc, 1, covFMat);
			HalconAPI.StoreI(proc, 2, width1);
			HalconAPI.StoreI(proc, 3, height1);
			HalconAPI.StoreI(proc, 4, width2);
			HalconAPI.StoreI(proc, 5, height2);
			HalconAPI.Store(proc, 6, subSampling);
			HalconAPI.StoreS(proc, 7, mapping);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(FMatrix);
			HalconAPI.UnpinTuple(covFMat);
			HalconAPI.UnpinTuple(subSampling);
			err = base.Load(proc, 1, err);
			HImage result = null;
			err = HImage.LoadNew(proc, 2, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out covFMatRect);
			err = HHomMat2D.LoadNew(proc, 1, err, out h1);
			err = HHomMat2D.LoadNew(proc, 2, err, out h2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenBinocularProjRectification(HHomMat2D FMatrix, HTuple covFMat, int width1, int height1, int width2, int height2, int subSampling, string mapping, out HTuple covFMatRect, out HHomMat2D h1, out HHomMat2D h2)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(351);
			HalconAPI.Store(proc, 0, FMatrix);
			HalconAPI.Store(proc, 1, covFMat);
			HalconAPI.StoreI(proc, 2, width1);
			HalconAPI.StoreI(proc, 3, height1);
			HalconAPI.StoreI(proc, 4, width2);
			HalconAPI.StoreI(proc, 5, height2);
			HalconAPI.StoreI(proc, 6, subSampling);
			HalconAPI.StoreS(proc, 7, mapping);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(FMatrix);
			HalconAPI.UnpinTuple(covFMat);
			err = base.Load(proc, 1, err);
			HImage result = null;
			err = HImage.LoadNew(proc, 2, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out covFMatRect);
			err = HHomMat2D.LoadNew(proc, 1, err, out h1);
			err = HHomMat2D.LoadNew(proc, 2, err, out h2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D MatchFundamentalMatrixDistortionRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out double kappa, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(358);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.Store(proc, 10, rotation);
			HalconAPI.Store(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.Store(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out kappa);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D MatchFundamentalMatrixDistortionRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out double kappa, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(358);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.StoreD(proc, 10, rotation);
			HalconAPI.StoreI(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.StoreD(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out kappa);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HPose MatchRelPoseRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HCamPar camPar1, HCamPar camPar2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out HTuple covRelPose, out HTuple error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(359);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, camPar1);
			HalconAPI.Store(proc, 5, camPar2);
			HalconAPI.StoreS(proc, 6, grayMatchMethod);
			HalconAPI.StoreI(proc, 7, maskSize);
			HalconAPI.StoreI(proc, 8, rowMove);
			HalconAPI.StoreI(proc, 9, colMove);
			HalconAPI.StoreI(proc, 10, rowTolerance);
			HalconAPI.StoreI(proc, 11, colTolerance);
			HalconAPI.Store(proc, 12, rotation);
			HalconAPI.Store(proc, 13, matchThreshold);
			HalconAPI.StoreS(proc, 14, estimationMethod);
			HalconAPI.Store(proc, 15, distanceThreshold);
			HalconAPI.StoreI(proc, 16, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(camPar1);
			HalconAPI.UnpinTuple(camPar2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covRelPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HPose MatchRelPoseRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HCamPar camPar1, HCamPar camPar2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple covRelPose, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(359);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, camPar1);
			HalconAPI.Store(proc, 5, camPar2);
			HalconAPI.StoreS(proc, 6, grayMatchMethod);
			HalconAPI.StoreI(proc, 7, maskSize);
			HalconAPI.StoreI(proc, 8, rowMove);
			HalconAPI.StoreI(proc, 9, colMove);
			HalconAPI.StoreI(proc, 10, rowTolerance);
			HalconAPI.StoreI(proc, 11, colTolerance);
			HalconAPI.StoreD(proc, 12, rotation);
			HalconAPI.StoreI(proc, 13, matchThreshold);
			HalconAPI.StoreS(proc, 14, estimationMethod);
			HalconAPI.StoreD(proc, 15, distanceThreshold);
			HalconAPI.StoreI(proc, 16, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(camPar1);
			HalconAPI.UnpinTuple(camPar2);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covRelPose);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D MatchEssentialMatrixRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HHomMat2D camMat1, HHomMat2D camMat2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out HTuple covEMat, out HTuple error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(360);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, camMat1);
			HalconAPI.Store(proc, 5, camMat2);
			HalconAPI.StoreS(proc, 6, grayMatchMethod);
			HalconAPI.StoreI(proc, 7, maskSize);
			HalconAPI.StoreI(proc, 8, rowMove);
			HalconAPI.StoreI(proc, 9, colMove);
			HalconAPI.StoreI(proc, 10, rowTolerance);
			HalconAPI.StoreI(proc, 11, colTolerance);
			HalconAPI.Store(proc, 12, rotation);
			HalconAPI.Store(proc, 13, matchThreshold);
			HalconAPI.StoreS(proc, 14, estimationMethod);
			HalconAPI.Store(proc, 15, distanceThreshold);
			HalconAPI.StoreI(proc, 16, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(camMat1);
			HalconAPI.UnpinTuple(camMat2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covEMat);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D MatchEssentialMatrixRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HHomMat2D camMat1, HHomMat2D camMat2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple covEMat, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(360);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, camMat1);
			HalconAPI.Store(proc, 5, camMat2);
			HalconAPI.StoreS(proc, 6, grayMatchMethod);
			HalconAPI.StoreI(proc, 7, maskSize);
			HalconAPI.StoreI(proc, 8, rowMove);
			HalconAPI.StoreI(proc, 9, colMove);
			HalconAPI.StoreI(proc, 10, rowTolerance);
			HalconAPI.StoreI(proc, 11, colTolerance);
			HalconAPI.StoreD(proc, 12, rotation);
			HalconAPI.StoreI(proc, 13, matchThreshold);
			HalconAPI.StoreS(proc, 14, estimationMethod);
			HalconAPI.StoreD(proc, 15, distanceThreshold);
			HalconAPI.StoreI(proc, 16, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(camMat1);
			HalconAPI.UnpinTuple(camMat2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covEMat);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D MatchFundamentalMatrixRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out HTuple covFMat, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(361);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.Store(proc, 10, rotation);
			HalconAPI.Store(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.Store(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covFMat);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D MatchFundamentalMatrixRansac(HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple covFMat, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(361);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.StoreD(proc, 10, rotation);
			HalconAPI.StoreI(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.StoreD(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covFMat);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage BinocularDistance(HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, HPose relPoseRect, string method, int maskWidth, int maskHeight, HTuple textureThresh, int minDisparity, int maxDisparity, int numLevels, HTuple scoreThresh, HTuple filter, HTuple subDistance)
		{
			IntPtr proc = HalconAPI.PreCall(362);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreS(proc, 3, method);
			HalconAPI.StoreI(proc, 4, maskWidth);
			HalconAPI.StoreI(proc, 5, maskHeight);
			HalconAPI.Store(proc, 6, textureThresh);
			HalconAPI.StoreI(proc, 7, minDisparity);
			HalconAPI.StoreI(proc, 8, maxDisparity);
			HalconAPI.StoreI(proc, 9, numLevels);
			HalconAPI.Store(proc, 10, scoreThresh);
			HalconAPI.Store(proc, 11, filter);
			HalconAPI.Store(proc, 12, subDistance);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HalconAPI.UnpinTuple(textureThresh);
			HalconAPI.UnpinTuple(scoreThresh);
			HalconAPI.UnpinTuple(filter);
			HalconAPI.UnpinTuple(subDistance);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDistance(HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, HPose relPoseRect, string method, int maskWidth, int maskHeight, double textureThresh, int minDisparity, int maxDisparity, int numLevels, double scoreThresh, string filter, string subDistance)
		{
			IntPtr proc = HalconAPI.PreCall(362);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreS(proc, 3, method);
			HalconAPI.StoreI(proc, 4, maskWidth);
			HalconAPI.StoreI(proc, 5, maskHeight);
			HalconAPI.StoreD(proc, 6, textureThresh);
			HalconAPI.StoreI(proc, 7, minDisparity);
			HalconAPI.StoreI(proc, 8, maxDisparity);
			HalconAPI.StoreI(proc, 9, numLevels);
			HalconAPI.StoreD(proc, 10, scoreThresh);
			HalconAPI.StoreS(proc, 11, filter);
			HalconAPI.StoreS(proc, 12, subDistance);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDisparity(HImage imageRect2, out HImage score, string method, int maskWidth, int maskHeight, HTuple textureThresh, int minDisparity, int maxDisparity, int numLevels, HTuple scoreThresh, HTuple filter, string subDisparity)
		{
			IntPtr proc = HalconAPI.PreCall(363);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.StoreI(proc, 1, maskWidth);
			HalconAPI.StoreI(proc, 2, maskHeight);
			HalconAPI.Store(proc, 3, textureThresh);
			HalconAPI.StoreI(proc, 4, minDisparity);
			HalconAPI.StoreI(proc, 5, maxDisparity);
			HalconAPI.StoreI(proc, 6, numLevels);
			HalconAPI.Store(proc, 7, scoreThresh);
			HalconAPI.Store(proc, 8, filter);
			HalconAPI.StoreS(proc, 9, subDisparity);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(textureThresh);
			HalconAPI.UnpinTuple(scoreThresh);
			HalconAPI.UnpinTuple(filter);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDisparity(HImage imageRect2, out HImage score, string method, int maskWidth, int maskHeight, double textureThresh, int minDisparity, int maxDisparity, int numLevels, double scoreThresh, string filter, string subDisparity)
		{
			IntPtr proc = HalconAPI.PreCall(363);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.StoreI(proc, 1, maskWidth);
			HalconAPI.StoreI(proc, 2, maskHeight);
			HalconAPI.StoreD(proc, 3, textureThresh);
			HalconAPI.StoreI(proc, 4, minDisparity);
			HalconAPI.StoreI(proc, 5, maxDisparity);
			HalconAPI.StoreI(proc, 6, numLevels);
			HalconAPI.StoreD(proc, 7, scoreThresh);
			HalconAPI.StoreS(proc, 8, filter);
			HalconAPI.StoreS(proc, 9, subDisparity);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage DisparityImageToXyz(out HImage y, out HImage z, HCamPar camParamRect1, HCamPar camParamRect2, HPose relPoseRect)
		{
			IntPtr proc = HalconAPI.PreCall(365);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out y);
			err = HImage.LoadNew(proc, 3, err, out z);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenBinocularRectificationMap(HCamPar camParam1, HCamPar camParam2, HPose relPose, double subSampling, string method, string mapType, out HCamPar camParamRect1, out HCamPar camParamRect2, out HPose camPoseRect1, out HPose camPoseRect2, out HPose relPoseRect)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(369);
			HalconAPI.Store(proc, 0, camParam1);
			HalconAPI.Store(proc, 1, camParam2);
			HalconAPI.Store(proc, 2, relPose);
			HalconAPI.StoreD(proc, 3, subSampling);
			HalconAPI.StoreS(proc, 4, method);
			HalconAPI.StoreS(proc, 5, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam1);
			HalconAPI.UnpinTuple(camParam2);
			HalconAPI.UnpinTuple(relPose);
			err = base.Load(proc, 1, err);
			HImage result = null;
			err = HImage.LoadNew(proc, 2, err, out result);
			err = HCamPar.LoadNew(proc, 0, err, out camParamRect1);
			err = HCamPar.LoadNew(proc, 1, err, out camParamRect2);
			err = HPose.LoadNew(proc, 2, err, out camPoseRect1);
			err = HPose.LoadNew(proc, 3, err, out camPoseRect2);
			err = HPose.LoadNew(proc, 4, err, out relPoseRect);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetSheetOfLightResult(HSheetOfLightModel sheetOfLightModelID, HTuple resultName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(381);
			HalconAPI.Store(proc, 0, sheetOfLightModelID);
			HalconAPI.Store(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultName);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sheetOfLightModelID);
		}

		public void GetSheetOfLightResult(HSheetOfLightModel sheetOfLightModelID, string resultName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(381);
			HalconAPI.Store(proc, 0, sheetOfLightModelID);
			HalconAPI.StoreS(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sheetOfLightModelID);
		}

		public void ApplySheetOfLightCalibration(HSheetOfLightModel sheetOfLightModelID)
		{
			IntPtr proc = HalconAPI.PreCall(382);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sheetOfLightModelID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(sheetOfLightModelID);
		}

		public void SetProfileSheetOfLight(HSheetOfLightModel sheetOfLightModelID, HTuple movementPoses)
		{
			IntPtr proc = HalconAPI.PreCall(383);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sheetOfLightModelID);
			HalconAPI.Store(proc, 1, movementPoses);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(movementPoses);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(sheetOfLightModelID);
		}

		public void MeasureProfileSheetOfLight(HSheetOfLightModel sheetOfLightModelID, HTuple movementPose)
		{
			IntPtr proc = HalconAPI.PreCall(384);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sheetOfLightModelID);
			HalconAPI.Store(proc, 1, movementPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(movementPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(sheetOfLightModelID);
		}

		public HImage ShadeHeightField(HTuple slant, HTuple tilt, HTuple albedo, HTuple ambient, string shadows)
		{
			IntPtr proc = HalconAPI.PreCall(392);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, slant);
			HalconAPI.Store(proc, 1, tilt);
			HalconAPI.Store(proc, 2, albedo);
			HalconAPI.Store(proc, 3, ambient);
			HalconAPI.StoreS(proc, 4, shadows);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(slant);
			HalconAPI.UnpinTuple(tilt);
			HalconAPI.UnpinTuple(albedo);
			HalconAPI.UnpinTuple(ambient);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ShadeHeightField(double slant, double tilt, double albedo, double ambient, string shadows)
		{
			IntPtr proc = HalconAPI.PreCall(392);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, slant);
			HalconAPI.StoreD(proc, 1, tilt);
			HalconAPI.StoreD(proc, 2, albedo);
			HalconAPI.StoreD(proc, 3, ambient);
			HalconAPI.StoreS(proc, 4, shadows);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple EstimateAlAm(out HTuple ambient)
		{
			IntPtr proc = HalconAPI.PreCall(393);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out ambient);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double EstimateAlAm(out double ambient)
		{
			IntPtr proc = HalconAPI.PreCall(393);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out ambient);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple EstimateSlAlZc(out HTuple albedo)
		{
			IntPtr proc = HalconAPI.PreCall(394);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out albedo);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double EstimateSlAlZc(out double albedo)
		{
			IntPtr proc = HalconAPI.PreCall(394);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out albedo);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple EstimateSlAlLr(out HTuple albedo)
		{
			IntPtr proc = HalconAPI.PreCall(395);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out albedo);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double EstimateSlAlLr(out double albedo)
		{
			IntPtr proc = HalconAPI.PreCall(395);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out albedo);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple EstimateTiltZc()
		{
			IntPtr proc = HalconAPI.PreCall(396);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple EstimateTiltLr()
		{
			IntPtr proc = HalconAPI.PreCall(397);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ReconstructHeightFieldFromGradient(string reconstructionMethod, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(398);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, reconstructionMethod);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PhotometricStereo(out HImage gradient, out HImage albedo, HTuple slants, HTuple tilts, HTuple resultType, string reconstructionMethod, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(399);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, slants);
			HalconAPI.Store(proc, 1, tilts);
			HalconAPI.Store(proc, 2, resultType);
			HalconAPI.StoreS(proc, 3, reconstructionMethod);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(slants);
			HalconAPI.UnpinTuple(tilts);
			HalconAPI.UnpinTuple(resultType);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out gradient);
			err = HImage.LoadNew(proc, 3, err, out albedo);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SfsPentland(HTuple slant, HTuple tilt, HTuple albedo, HTuple ambient)
		{
			IntPtr proc = HalconAPI.PreCall(400);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, slant);
			HalconAPI.Store(proc, 1, tilt);
			HalconAPI.Store(proc, 2, albedo);
			HalconAPI.Store(proc, 3, ambient);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(slant);
			HalconAPI.UnpinTuple(tilt);
			HalconAPI.UnpinTuple(albedo);
			HalconAPI.UnpinTuple(ambient);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SfsPentland(double slant, double tilt, double albedo, double ambient)
		{
			IntPtr proc = HalconAPI.PreCall(400);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, slant);
			HalconAPI.StoreD(proc, 1, tilt);
			HalconAPI.StoreD(proc, 2, albedo);
			HalconAPI.StoreD(proc, 3, ambient);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SfsOrigLr(HTuple slant, HTuple tilt, HTuple albedo, HTuple ambient)
		{
			IntPtr proc = HalconAPI.PreCall(401);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, slant);
			HalconAPI.Store(proc, 1, tilt);
			HalconAPI.Store(proc, 2, albedo);
			HalconAPI.Store(proc, 3, ambient);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(slant);
			HalconAPI.UnpinTuple(tilt);
			HalconAPI.UnpinTuple(albedo);
			HalconAPI.UnpinTuple(ambient);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SfsOrigLr(double slant, double tilt, double albedo, double ambient)
		{
			IntPtr proc = HalconAPI.PreCall(401);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, slant);
			HalconAPI.StoreD(proc, 1, tilt);
			HalconAPI.StoreD(proc, 2, albedo);
			HalconAPI.StoreD(proc, 3, ambient);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SfsModLr(HTuple slant, HTuple tilt, HTuple albedo, HTuple ambient)
		{
			IntPtr proc = HalconAPI.PreCall(402);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, slant);
			HalconAPI.Store(proc, 1, tilt);
			HalconAPI.Store(proc, 2, albedo);
			HalconAPI.Store(proc, 3, ambient);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(slant);
			HalconAPI.UnpinTuple(tilt);
			HalconAPI.UnpinTuple(albedo);
			HalconAPI.UnpinTuple(ambient);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SfsModLr(double slant, double tilt, double albedo, double ambient)
		{
			IntPtr proc = HalconAPI.PreCall(402);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, slant);
			HalconAPI.StoreD(proc, 1, tilt);
			HalconAPI.StoreD(proc, 2, albedo);
			HalconAPI.StoreD(proc, 3, ambient);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTextResult FindText(HTextModel textModel)
		{
			IntPtr proc = HalconAPI.PreCall(417);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, textModel);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTextResult result = null;
			err = HTextResult.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(textModel);
			return result;
		}

		public HRegion ClassifyImageClassLut(HClassLUT classLUTHandle)
		{
			IntPtr proc = HalconAPI.PreCall(428);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, classLUTHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classLUTHandle);
			return result;
		}

		public HRegion ClassifyImageClassKnn(out HImage distanceImage, HClassKnn KNNHandle, double rejectionThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(429);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, KNNHandle);
			HalconAPI.StoreD(proc, 1, rejectionThreshold);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out distanceImage);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(KNNHandle);
			return result;
		}

		public void AddSamplesImageClassKnn(HRegion classRegions, HClassKnn KNNHandle)
		{
			IntPtr proc = HalconAPI.PreCall(430);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, classRegions);
			HalconAPI.Store(proc, 0, KNNHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classRegions);
			GC.KeepAlive(KNNHandle);
		}

		public HRegion ClassifyImageClassGmm(HClassGmm GMMHandle, double rejectionThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(431);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, GMMHandle);
			HalconAPI.StoreD(proc, 1, rejectionThreshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(GMMHandle);
			return result;
		}

		public void AddSamplesImageClassGmm(HRegion classRegions, HClassGmm GMMHandle, double randomize)
		{
			IntPtr proc = HalconAPI.PreCall(432);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, classRegions);
			HalconAPI.Store(proc, 0, GMMHandle);
			HalconAPI.StoreD(proc, 1, randomize);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classRegions);
			GC.KeepAlive(GMMHandle);
		}

		public HRegion ClassifyImageClassSvm(HClassSvm SVMHandle)
		{
			IntPtr proc = HalconAPI.PreCall(433);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, SVMHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SVMHandle);
			return result;
		}

		public void AddSamplesImageClassSvm(HRegion classRegions, HClassSvm SVMHandle)
		{
			IntPtr proc = HalconAPI.PreCall(434);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, classRegions);
			HalconAPI.Store(proc, 0, SVMHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classRegions);
			GC.KeepAlive(SVMHandle);
		}

		public HRegion ClassifyImageClassMlp(HClassMlp MLPHandle, double rejectionThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(435);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, MLPHandle);
			HalconAPI.StoreD(proc, 1, rejectionThreshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(MLPHandle);
			return result;
		}

		public void AddSamplesImageClassMlp(HRegion classRegions, HClassMlp MLPHandle)
		{
			IntPtr proc = HalconAPI.PreCall(436);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, classRegions);
			HalconAPI.Store(proc, 0, MLPHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classRegions);
			GC.KeepAlive(MLPHandle);
		}

		public HTuple LearnNdimNorm(HRegion foreground, HRegion background, string metric, HTuple distance, HTuple minNumberPercent, out HTuple center, out double quality)
		{
			IntPtr proc = HalconAPI.PreCall(437);
			base.Store(proc, 3);
			HalconAPI.Store(proc, 1, foreground);
			HalconAPI.Store(proc, 2, background);
			HalconAPI.StoreS(proc, 0, metric);
			HalconAPI.Store(proc, 1, distance);
			HalconAPI.Store(proc, 2, minNumberPercent);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(distance);
			HalconAPI.UnpinTuple(minNumberPercent);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out center);
			err = HalconAPI.LoadD(proc, 2, err, out quality);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(foreground);
			GC.KeepAlive(background);
			return result;
		}

		public HTuple LearnNdimNorm(HRegion foreground, HRegion background, string metric, double distance, double minNumberPercent, out HTuple center, out double quality)
		{
			IntPtr proc = HalconAPI.PreCall(437);
			base.Store(proc, 3);
			HalconAPI.Store(proc, 1, foreground);
			HalconAPI.Store(proc, 2, background);
			HalconAPI.StoreS(proc, 0, metric);
			HalconAPI.StoreD(proc, 1, distance);
			HalconAPI.StoreD(proc, 2, minNumberPercent);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out center);
			err = HalconAPI.LoadD(proc, 2, err, out quality);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(foreground);
			GC.KeepAlive(background);
			return result;
		}

		public void LearnNdimBox(HRegion foreground, HRegion background, HClassBox classifHandle)
		{
			IntPtr proc = HalconAPI.PreCall(438);
			base.Store(proc, 3);
			HalconAPI.Store(proc, 1, foreground);
			HalconAPI.Store(proc, 2, background);
			HalconAPI.Store(proc, 0, classifHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(foreground);
			GC.KeepAlive(background);
			GC.KeepAlive(classifHandle);
		}

		public HRegion ClassNdimBox(HClassBox classifHandle)
		{
			IntPtr proc = HalconAPI.PreCall(439);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, classifHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classifHandle);
			return result;
		}

		public HRegion ClassNdimNorm(string metric, string singleMultiple, HTuple radius, HTuple center)
		{
			IntPtr proc = HalconAPI.PreCall(440);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, metric);
			HalconAPI.StoreS(proc, 1, singleMultiple);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.Store(proc, 3, center);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(center);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion ClassNdimNorm(string metric, string singleMultiple, double radius, double center)
		{
			IntPtr proc = HalconAPI.PreCall(440);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, metric);
			HalconAPI.StoreS(proc, 1, singleMultiple);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, center);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion Class2dimSup(HImage imageRow, HRegion featureSpace)
		{
			IntPtr proc = HalconAPI.PreCall(441);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRow);
			HalconAPI.Store(proc, 3, featureSpace);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRow);
			GC.KeepAlive(featureSpace);
			return result;
		}

		public HRegion Class2dimUnsup(HImage image2, int threshold, int numClasses)
		{
			IntPtr proc = HalconAPI.PreCall(442);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.StoreI(proc, 0, threshold);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HRegion CheckDifference(HImage pattern, string mode, int diffLowerBound, int diffUpperBound, int grayOffset, int addRow, int addCol)
		{
			IntPtr proc = HalconAPI.PreCall(443);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, pattern);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, diffLowerBound);
			HalconAPI.StoreI(proc, 2, diffUpperBound);
			HalconAPI.StoreI(proc, 3, grayOffset);
			HalconAPI.StoreI(proc, 4, addRow);
			HalconAPI.StoreI(proc, 5, addCol);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
			return result;
		}

		public HRegion CharThreshold(HRegion histoRegion, double sigma, HTuple percent, out HTuple threshold)
		{
			IntPtr proc = HalconAPI.PreCall(444);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, histoRegion);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.Store(proc, 1, percent);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(percent);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out threshold);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(histoRegion);
			return result;
		}

		public HRegion CharThreshold(HRegion histoRegion, double sigma, double percent, out int threshold)
		{
			IntPtr proc = HalconAPI.PreCall(444);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, histoRegion);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreD(proc, 1, percent);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HalconAPI.LoadI(proc, 0, err, out threshold);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(histoRegion);
			return result;
		}

		public HRegion LabelToRegion()
		{
			IntPtr proc = HalconAPI.PreCall(445);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage NonmaxSuppressionAmp(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(446);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage NonmaxSuppressionDir(HImage imgDir, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(447);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imgDir);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imgDir);
			return result;
		}

		public HRegion HysteresisThreshold(HTuple low, HTuple high, int maxLength)
		{
			IntPtr proc = HalconAPI.PreCall(448);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, low);
			HalconAPI.Store(proc, 1, high);
			HalconAPI.StoreI(proc, 2, maxLength);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(low);
			HalconAPI.UnpinTuple(high);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion HysteresisThreshold(int low, int high, int maxLength)
		{
			IntPtr proc = HalconAPI.PreCall(448);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, low);
			HalconAPI.StoreI(proc, 1, high);
			HalconAPI.StoreI(proc, 2, maxLength);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion BinaryThreshold(string method, string lightDark, out HTuple usedThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(449);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.StoreS(proc, 1, lightDark);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out usedThreshold);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion BinaryThreshold(string method, string lightDark, out int usedThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(449);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.StoreS(proc, 1, lightDark);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HalconAPI.LoadI(proc, 0, err, out usedThreshold);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion LocalThreshold(string method, string lightDark, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(450);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.StoreS(proc, 1, lightDark);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion LocalThreshold(string method, string lightDark, string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(450);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.StoreS(proc, 1, lightDark);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreI(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion VarThreshold(int maskWidth, int maskHeight, HTuple stdDevScale, HTuple absThreshold, string lightDark)
		{
			IntPtr proc = HalconAPI.PreCall(451);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.Store(proc, 2, stdDevScale);
			HalconAPI.Store(proc, 3, absThreshold);
			HalconAPI.StoreS(proc, 4, lightDark);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(stdDevScale);
			HalconAPI.UnpinTuple(absThreshold);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion VarThreshold(int maskWidth, int maskHeight, double stdDevScale, double absThreshold, string lightDark)
		{
			IntPtr proc = HalconAPI.PreCall(451);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.StoreD(proc, 2, stdDevScale);
			HalconAPI.StoreD(proc, 3, absThreshold);
			HalconAPI.StoreS(proc, 4, lightDark);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion DynThreshold(HImage thresholdImage, HTuple offset, string lightDark)
		{
			IntPtr proc = HalconAPI.PreCall(452);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, thresholdImage);
			HalconAPI.Store(proc, 0, offset);
			HalconAPI.StoreS(proc, 1, lightDark);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(offset);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(thresholdImage);
			return result;
		}

		public HRegion DynThreshold(HImage thresholdImage, double offset, string lightDark)
		{
			IntPtr proc = HalconAPI.PreCall(452);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, thresholdImage);
			HalconAPI.StoreD(proc, 0, offset);
			HalconAPI.StoreS(proc, 1, lightDark);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(thresholdImage);
			return result;
		}

		public HRegion Threshold(HTuple minGray, HTuple maxGray)
		{
			IntPtr proc = HalconAPI.PreCall(453);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, minGray);
			HalconAPI.Store(proc, 1, maxGray);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minGray);
			HalconAPI.UnpinTuple(maxGray);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion Threshold(double minGray, double maxGray)
		{
			IntPtr proc = HalconAPI.PreCall(453);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, minGray);
			HalconAPI.StoreD(proc, 1, maxGray);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ThresholdSubPix(HTuple threshold)
		{
			IntPtr proc = HalconAPI.PreCall(454);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(threshold);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ThresholdSubPix(double threshold)
		{
			IntPtr proc = HalconAPI.PreCall(454);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion RegiongrowingN(string metric, HTuple minTolerance, HTuple maxTolerance, int minSize)
		{
			IntPtr proc = HalconAPI.PreCall(455);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, metric);
			HalconAPI.Store(proc, 1, minTolerance);
			HalconAPI.Store(proc, 2, maxTolerance);
			HalconAPI.StoreI(proc, 3, minSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minTolerance);
			HalconAPI.UnpinTuple(maxTolerance);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion RegiongrowingN(string metric, double minTolerance, double maxTolerance, int minSize)
		{
			IntPtr proc = HalconAPI.PreCall(455);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, metric);
			HalconAPI.StoreD(proc, 1, minTolerance);
			HalconAPI.StoreD(proc, 2, maxTolerance);
			HalconAPI.StoreI(proc, 3, minSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion Regiongrowing(int row, int column, HTuple tolerance, int minSize)
		{
			IntPtr proc = HalconAPI.PreCall(456);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.Store(proc, 2, tolerance);
			HalconAPI.StoreI(proc, 3, minSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(tolerance);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion Regiongrowing(int row, int column, double tolerance, int minSize)
		{
			IntPtr proc = HalconAPI.PreCall(456);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreD(proc, 2, tolerance);
			HalconAPI.StoreI(proc, 3, minSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion RegiongrowingMean(HTuple startRows, HTuple startColumns, double tolerance, int minSize)
		{
			IntPtr proc = HalconAPI.PreCall(457);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, startRows);
			HalconAPI.Store(proc, 1, startColumns);
			HalconAPI.StoreD(proc, 2, tolerance);
			HalconAPI.StoreI(proc, 3, minSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(startRows);
			HalconAPI.UnpinTuple(startColumns);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion RegiongrowingMean(int startRows, int startColumns, double tolerance, int minSize)
		{
			IntPtr proc = HalconAPI.PreCall(457);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, startRows);
			HalconAPI.StoreI(proc, 1, startColumns);
			HalconAPI.StoreD(proc, 2, tolerance);
			HalconAPI.StoreI(proc, 3, minSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion Pouring(string mode, int minGray, int maxGray)
		{
			IntPtr proc = HalconAPI.PreCall(458);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, minGray);
			HalconAPI.StoreI(proc, 2, maxGray);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion WatershedsThreshold(HTuple threshold)
		{
			IntPtr proc = HalconAPI.PreCall(459);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(threshold);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion WatershedsThreshold(int threshold)
		{
			IntPtr proc = HalconAPI.PreCall(459);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion Watersheds(out HRegion watersheds)
		{
			IntPtr proc = HalconAPI.PreCall(460);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HRegion.LoadNew(proc, 2, err, out watersheds);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion ZeroCrossing()
		{
			IntPtr proc = HalconAPI.PreCall(461);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ZeroCrossingSubPix()
		{
			IntPtr proc = HalconAPI.PreCall(462);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion DualThreshold(int minSize, double minGray, double threshold)
		{
			IntPtr proc = HalconAPI.PreCall(463);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, minSize);
			HalconAPI.StoreD(proc, 1, minGray);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion ExpandLine(int coordinate, string expandType, string rowColumn, HTuple threshold)
		{
			IntPtr proc = HalconAPI.PreCall(464);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, coordinate);
			HalconAPI.StoreS(proc, 1, expandType);
			HalconAPI.StoreS(proc, 2, rowColumn);
			HalconAPI.Store(proc, 3, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(threshold);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion ExpandLine(int coordinate, string expandType, string rowColumn, double threshold)
		{
			IntPtr proc = HalconAPI.PreCall(464);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, coordinate);
			HalconAPI.StoreS(proc, 1, expandType);
			HalconAPI.StoreS(proc, 2, rowColumn);
			HalconAPI.StoreD(proc, 3, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion LocalMin()
		{
			IntPtr proc = HalconAPI.PreCall(465);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion Lowlands()
		{
			IntPtr proc = HalconAPI.PreCall(466);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion LowlandsCenter()
		{
			IntPtr proc = HalconAPI.PreCall(467);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion LocalMax()
		{
			IntPtr proc = HalconAPI.PreCall(468);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion Plateaus()
		{
			IntPtr proc = HalconAPI.PreCall(469);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion PlateausCenter()
		{
			IntPtr proc = HalconAPI.PreCall(470);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion AutoThreshold(HTuple sigma)
		{
			IntPtr proc = HalconAPI.PreCall(472);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sigma);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(sigma);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion AutoThreshold(double sigma)
		{
			IntPtr proc = HalconAPI.PreCall(472);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion BinThreshold()
		{
			IntPtr proc = HalconAPI.PreCall(473);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion FastThreshold(HTuple minGray, HTuple maxGray, int minSize)
		{
			IntPtr proc = HalconAPI.PreCall(474);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, minGray);
			HalconAPI.Store(proc, 1, maxGray);
			HalconAPI.StoreI(proc, 2, minSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minGray);
			HalconAPI.UnpinTuple(maxGray);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion FastThreshold(double minGray, double maxGray, int minSize)
		{
			IntPtr proc = HalconAPI.PreCall(474);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, minGray);
			HalconAPI.StoreD(proc, 1, maxGray);
			HalconAPI.StoreI(proc, 2, minSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion ExpandGray(HRegion regions, HRegion forbiddenArea, HTuple iterations, string mode, HTuple threshold)
		{
			IntPtr proc = HalconAPI.PreCall(509);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.Store(proc, 3, forbiddenArea);
			HalconAPI.Store(proc, 0, iterations);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.Store(proc, 2, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(iterations);
			HalconAPI.UnpinTuple(threshold);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			GC.KeepAlive(forbiddenArea);
			return result;
		}

		public HRegion ExpandGray(HRegion regions, HRegion forbiddenArea, string iterations, string mode, int threshold)
		{
			IntPtr proc = HalconAPI.PreCall(509);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.Store(proc, 3, forbiddenArea);
			HalconAPI.StoreS(proc, 0, iterations);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.StoreI(proc, 2, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			GC.KeepAlive(forbiddenArea);
			return result;
		}

		public HRegion ExpandGrayRef(HRegion regions, HRegion forbiddenArea, HTuple iterations, string mode, HTuple refGray, HTuple threshold)
		{
			IntPtr proc = HalconAPI.PreCall(510);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.Store(proc, 3, forbiddenArea);
			HalconAPI.Store(proc, 0, iterations);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.Store(proc, 2, refGray);
			HalconAPI.Store(proc, 3, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(iterations);
			HalconAPI.UnpinTuple(refGray);
			HalconAPI.UnpinTuple(threshold);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			GC.KeepAlive(forbiddenArea);
			return result;
		}

		public HRegion ExpandGrayRef(HRegion regions, HRegion forbiddenArea, string iterations, string mode, int refGray, int threshold)
		{
			IntPtr proc = HalconAPI.PreCall(510);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.Store(proc, 3, forbiddenArea);
			HalconAPI.StoreS(proc, 0, iterations);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.StoreI(proc, 2, refGray);
			HalconAPI.StoreI(proc, 3, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			GC.KeepAlive(forbiddenArea);
			return result;
		}

		public HImage ObjDiff(HImage objectsSub)
		{
			IntPtr proc = HalconAPI.PreCall(573);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsSub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsSub);
			return result;
		}

		public void SetGrayval(HTuple row, HTuple column, HTuple grayval)
		{
			IntPtr proc = HalconAPI.PreCall(574);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, grayval);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(grayval);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetGrayval(int row, int column, double grayval)
		{
			IntPtr proc = HalconAPI.PreCall(574);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreD(proc, 2, grayval);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HImage PaintXld(HXLD XLD, HTuple grayval)
		{
			IntPtr proc = HalconAPI.PreCall(575);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, XLD);
			HalconAPI.Store(proc, 0, grayval);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(grayval);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(XLD);
			return result;
		}

		public HImage PaintXld(HXLD XLD, double grayval)
		{
			IntPtr proc = HalconAPI.PreCall(575);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, XLD);
			HalconAPI.StoreD(proc, 0, grayval);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(XLD);
			return result;
		}

		public HImage PaintRegion(HRegion region, HTuple grayval, string type)
		{
			IntPtr proc = HalconAPI.PreCall(576);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, region);
			HalconAPI.Store(proc, 0, grayval);
			HalconAPI.StoreS(proc, 1, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(grayval);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HImage PaintRegion(HRegion region, double grayval, string type)
		{
			IntPtr proc = HalconAPI.PreCall(576);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, region);
			HalconAPI.StoreD(proc, 0, grayval);
			HalconAPI.StoreS(proc, 1, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public void OverpaintRegion(HRegion region, HTuple grayval, string type)
		{
			IntPtr proc = HalconAPI.PreCall(577);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.Store(proc, 0, grayval);
			HalconAPI.StoreS(proc, 1, type);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(grayval);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
		}

		public void OverpaintRegion(HRegion region, double grayval, string type)
		{
			IntPtr proc = HalconAPI.PreCall(577);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.StoreD(proc, 0, grayval);
			HalconAPI.StoreS(proc, 1, type);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
		}

		public HImage GenImageProto(HTuple grayval)
		{
			IntPtr proc = HalconAPI.PreCall(578);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, grayval);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(grayval);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenImageProto(double grayval)
		{
			IntPtr proc = HalconAPI.PreCall(578);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, grayval);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PaintGray(HImage imageDestination)
		{
			IntPtr proc = HalconAPI.PreCall(579);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageDestination);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageDestination);
			return result;
		}

		public void OverpaintGray(HImage imageSource)
		{
			IntPtr proc = HalconAPI.PreCall(580);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageSource);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(imageSource);
		}

		public new HImage CopyObj(int index, int numObj)
		{
			IntPtr proc = HalconAPI.PreCall(583);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.StoreI(proc, 1, numObj);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ConcatObj(HImage objects2)
		{
			IntPtr proc = HalconAPI.PreCall(584);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public HImage CopyImage()
		{
			IntPtr proc = HalconAPI.PreCall(586);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HImage SelectObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HImage SelectObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int CompareObj(HImage objects2, HTuple epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.Store(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(epsilon);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public int CompareObj(HImage objects2, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.StoreD(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public int TestEqualObj(HImage objects2)
		{
			IntPtr proc = HalconAPI.PreCall(591);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public void GenImageInterleaved(IntPtr pixelPointer, string colorFormat, int originalWidth, int originalHeight, int alignment, string type, int imageWidth, int imageHeight, int startRow, int startColumn, int bitsPerChannel, int bitShift)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(595);
			HalconAPI.StoreIP(proc, 0, pixelPointer);
			HalconAPI.StoreS(proc, 1, colorFormat);
			HalconAPI.StoreI(proc, 2, originalWidth);
			HalconAPI.StoreI(proc, 3, originalHeight);
			HalconAPI.StoreI(proc, 4, alignment);
			HalconAPI.StoreS(proc, 5, type);
			HalconAPI.StoreI(proc, 6, imageWidth);
			HalconAPI.StoreI(proc, 7, imageHeight);
			HalconAPI.StoreI(proc, 8, startRow);
			HalconAPI.StoreI(proc, 9, startColumn);
			HalconAPI.StoreI(proc, 10, bitsPerChannel);
			HalconAPI.StoreI(proc, 11, bitShift);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenImage3(string type, int width, int height, IntPtr pixelPointerRed, IntPtr pixelPointerGreen, IntPtr pixelPointerBlue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(605);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.StoreIP(proc, 3, pixelPointerRed);
			HalconAPI.StoreIP(proc, 4, pixelPointerGreen);
			HalconAPI.StoreIP(proc, 5, pixelPointerBlue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenImage1(string type, int width, int height, IntPtr pixelPointer)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(606);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.StoreIP(proc, 3, pixelPointer);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenImageConst(string type, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(607);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenImageGrayRamp(double alpha, double beta, double mean, int row, int column, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(619);
			HalconAPI.StoreD(proc, 0, alpha);
			HalconAPI.StoreD(proc, 1, beta);
			HalconAPI.StoreD(proc, 2, mean);
			HalconAPI.StoreI(proc, 3, row);
			HalconAPI.StoreI(proc, 4, column);
			HalconAPI.StoreI(proc, 5, width);
			HalconAPI.StoreI(proc, 6, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenImage3Extern(string type, int width, int height, IntPtr pointerRed, IntPtr pointerGreen, IntPtr pointerBlue, IntPtr clearProc)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(620);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.StoreIP(proc, 3, pointerRed);
			HalconAPI.StoreIP(proc, 4, pointerGreen);
			HalconAPI.StoreIP(proc, 5, pointerBlue);
			HalconAPI.StoreIP(proc, 6, clearProc);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenImage1Extern(string type, int width, int height, IntPtr pixelPointer, IntPtr clearProc)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(621);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.StoreIP(proc, 3, pixelPointer);
			HalconAPI.StoreIP(proc, 4, clearProc);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenImage1Rect(IntPtr pixelPointer, int width, int height, int verticalPitch, int horizontalBitPitch, int bitsPerPixel, string doCopy, IntPtr clearProc)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(622);
			HalconAPI.StoreIP(proc, 0, pixelPointer);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.StoreI(proc, 3, verticalPitch);
			HalconAPI.StoreI(proc, 4, horizontalBitPitch);
			HalconAPI.StoreI(proc, 5, bitsPerPixel);
			HalconAPI.StoreS(proc, 6, doCopy);
			HalconAPI.StoreIP(proc, 7, clearProc);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public IntPtr GetImagePointer1Rect(out int width, out int height, out int verticalPitch, out int horizontalBitPitch, out int bitsPerPixel)
		{
			IntPtr proc = HalconAPI.PreCall(623);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			IntPtr result = default(IntPtr);
			err = HalconAPI.LoadIP(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out width);
			err = HalconAPI.LoadI(proc, 2, err, out height);
			err = HalconAPI.LoadI(proc, 3, err, out verticalPitch);
			err = HalconAPI.LoadI(proc, 4, err, out horizontalBitPitch);
			err = HalconAPI.LoadI(proc, 5, err, out bitsPerPixel);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetImagePointer3(out HTuple pointerRed, out HTuple pointerGreen, out HTuple pointerBlue, out HTuple type, out HTuple width, out HTuple height)
		{
			IntPtr proc = HalconAPI.PreCall(624);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out pointerRed);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out pointerGreen);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out pointerBlue);
			err = HTuple.LoadNew(proc, 3, err, out type);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out width);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetImagePointer3(out IntPtr pointerRed, out IntPtr pointerGreen, out IntPtr pointerBlue, out string type, out int width, out int height)
		{
			IntPtr proc = HalconAPI.PreCall(624);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadIP(proc, 0, err, out pointerRed);
			err = HalconAPI.LoadIP(proc, 1, err, out pointerGreen);
			err = HalconAPI.LoadIP(proc, 2, err, out pointerBlue);
			err = HalconAPI.LoadS(proc, 3, err, out type);
			err = HalconAPI.LoadI(proc, 4, err, out width);
			err = HalconAPI.LoadI(proc, 5, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetImagePointer1(out HTuple type, out HTuple width, out HTuple height)
		{
			IntPtr proc = HalconAPI.PreCall(625);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out type);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out width);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public IntPtr GetImagePointer1(out string type, out int width, out int height)
		{
			IntPtr proc = HalconAPI.PreCall(625);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			IntPtr result = default(IntPtr);
			err = HalconAPI.LoadIP(proc, 0, err, out result);
			err = HalconAPI.LoadS(proc, 1, err, out type);
			err = HalconAPI.LoadI(proc, 2, err, out width);
			err = HalconAPI.LoadI(proc, 3, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetImageType()
		{
			IntPtr proc = HalconAPI.PreCall(626);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetImageSize(out HTuple width, out HTuple height)
		{
			IntPtr proc = HalconAPI.PreCall(627);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out width);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetImageSize(out int width, out int height)
		{
			IntPtr proc = HalconAPI.PreCall(627);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out width);
			err = HalconAPI.LoadI(proc, 1, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public int GetImageTime(out int second, out int minute, out int hour, out int day, out int YDay, out int month, out int year)
		{
			IntPtr proc = HalconAPI.PreCall(628);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out second);
			err = HalconAPI.LoadI(proc, 2, err, out minute);
			err = HalconAPI.LoadI(proc, 3, err, out hour);
			err = HalconAPI.LoadI(proc, 4, err, out day);
			err = HalconAPI.LoadI(proc, 5, err, out YDay);
			err = HalconAPI.LoadI(proc, 6, err, out month);
			err = HalconAPI.LoadI(proc, 7, err, out year);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetGrayvalInterpolated(HTuple row, HTuple column, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(629);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double GetGrayvalInterpolated(double row, double column, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(629);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetGrayval(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(630);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetGrayval(int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(630);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple DoOcvSimple(HOCV OCVHandle, HTuple patternName, string adaptPos, string adaptSize, string adaptAngle, string adaptGray, double threshold)
		{
			IntPtr proc = HalconAPI.PreCall(638);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, OCVHandle);
			HalconAPI.Store(proc, 1, patternName);
			HalconAPI.StoreS(proc, 2, adaptPos);
			HalconAPI.StoreS(proc, 3, adaptSize);
			HalconAPI.StoreS(proc, 4, adaptAngle);
			HalconAPI.StoreS(proc, 5, adaptGray);
			HalconAPI.StoreD(proc, 6, threshold);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(patternName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(OCVHandle);
			return result;
		}

		public double DoOcvSimple(HOCV OCVHandle, string patternName, string adaptPos, string adaptSize, string adaptAngle, string adaptGray, double threshold)
		{
			IntPtr proc = HalconAPI.PreCall(638);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, OCVHandle);
			HalconAPI.StoreS(proc, 1, patternName);
			HalconAPI.StoreS(proc, 2, adaptPos);
			HalconAPI.StoreS(proc, 3, adaptSize);
			HalconAPI.StoreS(proc, 4, adaptAngle);
			HalconAPI.StoreS(proc, 5, adaptGray);
			HalconAPI.StoreD(proc, 6, threshold);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(OCVHandle);
			return result;
		}

		public void TraindOcvProj(HOCV OCVHandle, HTuple name, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(639);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, OCVHandle);
			HalconAPI.Store(proc, 1, name);
			HalconAPI.StoreS(proc, 2, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(name);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(OCVHandle);
		}

		public void TraindOcvProj(HOCV OCVHandle, string name, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(639);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, OCVHandle);
			HalconAPI.StoreS(proc, 1, name);
			HalconAPI.StoreS(proc, 2, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(OCVHandle);
		}

		public HTuple GetFeaturesOcrClassKnn(HOCRKnn OCRHandle, string transform)
		{
			IntPtr proc = HalconAPI.PreCall(656);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.StoreS(proc, 1, transform);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(OCRHandle);
			return result;
		}

		public HTuple GetFeaturesOcrClassSvm(HOCRSvm OCRHandle, string transform)
		{
			IntPtr proc = HalconAPI.PreCall(678);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.StoreS(proc, 1, transform);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(OCRHandle);
			return result;
		}

		public HTuple GetFeaturesOcrClassMlp(HOCRMlp OCRHandle, string transform)
		{
			IntPtr proc = HalconAPI.PreCall(696);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.StoreS(proc, 1, transform);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(OCRHandle);
			return result;
		}

		public HImage CropDomainRel(int top, int left, int bottom, int right)
		{
			IntPtr proc = HalconAPI.PreCall(726);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, top);
			HalconAPI.StoreI(proc, 1, left);
			HalconAPI.StoreI(proc, 2, bottom);
			HalconAPI.StoreI(proc, 3, right);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple OcrGetFeatures(HOCRBox ocrHandle)
		{
			IntPtr proc = HalconAPI.PreCall(727);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, ocrHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(ocrHandle);
			return result;
		}

		public void WriteOcrTrainfImage(HTuple classVal, string trainingFile)
		{
			IntPtr proc = HalconAPI.PreCall(729);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, classVal);
			HalconAPI.StoreS(proc, 1, trainingFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(classVal);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteOcrTrainfImage(string classVal, string trainingFile)
		{
			IntPtr proc = HalconAPI.PreCall(729);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, classVal);
			HalconAPI.StoreS(proc, 1, trainingFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ReadOcrTrainfSelect(HTuple trainingFile, HTuple searchNames)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(733);
			HalconAPI.Store(proc, 0, trainingFile);
			HalconAPI.Store(proc, 1, searchNames);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(searchNames);
			err = base.Load(proc, 1, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string ReadOcrTrainfSelect(string trainingFile, string searchNames)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(733);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.StoreS(proc, 1, searchNames);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ReadOcrTrainf(HTuple trainingFile)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(734);
			HalconAPI.Store(proc, 0, trainingFile);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			err = base.Load(proc, 1, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ReadOcrTrainf(string trainingFile)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(734);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayBothat(HImage SE)
		{
			IntPtr proc = HalconAPI.PreCall(780);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, SE);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SE);
			return result;
		}

		public HImage GrayTophat(HImage SE)
		{
			IntPtr proc = HalconAPI.PreCall(781);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, SE);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SE);
			return result;
		}

		public HImage GrayClosing(HImage SE)
		{
			IntPtr proc = HalconAPI.PreCall(782);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, SE);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SE);
			return result;
		}

		public HImage GrayOpening(HImage SE)
		{
			IntPtr proc = HalconAPI.PreCall(783);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, SE);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SE);
			return result;
		}

		public HImage GrayDilation(HImage SE)
		{
			IntPtr proc = HalconAPI.PreCall(784);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, SE);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SE);
			return result;
		}

		public HImage GrayErosion(HImage SE)
		{
			IntPtr proc = HalconAPI.PreCall(785);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, SE);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SE);
			return result;
		}

		public void ReadGraySe(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(786);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenDiscSe(string type, int width, int height, HTuple smax)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(787);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.Store(proc, 3, smax);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(smax);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenDiscSe(string type, int width, int height, double smax)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(787);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.StoreD(proc, 3, smax);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void MeasureThresh(HMeasure measureHandle, double sigma, double threshold, string select, out HTuple rowThresh, out HTuple columnThresh, out HTuple distance)
		{
			IntPtr proc = HalconAPI.PreCall(825);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, measureHandle);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.StoreS(proc, 3, select);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowThresh);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnThresh);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out distance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(measureHandle);
		}

		public HTuple MeasureProjection(HMeasure measureHandle)
		{
			IntPtr proc = HalconAPI.PreCall(828);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, measureHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(measureHandle);
			return result;
		}

		public void FuzzyMeasurePairing(HMeasure measureHandle, double sigma, double ampThresh, double fuzzyThresh, string transition, string pairing, int numPairs, out HTuple rowEdgeFirst, out HTuple columnEdgeFirst, out HTuple amplitudeFirst, out HTuple rowEdgeSecond, out HTuple columnEdgeSecond, out HTuple amplitudeSecond, out HTuple rowPairCenter, out HTuple columnPairCenter, out HTuple fuzzyScore, out HTuple intraDistance)
		{
			IntPtr proc = HalconAPI.PreCall(832);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, measureHandle);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, ampThresh);
			HalconAPI.StoreD(proc, 3, fuzzyThresh);
			HalconAPI.StoreS(proc, 4, transition);
			HalconAPI.StoreS(proc, 5, pairing);
			HalconAPI.StoreI(proc, 6, numPairs);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowEdgeFirst);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnEdgeFirst);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out amplitudeFirst);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rowEdgeSecond);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out columnEdgeSecond);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out amplitudeSecond);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out rowPairCenter);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out columnPairCenter);
			err = HTuple.LoadNew(proc, 8, HTupleType.DOUBLE, err, out fuzzyScore);
			err = HTuple.LoadNew(proc, 9, HTupleType.DOUBLE, err, out intraDistance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(measureHandle);
		}

		public void FuzzyMeasurePairs(HMeasure measureHandle, double sigma, double ampThresh, double fuzzyThresh, string transition, out HTuple rowEdgeFirst, out HTuple columnEdgeFirst, out HTuple amplitudeFirst, out HTuple rowEdgeSecond, out HTuple columnEdgeSecond, out HTuple amplitudeSecond, out HTuple rowEdgeCenter, out HTuple columnEdgeCenter, out HTuple fuzzyScore, out HTuple intraDistance, out HTuple interDistance)
		{
			IntPtr proc = HalconAPI.PreCall(833);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, measureHandle);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, ampThresh);
			HalconAPI.StoreD(proc, 3, fuzzyThresh);
			HalconAPI.StoreS(proc, 4, transition);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			HalconAPI.InitOCT(proc, 10);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowEdgeFirst);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnEdgeFirst);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out amplitudeFirst);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rowEdgeSecond);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out columnEdgeSecond);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out amplitudeSecond);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out rowEdgeCenter);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out columnEdgeCenter);
			err = HTuple.LoadNew(proc, 8, HTupleType.DOUBLE, err, out fuzzyScore);
			err = HTuple.LoadNew(proc, 9, HTupleType.DOUBLE, err, out intraDistance);
			err = HTuple.LoadNew(proc, 10, HTupleType.DOUBLE, err, out interDistance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(measureHandle);
		}

		public void FuzzyMeasurePos(HMeasure measureHandle, double sigma, double ampThresh, double fuzzyThresh, string transition, out HTuple rowEdge, out HTuple columnEdge, out HTuple amplitude, out HTuple fuzzyScore, out HTuple distance)
		{
			IntPtr proc = HalconAPI.PreCall(834);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, measureHandle);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, ampThresh);
			HalconAPI.StoreD(proc, 3, fuzzyThresh);
			HalconAPI.StoreS(proc, 4, transition);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowEdge);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnEdge);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out amplitude);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out fuzzyScore);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out distance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(measureHandle);
		}

		public void MeasurePairs(HMeasure measureHandle, double sigma, double threshold, string transition, string select, out HTuple rowEdgeFirst, out HTuple columnEdgeFirst, out HTuple amplitudeFirst, out HTuple rowEdgeSecond, out HTuple columnEdgeSecond, out HTuple amplitudeSecond, out HTuple intraDistance, out HTuple interDistance)
		{
			IntPtr proc = HalconAPI.PreCall(835);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, measureHandle);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.StoreS(proc, 3, transition);
			HalconAPI.StoreS(proc, 4, select);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowEdgeFirst);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnEdgeFirst);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out amplitudeFirst);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rowEdgeSecond);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out columnEdgeSecond);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out amplitudeSecond);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out intraDistance);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out interDistance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(measureHandle);
		}

		public void MeasurePos(HMeasure measureHandle, double sigma, double threshold, string transition, string select, out HTuple rowEdge, out HTuple columnEdge, out HTuple amplitude, out HTuple distance)
		{
			IntPtr proc = HalconAPI.PreCall(836);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, measureHandle);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.StoreS(proc, 3, transition);
			HalconAPI.StoreS(proc, 4, select);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowEdge);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnEdge);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out amplitude);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out distance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(measureHandle);
		}

		public HTuple ApplySampleIdentifier(HSampleIdentifier sampleIdentifier, int numResults, double ratingThreshold, HTuple genParamName, HTuple genParamValue, out HTuple rating)
		{
			IntPtr proc = HalconAPI.PreCall(904);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sampleIdentifier);
			HalconAPI.StoreI(proc, 1, numResults);
			HalconAPI.StoreD(proc, 2, ratingThreshold);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out rating);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampleIdentifier);
			return result;
		}

		public int ApplySampleIdentifier(HSampleIdentifier sampleIdentifier, int numResults, double ratingThreshold, HTuple genParamName, HTuple genParamValue, out double rating)
		{
			IntPtr proc = HalconAPI.PreCall(904);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sampleIdentifier);
			HalconAPI.StoreI(proc, 1, numResults);
			HalconAPI.StoreD(proc, 2, ratingThreshold);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out rating);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampleIdentifier);
			return result;
		}

		public int AddSampleIdentifierTrainingData(HSampleIdentifier sampleIdentifier, HTuple objectIdx, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(912);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sampleIdentifier);
			HalconAPI.Store(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectIdx);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampleIdentifier);
			return result;
		}

		public int AddSampleIdentifierTrainingData(HSampleIdentifier sampleIdentifier, int objectIdx, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(912);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sampleIdentifier);
			HalconAPI.StoreI(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampleIdentifier);
			return result;
		}

		public int AddSampleIdentifierPreparationData(HSampleIdentifier sampleIdentifier, HTuple objectIdx, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(914);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sampleIdentifier);
			HalconAPI.Store(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectIdx);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampleIdentifier);
			return result;
		}

		public int AddSampleIdentifierPreparationData(HSampleIdentifier sampleIdentifier, int objectIdx, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(914);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sampleIdentifier);
			HalconAPI.StoreI(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampleIdentifier);
			return result;
		}

		public HTuple DetermineShapeModelParams(HTuple numLevels, double angleStart, double angleExtent, HTuple scaleMin, HTuple scaleMax, string optimization, string metric, HTuple contrast, HTuple minContrast, HTuple parameters, out HTuple parameterValue)
		{
			IntPtr proc = HalconAPI.PreCall(923);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, scaleMin);
			HalconAPI.Store(proc, 4, scaleMax);
			HalconAPI.StoreS(proc, 5, optimization);
			HalconAPI.StoreS(proc, 6, metric);
			HalconAPI.Store(proc, 7, contrast);
			HalconAPI.Store(proc, 8, minContrast);
			HalconAPI.Store(proc, 9, parameters);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(scaleMin);
			HalconAPI.UnpinTuple(scaleMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(parameters);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out parameterValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple DetermineShapeModelParams(int numLevels, double angleStart, double angleExtent, double scaleMin, double scaleMax, string optimization, string metric, int contrast, int minContrast, string parameters, out HTuple parameterValue)
		{
			IntPtr proc = HalconAPI.PreCall(923);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleMin);
			HalconAPI.StoreD(proc, 4, scaleMax);
			HalconAPI.StoreS(proc, 5, optimization);
			HalconAPI.StoreS(proc, 6, metric);
			HalconAPI.StoreI(proc, 7, contrast);
			HalconAPI.StoreI(proc, 8, minContrast);
			HalconAPI.StoreS(proc, 9, parameters);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out parameterValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void FindAnisoShapeModels(HShapeModel[] modelIDs, HTuple angleStart, HTuple angleExtent, HTuple scaleRMin, HTuple scaleRMax, HTuple scaleCMin, HTuple scaleCMax, HTuple minScore, HTuple numMatches, HTuple maxOverlap, HTuple subPixel, HTuple numLevels, HTuple greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scaleR, out HTuple scaleC, out HTuple score, out HTuple model)
		{
			HTuple hTuple = HTool.ConcatArray(modelIDs);
			IntPtr proc = HalconAPI.PreCall(927);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, scaleRMin);
			HalconAPI.Store(proc, 4, scaleRMax);
			HalconAPI.Store(proc, 5, scaleCMin);
			HalconAPI.Store(proc, 6, scaleCMax);
			HalconAPI.Store(proc, 7, minScore);
			HalconAPI.Store(proc, 8, numMatches);
			HalconAPI.Store(proc, 9, maxOverlap);
			HalconAPI.Store(proc, 10, subPixel);
			HalconAPI.Store(proc, 11, numLevels);
			HalconAPI.Store(proc, 12, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMin);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMin);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(numMatches);
			HalconAPI.UnpinTuple(maxOverlap);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(greediness);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scaleR);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out scaleC);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 6, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelIDs);
		}

		public void FindAnisoShapeModels(HShapeModel modelIDs, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scaleR, out HTuple scaleC, out HTuple score, out HTuple model)
		{
			IntPtr proc = HalconAPI.PreCall(927);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelIDs);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.StoreS(proc, 10, subPixel);
			HalconAPI.StoreI(proc, 11, numLevels);
			HalconAPI.StoreD(proc, 12, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scaleR);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out scaleC);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 6, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelIDs);
		}

		public void FindScaledShapeModels(HShapeModel[] modelIDs, HTuple angleStart, HTuple angleExtent, HTuple scaleMin, HTuple scaleMax, HTuple minScore, HTuple numMatches, HTuple maxOverlap, HTuple subPixel, HTuple numLevels, HTuple greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scale, out HTuple score, out HTuple model)
		{
			HTuple hTuple = HTool.ConcatArray(modelIDs);
			IntPtr proc = HalconAPI.PreCall(928);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, scaleMin);
			HalconAPI.Store(proc, 4, scaleMax);
			HalconAPI.Store(proc, 5, minScore);
			HalconAPI.Store(proc, 6, numMatches);
			HalconAPI.Store(proc, 7, maxOverlap);
			HalconAPI.Store(proc, 8, subPixel);
			HalconAPI.Store(proc, 9, numLevels);
			HalconAPI.Store(proc, 10, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleMin);
			HalconAPI.UnpinTuple(scaleMax);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(numMatches);
			HalconAPI.UnpinTuple(maxOverlap);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(greediness);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scale);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelIDs);
		}

		public void FindScaledShapeModels(HShapeModel modelIDs, double angleStart, double angleExtent, double scaleMin, double scaleMax, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scale, out HTuple score, out HTuple model)
		{
			IntPtr proc = HalconAPI.PreCall(928);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelIDs);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleMin);
			HalconAPI.StoreD(proc, 4, scaleMax);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.StoreD(proc, 7, maxOverlap);
			HalconAPI.StoreS(proc, 8, subPixel);
			HalconAPI.StoreI(proc, 9, numLevels);
			HalconAPI.StoreD(proc, 10, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scale);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelIDs);
		}

		public void FindShapeModels(HShapeModel[] modelIDs, HTuple angleStart, HTuple angleExtent, HTuple minScore, HTuple numMatches, HTuple maxOverlap, HTuple subPixel, HTuple numLevels, HTuple greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple score, out HTuple model)
		{
			HTuple hTuple = HTool.ConcatArray(modelIDs);
			IntPtr proc = HalconAPI.PreCall(929);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, numMatches);
			HalconAPI.Store(proc, 5, maxOverlap);
			HalconAPI.Store(proc, 6, subPixel);
			HalconAPI.Store(proc, 7, numLevels);
			HalconAPI.Store(proc, 8, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(numMatches);
			HalconAPI.UnpinTuple(maxOverlap);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(greediness);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelIDs);
		}

		public void FindShapeModels(HShapeModel modelIDs, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple score, out HTuple model)
		{
			IntPtr proc = HalconAPI.PreCall(929);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelIDs);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, subPixel);
			HalconAPI.StoreI(proc, 7, numLevels);
			HalconAPI.StoreD(proc, 8, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelIDs);
		}

		public void FindAnisoShapeModel(HShapeModel modelID, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, HTuple subPixel, HTuple numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scaleR, out HTuple scaleC, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(930);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.Store(proc, 10, subPixel);
			HalconAPI.Store(proc, 11, numLevels);
			HalconAPI.StoreD(proc, 12, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scaleR);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out scaleC);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public void FindAnisoShapeModel(HShapeModel modelID, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scaleR, out HTuple scaleC, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(930);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.StoreS(proc, 10, subPixel);
			HalconAPI.StoreI(proc, 11, numLevels);
			HalconAPI.StoreD(proc, 12, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scaleR);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out scaleC);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public void FindScaledShapeModel(HShapeModel modelID, double angleStart, double angleExtent, double scaleMin, double scaleMax, double minScore, int numMatches, double maxOverlap, HTuple subPixel, HTuple numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scale, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(931);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleMin);
			HalconAPI.StoreD(proc, 4, scaleMax);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.StoreD(proc, 7, maxOverlap);
			HalconAPI.Store(proc, 8, subPixel);
			HalconAPI.Store(proc, 9, numLevels);
			HalconAPI.StoreD(proc, 10, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scale);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public void FindScaledShapeModel(HShapeModel modelID, double angleStart, double angleExtent, double scaleMin, double scaleMax, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scale, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(931);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleMin);
			HalconAPI.StoreD(proc, 4, scaleMax);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.StoreD(proc, 7, maxOverlap);
			HalconAPI.StoreS(proc, 8, subPixel);
			HalconAPI.StoreI(proc, 9, numLevels);
			HalconAPI.StoreD(proc, 10, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scale);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public void FindShapeModel(HShapeModel modelID, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, HTuple subPixel, HTuple numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(932);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.Store(proc, 6, subPixel);
			HalconAPI.Store(proc, 7, numLevels);
			HalconAPI.StoreD(proc, 8, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public void FindShapeModel(HShapeModel modelID, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(932);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, subPixel);
			HalconAPI.StoreI(proc, 7, numLevels);
			HalconAPI.StoreD(proc, 8, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public void SetShapeModelMetric(HShapeModel modelID, HHomMat2D homMat2D, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(933);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, homMat2D);
			HalconAPI.StoreS(proc, 2, metric);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public static void SetShapeModelParam(HShapeModel modelID, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(934);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(modelID);
		}

		public HShapeModel CreateAnisoShapeModel(HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleRMin, double scaleRMax, HTuple scaleRStep, double scaleCMin, double scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(938);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.Store(proc, 13, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateAnisoShapeModel(int numLevels, double angleStart, double angleExtent, double angleStep, double scaleRMin, double scaleRMax, double scaleRStep, double scaleCMin, double scaleCMax, double scaleCStep, string optimization, string metric, int contrast, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(938);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, contrast);
			HalconAPI.StoreI(proc, 13, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateScaledShapeModel(HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleMin, double scaleMax, HTuple scaleStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(939);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.Store(proc, 6, scaleStep);
			HalconAPI.Store(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.Store(proc, 9, contrast);
			HalconAPI.Store(proc, 10, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateScaledShapeModel(int numLevels, double angleStart, double angleExtent, double angleStep, double scaleMin, double scaleMax, double scaleStep, string optimization, string metric, int contrast, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(939);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.StoreD(proc, 6, scaleStep);
			HalconAPI.StoreS(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, contrast);
			HalconAPI.StoreI(proc, 10, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateShapeModel(HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(940);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.Store(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.Store(proc, 6, contrast);
			HalconAPI.Store(proc, 7, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateShapeModel(int numLevels, double angleStart, double angleExtent, double angleStep, string optimization, string metric, int contrast, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(940);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, contrast);
			HalconAPI.StoreI(proc, 7, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage InspectShapeModel(out HRegion modelRegions, int numLevels, HTuple contrast)
		{
			IntPtr proc = HalconAPI.PreCall(941);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, contrast);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(contrast);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HRegion.LoadNew(proc, 2, err, out modelRegions);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage InspectShapeModel(out HRegion modelRegions, int numLevels, int contrast)
		{
			IntPtr proc = HalconAPI.PreCall(941);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreI(proc, 1, contrast);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HRegion.LoadNew(proc, 2, err, out modelRegions);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HPose[] FindCalibDescriptorModel(HDescriptorModel modelID, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, HTuple minScore, int numMatches, HCamPar camParam, HTuple scoreType, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(948);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.Store(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.Store(proc, 7, camParam);
			HalconAPI.Store(proc, 8, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(scoreType);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, err, out score);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public HPose FindCalibDescriptorModel(HDescriptorModel modelID, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, double minScore, int numMatches, HCamPar camParam, string scoreType, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(948);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.Store(proc, 7, camParam);
			HalconAPI.StoreS(proc, 8, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HalconAPI.UnpinTuple(camParam);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public HHomMat2D[] FindUncalibDescriptorModel(HDescriptorModel modelID, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, HTuple minScore, int numMatches, HTuple scoreType, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(949);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.Store(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.Store(proc, 7, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(scoreType);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, err, out score);
			HalconAPI.PostCall(proc, err);
			HHomMat2D[] result = HHomMat2D.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public HHomMat2D FindUncalibDescriptorModel(HDescriptorModel modelID, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, double minScore, int numMatches, string scoreType, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(949);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.StoreS(proc, 7, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public HDescriptorModel CreateCalibDescriptorModel(HCamPar camParam, HPose referencePose, string detectorType, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, int seed)
		{
			IntPtr proc = HalconAPI.PreCall(952);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreS(proc, 2, detectorType);
			HalconAPI.Store(proc, 3, detectorParamName);
			HalconAPI.Store(proc, 4, detectorParamValue);
			HalconAPI.Store(proc, 5, descriptorParamName);
			HalconAPI.Store(proc, 6, descriptorParamValue);
			HalconAPI.StoreI(proc, 7, seed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HDescriptorModel result = null;
			err = HDescriptorModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDescriptorModel CreateUncalibDescriptorModel(string detectorType, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, int seed)
		{
			IntPtr proc = HalconAPI.PreCall(953);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, detectorType);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.StoreI(proc, 5, seed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HDescriptorModel result = null;
			err = HDescriptorModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple DetermineDeformableModelParams(HTuple numLevels, double angleStart, double angleExtent, HTuple scaleMin, HTuple scaleMax, string optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue, HTuple parameters, out HTuple parameterValue)
		{
			IntPtr proc = HalconAPI.PreCall(962);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, scaleMin);
			HalconAPI.Store(proc, 4, scaleMax);
			HalconAPI.StoreS(proc, 5, optimization);
			HalconAPI.StoreS(proc, 6, metric);
			HalconAPI.Store(proc, 7, contrast);
			HalconAPI.Store(proc, 8, minContrast);
			HalconAPI.Store(proc, 9, genParamName);
			HalconAPI.Store(proc, 10, genParamValue);
			HalconAPI.Store(proc, 11, parameters);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(scaleMin);
			HalconAPI.UnpinTuple(scaleMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.UnpinTuple(parameters);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out parameterValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple DetermineDeformableModelParams(int numLevels, double angleStart, double angleExtent, double scaleMin, double scaleMax, string optimization, string metric, int contrast, int minContrast, HTuple genParamName, HTuple genParamValue, string parameters, out HTuple parameterValue)
		{
			IntPtr proc = HalconAPI.PreCall(962);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleMin);
			HalconAPI.StoreD(proc, 4, scaleMax);
			HalconAPI.StoreS(proc, 5, optimization);
			HalconAPI.StoreS(proc, 6, metric);
			HalconAPI.StoreI(proc, 7, contrast);
			HalconAPI.StoreI(proc, 8, minContrast);
			HalconAPI.Store(proc, 9, genParamName);
			HalconAPI.Store(proc, 10, genParamValue);
			HalconAPI.StoreS(proc, 11, parameters);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out parameterValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage FindLocalDeformableModel(out HImage vectorField, out HXLDCont deformedContours, HDeformableModel modelID, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, int numLevels, double greediness, HTuple resultType, HTuple genParamName, HTuple genParamValue, out HTuple score, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(969);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.StoreI(proc, 10, numLevels);
			HalconAPI.StoreD(proc, 11, greediness);
			HalconAPI.Store(proc, 12, resultType);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultType);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out vectorField);
			err = HXLDCont.LoadNew(proc, 3, err, out deformedContours);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public HPose[] FindPlanarCalibDeformableModel(HDeformableModel modelID, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, HTuple numLevels, double greediness, HTuple genParamName, HTuple genParamValue, out HTuple covPose, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(970);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.Store(proc, 10, numLevels);
			HalconAPI.StoreD(proc, 11, greediness);
			HalconAPI.Store(proc, 12, genParamName);
			HalconAPI.Store(proc, 13, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public HPose FindPlanarCalibDeformableModel(HDeformableModel modelID, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, int numLevels, double greediness, HTuple genParamName, HTuple genParamValue, out HTuple covPose, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(970);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.StoreI(proc, 10, numLevels);
			HalconAPI.StoreD(proc, 11, greediness);
			HalconAPI.Store(proc, 12, genParamName);
			HalconAPI.Store(proc, 13, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public HHomMat2D[] FindPlanarUncalibDeformableModel(HDeformableModel modelID, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, HTuple numLevels, double greediness, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(971);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.Store(proc, 10, numLevels);
			HalconAPI.StoreD(proc, 11, greediness);
			HalconAPI.Store(proc, 12, genParamName);
			HalconAPI.Store(proc, 13, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			HHomMat2D[] result = HHomMat2D.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public HHomMat2D FindPlanarUncalibDeformableModel(HDeformableModel modelID, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, int numLevels, double greediness, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(971);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.StoreI(proc, 10, numLevels);
			HalconAPI.StoreD(proc, 11, greediness);
			HalconAPI.Store(proc, 12, genParamName);
			HalconAPI.Store(proc, 13, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
			return result;
		}

		public void SetLocalDeformableModelMetric(HImage vectorField, HDeformableModel modelID, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(972);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, vectorField);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreS(proc, 1, metric);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(vectorField);
			GC.KeepAlive(modelID);
		}

		public void SetPlanarCalibDeformableModelMetric(HDeformableModel modelID, HPose pose, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(973);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, pose);
			HalconAPI.StoreS(proc, 2, metric);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public void SetPlanarUncalibDeformableModelMetric(HDeformableModel modelID, HHomMat2D homMat2D, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(974);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, homMat2D);
			HalconAPI.StoreS(proc, 2, metric);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public HDeformableModel CreateLocalDeformableModel(HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(978);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.Store(proc, 13, minContrast);
			HalconAPI.Store(proc, 14, genParamName);
			HalconAPI.Store(proc, 15, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreateLocalDeformableModel(int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, HTuple contrast, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(978);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.StoreI(proc, 13, minContrast);
			HalconAPI.Store(proc, 14, genParamName);
			HalconAPI.Store(proc, 15, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreatePlanarCalibDeformableModel(HCamPar camParam, HPose referencePose, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(979);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.Store(proc, 14, contrast);
			HalconAPI.Store(proc, 15, minContrast);
			HalconAPI.Store(proc, 16, genParamName);
			HalconAPI.Store(proc, 17, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreatePlanarCalibDeformableModel(HCamPar camParam, HPose referencePose, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, HTuple contrast, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(979);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.Store(proc, 14, contrast);
			HalconAPI.StoreI(proc, 15, minContrast);
			HalconAPI.Store(proc, 16, genParamName);
			HalconAPI.Store(proc, 17, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreatePlanarUncalibDeformableModel(HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(980);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.Store(proc, 13, minContrast);
			HalconAPI.Store(proc, 14, genParamName);
			HalconAPI.Store(proc, 15, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreatePlanarUncalibDeformableModel(int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, HTuple contrast, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(980);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.StoreI(proc, 13, minContrast);
			HalconAPI.Store(proc, 14, genParamName);
			HalconAPI.Store(proc, 15, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void FindNccModel(HNCCModel modelID, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, string subPixel, HTuple numLevels, out HTuple row, out HTuple column, out HTuple angle, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(991);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, subPixel);
			HalconAPI.Store(proc, 7, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public void FindNccModel(HNCCModel modelID, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, out HTuple row, out HTuple column, out HTuple angle, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(991);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, subPixel);
			HalconAPI.StoreI(proc, 7, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelID);
		}

		public static void SetNccModelParam(HNCCModel modelID, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(992);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(modelID);
		}

		public HNCCModel CreateNccModel(HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(993);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, metric);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HNCCModel result = null;
			err = HNCCModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HNCCModel CreateNccModel(int numLevels, double angleStart, double angleExtent, double angleStep, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(993);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, metric);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HNCCModel result = null;
			err = HNCCModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple FindComponentModel(HComponentModel componentModelID, HTuple rootComponent, HTuple angleStartRoot, HTuple angleExtentRoot, double minScore, int numMatches, double maxOverlap, string ifRootNotFound, string ifComponentNotFound, string posePrediction, HTuple minScoreComp, HTuple subPixelComp, HTuple numLevelsComp, HTuple greedinessComp, out HTuple modelEnd, out HTuple score, out HTuple rowComp, out HTuple columnComp, out HTuple angleComp, out HTuple scoreComp, out HTuple modelComp)
		{
			IntPtr proc = HalconAPI.PreCall(995);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, componentModelID);
			HalconAPI.Store(proc, 1, rootComponent);
			HalconAPI.Store(proc, 2, angleStartRoot);
			HalconAPI.Store(proc, 3, angleExtentRoot);
			HalconAPI.StoreD(proc, 4, minScore);
			HalconAPI.StoreI(proc, 5, numMatches);
			HalconAPI.StoreD(proc, 6, maxOverlap);
			HalconAPI.StoreS(proc, 7, ifRootNotFound);
			HalconAPI.StoreS(proc, 8, ifComponentNotFound);
			HalconAPI.StoreS(proc, 9, posePrediction);
			HalconAPI.Store(proc, 10, minScoreComp);
			HalconAPI.Store(proc, 11, subPixelComp);
			HalconAPI.Store(proc, 12, numLevelsComp);
			HalconAPI.Store(proc, 13, greedinessComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rootComponent);
			HalconAPI.UnpinTuple(angleStartRoot);
			HalconAPI.UnpinTuple(angleExtentRoot);
			HalconAPI.UnpinTuple(minScoreComp);
			HalconAPI.UnpinTuple(subPixelComp);
			HalconAPI.UnpinTuple(numLevelsComp);
			HalconAPI.UnpinTuple(greedinessComp);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out modelEnd);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rowComp);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out columnComp);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out angleComp);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out scoreComp);
			err = HTuple.LoadNew(proc, 7, HTupleType.INTEGER, err, out modelComp);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(componentModelID);
			return result;
		}

		public int FindComponentModel(HComponentModel componentModelID, int rootComponent, double angleStartRoot, double angleExtentRoot, double minScore, int numMatches, double maxOverlap, string ifRootNotFound, string ifComponentNotFound, string posePrediction, double minScoreComp, string subPixelComp, int numLevelsComp, double greedinessComp, out int modelEnd, out double score, out double rowComp, out double columnComp, out double angleComp, out double scoreComp, out int modelComp)
		{
			IntPtr proc = HalconAPI.PreCall(995);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, componentModelID);
			HalconAPI.StoreI(proc, 1, rootComponent);
			HalconAPI.StoreD(proc, 2, angleStartRoot);
			HalconAPI.StoreD(proc, 3, angleExtentRoot);
			HalconAPI.StoreD(proc, 4, minScore);
			HalconAPI.StoreI(proc, 5, numMatches);
			HalconAPI.StoreD(proc, 6, maxOverlap);
			HalconAPI.StoreS(proc, 7, ifRootNotFound);
			HalconAPI.StoreS(proc, 8, ifComponentNotFound);
			HalconAPI.StoreS(proc, 9, posePrediction);
			HalconAPI.StoreD(proc, 10, minScoreComp);
			HalconAPI.StoreS(proc, 11, subPixelComp);
			HalconAPI.StoreI(proc, 12, numLevelsComp);
			HalconAPI.StoreD(proc, 13, greedinessComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out modelEnd);
			err = HalconAPI.LoadD(proc, 2, err, out score);
			err = HalconAPI.LoadD(proc, 3, err, out rowComp);
			err = HalconAPI.LoadD(proc, 4, err, out columnComp);
			err = HalconAPI.LoadD(proc, 5, err, out angleComp);
			err = HalconAPI.LoadD(proc, 6, err, out scoreComp);
			err = HalconAPI.LoadI(proc, 7, err, out modelComp);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(componentModelID);
			return result;
		}

		public HComponentModel CreateComponentModel(HRegion componentRegions, HTuple variationRow, HTuple variationColumn, HTuple variationAngle, double angleStart, double angleExtent, HTuple contrastLowComp, HTuple contrastHighComp, HTuple minSizeComp, HTuple minContrastComp, HTuple minScoreComp, HTuple numLevelsComp, HTuple angleStepComp, string optimizationComp, HTuple metricComp, HTuple pregenerationComp, out HTuple rootRanking)
		{
			IntPtr proc = HalconAPI.PreCall(1004);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, componentRegions);
			HalconAPI.Store(proc, 0, variationRow);
			HalconAPI.Store(proc, 1, variationColumn);
			HalconAPI.Store(proc, 2, variationAngle);
			HalconAPI.StoreD(proc, 3, angleStart);
			HalconAPI.StoreD(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, contrastLowComp);
			HalconAPI.Store(proc, 6, contrastHighComp);
			HalconAPI.Store(proc, 7, minSizeComp);
			HalconAPI.Store(proc, 8, minContrastComp);
			HalconAPI.Store(proc, 9, minScoreComp);
			HalconAPI.Store(proc, 10, numLevelsComp);
			HalconAPI.Store(proc, 11, angleStepComp);
			HalconAPI.StoreS(proc, 12, optimizationComp);
			HalconAPI.Store(proc, 13, metricComp);
			HalconAPI.Store(proc, 14, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(variationRow);
			HalconAPI.UnpinTuple(variationColumn);
			HalconAPI.UnpinTuple(variationAngle);
			HalconAPI.UnpinTuple(contrastLowComp);
			HalconAPI.UnpinTuple(contrastHighComp);
			HalconAPI.UnpinTuple(minSizeComp);
			HalconAPI.UnpinTuple(minContrastComp);
			HalconAPI.UnpinTuple(minScoreComp);
			HalconAPI.UnpinTuple(numLevelsComp);
			HalconAPI.UnpinTuple(angleStepComp);
			HalconAPI.UnpinTuple(metricComp);
			HalconAPI.UnpinTuple(pregenerationComp);
			HComponentModel result = null;
			err = HComponentModel.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out rootRanking);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(componentRegions);
			return result;
		}

		public HComponentModel CreateComponentModel(HRegion componentRegions, int variationRow, int variationColumn, double variationAngle, double angleStart, double angleExtent, int contrastLowComp, int contrastHighComp, int minSizeComp, int minContrastComp, double minScoreComp, int numLevelsComp, double angleStepComp, string optimizationComp, string metricComp, string pregenerationComp, out int rootRanking)
		{
			IntPtr proc = HalconAPI.PreCall(1004);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, componentRegions);
			HalconAPI.StoreI(proc, 0, variationRow);
			HalconAPI.StoreI(proc, 1, variationColumn);
			HalconAPI.StoreD(proc, 2, variationAngle);
			HalconAPI.StoreD(proc, 3, angleStart);
			HalconAPI.StoreD(proc, 4, angleExtent);
			HalconAPI.StoreI(proc, 5, contrastLowComp);
			HalconAPI.StoreI(proc, 6, contrastHighComp);
			HalconAPI.StoreI(proc, 7, minSizeComp);
			HalconAPI.StoreI(proc, 8, minContrastComp);
			HalconAPI.StoreD(proc, 9, minScoreComp);
			HalconAPI.StoreI(proc, 10, numLevelsComp);
			HalconAPI.StoreD(proc, 11, angleStepComp);
			HalconAPI.StoreS(proc, 12, optimizationComp);
			HalconAPI.StoreS(proc, 13, metricComp);
			HalconAPI.StoreS(proc, 14, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HComponentModel result = null;
			err = HComponentModel.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out rootRanking);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(componentRegions);
			return result;
		}

		public HRegion ClusterModelComponents(HComponentTraining componentTrainingID, string ambiguityCriterion, double maxContourOverlap, double clusterThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(1015);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, componentTrainingID);
			HalconAPI.StoreS(proc, 1, ambiguityCriterion);
			HalconAPI.StoreD(proc, 2, maxContourOverlap);
			HalconAPI.StoreD(proc, 3, clusterThreshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(componentTrainingID);
			return result;
		}

		public HRegion TrainModelComponents(HRegion initialComponents, HImage trainingImages, HTuple contrastLow, HTuple contrastHigh, HTuple minSize, HTuple minScore, HTuple searchRowTol, HTuple searchColumnTol, HTuple searchAngleTol, string trainingEmphasis, string ambiguityCriterion, double maxContourOverlap, double clusterThreshold, out HComponentTraining componentTrainingID)
		{
			IntPtr proc = HalconAPI.PreCall(1017);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, initialComponents);
			HalconAPI.Store(proc, 3, trainingImages);
			HalconAPI.Store(proc, 0, contrastLow);
			HalconAPI.Store(proc, 1, contrastHigh);
			HalconAPI.Store(proc, 2, minSize);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, searchRowTol);
			HalconAPI.Store(proc, 5, searchColumnTol);
			HalconAPI.Store(proc, 6, searchAngleTol);
			HalconAPI.StoreS(proc, 7, trainingEmphasis);
			HalconAPI.StoreS(proc, 8, ambiguityCriterion);
			HalconAPI.StoreD(proc, 9, maxContourOverlap);
			HalconAPI.StoreD(proc, 10, clusterThreshold);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(contrastLow);
			HalconAPI.UnpinTuple(contrastHigh);
			HalconAPI.UnpinTuple(minSize);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(searchRowTol);
			HalconAPI.UnpinTuple(searchColumnTol);
			HalconAPI.UnpinTuple(searchAngleTol);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HComponentTraining.LoadNew(proc, 0, err, out componentTrainingID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(initialComponents);
			GC.KeepAlive(trainingImages);
			return result;
		}

		public HRegion TrainModelComponents(HRegion initialComponents, HImage trainingImages, int contrastLow, int contrastHigh, int minSize, double minScore, int searchRowTol, int searchColumnTol, double searchAngleTol, string trainingEmphasis, string ambiguityCriterion, double maxContourOverlap, double clusterThreshold, out HComponentTraining componentTrainingID)
		{
			IntPtr proc = HalconAPI.PreCall(1017);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, initialComponents);
			HalconAPI.Store(proc, 3, trainingImages);
			HalconAPI.StoreI(proc, 0, contrastLow);
			HalconAPI.StoreI(proc, 1, contrastHigh);
			HalconAPI.StoreI(proc, 2, minSize);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, searchRowTol);
			HalconAPI.StoreI(proc, 5, searchColumnTol);
			HalconAPI.StoreD(proc, 6, searchAngleTol);
			HalconAPI.StoreS(proc, 7, trainingEmphasis);
			HalconAPI.StoreS(proc, 8, ambiguityCriterion);
			HalconAPI.StoreD(proc, 9, maxContourOverlap);
			HalconAPI.StoreD(proc, 10, clusterThreshold);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HComponentTraining.LoadNew(proc, 0, err, out componentTrainingID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(initialComponents);
			GC.KeepAlive(trainingImages);
			return result;
		}

		public HRegion GenInitialComponents(HTuple contrastLow, HTuple contrastHigh, HTuple minSize, string mode, HTuple genericName, HTuple genericValue)
		{
			IntPtr proc = HalconAPI.PreCall(1018);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, contrastLow);
			HalconAPI.Store(proc, 1, contrastHigh);
			HalconAPI.Store(proc, 2, minSize);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.Store(proc, 4, genericName);
			HalconAPI.Store(proc, 5, genericValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(contrastLow);
			HalconAPI.UnpinTuple(contrastHigh);
			HalconAPI.UnpinTuple(minSize);
			HalconAPI.UnpinTuple(genericName);
			HalconAPI.UnpinTuple(genericValue);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion GenInitialComponents(int contrastLow, int contrastHigh, int minSize, string mode, string genericName, double genericValue)
		{
			IntPtr proc = HalconAPI.PreCall(1018);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, contrastLow);
			HalconAPI.StoreI(proc, 1, contrastHigh);
			HalconAPI.StoreI(proc, 2, minSize);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.StoreS(proc, 4, genericName);
			HalconAPI.StoreD(proc, 5, genericValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HPose[] FindShapeModel3d(HShapeModel3D shapeModel3DID, double minScore, double greediness, HTuple numLevels, HTuple genParamName, HTuple genParamValue, out HTuple covPose, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1058);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, shapeModel3DID);
			HalconAPI.StoreD(proc, 1, minScore);
			HalconAPI.StoreD(proc, 2, greediness);
			HalconAPI.Store(proc, 3, numLevels);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(shapeModel3DID);
			return result;
		}

		public HImage ChannelsToImage()
		{
			IntPtr proc = HalconAPI.PreCall(1119);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ImageToChannels()
		{
			IntPtr proc = HalconAPI.PreCall(1120);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Compose7(HImage image2, HImage image3, HImage image4, HImage image5, HImage image6, HImage image7)
		{
			IntPtr proc = HalconAPI.PreCall(1121);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 3, image3);
			HalconAPI.Store(proc, 4, image4);
			HalconAPI.Store(proc, 5, image5);
			HalconAPI.Store(proc, 6, image6);
			HalconAPI.Store(proc, 7, image7);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			GC.KeepAlive(image3);
			GC.KeepAlive(image4);
			GC.KeepAlive(image5);
			GC.KeepAlive(image6);
			GC.KeepAlive(image7);
			return result;
		}

		public HImage Compose6(HImage image2, HImage image3, HImage image4, HImage image5, HImage image6)
		{
			IntPtr proc = HalconAPI.PreCall(1122);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 3, image3);
			HalconAPI.Store(proc, 4, image4);
			HalconAPI.Store(proc, 5, image5);
			HalconAPI.Store(proc, 6, image6);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			GC.KeepAlive(image3);
			GC.KeepAlive(image4);
			GC.KeepAlive(image5);
			GC.KeepAlive(image6);
			return result;
		}

		public HImage Compose5(HImage image2, HImage image3, HImage image4, HImage image5)
		{
			IntPtr proc = HalconAPI.PreCall(1123);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 3, image3);
			HalconAPI.Store(proc, 4, image4);
			HalconAPI.Store(proc, 5, image5);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			GC.KeepAlive(image3);
			GC.KeepAlive(image4);
			GC.KeepAlive(image5);
			return result;
		}

		public HImage Compose4(HImage image2, HImage image3, HImage image4)
		{
			IntPtr proc = HalconAPI.PreCall(1124);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 3, image3);
			HalconAPI.Store(proc, 4, image4);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			GC.KeepAlive(image3);
			GC.KeepAlive(image4);
			return result;
		}

		public HImage Compose3(HImage image2, HImage image3)
		{
			IntPtr proc = HalconAPI.PreCall(1125);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 3, image3);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			GC.KeepAlive(image3);
			return result;
		}

		public HImage Compose2(HImage image2)
		{
			IntPtr proc = HalconAPI.PreCall(1126);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage Decompose7(out HImage image2, out HImage image3, out HImage image4, out HImage image5, out HImage image6, out HImage image7)
		{
			IntPtr proc = HalconAPI.PreCall(1127);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out image2);
			err = HImage.LoadNew(proc, 3, err, out image3);
			err = HImage.LoadNew(proc, 4, err, out image4);
			err = HImage.LoadNew(proc, 5, err, out image5);
			err = HImage.LoadNew(proc, 6, err, out image6);
			err = HImage.LoadNew(proc, 7, err, out image7);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Decompose6(out HImage image2, out HImage image3, out HImage image4, out HImage image5, out HImage image6)
		{
			IntPtr proc = HalconAPI.PreCall(1128);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out image2);
			err = HImage.LoadNew(proc, 3, err, out image3);
			err = HImage.LoadNew(proc, 4, err, out image4);
			err = HImage.LoadNew(proc, 5, err, out image5);
			err = HImage.LoadNew(proc, 6, err, out image6);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Decompose5(out HImage image2, out HImage image3, out HImage image4, out HImage image5)
		{
			IntPtr proc = HalconAPI.PreCall(1129);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out image2);
			err = HImage.LoadNew(proc, 3, err, out image3);
			err = HImage.LoadNew(proc, 4, err, out image4);
			err = HImage.LoadNew(proc, 5, err, out image5);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Decompose4(out HImage image2, out HImage image3, out HImage image4)
		{
			IntPtr proc = HalconAPI.PreCall(1130);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out image2);
			err = HImage.LoadNew(proc, 3, err, out image3);
			err = HImage.LoadNew(proc, 4, err, out image4);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Decompose3(out HImage image2, out HImage image3)
		{
			IntPtr proc = HalconAPI.PreCall(1131);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out image2);
			err = HImage.LoadNew(proc, 3, err, out image3);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Decompose2(out HImage image2)
		{
			IntPtr proc = HalconAPI.PreCall(1132);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out image2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple CountChannels()
		{
			IntPtr proc = HalconAPI.PreCall(1133);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage AppendChannel(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(1134);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HImage AccessChannel(int channel)
		{
			IntPtr proc = HalconAPI.PreCall(1135);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, channel);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage TileImagesOffset(HTuple offsetRow, HTuple offsetCol, HTuple row1, HTuple col1, HTuple row2, HTuple col2, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1136);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, offsetRow);
			HalconAPI.Store(proc, 1, offsetCol);
			HalconAPI.Store(proc, 2, row1);
			HalconAPI.Store(proc, 3, col1);
			HalconAPI.Store(proc, 4, row2);
			HalconAPI.Store(proc, 5, col2);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(offsetRow);
			HalconAPI.UnpinTuple(offsetCol);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(col1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(col2);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage TileImagesOffset(int offsetRow, int offsetCol, int row1, int col1, int row2, int col2, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1136);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, offsetRow);
			HalconAPI.StoreI(proc, 1, offsetCol);
			HalconAPI.StoreI(proc, 2, row1);
			HalconAPI.StoreI(proc, 3, col1);
			HalconAPI.StoreI(proc, 4, row2);
			HalconAPI.StoreI(proc, 5, col2);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage TileImages(int numColumns, string tileOrder)
		{
			IntPtr proc = HalconAPI.PreCall(1137);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numColumns);
			HalconAPI.StoreS(proc, 1, tileOrder);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage TileChannels(int numColumns, string tileOrder)
		{
			IntPtr proc = HalconAPI.PreCall(1138);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numColumns);
			HalconAPI.StoreS(proc, 1, tileOrder);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage CropDomain()
		{
			IntPtr proc = HalconAPI.PreCall(1139);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage CropRectangle1(HTuple row1, HTuple column1, HTuple row2, HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1140);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage CropRectangle1(int row1, int column1, int row2, int column2)
		{
			IntPtr proc = HalconAPI.PreCall(1140);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row1);
			HalconAPI.StoreI(proc, 1, column1);
			HalconAPI.StoreI(proc, 2, row2);
			HalconAPI.StoreI(proc, 3, column2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage CropPart(HTuple row, HTuple column, HTuple width, HTuple height)
		{
			IntPtr proc = HalconAPI.PreCall(1141);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, width);
			HalconAPI.Store(proc, 3, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(width);
			HalconAPI.UnpinTuple(height);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage CropPart(int row, int column, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1141);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ChangeFormat(int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1142);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ChangeDomain(HRegion newDomain)
		{
			IntPtr proc = HalconAPI.PreCall(1143);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, newDomain);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(newDomain);
			return result;
		}

		public HImage Rectangle1Domain(int row1, int column1, int row2, int column2)
		{
			IntPtr proc = HalconAPI.PreCall(1145);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row1);
			HalconAPI.StoreI(proc, 1, column1);
			HalconAPI.StoreI(proc, 2, row2);
			HalconAPI.StoreI(proc, 3, column2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ReduceDomain(HRegion region)
		{
			IntPtr proc = HalconAPI.PreCall(1146);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HImage FullDomain()
		{
			IntPtr proc = HalconAPI.PreCall(1147);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion GetDomain()
		{
			IntPtr proc = HalconAPI.PreCall(1148);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage HoughLinesDir(out HRegion lines, int directionUncertainty, int angleResolution, string smoothing, int filterSize, int threshold, int angleGap, int distGap, string genLines, out HTuple angle, out HTuple dist)
		{
			IntPtr proc = HalconAPI.PreCall(1151);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, directionUncertainty);
			HalconAPI.StoreI(proc, 1, angleResolution);
			HalconAPI.StoreS(proc, 2, smoothing);
			HalconAPI.StoreI(proc, 3, filterSize);
			HalconAPI.StoreI(proc, 4, threshold);
			HalconAPI.StoreI(proc, 5, angleGap);
			HalconAPI.StoreI(proc, 6, distGap);
			HalconAPI.StoreS(proc, 7, genLines);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HRegion.LoadNew(proc, 2, err, out lines);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out dist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage HoughLineTransDir(int directionUncertainty, int angleResolution)
		{
			IntPtr proc = HalconAPI.PreCall(1152);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, directionUncertainty);
			HalconAPI.StoreI(proc, 1, angleResolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion FindRectificationGrid(HTuple minContrast, HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(1156);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, minContrast);
			HalconAPI.Store(proc, 1, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(radius);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion FindRectificationGrid(double minContrast, double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1156);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, minContrast);
			HalconAPI.StoreD(proc, 1, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLD ConnectGridPoints(HTuple row, HTuple column, HTuple sigma, HTuple maxDist)
		{
			IntPtr proc = HalconAPI.PreCall(1158);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, sigma);
			HalconAPI.Store(proc, 3, maxDist);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(sigma);
			HalconAPI.UnpinTuple(maxDist);
			HXLD result = null;
			err = HXLD.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLD ConnectGridPoints(HTuple row, HTuple column, int sigma, double maxDist)
		{
			IntPtr proc = HalconAPI.PreCall(1158);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreI(proc, 2, sigma);
			HalconAPI.StoreD(proc, 3, maxDist);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HXLD result = null;
			err = HXLD.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenGridRectificationMap(HXLD connectingLines, out HXLD meshes, int gridSpacing, HTuple rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, connectingLines);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.Store(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HXLD.LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(connectingLines);
			return result;
		}

		public HImage GenGridRectificationMap(HXLD connectingLines, out HXLD meshes, int gridSpacing, string rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, connectingLines);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.StoreS(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HXLD.LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(connectingLines);
			return result;
		}

		public void UnprojectCoordinates(HWindow windowHandle, HTuple row, HTuple column, out int imageRow, out int imageColumn, out HTuple height)
		{
			IntPtr proc = HalconAPI.PreCall(1168);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HalconAPI.LoadI(proc, 0, err, out imageRow);
			err = HalconAPI.LoadI(proc, 1, err, out imageColumn);
			err = HTuple.LoadNew(proc, 2, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void UnprojectCoordinates(HWindow windowHandle, double row, double column, out int imageRow, out int imageColumn, out int height)
		{
			IntPtr proc = HalconAPI.PreCall(1168);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out imageRow);
			err = HalconAPI.LoadI(proc, 1, err, out imageColumn);
			err = HalconAPI.LoadI(proc, 2, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DumpWindowImage(HWindow windowHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1184);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DispImage(HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1268);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DispChannel(HWindow windowHandle, HTuple channel)
		{
			IntPtr proc = HalconAPI.PreCall(1269);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, channel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(channel);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DispChannel(HWindow windowHandle, int channel)
		{
			IntPtr proc = HalconAPI.PreCall(1269);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, channel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DispColor(HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1270);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void GnuplotPlotImage(HGnuplot gnuplotFileID, int samplesX, int samplesY, HTuple viewRotX, HTuple viewRotZ, string hidden3D)
		{
			IntPtr proc = HalconAPI.PreCall(1297);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, gnuplotFileID);
			HalconAPI.StoreI(proc, 1, samplesX);
			HalconAPI.StoreI(proc, 2, samplesY);
			HalconAPI.Store(proc, 3, viewRotX);
			HalconAPI.Store(proc, 4, viewRotZ);
			HalconAPI.StoreS(proc, 5, hidden3D);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(viewRotX);
			HalconAPI.UnpinTuple(viewRotZ);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(gnuplotFileID);
		}

		public void GnuplotPlotImage(HGnuplot gnuplotFileID, int samplesX, int samplesY, double viewRotX, double viewRotZ, string hidden3D)
		{
			IntPtr proc = HalconAPI.PreCall(1297);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, gnuplotFileID);
			HalconAPI.StoreI(proc, 1, samplesX);
			HalconAPI.StoreI(proc, 2, samplesY);
			HalconAPI.StoreD(proc, 3, viewRotX);
			HalconAPI.StoreD(proc, 4, viewRotZ);
			HalconAPI.StoreS(proc, 5, hidden3D);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(gnuplotFileID);
		}

		public HImage TextureLaws(string filterTypes, int shift, int filterSize)
		{
			IntPtr proc = HalconAPI.PreCall(1402);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filterTypes);
			HalconAPI.StoreI(proc, 1, shift);
			HalconAPI.StoreI(proc, 2, filterSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage DeviationImage(int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1403);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage EntropyImage(int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1404);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage IsotropicDiffusion(double sigma, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(1405);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage AnisotropicDiffusion(string mode, double contrast, double theta, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(1406);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, contrast);
			HalconAPI.StoreD(proc, 2, theta);
			HalconAPI.StoreI(proc, 3, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SmoothImage(string filter, double alpha)
		{
			IntPtr proc = HalconAPI.PreCall(1407);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SigmaImage(int maskHeight, int maskWidth, int sigma)
		{
			IntPtr proc = HalconAPI.PreCall(1408);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskHeight);
			HalconAPI.StoreI(proc, 1, maskWidth);
			HalconAPI.StoreI(proc, 2, sigma);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MidrangeImage(HRegion mask, HTuple margin)
		{
			IntPtr proc = HalconAPI.PreCall(1409);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, mask);
			HalconAPI.Store(proc, 0, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(margin);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(mask);
			return result;
		}

		public HImage MidrangeImage(HRegion mask, string margin)
		{
			IntPtr proc = HalconAPI.PreCall(1409);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, mask);
			HalconAPI.StoreS(proc, 0, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(mask);
			return result;
		}

		public HImage TrimmedMean(HRegion mask, int number, HTuple margin)
		{
			IntPtr proc = HalconAPI.PreCall(1410);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, mask);
			HalconAPI.StoreI(proc, 0, number);
			HalconAPI.Store(proc, 1, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(margin);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(mask);
			return result;
		}

		public HImage TrimmedMean(HRegion mask, int number, string margin)
		{
			IntPtr proc = HalconAPI.PreCall(1410);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, mask);
			HalconAPI.StoreI(proc, 0, number);
			HalconAPI.StoreS(proc, 1, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(mask);
			return result;
		}

		public HImage MedianSeparate(int maskWidth, int maskHeight, HTuple margin)
		{
			IntPtr proc = HalconAPI.PreCall(1411);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.Store(proc, 2, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(margin);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MedianSeparate(int maskWidth, int maskHeight, string margin)
		{
			IntPtr proc = HalconAPI.PreCall(1411);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.StoreS(proc, 2, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MedianRect(int maskWidth, int maskHeight)
		{
			IntPtr proc = HalconAPI.PreCall(1412);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MedianImage(string maskType, int radius, HTuple margin)
		{
			IntPtr proc = HalconAPI.PreCall(1413);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, maskType);
			HalconAPI.StoreI(proc, 1, radius);
			HalconAPI.Store(proc, 2, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(margin);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MedianImage(string maskType, int radius, string margin)
		{
			IntPtr proc = HalconAPI.PreCall(1413);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, maskType);
			HalconAPI.StoreI(proc, 1, radius);
			HalconAPI.StoreS(proc, 2, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MedianWeighted(string maskType, int maskSize)
		{
			IntPtr proc = HalconAPI.PreCall(1414);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, maskType);
			HalconAPI.StoreI(proc, 1, maskSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage RankRect(int maskWidth, int maskHeight, int rank)
		{
			IntPtr proc = HalconAPI.PreCall(1415);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.StoreI(proc, 2, rank);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage RankImage(HRegion mask, int rank, HTuple margin)
		{
			IntPtr proc = HalconAPI.PreCall(1416);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, mask);
			HalconAPI.StoreI(proc, 0, rank);
			HalconAPI.Store(proc, 1, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(margin);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(mask);
			return result;
		}

		public HImage RankImage(HRegion mask, int rank, string margin)
		{
			IntPtr proc = HalconAPI.PreCall(1416);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, mask);
			HalconAPI.StoreI(proc, 0, rank);
			HalconAPI.StoreS(proc, 1, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(mask);
			return result;
		}

		public HImage DualRank(string maskType, int radius, int modePercent, HTuple margin)
		{
			IntPtr proc = HalconAPI.PreCall(1417);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, maskType);
			HalconAPI.StoreI(proc, 1, radius);
			HalconAPI.StoreI(proc, 2, modePercent);
			HalconAPI.Store(proc, 3, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(margin);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage DualRank(string maskType, int radius, int modePercent, string margin)
		{
			IntPtr proc = HalconAPI.PreCall(1417);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, maskType);
			HalconAPI.StoreI(proc, 1, radius);
			HalconAPI.StoreI(proc, 2, modePercent);
			HalconAPI.StoreS(proc, 3, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MeanImage(int maskWidth, int maskHeight)
		{
			IntPtr proc = HalconAPI.PreCall(1418);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage BinomialFilter(int maskWidth, int maskHeight)
		{
			IntPtr proc = HalconAPI.PreCall(1420);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GaussImage(int size)
		{
			IntPtr proc = HalconAPI.PreCall(1421);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, size);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GaussFilter(int size)
		{
			IntPtr proc = HalconAPI.PreCall(1422);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, size);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage EliminateMinMax(int maskWidth, int maskHeight, double gap, int mode)
		{
			IntPtr proc = HalconAPI.PreCall(1423);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.StoreD(proc, 2, gap);
			HalconAPI.StoreI(proc, 3, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage FillInterlace(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1424);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage RankN(int rankIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1425);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, rankIndex);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MeanN()
		{
			IntPtr proc = HalconAPI.PreCall(1426);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage EliminateSp(int maskWidth, int maskHeight, int minThresh, int maxThresh)
		{
			IntPtr proc = HalconAPI.PreCall(1427);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.StoreI(proc, 2, minThresh);
			HalconAPI.StoreI(proc, 3, maxThresh);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MeanSp(int maskWidth, int maskHeight, int minThresh, int maxThresh)
		{
			IntPtr proc = HalconAPI.PreCall(1428);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.StoreI(proc, 2, minThresh);
			HalconAPI.StoreI(proc, 3, maxThresh);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void PointsSojka(int maskSize, HTuple sigmaW, HTuple sigmaD, HTuple minGrad, HTuple minApparentness, double minAngle, string subpix, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1429);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskSize);
			HalconAPI.Store(proc, 1, sigmaW);
			HalconAPI.Store(proc, 2, sigmaD);
			HalconAPI.Store(proc, 3, minGrad);
			HalconAPI.Store(proc, 4, minApparentness);
			HalconAPI.StoreD(proc, 5, minAngle);
			HalconAPI.StoreS(proc, 6, subpix);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(sigmaW);
			HalconAPI.UnpinTuple(sigmaD);
			HalconAPI.UnpinTuple(minGrad);
			HalconAPI.UnpinTuple(minApparentness);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void PointsSojka(int maskSize, double sigmaW, double sigmaD, double minGrad, double minApparentness, double minAngle, string subpix, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1429);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskSize);
			HalconAPI.StoreD(proc, 1, sigmaW);
			HalconAPI.StoreD(proc, 2, sigmaD);
			HalconAPI.StoreD(proc, 3, minGrad);
			HalconAPI.StoreD(proc, 4, minApparentness);
			HalconAPI.StoreD(proc, 5, minAngle);
			HalconAPI.StoreS(proc, 6, subpix);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage DotsImage(int diameter, string filterType, int pixelShift)
		{
			IntPtr proc = HalconAPI.PreCall(1430);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, diameter);
			HalconAPI.StoreS(proc, 1, filterType);
			HalconAPI.StoreI(proc, 2, pixelShift);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void LocalMinSubPix(string filter, double sigma, double threshold, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1431);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void LocalMaxSubPix(string filter, double sigma, double threshold, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1432);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SaddlePointsSubPix(string filter, double sigma, double threshold, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1433);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CriticalPointsSubPix(string filter, double sigma, double threshold, out HTuple rowMin, out HTuple columnMin, out HTuple rowMax, out HTuple columnMax, out HTuple rowSaddle, out HTuple columnSaddle)
		{
			IntPtr proc = HalconAPI.PreCall(1434);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnMin);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out rowMax);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out columnMax);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out rowSaddle);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out columnSaddle);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void PointsHarris(double sigmaGrad, double sigmaSmooth, double alpha, HTuple threshold, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1435);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigmaGrad);
			HalconAPI.StoreD(proc, 1, sigmaSmooth);
			HalconAPI.StoreD(proc, 2, alpha);
			HalconAPI.Store(proc, 3, threshold);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(threshold);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void PointsHarris(double sigmaGrad, double sigmaSmooth, double alpha, double threshold, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1435);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigmaGrad);
			HalconAPI.StoreD(proc, 1, sigmaSmooth);
			HalconAPI.StoreD(proc, 2, alpha);
			HalconAPI.StoreD(proc, 3, threshold);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void PointsHarrisBinomial(int maskSizeGrad, int maskSizeSmooth, double alpha, HTuple threshold, string subpix, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1436);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskSizeGrad);
			HalconAPI.StoreI(proc, 1, maskSizeSmooth);
			HalconAPI.StoreD(proc, 2, alpha);
			HalconAPI.Store(proc, 3, threshold);
			HalconAPI.StoreS(proc, 4, subpix);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(threshold);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void PointsHarrisBinomial(int maskSizeGrad, int maskSizeSmooth, double alpha, double threshold, string subpix, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1436);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskSizeGrad);
			HalconAPI.StoreI(proc, 1, maskSizeSmooth);
			HalconAPI.StoreD(proc, 2, alpha);
			HalconAPI.StoreD(proc, 3, threshold);
			HalconAPI.StoreS(proc, 4, subpix);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void PointsLepetit(int radius, int checkNeighbor, int minCheckNeighborDiff, int minScore, string subpix, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1437);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, radius);
			HalconAPI.StoreI(proc, 1, checkNeighbor);
			HalconAPI.StoreI(proc, 2, minCheckNeighborDiff);
			HalconAPI.StoreI(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, subpix);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out row);
			err = HTuple.LoadNew(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void PointsFoerstner(HTuple sigmaGrad, HTuple sigmaInt, HTuple sigmaPoints, HTuple threshInhom, double threshShape, string smoothing, string eliminateDoublets, out HTuple rowJunctions, out HTuple columnJunctions, out HTuple coRRJunctions, out HTuple coRCJunctions, out HTuple coCCJunctions, out HTuple rowArea, out HTuple columnArea, out HTuple coRRArea, out HTuple coRCArea, out HTuple coCCArea)
		{
			IntPtr proc = HalconAPI.PreCall(1438);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sigmaGrad);
			HalconAPI.Store(proc, 1, sigmaInt);
			HalconAPI.Store(proc, 2, sigmaPoints);
			HalconAPI.Store(proc, 3, threshInhom);
			HalconAPI.StoreD(proc, 4, threshShape);
			HalconAPI.StoreS(proc, 5, smoothing);
			HalconAPI.StoreS(proc, 6, eliminateDoublets);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(sigmaGrad);
			HalconAPI.UnpinTuple(sigmaInt);
			HalconAPI.UnpinTuple(sigmaPoints);
			HalconAPI.UnpinTuple(threshInhom);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowJunctions);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnJunctions);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out coRRJunctions);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out coRCJunctions);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out coCCJunctions);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out rowArea);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out columnArea);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out coRRArea);
			err = HTuple.LoadNew(proc, 8, HTupleType.DOUBLE, err, out coRCArea);
			err = HTuple.LoadNew(proc, 9, HTupleType.DOUBLE, err, out coCCArea);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void PointsFoerstner(double sigmaGrad, double sigmaInt, double sigmaPoints, double threshInhom, double threshShape, string smoothing, string eliminateDoublets, out HTuple rowJunctions, out HTuple columnJunctions, out HTuple coRRJunctions, out HTuple coRCJunctions, out HTuple coCCJunctions, out HTuple rowArea, out HTuple columnArea, out HTuple coRRArea, out HTuple coRCArea, out HTuple coCCArea)
		{
			IntPtr proc = HalconAPI.PreCall(1438);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigmaGrad);
			HalconAPI.StoreD(proc, 1, sigmaInt);
			HalconAPI.StoreD(proc, 2, sigmaPoints);
			HalconAPI.StoreD(proc, 3, threshInhom);
			HalconAPI.StoreD(proc, 4, threshShape);
			HalconAPI.StoreS(proc, 5, smoothing);
			HalconAPI.StoreS(proc, 6, eliminateDoublets);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowJunctions);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnJunctions);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out coRRJunctions);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out coRCJunctions);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out coCCJunctions);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out rowArea);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out columnArea);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out coRRArea);
			err = HTuple.LoadNew(proc, 8, HTupleType.DOUBLE, err, out coRCArea);
			err = HTuple.LoadNew(proc, 9, HTupleType.DOUBLE, err, out coCCArea);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple EstimateNoise(string method, HTuple percent)
		{
			IntPtr proc = HalconAPI.PreCall(1439);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.Store(proc, 1, percent);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(percent);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double EstimateNoise(string method, double percent)
		{
			IntPtr proc = HalconAPI.PreCall(1439);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.StoreD(proc, 1, percent);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple NoiseDistributionMean(HRegion constRegion, int filterSize)
		{
			IntPtr proc = HalconAPI.PreCall(1440);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, constRegion);
			HalconAPI.StoreI(proc, 0, filterSize);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(constRegion);
			return result;
		}

		public HImage AddNoiseWhite(double amp)
		{
			IntPtr proc = HalconAPI.PreCall(1441);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, amp);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage AddNoiseDistribution(HTuple distribution)
		{
			IntPtr proc = HalconAPI.PreCall(1442);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, distribution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(distribution);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage DeviationN()
		{
			IntPtr proc = HalconAPI.PreCall(1445);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage InpaintingTexture(HRegion region, int maskSize, int searchSize, double anisotropy, string postIteration, double smoothness)
		{
			IntPtr proc = HalconAPI.PreCall(1446);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.StoreI(proc, 0, maskSize);
			HalconAPI.StoreI(proc, 1, searchSize);
			HalconAPI.StoreD(proc, 2, anisotropy);
			HalconAPI.StoreS(proc, 3, postIteration);
			HalconAPI.StoreD(proc, 4, smoothness);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HImage InpaintingCt(HRegion region, double epsilon, double kappa, double sigma, double rho, HTuple channelCoefficients)
		{
			IntPtr proc = HalconAPI.PreCall(1447);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.StoreD(proc, 0, epsilon);
			HalconAPI.StoreD(proc, 1, kappa);
			HalconAPI.StoreD(proc, 2, sigma);
			HalconAPI.StoreD(proc, 3, rho);
			HalconAPI.Store(proc, 4, channelCoefficients);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(channelCoefficients);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HImage InpaintingCt(HRegion region, double epsilon, double kappa, double sigma, double rho, double channelCoefficients)
		{
			IntPtr proc = HalconAPI.PreCall(1447);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.StoreD(proc, 0, epsilon);
			HalconAPI.StoreD(proc, 1, kappa);
			HalconAPI.StoreD(proc, 2, sigma);
			HalconAPI.StoreD(proc, 3, rho);
			HalconAPI.StoreD(proc, 4, channelCoefficients);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HImage InpaintingMcf(HRegion region, double sigma, double theta, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(1448);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreD(proc, 1, theta);
			HalconAPI.StoreI(proc, 2, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HImage InpaintingCed(HRegion region, double sigma, double rho, double theta, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(1449);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreD(proc, 1, rho);
			HalconAPI.StoreD(proc, 2, theta);
			HalconAPI.StoreI(proc, 3, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HImage InpaintingAniso(HRegion region, string mode, double contrast, double theta, int iterations, double rho)
		{
			IntPtr proc = HalconAPI.PreCall(1450);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, contrast);
			HalconAPI.StoreD(proc, 2, theta);
			HalconAPI.StoreI(proc, 3, iterations);
			HalconAPI.StoreD(proc, 4, rho);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HImage HarmonicInterpolation(HRegion region, double precision)
		{
			IntPtr proc = HalconAPI.PreCall(1451);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.StoreD(proc, 0, precision);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HImage ExpandDomainGray(int expansionRange)
		{
			IntPtr proc = HalconAPI.PreCall(1452);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, expansionRange);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage TopographicSketch()
		{
			IntPtr proc = HalconAPI.PreCall(1453);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage LinearTransColor(HTuple transMat)
		{
			IntPtr proc = HalconAPI.PreCall(1454);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, transMat);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(transMat);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GenPrincipalCompTrans(out HTuple transInv, out HTuple mean, out HTuple cov, out HTuple infoPerComp)
		{
			IntPtr proc = HalconAPI.PreCall(1455);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out transInv);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out mean);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out cov);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out infoPerComp);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PrincipalComp(out HTuple infoPerComp)
		{
			IntPtr proc = HalconAPI.PreCall(1456);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out infoPerComp);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple FuzzyEntropy(HRegion regions, int apar, int cpar)
		{
			IntPtr proc = HalconAPI.PreCall(1457);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreI(proc, 0, apar);
			HalconAPI.StoreI(proc, 1, cpar);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HTuple FuzzyPerimeter(HRegion regions, int apar, int cpar)
		{
			IntPtr proc = HalconAPI.PreCall(1458);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreI(proc, 0, apar);
			HalconAPI.StoreI(proc, 1, cpar);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HImage GrayClosingShape(HTuple maskHeight, HTuple maskWidth, string maskShape)
		{
			IntPtr proc = HalconAPI.PreCall(1459);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, maskHeight);
			HalconAPI.Store(proc, 1, maskWidth);
			HalconAPI.StoreS(proc, 2, maskShape);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maskHeight);
			HalconAPI.UnpinTuple(maskWidth);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayClosingShape(double maskHeight, double maskWidth, string maskShape)
		{
			IntPtr proc = HalconAPI.PreCall(1459);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maskHeight);
			HalconAPI.StoreD(proc, 1, maskWidth);
			HalconAPI.StoreS(proc, 2, maskShape);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayOpeningShape(HTuple maskHeight, HTuple maskWidth, string maskShape)
		{
			IntPtr proc = HalconAPI.PreCall(1460);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, maskHeight);
			HalconAPI.Store(proc, 1, maskWidth);
			HalconAPI.StoreS(proc, 2, maskShape);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maskHeight);
			HalconAPI.UnpinTuple(maskWidth);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayOpeningShape(double maskHeight, double maskWidth, string maskShape)
		{
			IntPtr proc = HalconAPI.PreCall(1460);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maskHeight);
			HalconAPI.StoreD(proc, 1, maskWidth);
			HalconAPI.StoreS(proc, 2, maskShape);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayErosionShape(HTuple maskHeight, HTuple maskWidth, string maskShape)
		{
			IntPtr proc = HalconAPI.PreCall(1461);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, maskHeight);
			HalconAPI.Store(proc, 1, maskWidth);
			HalconAPI.StoreS(proc, 2, maskShape);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maskHeight);
			HalconAPI.UnpinTuple(maskWidth);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayErosionShape(double maskHeight, double maskWidth, string maskShape)
		{
			IntPtr proc = HalconAPI.PreCall(1461);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maskHeight);
			HalconAPI.StoreD(proc, 1, maskWidth);
			HalconAPI.StoreS(proc, 2, maskShape);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayDilationShape(HTuple maskHeight, HTuple maskWidth, string maskShape)
		{
			IntPtr proc = HalconAPI.PreCall(1462);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, maskHeight);
			HalconAPI.Store(proc, 1, maskWidth);
			HalconAPI.StoreS(proc, 2, maskShape);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maskHeight);
			HalconAPI.UnpinTuple(maskWidth);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayDilationShape(double maskHeight, double maskWidth, string maskShape)
		{
			IntPtr proc = HalconAPI.PreCall(1462);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maskHeight);
			HalconAPI.StoreD(proc, 1, maskWidth);
			HalconAPI.StoreS(proc, 2, maskShape);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayRangeRect(int maskHeight, int maskWidth)
		{
			IntPtr proc = HalconAPI.PreCall(1463);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskHeight);
			HalconAPI.StoreI(proc, 1, maskWidth);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayClosingRect(int maskHeight, int maskWidth)
		{
			IntPtr proc = HalconAPI.PreCall(1464);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskHeight);
			HalconAPI.StoreI(proc, 1, maskWidth);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayOpeningRect(int maskHeight, int maskWidth)
		{
			IntPtr proc = HalconAPI.PreCall(1465);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskHeight);
			HalconAPI.StoreI(proc, 1, maskWidth);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayErosionRect(int maskHeight, int maskWidth)
		{
			IntPtr proc = HalconAPI.PreCall(1466);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskHeight);
			HalconAPI.StoreI(proc, 1, maskWidth);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GrayDilationRect(int maskHeight, int maskWidth)
		{
			IntPtr proc = HalconAPI.PreCall(1467);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskHeight);
			HalconAPI.StoreI(proc, 1, maskWidth);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GraySkeleton()
		{
			IntPtr proc = HalconAPI.PreCall(1468);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage LutTrans(HTuple lut)
		{
			IntPtr proc = HalconAPI.PreCall(1469);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, lut);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(lut);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ConvolImage(HTuple filterMask, HTuple margin)
		{
			IntPtr proc = HalconAPI.PreCall(1470);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, filterMask);
			HalconAPI.Store(proc, 1, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(filterMask);
			HalconAPI.UnpinTuple(margin);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ConvolImage(string filterMask, string margin)
		{
			IntPtr proc = HalconAPI.PreCall(1470);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filterMask);
			HalconAPI.StoreS(proc, 1, margin);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ConvertImageType(string newType)
		{
			IntPtr proc = HalconAPI.PreCall(1471);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, newType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage RealToVectorField(HImage col, string type)
		{
			IntPtr proc = HalconAPI.PreCall(1472);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, col);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(col);
			return result;
		}

		public HImage VectorFieldToReal(out HImage col)
		{
			IntPtr proc = HalconAPI.PreCall(1473);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out col);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage RealToComplex(HImage imageImaginary)
		{
			IntPtr proc = HalconAPI.PreCall(1474);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageImaginary);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageImaginary);
			return result;
		}

		public HImage ComplexToReal(out HImage imageImaginary)
		{
			IntPtr proc = HalconAPI.PreCall(1475);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imageImaginary);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage RegionToMean(HRegion regions)
		{
			IntPtr proc = HalconAPI.PreCall(1476);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HImage GrayInside()
		{
			IntPtr proc = HalconAPI.PreCall(1477);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Symmetry(int maskSize, double direction, double exponent)
		{
			IntPtr proc = HalconAPI.PreCall(1478);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskSize);
			HalconAPI.StoreD(proc, 1, direction);
			HalconAPI.StoreD(proc, 2, exponent);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SelectGrayvaluesFromChannels(HImage indexImage)
		{
			IntPtr proc = HalconAPI.PreCall(1479);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, indexImage);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(indexImage);
			return result;
		}

		public HImage DepthFromFocus(out HImage confidence, HTuple filter, HTuple selection)
		{
			IntPtr proc = HalconAPI.PreCall(1480);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, filter);
			HalconAPI.Store(proc, 1, selection);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(filter);
			HalconAPI.UnpinTuple(selection);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage DepthFromFocus(out HImage confidence, string filter, string selection)
		{
			IntPtr proc = HalconAPI.PreCall(1480);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreS(proc, 1, selection);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SceneFlowUncalib(HImage imageRect2T1, HImage imageRect1T2, HImage imageRect2T2, HImage disparity, out HImage disparityChange, HTuple smoothingFlow, HTuple smoothingDisparity, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1482);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2T1);
			HalconAPI.Store(proc, 3, imageRect1T2);
			HalconAPI.Store(proc, 4, imageRect2T2);
			HalconAPI.Store(proc, 5, disparity);
			HalconAPI.Store(proc, 0, smoothingFlow);
			HalconAPI.Store(proc, 1, smoothingDisparity);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(smoothingFlow);
			HalconAPI.UnpinTuple(smoothingDisparity);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out disparityChange);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2T1);
			GC.KeepAlive(imageRect1T2);
			GC.KeepAlive(imageRect2T2);
			GC.KeepAlive(disparity);
			return result;
		}

		public HImage SceneFlowUncalib(HImage imageRect2T1, HImage imageRect1T2, HImage imageRect2T2, HImage disparity, out HImage disparityChange, double smoothingFlow, double smoothingDisparity, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1482);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageRect2T1);
			HalconAPI.Store(proc, 3, imageRect1T2);
			HalconAPI.Store(proc, 4, imageRect2T2);
			HalconAPI.Store(proc, 5, disparity);
			HalconAPI.StoreD(proc, 0, smoothingFlow);
			HalconAPI.StoreD(proc, 1, smoothingDisparity);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out disparityChange);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect2T1);
			GC.KeepAlive(imageRect1T2);
			GC.KeepAlive(imageRect2T2);
			GC.KeepAlive(disparity);
			return result;
		}

		public HImage UnwarpImageVectorField(HImage vectorField)
		{
			IntPtr proc = HalconAPI.PreCall(1483);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, vectorField);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(vectorField);
			return result;
		}

		public HImage DerivateVectorField(HTuple sigma, string component)
		{
			IntPtr proc = HalconAPI.PreCall(1484);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sigma);
			HalconAPI.StoreS(proc, 1, component);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(sigma);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage DerivateVectorField(double sigma, string component)
		{
			IntPtr proc = HalconAPI.PreCall(1484);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreS(proc, 1, component);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage VectorFieldLength(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1485);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage OpticalFlowMg(HImage imageT2, string algorithm, double smoothingSigma, double integrationSigma, double flowSmoothness, double gradientConstancy, HTuple MGParamName, HTuple MGParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1486);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageT2);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreD(proc, 1, smoothingSigma);
			HalconAPI.StoreD(proc, 2, integrationSigma);
			HalconAPI.StoreD(proc, 3, flowSmoothness);
			HalconAPI.StoreD(proc, 4, gradientConstancy);
			HalconAPI.Store(proc, 5, MGParamName);
			HalconAPI.Store(proc, 6, MGParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(MGParamName);
			HalconAPI.UnpinTuple(MGParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageT2);
			return result;
		}

		public HImage OpticalFlowMg(HImage imageT2, string algorithm, double smoothingSigma, double integrationSigma, double flowSmoothness, double gradientConstancy, string MGParamName, string MGParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1486);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageT2);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreD(proc, 1, smoothingSigma);
			HalconAPI.StoreD(proc, 2, integrationSigma);
			HalconAPI.StoreD(proc, 3, flowSmoothness);
			HalconAPI.StoreD(proc, 4, gradientConstancy);
			HalconAPI.StoreS(proc, 5, MGParamName);
			HalconAPI.StoreS(proc, 6, MGParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageT2);
			return result;
		}

		public HImage ExhaustiveMatchMg(HImage imageTemplate, string mode, int level, int threshold)
		{
			IntPtr proc = HalconAPI.PreCall(1487);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageTemplate);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, level);
			HalconAPI.StoreI(proc, 2, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageTemplate);
			return result;
		}

		public HTemplate CreateTemplateRot(int numLevel, double angleStart, double angleExtend, double angleStep, string optimize, string grayValues)
		{
			IntPtr proc = HalconAPI.PreCall(1488);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevel);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, optimize);
			HalconAPI.StoreS(proc, 5, grayValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTemplate result = null;
			err = HTemplate.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTemplate CreateTemplate(int firstError, int numLevel, string optimize, string grayValues)
		{
			IntPtr proc = HalconAPI.PreCall(1489);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, firstError);
			HalconAPI.StoreI(proc, 1, numLevel);
			HalconAPI.StoreS(proc, 2, optimize);
			HalconAPI.StoreS(proc, 3, grayValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTemplate result = null;
			err = HTemplate.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AdaptTemplate(HTemplate templateID)
		{
			IntPtr proc = HalconAPI.PreCall(1498);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public HRegion FastMatchMg(HTemplate templateID, double maxError, HTuple numLevel)
		{
			IntPtr proc = HalconAPI.PreCall(1499);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.Store(proc, 2, numLevel);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevel);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
			return result;
		}

		public HRegion FastMatchMg(HTemplate templateID, double maxError, int numLevel)
		{
			IntPtr proc = HalconAPI.PreCall(1499);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreI(proc, 2, numLevel);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
			return result;
		}

		public void BestMatchPreMg(HTemplate templateID, double maxError, string subPixel, int numLevels, HTuple whichLevels, out double row, out double column, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1500);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.StoreI(proc, 3, numLevels);
			HalconAPI.Store(proc, 4, whichLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(whichLevels);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public void BestMatchPreMg(HTemplate templateID, double maxError, string subPixel, int numLevels, int whichLevels, out double row, out double column, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1500);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.StoreI(proc, 3, numLevels);
			HalconAPI.StoreI(proc, 4, whichLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public void BestMatchMg(HTemplate templateID, double maxError, string subPixel, int numLevels, HTuple whichLevels, out double row, out double column, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1501);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.StoreI(proc, 3, numLevels);
			HalconAPI.Store(proc, 4, whichLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(whichLevels);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public void BestMatchMg(HTemplate templateID, double maxError, string subPixel, int numLevels, int whichLevels, out double row, out double column, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1501);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.StoreI(proc, 3, numLevels);
			HalconAPI.StoreI(proc, 4, whichLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public HRegion FastMatch(HTemplate templateID, double maxError)
		{
			IntPtr proc = HalconAPI.PreCall(1502);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
			return result;
		}

		public void BestMatchRotMg(HTemplate templateID, double angleStart, double angleExtend, double maxError, string subPixel, int numLevels, out HTuple row, out HTuple column, out HTuple angle, out HTuple error)
		{
			IntPtr proc = HalconAPI.PreCall(1503);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, maxError);
			HalconAPI.StoreS(proc, 4, subPixel);
			HalconAPI.StoreI(proc, 5, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public void BestMatchRotMg(HTemplate templateID, double angleStart, double angleExtend, double maxError, string subPixel, int numLevels, out double row, out double column, out double angle, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1503);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, maxError);
			HalconAPI.StoreS(proc, 4, subPixel);
			HalconAPI.StoreI(proc, 5, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out angle);
			err = HalconAPI.LoadD(proc, 3, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public void BestMatchRot(HTemplate templateID, double angleStart, double angleExtend, double maxError, string subPixel, out HTuple row, out HTuple column, out HTuple angle, out HTuple error)
		{
			IntPtr proc = HalconAPI.PreCall(1504);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, maxError);
			HalconAPI.StoreS(proc, 4, subPixel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public void BestMatchRot(HTemplate templateID, double angleStart, double angleExtend, double maxError, string subPixel, out double row, out double column, out double angle, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1504);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, maxError);
			HalconAPI.StoreS(proc, 4, subPixel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out angle);
			err = HalconAPI.LoadD(proc, 3, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public void BestMatch(HTemplate templateID, double maxError, string subPixel, out HTuple row, out HTuple column, out HTuple error)
		{
			IntPtr proc = HalconAPI.PreCall(1505);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public void BestMatch(HTemplate templateID, double maxError, string subPixel, out double row, out double column, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1505);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, templateID);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(templateID);
		}

		public HImage ExhaustiveMatch(HRegion regionOfInterest, HImage imageTemplate, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1506);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, regionOfInterest);
			HalconAPI.Store(proc, 3, imageTemplate);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regionOfInterest);
			GC.KeepAlive(imageTemplate);
			return result;
		}

		public HImage CornerResponse(int size, double weight)
		{
			IntPtr proc = HalconAPI.PreCall(1507);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, size);
			HalconAPI.StoreD(proc, 1, weight);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenGaussPyramid(string mode, double scale)
		{
			IntPtr proc = HalconAPI.PreCall(1508);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, scale);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Monotony()
		{
			IntPtr proc = HalconAPI.PreCall(1509);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage BandpassImage(string filterType)
		{
			IntPtr proc = HalconAPI.PreCall(1510);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filterType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont LinesColor(HTuple sigma, HTuple low, HTuple high, string extractWidth, string completeJunctions)
		{
			IntPtr proc = HalconAPI.PreCall(1511);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sigma);
			HalconAPI.Store(proc, 1, low);
			HalconAPI.Store(proc, 2, high);
			HalconAPI.StoreS(proc, 3, extractWidth);
			HalconAPI.StoreS(proc, 4, completeJunctions);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(sigma);
			HalconAPI.UnpinTuple(low);
			HalconAPI.UnpinTuple(high);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont LinesColor(double sigma, double low, double high, string extractWidth, string completeJunctions)
		{
			IntPtr proc = HalconAPI.PreCall(1511);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreD(proc, 1, low);
			HalconAPI.StoreD(proc, 2, high);
			HalconAPI.StoreS(proc, 3, extractWidth);
			HalconAPI.StoreS(proc, 4, completeJunctions);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont LinesGauss(HTuple sigma, HTuple low, HTuple high, string lightDark, string extractWidth, string lineModel, string completeJunctions)
		{
			IntPtr proc = HalconAPI.PreCall(1512);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sigma);
			HalconAPI.Store(proc, 1, low);
			HalconAPI.Store(proc, 2, high);
			HalconAPI.StoreS(proc, 3, lightDark);
			HalconAPI.StoreS(proc, 4, extractWidth);
			HalconAPI.StoreS(proc, 5, lineModel);
			HalconAPI.StoreS(proc, 6, completeJunctions);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(sigma);
			HalconAPI.UnpinTuple(low);
			HalconAPI.UnpinTuple(high);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont LinesGauss(double sigma, double low, double high, string lightDark, string extractWidth, string lineModel, string completeJunctions)
		{
			IntPtr proc = HalconAPI.PreCall(1512);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreD(proc, 1, low);
			HalconAPI.StoreD(proc, 2, high);
			HalconAPI.StoreS(proc, 3, lightDark);
			HalconAPI.StoreS(proc, 4, extractWidth);
			HalconAPI.StoreS(proc, 5, lineModel);
			HalconAPI.StoreS(proc, 6, completeJunctions);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont LinesFacet(int maskSize, HTuple low, HTuple high, string lightDark)
		{
			IntPtr proc = HalconAPI.PreCall(1513);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskSize);
			HalconAPI.Store(proc, 1, low);
			HalconAPI.Store(proc, 2, high);
			HalconAPI.StoreS(proc, 3, lightDark);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(low);
			HalconAPI.UnpinTuple(high);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont LinesFacet(int maskSize, double low, double high, string lightDark)
		{
			IntPtr proc = HalconAPI.PreCall(1513);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskSize);
			HalconAPI.StoreD(proc, 1, low);
			HalconAPI.StoreD(proc, 2, high);
			HalconAPI.StoreS(proc, 3, lightDark);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GenFilterMask(HTuple filterMask, double scale, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1514);
			HalconAPI.Store(proc, 0, filterMask);
			HalconAPI.StoreD(proc, 1, scale);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(filterMask);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenFilterMask(string filterMask, double scale, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1514);
			HalconAPI.StoreS(proc, 0, filterMask);
			HalconAPI.StoreD(proc, 1, scale);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenMeanFilter(string maskShape, double diameter1, double diameter2, double phi, string norm, string mode, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1515);
			HalconAPI.StoreS(proc, 0, maskShape);
			HalconAPI.StoreD(proc, 1, diameter1);
			HalconAPI.StoreD(proc, 2, diameter2);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreS(proc, 4, norm);
			HalconAPI.StoreS(proc, 5, mode);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenGaussFilter(double sigma1, double sigma2, double phi, string norm, string mode, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1516);
			HalconAPI.StoreD(proc, 0, sigma1);
			HalconAPI.StoreD(proc, 1, sigma2);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreS(proc, 3, norm);
			HalconAPI.StoreS(proc, 4, mode);
			HalconAPI.StoreI(proc, 5, width);
			HalconAPI.StoreI(proc, 6, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenDerivativeFilter(string derivative, int exponent, string norm, string mode, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1517);
			HalconAPI.StoreS(proc, 0, derivative);
			HalconAPI.StoreI(proc, 1, exponent);
			HalconAPI.StoreS(proc, 2, norm);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.StoreI(proc, 4, width);
			HalconAPI.StoreI(proc, 5, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenStdBandpass(double frequency, double sigma, string type, string norm, string mode, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1518);
			HalconAPI.StoreD(proc, 0, frequency);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreS(proc, 2, type);
			HalconAPI.StoreS(proc, 3, norm);
			HalconAPI.StoreS(proc, 4, mode);
			HalconAPI.StoreI(proc, 5, width);
			HalconAPI.StoreI(proc, 6, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenSinBandpass(double frequency, string norm, string mode, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1519);
			HalconAPI.StoreD(proc, 0, frequency);
			HalconAPI.StoreS(proc, 1, norm);
			HalconAPI.StoreS(proc, 2, mode);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenBandfilter(double minFrequency, double maxFrequency, string norm, string mode, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1520);
			HalconAPI.StoreD(proc, 0, minFrequency);
			HalconAPI.StoreD(proc, 1, maxFrequency);
			HalconAPI.StoreS(proc, 2, norm);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.StoreI(proc, 4, width);
			HalconAPI.StoreI(proc, 5, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenBandpass(double minFrequency, double maxFrequency, string norm, string mode, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1521);
			HalconAPI.StoreD(proc, 0, minFrequency);
			HalconAPI.StoreD(proc, 1, maxFrequency);
			HalconAPI.StoreS(proc, 2, norm);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.StoreI(proc, 4, width);
			HalconAPI.StoreI(proc, 5, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenLowpass(double frequency, string norm, string mode, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1522);
			HalconAPI.StoreD(proc, 0, frequency);
			HalconAPI.StoreS(proc, 1, norm);
			HalconAPI.StoreS(proc, 2, mode);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenHighpass(double frequency, string norm, string mode, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1523);
			HalconAPI.StoreD(proc, 0, frequency);
			HalconAPI.StoreS(proc, 1, norm);
			HalconAPI.StoreS(proc, 2, mode);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage PowerLn()
		{
			IntPtr proc = HalconAPI.PreCall(1524);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PowerReal()
		{
			IntPtr proc = HalconAPI.PreCall(1525);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PowerByte()
		{
			IntPtr proc = HalconAPI.PreCall(1526);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PhaseDeg()
		{
			IntPtr proc = HalconAPI.PreCall(1527);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PhaseRad()
		{
			IntPtr proc = HalconAPI.PreCall(1528);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage EnergyGabor(HImage imageHilbert)
		{
			IntPtr proc = HalconAPI.PreCall(1529);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageHilbert);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageHilbert);
			return result;
		}

		public HImage ConvolGabor(HImage gaborFilter, out HImage imageResultHilbert)
		{
			IntPtr proc = HalconAPI.PreCall(1530);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, gaborFilter);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imageResultHilbert);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(gaborFilter);
			return result;
		}

		public void GenGabor(double angle, double frequency, double bandwidth, double orientation, string norm, string mode, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1531);
			HalconAPI.StoreD(proc, 0, angle);
			HalconAPI.StoreD(proc, 1, frequency);
			HalconAPI.StoreD(proc, 2, bandwidth);
			HalconAPI.StoreD(proc, 3, orientation);
			HalconAPI.StoreS(proc, 4, norm);
			HalconAPI.StoreS(proc, 5, mode);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage PhaseCorrelationFft(HImage imageFFT2)
		{
			IntPtr proc = HalconAPI.PreCall(1532);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageFFT2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageFFT2);
			return result;
		}

		public HImage CorrelationFft(HImage imageFFT2)
		{
			IntPtr proc = HalconAPI.PreCall(1533);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageFFT2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageFFT2);
			return result;
		}

		public HImage ConvolFft(HImage imageFilter)
		{
			IntPtr proc = HalconAPI.PreCall(1534);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageFilter);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageFilter);
			return result;
		}

		public HImage RftGeneric(string direction, string norm, string resultType, int width)
		{
			IntPtr proc = HalconAPI.PreCall(1541);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, direction);
			HalconAPI.StoreS(proc, 1, norm);
			HalconAPI.StoreS(proc, 2, resultType);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage FftImageInv()
		{
			IntPtr proc = HalconAPI.PreCall(1542);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage FftImage()
		{
			IntPtr proc = HalconAPI.PreCall(1543);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage FftGeneric(string direction, int exponent, string norm, string mode, string resultType)
		{
			IntPtr proc = HalconAPI.PreCall(1544);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, direction);
			HalconAPI.StoreI(proc, 1, exponent);
			HalconAPI.StoreS(proc, 2, norm);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.StoreS(proc, 4, resultType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ShockFilter(double theta, int iterations, string mode, double sigma)
		{
			IntPtr proc = HalconAPI.PreCall(1545);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, theta);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreS(proc, 2, mode);
			HalconAPI.StoreD(proc, 3, sigma);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MeanCurvatureFlow(double sigma, double theta, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(1546);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreD(proc, 1, theta);
			HalconAPI.StoreI(proc, 2, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage CoherenceEnhancingDiff(double sigma, double rho, double theta, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(1547);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreD(proc, 1, rho);
			HalconAPI.StoreD(proc, 2, theta);
			HalconAPI.StoreI(proc, 3, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage EquHistoImage()
		{
			IntPtr proc = HalconAPI.PreCall(1548);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Illuminate(int maskWidth, int maskHeight, double factor)
		{
			IntPtr proc = HalconAPI.PreCall(1549);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.StoreD(proc, 2, factor);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Emphasize(int maskWidth, int maskHeight, double factor)
		{
			IntPtr proc = HalconAPI.PreCall(1550);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maskWidth);
			HalconAPI.StoreI(proc, 1, maskHeight);
			HalconAPI.StoreD(proc, 2, factor);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ScaleImageMax()
		{
			IntPtr proc = HalconAPI.PreCall(1551);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage RobinsonDir(out HImage imageEdgeDir)
		{
			IntPtr proc = HalconAPI.PreCall(1552);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imageEdgeDir);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage RobinsonAmp()
		{
			IntPtr proc = HalconAPI.PreCall(1553);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage KirschDir(out HImage imageEdgeDir)
		{
			IntPtr proc = HalconAPI.PreCall(1554);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imageEdgeDir);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage KirschAmp()
		{
			IntPtr proc = HalconAPI.PreCall(1555);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage FreiDir(out HImage imageEdgeDir)
		{
			IntPtr proc = HalconAPI.PreCall(1556);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imageEdgeDir);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage FreiAmp()
		{
			IntPtr proc = HalconAPI.PreCall(1557);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PrewittDir(out HImage imageEdgeDir)
		{
			IntPtr proc = HalconAPI.PreCall(1558);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imageEdgeDir);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PrewittAmp()
		{
			IntPtr proc = HalconAPI.PreCall(1559);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SobelAmp(string filterType, HTuple size)
		{
			IntPtr proc = HalconAPI.PreCall(1560);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filterType);
			HalconAPI.Store(proc, 1, size);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(size);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SobelAmp(string filterType, int size)
		{
			IntPtr proc = HalconAPI.PreCall(1560);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filterType);
			HalconAPI.StoreI(proc, 1, size);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SobelDir(out HImage edgeDirection, string filterType, HTuple size)
		{
			IntPtr proc = HalconAPI.PreCall(1561);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filterType);
			HalconAPI.Store(proc, 1, size);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(size);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out edgeDirection);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SobelDir(out HImage edgeDirection, string filterType, int size)
		{
			IntPtr proc = HalconAPI.PreCall(1561);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filterType);
			HalconAPI.StoreI(proc, 1, size);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out edgeDirection);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Roberts(string filterType)
		{
			IntPtr proc = HalconAPI.PreCall(1562);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filterType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Laplace(string resultType, HTuple maskSize, string filterMask)
		{
			IntPtr proc = HalconAPI.PreCall(1563);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, resultType);
			HalconAPI.Store(proc, 1, maskSize);
			HalconAPI.StoreS(proc, 2, filterMask);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maskSize);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Laplace(string resultType, int maskSize, string filterMask)
		{
			IntPtr proc = HalconAPI.PreCall(1563);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, resultType);
			HalconAPI.StoreI(proc, 1, maskSize);
			HalconAPI.StoreS(proc, 2, filterMask);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage HighpassImage(int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1564);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont EdgesColorSubPix(string filter, double alpha, HTuple low, HTuple high)
		{
			IntPtr proc = HalconAPI.PreCall(1566);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.Store(proc, 2, low);
			HalconAPI.Store(proc, 3, high);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(low);
			HalconAPI.UnpinTuple(high);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont EdgesColorSubPix(string filter, double alpha, double low, double high)
		{
			IntPtr proc = HalconAPI.PreCall(1566);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.StoreD(proc, 2, low);
			HalconAPI.StoreD(proc, 3, high);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage EdgesColor(out HImage imaDir, string filter, double alpha, string NMS, int low, int high)
		{
			IntPtr proc = HalconAPI.PreCall(1567);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.StoreS(proc, 2, NMS);
			HalconAPI.StoreI(proc, 3, low);
			HalconAPI.StoreI(proc, 4, high);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imaDir);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont EdgesSubPix(string filter, double alpha, HTuple low, HTuple high)
		{
			IntPtr proc = HalconAPI.PreCall(1568);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.Store(proc, 2, low);
			HalconAPI.Store(proc, 3, high);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(low);
			HalconAPI.UnpinTuple(high);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont EdgesSubPix(string filter, double alpha, int low, int high)
		{
			IntPtr proc = HalconAPI.PreCall(1568);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.StoreI(proc, 2, low);
			HalconAPI.StoreI(proc, 3, high);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage EdgesImage(out HImage imaDir, string filter, double alpha, string NMS, HTuple low, HTuple high)
		{
			IntPtr proc = HalconAPI.PreCall(1569);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.StoreS(proc, 2, NMS);
			HalconAPI.Store(proc, 3, low);
			HalconAPI.Store(proc, 4, high);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(low);
			HalconAPI.UnpinTuple(high);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imaDir);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage EdgesImage(out HImage imaDir, string filter, double alpha, string NMS, int low, int high)
		{
			IntPtr proc = HalconAPI.PreCall(1569);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, filter);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.StoreS(proc, 2, NMS);
			HalconAPI.StoreI(proc, 3, low);
			HalconAPI.StoreI(proc, 4, high);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imaDir);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage DerivateGauss(HTuple sigma, string component)
		{
			IntPtr proc = HalconAPI.PreCall(1570);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sigma);
			HalconAPI.StoreS(proc, 1, component);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(sigma);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage DerivateGauss(double sigma, string component)
		{
			IntPtr proc = HalconAPI.PreCall(1570);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreS(proc, 1, component);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage LaplaceOfGauss(HTuple sigma)
		{
			IntPtr proc = HalconAPI.PreCall(1571);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, sigma);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(sigma);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage LaplaceOfGauss(double sigma)
		{
			IntPtr proc = HalconAPI.PreCall(1571);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage DiffOfGauss(double sigma, double sigFactor)
		{
			IntPtr proc = HalconAPI.PreCall(1572);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, sigma);
			HalconAPI.StoreD(proc, 1, sigFactor);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DetectEdgeSegments(int sobelSize, int minAmplitude, int maxDistance, int minLength, out HTuple beginRow, out HTuple beginCol, out HTuple endRow, out HTuple endCol)
		{
			IntPtr proc = HalconAPI.PreCall(1575);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, sobelSize);
			HalconAPI.StoreI(proc, 1, minAmplitude);
			HalconAPI.StoreI(proc, 2, maxDistance);
			HalconAPI.StoreI(proc, 3, minLength);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out beginRow);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out beginCol);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out endRow);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out endCol);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static void ClearColorTransLut(HColorTransLUT colorTransLUTHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1577);
			HalconAPI.Store(proc, 0, colorTransLUTHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(colorTransLUTHandle);
		}

		public HImage ApplyColorTransLut(HImage image2, HImage image3, out HImage imageResult2, out HImage imageResult3, HColorTransLUT colorTransLUTHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1578);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 3, image3);
			HalconAPI.Store(proc, 0, colorTransLUTHandle);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imageResult2);
			err = HImage.LoadNew(proc, 3, err, out imageResult3);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			GC.KeepAlive(image3);
			GC.KeepAlive(colorTransLUTHandle);
			return result;
		}

		public static HColorTransLUT CreateColorTransLut(string colorSpace, string transDirection, int numBits)
		{
			IntPtr proc = HalconAPI.PreCall(1579);
			HalconAPI.StoreS(proc, 0, colorSpace);
			HalconAPI.StoreS(proc, 1, transDirection);
			HalconAPI.StoreI(proc, 2, numBits);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HColorTransLUT result = null;
			err = HColorTransLUT.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public HImage CfaToRgb(string CFAType, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1580);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, CFAType);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Rgb1ToGray()
		{
			IntPtr proc = HalconAPI.PreCall(1581);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Rgb3ToGray(HImage imageGreen, HImage imageBlue)
		{
			IntPtr proc = HalconAPI.PreCall(1582);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageGreen);
			HalconAPI.Store(proc, 3, imageBlue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageGreen);
			GC.KeepAlive(imageBlue);
			return result;
		}

		public HImage TransFromRgb(HImage imageGreen, HImage imageBlue, out HImage imageResult2, out HImage imageResult3, string colorSpace)
		{
			IntPtr proc = HalconAPI.PreCall(1583);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageGreen);
			HalconAPI.Store(proc, 3, imageBlue);
			HalconAPI.StoreS(proc, 0, colorSpace);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imageResult2);
			err = HImage.LoadNew(proc, 3, err, out imageResult3);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageGreen);
			GC.KeepAlive(imageBlue);
			return result;
		}

		public HImage TransToRgb(HImage imageInput2, HImage imageInput3, out HImage imageGreen, out HImage imageBlue, string colorSpace)
		{
			IntPtr proc = HalconAPI.PreCall(1584);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageInput2);
			HalconAPI.Store(proc, 3, imageInput3);
			HalconAPI.StoreS(proc, 0, colorSpace);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out imageGreen);
			err = HImage.LoadNew(proc, 3, err, out imageBlue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageInput2);
			GC.KeepAlive(imageInput3);
			return result;
		}

		public HImage BitMask(int bitMask)
		{
			IntPtr proc = HalconAPI.PreCall(1585);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, bitMask);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage BitSlice(int bit)
		{
			IntPtr proc = HalconAPI.PreCall(1586);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, bit);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage BitRshift(int shift)
		{
			IntPtr proc = HalconAPI.PreCall(1587);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, shift);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage BitLshift(int shift)
		{
			IntPtr proc = HalconAPI.PreCall(1588);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, shift);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage BitNot()
		{
			IntPtr proc = HalconAPI.PreCall(1589);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage BitXor(HImage image2)
		{
			IntPtr proc = HalconAPI.PreCall(1590);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage BitOr(HImage image2)
		{
			IntPtr proc = HalconAPI.PreCall(1591);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage BitAnd(HImage image2)
		{
			IntPtr proc = HalconAPI.PreCall(1592);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage GammaImage(double gamma, double offset, double threshold, HTuple maxGray, string encode)
		{
			IntPtr proc = HalconAPI.PreCall(1593);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, gamma);
			HalconAPI.StoreD(proc, 1, offset);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.Store(proc, 3, maxGray);
			HalconAPI.StoreS(proc, 4, encode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maxGray);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GammaImage(double gamma, double offset, double threshold, double maxGray, string encode)
		{
			IntPtr proc = HalconAPI.PreCall(1593);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, gamma);
			HalconAPI.StoreD(proc, 1, offset);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.StoreD(proc, 3, maxGray);
			HalconAPI.StoreS(proc, 4, encode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PowImage(HTuple exponent)
		{
			IntPtr proc = HalconAPI.PreCall(1594);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, exponent);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(exponent);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PowImage(double exponent)
		{
			IntPtr proc = HalconAPI.PreCall(1594);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, exponent);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ExpImage(HTuple baseVal)
		{
			IntPtr proc = HalconAPI.PreCall(1595);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, baseVal);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(baseVal);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ExpImage(string baseVal)
		{
			IntPtr proc = HalconAPI.PreCall(1595);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, baseVal);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage LogImage(HTuple baseVal)
		{
			IntPtr proc = HalconAPI.PreCall(1596);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, baseVal);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(baseVal);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage LogImage(string baseVal)
		{
			IntPtr proc = HalconAPI.PreCall(1596);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, baseVal);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage Atan2Image(HImage imageX)
		{
			IntPtr proc = HalconAPI.PreCall(1597);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageX);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageX);
			return result;
		}

		public HImage AtanImage()
		{
			IntPtr proc = HalconAPI.PreCall(1598);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage AcosImage()
		{
			IntPtr proc = HalconAPI.PreCall(1599);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage AsinImage()
		{
			IntPtr proc = HalconAPI.PreCall(1600);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage TanImage()
		{
			IntPtr proc = HalconAPI.PreCall(1601);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage CosImage()
		{
			IntPtr proc = HalconAPI.PreCall(1602);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SinImage()
		{
			IntPtr proc = HalconAPI.PreCall(1603);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage AbsDiffImage(HImage image2, HTuple mult)
		{
			IntPtr proc = HalconAPI.PreCall(1604);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, mult);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mult);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage AbsDiffImage(HImage image2, double mult)
		{
			IntPtr proc = HalconAPI.PreCall(1604);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.StoreD(proc, 0, mult);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage SqrtImage()
		{
			IntPtr proc = HalconAPI.PreCall(1605);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage SubImage(HImage imageSubtrahend, HTuple mult, HTuple add)
		{
			IntPtr proc = HalconAPI.PreCall(1606);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageSubtrahend);
			HalconAPI.Store(proc, 0, mult);
			HalconAPI.Store(proc, 1, add);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mult);
			HalconAPI.UnpinTuple(add);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageSubtrahend);
			return result;
		}

		public HImage SubImage(HImage imageSubtrahend, double mult, double add)
		{
			IntPtr proc = HalconAPI.PreCall(1606);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageSubtrahend);
			HalconAPI.StoreD(proc, 0, mult);
			HalconAPI.StoreD(proc, 1, add);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageSubtrahend);
			return result;
		}

		public HImage ScaleImage(HTuple mult, HTuple add)
		{
			IntPtr proc = HalconAPI.PreCall(1607);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, mult);
			HalconAPI.Store(proc, 1, add);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mult);
			HalconAPI.UnpinTuple(add);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ScaleImage(double mult, double add)
		{
			IntPtr proc = HalconAPI.PreCall(1607);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, mult);
			HalconAPI.StoreD(proc, 1, add);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage DivImage(HImage image2, HTuple mult, HTuple add)
		{
			IntPtr proc = HalconAPI.PreCall(1608);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, mult);
			HalconAPI.Store(proc, 1, add);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mult);
			HalconAPI.UnpinTuple(add);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage DivImage(HImage image2, double mult, double add)
		{
			IntPtr proc = HalconAPI.PreCall(1608);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.StoreD(proc, 0, mult);
			HalconAPI.StoreD(proc, 1, add);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage MultImage(HImage image2, HTuple mult, HTuple add)
		{
			IntPtr proc = HalconAPI.PreCall(1609);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, mult);
			HalconAPI.Store(proc, 1, add);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mult);
			HalconAPI.UnpinTuple(add);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage MultImage(HImage image2, double mult, double add)
		{
			IntPtr proc = HalconAPI.PreCall(1609);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.StoreD(proc, 0, mult);
			HalconAPI.StoreD(proc, 1, add);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage AddImage(HImage image2, HTuple mult, HTuple add)
		{
			IntPtr proc = HalconAPI.PreCall(1610);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, mult);
			HalconAPI.Store(proc, 1, add);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mult);
			HalconAPI.UnpinTuple(add);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage AddImage(HImage image2, double mult, double add)
		{
			IntPtr proc = HalconAPI.PreCall(1610);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.StoreD(proc, 0, mult);
			HalconAPI.StoreD(proc, 1, add);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage AbsImage()
		{
			IntPtr proc = HalconAPI.PreCall(1611);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MinImage(HImage image2)
		{
			IntPtr proc = HalconAPI.PreCall(1612);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage MaxImage(HImage image2)
		{
			IntPtr proc = HalconAPI.PreCall(1613);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage InvertImage()
		{
			IntPtr proc = HalconAPI.PreCall(1614);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage AdjustMosaicImages(HTuple from, HTuple to, int referenceImage, HTuple homMatrices2D, string estimationMethod, HTuple estimateParameters, string OECFModel)
		{
			IntPtr proc = HalconAPI.PreCall(1615);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, from);
			HalconAPI.Store(proc, 1, to);
			HalconAPI.StoreI(proc, 2, referenceImage);
			HalconAPI.Store(proc, 3, homMatrices2D);
			HalconAPI.StoreS(proc, 4, estimationMethod);
			HalconAPI.Store(proc, 5, estimateParameters);
			HalconAPI.StoreS(proc, 6, OECFModel);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(from);
			HalconAPI.UnpinTuple(to);
			HalconAPI.UnpinTuple(homMatrices2D);
			HalconAPI.UnpinTuple(estimateParameters);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage AdjustMosaicImages(HTuple from, HTuple to, int referenceImage, HTuple homMatrices2D, string estimationMethod, string estimateParameters, string OECFModel)
		{
			IntPtr proc = HalconAPI.PreCall(1615);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, from);
			HalconAPI.Store(proc, 1, to);
			HalconAPI.StoreI(proc, 2, referenceImage);
			HalconAPI.Store(proc, 3, homMatrices2D);
			HalconAPI.StoreS(proc, 4, estimationMethod);
			HalconAPI.StoreS(proc, 5, estimateParameters);
			HalconAPI.StoreS(proc, 6, OECFModel);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(from);
			HalconAPI.UnpinTuple(to);
			HalconAPI.UnpinTuple(homMatrices2D);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenCubeMapMosaic(out HImage rear, out HImage left, out HImage right, out HImage top, out HImage bottom, HHomMat2D[] cameraMatrices, HHomMat2D[] rotationMatrices, int cubeMapDimension, HTuple stackingOrder, string interpolation)
		{
			HTuple hTuple = HData.ConcatArray(cameraMatrices);
			HTuple hTuple2 = HData.ConcatArray(rotationMatrices);
			IntPtr proc = HalconAPI.PreCall(1616);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.StoreI(proc, 2, cubeMapDimension);
			HalconAPI.Store(proc, 3, stackingOrder);
			HalconAPI.StoreS(proc, 4, interpolation);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(stackingOrder);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out rear);
			err = HImage.LoadNew(proc, 3, err, out left);
			err = HImage.LoadNew(proc, 4, err, out right);
			err = HImage.LoadNew(proc, 5, err, out top);
			err = HImage.LoadNew(proc, 6, err, out bottom);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenCubeMapMosaic(out HImage rear, out HImage left, out HImage right, out HImage top, out HImage bottom, HHomMat2D[] cameraMatrices, HHomMat2D[] rotationMatrices, int cubeMapDimension, string stackingOrder, string interpolation)
		{
			HTuple hTuple = HData.ConcatArray(cameraMatrices);
			HTuple hTuple2 = HData.ConcatArray(rotationMatrices);
			IntPtr proc = HalconAPI.PreCall(1616);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.StoreI(proc, 2, cubeMapDimension);
			HalconAPI.StoreS(proc, 3, stackingOrder);
			HalconAPI.StoreS(proc, 4, interpolation);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out rear);
			err = HImage.LoadNew(proc, 3, err, out left);
			err = HImage.LoadNew(proc, 4, err, out right);
			err = HImage.LoadNew(proc, 5, err, out top);
			err = HImage.LoadNew(proc, 6, err, out bottom);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenSphericalMosaic(HHomMat2D[] cameraMatrices, HHomMat2D[] rotationMatrices, HTuple latMin, HTuple latMax, HTuple longMin, HTuple longMax, HTuple latLongStep, HTuple stackingOrder, HTuple interpolation)
		{
			HTuple hTuple = HData.ConcatArray(cameraMatrices);
			HTuple hTuple2 = HData.ConcatArray(rotationMatrices);
			IntPtr proc = HalconAPI.PreCall(1617);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.Store(proc, 2, latMin);
			HalconAPI.Store(proc, 3, latMax);
			HalconAPI.Store(proc, 4, longMin);
			HalconAPI.Store(proc, 5, longMax);
			HalconAPI.Store(proc, 6, latLongStep);
			HalconAPI.Store(proc, 7, stackingOrder);
			HalconAPI.Store(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(latMin);
			HalconAPI.UnpinTuple(latMax);
			HalconAPI.UnpinTuple(longMin);
			HalconAPI.UnpinTuple(longMax);
			HalconAPI.UnpinTuple(latLongStep);
			HalconAPI.UnpinTuple(stackingOrder);
			HalconAPI.UnpinTuple(interpolation);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenSphericalMosaic(HHomMat2D[] cameraMatrices, HHomMat2D[] rotationMatrices, double latMin, double latMax, double longMin, double longMax, double latLongStep, string stackingOrder, string interpolation)
		{
			HTuple hTuple = HData.ConcatArray(cameraMatrices);
			HTuple hTuple2 = HData.ConcatArray(rotationMatrices);
			IntPtr proc = HalconAPI.PreCall(1617);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.StoreD(proc, 2, latMin);
			HalconAPI.StoreD(proc, 3, latMax);
			HalconAPI.StoreD(proc, 4, longMin);
			HalconAPI.StoreD(proc, 5, longMax);
			HalconAPI.StoreD(proc, 6, latLongStep);
			HalconAPI.StoreS(proc, 7, stackingOrder);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenBundleAdjustedMosaic(HHomMat2D[] homMatrices2D, HTuple stackingOrder, string transformDomain, out HHomMat2D transMat2D)
		{
			HTuple hTuple = HData.ConcatArray(homMatrices2D);
			IntPtr proc = HalconAPI.PreCall(1618);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, stackingOrder);
			HalconAPI.StoreS(proc, 2, transformDomain);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(stackingOrder);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HHomMat2D.LoadNew(proc, 0, err, out transMat2D);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenBundleAdjustedMosaic(HHomMat2D[] homMatrices2D, string stackingOrder, string transformDomain, out HHomMat2D transMat2D)
		{
			HTuple hTuple = HData.ConcatArray(homMatrices2D);
			IntPtr proc = HalconAPI.PreCall(1618);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, stackingOrder);
			HalconAPI.StoreS(proc, 2, transformDomain);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HHomMat2D.LoadNew(proc, 0, err, out transMat2D);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenProjectiveMosaic(int startImage, HTuple mappingSource, HTuple mappingDest, HHomMat2D[] homMatrices2D, HTuple stackingOrder, string transformDomain, out HHomMat2D[] mosaicMatrices2D)
		{
			HTuple hTuple = HData.ConcatArray(homMatrices2D);
			IntPtr proc = HalconAPI.PreCall(1619);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, startImage);
			HalconAPI.Store(proc, 1, mappingSource);
			HalconAPI.Store(proc, 2, mappingDest);
			HalconAPI.Store(proc, 3, hTuple);
			HalconAPI.Store(proc, 4, stackingOrder);
			HalconAPI.StoreS(proc, 5, transformDomain);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mappingSource);
			HalconAPI.UnpinTuple(mappingDest);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(stackingOrder);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			mosaicMatrices2D = HHomMat2D.SplitArray(data);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenProjectiveMosaic(int startImage, HTuple mappingSource, HTuple mappingDest, HHomMat2D[] homMatrices2D, string stackingOrder, string transformDomain, out HHomMat2D[] mosaicMatrices2D)
		{
			HTuple hTuple = HData.ConcatArray(homMatrices2D);
			IntPtr proc = HalconAPI.PreCall(1619);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, startImage);
			HalconAPI.Store(proc, 1, mappingSource);
			HalconAPI.Store(proc, 2, mappingDest);
			HalconAPI.Store(proc, 3, hTuple);
			HalconAPI.StoreS(proc, 4, stackingOrder);
			HalconAPI.StoreS(proc, 5, transformDomain);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mappingSource);
			HalconAPI.UnpinTuple(mappingDest);
			HalconAPI.UnpinTuple(hTuple);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			mosaicMatrices2D = HHomMat2D.SplitArray(data);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ProjectiveTransImageSize(HHomMat2D homMat2D, string interpolation, int width, int height, string transformDomain)
		{
			IntPtr proc = HalconAPI.PreCall(1620);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.StoreS(proc, 4, transformDomain);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ProjectiveTransImage(HHomMat2D homMat2D, string interpolation, string adaptImageSize, string transformDomain)
		{
			IntPtr proc = HalconAPI.PreCall(1621);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.StoreS(proc, 2, adaptImageSize);
			HalconAPI.StoreS(proc, 3, transformDomain);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage AffineTransImageSize(HHomMat2D homMat2D, string interpolation, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1622);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage AffineTransImage(HHomMat2D homMat2D, string interpolation, string adaptImageSize)
		{
			IntPtr proc = HalconAPI.PreCall(1623);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.StoreS(proc, 2, adaptImageSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ZoomImageFactor(double scaleWidth, double scaleHeight, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1624);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, scaleWidth);
			HalconAPI.StoreD(proc, 1, scaleHeight);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ZoomImageSize(int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1625);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MirrorImage(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1626);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage RotateImage(HTuple phi, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1627);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, phi);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(phi);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage RotateImage(double phi, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1627);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, phi);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PolarTransImageInv(HTuple row, HTuple column, double angleStart, double angleEnd, HTuple radiusStart, HTuple radiusEnd, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1628);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.Store(proc, 4, radiusStart);
			HalconAPI.Store(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radiusStart);
			HalconAPI.UnpinTuple(radiusEnd);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PolarTransImageInv(double row, double column, double angleStart, double angleEnd, double radiusStart, double radiusEnd, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1628);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.StoreD(proc, 4, radiusStart);
			HalconAPI.StoreD(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PolarTransImageExt(HTuple row, HTuple column, double angleStart, double angleEnd, HTuple radiusStart, HTuple radiusEnd, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1629);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.Store(proc, 4, radiusStart);
			HalconAPI.Store(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radiusStart);
			HalconAPI.UnpinTuple(radiusEnd);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PolarTransImageExt(double row, double column, double angleStart, double angleEnd, double radiusStart, double radiusEnd, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1629);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.StoreD(proc, 4, radiusStart);
			HalconAPI.StoreD(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage PolarTransImage(int row, int column, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1630);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D VectorFieldToHomMat2d()
		{
			IntPtr proc = HalconAPI.PreCall(1631);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializeImage(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1650);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeImage()
		{
			IntPtr proc = HalconAPI.PreCall(1651);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void WriteImage(string format, HTuple fillColor, HTuple fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1655);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, format);
			HalconAPI.Store(proc, 1, fillColor);
			HalconAPI.Store(proc, 2, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fillColor);
			HalconAPI.UnpinTuple(fileName);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteImage(string format, int fillColor, string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1655);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, format);
			HalconAPI.StoreI(proc, 1, fillColor);
			HalconAPI.StoreS(proc, 2, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadSequence(int headerSize, int sourceWidth, int sourceHeight, int startRow, int startColumn, int destWidth, int destHeight, string pixelType, string bitOrder, string byteOrder, string pad, int index, string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1656);
			HalconAPI.StoreI(proc, 0, headerSize);
			HalconAPI.StoreI(proc, 1, sourceWidth);
			HalconAPI.StoreI(proc, 2, sourceHeight);
			HalconAPI.StoreI(proc, 3, startRow);
			HalconAPI.StoreI(proc, 4, startColumn);
			HalconAPI.StoreI(proc, 5, destWidth);
			HalconAPI.StoreI(proc, 6, destHeight);
			HalconAPI.StoreS(proc, 7, pixelType);
			HalconAPI.StoreS(proc, 8, bitOrder);
			HalconAPI.StoreS(proc, 9, byteOrder);
			HalconAPI.StoreS(proc, 10, pad);
			HalconAPI.StoreI(proc, 11, index);
			HalconAPI.StoreS(proc, 12, fileName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ReadImage(HTuple fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1658);
			HalconAPI.Store(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fileName);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ReadImage(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1658);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetGrayvalContourXld(HXLDCont contour, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1668);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, contour);
			HalconAPI.StoreS(proc, 0, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
			return result;
		}

		public HTuple FitSurfaceFirstOrder(HRegion regions, string algorithm, int iterations, double clippingFactor, out HTuple beta, out HTuple gamma)
		{
			IntPtr proc = HalconAPI.PreCall(1743);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreD(proc, 2, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out beta);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out gamma);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public double FitSurfaceFirstOrder(HRegion regions, string algorithm, int iterations, double clippingFactor, out double beta, out double gamma)
		{
			IntPtr proc = HalconAPI.PreCall(1743);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreD(proc, 2, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out beta);
			err = HalconAPI.LoadD(proc, 2, err, out gamma);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HTuple FitSurfaceSecondOrder(HRegion regions, string algorithm, int iterations, double clippingFactor, out HTuple beta, out HTuple gamma, out HTuple delta, out HTuple epsilon, out HTuple zeta)
		{
			IntPtr proc = HalconAPI.PreCall(1744);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreD(proc, 2, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out beta);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out gamma);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out delta);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out epsilon);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out zeta);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public double FitSurfaceSecondOrder(HRegion regions, string algorithm, int iterations, double clippingFactor, out double beta, out double gamma, out double delta, out double epsilon, out double zeta)
		{
			IntPtr proc = HalconAPI.PreCall(1744);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreD(proc, 2, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out beta);
			err = HalconAPI.LoadD(proc, 2, err, out gamma);
			err = HalconAPI.LoadD(proc, 3, err, out delta);
			err = HalconAPI.LoadD(proc, 4, err, out epsilon);
			err = HalconAPI.LoadD(proc, 5, err, out zeta);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public void GenImageSurfaceSecondOrder(string type, double alpha, double beta, double gamma, double delta, double epsilon, double zeta, double row, double column, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1745);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.StoreD(proc, 2, beta);
			HalconAPI.StoreD(proc, 3, gamma);
			HalconAPI.StoreD(proc, 4, delta);
			HalconAPI.StoreD(proc, 5, epsilon);
			HalconAPI.StoreD(proc, 6, zeta);
			HalconAPI.StoreD(proc, 7, row);
			HalconAPI.StoreD(proc, 8, column);
			HalconAPI.StoreI(proc, 9, width);
			HalconAPI.StoreI(proc, 10, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenImageSurfaceFirstOrder(string type, double alpha, double beta, double gamma, double row, double column, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1746);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.StoreD(proc, 2, beta);
			HalconAPI.StoreD(proc, 3, gamma);
			HalconAPI.StoreD(proc, 4, row);
			HalconAPI.StoreD(proc, 5, column);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void MinMaxGray(HRegion regions, HTuple percent, out HTuple min, out HTuple max, out HTuple range)
		{
			IntPtr proc = HalconAPI.PreCall(1751);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.Store(proc, 0, percent);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(percent);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out min);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out max);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out range);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
		}

		public void MinMaxGray(HRegion regions, double percent, out double min, out double max, out double range)
		{
			IntPtr proc = HalconAPI.PreCall(1751);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreD(proc, 0, percent);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out min);
			err = HalconAPI.LoadD(proc, 1, err, out max);
			err = HalconAPI.LoadD(proc, 2, err, out range);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
		}

		public HTuple Intensity(HRegion regions, out HTuple deviation)
		{
			IntPtr proc = HalconAPI.PreCall(1752);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out deviation);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public double Intensity(HRegion regions, out double deviation)
		{
			IntPtr proc = HalconAPI.PreCall(1752);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out deviation);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HTuple GrayHistoRange(HRegion regions, HTuple min, HTuple max, int numBins, out double binSize)
		{
			IntPtr proc = HalconAPI.PreCall(1753);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.Store(proc, 0, min);
			HalconAPI.Store(proc, 1, max);
			HalconAPI.StoreI(proc, 2, numBins);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out binSize);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public int GrayHistoRange(HRegion regions, double min, double max, int numBins, out double binSize)
		{
			IntPtr proc = HalconAPI.PreCall(1753);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreD(proc, 0, min);
			HalconAPI.StoreD(proc, 1, max);
			HalconAPI.StoreI(proc, 2, numBins);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out binSize);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HImage Histo2dim(HRegion regions, HImage imageRow)
		{
			IntPtr proc = HalconAPI.PreCall(1754);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.Store(proc, 3, imageRow);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			GC.KeepAlive(imageRow);
			return result;
		}

		public HTuple GrayHistoAbs(HRegion regions, HTuple quantization)
		{
			IntPtr proc = HalconAPI.PreCall(1755);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.Store(proc, 0, quantization);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(quantization);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HTuple GrayHistoAbs(HRegion regions, double quantization)
		{
			IntPtr proc = HalconAPI.PreCall(1755);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreD(proc, 0, quantization);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HTuple GrayHisto(HRegion regions, out HTuple relativeHisto)
		{
			IntPtr proc = HalconAPI.PreCall(1756);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out relativeHisto);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HTuple EntropyGray(HRegion regions, out HTuple anisotropy)
		{
			IntPtr proc = HalconAPI.PreCall(1757);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out anisotropy);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public double EntropyGray(HRegion regions, out double anisotropy)
		{
			IntPtr proc = HalconAPI.PreCall(1757);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out anisotropy);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public double CoocFeatureMatrix(out double correlation, out double homogeneity, out double contrast)
		{
			IntPtr proc = HalconAPI.PreCall(1758);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out correlation);
			err = HalconAPI.LoadD(proc, 2, err, out homogeneity);
			err = HalconAPI.LoadD(proc, 3, err, out contrast);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple CoocFeatureImage(HRegion regions, int ldGray, HTuple direction, out HTuple correlation, out HTuple homogeneity, out HTuple contrast)
		{
			IntPtr proc = HalconAPI.PreCall(1759);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreI(proc, 0, ldGray);
			HalconAPI.Store(proc, 1, direction);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(direction);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out correlation);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out homogeneity);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out contrast);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public double CoocFeatureImage(HRegion regions, int ldGray, int direction, out double correlation, out double homogeneity, out double contrast)
		{
			IntPtr proc = HalconAPI.PreCall(1759);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreI(proc, 0, ldGray);
			HalconAPI.StoreI(proc, 1, direction);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out correlation);
			err = HalconAPI.LoadD(proc, 2, err, out homogeneity);
			err = HalconAPI.LoadD(proc, 3, err, out contrast);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HImage GenCoocMatrix(HRegion regions, int ldGray, int direction)
		{
			IntPtr proc = HalconAPI.PreCall(1760);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreI(proc, 0, ldGray);
			HalconAPI.StoreI(proc, 1, direction);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public void MomentsGrayPlane(HRegion regions, out HTuple MRow, out HTuple MCol, out HTuple alpha, out HTuple beta, out HTuple mean)
		{
			IntPtr proc = HalconAPI.PreCall(1761);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out MRow);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out MCol);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out alpha);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out beta);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out mean);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
		}

		public void MomentsGrayPlane(HRegion regions, out double MRow, out double MCol, out double alpha, out double beta, out double mean)
		{
			IntPtr proc = HalconAPI.PreCall(1761);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out MRow);
			err = HalconAPI.LoadD(proc, 1, err, out MCol);
			err = HalconAPI.LoadD(proc, 2, err, out alpha);
			err = HalconAPI.LoadD(proc, 3, err, out beta);
			err = HalconAPI.LoadD(proc, 4, err, out mean);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
		}

		public HTuple PlaneDeviation(HRegion regions)
		{
			IntPtr proc = HalconAPI.PreCall(1762);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HTuple EllipticAxisGray(HRegion regions, out HTuple rb, out HTuple phi)
		{
			IntPtr proc = HalconAPI.PreCall(1763);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out rb);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public double EllipticAxisGray(HRegion regions, out double rb, out double phi)
		{
			IntPtr proc = HalconAPI.PreCall(1763);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out rb);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HTuple AreaCenterGray(HRegion regions, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1764);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public double AreaCenterGray(HRegion regions, out double row, out double column)
		{
			IntPtr proc = HalconAPI.PreCall(1764);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out row);
			err = HalconAPI.LoadD(proc, 2, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HTuple GrayProjections(HRegion region, string mode, out HTuple vertProjection)
		{
			IntPtr proc = HalconAPI.PreCall(1765);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, region);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out vertProjection);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HXLDCont FindDataCode2d(HDataCode2D dataCodeHandle, HTuple genParamName, HTuple genParamValue, out HTuple resultHandles, out HTuple decodedDataStrings)
		{
			IntPtr proc = HalconAPI.PreCall(1768);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, dataCodeHandle);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out resultHandles);
			err = HTuple.LoadNew(proc, 1, err, out decodedDataStrings);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(dataCodeHandle);
			return result;
		}

		public HXLDCont FindDataCode2d(HDataCode2D dataCodeHandle, string genParamName, int genParamValue, out int resultHandles, out string decodedDataStrings)
		{
			IntPtr proc = HalconAPI.PreCall(1768);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, dataCodeHandle);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreI(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HalconAPI.LoadI(proc, 0, err, out resultHandles);
			err = HalconAPI.LoadS(proc, 1, err, out decodedDataStrings);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(dataCodeHandle);
			return result;
		}

		public HImage ConvertMapType(string newType, HTuple imageWidth)
		{
			IntPtr proc = HalconAPI.PreCall(1901);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, newType);
			HalconAPI.Store(proc, 1, imageWidth);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(imageWidth);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ConvertMapType(string newType, int imageWidth)
		{
			IntPtr proc = HalconAPI.PreCall(1901);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, newType);
			HalconAPI.StoreI(proc, 1, imageWidth);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HPose VectorToPose(HTuple worldX, HTuple worldY, HTuple worldZ, HTuple imageRow, HTuple imageColumn, HCamPar cameraParam, string method, HTuple qualityType, out HTuple quality)
		{
			IntPtr proc = HalconAPI.PreCall(1902);
			HalconAPI.Store(proc, 0, worldX);
			HalconAPI.Store(proc, 1, worldY);
			HalconAPI.Store(proc, 2, worldZ);
			HalconAPI.Store(proc, 3, imageRow);
			HalconAPI.Store(proc, 4, imageColumn);
			HalconAPI.Store(proc, 5, cameraParam);
			HalconAPI.StoreS(proc, 6, method);
			HalconAPI.Store(proc, 7, qualityType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(worldX);
			HalconAPI.UnpinTuple(worldY);
			HalconAPI.UnpinTuple(worldZ);
			HalconAPI.UnpinTuple(imageRow);
			HalconAPI.UnpinTuple(imageColumn);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(qualityType);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out quality);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HPose VectorToPose(HTuple worldX, HTuple worldY, HTuple worldZ, HTuple imageRow, HTuple imageColumn, HCamPar cameraParam, string method, string qualityType, out double quality)
		{
			IntPtr proc = HalconAPI.PreCall(1902);
			HalconAPI.Store(proc, 0, worldX);
			HalconAPI.Store(proc, 1, worldY);
			HalconAPI.Store(proc, 2, worldZ);
			HalconAPI.Store(proc, 3, imageRow);
			HalconAPI.Store(proc, 4, imageColumn);
			HalconAPI.Store(proc, 5, cameraParam);
			HalconAPI.StoreS(proc, 6, method);
			HalconAPI.StoreS(proc, 7, qualityType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(worldX);
			HalconAPI.UnpinTuple(worldY);
			HalconAPI.UnpinTuple(worldZ);
			HalconAPI.UnpinTuple(imageRow);
			HalconAPI.UnpinTuple(imageColumn);
			HalconAPI.UnpinTuple(cameraParam);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out quality);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HPose ProjHomMat2dToPose(HHomMat2D homography, HHomMat2D cameraMatrix, string method)
		{
			IntPtr proc = HalconAPI.PreCall(1903);
			HalconAPI.Store(proc, 0, homography);
			HalconAPI.Store(proc, 1, cameraMatrix);
			HalconAPI.StoreS(proc, 2, method);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homography);
			HalconAPI.UnpinTuple(cameraMatrix);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public HTuple RadiometricSelfCalibration(HTuple exposureRatios, string features, string functionType, double smoothness, int polynomialDegree)
		{
			IntPtr proc = HalconAPI.PreCall(1910);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, exposureRatios);
			HalconAPI.StoreS(proc, 1, features);
			HalconAPI.StoreS(proc, 2, functionType);
			HalconAPI.StoreD(proc, 3, smoothness);
			HalconAPI.StoreI(proc, 4, polynomialDegree);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(exposureRatios);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple RadiometricSelfCalibration(double exposureRatios, string features, string functionType, double smoothness, int polynomialDegree)
		{
			IntPtr proc = HalconAPI.PreCall(1910);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, exposureRatios);
			HalconAPI.StoreS(proc, 1, features);
			HalconAPI.StoreS(proc, 2, functionType);
			HalconAPI.StoreD(proc, 3, smoothness);
			HalconAPI.StoreI(proc, 4, polynomialDegree);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage MapImage(HImage map)
		{
			IntPtr proc = HalconAPI.PreCall(1911);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, map);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(map);
			return result;
		}

		public void GenRadialDistortionMap(HCamPar camParamIn, HCamPar camParamOut, string mapType)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1912);
			HalconAPI.Store(proc, 0, camParamIn);
			HalconAPI.Store(proc, 1, camParamOut);
			HalconAPI.StoreS(proc, 2, mapType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamIn);
			HalconAPI.UnpinTuple(camParamOut);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenImageToWorldPlaneMap(HCamPar cameraParam, HPose worldPose, int widthIn, int heightIn, int widthMapped, int heightMapped, HTuple scale, string mapType)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1913);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.StoreI(proc, 2, widthIn);
			HalconAPI.StoreI(proc, 3, heightIn);
			HalconAPI.StoreI(proc, 4, widthMapped);
			HalconAPI.StoreI(proc, 5, heightMapped);
			HalconAPI.Store(proc, 6, scale);
			HalconAPI.StoreS(proc, 7, mapType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(worldPose);
			HalconAPI.UnpinTuple(scale);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenImageToWorldPlaneMap(HCamPar cameraParam, HPose worldPose, int widthIn, int heightIn, int widthMapped, int heightMapped, string scale, string mapType)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1913);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.StoreI(proc, 2, widthIn);
			HalconAPI.StoreI(proc, 3, heightIn);
			HalconAPI.StoreI(proc, 4, widthMapped);
			HalconAPI.StoreI(proc, 5, heightMapped);
			HalconAPI.StoreS(proc, 6, scale);
			HalconAPI.StoreS(proc, 7, mapType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(worldPose);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage ImageToWorldPlane(HCamPar cameraParam, HPose worldPose, int width, int height, HTuple scale, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1914);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.Store(proc, 4, scale);
			HalconAPI.StoreS(proc, 5, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(worldPose);
			HalconAPI.UnpinTuple(scale);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ImageToWorldPlane(HCamPar cameraParam, HPose worldPose, int width, int height, string scale, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1914);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.StoreS(proc, 4, scale);
			HalconAPI.StoreS(proc, 5, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(worldPose);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ChangeRadialDistortionImage(HRegion region, HCamPar camParamIn, HCamPar camParamOut)
		{
			IntPtr proc = HalconAPI.PreCall(1924);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.Store(proc, 0, camParamIn);
			HalconAPI.Store(proc, 1, camParamOut);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamIn);
			HalconAPI.UnpinTuple(camParamOut);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public void SimCaltab(string calPlateDescr, HCamPar cameraParam, HPose calPlatePose, int grayBackground, int grayPlate, int grayMarks, double scaleFac)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1944);
			HalconAPI.StoreS(proc, 0, calPlateDescr);
			HalconAPI.Store(proc, 1, cameraParam);
			HalconAPI.Store(proc, 2, calPlatePose);
			HalconAPI.StoreI(proc, 3, grayBackground);
			HalconAPI.StoreI(proc, 4, grayPlate);
			HalconAPI.StoreI(proc, 5, grayMarks);
			HalconAPI.StoreD(proc, 6, scaleFac);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(calPlatePose);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple FindMarksAndPose(HRegion calPlateRegion, string calPlateDescr, HCamPar startCamParam, int startThresh, int deltaThresh, int minThresh, double alpha, double minContLength, double maxDiamMarks, out HTuple CCoord, out HPose startPose)
		{
			IntPtr proc = HalconAPI.PreCall(1947);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, calPlateRegion);
			HalconAPI.StoreS(proc, 0, calPlateDescr);
			HalconAPI.Store(proc, 1, startCamParam);
			HalconAPI.StoreI(proc, 2, startThresh);
			HalconAPI.StoreI(proc, 3, deltaThresh);
			HalconAPI.StoreI(proc, 4, minThresh);
			HalconAPI.StoreD(proc, 5, alpha);
			HalconAPI.StoreD(proc, 6, minContLength);
			HalconAPI.StoreD(proc, 7, maxDiamMarks);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(startCamParam);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out CCoord);
			err = HPose.LoadNew(proc, 2, err, out startPose);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(calPlateRegion);
			return result;
		}

		public HRegion FindCaltab(string calPlateDescr, HTuple sizeGauss, HTuple markThresh, int minDiamMarks)
		{
			IntPtr proc = HalconAPI.PreCall(1948);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, calPlateDescr);
			HalconAPI.Store(proc, 1, sizeGauss);
			HalconAPI.Store(proc, 2, markThresh);
			HalconAPI.StoreI(proc, 3, minDiamMarks);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(sizeGauss);
			HalconAPI.UnpinTuple(markThresh);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion FindCaltab(string calPlateDescr, int sizeGauss, int markThresh, int minDiamMarks)
		{
			IntPtr proc = HalconAPI.PreCall(1948);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, calPlateDescr);
			HalconAPI.StoreI(proc, 1, sizeGauss);
			HalconAPI.StoreI(proc, 2, markThresh);
			HalconAPI.StoreI(proc, 3, minDiamMarks);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple DecodeBarCodeRectangle2(HBarCode barCodeHandle, HTuple codeType, HTuple row, HTuple column, HTuple phi, HTuple length1, HTuple length2)
		{
			IntPtr proc = HalconAPI.PreCall(1992);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, barCodeHandle);
			HalconAPI.Store(proc, 1, codeType);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.Store(proc, 4, phi);
			HalconAPI.Store(proc, 5, length1);
			HalconAPI.Store(proc, 6, length2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(codeType);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(length1);
			HalconAPI.UnpinTuple(length2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(barCodeHandle);
			return result;
		}

		public string DecodeBarCodeRectangle2(HBarCode barCodeHandle, string codeType, double row, double column, double phi, double length1, double length2)
		{
			IntPtr proc = HalconAPI.PreCall(1992);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, barCodeHandle);
			HalconAPI.StoreS(proc, 1, codeType);
			HalconAPI.StoreD(proc, 2, row);
			HalconAPI.StoreD(proc, 3, column);
			HalconAPI.StoreD(proc, 4, phi);
			HalconAPI.StoreD(proc, 5, length1);
			HalconAPI.StoreD(proc, 6, length2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(barCodeHandle);
			return result;
		}

		public HRegion FindBarCode(HBarCode barCodeHandle, HTuple codeType, out HTuple decodedDataStrings)
		{
			IntPtr proc = HalconAPI.PreCall(1993);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, barCodeHandle);
			HalconAPI.Store(proc, 1, codeType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(codeType);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, err, out decodedDataStrings);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(barCodeHandle);
			return result;
		}

		public HRegion FindBarCode(HBarCode barCodeHandle, string codeType, out string decodedDataStrings)
		{
			IntPtr proc = HalconAPI.PreCall(1993);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, barCodeHandle);
			HalconAPI.StoreS(proc, 1, codeType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HalconAPI.LoadS(proc, 0, err, out decodedDataStrings);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(barCodeHandle);
			return result;
		}

		public void GiveBgEsti(HBgEsti bgEstiHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2003);
			HalconAPI.Store(proc, 0, bgEstiHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(bgEstiHandle);
		}

		public void UpdateBgEsti(HRegion upDateRegion, HBgEsti bgEstiHandle)
		{
			IntPtr proc = HalconAPI.PreCall(2004);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, upDateRegion);
			HalconAPI.Store(proc, 0, bgEstiHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(upDateRegion);
			GC.KeepAlive(bgEstiHandle);
		}

		public HRegion RunBgEsti(HBgEsti bgEstiHandle)
		{
			IntPtr proc = HalconAPI.PreCall(2005);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, bgEstiHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(bgEstiHandle);
			return result;
		}

		public HBgEsti CreateBgEsti(double syspar1, double syspar2, string gainMode, double gain1, double gain2, string adaptMode, double minDiff, int statNum, double confidenceC, double timeC)
		{
			IntPtr proc = HalconAPI.PreCall(2008);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, syspar1);
			HalconAPI.StoreD(proc, 1, syspar2);
			HalconAPI.StoreS(proc, 2, gainMode);
			HalconAPI.StoreD(proc, 3, gain1);
			HalconAPI.StoreD(proc, 4, gain2);
			HalconAPI.StoreS(proc, 5, adaptMode);
			HalconAPI.StoreD(proc, 6, minDiff);
			HalconAPI.StoreI(proc, 7, statNum);
			HalconAPI.StoreD(proc, 8, confidenceC);
			HalconAPI.StoreD(proc, 9, timeC);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HBgEsti result = null;
			err = HBgEsti.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion GrabDataAsync(out HXLDCont contours, HFramegrabber acqHandle, double maxDelay, out HTuple data)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2029);
			HalconAPI.Store(proc, 0, acqHandle);
			HalconAPI.StoreD(proc, 1, maxDelay);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 2, err, out result);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(acqHandle);
			return result;
		}

		public HRegion GrabDataAsync(out HXLDCont contours, HFramegrabber acqHandle, double maxDelay, out string data)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2029);
			HalconAPI.Store(proc, 0, acqHandle);
			HalconAPI.StoreD(proc, 1, maxDelay);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 2, err, out result);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HalconAPI.LoadS(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(acqHandle);
			return result;
		}

		public HRegion GrabData(out HXLDCont contours, HFramegrabber acqHandle, out HTuple data)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2030);
			HalconAPI.Store(proc, 0, acqHandle);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 2, err, out result);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(acqHandle);
			return result;
		}

		public HRegion GrabData(out HXLDCont contours, HFramegrabber acqHandle, out string data)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2030);
			HalconAPI.Store(proc, 0, acqHandle);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 2, err, out result);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HalconAPI.LoadS(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(acqHandle);
			return result;
		}

		public void GrabImageAsync(HFramegrabber acqHandle, double maxDelay)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2031);
			HalconAPI.Store(proc, 0, acqHandle);
			HalconAPI.StoreD(proc, 1, maxDelay);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(acqHandle);
		}

		public void GrabImage(HFramegrabber acqHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2033);
			HalconAPI.Store(proc, 0, acqHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(acqHandle);
		}

		public HTuple AddTextureInspectionModelImage(HTextureInspectionModel textureInspectionModel)
		{
			IntPtr proc = HalconAPI.PreCall(2043);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(textureInspectionModel);
			return result;
		}

		public HRegion ApplyTextureInspectionModel(HTextureInspectionModel textureInspectionModel, out HTextureInspectionResult textureInspectionResultID)
		{
			IntPtr proc = HalconAPI.PreCall(2044);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HTextureInspectionResult.LoadNew(proc, 0, err, out textureInspectionResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(textureInspectionModel);
			return result;
		}

		public HImage BilateralFilter(HImage imageJoint, double sigmaSpatial, double sigmaRange, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2045);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageJoint);
			HalconAPI.StoreD(proc, 0, sigmaSpatial);
			HalconAPI.StoreD(proc, 1, sigmaRange);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageJoint);
			return result;
		}

		public HImage BilateralFilter(HImage imageJoint, double sigmaSpatial, double sigmaRange, string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2045);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageJoint);
			HalconAPI.StoreD(proc, 0, sigmaSpatial);
			HalconAPI.StoreD(proc, 1, sigmaRange);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageJoint);
			return result;
		}

		public void FindNccModels(HNCCModel[] modelIDs, HTuple angleStart, HTuple angleExtent, HTuple minScore, HTuple numMatches, HTuple maxOverlap, HTuple subPixel, HTuple numLevels, out HTuple row, out HTuple column, out HTuple angle, out HTuple score, out HTuple model)
		{
			HTuple hTuple = HTool.ConcatArray(modelIDs);
			IntPtr proc = HalconAPI.PreCall(2068);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, numMatches);
			HalconAPI.Store(proc, 5, maxOverlap);
			HalconAPI.Store(proc, 6, subPixel);
			HalconAPI.Store(proc, 7, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(numMatches);
			HalconAPI.UnpinTuple(maxOverlap);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelIDs);
		}

		public void FindNccModels(HNCCModel modelIDs, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, out HTuple row, out HTuple column, out HTuple angle, out HTuple score, out HTuple model)
		{
			IntPtr proc = HalconAPI.PreCall(2068);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, modelIDs);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, subPixel);
			HalconAPI.StoreI(proc, 7, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelIDs);
		}

		public void GetTextureInspectionModelImage(HTextureInspectionModel textureInspectionModel)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2075);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(textureInspectionModel);
		}

		public HImage GuidedFilter(HImage imageGuide, int radius, double amplitude)
		{
			IntPtr proc = HalconAPI.PreCall(2078);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, imageGuide);
			HalconAPI.StoreI(proc, 0, radius);
			HalconAPI.StoreD(proc, 1, amplitude);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageGuide);
			return result;
		}

		public HImage InterleaveChannels(string pixelFormat, HTuple rowBytes, int alpha)
		{
			IntPtr proc = HalconAPI.PreCall(2079);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, pixelFormat);
			HalconAPI.Store(proc, 1, rowBytes);
			HalconAPI.StoreI(proc, 2, alpha);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowBytes);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage InterleaveChannels(string pixelFormat, string rowBytes, int alpha)
		{
			IntPtr proc = HalconAPI.PreCall(2079);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, pixelFormat);
			HalconAPI.StoreS(proc, 1, rowBytes);
			HalconAPI.StoreI(proc, 2, alpha);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion SegmentImageMser(out HRegion MSERLight, string polarity, HTuple minArea, HTuple maxArea, HTuple delta, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2087);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, polarity);
			HalconAPI.Store(proc, 1, minArea);
			HalconAPI.Store(proc, 2, maxArea);
			HalconAPI.Store(proc, 3, delta);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minArea);
			HalconAPI.UnpinTuple(maxArea);
			HalconAPI.UnpinTuple(delta);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HRegion.LoadNew(proc, 2, err, out MSERLight);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion SegmentImageMser(out HRegion MSERLight, string polarity, int minArea, int maxArea, int delta, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2087);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, polarity);
			HalconAPI.StoreI(proc, 1, minArea);
			HalconAPI.StoreI(proc, 2, maxArea);
			HalconAPI.StoreI(proc, 3, delta);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HRegion.LoadNew(proc, 2, err, out MSERLight);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static void TrainTextureInspectionModel(HTextureInspectionModel textureInspectionModel)
		{
			IntPtr proc = HalconAPI.PreCall(2099);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(textureInspectionModel);
		}

		public HImage UncalibratedPhotometricStereo(out HImage gradient, out HImage albedo, HTuple resultType)
		{
			IntPtr proc = HalconAPI.PreCall(2101);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, resultType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultType);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out gradient);
			err = HImage.LoadNew(proc, 3, err, out albedo);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage InsertObj(HImage objectsInsert, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2121);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsInsert);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsInsert);
			return result;
		}

		public new HImage RemoveObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HImage RemoveObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ReplaceObj(HImage objectsReplace, HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return result;
		}

		public HImage ReplaceObj(HImage objectsReplace, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return result;
		}
	}
}
