using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HTextureInspectionModel : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionModel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextureInspectionModel obj)
		{
			obj = new HTextureInspectionModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextureInspectionModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HTextureInspectionModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HTextureInspectionModel(hTuple[i].IP);
			}
			return err;
		}

		public HTextureInspectionModel(string modelType)
		{
			IntPtr proc = HalconAPI.PreCall(2051);
			HalconAPI.StoreS(proc, 0, modelType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeTextureInspectionModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeTextureInspectionModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeTextureInspectionModel().Serialize(stream);
		}

		public static HTextureInspectionModel Deserialize(Stream stream)
		{
			HTextureInspectionModel hTextureInspectionModel = new HTextureInspectionModel();
			hTextureInspectionModel.DeserializeTextureInspectionModel(HSerializedItem.Deserialize(stream));
			return hTextureInspectionModel;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HTextureInspectionModel Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeTextureInspectionModel();
			HTextureInspectionModel hTextureInspectionModel = new HTextureInspectionModel();
			hTextureInspectionModel.DeserializeTextureInspectionModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hTextureInspectionModel;
		}

		public HTuple AddTextureInspectionModelImage(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(2043);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HRegion ApplyTextureInspectionModel(HImage image, out HTextureInspectionResult textureInspectionResultID)
		{
			IntPtr proc = HalconAPI.PreCall(2044);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HTextureInspectionResult.LoadNew(proc, 0, err, out textureInspectionResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void CreateTextureInspectionModel(string modelType)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2051);
			HalconAPI.StoreS(proc, 0, modelType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DeserializeTextureInspectionModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2054);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HImage GetTextureInspectionModelImage()
		{
			IntPtr proc = HalconAPI.PreCall(2075);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetTextureInspectionModelParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2076);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetTextureInspectionModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2076);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadTextureInspectionModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2083);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HTuple RemoveTextureInspectionModelImage(HTextureInspectionModel[] textureInspectionModel, HTuple indices)
		{
			HTuple hTuple = HTool.ConcatArray(textureInspectionModel);
			IntPtr proc = HalconAPI.PreCall(2085);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, indices);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(indices);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(textureInspectionModel);
			return result;
		}

		public HTuple RemoveTextureInspectionModelImage(HTuple indices)
		{
			IntPtr proc = HalconAPI.PreCall(2085);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, indices);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(indices);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HSerializedItem SerializeTextureInspectionModel()
		{
			IntPtr proc = HalconAPI.PreCall(2094);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetTextureInspectionModelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2098);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetTextureInspectionModelParam(string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2098);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreI(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TrainTextureInspectionModel()
		{
			IntPtr proc = HalconAPI.PreCall(2099);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteTextureInspectionModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(2100);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(2047);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
