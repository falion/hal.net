namespace VisionConfig
{
	public enum HTupleType
	{
		EMPTY = 0xF,
		INTEGER = 1,
		LONG = 129,
		DOUBLE = 2,
		STRING = 4,
		MIXED = 8
	}
}
