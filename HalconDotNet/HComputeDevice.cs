using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HComputeDevice : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComputeDevice()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComputeDevice(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HComputeDevice obj)
		{
			obj = new HComputeDevice(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HComputeDevice[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HComputeDevice[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HComputeDevice(hTuple[i].IP);
			}
			return err;
		}

		public HComputeDevice(int deviceIdentifier)
		{
			IntPtr proc = HalconAPI.PreCall(304);
			HalconAPI.StoreI(proc, 0, deviceIdentifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetComputeDeviceParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(296);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetComputeDeviceParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(297);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetComputeDeviceParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(297);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void ReleaseAllComputeDevices()
		{
			IntPtr proc = HalconAPI.PreCall(298);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public void ReleaseComputeDevice()
		{
			IntPtr proc = HalconAPI.PreCall(299);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void DeactivateAllComputeDevices()
		{
			IntPtr proc = HalconAPI.PreCall(300);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public void DeactivateComputeDevice()
		{
			IntPtr proc = HalconAPI.PreCall(301);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ActivateComputeDevice()
		{
			IntPtr proc = HalconAPI.PreCall(302);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void InitComputeDevice(HTuple operators)
		{
			IntPtr proc = HalconAPI.PreCall(303);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, operators);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(operators);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void OpenComputeDevice(int deviceIdentifier)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(304);
			HalconAPI.StoreI(proc, 0, deviceIdentifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HTuple GetComputeDeviceInfo(int deviceIdentifier, string infoName)
		{
			IntPtr proc = HalconAPI.PreCall(305);
			HalconAPI.StoreI(proc, 0, deviceIdentifier);
			HalconAPI.StoreS(proc, 1, infoName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple QueryAvailableComputeDevices()
		{
			IntPtr proc = HalconAPI.PreCall(306);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(301);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
