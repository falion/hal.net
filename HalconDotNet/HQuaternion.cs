using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HQuaternion : HData, ISerializable, ICloneable
	{
		private const int FIXEDSIZE = 4;

		public HQuaternion()
		{
		}

		public HQuaternion(HTuple tuple)
			: base(tuple)
		{
		}

		internal HQuaternion(HData data)
			: base(data)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, HTupleType type, int err, out HQuaternion obj)
		{
			HTuple t = null;
			err = HTuple.LoadNew(proc, parIndex, err, out t);
			obj = new HQuaternion(new HData(t));
			return err;
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HQuaternion obj)
		{
			return HQuaternion.LoadNew(proc, parIndex, HTupleType.MIXED, err, out obj);
		}

		internal static HQuaternion[] SplitArray(HTuple data)
		{
			int num = data.Length / 4;
			HQuaternion[] array = new HQuaternion[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = new HQuaternion(new HData(data.TupleSelectRange(i * 4, (i + 1) * 4 - 1)));
			}
			return array;
		}

		public HQuaternion(HTuple axisX, HTuple axisY, HTuple axisZ, HTuple angle)
		{
			IntPtr proc = HalconAPI.PreCall(225);
			HalconAPI.Store(proc, 0, axisX);
			HalconAPI.Store(proc, 1, axisY);
			HalconAPI.Store(proc, 2, axisZ);
			HalconAPI.Store(proc, 3, angle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(axisX);
			HalconAPI.UnpinTuple(axisY);
			HalconAPI.UnpinTuple(axisZ);
			HalconAPI.UnpinTuple(angle);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HQuaternion(double axisX, double axisY, double axisZ, double angle)
		{
			IntPtr proc = HalconAPI.PreCall(225);
			HalconAPI.StoreD(proc, 0, axisX);
			HalconAPI.StoreD(proc, 1, axisY);
			HalconAPI.StoreD(proc, 2, axisZ);
			HalconAPI.StoreD(proc, 3, angle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeQuat();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HQuaternion(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeQuat(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeQuat().Serialize(stream);
		}

		public static HQuaternion Deserialize(Stream stream)
		{
			HQuaternion hQuaternion = new HQuaternion();
			hQuaternion.DeserializeQuat(HSerializedItem.Deserialize(stream));
			return hQuaternion;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HQuaternion Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeQuat();
			HQuaternion hQuaternion = new HQuaternion();
			hQuaternion.DeserializeQuat(hSerializedItem);
			hSerializedItem.Dispose();
			return hQuaternion;
		}

		public static HQuaternion operator *(HQuaternion left, HQuaternion right)
		{
			return left.QuatCompose(right);
		}

		public static implicit operator HPose(HQuaternion quaternion)
		{
			return quaternion.QuatToPose();
		}

		public static implicit operator HHomMat3D(HQuaternion quaternion)
		{
			return quaternion.QuatToHomMat3d();
		}

		public static HQuaternion operator ~(HQuaternion quaternion)
		{
			return quaternion.QuatConjugate();
		}

		public double QuatRotatePoint3d(double px, double py, double pz, out double qy, out double qz)
		{
			IntPtr proc = HalconAPI.PreCall(222);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.StoreD(proc, 3, pz);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out qy);
			err = HalconAPI.LoadD(proc, 2, err, out qz);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HQuaternion QuatConjugate()
		{
			IntPtr proc = HalconAPI.PreCall(223);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HQuaternion result = null;
			err = HQuaternion.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HQuaternion QuatNormalize()
		{
			IntPtr proc = HalconAPI.PreCall(224);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HQuaternion result = null;
			err = HQuaternion.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AxisAngleToQuat(HTuple axisX, HTuple axisY, HTuple axisZ, HTuple angle)
		{
			IntPtr proc = HalconAPI.PreCall(225);
			HalconAPI.Store(proc, 0, axisX);
			HalconAPI.Store(proc, 1, axisY);
			HalconAPI.Store(proc, 2, axisZ);
			HalconAPI.Store(proc, 3, angle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(axisX);
			HalconAPI.UnpinTuple(axisY);
			HalconAPI.UnpinTuple(axisZ);
			HalconAPI.UnpinTuple(angle);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void AxisAngleToQuat(double axisX, double axisY, double axisZ, double angle)
		{
			IntPtr proc = HalconAPI.PreCall(225);
			HalconAPI.StoreD(proc, 0, axisX);
			HalconAPI.StoreD(proc, 1, axisY);
			HalconAPI.StoreD(proc, 2, axisZ);
			HalconAPI.StoreD(proc, 3, angle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HPose QuatToPose()
		{
			IntPtr proc = HalconAPI.PreCall(226);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D QuatToHomMat3d()
		{
			IntPtr proc = HalconAPI.PreCall(229);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HQuaternion[] PoseToQuat(HPose[] pose)
		{
			HTuple hTuple = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(230);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			return HQuaternion.SplitArray(data);
		}

		public void PoseToQuat(HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(230);
			HalconAPI.Store(proc, 0, pose);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HQuaternion QuatInterpolate(HQuaternion quaternionEnd, HTuple interpPos)
		{
			IntPtr proc = HalconAPI.PreCall(231);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, quaternionEnd);
			HalconAPI.Store(proc, 2, interpPos);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(quaternionEnd);
			HalconAPI.UnpinTuple(interpPos);
			HQuaternion result = null;
			err = HQuaternion.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HQuaternion QuatCompose(HQuaternion quaternionRight)
		{
			IntPtr proc = HalconAPI.PreCall(232);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, quaternionRight);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(quaternionRight);
			HQuaternion result = null;
			err = HQuaternion.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializeQuat(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(237);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeQuat()
		{
			IntPtr proc = HalconAPI.PreCall(238);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}
	}
}
