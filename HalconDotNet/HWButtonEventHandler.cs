using System.Windows.Input;

namespace VisionConfig
{
	internal delegate void HWButtonEventHandler(int x, int y, MouseButton button, MouseButtonState state);
}
