using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HMessage : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMessage(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMessage obj)
		{
			obj = new HMessage(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMessage[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HMessage[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HMessage(hTuple[i].IP);
			}
			return err;
		}

		public HMessage()
		{
			IntPtr proc = HalconAPI.PreCall(541);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetMessageParam(string genParamName, HTuple key)
		{
			IntPtr proc = HalconAPI.PreCall(534);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetMessageParam(string genParamName, string key)
		{
			IntPtr proc = HalconAPI.PreCall(534);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetMessageParam(string genParamName, HTuple key, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(535);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, key);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMessageParam(string genParamName, string key, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(535);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, key);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HObject GetMessageObj(string key)
		{
			IntPtr proc = HalconAPI.PreCall(536);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, key);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HObject result = null;
			err = HObject.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetMessageObj(HObject objectData, string key)
		{
			IntPtr proc = HalconAPI.PreCall(537);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectData);
			HalconAPI.StoreS(proc, 1, key);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectData);
		}

		public HTuple GetMessageTuple(string key)
		{
			IntPtr proc = HalconAPI.PreCall(538);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetMessageTuple(string key, HTuple tupleData)
		{
			IntPtr proc = HalconAPI.PreCall(539);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, key);
			HalconAPI.Store(proc, 2, tupleData);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(tupleData);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateMessage()
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(541);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(540);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
