using System;

namespace VisionConfig
{
	internal class HDevParamGuard : IDisposable
	{
		protected IntPtr mThreadHandle;

		protected bool mGlobal;

		protected int mReferenceCount;

		public HDevParamGuard(IntPtr threadHandle, bool global)
		{
			this.mThreadHandle = threadHandle;
			this.mGlobal = global;
			this.mReferenceCount = 0;
			if (this.mGlobal)
			{
				HDevThread.HCkHLib(HalconAPI.HXThreadLockGlobalVar(this.mThreadHandle));
			}
			else
			{
				IntPtr intPtr = default(IntPtr);
				HDevThread.HCkHLib(HalconAPI.HXThreadLockLocalVar(this.mThreadHandle, out intPtr));
				this.mReferenceCount = intPtr.ToInt32();
			}
		}

		public bool IsAvailable()
		{
			if (!this.mGlobal)
			{
				return this.mReferenceCount > 1;
			}
			return true;
		}

		public void Dispose()
		{
			if (this.mGlobal)
			{
				HDevThread.HCkHLib(HalconAPI.HXThreadUnlockGlobalVar(this.mThreadHandle));
			}
			else
			{
				HDevThread.HCkHLib(HalconAPI.HXThreadUnlockLocalVar(this.mThreadHandle));
			}
		}
	}
}
