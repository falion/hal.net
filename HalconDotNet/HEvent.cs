using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HEvent : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HEvent()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HEvent(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HEvent obj)
		{
			obj = new HEvent(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HEvent[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HEvent[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HEvent(hTuple[i].IP);
			}
			return err;
		}

		public HEvent(HTuple attribName, HTuple attribValue)
		{
			IntPtr proc = HalconAPI.PreCall(558);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HEvent(string attribName, string attribValue)
		{
			IntPtr proc = HalconAPI.PreCall(558);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SignalEvent()
		{
			IntPtr proc = HalconAPI.PreCall(555);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int TryWaitEvent()
		{
			IntPtr proc = HalconAPI.PreCall(556);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void WaitEvent()
		{
			IntPtr proc = HalconAPI.PreCall(557);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateEvent(HTuple attribName, HTuple attribValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(558);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateEvent(string attribName, string attribValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(558);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(554);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
