using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HTuple : ISerializable, ICloneable
	{
		private delegate void NativeInt2To1(int[] in1, int[] in2, int[] buffer);

		private delegate void NativeLong2To1(long[] in1, long[] in2, long[] buffer);

		private delegate void NativeDouble2To1(double[] in1, double[] in2, double[] buffer);

		private enum ResultSize
		{
			EQUAL,
			SUM
		}

		internal HTupleImplementation data;

		private static NativeInt2To1 addInt = HTuple.NativeIntAdd;

		private static NativeLong2To1 addLong = HTuple.NativeLongAdd;

		private static NativeDouble2To1 addDouble = HTuple.NativeDoubleAdd;

		private static NativeInt2To1 subInt = HTuple.NativeIntSub;

		private static NativeLong2To1 subLong = HTuple.NativeLongSub;

		private static NativeDouble2To1 subDouble = HTuple.NativeDoubleSub;

		private static NativeInt2To1 multInt = HTuple.NativeIntMult;

		private static NativeLong2To1 multLong = HTuple.NativeLongMult;

		private static NativeDouble2To1 multDouble = HTuple.NativeDoubleMult;

		private static NativeInt2To1 concatInt = HTuple.NativeIntConcat;

		private static NativeLong2To1 concatLong = HTuple.NativeLongConcat;

		private static NativeDouble2To1 concatDouble = HTuple.NativeDoubleConcat;

		public HTupleType Type
		{
			get
			{
				return this.data.Type;
			}
		}

		public int Length
		{
			get
			{
				return this.data.Length;
			}
		}

		public HTupleElements this[int[] indices]
		{
			get
			{
				int num = 0;
				while (num < indices.Length)
				{
					int num2 = indices[num];
					if (num2 >= 0 && num2 < this.data.Length)
					{
						num++;
						continue;
					}
					throw new HTupleAccessException("Index out of range");
				}
				return this.data.GetElements(indices, this);
			}
			set
			{
				if (indices.Length == 0)
				{
					if (value.Length <= 1)
					{
						return;
					}
					throw new HTupleAccessException("Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				foreach (int num in indices)
				{
					if (num < 0)
					{
						throw new HTupleAccessException("Index out of range");
					}
				}
				if (this.data.Type == HTupleType.EMPTY)
				{
					switch (value.Type)
					{
					case HTupleType.INTEGER:
						this.data = new HTupleInt32(0);
						break;
					case HTupleType.LONG:
						this.data = new HTupleInt64(0L);
						break;
					case HTupleType.DOUBLE:
						this.data = new HTupleDouble(0.0);
						break;
					case HTupleType.STRING:
						this.data = new HTupleString("");
						break;
					case HTupleType.MIXED:
						this.data = new HTupleMixed(0);
						break;
					default:
						throw new HTupleAccessException("Inconsistent tuple state encountered");
					}
				}
				this.data.AssertSize(indices);
				if (value.Type != this.data.Type)
				{
					this.ConvertToMixed();
				}
				try
				{
					this.data.SetElements(indices, value);
				}
				catch (HTupleAccessException)
				{
					this.ConvertToMixed();
					this.data.SetElements(indices, value);
				}
			}
		}

		public HTupleElements this[int index]
		{
			get
			{
				if (index >= 0 && index < this.data.Length)
				{
					return this.data.GetElement(index, this);
				}
				throw new HTupleAccessException("Index out of range");
			}
			set
			{
				if (index < 0)
				{
					throw new HTupleAccessException("Index out of range");
				}
				if (this.data.Type == HTupleType.EMPTY)
				{
					switch (value.Type)
					{
					case HTupleType.INTEGER:
						this.data = new HTupleInt32(0);
						break;
					case HTupleType.LONG:
						this.data = new HTupleInt64(0L);
						break;
					case HTupleType.DOUBLE:
						this.data = new HTupleDouble(0.0);
						break;
					case HTupleType.STRING:
						this.data = new HTupleString("");
						break;
					default:
						throw new HTupleAccessException("Inconsistent tuple state encountered");
					}
				}
				this.data.AssertSize(index);
				if (value.Type != this.data.Type)
				{
					this.ConvertToMixed();
				}
				try
				{
					this.data.SetElements(new int[1]
					{
						index
					}, value);
				}
				catch (HTupleAccessException)
				{
					this.ConvertToMixed();
					this.data.SetElements(new int[1]
					{
						index
					}, value);
				}
			}
		}

		public HTupleElements this[HTuple indices]
		{
			get
			{
				return this[HTuple.GetIndicesFromTuple(indices)];
			}
			set
			{
				this[HTuple.GetIndicesFromTuple(indices)] = value;
			}
		}

		public int[] IArr
		{
			get
			{
				return this.data.IArr;
			}
			set
			{
				if (this.Type == HTupleType.INTEGER)
				{
					this.data.IArr = value;
				}
				else
				{
					this.data = new HTupleInt32(value, false);
				}
			}
		}

		public long[] LArr
		{
			get
			{
				return this.data.LArr;
			}
			set
			{
				if (this.Type == HTupleType.LONG)
				{
					this.data.LArr = value;
				}
				else
				{
					this.data = new HTupleInt64(value, false);
				}
			}
		}

		public double[] DArr
		{
			get
			{
				return this.data.DArr;
			}
			set
			{
				if (this.Type == HTupleType.DOUBLE)
				{
					this.data.DArr = value;
				}
				else
				{
					this.data = new HTupleDouble(value, false);
				}
			}
		}

		public string[] SArr
		{
			get
			{
				return this.data.SArr;
			}
			set
			{
				if (this.Type == HTupleType.STRING)
				{
					this.data.SArr = value;
				}
				else
				{
					this.data = new HTupleString(value, false);
				}
			}
		}

		public int I
		{
			get
			{
				return this[0].I;
			}
			set
			{
				this[0].I = value;
			}
		}

		public long L
		{
			get
			{
				return this[0].L;
			}
			set
			{
				this[0].L = value;
			}
		}

		public double D
		{
			get
			{
				return this[0].D;
			}
			set
			{
				this[0].D = value;
			}
		}

		public string S
		{
			get
			{
				return this[0].S;
			}
			set
			{
				this[0].S = value;
			}
		}

		public object O
		{
			get
			{
				return this[0].O;
			}
			set
			{
				this[0].O = value;
			}
		}

		public IntPtr IP
		{
			get
			{
				return this[0].IP;
			}
			set
			{
				this[0].IP = value;
			}
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeTuple();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTuple(SerializationInfo info, StreamingContext context)
		{
			byte[] array = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(array);
			HTuple hTuple = HTuple.DeserializeTuple(hSerializedItem);
			hSerializedItem.Dispose();
			this.data = hTuple.data;
		}

		public void Serialize(Stream stream)
		{
			this.SerializeTuple().Serialize(stream);
		}

		public static HTuple Deserialize(Stream stream)
		{
			return HTuple.DeserializeTuple(HSerializedItem.Deserialize(stream));
		}

		public HTuple()
		{
			this.data = HTupleVoid.EMPTY;
		}

		public HTuple(bool b)
			: this(new IntPtr(b ? 1 : 0))
		{
		}

		public HTuple(int i)
		{
			this.data = new HTupleInt32(i);
		}

		public HTuple(params int[] i)
		{
			this.data = new HTupleInt32(i, true);
		}

		public HTuple(long l)
		{
			this.data = new HTupleInt64(l);
		}

		public HTuple(params long[] l)
		{
			this.data = new HTupleInt64(l, true);
		}

		public HTuple(IntPtr ip)
			: this(new IntPtr[1]
			{
				ip
			})
		{
		}

		public HTuple(params IntPtr[] ip)
		{
			if (HalconAPI.isPlatform64)
			{
				long[] array = new long[ip.Length];
				for (int i = 0; i < ip.Length; i++)
				{
					array[i] = ip[i].ToInt64();
				}
				this.data = new HTupleInt64(array, false);
			}
			else
			{
				int[] array2 = new int[ip.Length];
				for (int j = 0; j < ip.Length; j++)
				{
					array2[j] = ip[j].ToInt32();
				}
				this.data = new HTupleInt32(array2, false);
			}
		}

		internal HTuple(int i, bool platformSize)
		{
			if (platformSize && HalconAPI.isPlatform64)
			{
				this.data = new HTupleInt64(i);
			}
			else
			{
				this.data = new HTupleInt32(i);
			}
		}

		public HTuple(double d)
		{
			this.data = new HTupleDouble(d);
		}

		public HTuple(params double[] d)
		{
			this.data = new HTupleDouble(d, true);
		}

		public HTuple(float f)
		{
			this.data = new HTupleDouble((double)f);
		}

		public HTuple(params float[] f)
		{
			this.data = new HTupleDouble(f);
		}

		public HTuple(string s)
		{
			this.data = new HTupleString(s);
		}

		public HTuple(params string[] s)
		{
			this.data = new HTupleString(s, true);
		}

		internal HTuple(object o)
		{
			this.data = new HTupleMixed(o);
		}

		public HTuple(params object[] o)
		{
			this.data = new HTupleMixed(o, true);
		}

		public HTuple(HTuple t)
		{
			switch (t.Type)
			{
			case HTupleType.EMPTY:
				this.data = HTupleVoid.EMPTY;
				break;
			case HTupleType.INTEGER:
				this.data = new HTupleInt32(t.ToIArr(), false);
				break;
			case HTupleType.LONG:
				this.data = new HTupleInt64(t.ToLArr(), false);
				break;
			case HTupleType.DOUBLE:
				this.data = new HTupleDouble(t.ToDArr(), false);
				break;
			case HTupleType.STRING:
				this.data = new HTupleString(t.ToSArr(), false);
				break;
			case HTupleType.MIXED:
				this.data = new HTupleMixed(t.ToOArr(), false);
				break;
			default:
				throw new HTupleAccessException("Inconsistent tuple state encountered");
			}
		}

		public HTuple(params HTuple[] t)
			: this(new HTuple().TupleConcat(t))
		{
		}

		internal HTuple(HTupleImplementation data)
		{
			this.data = data;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void TransferOwnership(HTuple source)
		{
			if (source != this)
			{
				if (source == null)
				{
					this.data = HTupleVoid.EMPTY;
				}
				else
				{
					this.data = source.data;
					source.data = HTupleVoid.EMPTY;
				}
			}
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HTuple Clone()
		{
			return new HTuple(this);
		}

		public void UnpinTuple()
		{
			this.data.UnpinTuple();
		}

		internal static int[] GetIndicesFromTuple(HTuple indices)
		{
			if (indices.Type != HTupleType.LONG && indices.Type != HTupleType.INTEGER)
			{
				int[] array = new int[indices.Length];
				for (int i = 0; i < indices.Length; i++)
				{
					if (indices[i].Type == HTupleType.INTEGER)
					{
						array[i] = indices[i].I;
						continue;
					}
					if (indices[i].Type == HTupleType.LONG)
					{
						array[i] = indices[i].I;
						continue;
					}
					if (indices[i].Type == HTupleType.DOUBLE)
					{
						double d = indices[i].D;
						int num = (int)d;
						if ((double)num != d)
						{
							throw new HTupleAccessException("Index has fractional part");
						}
						array[i] = num;
						continue;
					}
					throw new HTupleAccessException("Invalid index type");
				}
				return array;
			}
			return indices.ToIArr();
		}

		private void ConvertToMixed()
		{
			if (!(this.data is HTupleMixed))
			{
				this.data = new HTupleMixed(this.data);
			}
		}

		internal HTupleElementsMixed ConvertToMixed(int[] indices)
		{
			this.ConvertToMixed();
			return new HTupleElementsMixed((HTupleMixed)this.data, indices);
		}

		public int[] ToIArr()
		{
			return this.data.ToIArr();
		}

		public long[] ToLArr()
		{
			return this.data.ToLArr();
		}

		public double[] ToDArr()
		{
			return this.data.ToDArr();
		}

		public string[] ToSArr()
		{
			return this.data.ToSArr();
		}

		public object[] ToOArr()
		{
			return this.data.ToOArr();
		}

		public float[] ToFArr()
		{
			return this.data.ToFArr();
		}

		public IntPtr[] ToIPArr()
		{
			return this.data.ToIPArr();
		}

		public static implicit operator HTupleElements(HTuple t)
		{
			if (t.Length == 1)
			{
				return t[0];
			}
			int[] array = new int[t.Length];
			for (int i = 0; i < t.Length; i++)
			{
				array[i] = i;
			}
			return t[array];
		}

		public static implicit operator bool(HTuple t)
		{
			return t[0];
		}

		public static implicit operator int(HTuple t)
		{
			return t[0];
		}

		public static implicit operator long(HTuple t)
		{
			return t[0];
		}

		public static implicit operator double(HTuple t)
		{
			return t[0];
		}

		public static implicit operator string(HTuple t)
		{
			return t[0];
		}

		public static implicit operator IntPtr(HTuple t)
		{
			return t[0];
		}

		public static implicit operator int[](HTuple t)
		{
			return t.ToIArr();
		}

		public static implicit operator long[](HTuple t)
		{
			return t.ToLArr();
		}

		public static implicit operator double[](HTuple t)
		{
			return t.ToDArr();
		}

		public static implicit operator string[](HTuple t)
		{
			return t.ToSArr();
		}

		public static implicit operator IntPtr[](HTuple t)
		{
			return t.ToIPArr();
		}

		public static implicit operator HTuple(HTupleElements e)
		{
			switch (e.Type)
			{
			case HTupleType.INTEGER:
				return new HTuple(e.IArr);
			case HTupleType.LONG:
				return new HTuple(e.LArr);
			case HTupleType.DOUBLE:
				return new HTuple(e.DArr);
			case HTupleType.STRING:
				return new HTuple(e.SArr);
			case HTupleType.MIXED:
				return new HTuple(e.OArr);
			case HTupleType.EMPTY:
				return new HTuple();
			default:
				throw new HTupleAccessException("Inconsistent tuple state encountered");
			}
		}

		public static implicit operator HTuple(int i)
		{
			return new HTuple(i);
		}

		public static implicit operator HTuple(int[] i)
		{
			return new HTuple(i);
		}

		public static implicit operator HTuple(long l)
		{
			return new HTuple(l);
		}

		public static implicit operator HTuple(long[] l)
		{
			return new HTuple(l);
		}

		public static implicit operator HTuple(double d)
		{
			return new HTuple(d);
		}

		public static implicit operator HTuple(double[] d)
		{
			return new HTuple(d);
		}

		public static implicit operator HTuple(string s)
		{
			return new HTuple(s);
		}

		public static implicit operator HTuple(string[] s)
		{
			return new HTuple(s);
		}

		public static implicit operator HTuple(object[] o)
		{
			return new HTuple(o);
		}

		public static implicit operator HTuple(IntPtr ip)
		{
			return new HTuple(ip);
		}

		public static implicit operator HTuple(IntPtr[] ip)
		{
			return new HTuple(ip);
		}

		internal void Store(IntPtr proc, int parIndex)
		{
			this.data.Store(proc, parIndex);
		}

		internal int Load(IntPtr proc, int parIndex, HTupleType type, int err)
		{
			if (HalconAPI.IsFailure(err))
			{
				this.data = HTupleVoid.EMPTY;
				return err;
			}
			return HTupleImplementation.Load(proc, parIndex, type, out this.data);
		}

		internal int Load(IntPtr proc, int parIndex, int err)
		{
			return this.Load(proc, parIndex, HTupleType.MIXED, err);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, HTupleType type, int err, out HTuple tuple)
		{
			tuple = new HTuple();
			return tuple.Load(proc, parIndex, type, err);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, int err, out HTuple tuple)
		{
			tuple = new HTuple();
			return tuple.Load(proc, parIndex, HTupleType.MIXED, err);
		}

		public override string ToString()
		{
			object[] array = this.ToOArr();
			string text = "";
			if (this.Length != 1)
			{
				text += "[";
			}
			for (int i = 0; i < array.Length; i++)
			{
				text += ((i > 0) ? ", " : "");
				text = ((this[i].Type != HTupleType.STRING) ? (text + array[i].ToString()) : (text + "\"" + array[i].ToString() + "\""));
			}
			if (this.Length != 1)
			{
				text += "]";
			}
			return text;
		}

		public static HTuple operator +(HTuple t1, HTuple t2)
		{
			return t1.TupleAdd(t2);
		}

		public static HTuple operator +(HTuple t1, int t2)
		{
			return t1 + (HTuple)t2;
		}

		public static HTuple operator +(HTuple t1, long t2)
		{
			return t1 + (HTuple)t2;
		}

		public static HTuple operator +(HTuple t1, float t2)
		{
			return t1 + (HTuple)(double)t2;
		}

		public static HTuple operator +(HTuple t1, double t2)
		{
			return t1 + (HTuple)t2;
		}

		public static HTuple operator +(HTuple t1, string t2)
		{
			return t1 + (HTuple)t2;
		}

		public static HTuple operator +(HTuple t1, HTupleElements t2)
		{
			return t1 + (HTuple)t2;
		}

		public static HTuple operator -(HTuple t1, HTuple t2)
		{
			return t1.TupleSub(t2);
		}

		public static HTuple operator -(HTuple t1, int t2)
		{
			return t1 - (HTuple)t2;
		}

		public static HTuple operator -(HTuple t1, long t2)
		{
			return t1 - (HTuple)t2;
		}

		public static HTuple operator -(HTuple t1, float t2)
		{
			return t1 - (HTuple)(double)t2;
		}

		public static HTuple operator -(HTuple t1, double t2)
		{
			return t1 - (HTuple)t2;
		}

		public static HTuple operator -(HTuple t1, string t2)
		{
			return t1 - (HTuple)t2;
		}

		public static HTuple operator -(HTuple t1, HTupleElements t2)
		{
			return t1 - (HTuple)t2;
		}

		public static HTuple operator *(HTuple t1, HTuple t2)
		{
			return t1.TupleMult(t2);
		}

		public static HTuple operator *(HTuple t1, int t2)
		{
			return t1 * (HTuple)t2;
		}

		public static HTuple operator *(HTuple t1, long t2)
		{
			return t1 * (HTuple)t2;
		}

		public static HTuple operator *(HTuple t1, float t2)
		{
			return t1 * (HTuple)(double)t2;
		}

		public static HTuple operator *(HTuple t1, double t2)
		{
			return t1 * (HTuple)t2;
		}

		public static HTuple operator *(HTuple t1, string t2)
		{
			return t1 * (HTuple)t2;
		}

		public static HTuple operator *(HTuple t1, HTupleElements t2)
		{
			return t1 * (HTuple)t2;
		}

		public static HTuple operator /(HTuple t1, HTuple t2)
		{
			return t1.TupleDiv(t2);
		}

		public static HTuple operator /(HTuple t1, int t2)
		{
			return t1 / (HTuple)t2;
		}

		public static HTuple operator /(HTuple t1, long t2)
		{
			return t1 / (HTuple)t2;
		}

		public static HTuple operator /(HTuple t1, float t2)
		{
			return t1 / (HTuple)(double)t2;
		}

		public static HTuple operator /(HTuple t1, double t2)
		{
			return t1 / (HTuple)t2;
		}

		public static HTuple operator /(HTuple t1, string t2)
		{
			return t1 / (HTuple)t2;
		}

		public static HTuple operator /(HTuple t1, HTupleElements t2)
		{
			return t1 / (HTuple)t2;
		}

		public static HTuple operator %(HTuple t1, HTuple t2)
		{
			return t1.TupleMod(t2);
		}

		public static HTuple operator %(HTuple t1, int t2)
		{
			return t1 % (HTuple)t2;
		}

		public static HTuple operator %(HTuple t1, long t2)
		{
			return t1 % (HTuple)t2;
		}

		public static HTuple operator %(HTuple t1, float t2)
		{
			return t1 % (HTuple)(double)t2;
		}

		public static HTuple operator %(HTuple t1, double t2)
		{
			return t1 % (HTuple)t2;
		}

		public static HTuple operator %(HTuple t1, string t2)
		{
			return t1 % (HTuple)t2;
		}

		public static HTuple operator %(HTuple t1, HTupleElements t2)
		{
			return t1 % (HTuple)t2;
		}

		public static HTuple operator &(HTuple t1, HTuple t2)
		{
			return t1.TupleAnd(t2);
		}

		public static HTuple operator &(HTuple t1, int t2)
		{
			return t1 & (HTuple)t2;
		}

		public static HTuple operator &(HTuple t1, long t2)
		{
			return t1 & (HTuple)t2;
		}

		public static HTuple operator &(HTuple t1, float t2)
		{
			return t1 & (HTuple)(double)t2;
		}

		public static HTuple operator &(HTuple t1, double t2)
		{
			return t1 & (HTuple)t2;
		}

		public static HTuple operator &(HTuple t1, string t2)
		{
			return t1 & (HTuple)t2;
		}

		public static HTuple operator &(HTuple t1, HTupleElements t2)
		{
			return t1 & (HTuple)t2;
		}

		public static HTuple operator |(HTuple t1, HTuple t2)
		{
			return t1.TupleOr(t2);
		}

		public static HTuple operator |(HTuple t1, int t2)
		{
			return t1 | (HTuple)t2;
		}

		public static HTuple operator |(HTuple t1, long t2)
		{
			return t1 | (HTuple)t2;
		}

		public static HTuple operator |(HTuple t1, float t2)
		{
			return t1 | (HTuple)(double)t2;
		}

		public static HTuple operator |(HTuple t1, double t2)
		{
			return t1 | (HTuple)t2;
		}

		public static HTuple operator |(HTuple t1, string t2)
		{
			return t1 | (HTuple)t2;
		}

		public static HTuple operator |(HTuple t1, HTupleElements t2)
		{
			return t1 | (HTuple)t2;
		}

		public static HTuple operator ^(HTuple t1, HTuple t2)
		{
			return t1.TupleXor(t2);
		}

		public static HTuple operator ^(HTuple t1, int t2)
		{
			return t1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTuple t1, long t2)
		{
			return t1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTuple t1, float t2)
		{
			return t1 ^ (HTuple)(double)t2;
		}

		public static HTuple operator ^(HTuple t1, double t2)
		{
			return t1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTuple t1, string t2)
		{
			return t1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTuple t1, HTupleElements t2)
		{
			return t1 ^ (HTuple)t2;
		}

		public static bool operator false(HTuple t)
		{
			return !(bool)t;
		}

		public static bool operator true(HTuple t)
		{
			return t;
		}

		public static HTuple operator <<(HTuple t1, int shift)
		{
			return t1.TupleLsh(shift);
		}

		public static HTuple operator >>(HTuple t1, int shift)
		{
			return t1.TupleRsh(shift);
		}

		public static bool operator <(HTuple t1, HTuple t2)
		{
			return (int)t1.TupleLess(t2) != 0;
		}

		public static bool operator <(HTuple t1, int t2)
		{
			return t1 < (HTuple)t2;
		}

		public static bool operator <(HTuple t1, long t2)
		{
			return t1 < (HTuple)t2;
		}

		public static bool operator <(HTuple t1, float t2)
		{
			return t1 < (HTuple)(double)t2;
		}

		public static bool operator <(HTuple t1, double t2)
		{
			return t1 < (HTuple)t2;
		}

		public static bool operator <(HTuple t1, string t2)
		{
			return t1 < (HTuple)t2;
		}

		public static bool operator <(HTuple t1, HTupleElements t2)
		{
			return t1 < (HTuple)t2;
		}

		public static bool operator >(HTuple t1, HTuple t2)
		{
			return (int)t1.TupleGreater(t2) != 0;
		}

		public static bool operator >(HTuple t1, int t2)
		{
			return t1 > (HTuple)t2;
		}

		public static bool operator >(HTuple t1, long t2)
		{
			return t1 > (HTuple)t2;
		}

		public static bool operator >(HTuple t1, float t2)
		{
			return t1 > (HTuple)(double)t2;
		}

		public static bool operator >(HTuple t1, double t2)
		{
			return t1 > (HTuple)t2;
		}

		public static bool operator >(HTuple t1, string t2)
		{
			return t1 > (HTuple)t2;
		}

		public static bool operator >(HTuple t1, HTupleElements t2)
		{
			return t1 > (HTuple)t2;
		}

		public static bool operator >=(HTuple t1, HTuple t2)
		{
			return !(t1 < t2);
		}

		public static bool operator >=(HTuple t1, int t2)
		{
			return t1 >= (HTuple)t2;
		}

		public static bool operator >=(HTuple t1, long t2)
		{
			return t1 >= (HTuple)t2;
		}

		public static bool operator >=(HTuple t1, float t2)
		{
			return t1 >= (HTuple)(double)t2;
		}

		public static bool operator >=(HTuple t1, double t2)
		{
			return t1 >= (HTuple)t2;
		}

		public static bool operator >=(HTuple t1, string t2)
		{
			return t1 >= (HTuple)t2;
		}

		public static bool operator >=(HTuple t1, HTupleElements t2)
		{
			return t1 >= (HTuple)t2;
		}

		public static bool operator <=(HTuple t1, HTuple t2)
		{
			return !(t1 > t2);
		}

		public static bool operator <=(HTuple t1, int t2)
		{
			return t1 <= (HTuple)t2;
		}

		public static bool operator <=(HTuple t1, long t2)
		{
			return t1 <= (HTuple)t2;
		}

		public static bool operator <=(HTuple t1, float t2)
		{
			return t1 <= (HTuple)(double)t2;
		}

		public static bool operator <=(HTuple t1, double t2)
		{
			return t1 <= (HTuple)t2;
		}

		public static bool operator <=(HTuple t1, string t2)
		{
			return t1 <= (HTuple)t2;
		}

		public static bool operator <=(HTuple t1, HTupleElements t2)
		{
			return t1 <= (HTuple)t2;
		}

		public static HTuple operator -(HTuple t)
		{
			return t.TupleNeg();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool Continue(HTuple final_value, HTuple increment)
		{
			if (increment[0] < 0.0)
			{
				return this[0].D >= final_value[0].D;
			}
			return this[0].D <= final_value[0].D;
		}

		public HTuple TupleConcat(params HTuple[] tuples)
		{
			HTuple hTuple = this;
			for (int i = 0; i < tuples.Length; i++)
			{
				hTuple = hTuple.TupleConcat(tuples[i]);
			}
			return hTuple;
		}

		public void Append(HTuple tuple)
		{
			this.data = this.TupleConcat(tuple).data;
		}

		private bool ProcessNative2To1(HTuple t2, ResultSize type, out HTuple result, NativeInt2To1 opInt, NativeLong2To1 opLong, NativeDouble2To1 opDouble)
		{
			int num = (type == ResultSize.EQUAL) ? this.Length : (this.Length + t2.Length);
			if (num == 0)
			{
				result = new HTuple();
				return true;
			}
			if (this.Type == t2.Type && (this.Length == t2.Length || type == ResultSize.SUM))
			{
				if (this.Type == HTupleType.DOUBLE && opDouble != null)
				{
					double[] dArr = this.DArr;
					double[] dArr2 = t2.DArr;
					double[] array = new double[num];
					array[0] = (double)this.Length;
					opDouble(dArr, dArr2, array);
					result = new HTuple(new HTupleDouble(array, false));
					return true;
				}
				if (this.Type == HTupleType.INTEGER && opInt != null)
				{
					int[] iArr = this.IArr;
					int[] iArr2 = t2.IArr;
					int[] array2 = new int[num];
					array2[0] = this.Length;
					opInt(iArr, iArr2, array2);
					result = new HTuple(new HTupleInt32(array2, false));
					return true;
				}
				if (this.Type == HTupleType.LONG && opLong != null)
				{
					long[] lArr = this.LArr;
					long[] lArr2 = t2.LArr;
					long[] array3 = new long[num];
					array3[0] = this.Length;
					opLong(lArr, lArr2, array3);
					result = new HTuple(new HTupleInt64(array3, false));
					return true;
				}
			}
			result = null;
			return false;
		}

		private static void NativeIntAdd(int[] in1, int[] in2, int[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] + in2[i];
			}
		}

		private static void NativeLongAdd(long[] in1, long[] in2, long[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] + in2[i];
			}
		}

		private static void NativeDoubleAdd(double[] in1, double[] in2, double[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] + in2[i];
			}
		}

		private static void NativeIntSub(int[] in1, int[] in2, int[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] - in2[i];
			}
		}

		private static void NativeLongSub(long[] in1, long[] in2, long[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] - in2[i];
			}
		}

		private static void NativeDoubleSub(double[] in1, double[] in2, double[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] - in2[i];
			}
		}

		private static void NativeIntMult(int[] in1, int[] in2, int[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] * in2[i];
			}
		}

		private static void NativeLongMult(long[] in1, long[] in2, long[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] * in2[i];
			}
		}

		private static void NativeDoubleMult(double[] in1, double[] in2, double[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] * in2[i];
			}
		}

		private static void NativeIntConcat(int[] in1, int[] in2, int[] buffer)
		{
			int num = buffer[0];
			int length = buffer.Length - num;
			Array.Copy(in1, 0, buffer, 0, num);
			Array.Copy(in2, 0, buffer, num, length);
		}

		private static void NativeLongConcat(long[] in1, long[] in2, long[] buffer)
		{
			int num = (int)buffer[0];
			int length = buffer.Length - num;
			Array.Copy(in1, 0, buffer, 0, num);
			Array.Copy(in2, 0, buffer, num, length);
		}

		private static void NativeDoubleConcat(double[] in1, double[] in2, double[] buffer)
		{
			int num = (int)buffer[0];
			int length = buffer.Length - num;
			Array.Copy(in1, 0, buffer, 0, num);
			Array.Copy(in2, 0, buffer, num, length);
		}

		public int TupleLength()
		{
			return this.Length;
		}

		public HTuple TupleAdd(HTuple s2)
		{
			HTuple result = null;
			if (!this.ProcessNative2To1(s2, ResultSize.EQUAL, out result, HTuple.addInt, HTuple.addLong, HTuple.addDouble))
			{
				result = this.TupleAddOp(s2);
				return result;
			}
			return result;
		}

		public HTuple TupleSub(HTuple d2)
		{
			HTuple result = null;
			if (!this.ProcessNative2To1(d2, ResultSize.EQUAL, out result, HTuple.subInt, HTuple.subLong, HTuple.subDouble))
			{
				result = this.TupleSubOp(d2);
				return result;
			}
			return result;
		}

		public HTuple TupleMult(HTuple p2)
		{
			HTuple result = null;
			if (!this.ProcessNative2To1(p2, ResultSize.EQUAL, out result, HTuple.multInt, HTuple.multLong, HTuple.multDouble))
			{
				result = this.TupleMultOp(p2);
				return result;
			}
			return result;
		}

		public HTuple TupleConcat(HTuple t2)
		{
			HTuple result = null;
			if (!this.ProcessNative2To1(t2, ResultSize.SUM, out result, HTuple.concatInt, HTuple.concatLong, HTuple.concatDouble))
			{
				result = this.TupleConcatOp(t2);
				return result;
			}
			return result;
		}

		public HTuple TupleUnion(HTuple set2)
		{
			IntPtr proc = HalconAPI.PreCall(96);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, set2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(set2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleIntersection(HTuple set2)
		{
			IntPtr proc = HalconAPI.PreCall(97);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, set2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(set2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleDifference(HTuple set2)
		{
			IntPtr proc = HalconAPI.PreCall(98);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, set2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(set2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSymmdiff(HTuple set2)
		{
			IntPtr proc = HalconAPI.PreCall(99);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, set2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(set2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleIsStringElem()
		{
			IntPtr proc = HalconAPI.PreCall(100);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleIsRealElem()
		{
			IntPtr proc = HalconAPI.PreCall(101);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleIsIntElem()
		{
			IntPtr proc = HalconAPI.PreCall(102);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleTypeElem()
		{
			IntPtr proc = HalconAPI.PreCall(103);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleIsMixed()
		{
			IntPtr proc = HalconAPI.PreCall(104);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleIsString()
		{
			IntPtr proc = HalconAPI.PreCall(105);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleIsReal()
		{
			IntPtr proc = HalconAPI.PreCall(106);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleIsInt()
		{
			IntPtr proc = HalconAPI.PreCall(107);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleType()
		{
			IntPtr proc = HalconAPI.PreCall(108);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleHistoRange(HTuple min, HTuple max, HTuple numBins, out HTuple binSize)
		{
			IntPtr proc = HalconAPI.PreCall(109);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, min);
			HalconAPI.Store(proc, 2, max);
			HalconAPI.Store(proc, 3, numBins);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			HalconAPI.UnpinTuple(numBins);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out binSize);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleRegexpSelect(HTuple expression)
		{
			IntPtr proc = HalconAPI.PreCall(110);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, expression);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(expression);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleRegexpTest(HTuple expression)
		{
			IntPtr proc = HalconAPI.PreCall(111);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, expression);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(expression);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleRegexpReplace(HTuple expression, HTuple replace)
		{
			IntPtr proc = HalconAPI.PreCall(112);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, expression);
			HalconAPI.Store(proc, 2, replace);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(expression);
			HalconAPI.UnpinTuple(replace);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleRegexpMatch(HTuple expression)
		{
			IntPtr proc = HalconAPI.PreCall(113);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, expression);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(expression);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple TupleRand(HTuple length)
		{
			IntPtr proc = HalconAPI.PreCall(114);
			HalconAPI.Store(proc, 0, length);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(length);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public HTuple TupleSgn()
		{
			IntPtr proc = HalconAPI.PreCall(116);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleMax2(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(117);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleMin2(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(118);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleMax()
		{
			IntPtr proc = HalconAPI.PreCall(119);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleMin()
		{
			IntPtr proc = HalconAPI.PreCall(120);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleCumul()
		{
			IntPtr proc = HalconAPI.PreCall(121);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSelectRank(HTuple rankIndex)
		{
			IntPtr proc = HalconAPI.PreCall(122);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, rankIndex);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(rankIndex);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleMedian()
		{
			IntPtr proc = HalconAPI.PreCall(123);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSum()
		{
			IntPtr proc = HalconAPI.PreCall(124);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleMean()
		{
			IntPtr proc = HalconAPI.PreCall(125);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleDeviation()
		{
			IntPtr proc = HalconAPI.PreCall(126);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleUniq()
		{
			IntPtr proc = HalconAPI.PreCall(127);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleFindLast(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(128);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleFindFirst(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(129);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleFind(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(130);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSortIndex()
		{
			IntPtr proc = HalconAPI.PreCall(131);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSort()
		{
			IntPtr proc = HalconAPI.PreCall(132);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleInverse()
		{
			IntPtr proc = HalconAPI.PreCall(133);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		private HTuple TupleConcatOp(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(134);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSelectRange(HTuple leftindex, HTuple rightindex)
		{
			IntPtr proc = HalconAPI.PreCall(135);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, leftindex);
			HalconAPI.Store(proc, 2, rightindex);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(leftindex);
			HalconAPI.UnpinTuple(rightindex);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleLastN(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(136);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(index);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleFirstN(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(137);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(index);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleInsert(HTuple index, HTuple insertTuple)
		{
			IntPtr proc = HalconAPI.PreCall(138);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, insertTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(insertTuple);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleReplace(HTuple index, HTuple replaceTuple)
		{
			IntPtr proc = HalconAPI.PreCall(139);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, replaceTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(replaceTuple);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleRemove(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(140);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(index);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSelectMask(HTuple mask)
		{
			IntPtr proc = HalconAPI.PreCall(141);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, mask);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(mask);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSelect(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(142);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(index);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleStrBitSelect(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(143);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(index);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple TupleGenSequence(HTuple start, HTuple end, HTuple step)
		{
			IntPtr proc = HalconAPI.PreCall(144);
			HalconAPI.Store(proc, 0, start);
			HalconAPI.Store(proc, 1, end);
			HalconAPI.Store(proc, 2, step);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(start);
			HalconAPI.UnpinTuple(end);
			HalconAPI.UnpinTuple(step);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple TupleGenConst(HTuple length, HTuple constVal)
		{
			IntPtr proc = HalconAPI.PreCall(145);
			HalconAPI.Store(proc, 0, length);
			HalconAPI.Store(proc, 1, constVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(length);
			HalconAPI.UnpinTuple(constVal);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public HTuple TupleEnvironment()
		{
			IntPtr proc = HalconAPI.PreCall(146);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSplit(HTuple separator)
		{
			IntPtr proc = HalconAPI.PreCall(147);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, separator);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(separator);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSubstr(HTuple position1, HTuple position2)
		{
			IntPtr proc = HalconAPI.PreCall(148);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, position1);
			HalconAPI.Store(proc, 2, position2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(position1);
			HalconAPI.UnpinTuple(position2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleStrLastN(HTuple position)
		{
			IntPtr proc = HalconAPI.PreCall(149);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, position);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(position);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleStrFirstN(HTuple position)
		{
			IntPtr proc = HalconAPI.PreCall(150);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, position);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(position);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleStrrchr(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(151);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleStrchr(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(152);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleStrrstr(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(153);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleStrstr(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(154);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleStrlen()
		{
			IntPtr proc = HalconAPI.PreCall(155);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleLessEqualElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(156);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleLessElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(157);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleGreaterEqualElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(158);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleGreaterElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(159);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleNotEqualElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(160);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleEqualElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(161);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleLessEqual(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(162);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleLess(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(163);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleGreaterEqual(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(164);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleGreater(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(165);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleNotEqual(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(166);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleEqual(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(167);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleNot()
		{
			IntPtr proc = HalconAPI.PreCall(168);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleXor(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(169);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleOr(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(170);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleAnd(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(171);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleBnot()
		{
			IntPtr proc = HalconAPI.PreCall(172);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleBxor(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(173);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleBor(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(174);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleBand(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(175);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleRsh(HTuple shift)
		{
			IntPtr proc = HalconAPI.PreCall(176);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, shift);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(shift);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleLsh(HTuple shift)
		{
			IntPtr proc = HalconAPI.PreCall(177);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, shift);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(shift);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleChrt()
		{
			IntPtr proc = HalconAPI.PreCall(178);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleOrds()
		{
			IntPtr proc = HalconAPI.PreCall(179);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleChr()
		{
			IntPtr proc = HalconAPI.PreCall(180);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleOrd()
		{
			IntPtr proc = HalconAPI.PreCall(181);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleString(HTuple format)
		{
			IntPtr proc = HalconAPI.PreCall(182);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, format);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(format);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleIsNumber()
		{
			IntPtr proc = HalconAPI.PreCall(183);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleNumber()
		{
			IntPtr proc = HalconAPI.PreCall(184);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleRound()
		{
			IntPtr proc = HalconAPI.PreCall(185);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleInt()
		{
			IntPtr proc = HalconAPI.PreCall(186);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleReal()
		{
			IntPtr proc = HalconAPI.PreCall(187);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleLdexp(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(188);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleFmod(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(189);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleMod(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(190);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleCeil()
		{
			IntPtr proc = HalconAPI.PreCall(191);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleFloor()
		{
			IntPtr proc = HalconAPI.PreCall(192);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TuplePow(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(193);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleLog10()
		{
			IntPtr proc = HalconAPI.PreCall(194);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleLog()
		{
			IntPtr proc = HalconAPI.PreCall(195);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleExp()
		{
			IntPtr proc = HalconAPI.PreCall(196);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleTanh()
		{
			IntPtr proc = HalconAPI.PreCall(197);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleCosh()
		{
			IntPtr proc = HalconAPI.PreCall(198);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSinh()
		{
			IntPtr proc = HalconAPI.PreCall(199);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleRad()
		{
			IntPtr proc = HalconAPI.PreCall(200);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleDeg()
		{
			IntPtr proc = HalconAPI.PreCall(201);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleAtan2(HTuple x)
		{
			IntPtr proc = HalconAPI.PreCall(202);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, x);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(x);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleAtan()
		{
			IntPtr proc = HalconAPI.PreCall(203);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleAcos()
		{
			IntPtr proc = HalconAPI.PreCall(204);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleAsin()
		{
			IntPtr proc = HalconAPI.PreCall(205);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleTan()
		{
			IntPtr proc = HalconAPI.PreCall(206);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleCos()
		{
			IntPtr proc = HalconAPI.PreCall(207);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSin()
		{
			IntPtr proc = HalconAPI.PreCall(208);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleFabs()
		{
			IntPtr proc = HalconAPI.PreCall(209);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleSqrt()
		{
			IntPtr proc = HalconAPI.PreCall(210);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleAbs()
		{
			IntPtr proc = HalconAPI.PreCall(211);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleNeg()
		{
			IntPtr proc = HalconAPI.PreCall(212);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TupleDiv(HTuple q2)
		{
			IntPtr proc = HalconAPI.PreCall(213);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, q2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(q2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		private HTuple TupleMultOp(HTuple p2)
		{
			IntPtr proc = HalconAPI.PreCall(214);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, p2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(p2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		private HTuple TupleSubOp(HTuple d2)
		{
			IntPtr proc = HalconAPI.PreCall(215);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, d2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(d2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		private HTuple TupleAddOp(HTuple s2)
		{
			IntPtr proc = HalconAPI.PreCall(216);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, s2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(s2);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple DeserializeTuple(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(217);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(serializedItemHandle);
			return result;
		}

		public HSerializedItem SerializeTuple()
		{
			IntPtr proc = HalconAPI.PreCall(218);
			this.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void WriteTuple(HTuple fileName)
		{
			IntPtr proc = HalconAPI.PreCall(219);
			this.Store(proc, 0);
			HalconAPI.Store(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			this.UnpinTuple();
			HalconAPI.UnpinTuple(fileName);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static HTuple ReadTuple(HTuple fileName)
		{
			IntPtr proc = HalconAPI.PreCall(220);
			HalconAPI.Store(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fileName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}
	}
}
