using System;
using System.ComponentModel;

namespace VisionConfig
{
	[Serializable]
	public class HXLDPara : HXLD
	{
		public new HXLDPara this[HTuple index]
		{
			get
			{
				return this.SelectObj(index);
			}
		}

		public HXLDPara()
			: base(HObjectBase.UNDEF, false)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDPara(IntPtr key)
			: this(key, true)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDPara(IntPtr key, bool copy)
			: base(key, copy)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDPara(HObject obj)
			: base(obj)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		private void AssertObjectClass()
		{
			HalconAPI.AssertObjectClass(base.key, "xld_para");
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, int err, out HXLDPara obj)
		{
			obj = new HXLDPara(HObjectBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		public HXLDModPara ModParallelsXld(HImage image, out HXLDExtPara extParallels, HTuple quality, int minGray, int maxGray, HTuple maxStandard)
		{
			IntPtr proc = HalconAPI.PreCall(39);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, quality);
			HalconAPI.StoreI(proc, 1, minGray);
			HalconAPI.StoreI(proc, 2, maxGray);
			HalconAPI.Store(proc, 3, maxStandard);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(quality);
			HalconAPI.UnpinTuple(maxStandard);
			HXLDModPara result = null;
			err = HXLDModPara.LoadNew(proc, 1, err, out result);
			err = HXLDExtPara.LoadNew(proc, 2, err, out extParallels);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HXLDModPara ModParallelsXld(HImage image, out HXLDExtPara extParallels, double quality, int minGray, int maxGray, double maxStandard)
		{
			IntPtr proc = HalconAPI.PreCall(39);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreD(proc, 0, quality);
			HalconAPI.StoreI(proc, 1, minGray);
			HalconAPI.StoreI(proc, 2, maxGray);
			HalconAPI.StoreD(proc, 3, maxStandard);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HXLDModPara result = null;
			err = HXLDModPara.LoadNew(proc, 1, err, out result);
			err = HXLDExtPara.LoadNew(proc, 2, err, out extParallels);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void InfoParallelsXld(HImage image, out double qualityMin, out double qualityMax, out int grayMin, out int grayMax, out double standardMin, out double standardMax)
		{
			IntPtr proc = HalconAPI.PreCall(40);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out qualityMin);
			err = HalconAPI.LoadD(proc, 1, err, out qualityMax);
			err = HalconAPI.LoadI(proc, 2, err, out grayMin);
			err = HalconAPI.LoadI(proc, 3, err, out grayMax);
			err = HalconAPI.LoadD(proc, 4, err, out standardMin);
			err = HalconAPI.LoadD(proc, 5, err, out standardMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HXLDPara ObjDiff(HXLDPara objectsSub)
		{
			IntPtr proc = HalconAPI.PreCall(573);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsSub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsSub);
			return result;
		}

		public new HXLDPara CopyObj(int index, int numObj)
		{
			IntPtr proc = HalconAPI.PreCall(583);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.StoreI(proc, 1, numObj);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPara ConcatObj(HXLDPara objects2)
		{
			IntPtr proc = HalconAPI.PreCall(584);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public new HXLDPara SelectObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPara SelectObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int CompareObj(HXLDPara objects2, HTuple epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.Store(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(epsilon);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public int CompareObj(HXLDPara objects2, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.StoreD(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public int TestEqualObj(HXLDPara objects2)
		{
			IntPtr proc = HalconAPI.PreCall(591);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLDPara meshes, int gridSpacing, HTuple rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.Store(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HXLDPara.LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLDPara meshes, int gridSpacing, string rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.StoreS(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HXLDPara.LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public new HXLDPara SelectXldPoint(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPara SelectXldPoint(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPara SelectShapeXld(HTuple features, string operation, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.Store(proc, 2, min);
			HalconAPI.Store(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPara SelectShapeXld(string features, string operation, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.StoreD(proc, 2, min);
			HalconAPI.StoreD(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPara ShapeTransXld(string type)
		{
			IntPtr proc = HalconAPI.PreCall(1689);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPara InsertObj(HXLDPara objectsInsert, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2121);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsInsert);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsInsert);
			return result;
		}

		public new HXLDPara RemoveObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPara RemoveObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPara ReplaceObj(HXLDPara objectsReplace, HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return result;
		}

		public HXLDPara ReplaceObj(HXLDPara objectsReplace, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return result;
		}
	}
}
