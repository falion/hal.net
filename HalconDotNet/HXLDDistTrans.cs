using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HXLDDistTrans : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDDistTrans()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDDistTrans(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HXLDDistTrans obj)
		{
			obj = new HXLDDistTrans(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HXLDDistTrans[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HXLDDistTrans[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HXLDDistTrans(hTuple[i].IP);
			}
			return err;
		}

		public HXLDDistTrans(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1353);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDDistTrans(HXLDCont contour, string mode, HTuple maxDistance)
		{
			IntPtr proc = HalconAPI.PreCall(1360);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, maxDistance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maxDistance);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
		}

		public HXLDDistTrans(HXLDCont contour, string mode, double maxDistance)
		{
			IntPtr proc = HalconAPI.PreCall(1360);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, maxDistance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeDistanceTransformXld();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDDistTrans(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeDistanceTransformXld(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeDistanceTransformXld().Serialize(stream);
		}

		public static HXLDDistTrans Deserialize(Stream stream)
		{
			HXLDDistTrans hXLDDistTrans = new HXLDDistTrans();
			hXLDDistTrans.DeserializeDistanceTransformXld(HSerializedItem.Deserialize(stream));
			return hXLDDistTrans;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HXLDDistTrans Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeDistanceTransformXld();
			HXLDDistTrans hXLDDistTrans = new HXLDDistTrans();
			hXLDDistTrans.DeserializeDistanceTransformXld(hSerializedItem);
			hSerializedItem.Dispose();
			return hXLDDistTrans;
		}

		public HXLDCont ApplyDistanceTransformXld(HXLDCont contour)
		{
			IntPtr proc = HalconAPI.PreCall(1352);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
			return result;
		}

		public void ReadDistanceTransformXld(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1353);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DeserializeDistanceTransformXld(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1354);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeDistanceTransformXld()
		{
			IntPtr proc = HalconAPI.PreCall(1355);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void WriteDistanceTransformXld(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1356);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDistanceTransformXldParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1357);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDistanceTransformXldParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1357);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetDistanceTransformXldParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1358);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetDistanceTransformXldParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1358);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont GetDistanceTransformXldContour()
		{
			IntPtr proc = HalconAPI.PreCall(1359);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void CreateDistanceTransformXld(HXLDCont contour, string mode, HTuple maxDistance)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1360);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, maxDistance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maxDistance);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
		}

		public void CreateDistanceTransformXld(HXLDCont contour, string mode, double maxDistance)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1360);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, maxDistance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1351);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
