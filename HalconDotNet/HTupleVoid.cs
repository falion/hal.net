using System;

namespace VisionConfig
{
	internal class HTupleVoid : HTupleImplementation
	{
		public static HTupleVoid EMPTY = new HTupleVoid();

		public override HTupleType Type
		{
			get
			{
				return HTupleType.EMPTY;
			}
		}

		protected override Array CreateArray(int size)
		{
			return new int[0];
		}

		private HTupleVoid()
		{
			base.SetArray(null, false);
		}

		public override void AssertSize(int index)
		{
			throw new HTupleAccessException(this);
		}

		public override int[] ToIArr()
		{
			return new int[0];
		}

		public override long[] ToLArr()
		{
			return new long[0];
		}

		public override double[] ToDArr()
		{
			return new double[0];
		}

		public override float[] ToFArr()
		{
			return new float[0];
		}

		public override IntPtr[] ToIPArr()
		{
			return new IntPtr[0];
		}

		protected override void StoreData(IntPtr proc, IntPtr tuple)
		{
		}
	}
}
