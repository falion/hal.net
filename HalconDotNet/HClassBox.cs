using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HClassBox : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassBox(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassBox obj)
		{
			obj = new HClassBox(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassBox[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HClassBox[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HClassBox(hTuple[i].IP);
			}
			return err;
		}

		public HClassBox()
		{
			IntPtr proc = HalconAPI.PreCall(1895);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeClassBox();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassBox(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeClassBox(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeClassBox().Serialize(stream);
		}

		public static HClassBox Deserialize(Stream stream)
		{
			HClassBox hClassBox = new HClassBox();
			hClassBox.DeserializeClassBox(HSerializedItem.Deserialize(stream));
			return hClassBox;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HClassBox Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeClassBox();
			HClassBox hClassBox = new HClassBox();
			hClassBox.DeserializeClassBox(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassBox;
		}

		public void LearnNdimBox(HRegion foreground, HRegion background, HImage multiChannelImage)
		{
			IntPtr proc = HalconAPI.PreCall(438);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, foreground);
			HalconAPI.Store(proc, 2, background);
			HalconAPI.Store(proc, 3, multiChannelImage);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(foreground);
			GC.KeepAlive(background);
			GC.KeepAlive(multiChannelImage);
		}

		public HRegion ClassNdimBox(HImage multiChannelImage)
		{
			IntPtr proc = HalconAPI.PreCall(439);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, multiChannelImage);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(multiChannelImage);
			return result;
		}

		public void DeserializeClassBox(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1884);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, serializedItemHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeClassBox()
		{
			IntPtr proc = HalconAPI.PreCall(1885);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void WriteClassBox(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1886);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetClassBoxParam(string flag, HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(1887);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, flag);
			HalconAPI.Store(proc, 2, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(value);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetClassBoxParam(string flag, double value)
		{
			IntPtr proc = HalconAPI.PreCall(1887);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, flag);
			HalconAPI.StoreD(proc, 2, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadClassBox(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1889);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void LearnSampsetBox(HFeatureSet sampKey, string outfile, int NSamples, double stopError, int errorN)
		{
			IntPtr proc = HalconAPI.PreCall(1890);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, sampKey);
			HalconAPI.StoreS(proc, 2, outfile);
			HalconAPI.StoreI(proc, 3, NSamples);
			HalconAPI.StoreD(proc, 4, stopError);
			HalconAPI.StoreI(proc, 5, errorN);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(sampKey);
		}

		public void LearnClassBox(HTuple features, int classVal)
		{
			IntPtr proc = HalconAPI.PreCall(1891);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.StoreI(proc, 2, classVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetClassBoxParam(string flag)
		{
			IntPtr proc = HalconAPI.PreCall(1892);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, flag);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void CreateClassBox()
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1895);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DescriptClassBox(int dimensions, out HTuple boxIdx, out HTuple boxLowerBound, out HTuple boxHigherBound, out HTuple boxNumSamplesTrain, out HTuple boxNumSamplesWrong)
		{
			IntPtr proc = HalconAPI.PreCall(1896);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, dimensions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out boxIdx);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out boxLowerBound);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out boxHigherBound);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out boxNumSamplesTrain);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out boxNumSamplesWrong);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int DescriptClassBox(int dimensions, out int boxIdx, out int boxLowerBound, out int boxHigherBound, out int boxNumSamplesTrain, out int boxNumSamplesWrong)
		{
			IntPtr proc = HalconAPI.PreCall(1896);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, dimensions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out boxIdx);
			err = HalconAPI.LoadI(proc, 2, err, out boxLowerBound);
			err = HalconAPI.LoadI(proc, 3, err, out boxHigherBound);
			err = HalconAPI.LoadI(proc, 4, err, out boxNumSamplesTrain);
			err = HalconAPI.LoadI(proc, 5, err, out boxNumSamplesWrong);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double TestSampsetBox(HFeatureSet sampKey)
		{
			IntPtr proc = HalconAPI.PreCall(1897);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, sampKey);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampKey);
			return result;
		}

		public int EnquireRejectClassBox(HTuple featureList)
		{
			IntPtr proc = HalconAPI.PreCall(1898);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, featureList);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(featureList);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int EnquireClassBox(HTuple featureList)
		{
			IntPtr proc = HalconAPI.PreCall(1899);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, featureList);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(featureList);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1894);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
