using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HClassGmm : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassGmm()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassGmm(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassGmm obj)
		{
			obj = new HClassGmm(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassGmm[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HClassGmm[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HClassGmm(hTuple[i].IP);
			}
			return err;
		}

		public HClassGmm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1828);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassGmm(int numDim, int numClasses, HTuple numCenters, string covarType, string preprocessing, int numComponents, int randSeed)
		{
			IntPtr proc = HalconAPI.PreCall(1840);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.Store(proc, 2, numCenters);
			HalconAPI.StoreS(proc, 3, covarType);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numCenters);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassGmm(int numDim, int numClasses, int numCenters, string covarType, string preprocessing, int numComponents, int randSeed)
		{
			IntPtr proc = HalconAPI.PreCall(1840);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.StoreI(proc, 2, numCenters);
			HalconAPI.StoreS(proc, 3, covarType);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeClassGmm();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassGmm(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeClassGmm(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeClassGmm().Serialize(stream);
		}

		public static HClassGmm Deserialize(Stream stream)
		{
			HClassGmm hClassGmm = new HClassGmm();
			hClassGmm.DeserializeClassGmm(HSerializedItem.Deserialize(stream));
			return hClassGmm;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HClassGmm Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeClassGmm();
			HClassGmm hClassGmm = new HClassGmm();
			hClassGmm.DeserializeClassGmm(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassGmm;
		}

		public HRegion ClassifyImageClassGmm(HImage image, double rejectionThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(431);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, rejectionThreshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void AddSamplesImageClassGmm(HImage image, HRegion classRegions, double randomize)
		{
			IntPtr proc = HalconAPI.PreCall(432);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 2, classRegions);
			HalconAPI.StoreD(proc, 1, randomize);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(classRegions);
		}

		public HClassTrainData GetClassTrainDataGmm()
		{
			IntPtr proc = HalconAPI.PreCall(1785);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HClassTrainData result = null;
			err = HClassTrainData.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AddClassTrainDataGmm(HClassTrainData classTrainDataHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1786);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, classTrainDataHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
		}

		public HTuple SelectFeatureSetGmm(HClassTrainData classTrainDataHandle, string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1801);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return result;
		}

		public HTuple SelectFeatureSetGmm(HClassTrainData classTrainDataHandle, string selectionMethod, string genParamName, double genParamValue, out HTuple score)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1801);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return result;
		}

		public HClassLUT CreateClassLutGmm(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1820);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HClassLUT result = null;
			err = HClassLUT.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static void ClearSamplesClassGmm(HClassGmm[] GMMHandle)
		{
			HTuple hTuple = HTool.ConcatArray(GMMHandle);
			IntPtr proc = HalconAPI.PreCall(1825);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(GMMHandle);
		}

		public void ClearSamplesClassGmm()
		{
			IntPtr proc = HalconAPI.PreCall(1825);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeClassGmm(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1826);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeClassGmm()
		{
			IntPtr proc = HalconAPI.PreCall(1827);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadClassGmm(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1828);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteClassGmm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1829);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadSamplesClassGmm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1830);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteSamplesClassGmm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1831);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ClassifyClassGmm(HTuple features, int num, out HTuple classProb, out HTuple density, out HTuple KSigmaProb)
		{
			IntPtr proc = HalconAPI.PreCall(1832);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.StoreI(proc, 2, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out classProb);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out density);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out KSigmaProb);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple EvaluateClassGmm(HTuple features, out double density, out double KSigmaProb)
		{
			IntPtr proc = HalconAPI.PreCall(1833);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out density);
			err = HalconAPI.LoadD(proc, 2, err, out KSigmaProb);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple TrainClassGmm(int maxIter, double threshold, string classPriors, double regularize, out HTuple iter)
		{
			IntPtr proc = HalconAPI.PreCall(1834);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, maxIter);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.StoreS(proc, 3, classPriors);
			HalconAPI.StoreD(proc, 4, regularize);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out iter);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetPrepInfoClassGmm(string preprocessing, out HTuple cumInformationCont)
		{
			IntPtr proc = HalconAPI.PreCall(1835);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, preprocessing);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cumInformationCont);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetSampleNumClassGmm()
		{
			IntPtr proc = HalconAPI.PreCall(1836);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetSampleClassGmm(int numSample, out int classID)
		{
			IntPtr proc = HalconAPI.PreCall(1837);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, numSample);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out classID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AddSampleClassGmm(HTuple features, int classID, double randomize)
		{
			IntPtr proc = HalconAPI.PreCall(1838);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.StoreI(proc, 2, classID);
			HalconAPI.StoreD(proc, 3, randomize);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int GetParamsClassGmm(out int numClasses, out HTuple minCenters, out HTuple maxCenters, out string covarType)
		{
			IntPtr proc = HalconAPI.PreCall(1839);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out numClasses);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out minCenters);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out maxCenters);
			err = HalconAPI.LoadS(proc, 4, err, out covarType);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void CreateClassGmm(int numDim, int numClasses, HTuple numCenters, string covarType, string preprocessing, int numComponents, int randSeed)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1840);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.Store(proc, 2, numCenters);
			HalconAPI.StoreS(proc, 3, covarType);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numCenters);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateClassGmm(int numDim, int numClasses, int numCenters, string covarType, string preprocessing, int numComponents, int randSeed)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1840);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.StoreI(proc, 2, numCenters);
			HalconAPI.StoreS(proc, 3, covarType);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1824);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
