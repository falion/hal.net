using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HHomMat2D : HData, ISerializable, ICloneable
	{
		private const int FIXEDSIZE = 9;

		public HHomMat2D(HTuple tuple)
			: base(tuple)
		{
		}

		internal HHomMat2D(HData data)
			: base(data)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, HTupleType type, int err, out HHomMat2D obj)
		{
			HTuple t = null;
			err = HTuple.LoadNew(proc, parIndex, err, out t);
			obj = new HHomMat2D(new HData(t));
			return err;
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HHomMat2D obj)
		{
			return HHomMat2D.LoadNew(proc, parIndex, HTupleType.MIXED, err, out obj);
		}

		internal static HHomMat2D[] SplitArray(HTuple data)
		{
			int num = data.Length / 9;
			HHomMat2D[] array = new HHomMat2D[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = new HHomMat2D(new HData(data.TupleSelectRange(i * 9, (i + 1) * 9 - 1)));
			}
			return array;
		}

		public HHomMat2D()
		{
			IntPtr proc = HalconAPI.PreCall(288);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeHomMat2d();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HHomMat2D(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeHomMat2d(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeHomMat2d().Serialize(stream);
		}

		public static HHomMat2D Deserialize(Stream stream)
		{
			HHomMat2D hHomMat2D = new HHomMat2D();
			hHomMat2D.DeserializeHomMat2d(HSerializedItem.Deserialize(stream));
			return hHomMat2D;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HHomMat2D Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeHomMat2d();
			HHomMat2D hHomMat2D = new HHomMat2D();
			hHomMat2D.DeserializeHomMat2d(hSerializedItem);
			hSerializedItem.Dispose();
			return hHomMat2D;
		}

		public void ReadWorldFile(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(22);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont ProjectiveTransContourXld(HXLDCont contours)
		{
			IntPtr proc = HalconAPI.PreCall(47);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
			return result;
		}

		public HXLDPoly AffineTransPolygonXld(HXLDPoly polygons)
		{
			IntPtr proc = HalconAPI.PreCall(48);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, polygons);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(polygons);
			return result;
		}

		public HXLDCont AffineTransContourXld(HXLDCont contours)
		{
			IntPtr proc = HalconAPI.PreCall(49);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
			return result;
		}

		public void DeserializeHomMat2d(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(235);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeHomMat2d()
		{
			IntPtr proc = HalconAPI.PreCall(236);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HHomMat2D[] BundleAdjustMosaic(int numImages, int referenceImage, HTuple mappingSource, HTuple mappingDest, HHomMat2D[] homMatrices2D, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple numCorrespondences, string transformation, out HTuple rows, out HTuple cols, out HTuple error)
		{
			HTuple hTuple = HData.ConcatArray(homMatrices2D);
			IntPtr proc = HalconAPI.PreCall(255);
			HalconAPI.StoreI(proc, 0, numImages);
			HalconAPI.StoreI(proc, 1, referenceImage);
			HalconAPI.Store(proc, 2, mappingSource);
			HalconAPI.Store(proc, 3, mappingDest);
			HalconAPI.Store(proc, 4, hTuple);
			HalconAPI.Store(proc, 5, rows1);
			HalconAPI.Store(proc, 6, cols1);
			HalconAPI.Store(proc, 7, rows2);
			HalconAPI.Store(proc, 8, cols2);
			HalconAPI.Store(proc, 9, numCorrespondences);
			HalconAPI.StoreS(proc, 10, transformation);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mappingSource);
			HalconAPI.UnpinTuple(mappingDest);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(numCorrespondences);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			return HHomMat2D.SplitArray(data);
		}

		public static HHomMat2D[] BundleAdjustMosaic(int numImages, int referenceImage, HTuple mappingSource, HTuple mappingDest, HHomMat2D[] homMatrices2D, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple numCorrespondences, string transformation, out HTuple rows, out HTuple cols, out double error)
		{
			HTuple hTuple = HData.ConcatArray(homMatrices2D);
			IntPtr proc = HalconAPI.PreCall(255);
			HalconAPI.StoreI(proc, 0, numImages);
			HalconAPI.StoreI(proc, 1, referenceImage);
			HalconAPI.Store(proc, 2, mappingSource);
			HalconAPI.Store(proc, 3, mappingDest);
			HalconAPI.Store(proc, 4, hTuple);
			HalconAPI.Store(proc, 5, rows1);
			HalconAPI.Store(proc, 6, cols1);
			HalconAPI.Store(proc, 7, rows2);
			HalconAPI.Store(proc, 8, cols2);
			HalconAPI.Store(proc, 9, numCorrespondences);
			HalconAPI.StoreS(proc, 10, transformation);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mappingSource);
			HalconAPI.UnpinTuple(mappingDest);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(numCorrespondences);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out cols);
			err = HalconAPI.LoadD(proc, 3, err, out error);
			HalconAPI.PostCall(proc, err);
			return HHomMat2D.SplitArray(data);
		}

		public HHomMat2D ProjMatchPointsDistortionRansacGuided(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, double kappaGuide, double distanceTolerance, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out double kappa, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(256);
			base.Store(proc, 6);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreD(proc, 7, kappaGuide);
			HalconAPI.StoreD(proc, 8, distanceTolerance);
			HalconAPI.Store(proc, 9, matchThreshold);
			HalconAPI.StoreS(proc, 10, estimationMethod);
			HalconAPI.Store(proc, 11, distanceThreshold);
			HalconAPI.StoreI(proc, 12, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out kappa);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D ProjMatchPointsDistortionRansacGuided(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, double kappaGuide, double distanceTolerance, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out double kappa, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(256);
			base.Store(proc, 6);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreD(proc, 7, kappaGuide);
			HalconAPI.StoreD(proc, 8, distanceTolerance);
			HalconAPI.StoreI(proc, 9, matchThreshold);
			HalconAPI.StoreS(proc, 10, estimationMethod);
			HalconAPI.StoreD(proc, 11, distanceThreshold);
			HalconAPI.StoreI(proc, 12, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out kappa);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public double ProjMatchPointsDistortionRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(257);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.Store(proc, 10, rotation);
			HalconAPI.Store(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.Store(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			err = base.Load(proc, 0, err);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 1, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public double ProjMatchPointsDistortionRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(257);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.StoreD(proc, 10, rotation);
			HalconAPI.StoreI(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.StoreD(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			err = base.Load(proc, 0, err);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 1, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D ProjMatchPointsRansacGuided(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, double distanceTolerance, HTuple matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(258);
			base.Store(proc, 6);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreD(proc, 7, distanceTolerance);
			HalconAPI.Store(proc, 8, matchThreshold);
			HalconAPI.StoreS(proc, 9, estimationMethod);
			HalconAPI.StoreD(proc, 10, distanceThreshold);
			HalconAPI.StoreI(proc, 11, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(matchThreshold);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D ProjMatchPointsRansacGuided(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, double distanceTolerance, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(258);
			base.Store(proc, 6);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreD(proc, 7, distanceTolerance);
			HalconAPI.StoreI(proc, 8, matchThreshold);
			HalconAPI.StoreS(proc, 9, estimationMethod);
			HalconAPI.StoreD(proc, 10, distanceThreshold);
			HalconAPI.StoreI(proc, 11, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HTuple ProjMatchPointsRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(259);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.Store(proc, 10, rotation);
			HalconAPI.Store(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.StoreD(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HTuple ProjMatchPointsRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(259);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.StoreD(proc, 10, rotation);
			HalconAPI.StoreI(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.StoreD(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public double VectorToProjHomMat2dDistortion(HTuple points1Row, HTuple points1Col, HTuple points2Row, HTuple points2Col, HTuple covRR1, HTuple covRC1, HTuple covCC1, HTuple covRR2, HTuple covRC2, HTuple covCC2, int imageWidth, int imageHeight, string method, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(260);
			HalconAPI.Store(proc, 0, points1Row);
			HalconAPI.Store(proc, 1, points1Col);
			HalconAPI.Store(proc, 2, points2Row);
			HalconAPI.Store(proc, 3, points2Col);
			HalconAPI.Store(proc, 4, covRR1);
			HalconAPI.Store(proc, 5, covRC1);
			HalconAPI.Store(proc, 6, covCC1);
			HalconAPI.Store(proc, 7, covRR2);
			HalconAPI.Store(proc, 8, covRC2);
			HalconAPI.Store(proc, 9, covCC2);
			HalconAPI.StoreI(proc, 10, imageWidth);
			HalconAPI.StoreI(proc, 11, imageHeight);
			HalconAPI.StoreS(proc, 12, method);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(points1Row);
			HalconAPI.UnpinTuple(points1Col);
			HalconAPI.UnpinTuple(points2Row);
			HalconAPI.UnpinTuple(points2Col);
			HalconAPI.UnpinTuple(covRR1);
			HalconAPI.UnpinTuple(covRC1);
			HalconAPI.UnpinTuple(covCC1);
			HalconAPI.UnpinTuple(covRR2);
			HalconAPI.UnpinTuple(covRC2);
			HalconAPI.UnpinTuple(covCC2);
			err = base.Load(proc, 0, err);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 1, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void HomVectorToProjHomMat2d(HTuple px, HTuple py, HTuple pw, HTuple qx, HTuple qy, HTuple qw, string method)
		{
			IntPtr proc = HalconAPI.PreCall(261);
			HalconAPI.Store(proc, 0, px);
			HalconAPI.Store(proc, 1, py);
			HalconAPI.Store(proc, 2, pw);
			HalconAPI.Store(proc, 3, qx);
			HalconAPI.Store(proc, 4, qy);
			HalconAPI.Store(proc, 5, qw);
			HalconAPI.StoreS(proc, 6, method);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(pw);
			HalconAPI.UnpinTuple(qx);
			HalconAPI.UnpinTuple(qy);
			HalconAPI.UnpinTuple(qw);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple VectorToProjHomMat2d(HTuple px, HTuple py, HTuple qx, HTuple qy, string method, HTuple covXX1, HTuple covYY1, HTuple covXY1, HTuple covXX2, HTuple covYY2, HTuple covXY2)
		{
			IntPtr proc = HalconAPI.PreCall(262);
			HalconAPI.Store(proc, 0, px);
			HalconAPI.Store(proc, 1, py);
			HalconAPI.Store(proc, 2, qx);
			HalconAPI.Store(proc, 3, qy);
			HalconAPI.StoreS(proc, 4, method);
			HalconAPI.Store(proc, 5, covXX1);
			HalconAPI.Store(proc, 6, covYY1);
			HalconAPI.Store(proc, 7, covXY1);
			HalconAPI.Store(proc, 8, covXX2);
			HalconAPI.Store(proc, 9, covYY2);
			HalconAPI.Store(proc, 10, covXY2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(qx);
			HalconAPI.UnpinTuple(qy);
			HalconAPI.UnpinTuple(covXX1);
			HalconAPI.UnpinTuple(covYY1);
			HalconAPI.UnpinTuple(covXY1);
			HalconAPI.UnpinTuple(covXX2);
			HalconAPI.UnpinTuple(covYY2);
			HalconAPI.UnpinTuple(covXY2);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double HomMat2dToAffinePar(out double sy, out double phi, out double theta, out double tx, out double ty)
		{
			IntPtr proc = HalconAPI.PreCall(263);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out sy);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out theta);
			err = HalconAPI.LoadD(proc, 4, err, out tx);
			err = HalconAPI.LoadD(proc, 5, err, out ty);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void VectorAngleToRigid(HTuple row1, HTuple column1, HTuple angle1, HTuple row2, HTuple column2, HTuple angle2)
		{
			IntPtr proc = HalconAPI.PreCall(264);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, angle1);
			HalconAPI.Store(proc, 3, row2);
			HalconAPI.Store(proc, 4, column2);
			HalconAPI.Store(proc, 5, angle2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(angle1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HalconAPI.UnpinTuple(angle2);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void VectorAngleToRigid(double row1, double column1, double angle1, double row2, double column2, double angle2)
		{
			IntPtr proc = HalconAPI.PreCall(264);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, angle1);
			HalconAPI.StoreD(proc, 3, row2);
			HalconAPI.StoreD(proc, 4, column2);
			HalconAPI.StoreD(proc, 5, angle2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void PointLineToHomMat2d(string transformationType, HTuple px, HTuple py, HTuple l1x, HTuple l1y, HTuple l2x, HTuple l2y)
		{
			IntPtr proc = HalconAPI.PreCall(265);
			HalconAPI.StoreS(proc, 0, transformationType);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.Store(proc, 3, l1x);
			HalconAPI.Store(proc, 4, l1y);
			HalconAPI.Store(proc, 5, l2x);
			HalconAPI.Store(proc, 6, l2y);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(l1x);
			HalconAPI.UnpinTuple(l1y);
			HalconAPI.UnpinTuple(l2x);
			HalconAPI.UnpinTuple(l2y);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void VectorToRigid(HTuple px, HTuple py, HTuple qx, HTuple qy)
		{
			IntPtr proc = HalconAPI.PreCall(266);
			HalconAPI.Store(proc, 0, px);
			HalconAPI.Store(proc, 1, py);
			HalconAPI.Store(proc, 2, qx);
			HalconAPI.Store(proc, 3, qy);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(qx);
			HalconAPI.UnpinTuple(qy);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void VectorToSimilarity(HTuple px, HTuple py, HTuple qx, HTuple qy)
		{
			IntPtr proc = HalconAPI.PreCall(267);
			HalconAPI.Store(proc, 0, px);
			HalconAPI.Store(proc, 1, py);
			HalconAPI.Store(proc, 2, qx);
			HalconAPI.Store(proc, 3, qy);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(qx);
			HalconAPI.UnpinTuple(qy);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void VectorToAniso(HTuple px, HTuple py, HTuple qx, HTuple qy)
		{
			IntPtr proc = HalconAPI.PreCall(268);
			HalconAPI.Store(proc, 0, px);
			HalconAPI.Store(proc, 1, py);
			HalconAPI.Store(proc, 2, qx);
			HalconAPI.Store(proc, 3, qy);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(qx);
			HalconAPI.UnpinTuple(qy);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void VectorToHomMat2d(HTuple px, HTuple py, HTuple qx, HTuple qy)
		{
			IntPtr proc = HalconAPI.PreCall(269);
			HalconAPI.Store(proc, 0, px);
			HalconAPI.Store(proc, 1, py);
			HalconAPI.Store(proc, 2, qx);
			HalconAPI.Store(proc, 3, qy);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(qx);
			HalconAPI.UnpinTuple(qy);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ProjectiveTransPixel(HTuple row, HTuple col, out HTuple rowTrans, out HTuple colTrans)
		{
			IntPtr proc = HalconAPI.PreCall(270);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, col);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowTrans);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out colTrans);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ProjectiveTransPixel(double row, double col, out double rowTrans, out double colTrans)
		{
			IntPtr proc = HalconAPI.PreCall(270);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, col);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			err = HalconAPI.LoadD(proc, 0, err, out rowTrans);
			err = HalconAPI.LoadD(proc, 1, err, out colTrans);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple ProjectiveTransPoint2d(HTuple px, HTuple py, HTuple pw, out HTuple qy, out HTuple qw)
		{
			IntPtr proc = HalconAPI.PreCall(271);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.Store(proc, 3, pw);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(pw);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out qy);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out qw);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double ProjectiveTransPoint2d(double px, double py, double pw, out double qy, out double qw)
		{
			IntPtr proc = HalconAPI.PreCall(271);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.StoreD(proc, 3, pw);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out qy);
			err = HalconAPI.LoadD(proc, 2, err, out qw);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AffineTransPixel(HTuple row, HTuple col, out HTuple rowTrans, out HTuple colTrans)
		{
			IntPtr proc = HalconAPI.PreCall(272);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, col);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowTrans);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out colTrans);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void AffineTransPixel(double row, double col, out double rowTrans, out double colTrans)
		{
			IntPtr proc = HalconAPI.PreCall(272);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, col);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			err = HalconAPI.LoadD(proc, 0, err, out rowTrans);
			err = HalconAPI.LoadD(proc, 1, err, out colTrans);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple AffineTransPoint2d(HTuple px, HTuple py, out HTuple qy)
		{
			IntPtr proc = HalconAPI.PreCall(273);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out qy);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double AffineTransPoint2d(double px, double py, out double qy)
		{
			IntPtr proc = HalconAPI.PreCall(273);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out qy);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double HomMat2dDeterminant()
		{
			IntPtr proc = HalconAPI.PreCall(274);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dTranspose()
		{
			IntPtr proc = HalconAPI.PreCall(275);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dInvert()
		{
			IntPtr proc = HalconAPI.PreCall(276);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dCompose(HHomMat2D homMat2DRight)
		{
			IntPtr proc = HalconAPI.PreCall(277);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, homMat2DRight);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(homMat2DRight);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dReflectLocal(HTuple px, HTuple py)
		{
			IntPtr proc = HalconAPI.PreCall(278);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dReflectLocal(double px, double py)
		{
			IntPtr proc = HalconAPI.PreCall(278);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dReflect(HTuple px, HTuple py, HTuple qx, HTuple qy)
		{
			IntPtr proc = HalconAPI.PreCall(279);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, px);
			HalconAPI.Store(proc, 2, py);
			HalconAPI.Store(proc, 3, qx);
			HalconAPI.Store(proc, 4, qy);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HalconAPI.UnpinTuple(qx);
			HalconAPI.UnpinTuple(qy);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dReflect(double px, double py, double qx, double qy)
		{
			IntPtr proc = HalconAPI.PreCall(279);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.StoreD(proc, 3, qx);
			HalconAPI.StoreD(proc, 4, qy);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dSlantLocal(HTuple theta, string axis)
		{
			IntPtr proc = HalconAPI.PreCall(280);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, theta);
			HalconAPI.StoreS(proc, 2, axis);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(theta);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dSlantLocal(double theta, string axis)
		{
			IntPtr proc = HalconAPI.PreCall(280);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, theta);
			HalconAPI.StoreS(proc, 2, axis);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dSlant(HTuple theta, string axis, HTuple px, HTuple py)
		{
			IntPtr proc = HalconAPI.PreCall(281);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, theta);
			HalconAPI.StoreS(proc, 2, axis);
			HalconAPI.Store(proc, 3, px);
			HalconAPI.Store(proc, 4, py);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(theta);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dSlant(double theta, string axis, double px, double py)
		{
			IntPtr proc = HalconAPI.PreCall(281);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, theta);
			HalconAPI.StoreS(proc, 2, axis);
			HalconAPI.StoreD(proc, 3, px);
			HalconAPI.StoreD(proc, 4, py);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dRotateLocal(HTuple phi)
		{
			IntPtr proc = HalconAPI.PreCall(282);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, phi);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(phi);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dRotateLocal(double phi)
		{
			IntPtr proc = HalconAPI.PreCall(282);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, phi);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dRotate(HTuple phi, HTuple px, HTuple py)
		{
			IntPtr proc = HalconAPI.PreCall(283);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, phi);
			HalconAPI.Store(proc, 2, px);
			HalconAPI.Store(proc, 3, py);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dRotate(double phi, double px, double py)
		{
			IntPtr proc = HalconAPI.PreCall(283);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, phi);
			HalconAPI.StoreD(proc, 2, px);
			HalconAPI.StoreD(proc, 3, py);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dScaleLocal(HTuple sx, HTuple sy)
		{
			IntPtr proc = HalconAPI.PreCall(284);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, sx);
			HalconAPI.Store(proc, 2, sy);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(sx);
			HalconAPI.UnpinTuple(sy);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dScaleLocal(double sx, double sy)
		{
			IntPtr proc = HalconAPI.PreCall(284);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, sx);
			HalconAPI.StoreD(proc, 2, sy);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dScale(HTuple sx, HTuple sy, HTuple px, HTuple py)
		{
			IntPtr proc = HalconAPI.PreCall(285);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, sx);
			HalconAPI.Store(proc, 2, sy);
			HalconAPI.Store(proc, 3, px);
			HalconAPI.Store(proc, 4, py);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(sx);
			HalconAPI.UnpinTuple(sy);
			HalconAPI.UnpinTuple(px);
			HalconAPI.UnpinTuple(py);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dScale(double sx, double sy, double px, double py)
		{
			IntPtr proc = HalconAPI.PreCall(285);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, sx);
			HalconAPI.StoreD(proc, 2, sy);
			HalconAPI.StoreD(proc, 3, px);
			HalconAPI.StoreD(proc, 4, py);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dTranslateLocal(HTuple tx, HTuple ty)
		{
			IntPtr proc = HalconAPI.PreCall(286);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, tx);
			HalconAPI.Store(proc, 2, ty);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(tx);
			HalconAPI.UnpinTuple(ty);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dTranslateLocal(double tx, double ty)
		{
			IntPtr proc = HalconAPI.PreCall(286);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, tx);
			HalconAPI.StoreD(proc, 2, ty);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dTranslate(HTuple tx, HTuple ty)
		{
			IntPtr proc = HalconAPI.PreCall(287);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, tx);
			HalconAPI.Store(proc, 2, ty);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(tx);
			HalconAPI.UnpinTuple(ty);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D HomMat2dTranslate(double tx, double ty)
		{
			IntPtr proc = HalconAPI.PreCall(287);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, tx);
			HalconAPI.StoreD(proc, 2, ty);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void HomMat2dIdentity()
		{
			IntPtr proc = HalconAPI.PreCall(288);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void Reconst3dFromFundamentalMatrix(HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple covRR1, HTuple covRC1, HTuple covCC1, HTuple covRR2, HTuple covRC2, HTuple covCC2, HTuple covFMat, out HTuple x, out HTuple y, out HTuple z, out HTuple w, out HTuple covXYZW)
		{
			IntPtr proc = HalconAPI.PreCall(350);
			base.Store(proc, 10);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, covRR1);
			HalconAPI.Store(proc, 5, covRC1);
			HalconAPI.Store(proc, 6, covCC1);
			HalconAPI.Store(proc, 7, covRR2);
			HalconAPI.Store(proc, 8, covRC2);
			HalconAPI.Store(proc, 9, covCC2);
			HalconAPI.Store(proc, 11, covFMat);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(covRR1);
			HalconAPI.UnpinTuple(covRC1);
			HalconAPI.UnpinTuple(covCC1);
			HalconAPI.UnpinTuple(covRR2);
			HalconAPI.UnpinTuple(covRC2);
			HalconAPI.UnpinTuple(covCC2);
			HalconAPI.UnpinTuple(covFMat);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out w);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out covXYZW);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void Reconst3dFromFundamentalMatrix(double rows1, double cols1, double rows2, double cols2, double covRR1, double covRC1, double covCC1, double covRR2, double covRC2, double covCC2, HTuple covFMat, out double x, out double y, out double z, out double w, out double covXYZW)
		{
			IntPtr proc = HalconAPI.PreCall(350);
			base.Store(proc, 10);
			HalconAPI.StoreD(proc, 0, rows1);
			HalconAPI.StoreD(proc, 1, cols1);
			HalconAPI.StoreD(proc, 2, rows2);
			HalconAPI.StoreD(proc, 3, cols2);
			HalconAPI.StoreD(proc, 4, covRR1);
			HalconAPI.StoreD(proc, 5, covRC1);
			HalconAPI.StoreD(proc, 6, covCC1);
			HalconAPI.StoreD(proc, 7, covRR2);
			HalconAPI.StoreD(proc, 8, covRC2);
			HalconAPI.StoreD(proc, 9, covCC2);
			HalconAPI.Store(proc, 11, covFMat);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(covFMat);
			err = HalconAPI.LoadD(proc, 0, err, out x);
			err = HalconAPI.LoadD(proc, 1, err, out y);
			err = HalconAPI.LoadD(proc, 2, err, out z);
			err = HalconAPI.LoadD(proc, 3, err, out w);
			err = HalconAPI.LoadD(proc, 4, err, out covXYZW);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage GenBinocularProjRectification(out HImage map2, HTuple covFMat, int width1, int height1, int width2, int height2, HTuple subSampling, string mapping, out HTuple covFMatRect, out HHomMat2D h1, out HHomMat2D h2)
		{
			IntPtr proc = HalconAPI.PreCall(351);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, covFMat);
			HalconAPI.StoreI(proc, 2, width1);
			HalconAPI.StoreI(proc, 3, height1);
			HalconAPI.StoreI(proc, 4, width2);
			HalconAPI.StoreI(proc, 5, height2);
			HalconAPI.Store(proc, 6, subSampling);
			HalconAPI.StoreS(proc, 7, mapping);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(covFMat);
			HalconAPI.UnpinTuple(subSampling);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out map2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out covFMatRect);
			err = HHomMat2D.LoadNew(proc, 1, err, out h1);
			err = HHomMat2D.LoadNew(proc, 2, err, out h2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenBinocularProjRectification(out HImage map2, HTuple covFMat, int width1, int height1, int width2, int height2, int subSampling, string mapping, out HTuple covFMatRect, out HHomMat2D h1, out HHomMat2D h2)
		{
			IntPtr proc = HalconAPI.PreCall(351);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, covFMat);
			HalconAPI.StoreI(proc, 2, width1);
			HalconAPI.StoreI(proc, 3, height1);
			HalconAPI.StoreI(proc, 4, width2);
			HalconAPI.StoreI(proc, 5, height2);
			HalconAPI.StoreI(proc, 6, subSampling);
			HalconAPI.StoreS(proc, 7, mapping);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(covFMat);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out map2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out covFMatRect);
			err = HHomMat2D.LoadNew(proc, 1, err, out h1);
			err = HHomMat2D.LoadNew(proc, 2, err, out h2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double VectorToFundamentalMatrixDistortion(HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple covRR1, HTuple covRC1, HTuple covCC1, HTuple covRR2, HTuple covRC2, HTuple covCC2, int imageWidth, int imageHeight, string method, out double error, out HTuple x, out HTuple y, out HTuple z, out HTuple w)
		{
			IntPtr proc = HalconAPI.PreCall(352);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, covRR1);
			HalconAPI.Store(proc, 5, covRC1);
			HalconAPI.Store(proc, 6, covCC1);
			HalconAPI.Store(proc, 7, covRR2);
			HalconAPI.Store(proc, 8, covRC2);
			HalconAPI.Store(proc, 9, covCC2);
			HalconAPI.StoreI(proc, 10, imageWidth);
			HalconAPI.StoreI(proc, 11, imageHeight);
			HalconAPI.StoreS(proc, 12, method);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(covRR1);
			HalconAPI.UnpinTuple(covRC1);
			HalconAPI.UnpinTuple(covCC1);
			HalconAPI.UnpinTuple(covRR2);
			HalconAPI.UnpinTuple(covRC2);
			HalconAPI.UnpinTuple(covCC2);
			err = base.Load(proc, 0, err);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 1, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out w);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple RelPoseToFundamentalMatrix(HPose relPose, HTuple covRelPose, HCamPar camPar1, HCamPar camPar2)
		{
			IntPtr proc = HalconAPI.PreCall(353);
			HalconAPI.Store(proc, 0, relPose);
			HalconAPI.Store(proc, 1, covRelPose);
			HalconAPI.Store(proc, 2, camPar1);
			HalconAPI.Store(proc, 3, camPar2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(relPose);
			HalconAPI.UnpinTuple(covRelPose);
			HalconAPI.UnpinTuple(camPar1);
			HalconAPI.UnpinTuple(camPar2);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D EssentialToFundamentalMatrix(HTuple covEMat, HHomMat2D camMat1, HHomMat2D camMat2, out HTuple covFMat)
		{
			IntPtr proc = HalconAPI.PreCall(354);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, covEMat);
			HalconAPI.Store(proc, 2, camMat1);
			HalconAPI.Store(proc, 3, camMat2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(covEMat);
			HalconAPI.UnpinTuple(camMat1);
			HalconAPI.UnpinTuple(camMat2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covFMat);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D VectorToEssentialMatrix(HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple covRR1, HTuple covRC1, HTuple covCC1, HTuple covRR2, HTuple covRC2, HTuple covCC2, HHomMat2D camMat2, string method, out HTuple covEMat, out HTuple error, out HTuple x, out HTuple y, out HTuple z, out HTuple covXYZ)
		{
			IntPtr proc = HalconAPI.PreCall(356);
			base.Store(proc, 10);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, covRR1);
			HalconAPI.Store(proc, 5, covRC1);
			HalconAPI.Store(proc, 6, covCC1);
			HalconAPI.Store(proc, 7, covRR2);
			HalconAPI.Store(proc, 8, covRC2);
			HalconAPI.Store(proc, 9, covCC2);
			HalconAPI.Store(proc, 11, camMat2);
			HalconAPI.StoreS(proc, 12, method);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(covRR1);
			HalconAPI.UnpinTuple(covRC1);
			HalconAPI.UnpinTuple(covCC1);
			HalconAPI.UnpinTuple(covRR2);
			HalconAPI.UnpinTuple(covRC2);
			HalconAPI.UnpinTuple(covCC2);
			HalconAPI.UnpinTuple(camMat2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covEMat);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out covXYZ);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat2D VectorToEssentialMatrix(HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple covRR1, HTuple covRC1, HTuple covCC1, HTuple covRR2, HTuple covRC2, HTuple covCC2, HHomMat2D camMat2, string method, out HTuple covEMat, out double error, out HTuple x, out HTuple y, out HTuple z, out HTuple covXYZ)
		{
			IntPtr proc = HalconAPI.PreCall(356);
			base.Store(proc, 10);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, covRR1);
			HalconAPI.Store(proc, 5, covRC1);
			HalconAPI.Store(proc, 6, covCC1);
			HalconAPI.Store(proc, 7, covRR2);
			HalconAPI.Store(proc, 8, covRC2);
			HalconAPI.Store(proc, 9, covCC2);
			HalconAPI.Store(proc, 11, camMat2);
			HalconAPI.StoreS(proc, 12, method);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(covRR1);
			HalconAPI.UnpinTuple(covRC1);
			HalconAPI.UnpinTuple(covCC1);
			HalconAPI.UnpinTuple(covRR2);
			HalconAPI.UnpinTuple(covRC2);
			HalconAPI.UnpinTuple(covCC2);
			HalconAPI.UnpinTuple(camMat2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covEMat);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out covXYZ);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple VectorToFundamentalMatrix(HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple covRR1, HTuple covRC1, HTuple covCC1, HTuple covRR2, HTuple covRC2, HTuple covCC2, string method, out double error, out HTuple x, out HTuple y, out HTuple z, out HTuple w, out HTuple covXYZW)
		{
			IntPtr proc = HalconAPI.PreCall(357);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, covRR1);
			HalconAPI.Store(proc, 5, covRC1);
			HalconAPI.Store(proc, 6, covCC1);
			HalconAPI.Store(proc, 7, covRR2);
			HalconAPI.Store(proc, 8, covRC2);
			HalconAPI.Store(proc, 9, covCC2);
			HalconAPI.StoreS(proc, 10, method);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(covRR1);
			HalconAPI.UnpinTuple(covRC1);
			HalconAPI.UnpinTuple(covCC1);
			HalconAPI.UnpinTuple(covRR2);
			HalconAPI.UnpinTuple(covRC2);
			HalconAPI.UnpinTuple(covCC2);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out w);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out covXYZW);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double MatchFundamentalMatrixDistortionRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(358);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.Store(proc, 10, rotation);
			HalconAPI.Store(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.Store(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			err = base.Load(proc, 0, err);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 1, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public double MatchFundamentalMatrixDistortionRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(358);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.StoreD(proc, 10, rotation);
			HalconAPI.StoreI(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.StoreD(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			err = base.Load(proc, 0, err);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 1, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D MatchEssentialMatrixRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HHomMat2D camMat2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out HTuple covEMat, out HTuple error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(360);
			base.Store(proc, 4);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 5, camMat2);
			HalconAPI.StoreS(proc, 6, grayMatchMethod);
			HalconAPI.StoreI(proc, 7, maskSize);
			HalconAPI.StoreI(proc, 8, rowMove);
			HalconAPI.StoreI(proc, 9, colMove);
			HalconAPI.StoreI(proc, 10, rowTolerance);
			HalconAPI.StoreI(proc, 11, colTolerance);
			HalconAPI.Store(proc, 12, rotation);
			HalconAPI.Store(proc, 13, matchThreshold);
			HalconAPI.StoreS(proc, 14, estimationMethod);
			HalconAPI.Store(proc, 15, distanceThreshold);
			HalconAPI.StoreI(proc, 16, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(camMat2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covEMat);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HHomMat2D MatchEssentialMatrixRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HHomMat2D camMat2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple covEMat, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(360);
			base.Store(proc, 4);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 5, camMat2);
			HalconAPI.StoreS(proc, 6, grayMatchMethod);
			HalconAPI.StoreI(proc, 7, maskSize);
			HalconAPI.StoreI(proc, 8, rowMove);
			HalconAPI.StoreI(proc, 9, colMove);
			HalconAPI.StoreI(proc, 10, rowTolerance);
			HalconAPI.StoreI(proc, 11, colTolerance);
			HalconAPI.StoreD(proc, 12, rotation);
			HalconAPI.StoreI(proc, 13, matchThreshold);
			HalconAPI.StoreS(proc, 14, estimationMethod);
			HalconAPI.StoreD(proc, 15, distanceThreshold);
			HalconAPI.StoreI(proc, 16, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(camMat2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covEMat);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HTuple MatchFundamentalMatrixRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(361);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.Store(proc, 10, rotation);
			HalconAPI.Store(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.Store(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HTuple MatchFundamentalMatrixRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(361);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.StoreS(proc, 4, grayMatchMethod);
			HalconAPI.StoreI(proc, 5, maskSize);
			HalconAPI.StoreI(proc, 6, rowMove);
			HalconAPI.StoreI(proc, 7, colMove);
			HalconAPI.StoreI(proc, 8, rowTolerance);
			HalconAPI.StoreI(proc, 9, colTolerance);
			HalconAPI.StoreD(proc, 10, rotation);
			HalconAPI.StoreI(proc, 11, matchThreshold);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.StoreD(proc, 13, distanceThreshold);
			HalconAPI.StoreI(proc, 14, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HRegion ProjectiveTransRegion(HRegion regions, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(487);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
			return result;
		}

		public HRegion AffineTransRegion(HRegion region, string interpolate)
		{
			IntPtr proc = HalconAPI.PreCall(488);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, region);
			HalconAPI.StoreS(proc, 1, interpolate);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public HImage ProjectiveTransImageSize(HImage image, string interpolation, int width, int height, string transformDomain)
		{
			IntPtr proc = HalconAPI.PreCall(1620);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.StoreS(proc, 4, transformDomain);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HImage ProjectiveTransImage(HImage image, string interpolation, string adaptImageSize, string transformDomain)
		{
			IntPtr proc = HalconAPI.PreCall(1621);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.StoreS(proc, 2, adaptImageSize);
			HalconAPI.StoreS(proc, 3, transformDomain);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HImage AffineTransImageSize(HImage image, string interpolation, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1622);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HImage AffineTransImage(HImage image, string interpolation, string adaptImageSize)
		{
			IntPtr proc = HalconAPI.PreCall(1623);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.StoreS(proc, 2, adaptImageSize);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void VectorFieldToHomMat2d(HImage vectorField)
		{
			IntPtr proc = HalconAPI.PreCall(1631);
			HalconAPI.Store(proc, 1, vectorField);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(vectorField);
		}

		public void CamParToCamMat(HCamPar cameraParam, out int imageWidth, out int imageHeight)
		{
			IntPtr proc = HalconAPI.PreCall(1905);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			err = base.Load(proc, 0, err);
			err = HalconAPI.LoadI(proc, 1, err, out imageWidth);
			err = HalconAPI.LoadI(proc, 2, err, out imageHeight);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HCamPar CamMatToCamPar(double kappa, int imageWidth, int imageHeight)
		{
			IntPtr proc = HalconAPI.PreCall(1906);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, kappa);
			HalconAPI.StoreI(proc, 2, imageWidth);
			HalconAPI.StoreI(proc, 3, imageHeight);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HCamPar result = null;
			err = HCamPar.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HHomMat2D[] StationaryCameraSelfCalibration(int numImages, int imageWidth, int imageHeight, int referenceImage, HTuple mappingSource, HTuple mappingDest, HHomMat2D[] homMatrices2D, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple numCorrespondences, string estimationMethod, HTuple cameraModel, string fixedCameraParams, out HTuple kappa, out HHomMat2D[] rotationMatrices, out HTuple x, out HTuple y, out HTuple z, out HTuple error)
		{
			HTuple hTuple = HData.ConcatArray(homMatrices2D);
			IntPtr proc = HalconAPI.PreCall(1907);
			HalconAPI.StoreI(proc, 0, numImages);
			HalconAPI.StoreI(proc, 1, imageWidth);
			HalconAPI.StoreI(proc, 2, imageHeight);
			HalconAPI.StoreI(proc, 3, referenceImage);
			HalconAPI.Store(proc, 4, mappingSource);
			HalconAPI.Store(proc, 5, mappingDest);
			HalconAPI.Store(proc, 6, hTuple);
			HalconAPI.Store(proc, 7, rows1);
			HalconAPI.Store(proc, 8, cols1);
			HalconAPI.Store(proc, 9, rows2);
			HalconAPI.Store(proc, 10, cols2);
			HalconAPI.Store(proc, 11, numCorrespondences);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.Store(proc, 13, cameraModel);
			HalconAPI.StoreS(proc, 14, fixedCameraParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mappingSource);
			HalconAPI.UnpinTuple(mappingDest);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(numCorrespondences);
			HalconAPI.UnpinTuple(cameraModel);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out kappa);
			HTuple data2 = null;
			err = HTuple.LoadNew(proc, 2, err, out data2);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			rotationMatrices = HHomMat2D.SplitArray(data2);
			return HHomMat2D.SplitArray(data);
		}

		public static HHomMat2D[] StationaryCameraSelfCalibration(int numImages, int imageWidth, int imageHeight, int referenceImage, HTuple mappingSource, HTuple mappingDest, HHomMat2D[] homMatrices2D, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple numCorrespondences, string estimationMethod, HTuple cameraModel, string fixedCameraParams, out double kappa, out HHomMat2D[] rotationMatrices, out HTuple x, out HTuple y, out HTuple z, out double error)
		{
			HTuple hTuple = HData.ConcatArray(homMatrices2D);
			IntPtr proc = HalconAPI.PreCall(1907);
			HalconAPI.StoreI(proc, 0, numImages);
			HalconAPI.StoreI(proc, 1, imageWidth);
			HalconAPI.StoreI(proc, 2, imageHeight);
			HalconAPI.StoreI(proc, 3, referenceImage);
			HalconAPI.Store(proc, 4, mappingSource);
			HalconAPI.Store(proc, 5, mappingDest);
			HalconAPI.Store(proc, 6, hTuple);
			HalconAPI.Store(proc, 7, rows1);
			HalconAPI.Store(proc, 8, cols1);
			HalconAPI.Store(proc, 9, rows2);
			HalconAPI.Store(proc, 10, cols2);
			HalconAPI.Store(proc, 11, numCorrespondences);
			HalconAPI.StoreS(proc, 12, estimationMethod);
			HalconAPI.Store(proc, 13, cameraModel);
			HalconAPI.StoreS(proc, 14, fixedCameraParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mappingSource);
			HalconAPI.UnpinTuple(mappingDest);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(numCorrespondences);
			HalconAPI.UnpinTuple(cameraModel);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HalconAPI.LoadD(proc, 1, err, out kappa);
			HTuple data2 = null;
			err = HTuple.LoadNew(proc, 2, err, out data2);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out z);
			err = HalconAPI.LoadD(proc, 6, err, out error);
			HalconAPI.PostCall(proc, err);
			rotationMatrices = HHomMat2D.SplitArray(data2);
			return HHomMat2D.SplitArray(data);
		}
	}
}
