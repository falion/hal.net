using System;

namespace VisionConfig
{
	internal class HTupleString : HTupleImplementation
	{
		protected string[] s;

		public override string[] SArr
		{
			get
			{
				return this.s;
			}
			set
			{
				base.SetArray(value, false);
			}
		}

		public override HTupleType Type
		{
			get
			{
				return HTupleType.STRING;
			}
		}

		protected override Array CreateArray(int size)
		{
			string[] array = new string[size];
			for (int i = 0; i < size; i++)
			{
				array[i] = "";
			}
			return array;
		}

		protected override void NotifyArrayUpdate()
		{
			this.s = (string[])base.data;
		}

		public HTupleString(string s)
		{
			base.SetArray(new string[1]
			{
				s
			}, false);
		}

		public HTupleString(string[] s, bool copy)
		{
			base.SetArray(s, copy);
		}

		public override HTupleElements GetElement(int index, HTuple parent)
		{
			return new HTupleElements(parent, this, index);
		}

		public override HTupleElements GetElements(int[] indices, HTuple parent)
		{
			if (indices != null && indices.Length != 0)
			{
				return new HTupleElements(parent, this, indices);
			}
			return new HTupleElements();
		}

		public override void SetElements(int[] indices, HTupleElements elements)
		{
			if (indices == null)
			{
				return;
			}
			if (indices.Length == 0)
			{
				return;
			}
			string[] sArr = elements.SArr;
			if (sArr.Length == indices.Length)
			{
				for (int i = 0; i < indices.Length; i++)
				{
					this.s[indices[i]] = sArr[i];
				}
				return;
			}
			if (sArr.Length == 1)
			{
				for (int j = 0; j < indices.Length; j++)
				{
					this.s[indices[j]] = sArr[0];
				}
				return;
			}
			throw new HTupleAccessException(this, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
		}

		public override string[] ToSArr()
		{
			return (string[])base.ToArray(base.typeS);
		}

		protected override void StoreData(IntPtr proc, IntPtr tuple)
		{
			for (int i = 0; i < base.iLength; i++)
			{
				HalconAPI.HCkP(proc, HalconAPI.SetS(tuple, i, this.s[i]));
			}
		}

		public static int Load(IntPtr tuple, out HTupleString data)
		{
			int num = 2;
			int num2 = 0;
			HalconAPI.GetTupleLength(tuple, out num2);
			string[] array = new string[num2];
			for (int i = 0; i < num2; i++)
			{
				if (!HalconAPI.IsFailure(num))
				{
					num = HalconAPI.GetS(tuple, i, out array[i]);
				}
			}
			data = new HTupleString(array, false);
			return num;
		}
	}
}
