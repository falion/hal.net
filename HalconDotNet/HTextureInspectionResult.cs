using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HTextureInspectionResult : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionResult()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionResult(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextureInspectionResult obj)
		{
			obj = new HTextureInspectionResult(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextureInspectionResult[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HTextureInspectionResult[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HTextureInspectionResult(hTuple[i].IP);
			}
			return err;
		}

		public HTextureInspectionResult(HImage image, out HRegion noveltyRegion, HTextureInspectionModel textureInspectionModel)
		{
			IntPtr proc = HalconAPI.PreCall(2044);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			err = HRegion.LoadNew(proc, 1, err, out noveltyRegion);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(textureInspectionModel);
		}

		public static HTuple AddTextureInspectionModelImage(HImage image, HTextureInspectionModel textureInspectionModel)
		{
			IntPtr proc = HalconAPI.PreCall(2043);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(image);
			GC.KeepAlive(textureInspectionModel);
			return result;
		}

		public HRegion ApplyTextureInspectionModel(HImage image, HTextureInspectionModel textureInspectionModel)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2044);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(textureInspectionModel);
			return result;
		}

		public static HImage GetTextureInspectionModelImage(HTextureInspectionModel textureInspectionModel)
		{
			IntPtr proc = HalconAPI.PreCall(2075);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(textureInspectionModel);
			return result;
		}

		public HObject GetTextureInspectionResultObject(HTuple resultName)
		{
			IntPtr proc = HalconAPI.PreCall(2077);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultName);
			HObject result = null;
			err = HObject.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HObject GetTextureInspectionResultObject(string resultName)
		{
			IntPtr proc = HalconAPI.PreCall(2077);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HObject result = null;
			err = HObject.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static void TrainTextureInspectionModel(HTextureInspectionModel textureInspectionModel)
		{
			IntPtr proc = HalconAPI.PreCall(2099);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(textureInspectionModel);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(2048);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
