using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HCondition : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCondition()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCondition(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCondition obj)
		{
			obj = new HCondition(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCondition[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HCondition[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HCondition(hTuple[i].IP);
			}
			return err;
		}

		public HCondition(HTuple attribName, HTuple attribValue)
		{
			IntPtr proc = HalconAPI.PreCall(548);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HCondition(string attribName, string attribValue)
		{
			IntPtr proc = HalconAPI.PreCall(548);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void BroadcastCondition()
		{
			IntPtr proc = HalconAPI.PreCall(544);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SignalCondition()
		{
			IntPtr proc = HalconAPI.PreCall(545);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TimedWaitCondition(HMutex mutexHandle, int timeout)
		{
			IntPtr proc = HalconAPI.PreCall(546);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, mutexHandle);
			HalconAPI.StoreI(proc, 2, timeout);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(mutexHandle);
		}

		public void WaitCondition(HMutex mutexHandle)
		{
			IntPtr proc = HalconAPI.PreCall(547);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, mutexHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(mutexHandle);
		}

		public void CreateCondition(HTuple attribName, HTuple attribValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(548);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateCondition(string attribName, string attribValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(548);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(543);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
