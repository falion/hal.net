namespace VisionConfig
{
	internal class HTupleElementsInt64 : HTupleElementsImplementation
	{
		internal HTupleElementsInt64(HTupleInt64 source, int index)
			: base(source, index)
		{
		}

		internal HTupleElementsInt64(HTupleInt64 source, int[] indices)
			: base(source, indices)
		{
		}

		public override int[] getI()
		{
			if (base.indices == null)
			{
				return null;
			}
			int[] array = new int[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = (int)base.source.LArr[base.indices[i]];
			}
			return array;
		}

		public override void setI(int[] i)
		{
			if (i != null)
			{
				if (i.Length > 1)
				{
					if (i.Length == base.indices.Length)
					{
						for (int j = 0; j < i.Length; j++)
						{
							base.source.LArr[base.indices[j]] = i[j];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int k = 0; k < i.Length; k++)
				{
					base.source.LArr[base.indices[k]] = i[0];
				}
			}
		}

		public override long[] getL()
		{
			if (base.indices == null)
			{
				return null;
			}
			long[] array = new long[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = base.source.LArr[base.indices[i]];
			}
			return array;
		}

		public override void setL(long[] l)
		{
			if (l != null)
			{
				if (l.Length > 1)
				{
					if (l.Length == base.indices.Length)
					{
						for (int i = 0; i < l.Length; i++)
						{
							base.source.LArr[base.indices[i]] = l[i];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int j = 0; j < l.Length; j++)
				{
					base.source.LArr[base.indices[j]] = l[0];
				}
			}
		}

		public override double[] getD()
		{
			if (base.indices == null)
			{
				return null;
			}
			double[] array = new double[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = (double)base.source.LArr[base.indices[i]];
			}
			return array;
		}

		public override object[] getO()
		{
			if (base.indices == null)
			{
				return null;
			}
			object[] array = new object[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = base.source.LArr[base.indices[i]];
			}
			return array;
		}

		public override HTupleType getType()
		{
			return HTupleType.LONG;
		}
	}
}
