using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HStructuredLightModel : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HStructuredLightModel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HStructuredLightModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HStructuredLightModel obj)
		{
			obj = new HStructuredLightModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HStructuredLightModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HStructuredLightModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HStructuredLightModel(hTuple[i].IP);
			}
			return err;
		}

		public HStructuredLightModel(string modelType)
		{
			IntPtr proc = HalconAPI.PreCall(2107);
			HalconAPI.StoreS(proc, 0, modelType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeStructuredLightModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HStructuredLightModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeStructuredLightModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeStructuredLightModel().Serialize(stream);
		}

		public static HStructuredLightModel Deserialize(Stream stream)
		{
			HStructuredLightModel hStructuredLightModel = new HStructuredLightModel();
			hStructuredLightModel.DeserializeStructuredLightModel(HSerializedItem.Deserialize(stream));
			return hStructuredLightModel;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HStructuredLightModel Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeStructuredLightModel();
			HStructuredLightModel hStructuredLightModel = new HStructuredLightModel();
			hStructuredLightModel.DeserializeStructuredLightModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hStructuredLightModel;
		}

		public void CreateStructuredLightModel(string modelType)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2107);
			HalconAPI.StoreS(proc, 0, modelType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DecodeStructuredLightPattern(HImage cameraImages)
		{
			IntPtr proc = HalconAPI.PreCall(2108);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraImages);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(cameraImages);
		}

		public void DeserializeStructuredLightModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2110);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HImage GenStructuredLightPattern()
		{
			IntPtr proc = HalconAPI.PreCall(2113);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetStructuredLightModelParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2117);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetStructuredLightModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2117);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HObject GetStructuredLightObject(HTuple objectName)
		{
			IntPtr proc = HalconAPI.PreCall(2118);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectName);
			HObject result = null;
			err = HObject.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HObject GetStructuredLightObject(string objectName)
		{
			IntPtr proc = HalconAPI.PreCall(2118);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HObject result = null;
			err = HObject.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadStructuredLightModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2123);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem SerializeStructuredLightModel()
		{
			IntPtr proc = HalconAPI.PreCall(2127);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetStructuredLightModelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2130);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetStructuredLightModelParam(string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2130);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreI(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteStructuredLightModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(2133);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(2106);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
