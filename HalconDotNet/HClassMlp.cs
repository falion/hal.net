using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HClassMlp : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassMlp()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassMlp(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassMlp obj)
		{
			obj = new HClassMlp(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassMlp[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HClassMlp[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HClassMlp(hTuple[i].IP);
			}
			return err;
		}

		public HClassMlp(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1867);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassMlp(int numInput, int numHidden, int numOutput, string outputFunction, string preprocessing, int numComponents, int randSeed)
		{
			IntPtr proc = HalconAPI.PreCall(1883);
			HalconAPI.StoreI(proc, 0, numInput);
			HalconAPI.StoreI(proc, 1, numHidden);
			HalconAPI.StoreI(proc, 2, numOutput);
			HalconAPI.StoreS(proc, 3, outputFunction);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeClassMlp();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassMlp(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeClassMlp(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeClassMlp().Serialize(stream);
		}

		public static HClassMlp Deserialize(Stream stream)
		{
			HClassMlp hClassMlp = new HClassMlp();
			hClassMlp.DeserializeClassMlp(HSerializedItem.Deserialize(stream));
			return hClassMlp;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HClassMlp Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeClassMlp();
			HClassMlp hClassMlp = new HClassMlp();
			hClassMlp.DeserializeClassMlp(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassMlp;
		}

		public HRegion ClassifyImageClassMlp(HImage image, double rejectionThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(435);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, rejectionThreshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void AddSamplesImageClassMlp(HImage image, HRegion classRegions)
		{
			IntPtr proc = HalconAPI.PreCall(436);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 2, classRegions);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(classRegions);
		}

		public HClassTrainData GetClassTrainDataMlp()
		{
			IntPtr proc = HalconAPI.PreCall(1787);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HClassTrainData result = null;
			err = HClassTrainData.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AddClassTrainDataMlp(HClassTrainData classTrainDataHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1788);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, classTrainDataHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
		}

		public HTuple SelectFeatureSetMlp(HClassTrainData classTrainDataHandle, string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1799);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return result;
		}

		public HTuple SelectFeatureSetMlp(HClassTrainData classTrainDataHandle, string selectionMethod, string genParamName, double genParamValue, out HTuple score)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1799);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return result;
		}

		public HClassLUT CreateClassLutMlp(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1822);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HClassLUT result = null;
			err = HClassLUT.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static void ClearSamplesClassMlp(HClassMlp[] MLPHandle)
		{
			HTuple hTuple = HTool.ConcatArray(MLPHandle);
			IntPtr proc = HalconAPI.PreCall(1864);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(MLPHandle);
		}

		public void ClearSamplesClassMlp()
		{
			IntPtr proc = HalconAPI.PreCall(1864);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeClassMlp(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1865);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeClassMlp()
		{
			IntPtr proc = HalconAPI.PreCall(1866);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadClassMlp(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1867);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteClassMlp(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1868);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadSamplesClassMlp(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1869);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteSamplesClassMlp(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1870);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ClassifyClassMlp(HTuple features, HTuple num, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(1871);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.Store(proc, 2, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(num);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int ClassifyClassMlp(HTuple features, HTuple num, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(1871);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.Store(proc, 2, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(num);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple EvaluateClassMlp(HTuple features)
		{
			IntPtr proc = HalconAPI.PreCall(1872);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double TrainClassMlp(int maxIterations, double weightTolerance, double errorTolerance, out HTuple errorLog)
		{
			IntPtr proc = HalconAPI.PreCall(1873);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, maxIterations);
			HalconAPI.StoreD(proc, 2, weightTolerance);
			HalconAPI.StoreD(proc, 3, errorTolerance);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out errorLog);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetPrepInfoClassMlp(string preprocessing, out HTuple cumInformationCont)
		{
			IntPtr proc = HalconAPI.PreCall(1874);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, preprocessing);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cumInformationCont);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetSampleNumClassMlp()
		{
			IntPtr proc = HalconAPI.PreCall(1875);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetSampleClassMlp(int indexSample, out HTuple target)
		{
			IntPtr proc = HalconAPI.PreCall(1876);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, indexSample);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out target);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetRejectionParamsClassMlp(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1877);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetRejectionParamsClassMlp(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1877);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetRejectionParamsClassMlp(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1878);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetRejectionParamsClassMlp(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1878);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AddSampleClassMlp(HTuple features, HTuple target)
		{
			IntPtr proc = HalconAPI.PreCall(1879);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.Store(proc, 2, target);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(target);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AddSampleClassMlp(HTuple features, int target)
		{
			IntPtr proc = HalconAPI.PreCall(1879);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.StoreI(proc, 2, target);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetRegularizationParamsClassMlp(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1880);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetRegularizationParamsClassMlp(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1881);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetRegularizationParamsClassMlp(string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1881);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int GetParamsClassMlp(out int numHidden, out int numOutput, out string outputFunction, out string preprocessing, out int numComponents)
		{
			IntPtr proc = HalconAPI.PreCall(1882);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out numHidden);
			err = HalconAPI.LoadI(proc, 2, err, out numOutput);
			err = HalconAPI.LoadS(proc, 3, err, out outputFunction);
			err = HalconAPI.LoadS(proc, 4, err, out preprocessing);
			err = HalconAPI.LoadI(proc, 5, err, out numComponents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void CreateClassMlp(int numInput, int numHidden, int numOutput, string outputFunction, string preprocessing, int numComponents, int randSeed)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1883);
			HalconAPI.StoreI(proc, 0, numInput);
			HalconAPI.StoreI(proc, 1, numHidden);
			HalconAPI.StoreI(proc, 2, numOutput);
			HalconAPI.StoreS(proc, 3, outputFunction);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1863);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
