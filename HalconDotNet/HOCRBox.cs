using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HOCRBox : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRBox()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRBox(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRBox obj)
		{
			obj = new HOCRBox(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRBox[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HOCRBox[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HOCRBox(hTuple[i].IP);
			}
			return err;
		}

		public HOCRBox(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(712);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCRBox(int widthPattern, int heightPattern, int interpolation, HTuple features, HTuple character)
		{
			IntPtr proc = HalconAPI.PreCall(716);
			HalconAPI.StoreI(proc, 0, widthPattern);
			HalconAPI.StoreI(proc, 1, heightPattern);
			HalconAPI.StoreI(proc, 2, interpolation);
			HalconAPI.Store(proc, 3, features);
			HalconAPI.Store(proc, 4, character);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(character);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCRBox(int widthPattern, int heightPattern, int interpolation, string features, HTuple character)
		{
			IntPtr proc = HalconAPI.PreCall(716);
			HalconAPI.StoreI(proc, 0, widthPattern);
			HalconAPI.StoreI(proc, 1, heightPattern);
			HalconAPI.StoreI(proc, 2, interpolation);
			HalconAPI.StoreS(proc, 3, features);
			HalconAPI.Store(proc, 4, character);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(character);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeOcr();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRBox(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeOcr(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeOcr().Serialize(stream);
		}

		public static HOCRBox Deserialize(Stream stream)
		{
			HOCRBox hOCRBox = new HOCRBox();
			hOCRBox.DeserializeOcr(HSerializedItem.Deserialize(stream));
			return hOCRBox;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HOCRBox Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeOcr();
			HOCRBox hOCRBox = new HOCRBox();
			hOCRBox.DeserializeOcr(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRBox;
		}

		public HSerializedItem SerializeOcr()
		{
			IntPtr proc = HalconAPI.PreCall(709);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializeOcr(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(710);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public void WriteOcr(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(711);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadOcr(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(712);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DoOcrSingle(HRegion character, HImage image, out HTuple confidences)
		{
			IntPtr proc = HalconAPI.PreCall(713);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidences);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public HTuple DoOcrMulti(HRegion character, HImage image, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(714);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public string DoOcrMulti(HRegion character, HImage image, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(714);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public void InfoOcrClassBox(out int widthPattern, out int heightPattern, out int interpolation, out int widthMaxChar, out int heightMaxChar, out HTuple features, out HTuple characters)
		{
			IntPtr proc = HalconAPI.PreCall(715);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out widthPattern);
			err = HalconAPI.LoadI(proc, 1, err, out heightPattern);
			err = HalconAPI.LoadI(proc, 2, err, out interpolation);
			err = HalconAPI.LoadI(proc, 3, err, out widthMaxChar);
			err = HalconAPI.LoadI(proc, 4, err, out heightMaxChar);
			err = HTuple.LoadNew(proc, 5, err, out features);
			err = HTuple.LoadNew(proc, 6, err, out characters);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcrClassBox(int widthPattern, int heightPattern, int interpolation, HTuple features, HTuple character)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(716);
			HalconAPI.StoreI(proc, 0, widthPattern);
			HalconAPI.StoreI(proc, 1, heightPattern);
			HalconAPI.StoreI(proc, 2, interpolation);
			HalconAPI.Store(proc, 3, features);
			HalconAPI.Store(proc, 4, character);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(character);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcrClassBox(int widthPattern, int heightPattern, int interpolation, string features, HTuple character)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(716);
			HalconAPI.StoreI(proc, 0, widthPattern);
			HalconAPI.StoreI(proc, 1, heightPattern);
			HalconAPI.StoreI(proc, 2, interpolation);
			HalconAPI.StoreS(proc, 3, features);
			HalconAPI.Store(proc, 4, character);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(character);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public double TraindOcrClassBox(HRegion character, HImage image, HTuple classVal)
		{
			IntPtr proc = HalconAPI.PreCall(717);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(classVal);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public double TraindOcrClassBox(HRegion character, HImage image, string classVal)
		{
			IntPtr proc = HalconAPI.PreCall(717);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public double TrainfOcrClassBox(HTuple trainingFile)
		{
			IntPtr proc = HalconAPI.PreCall(718);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, trainingFile);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double TrainfOcrClassBox(string trainingFile)
		{
			IntPtr proc = HalconAPI.PreCall(718);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, trainingFile);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void OcrChangeChar(HTuple character)
		{
			IntPtr proc = HalconAPI.PreCall(721);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(character);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple TestdOcrClassBox(HRegion character, HImage image, HTuple classVal)
		{
			IntPtr proc = HalconAPI.PreCall(725);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(classVal);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public double TestdOcrClassBox(HRegion character, HImage image, string classVal)
		{
			IntPtr proc = HalconAPI.PreCall(725);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return result;
		}

		public HTuple OcrGetFeatures(HImage character)
		{
			IntPtr proc = HalconAPI.PreCall(727);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(722);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
