using System;

namespace VisionConfig
{
	public class HTupleElements
	{
		private HTuple parent;

		private HTupleElementsImplementation elements;

		public int I
		{
			get
			{
				return this.elements.I[0];
			}
			set
			{
				int[] i = new int[1]
				{
					value
				};
				try
				{
					this.elements.I = i;
				}
				catch (HTupleAccessException)
				{
					this.ConvertToMixed();
					this.elements.I = i;
				}
			}
		}

		public int[] IArr
		{
			get
			{
				return this.elements.I;
			}
			set
			{
				try
				{
					this.elements.I = value;
				}
				catch (HTupleAccessException)
				{
					this.ConvertToMixed();
					this.elements.I = value;
				}
			}
		}

		public long L
		{
			get
			{
				return this.elements.L[0];
			}
			set
			{
				long[] l = new long[1]
				{
					value
				};
				try
				{
					this.elements.L = l;
				}
				catch (HTupleAccessException)
				{
					this.ConvertToMixed();
					this.elements.L = l;
				}
			}
		}

		public long[] LArr
		{
			get
			{
				return this.elements.L;
			}
			set
			{
				try
				{
					this.elements.L = value;
				}
				catch (HTupleAccessException)
				{
					this.ConvertToMixed();
					this.elements.L = value;
				}
			}
		}

		public double D
		{
			get
			{
				return this.elements.D[0];
			}
			set
			{
				double[] d = new double[1]
				{
					value
				};
				try
				{
					this.elements.D = d;
				}
				catch (HTupleAccessException)
				{
					this.ConvertToMixed();
					this.elements.D = d;
				}
			}
		}

		public double[] DArr
		{
			get
			{
				return this.elements.D;
			}
			set
			{
				try
				{
					this.elements.D = value;
				}
				catch (HTupleAccessException)
				{
					this.ConvertToMixed();
					this.elements.D = value;
				}
			}
		}

		public string S
		{
			get
			{
				return this.elements.S[0];
			}
			set
			{
				string[] s = new string[1]
				{
					value
				};
				try
				{
					this.elements.S = s;
				}
				catch (HTupleAccessException)
				{
					this.ConvertToMixed();
					this.elements.S = s;
				}
			}
		}

		public string[] SArr
		{
			get
			{
				return this.elements.S;
			}
			set
			{
				try
				{
					this.elements.S = value;
				}
				catch (HTupleAccessException)
				{
					this.ConvertToMixed();
					this.elements.S = value;
				}
			}
		}

		public object O
		{
			get
			{
				return this.elements.O[0];
			}
			set
			{
				if (this.elements is HTupleElementsMixed)
				{
					this.elements.O[0] = value;
				}
				else
				{
					switch (HTupleImplementation.GetObjectType(value))
					{
					case 1:
						this.I = (int)value;
						break;
					case 129:
						this.L = (long)value;
						break;
					case 2:
						this.D = (double)value;
						break;
					case 32898:
						this.F = (float)value;
						break;
					case 4:
						this.S = (string)value;
						break;
					case 32900:
						this.IP = (IntPtr)value;
						break;
					default:
						throw new HTupleAccessException("Attempting to assign object containing invalid type");
					}
				}
			}
		}

		public object[] OArr
		{
			get
			{
				return this.elements.O;
			}
			set
			{
				if (this.elements is HTupleElementsMixed)
				{
					this.elements.O = value;
				}
				else
				{
					switch (HTupleImplementation.GetObjectsType(value))
					{
					case 1:
						this.IArr = Array.ConvertAll(value, HTupleElements.ObjectToInt);
						break;
					case 129:
						this.LArr = Array.ConvertAll(value, HTupleElements.ObjectToLong);
						break;
					case 2:
						this.DArr = Array.ConvertAll(value, HTupleElements.ObjectToDouble);
						break;
					case 32898:
						this.FArr = Array.ConvertAll(value, HTupleElements.ObjectToFloat);
						break;
					case 4:
						this.SArr = Array.ConvertAll(value, HTupleElements.ObjectToString);
						break;
					case 32900:
						this.IPArr = Array.ConvertAll(value, HTupleElements.ObjectToIntPtr);
						break;
					default:
						throw new HTupleAccessException("Attempting to assign object containing invalid type");
					}
				}
			}
		}

		public float F
		{
			get
			{
				return (float)this.D;
			}
			set
			{
				this.D = (double)value;
			}
		}

		public float[] FArr
		{
			get
			{
				double[] dArr = this.DArr;
				float[] array = new float[dArr.Length];
				for (int i = 0; i < dArr.Length; i++)
				{
					array[i] = (float)dArr[i];
				}
				return array;
			}
			set
			{
				double[] array = new double[value.Length];
				for (int i = 0; i < value.Length; i++)
				{
					array[i] = (double)value[i];
				}
				this.DArr = array;
			}
		}

		public IntPtr IP
		{
			get
			{
				if (HalconAPI.isPlatform64 && this.Type == HTupleType.LONG)
				{
					return new IntPtr(this.L);
				}
				if (this.Type == HTupleType.INTEGER)
				{
					return new IntPtr(this.I);
				}
				throw new HTupleAccessException("Value does not represent a pointer on this platform");
			}
			set
			{
				if (HalconAPI.isPlatform64)
				{
					this.L = value.ToInt64();
				}
				else
				{
					this.I = value.ToInt32();
				}
			}
		}

		public IntPtr[] IPArr
		{
			get
			{
				if (HalconAPI.isPlatform64 && this.Type == HTupleType.LONG)
				{
					IntPtr[] array = new IntPtr[this.LArr.Length];
					for (int i = 0; i < this.LArr.Length; i++)
					{
						array[i] = new IntPtr(this.LArr[i]);
					}
					return array;
				}
				if (this.Type == HTupleType.INTEGER)
				{
					IntPtr[] array2 = new IntPtr[this.IArr.Length];
					for (int j = 0; j < this.IArr.Length; j++)
					{
						array2[j] = new IntPtr(this.IArr[j]);
					}
					return array2;
				}
				throw new HTupleAccessException("Value does not represent a pointer on this platform");
			}
			set
			{
				if (HalconAPI.isPlatform64)
				{
					long[] array = new long[value.Length];
					for (int i = 0; i < value.Length; i++)
					{
						array[i] = value[i].ToInt64();
					}
					this.LArr = array;
				}
				else
				{
					int[] array2 = new int[value.Length];
					for (int j = 0; j < value.Length; j++)
					{
						array2[j] = value[j].ToInt32();
					}
					this.IArr = array2;
				}
			}
		}

		public HTupleType Type
		{
			get
			{
				return this.elements.Type;
			}
		}

		internal int Length
		{
			get
			{
				return this.elements.Length;
			}
		}

		internal HTupleElements()
		{
			this.parent = null;
			this.elements = new HTupleElementsImplementation();
		}

		internal HTupleElements(HTuple parent, HTupleInt32 source, int index)
		{
			this.parent = parent;
			this.elements = new HTupleElementsInt32(source, index);
		}

		internal HTupleElements(HTuple parent, HTupleInt32 source, int[] indices)
		{
			this.parent = parent;
			this.elements = new HTupleElementsInt32(source, indices);
		}

		internal HTupleElements(HTuple parent, HTupleInt64 tupleImp, int index)
		{
			this.parent = parent;
			this.elements = new HTupleElementsInt64(tupleImp, index);
		}

		internal HTupleElements(HTuple parent, HTupleInt64 tupleImp, int[] indices)
		{
			this.parent = parent;
			this.elements = new HTupleElementsInt64(tupleImp, indices);
		}

		internal HTupleElements(HTuple parent, HTupleDouble tupleImp, int index)
		{
			this.parent = parent;
			this.elements = new HTupleElementsDouble(tupleImp, index);
		}

		internal HTupleElements(HTuple parent, HTupleDouble tupleImp, int[] indices)
		{
			this.parent = parent;
			this.elements = new HTupleElementsDouble(tupleImp, indices);
		}

		internal HTupleElements(HTuple parent, HTupleString tupleImp, int index)
		{
			this.parent = parent;
			this.elements = new HTupleElementsString(tupleImp, index);
		}

		internal HTupleElements(HTuple parent, HTupleString tupleImp, int[] indices)
		{
			this.parent = parent;
			this.elements = new HTupleElementsString(tupleImp, indices);
		}

		internal HTupleElements(HTuple parent, HTupleMixed tupleImp, int index)
		{
			this.parent = parent;
			this.elements = new HTupleElementsMixed(tupleImp, index);
		}

		internal HTupleElements(HTuple parent, HTupleMixed tupleImp, int[] indices)
		{
			this.parent = parent;
			this.elements = new HTupleElementsMixed(tupleImp, indices);
		}

		public static int ObjectToInt(object o)
		{
			return (int)o;
		}

		public static long ObjectToLong(object o)
		{
			return (long)o;
		}

		public static double ObjectToDouble(object o)
		{
			return (double)o;
		}

		public static float ObjectToFloat(object o)
		{
			return (float)o;
		}

		public static string ObjectToString(object o)
		{
			return (string)o;
		}

		public static IntPtr ObjectToIntPtr(object o)
		{
			return (IntPtr)o;
		}

		internal void ConvertToMixed()
		{
			if (this.elements is HTupleElementsMixed)
			{
				throw new HTupleAccessException();
			}
			this.elements = this.parent.ConvertToMixed(this.elements.getIndices());
		}

		public static HTuple operator +(HTupleElements e1, int t2)
		{
			return (HTuple)e1 + (HTuple)t2;
		}

		public static HTuple operator +(HTupleElements e1, long t2)
		{
			return (HTuple)e1 + (HTuple)t2;
		}

		public static HTuple operator +(HTupleElements e1, float t2)
		{
			return (HTuple)e1 + (HTuple)(double)t2;
		}

		public static HTuple operator +(HTupleElements e1, double t2)
		{
			return (HTuple)e1 + (HTuple)t2;
		}

		public static HTuple operator +(HTupleElements e1, string t2)
		{
			return (HTuple)e1 + (HTuple)t2;
		}

		public static HTuple operator +(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 + (HTuple)t2;
		}

		public static HTuple operator +(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 + t2;
		}

		public static HTuple operator -(HTupleElements e1, int t2)
		{
			return (HTuple)e1 - (HTuple)t2;
		}

		public static HTuple operator -(HTupleElements e1, long t2)
		{
			return (HTuple)e1 - (HTuple)t2;
		}

		public static HTuple operator -(HTupleElements e1, float t2)
		{
			return (HTuple)e1 - (HTuple)(double)t2;
		}

		public static HTuple operator -(HTupleElements e1, double t2)
		{
			return (HTuple)e1 - (HTuple)t2;
		}

		public static HTuple operator -(HTupleElements e1, string t2)
		{
			return (HTuple)e1 - (HTuple)t2;
		}

		public static HTuple operator -(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 - (HTuple)t2;
		}

		public static HTuple operator -(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 - t2;
		}

		public static HTuple operator *(HTupleElements e1, int t2)
		{
			return (HTuple)e1 * (HTuple)t2;
		}

		public static HTuple operator *(HTupleElements e1, long t2)
		{
			return (HTuple)e1 * (HTuple)t2;
		}

		public static HTuple operator *(HTupleElements e1, float t2)
		{
			return (HTuple)e1 * (HTuple)(double)t2;
		}

		public static HTuple operator *(HTupleElements e1, double t2)
		{
			return (HTuple)e1 * (HTuple)t2;
		}

		public static HTuple operator *(HTupleElements e1, string t2)
		{
			return (HTuple)e1 * (HTuple)t2;
		}

		public static HTuple operator *(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 * (HTuple)t2;
		}

		public static HTuple operator *(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 * t2;
		}

		public static HTuple operator /(HTupleElements e1, int t2)
		{
			return (HTuple)e1 / (HTuple)t2;
		}

		public static HTuple operator /(HTupleElements e1, long t2)
		{
			return (HTuple)e1 / (HTuple)t2;
		}

		public static HTuple operator /(HTupleElements e1, float t2)
		{
			return (HTuple)e1 / (HTuple)(double)t2;
		}

		public static HTuple operator /(HTupleElements e1, double t2)
		{
			return (HTuple)e1 / (HTuple)t2;
		}

		public static HTuple operator /(HTupleElements e1, string t2)
		{
			return (HTuple)e1 / (HTuple)t2;
		}

		public static HTuple operator /(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 / (HTuple)t2;
		}

		public static HTuple operator /(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 / t2;
		}

		public static HTuple operator %(HTupleElements e1, int t2)
		{
			return (HTuple)e1 % (HTuple)t2;
		}

		public static HTuple operator %(HTupleElements e1, long t2)
		{
			return (HTuple)e1 % (HTuple)t2;
		}

		public static HTuple operator %(HTupleElements e1, float t2)
		{
			return (HTuple)e1 % (HTuple)(double)t2;
		}

		public static HTuple operator %(HTupleElements e1, double t2)
		{
			return (HTuple)e1 % (HTuple)t2;
		}

		public static HTuple operator %(HTupleElements e1, string t2)
		{
			return (HTuple)e1 % (HTuple)t2;
		}

		public static HTuple operator %(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 % (HTuple)t2;
		}

		public static HTuple operator %(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 % t2;
		}

		public static HTuple operator &(HTupleElements e1, int t2)
		{
			return (HTuple)e1 & (HTuple)t2;
		}

		public static HTuple operator &(HTupleElements e1, long t2)
		{
			return (HTuple)e1 & (HTuple)t2;
		}

		public static HTuple operator &(HTupleElements e1, float t2)
		{
			return (HTuple)e1 & (HTuple)(double)t2;
		}

		public static HTuple operator &(HTupleElements e1, double t2)
		{
			return (HTuple)e1 & (HTuple)t2;
		}

		public static HTuple operator &(HTupleElements e1, string t2)
		{
			return (HTuple)e1 & (HTuple)t2;
		}

		public static HTuple operator &(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 & (HTuple)t2;
		}

		public static HTuple operator &(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 & t2;
		}

		public static HTuple operator |(HTupleElements e1, int t2)
		{
			return (HTuple)e1 | (HTuple)t2;
		}

		public static HTuple operator |(HTupleElements e1, long t2)
		{
			return (HTuple)e1 | (HTuple)t2;
		}

		public static HTuple operator |(HTupleElements e1, float t2)
		{
			return (HTuple)e1 | (HTuple)(double)t2;
		}

		public static HTuple operator |(HTupleElements e1, double t2)
		{
			return (HTuple)e1 | (HTuple)t2;
		}

		public static HTuple operator |(HTupleElements e1, string t2)
		{
			return (HTuple)e1 | (HTuple)t2;
		}

		public static HTuple operator |(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 | (HTuple)t2;
		}

		public static HTuple operator |(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 | t2;
		}

		public static HTuple operator ^(HTupleElements e1, int t2)
		{
			return (HTuple)e1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTupleElements e1, long t2)
		{
			return (HTuple)e1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTupleElements e1, float t2)
		{
			return (HTuple)e1 ^ (HTuple)(double)t2;
		}

		public static HTuple operator ^(HTupleElements e1, double t2)
		{
			return (HTuple)e1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTupleElements e1, string t2)
		{
			return (HTuple)e1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 ^ t2;
		}

		public static bool operator <(HTupleElements e1, int t2)
		{
			return (HTuple)e1 < (HTuple)t2;
		}

		public static bool operator <(HTupleElements e1, long t2)
		{
			return (HTuple)e1 < (HTuple)t2;
		}

		public static bool operator <(HTupleElements e1, float t2)
		{
			return (HTuple)e1 < (HTuple)(double)t2;
		}

		public static bool operator <(HTupleElements e1, double t2)
		{
			return (HTuple)e1 < (HTuple)t2;
		}

		public static bool operator <(HTupleElements e1, string t2)
		{
			return (HTuple)e1 < (HTuple)t2;
		}

		public static bool operator <(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 < (HTuple)t2;
		}

		public static bool operator <(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 < t2;
		}

		public static bool operator >(HTupleElements e1, int t2)
		{
			return (HTuple)e1 > (HTuple)t2;
		}

		public static bool operator >(HTupleElements e1, long t2)
		{
			return (HTuple)e1 > (HTuple)t2;
		}

		public static bool operator >(HTupleElements e1, float t2)
		{
			return (HTuple)e1 > (HTuple)(double)t2;
		}

		public static bool operator >(HTupleElements e1, double t2)
		{
			return (HTuple)e1 > (HTuple)t2;
		}

		public static bool operator >(HTupleElements e1, string t2)
		{
			return (HTuple)e1 > (HTuple)t2;
		}

		public static bool operator >(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 > (HTuple)t2;
		}

		public static bool operator >(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 > t2;
		}

		public static bool operator <=(HTupleElements e1, int t2)
		{
			return (HTuple)e1 <= (HTuple)t2;
		}

		public static bool operator <=(HTupleElements e1, long t2)
		{
			return (HTuple)e1 <= (HTuple)t2;
		}

		public static bool operator <=(HTupleElements e1, float t2)
		{
			return (HTuple)e1 <= (HTuple)(double)t2;
		}

		public static bool operator <=(HTupleElements e1, double t2)
		{
			return (HTuple)e1 <= (HTuple)t2;
		}

		public static bool operator <=(HTupleElements e1, string t2)
		{
			return (HTuple)e1 <= (HTuple)t2;
		}

		public static bool operator <=(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 <= (HTuple)t2;
		}

		public static bool operator <=(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 <= t2;
		}

		public static bool operator >=(HTupleElements e1, int t2)
		{
			return (HTuple)e1 >= (HTuple)t2;
		}

		public static bool operator >=(HTupleElements e1, long t2)
		{
			return (HTuple)e1 >= (HTuple)t2;
		}

		public static bool operator >=(HTupleElements e1, float t2)
		{
			return (HTuple)e1 >= (HTuple)(double)t2;
		}

		public static bool operator >=(HTupleElements e1, double t2)
		{
			return (HTuple)e1 >= (HTuple)t2;
		}

		public static bool operator >=(HTupleElements e1, string t2)
		{
			return (HTuple)e1 >= (HTuple)t2;
		}

		public static bool operator >=(HTupleElements e1, HTupleElements t2)
		{
			return (HTuple)e1 >= (HTuple)t2;
		}

		public static bool operator >=(HTupleElements e1, HTuple t2)
		{
			return (HTuple)e1 >= t2;
		}

		public static implicit operator bool(HTupleElements hte)
		{
			return hte.I != 0;
		}

		public static implicit operator int(HTupleElements hte)
		{
			return hte.I;
		}

		public static implicit operator long(HTupleElements hte)
		{
			return hte.L;
		}

		public static implicit operator IntPtr(HTupleElements hte)
		{
			return hte.IP;
		}

		public static implicit operator double(HTupleElements hte)
		{
			return hte.D;
		}

		public static implicit operator string(HTupleElements hte)
		{
			return hte.S;
		}

		public static implicit operator HTupleElements(int i)
		{
			return new HTuple(i)[0];
		}

		public static implicit operator HTupleElements(long l)
		{
			return new HTuple(l)[0];
		}

		public static implicit operator HTupleElements(IntPtr ip)
		{
			return new HTuple(ip)[0];
		}

		public static implicit operator HTupleElements(double d)
		{
			return new HTuple(d)[0];
		}

		public static implicit operator HTupleElements(string s)
		{
			return new HTuple(s)[0];
		}
	}
}
