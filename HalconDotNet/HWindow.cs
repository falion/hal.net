using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace VisionConfig
{
	public class HWindow : HTool
	{
		public delegate void ContentUpdateCallback(IntPtr context);

		private ContentUpdateCallback _callback;

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HWindow()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HWindow(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HWindow obj)
		{
			obj = new HWindow(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HWindow[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HWindow[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HWindow(hTuple[i].IP);
			}
			return err;
		}

		public HWindow(int row, int column, int width, int height, HTuple fatherWindow, string mode, string machine)
		{
			IntPtr proc = HalconAPI.PreCall(1178);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.Store(proc, 4, fatherWindow);
			HalconAPI.StoreS(proc, 5, mode);
			HalconAPI.StoreS(proc, 6, machine);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fatherWindow);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HWindow(int row, int column, int width, int height, IntPtr fatherWindow, string mode, string machine)
		{
			IntPtr proc = HalconAPI.PreCall(1178);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.StoreIP(proc, 4, fatherWindow);
			HalconAPI.StoreS(proc, 5, mode);
			HalconAPI.StoreS(proc, 6, machine);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CloseWindow()
		{
			this.Dispose();
		}

		[DllImport("X11", EntryPoint = "XInitThreads")]
		private static extern int _XInitThreads();

		public static int XInitThreads()
		{
			try
			{
				return HWindow._XInitThreads();
			}
			catch (DllNotFoundException)
			{
				return 0;
			}
		}

		public void OnContentUpdate(ContentUpdateCallback f)
		{
			this._callback = f;
			this.SetContentUpdateCallback(Marshal.GetFunctionPointerForDelegate(this._callback), new IntPtr(0));
		}

		public void DispXld(HXLD XLDObject)
		{
			IntPtr proc = HalconAPI.PreCall(74);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, XLDObject);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(XLDObject);
		}

		public HImage GetWindowBackgroundImage()
		{
			IntPtr proc = HalconAPI.PreCall(1161);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DetachBackgroundFromWindow()
		{
			IntPtr proc = HalconAPI.PreCall(1163);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AttachBackgroundToWindow(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(1164);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void DetachDrawingObjectFromWindow(HDrawingObject drawID)
		{
			IntPtr proc = HalconAPI.PreCall(1165);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, drawID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(drawID);
		}

		public void AttachDrawingObjectToWindow(HDrawingObject drawID)
		{
			IntPtr proc = HalconAPI.PreCall(1166);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, drawID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(drawID);
		}

		public void UpdateWindowPose(HTuple lastRow, HTuple lastCol, HTuple currentRow, HTuple currentCol, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1167);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, lastRow);
			HalconAPI.Store(proc, 2, lastCol);
			HalconAPI.Store(proc, 3, currentRow);
			HalconAPI.Store(proc, 4, currentCol);
			HalconAPI.StoreS(proc, 5, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(lastRow);
			HalconAPI.UnpinTuple(lastCol);
			HalconAPI.UnpinTuple(currentRow);
			HalconAPI.UnpinTuple(currentCol);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void UpdateWindowPose(double lastRow, double lastCol, double currentRow, double currentCol, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1167);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, lastRow);
			HalconAPI.StoreD(proc, 2, lastCol);
			HalconAPI.StoreD(proc, 3, currentRow);
			HalconAPI.StoreD(proc, 4, currentCol);
			HalconAPI.StoreS(proc, 5, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void UnprojectCoordinates(HImage image, HTuple row, HTuple column, out int imageRow, out int imageColumn, out HTuple height)
		{
			IntPtr proc = HalconAPI.PreCall(1168);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HalconAPI.LoadI(proc, 0, err, out imageRow);
			err = HalconAPI.LoadI(proc, 1, err, out imageColumn);
			err = HTuple.LoadNew(proc, 2, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void UnprojectCoordinates(HImage image, double row, double column, out int imageRow, out int imageColumn, out int height)
		{
			IntPtr proc = HalconAPI.PreCall(1168);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out imageRow);
			err = HalconAPI.LoadI(proc, 1, err, out imageColumn);
			err = HalconAPI.LoadI(proc, 2, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public IntPtr GetOsWindowHandle(out IntPtr OSDisplayHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1169);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			IntPtr result = default(IntPtr);
			err = HalconAPI.LoadIP(proc, 0, err, out result);
			err = HalconAPI.LoadIP(proc, 1, err, out OSDisplayHandle);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetWindowDc(IntPtr WINHDC)
		{
			IntPtr proc = HalconAPI.PreCall(1170);
			base.Store(proc, 0);
			HalconAPI.StoreIP(proc, 1, WINHDC);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void NewExternWindow(IntPtr WINHWnd, int row, int column, int width, int height)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1171);
			HalconAPI.StoreIP(proc, 0, WINHWnd);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SlideImage(HWindow windowHandleSource2, HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1172);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, windowHandleSource2);
			HalconAPI.Store(proc, 2, windowHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandleSource2);
			GC.KeepAlive(windowHandle);
		}

		public void SetWindowExtents(int row, int column, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1174);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void OpenWindow(int row, int column, int width, int height, HTuple fatherWindow, string mode, string machine)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1178);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.Store(proc, 4, fatherWindow);
			HalconAPI.StoreS(proc, 5, mode);
			HalconAPI.StoreS(proc, 6, machine);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fatherWindow);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenWindow(int row, int column, int width, int height, IntPtr fatherWindow, string mode, string machine)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1178);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.StoreIP(proc, 4, fatherWindow);
			HalconAPI.StoreS(proc, 5, mode);
			HalconAPI.StoreS(proc, 6, machine);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenTextwindow(int row, int column, int width, int height, int borderWidth, string borderColor, string backgroundColor, HTuple fatherWindow, string mode, string machine)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1179);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.StoreI(proc, 4, borderWidth);
			HalconAPI.StoreS(proc, 5, borderColor);
			HalconAPI.StoreS(proc, 6, backgroundColor);
			HalconAPI.Store(proc, 7, fatherWindow);
			HalconAPI.StoreS(proc, 8, mode);
			HalconAPI.StoreS(proc, 9, machine);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fatherWindow);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenTextwindow(int row, int column, int width, int height, int borderWidth, string borderColor, string backgroundColor, IntPtr fatherWindow, string mode, string machine)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1179);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.StoreI(proc, 4, borderWidth);
			HalconAPI.StoreS(proc, 5, borderColor);
			HalconAPI.StoreS(proc, 6, backgroundColor);
			HalconAPI.StoreIP(proc, 7, fatherWindow);
			HalconAPI.StoreS(proc, 8, mode);
			HalconAPI.StoreS(proc, 9, machine);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void MoveRectangle(HTuple row1, HTuple column1, HTuple row2, HTuple column2, HTuple destRow, HTuple destColumn)
		{
			IntPtr proc = HalconAPI.PreCall(1180);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row1);
			HalconAPI.Store(proc, 2, column1);
			HalconAPI.Store(proc, 3, row2);
			HalconAPI.Store(proc, 4, column2);
			HalconAPI.Store(proc, 5, destRow);
			HalconAPI.Store(proc, 6, destColumn);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HalconAPI.UnpinTuple(destRow);
			HalconAPI.UnpinTuple(destColumn);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void MoveRectangle(int row1, int column1, int row2, int column2, int destRow, int destColumn)
		{
			IntPtr proc = HalconAPI.PreCall(1180);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row1);
			HalconAPI.StoreI(proc, 2, column1);
			HalconAPI.StoreI(proc, 3, row2);
			HalconAPI.StoreI(proc, 4, column2);
			HalconAPI.StoreI(proc, 5, destRow);
			HalconAPI.StoreI(proc, 6, destColumn);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string GetWindowType()
		{
			IntPtr proc = HalconAPI.PreCall(1181);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetWindowPointer3(out int imageRed, out int imageGreen, out int imageBlue, out int width, out int height)
		{
			IntPtr proc = HalconAPI.PreCall(1182);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out imageRed);
			err = HalconAPI.LoadI(proc, 1, err, out imageGreen);
			err = HalconAPI.LoadI(proc, 2, err, out imageBlue);
			err = HalconAPI.LoadI(proc, 3, err, out width);
			err = HalconAPI.LoadI(proc, 4, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetWindowExtents(out int row, out int column, out int width, out int height)
		{
			IntPtr proc = HalconAPI.PreCall(1183);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out row);
			err = HalconAPI.LoadI(proc, 1, err, out column);
			err = HalconAPI.LoadI(proc, 2, err, out width);
			err = HalconAPI.LoadI(proc, 3, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage DumpWindowImage()
		{
			IntPtr proc = HalconAPI.PreCall(1184);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DumpWindow(HTuple device, string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1185);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, device);
			HalconAPI.StoreS(proc, 2, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(device);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DumpWindow(string device, string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1185);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, device);
			HalconAPI.StoreS(proc, 2, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CopyRectangle(HWindow windowHandleDestination, HTuple row1, HTuple column1, HTuple row2, HTuple column2, HTuple destRow, HTuple destColumn)
		{
			IntPtr proc = HalconAPI.PreCall(1186);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, windowHandleDestination);
			HalconAPI.Store(proc, 2, row1);
			HalconAPI.Store(proc, 3, column1);
			HalconAPI.Store(proc, 4, row2);
			HalconAPI.Store(proc, 5, column2);
			HalconAPI.Store(proc, 6, destRow);
			HalconAPI.Store(proc, 7, destColumn);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HalconAPI.UnpinTuple(destRow);
			HalconAPI.UnpinTuple(destColumn);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandleDestination);
		}

		public void CopyRectangle(HWindow windowHandleDestination, int row1, int column1, int row2, int column2, int destRow, int destColumn)
		{
			IntPtr proc = HalconAPI.PreCall(1186);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, windowHandleDestination);
			HalconAPI.StoreI(proc, 2, row1);
			HalconAPI.StoreI(proc, 3, column1);
			HalconAPI.StoreI(proc, 4, row2);
			HalconAPI.StoreI(proc, 5, column2);
			HalconAPI.StoreI(proc, 6, destRow);
			HalconAPI.StoreI(proc, 7, destColumn);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandleDestination);
		}

		public void ClearWindow()
		{
			IntPtr proc = HalconAPI.PreCall(1188);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ClearRectangle(HTuple row1, HTuple column1, HTuple row2, HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1189);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row1);
			HalconAPI.Store(proc, 2, column1);
			HalconAPI.Store(proc, 3, row2);
			HalconAPI.Store(proc, 4, column2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ClearRectangle(int row1, int column1, int row2, int column2)
		{
			IntPtr proc = HalconAPI.PreCall(1189);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row1);
			HalconAPI.StoreI(proc, 2, column1);
			HalconAPI.StoreI(proc, 3, row2);
			HalconAPI.StoreI(proc, 4, column2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteString(HTuple stringVal)
		{
			IntPtr proc = HalconAPI.PreCall(1190);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, stringVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(stringVal);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteString(string stringVal)
		{
			IntPtr proc = HalconAPI.PreCall(1190);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, stringVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetTshape(string textCursor)
		{
			IntPtr proc = HalconAPI.PreCall(1191);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, textCursor);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetTposition(int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(1192);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string ReadString(string inString, int length)
		{
			IntPtr proc = HalconAPI.PreCall(1193);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, inString);
			HalconAPI.StoreI(proc, 2, length);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string ReadChar(out string code)
		{
			IntPtr proc = HalconAPI.PreCall(1194);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadS(proc, 1, err, out code);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void NewLine()
		{
			IntPtr proc = HalconAPI.PreCall(1195);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string GetTshape()
		{
			IntPtr proc = HalconAPI.PreCall(1196);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetTposition(out int row, out int column)
		{
			IntPtr proc = HalconAPI.PreCall(1197);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out row);
			err = HalconAPI.LoadI(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetFontExtents(out HTuple maxDescent, out HTuple maxWidth, out HTuple maxHeight)
		{
			IntPtr proc = HalconAPI.PreCall(1198);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out maxDescent);
			err = HTuple.LoadNew(proc, 2, err, out maxWidth);
			err = HTuple.LoadNew(proc, 3, err, out maxHeight);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetFontExtents(out int maxDescent, out int maxWidth, out int maxHeight)
		{
			IntPtr proc = HalconAPI.PreCall(1198);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out maxDescent);
			err = HalconAPI.LoadI(proc, 2, err, out maxWidth);
			err = HalconAPI.LoadI(proc, 3, err, out maxHeight);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetStringExtents(HTuple values, out HTuple descent, out HTuple width, out HTuple height)
		{
			IntPtr proc = HalconAPI.PreCall(1199);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, values);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(values);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out descent);
			err = HTuple.LoadNew(proc, 2, err, out width);
			err = HTuple.LoadNew(proc, 3, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetStringExtents(string values, out int descent, out int width, out int height)
		{
			IntPtr proc = HalconAPI.PreCall(1199);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, values);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out descent);
			err = HalconAPI.LoadI(proc, 2, err, out width);
			err = HalconAPI.LoadI(proc, 3, err, out height);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryFont()
		{
			IntPtr proc = HalconAPI.PreCall(1200);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryTshape()
		{
			IntPtr proc = HalconAPI.PreCall(1201);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetFont(string font)
		{
			IntPtr proc = HalconAPI.PreCall(1202);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, font);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string GetFont()
		{
			IntPtr proc = HalconAPI.PreCall(1203);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetWindowParam(string param)
		{
			IntPtr proc = HalconAPI.PreCall(1221);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, param);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetWindowParam(string param, HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(1222);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, param);
			HalconAPI.Store(proc, 2, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(value);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetWindowParam(string param, string value)
		{
			IntPtr proc = HalconAPI.PreCall(1222);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, param);
			HalconAPI.StoreS(proc, 2, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetShape(string shape)
		{
			IntPtr proc = HalconAPI.PreCall(1223);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, shape);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetRgb(HTuple red, HTuple green, HTuple blue)
		{
			IntPtr proc = HalconAPI.PreCall(1224);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, red);
			HalconAPI.Store(proc, 2, green);
			HalconAPI.Store(proc, 3, blue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(red);
			HalconAPI.UnpinTuple(green);
			HalconAPI.UnpinTuple(blue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetRgb(int red, int green, int blue)
		{
			IntPtr proc = HalconAPI.PreCall(1224);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, red);
			HalconAPI.StoreI(proc, 2, green);
			HalconAPI.StoreI(proc, 3, blue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetPixel(HTuple pixel)
		{
			IntPtr proc = HalconAPI.PreCall(1225);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, pixel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pixel);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetPixel(int pixel)
		{
			IntPtr proc = HalconAPI.PreCall(1225);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, pixel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetPartStyle(int style)
		{
			IntPtr proc = HalconAPI.PreCall(1226);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, style);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetPart(HTuple row1, HTuple column1, HTuple row2, HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1227);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row1);
			HalconAPI.Store(proc, 2, column1);
			HalconAPI.Store(proc, 3, row2);
			HalconAPI.Store(proc, 4, column2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetPart(int row1, int column1, int row2, int column2)
		{
			IntPtr proc = HalconAPI.PreCall(1227);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row1);
			HalconAPI.StoreI(proc, 2, column1);
			HalconAPI.StoreI(proc, 3, row2);
			HalconAPI.StoreI(proc, 4, column2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetPaint(HTuple mode)
		{
			IntPtr proc = HalconAPI.PreCall(1228);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(mode);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetLineWidth(double width)
		{
			IntPtr proc = HalconAPI.PreCall(1229);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, width);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetLineStyle(HTuple style)
		{
			IntPtr proc = HalconAPI.PreCall(1230);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, style);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(style);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetLineApprox(int approximation)
		{
			IntPtr proc = HalconAPI.PreCall(1231);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, approximation);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetInsert(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1232);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetHsi(HTuple hue, HTuple saturation, HTuple intensity)
		{
			IntPtr proc = HalconAPI.PreCall(1233);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, hue);
			HalconAPI.Store(proc, 2, saturation);
			HalconAPI.Store(proc, 3, intensity);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hue);
			HalconAPI.UnpinTuple(saturation);
			HalconAPI.UnpinTuple(intensity);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetHsi(int hue, int saturation, int intensity)
		{
			IntPtr proc = HalconAPI.PreCall(1233);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, hue);
			HalconAPI.StoreI(proc, 2, saturation);
			HalconAPI.StoreI(proc, 3, intensity);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetGray(HTuple grayValues)
		{
			IntPtr proc = HalconAPI.PreCall(1234);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, grayValues);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(grayValues);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetGray(int grayValues)
		{
			IntPtr proc = HalconAPI.PreCall(1234);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, grayValues);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDraw(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1235);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetComprise(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1236);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetColored(int numberOfColors)
		{
			IntPtr proc = HalconAPI.PreCall(1237);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, numberOfColors);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetColor(HTuple color)
		{
			IntPtr proc = HalconAPI.PreCall(1238);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, color);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(color);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetColor(string color)
		{
			IntPtr proc = HalconAPI.PreCall(1238);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, color);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string GetShape()
		{
			IntPtr proc = HalconAPI.PreCall(1239);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetRgb(out HTuple red, out HTuple green, out HTuple blue)
		{
			IntPtr proc = HalconAPI.PreCall(1240);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out red);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out green);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out blue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetPixel()
		{
			IntPtr proc = HalconAPI.PreCall(1241);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetPartStyle()
		{
			IntPtr proc = HalconAPI.PreCall(1242);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetPart(out HTuple row1, out HTuple column1, out HTuple row2, out HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1243);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out row1);
			err = HTuple.LoadNew(proc, 1, err, out column1);
			err = HTuple.LoadNew(proc, 2, err, out row2);
			err = HTuple.LoadNew(proc, 3, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetPart(out int row1, out int column1, out int row2, out int column2)
		{
			IntPtr proc = HalconAPI.PreCall(1243);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out row1);
			err = HalconAPI.LoadI(proc, 1, err, out column1);
			err = HalconAPI.LoadI(proc, 2, err, out row2);
			err = HalconAPI.LoadI(proc, 3, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetPaint()
		{
			IntPtr proc = HalconAPI.PreCall(1244);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double GetLineWidth()
		{
			IntPtr proc = HalconAPI.PreCall(1245);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetLineStyle()
		{
			IntPtr proc = HalconAPI.PreCall(1246);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetLineApprox()
		{
			IntPtr proc = HalconAPI.PreCall(1247);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string GetInsert()
		{
			IntPtr proc = HalconAPI.PreCall(1248);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetHsi(out HTuple saturation, out HTuple intensity)
		{
			IntPtr proc = HalconAPI.PreCall(1249);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out saturation);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out intensity);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string GetDraw()
		{
			IntPtr proc = HalconAPI.PreCall(1250);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryPaint()
		{
			IntPtr proc = HalconAPI.PreCall(1253);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryInsert()
		{
			IntPtr proc = HalconAPI.PreCall(1255);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryGray()
		{
			IntPtr proc = HalconAPI.PreCall(1256);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryAllColors()
		{
			IntPtr proc = HalconAPI.PreCall(1258);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryColor()
		{
			IntPtr proc = HalconAPI.PreCall(1259);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion GetIcon()
		{
			IntPtr proc = HalconAPI.PreCall(1260);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetIcon(HRegion icon)
		{
			IntPtr proc = HalconAPI.PreCall(1261);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, icon);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(icon);
		}

		public void DispRegion(HRegion dispRegions)
		{
			IntPtr proc = HalconAPI.PreCall(1262);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, dispRegions);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(dispRegions);
		}

		public void DispRectangle2(HTuple centerRow, HTuple centerCol, HTuple phi, HTuple length1, HTuple length2)
		{
			IntPtr proc = HalconAPI.PreCall(1263);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, centerRow);
			HalconAPI.Store(proc, 2, centerCol);
			HalconAPI.Store(proc, 3, phi);
			HalconAPI.Store(proc, 4, length1);
			HalconAPI.Store(proc, 5, length2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(centerRow);
			HalconAPI.UnpinTuple(centerCol);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(length1);
			HalconAPI.UnpinTuple(length2);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispRectangle2(double centerRow, double centerCol, double phi, double length1, double length2)
		{
			IntPtr proc = HalconAPI.PreCall(1263);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, centerRow);
			HalconAPI.StoreD(proc, 2, centerCol);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreD(proc, 4, length1);
			HalconAPI.StoreD(proc, 5, length2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispRectangle1(HTuple row1, HTuple column1, HTuple row2, HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1264);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row1);
			HalconAPI.Store(proc, 2, column1);
			HalconAPI.Store(proc, 3, row2);
			HalconAPI.Store(proc, 4, column2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispRectangle1(double row1, double column1, double row2, double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1264);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row1);
			HalconAPI.StoreD(proc, 2, column1);
			HalconAPI.StoreD(proc, 3, row2);
			HalconAPI.StoreD(proc, 4, column2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispPolygon(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1265);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispLine(HTuple row1, HTuple column1, HTuple row2, HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1266);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row1);
			HalconAPI.Store(proc, 2, column1);
			HalconAPI.Store(proc, 3, row2);
			HalconAPI.Store(proc, 4, column2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispLine(double row1, double column1, double row2, double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1266);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row1);
			HalconAPI.StoreD(proc, 2, column1);
			HalconAPI.StoreD(proc, 3, row2);
			HalconAPI.StoreD(proc, 4, column2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispCross(HTuple row, HTuple column, double size, double angle)
		{
			IntPtr proc = HalconAPI.PreCall(1267);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreD(proc, 3, size);
			HalconAPI.StoreD(proc, 4, angle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispCross(double row, double column, double size, double angle)
		{
			IntPtr proc = HalconAPI.PreCall(1267);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, size);
			HalconAPI.StoreD(proc, 4, angle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispImage(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(1268);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void DispChannel(HImage multichannelImage, HTuple channel)
		{
			IntPtr proc = HalconAPI.PreCall(1269);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, multichannelImage);
			HalconAPI.Store(proc, 1, channel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(channel);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(multichannelImage);
		}

		public void DispChannel(HImage multichannelImage, int channel)
		{
			IntPtr proc = HalconAPI.PreCall(1269);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, multichannelImage);
			HalconAPI.StoreI(proc, 1, channel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(multichannelImage);
		}

		public void DispColor(HImage colorImage)
		{
			IntPtr proc = HalconAPI.PreCall(1270);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, colorImage);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(colorImage);
		}

		public void DispEllipse(HTuple centerRow, HTuple centerCol, HTuple phi, HTuple radius1, HTuple radius2)
		{
			IntPtr proc = HalconAPI.PreCall(1271);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, centerRow);
			HalconAPI.Store(proc, 2, centerCol);
			HalconAPI.Store(proc, 3, phi);
			HalconAPI.Store(proc, 4, radius1);
			HalconAPI.Store(proc, 5, radius2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(centerRow);
			HalconAPI.UnpinTuple(centerCol);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(radius1);
			HalconAPI.UnpinTuple(radius2);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispEllipse(int centerRow, int centerCol, double phi, double radius1, double radius2)
		{
			IntPtr proc = HalconAPI.PreCall(1271);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, centerRow);
			HalconAPI.StoreI(proc, 2, centerCol);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreD(proc, 4, radius1);
			HalconAPI.StoreD(proc, 5, radius2);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispDistribution(HTuple distribution, int row, int column, int scale)
		{
			IntPtr proc = HalconAPI.PreCall(1272);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, distribution);
			HalconAPI.StoreI(proc, 2, row);
			HalconAPI.StoreI(proc, 3, column);
			HalconAPI.StoreI(proc, 4, scale);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(distribution);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispCircle(HTuple row, HTuple column, HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(1273);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, radius);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispCircle(double row, double column, double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1273);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, radius);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispArrow(HTuple row1, HTuple column1, HTuple row2, HTuple column2, HTuple size)
		{
			IntPtr proc = HalconAPI.PreCall(1274);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row1);
			HalconAPI.Store(proc, 2, column1);
			HalconAPI.Store(proc, 3, row2);
			HalconAPI.Store(proc, 4, column2);
			HalconAPI.Store(proc, 5, size);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			HalconAPI.UnpinTuple(size);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispArrow(double row1, double column1, double row2, double column2, double size)
		{
			IntPtr proc = HalconAPI.PreCall(1274);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row1);
			HalconAPI.StoreD(proc, 2, column1);
			HalconAPI.StoreD(proc, 3, row2);
			HalconAPI.StoreD(proc, 4, column2);
			HalconAPI.StoreD(proc, 5, size);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispArc(HTuple centerRow, HTuple centerCol, HTuple angle, HTuple beginRow, HTuple beginCol)
		{
			IntPtr proc = HalconAPI.PreCall(1275);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, centerRow);
			HalconAPI.Store(proc, 2, centerCol);
			HalconAPI.Store(proc, 3, angle);
			HalconAPI.Store(proc, 4, beginRow);
			HalconAPI.Store(proc, 5, beginCol);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(centerRow);
			HalconAPI.UnpinTuple(centerCol);
			HalconAPI.UnpinTuple(angle);
			HalconAPI.UnpinTuple(beginRow);
			HalconAPI.UnpinTuple(beginCol);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispArc(double centerRow, double centerCol, double angle, int beginRow, int beginCol)
		{
			IntPtr proc = HalconAPI.PreCall(1275);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, centerRow);
			HalconAPI.StoreD(proc, 2, centerCol);
			HalconAPI.StoreD(proc, 3, angle);
			HalconAPI.StoreI(proc, 4, beginRow);
			HalconAPI.StoreI(proc, 5, beginCol);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispObj(HObject objectVal)
		{
			IntPtr proc = HalconAPI.PreCall(1276);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectVal);
		}

		public void SetMshape(string cursor)
		{
			IntPtr proc = HalconAPI.PreCall(1277);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, cursor);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string GetMshape()
		{
			IntPtr proc = HalconAPI.PreCall(1278);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryMshape()
		{
			IntPtr proc = HalconAPI.PreCall(1279);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetMpositionSubPix(out double row, out double column, out int button)
		{
			IntPtr proc = HalconAPI.PreCall(1280);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadI(proc, 2, err, out button);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetMposition(out int row, out int column, out int button)
		{
			IntPtr proc = HalconAPI.PreCall(1281);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out row);
			err = HalconAPI.LoadI(proc, 1, err, out column);
			err = HalconAPI.LoadI(proc, 2, err, out button);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetMbuttonSubPix(out double row, out double column, out int button)
		{
			IntPtr proc = HalconAPI.PreCall(1282);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadI(proc, 2, err, out button);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetMbutton(out int row, out int column, out int button)
		{
			IntPtr proc = HalconAPI.PreCall(1283);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out row);
			err = HalconAPI.LoadI(proc, 1, err, out column);
			err = HalconAPI.LoadI(proc, 2, err, out button);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteLut(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1284);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispLut(int row, int column, int scale)
		{
			IntPtr proc = HalconAPI.PreCall(1285);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, scale);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple QueryLut()
		{
			IntPtr proc = HalconAPI.PreCall(1286);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double GetLutStyle(out double saturation, out double intensity)
		{
			IntPtr proc = HalconAPI.PreCall(1287);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out saturation);
			err = HalconAPI.LoadD(proc, 2, err, out intensity);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetLutStyle(double hue, double saturation, double intensity)
		{
			IntPtr proc = HalconAPI.PreCall(1288);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, hue);
			HalconAPI.StoreD(proc, 2, saturation);
			HalconAPI.StoreD(proc, 3, intensity);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetLut()
		{
			IntPtr proc = HalconAPI.PreCall(1289);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetLut(HTuple lookUpTable)
		{
			IntPtr proc = HalconAPI.PreCall(1290);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, lookUpTable);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(lookUpTable);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetLut(string lookUpTable)
		{
			IntPtr proc = HalconAPI.PreCall(1290);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, lookUpTable);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string GetFix()
		{
			IntPtr proc = HalconAPI.PreCall(1291);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetFix(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1292);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string GetFixedLut()
		{
			IntPtr proc = HalconAPI.PreCall(1293);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetFixedLut(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1294);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HRegion DragRegion3(HRegion sourceRegion, HRegion maskRegion, int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(1315);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, sourceRegion);
			HalconAPI.Store(proc, 2, maskRegion);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sourceRegion);
			GC.KeepAlive(maskRegion);
			return result;
		}

		public HRegion DragRegion2(HRegion sourceRegion, int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(1316);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, sourceRegion);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sourceRegion);
			return result;
		}

		public HRegion DragRegion1(HRegion sourceRegion)
		{
			IntPtr proc = HalconAPI.PreCall(1317);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, sourceRegion);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sourceRegion);
			return result;
		}

		public HXLDCont DrawNurbsInterpMod(string rotate, string move, string scale, string keepRatio, string edit, int degree, HTuple rowsIn, HTuple colsIn, HTuple tangentsIn, out HTuple controlRows, out HTuple controlCols, out HTuple knots, out HTuple rows, out HTuple cols, out HTuple tangents)
		{
			IntPtr proc = HalconAPI.PreCall(1318);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreS(proc, 5, edit);
			HalconAPI.StoreI(proc, 6, degree);
			HalconAPI.Store(proc, 7, rowsIn);
			HalconAPI.Store(proc, 8, colsIn);
			HalconAPI.Store(proc, 9, tangentsIn);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowsIn);
			HalconAPI.UnpinTuple(colsIn);
			HalconAPI.UnpinTuple(tangentsIn);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out controlRows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out controlCols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out knots);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out tangents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont DrawNurbsInterp(string rotate, string move, string scale, string keepRatio, int degree, out HTuple controlRows, out HTuple controlCols, out HTuple knots, out HTuple rows, out HTuple cols, out HTuple tangents)
		{
			IntPtr proc = HalconAPI.PreCall(1319);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreI(proc, 5, degree);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out controlRows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out controlCols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out knots);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out tangents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont DrawNurbsMod(string rotate, string move, string scale, string keepRatio, string edit, int degree, HTuple rowsIn, HTuple colsIn, HTuple weightsIn, out HTuple rows, out HTuple cols, out HTuple weights)
		{
			IntPtr proc = HalconAPI.PreCall(1320);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreS(proc, 5, edit);
			HalconAPI.StoreI(proc, 6, degree);
			HalconAPI.Store(proc, 7, rowsIn);
			HalconAPI.Store(proc, 8, colsIn);
			HalconAPI.Store(proc, 9, weightsIn);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowsIn);
			HalconAPI.UnpinTuple(colsIn);
			HalconAPI.UnpinTuple(weightsIn);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out weights);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont DrawNurbs(string rotate, string move, string scale, string keepRatio, int degree, out HTuple rows, out HTuple cols, out HTuple weights)
		{
			IntPtr proc = HalconAPI.PreCall(1321);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreI(proc, 5, degree);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out weights);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont DrawXldMod(HXLDCont contIn, string rotate, string move, string scale, string keepRatio, string edit)
		{
			IntPtr proc = HalconAPI.PreCall(1322);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, contIn);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreS(proc, 5, edit);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contIn);
			return result;
		}

		public HXLDCont DrawXld(string rotate, string move, string scale, string keepRatio)
		{
			IntPtr proc = HalconAPI.PreCall(1323);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DrawRectangle2Mod(double rowIn, double columnIn, double phiIn, double length1In, double length2In, out double row, out double column, out double phi, out double length1, out double length2)
		{
			IntPtr proc = HalconAPI.PreCall(1324);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, rowIn);
			HalconAPI.StoreD(proc, 2, columnIn);
			HalconAPI.StoreD(proc, 3, phiIn);
			HalconAPI.StoreD(proc, 4, length1In);
			HalconAPI.StoreD(proc, 5, length2In);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out length1);
			err = HalconAPI.LoadD(proc, 4, err, out length2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawRectangle2(out double row, out double column, out double phi, out double length1, out double length2)
		{
			IntPtr proc = HalconAPI.PreCall(1325);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out length1);
			err = HalconAPI.LoadD(proc, 4, err, out length2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawRectangle1Mod(double row1In, double column1In, double row2In, double column2In, out double row1, out double column1, out double row2, out double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1326);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row1In);
			HalconAPI.StoreD(proc, 2, column1In);
			HalconAPI.StoreD(proc, 3, row2In);
			HalconAPI.StoreD(proc, 4, column2In);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row1);
			err = HalconAPI.LoadD(proc, 1, err, out column1);
			err = HalconAPI.LoadD(proc, 2, err, out row2);
			err = HalconAPI.LoadD(proc, 3, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawRectangle1(out double row1, out double column1, out double row2, out double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1327);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row1);
			err = HalconAPI.LoadD(proc, 1, err, out column1);
			err = HalconAPI.LoadD(proc, 2, err, out row2);
			err = HalconAPI.LoadD(proc, 3, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawPointMod(double rowIn, double columnIn, out double row, out double column)
		{
			IntPtr proc = HalconAPI.PreCall(1328);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, rowIn);
			HalconAPI.StoreD(proc, 2, columnIn);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawPoint(out double row, out double column)
		{
			IntPtr proc = HalconAPI.PreCall(1329);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawLineMod(double row1In, double column1In, double row2In, double column2In, out double row1, out double column1, out double row2, out double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1330);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row1In);
			HalconAPI.StoreD(proc, 2, column1In);
			HalconAPI.StoreD(proc, 3, row2In);
			HalconAPI.StoreD(proc, 4, column2In);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row1);
			err = HalconAPI.LoadD(proc, 1, err, out column1);
			err = HalconAPI.LoadD(proc, 2, err, out row2);
			err = HalconAPI.LoadD(proc, 3, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawLine(out double row1, out double column1, out double row2, out double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1331);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row1);
			err = HalconAPI.LoadD(proc, 1, err, out column1);
			err = HalconAPI.LoadD(proc, 2, err, out row2);
			err = HalconAPI.LoadD(proc, 3, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawEllipseMod(double rowIn, double columnIn, double phiIn, double radius1In, double radius2In, out double row, out double column, out double phi, out double radius1, out double radius2)
		{
			IntPtr proc = HalconAPI.PreCall(1332);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, rowIn);
			HalconAPI.StoreD(proc, 2, columnIn);
			HalconAPI.StoreD(proc, 3, phiIn);
			HalconAPI.StoreD(proc, 4, radius1In);
			HalconAPI.StoreD(proc, 5, radius2In);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out radius1);
			err = HalconAPI.LoadD(proc, 4, err, out radius2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawEllipse(out double row, out double column, out double phi, out double radius1, out double radius2)
		{
			IntPtr proc = HalconAPI.PreCall(1333);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out radius1);
			err = HalconAPI.LoadD(proc, 4, err, out radius2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawCircleMod(double rowIn, double columnIn, double radiusIn, out double row, out double column, out double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1334);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, rowIn);
			HalconAPI.StoreD(proc, 2, columnIn);
			HalconAPI.StoreD(proc, 3, radiusIn);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out radius);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DrawCircle(out double row, out double column, out double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1335);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out radius);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion DrawRegion()
		{
			IntPtr proc = HalconAPI.PreCall(1336);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion DrawPolygon()
		{
			IntPtr proc = HalconAPI.PreCall(1337);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DispCaltab(string calPlateDescr, HCamPar cameraParam, HPose calPlatePose, double scaleFac)
		{
			IntPtr proc = HalconAPI.PreCall(1945);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, calPlateDescr);
			HalconAPI.Store(proc, 2, cameraParam);
			HalconAPI.Store(proc, 3, calPlatePose);
			HalconAPI.StoreD(proc, 4, scaleFac);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(calPlatePose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ConvertCoordinatesImageToWindow(HTuple rowImage, HTuple columnImage, out HTuple rowWindow, out HTuple columnWindow)
		{
			IntPtr proc = HalconAPI.PreCall(2049);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, rowImage);
			HalconAPI.Store(proc, 2, columnImage);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowImage);
			HalconAPI.UnpinTuple(columnImage);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowWindow);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnWindow);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ConvertCoordinatesImageToWindow(double rowImage, double columnImage, out double rowWindow, out double columnWindow)
		{
			IntPtr proc = HalconAPI.PreCall(2049);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, rowImage);
			HalconAPI.StoreD(proc, 2, columnImage);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out rowWindow);
			err = HalconAPI.LoadD(proc, 1, err, out columnWindow);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ConvertCoordinatesWindowToImage(HTuple rowWindow, HTuple columnWindow, out HTuple rowImage, out HTuple columnImage)
		{
			IntPtr proc = HalconAPI.PreCall(2050);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, rowWindow);
			HalconAPI.Store(proc, 2, columnWindow);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowWindow);
			HalconAPI.UnpinTuple(columnWindow);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowImage);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnImage);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ConvertCoordinatesWindowToImage(double rowWindow, double columnWindow, out double rowImage, out double columnImage)
		{
			IntPtr proc = HalconAPI.PreCall(2050);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, rowWindow);
			HalconAPI.StoreD(proc, 2, columnWindow);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out rowImage);
			err = HalconAPI.LoadD(proc, 1, err, out columnImage);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DispText(HTuple stringVal, string coordSystem, HTuple row, HTuple column, HTuple color, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2055);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, stringVal);
			HalconAPI.StoreS(proc, 2, coordSystem);
			HalconAPI.Store(proc, 3, row);
			HalconAPI.Store(proc, 4, column);
			HalconAPI.Store(proc, 5, color);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(stringVal);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(color);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DispText(string stringVal, string coordSystem, int row, int column, string color, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2055);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, stringVal);
			HalconAPI.StoreS(proc, 2, coordSystem);
			HalconAPI.StoreI(proc, 3, row);
			HalconAPI.StoreI(proc, 4, column);
			HalconAPI.StoreS(proc, 5, color);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void FlushBuffer()
		{
			IntPtr proc = HalconAPI.PreCall(2070);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GetRgba(out HTuple red, out HTuple green, out HTuple blue, out HTuple alpha)
		{
			IntPtr proc = HalconAPI.PreCall(2073);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out red);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out green);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out blue);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out alpha);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public string SendMouseDoubleClickEvent(HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2088);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string SendMouseDoubleClickEvent(int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2088);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string SendMouseDownEvent(HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2089);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string SendMouseDownEvent(int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2089);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string SendMouseDragEvent(HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2090);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string SendMouseDragEvent(int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2090);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string SendMouseUpEvent(HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2091);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string SendMouseUpEvent(int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2091);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetContentUpdateCallback(IntPtr callbackFunction, IntPtr callbackContext)
		{
			IntPtr proc = HalconAPI.PreCall(2095);
			base.Store(proc, 0);
			HalconAPI.StoreIP(proc, 1, callbackFunction);
			HalconAPI.StoreIP(proc, 2, callbackContext);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetRgba(HTuple red, HTuple green, HTuple blue, HTuple alpha)
		{
			IntPtr proc = HalconAPI.PreCall(2096);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, red);
			HalconAPI.Store(proc, 2, green);
			HalconAPI.Store(proc, 3, blue);
			HalconAPI.Store(proc, 4, alpha);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(red);
			HalconAPI.UnpinTuple(green);
			HalconAPI.UnpinTuple(blue);
			HalconAPI.UnpinTuple(alpha);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetRgba(int red, int green, int blue, int alpha)
		{
			IntPtr proc = HalconAPI.PreCall(2096);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, red);
			HalconAPI.StoreI(proc, 2, green);
			HalconAPI.StoreI(proc, 3, blue);
			HalconAPI.StoreI(proc, 4, alpha);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1187);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
