using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace VisionConfig
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class HDevDisposeHelper : IDisposable
	{
		protected List<IDisposable> mDisposables;

		public HDevDisposeHelper()
		{
			this.mDisposables = new List<IDisposable>();
		}

		public IDisposable Add(IDisposable disposable)
		{
			for (int i = 0; i < this.mDisposables.Count; i++)
			{
				if (this.mDisposables[i] == disposable)
				{
					return disposable;
				}
			}
			this.mDisposables.Add(disposable);
			return disposable;
		}

		public IDisposable Take(IDisposable disposable)
		{
			for (int i = 0; i < this.mDisposables.Count; i++)
			{
				if (this.mDisposables[i] == disposable)
				{
					this.mDisposables.RemoveAt(i);
					return disposable;
				}
			}
			return disposable;
		}

		void IDisposable.Dispose()
		{
			for (int i = 0; i < this.mDisposables.Count; i++)
			{
				this.mDisposables[i].Dispose();
			}
			this.mDisposables.Clear();
		}

		public HObjectVector Add(HObjectVector disposable)
		{
			return (HObjectVector)this.Add((IDisposable)disposable);
		}

		public HObjectVector Take(HObjectVector disposable)
		{
			return (HObjectVector)this.Take((IDisposable)disposable);
		}

		public HObject Add(HObject disposable)
		{
			return (HObject)this.Add((IDisposable)disposable);
		}

		public HObject Take(HObject disposable)
		{
			return (HObject)this.Take((IDisposable)disposable);
		}
	}
}
