using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HMetrologyModel : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMetrologyModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMetrologyModel obj)
		{
			obj = new HMetrologyModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMetrologyModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HMetrologyModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HMetrologyModel(hTuple[i].IP);
			}
			return err;
		}

		public HMetrologyModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(798);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMetrologyModel()
		{
			IntPtr proc = HalconAPI.PreCall(820);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeMetrologyModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMetrologyModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeMetrologyModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeMetrologyModel().Serialize(stream);
		}

		public static HMetrologyModel Deserialize(Stream stream)
		{
			HMetrologyModel hMetrologyModel = new HMetrologyModel();
			hMetrologyModel.DeserializeMetrologyModel(HSerializedItem.Deserialize(stream));
			return hMetrologyModel;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HMetrologyModel Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeMetrologyModel();
			HMetrologyModel hMetrologyModel = new HMetrologyModel();
			hMetrologyModel.DeserializeMetrologyModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hMetrologyModel;
		}

		public HXLDCont GetMetrologyObjectModelContour(HTuple index, double resolution)
		{
			IntPtr proc = HalconAPI.PreCall(788);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.StoreD(proc, 2, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont GetMetrologyObjectModelContour(int index, double resolution)
		{
			IntPtr proc = HalconAPI.PreCall(788);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, index);
			HalconAPI.StoreD(proc, 2, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont GetMetrologyObjectResultContour(HTuple index, HTuple instance, double resolution)
		{
			IntPtr proc = HalconAPI.PreCall(789);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, instance);
			HalconAPI.StoreD(proc, 3, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(instance);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont GetMetrologyObjectResultContour(int index, int instance, double resolution)
		{
			IntPtr proc = HalconAPI.PreCall(789);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, index);
			HalconAPI.StoreI(proc, 2, instance);
			HalconAPI.StoreD(proc, 3, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AlignMetrologyModel(HTuple row, HTuple column, HTuple angle)
		{
			IntPtr proc = HalconAPI.PreCall(790);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, angle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(angle);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AlignMetrologyModel(double row, double column, double angle)
		{
			IntPtr proc = HalconAPI.PreCall(790);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, angle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int AddMetrologyObjectGeneric(HTuple shape, HTuple shapeParam, HTuple measureLength1, HTuple measureLength2, HTuple measureSigma, HTuple measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(791);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, shape);
			HalconAPI.Store(proc, 2, shapeParam);
			HalconAPI.Store(proc, 3, measureLength1);
			HalconAPI.Store(proc, 4, measureLength2);
			HalconAPI.Store(proc, 5, measureSigma);
			HalconAPI.Store(proc, 6, measureThreshold);
			HalconAPI.Store(proc, 7, genParamName);
			HalconAPI.Store(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(shape);
			HalconAPI.UnpinTuple(shapeParam);
			HalconAPI.UnpinTuple(measureLength1);
			HalconAPI.UnpinTuple(measureLength2);
			HalconAPI.UnpinTuple(measureSigma);
			HalconAPI.UnpinTuple(measureThreshold);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int AddMetrologyObjectGeneric(string shape, HTuple shapeParam, double measureLength1, double measureLength2, double measureSigma, double measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(791);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, shape);
			HalconAPI.Store(proc, 2, shapeParam);
			HalconAPI.StoreD(proc, 3, measureLength1);
			HalconAPI.StoreD(proc, 4, measureLength2);
			HalconAPI.StoreD(proc, 5, measureSigma);
			HalconAPI.StoreD(proc, 6, measureThreshold);
			HalconAPI.Store(proc, 7, genParamName);
			HalconAPI.Store(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(shapeParam);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetMetrologyModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(792);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetMetrologyModelParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(793);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMetrologyModelParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(793);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeMetrologyModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(794);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeMetrologyModel()
		{
			IntPtr proc = HalconAPI.PreCall(795);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void TransformMetrologyObject(HTuple index, HTuple row, HTuple column, HTuple phi, HTuple mode)
		{
			IntPtr proc = HalconAPI.PreCall(796);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.Store(proc, 4, phi);
			HalconAPI.Store(proc, 5, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(mode);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TransformMetrologyObject(string index, double row, double column, double phi, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(796);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.StoreD(proc, 2, row);
			HalconAPI.StoreD(proc, 3, column);
			HalconAPI.StoreD(proc, 4, phi);
			HalconAPI.StoreS(proc, 5, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteMetrologyModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(797);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadMetrologyModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(798);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public int CopyMetrologyModel(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(799);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int CopyMetrologyModel(string index)
		{
			IntPtr proc = HalconAPI.PreCall(799);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple CopyMetrologyObject(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(800);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int CopyMetrologyObject(string index)
		{
			IntPtr proc = HalconAPI.PreCall(800);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetMetrologyObjectNumInstances(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(801);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double GetMetrologyObjectNumInstances(int index)
		{
			IntPtr proc = HalconAPI.PreCall(801);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetMetrologyObjectResult(HTuple index, HTuple instance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(802);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, instance);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(instance);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetMetrologyObjectResult(int index, int instance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(802);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, index);
			HalconAPI.StoreI(proc, 2, instance);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont GetMetrologyObjectMeasures(HTuple index, string transition, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(803);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.StoreS(proc, 2, transition);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont GetMetrologyObjectMeasures(string index, string transition, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(803);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.StoreS(proc, 2, transition);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ApplyMetrologyModel(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(804);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HTuple GetMetrologyObjectIndices()
		{
			IntPtr proc = HalconAPI.PreCall(805);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ResetMetrologyObjectFuzzyParam(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(806);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ResetMetrologyObjectFuzzyParam(string index)
		{
			IntPtr proc = HalconAPI.PreCall(806);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ResetMetrologyObjectParam(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(807);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ResetMetrologyObjectParam(string index)
		{
			IntPtr proc = HalconAPI.PreCall(807);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetMetrologyObjectFuzzyParam(HTuple index, HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(808);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetMetrologyObjectFuzzyParam(string index, HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(808);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetMetrologyObjectParam(HTuple index, HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(809);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetMetrologyObjectParam(string index, HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(809);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetMetrologyObjectFuzzyParam(HTuple index, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(810);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMetrologyObjectFuzzyParam(string index, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(810);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMetrologyObjectParam(HTuple index, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(811);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMetrologyObjectParam(string index, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(811);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int AddMetrologyObjectRectangle2Measure(HTuple row, HTuple column, HTuple phi, HTuple length1, HTuple length2, HTuple measureLength1, HTuple measureLength2, HTuple measureSigma, HTuple measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(812);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, phi);
			HalconAPI.Store(proc, 4, length1);
			HalconAPI.Store(proc, 5, length2);
			HalconAPI.Store(proc, 6, measureLength1);
			HalconAPI.Store(proc, 7, measureLength2);
			HalconAPI.Store(proc, 8, measureSigma);
			HalconAPI.Store(proc, 9, measureThreshold);
			HalconAPI.Store(proc, 10, genParamName);
			HalconAPI.Store(proc, 11, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(length1);
			HalconAPI.UnpinTuple(length2);
			HalconAPI.UnpinTuple(measureLength1);
			HalconAPI.UnpinTuple(measureLength2);
			HalconAPI.UnpinTuple(measureSigma);
			HalconAPI.UnpinTuple(measureThreshold);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int AddMetrologyObjectRectangle2Measure(double row, double column, double phi, double length1, double length2, double measureLength1, double measureLength2, double measureSigma, double measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(812);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreD(proc, 4, length1);
			HalconAPI.StoreD(proc, 5, length2);
			HalconAPI.StoreD(proc, 6, measureLength1);
			HalconAPI.StoreD(proc, 7, measureLength2);
			HalconAPI.StoreD(proc, 8, measureSigma);
			HalconAPI.StoreD(proc, 9, measureThreshold);
			HalconAPI.Store(proc, 10, genParamName);
			HalconAPI.Store(proc, 11, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int AddMetrologyObjectLineMeasure(HTuple rowBegin, HTuple columnBegin, HTuple rowEnd, HTuple columnEnd, HTuple measureLength1, HTuple measureLength2, HTuple measureSigma, HTuple measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(813);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, rowBegin);
			HalconAPI.Store(proc, 2, columnBegin);
			HalconAPI.Store(proc, 3, rowEnd);
			HalconAPI.Store(proc, 4, columnEnd);
			HalconAPI.Store(proc, 5, measureLength1);
			HalconAPI.Store(proc, 6, measureLength2);
			HalconAPI.Store(proc, 7, measureSigma);
			HalconAPI.Store(proc, 8, measureThreshold);
			HalconAPI.Store(proc, 9, genParamName);
			HalconAPI.Store(proc, 10, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowBegin);
			HalconAPI.UnpinTuple(columnBegin);
			HalconAPI.UnpinTuple(rowEnd);
			HalconAPI.UnpinTuple(columnEnd);
			HalconAPI.UnpinTuple(measureLength1);
			HalconAPI.UnpinTuple(measureLength2);
			HalconAPI.UnpinTuple(measureSigma);
			HalconAPI.UnpinTuple(measureThreshold);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int AddMetrologyObjectLineMeasure(double rowBegin, double columnBegin, double rowEnd, double columnEnd, double measureLength1, double measureLength2, double measureSigma, double measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(813);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, rowBegin);
			HalconAPI.StoreD(proc, 2, columnBegin);
			HalconAPI.StoreD(proc, 3, rowEnd);
			HalconAPI.StoreD(proc, 4, columnEnd);
			HalconAPI.StoreD(proc, 5, measureLength1);
			HalconAPI.StoreD(proc, 6, measureLength2);
			HalconAPI.StoreD(proc, 7, measureSigma);
			HalconAPI.StoreD(proc, 8, measureThreshold);
			HalconAPI.Store(proc, 9, genParamName);
			HalconAPI.Store(proc, 10, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int AddMetrologyObjectEllipseMeasure(HTuple row, HTuple column, HTuple phi, HTuple radius1, HTuple radius2, HTuple measureLength1, HTuple measureLength2, HTuple measureSigma, HTuple measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(814);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, phi);
			HalconAPI.Store(proc, 4, radius1);
			HalconAPI.Store(proc, 5, radius2);
			HalconAPI.Store(proc, 6, measureLength1);
			HalconAPI.Store(proc, 7, measureLength2);
			HalconAPI.Store(proc, 8, measureSigma);
			HalconAPI.Store(proc, 9, measureThreshold);
			HalconAPI.Store(proc, 10, genParamName);
			HalconAPI.Store(proc, 11, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(radius1);
			HalconAPI.UnpinTuple(radius2);
			HalconAPI.UnpinTuple(measureLength1);
			HalconAPI.UnpinTuple(measureLength2);
			HalconAPI.UnpinTuple(measureSigma);
			HalconAPI.UnpinTuple(measureThreshold);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int AddMetrologyObjectEllipseMeasure(double row, double column, double phi, double radius1, double radius2, double measureLength1, double measureLength2, double measureSigma, double measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(814);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreD(proc, 4, radius1);
			HalconAPI.StoreD(proc, 5, radius2);
			HalconAPI.StoreD(proc, 6, measureLength1);
			HalconAPI.StoreD(proc, 7, measureLength2);
			HalconAPI.StoreD(proc, 8, measureSigma);
			HalconAPI.StoreD(proc, 9, measureThreshold);
			HalconAPI.Store(proc, 10, genParamName);
			HalconAPI.Store(proc, 11, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int AddMetrologyObjectCircleMeasure(HTuple row, HTuple column, HTuple radius, HTuple measureLength1, HTuple measureLength2, HTuple measureSigma, HTuple measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(815);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, radius);
			HalconAPI.Store(proc, 4, measureLength1);
			HalconAPI.Store(proc, 5, measureLength2);
			HalconAPI.Store(proc, 6, measureSigma);
			HalconAPI.Store(proc, 7, measureThreshold);
			HalconAPI.Store(proc, 8, genParamName);
			HalconAPI.Store(proc, 9, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(measureLength1);
			HalconAPI.UnpinTuple(measureLength2);
			HalconAPI.UnpinTuple(measureSigma);
			HalconAPI.UnpinTuple(measureThreshold);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int AddMetrologyObjectCircleMeasure(double row, double column, double radius, double measureLength1, double measureLength2, double measureSigma, double measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(815);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, radius);
			HalconAPI.StoreD(proc, 4, measureLength1);
			HalconAPI.StoreD(proc, 5, measureLength2);
			HalconAPI.StoreD(proc, 6, measureSigma);
			HalconAPI.StoreD(proc, 7, measureThreshold);
			HalconAPI.Store(proc, 8, genParamName);
			HalconAPI.Store(proc, 9, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ClearMetrologyObject(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(818);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ClearMetrologyObject(string index)
		{
			IntPtr proc = HalconAPI.PreCall(818);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMetrologyModelImageSize(int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(819);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateMetrologyModel()
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(820);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(817);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
