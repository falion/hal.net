using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HStereoModel : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HStereoModel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HStereoModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HStereoModel obj)
		{
			obj = new HStereoModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HStereoModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HStereoModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HStereoModel(hTuple[i].IP);
			}
			return err;
		}

		public HStereoModel(HCameraSetupModel cameraSetupModelID, string method, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(527);
			HalconAPI.Store(proc, 0, cameraSetupModelID);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(cameraSetupModelID);
		}

		public HStereoModel(HCameraSetupModel cameraSetupModelID, string method, string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(527);
			HalconAPI.Store(proc, 0, cameraSetupModelID);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(cameraSetupModelID);
		}

		public void ReconstructPointsStereo(HTuple row, HTuple column, HTuple covIP, int cameraIdx, int pointIdx, out HTuple x, out HTuple y, out HTuple z, out HTuple covWP, out HTuple pointIdxOut)
		{
			IntPtr proc = HalconAPI.PreCall(520);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, covIP);
			HalconAPI.StoreI(proc, 4, cameraIdx);
			HalconAPI.StoreI(proc, 5, pointIdx);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(covIP);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out covWP);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out pointIdxOut);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ReconstructPointsStereo(double row, double column, HTuple covIP, int cameraIdx, int pointIdx, out double x, out double y, out double z, out double covWP, out int pointIdxOut)
		{
			IntPtr proc = HalconAPI.PreCall(520);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.Store(proc, 3, covIP);
			HalconAPI.StoreI(proc, 4, cameraIdx);
			HalconAPI.StoreI(proc, 5, pointIdx);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(covIP);
			err = HalconAPI.LoadD(proc, 0, err, out x);
			err = HalconAPI.LoadD(proc, 1, err, out y);
			err = HalconAPI.LoadD(proc, 2, err, out z);
			err = HalconAPI.LoadD(proc, 3, err, out covWP);
			err = HalconAPI.LoadI(proc, 4, err, out pointIdxOut);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HObjectModel3D ReconstructSurfaceStereo(HImage images)
		{
			IntPtr proc = HalconAPI.PreCall(521);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, images);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(images);
			return result;
		}

		public HObject GetStereoModelObject(HTuple pairIndex, string objectName)
		{
			IntPtr proc = HalconAPI.PreCall(522);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, pairIndex);
			HalconAPI.StoreS(proc, 2, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pairIndex);
			HObject result = null;
			err = HObject.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HObject GetStereoModelObject(int pairIndex, string objectName)
		{
			IntPtr proc = HalconAPI.PreCall(522);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, pairIndex);
			HalconAPI.StoreS(proc, 2, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HObject result = null;
			err = HObject.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetStereoModelImagePairs(out HTuple to)
		{
			IntPtr proc = HalconAPI.PreCall(523);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out to);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetStereoModelImagePairs(HTuple from, HTuple to)
		{
			IntPtr proc = HalconAPI.PreCall(524);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, from);
			HalconAPI.Store(proc, 2, to);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(from);
			HalconAPI.UnpinTuple(to);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetStereoModelParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(525);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetStereoModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(525);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetStereoModelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(526);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetStereoModelParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(526);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateStereoModel(HCameraSetupModel cameraSetupModelID, string method, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(527);
			HalconAPI.Store(proc, 0, cameraSetupModelID);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(cameraSetupModelID);
		}

		public void CreateStereoModel(HCameraSetupModel cameraSetupModelID, string method, string genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(527);
			HalconAPI.Store(proc, 0, cameraSetupModelID);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(cameraSetupModelID);
		}

		public HObjectModel3D GetStereoModelObjectModel3d(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2074);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HObjectModel3D GetStereoModelObjectModel3d(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2074);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(519);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
