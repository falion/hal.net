using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace VisionConfig
{
	internal abstract class HTupleImplementation
	{
		public const int INTEGER = 1;

		public const int DOUBLE = 2;

		public const int STRING = 4;

		public const int ANY_ELEM = 7;

		public const int MIXED = 8;

		public const int ANY_TUPLE = 15;

		public const int LONG = 129;

		public const int FLOAT = 32898;

		public const int INTPTR = 32900;

		public const int BAN_IN_MIXED = 32768;

		protected Array data;

		protected int iLength;

		internal GCHandle pinHandle;

		internal int pinCount;

		protected Type typeI = typeof(int);

		protected Type typeL = typeof(long);

		protected Type typeD = typeof(double);

		protected Type typeS = typeof(string);

		protected Type typeO = typeof(object);

		protected Type typeF = typeof(float);

		protected Type typeIP = typeof(IntPtr);

		protected int Capacity
		{
			get
			{
				return this.data.Length;
			}
		}

		public int Length
		{
			get
			{
				return this.iLength;
			}
		}

		public virtual HTupleType Type
		{
			get
			{
				throw new HTupleAccessException(this);
			}
		}

		public virtual int[] IArr
		{
			get
			{
				throw new HTupleAccessException(this);
			}
			set
			{
				throw new HTupleAccessException(this);
			}
		}

		public virtual long[] LArr
		{
			get
			{
				throw new HTupleAccessException(this);
			}
			set
			{
				throw new HTupleAccessException(this);
			}
		}

		public virtual double[] DArr
		{
			get
			{
				throw new HTupleAccessException(this);
			}
			set
			{
				throw new HTupleAccessException(this);
			}
		}

		public virtual string[] SArr
		{
			get
			{
				throw new HTupleAccessException(this);
			}
			set
			{
				throw new HTupleAccessException(this);
			}
		}

		public virtual object[] OArr
		{
			get
			{
				throw new HTupleAccessException(this);
			}
			set
			{
				throw new HTupleAccessException(this);
			}
		}

		public static int GetObjectType(object o)
		{
			if (o is int)
			{
				return 1;
			}
			if (o is long)
			{
				return 129;
			}
			if (o is double)
			{
				return 2;
			}
			if (o is float)
			{
				return 32898;
			}
			if (o is string)
			{
				return 4;
			}
			if (o is IntPtr)
			{
				return 32900;
			}
			return 15;
		}

		public static int GetObjectsType(object[] o)
		{
			if (o == null)
			{
				return 15;
			}
			int num = 15;
			int num2 = 15;
			for (int i = 0; i < o.Length; i++)
			{
				if (o[i] is int)
				{
					num = 1;
				}
				if (o[i] is long)
				{
					num = 129;
				}
				if (o[i] is double)
				{
					num = 2;
				}
				if (o[i] is float)
				{
					num = 32898;
				}
				if (o[i] is string)
				{
					num = 4;
				}
				if (o[i] is IntPtr)
				{
					num = 32900;
				}
				if (i == 0)
				{
					num2 = num;
				}
				else if (num != num2)
				{
					return 8;
				}
			}
			return num2;
		}

		internal virtual void PinTuple()
		{
		}

		internal void UnpinTuple()
		{
			Monitor.Enter(this);
			if (this.pinCount > 0)
			{
				this.pinCount--;
				if (this.pinCount == 0)
				{
					this.pinHandle.Free();
				}
			}
			Monitor.Exit(this);
		}

		protected abstract Array CreateArray(int size);

		protected void SetArray(Array source, bool copy)
		{
			if (source == null)
			{
				source = this.CreateArray(0);
			}
			if (copy)
			{
				this.data = this.CreateArray(source.Length);
				Array.Copy(source, this.data, source.Length);
			}
			else
			{
				this.data = source;
			}
			this.iLength = this.data.Length;
			this.NotifyArrayUpdate();
		}

		protected virtual void NotifyArrayUpdate()
		{
		}

		public virtual void AssertSize(int index)
		{
			if (index >= this.iLength)
			{
				if (index >= this.data.Length)
				{
					Array sourceArray = this.data;
					this.data = this.CreateArray(Math.Max(10, 2 * index));
					Array.Copy(sourceArray, this.data, this.iLength);
					this.NotifyArrayUpdate();
				}
				this.iLength = index + 1;
			}
		}

		public virtual void AssertSize(int[] indices)
		{
			int num;
			if (indices.Length == 0)
			{
				num = 0;
			}
			else
			{
				num = indices[0];
				foreach (int num2 in indices)
				{
					if (num2 > num)
					{
						num = num2;
					}
				}
			}
			this.AssertSize(num);
		}

		public virtual HTupleElements GetElement(int index, HTuple parent)
		{
			throw new HTupleAccessException(this);
		}

		public virtual HTupleElements GetElements(int[] indices, HTuple parent)
		{
			if (indices != null && indices.Length != 0)
			{
				throw new HTupleAccessException(this);
			}
			return new HTupleElements();
		}

		public virtual void SetElements(int[] indices, HTupleElements elements)
		{
			if (indices == null)
			{
				return;
			}
			if (indices.Length == 0)
			{
				return;
			}
			throw new HTupleAccessException(this);
		}

		protected Array ToArray(Type t)
		{
			Array array = Array.CreateInstance(t, this.iLength);
			Array.Copy(this.data, array, this.iLength);
			return array;
		}

		public virtual int[] ToIArr()
		{
			throw new HTupleAccessException(this, "Cannot convert to int array");
		}

		public virtual long[] ToLArr()
		{
			throw new HTupleAccessException(this, "Cannot convert to long array");
		}

		public virtual double[] ToDArr()
		{
			throw new HTupleAccessException(this, "Cannot convert to double array");
		}

		public virtual string[] ToSArr()
		{
			string[] array = new string[this.iLength];
			for (int i = 0; i < this.iLength; i++)
			{
				array[i] = this.data.GetValue(i).ToString();
			}
			return array;
		}

		public virtual object[] ToOArr()
		{
			return (object[])this.ToArray(this.typeO);
		}

		public virtual float[] ToFArr()
		{
			throw new HTupleAccessException(this, "Cannot convert to float array");
		}

		public virtual IntPtr[] ToIPArr()
		{
			throw new HTupleAccessException(this, "Values in tuple do not represent pointers on this platform");
		}

		public virtual void Store(IntPtr proc, int parIndex)
		{
			IntPtr tuple = default(IntPtr);
			HalconAPI.HCkP(proc, HalconAPI.CreateInputTuple(proc, parIndex, this.iLength, out tuple));
			this.StoreData(proc, tuple);
		}

		protected abstract void StoreData(IntPtr proc, IntPtr tuple);

		public static int Load(IntPtr proc, int parIndex, HTupleType type, out HTupleImplementation data)
		{
			IntPtr tuple = default(IntPtr);
			HalconAPI.GetOutputTuple(proc, parIndex, out tuple);
			return HTupleImplementation.LoadData(tuple, type, out data);
		}

		public static int LoadData(IntPtr tuple, HTupleType type, out HTupleImplementation data)
		{
			int result = 2;
			if (tuple == IntPtr.Zero)
			{
				data = HTupleVoid.EMPTY;
				return result;
			}
			int num = 0;
			HalconAPI.GetTupleTypeScanElem(tuple, out num);
			switch (num)
			{
			case 15:
				data = HTupleVoid.EMPTY;
				type = HTupleType.EMPTY;
				break;
			case 1:
				if (HalconAPI.isPlatform64)
				{
					HTupleInt64 hTupleInt = null;
					result = HTupleInt64.Load(tuple, out hTupleInt);
					data = hTupleInt;
				}
				else
				{
					HTupleInt32 hTupleInt2 = null;
					result = HTupleInt32.Load(tuple, out hTupleInt2);
					data = hTupleInt2;
				}
				type = HTupleType.INTEGER;
				break;
			case 2:
			{
				HTupleDouble hTupleDouble = null;
				result = HTupleDouble.Load(tuple, out hTupleDouble);
				data = hTupleDouble;
				type = HTupleType.DOUBLE;
				break;
			}
			case 4:
			{
				HTupleString hTupleString = null;
				result = HTupleString.Load(tuple, out hTupleString);
				data = hTupleString;
				type = HTupleType.STRING;
				break;
			}
			case 7:
			{
				HTupleMixed hTupleMixed = null;
				result = HTupleMixed.Load(tuple, out hTupleMixed);
				data = hTupleMixed;
				type = HTupleType.MIXED;
				break;
			}
			default:
				data = HTupleVoid.EMPTY;
				result = 7002;
				break;
			}
			return result;
		}
	}
}
