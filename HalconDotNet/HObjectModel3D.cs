using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HObjectModel3D : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HObjectModel3D(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HObjectModel3D obj)
		{
			obj = new HObjectModel3D(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HObjectModel3D[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HObjectModel3D[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HObjectModel3D(hTuple[i].IP);
			}
			return err;
		}

		public HObjectModel3D()
		{
			IntPtr proc = HalconAPI.PreCall(1065);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HObjectModel3D(HTuple x, HTuple y, HTuple z)
		{
			IntPtr proc = HalconAPI.PreCall(1069);
			HalconAPI.Store(proc, 0, x);
			HalconAPI.Store(proc, 1, y);
			HalconAPI.Store(proc, 2, z);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(x);
			HalconAPI.UnpinTuple(y);
			HalconAPI.UnpinTuple(z);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HObjectModel3D(double x, double y, double z)
		{
			IntPtr proc = HalconAPI.PreCall(1069);
			HalconAPI.StoreD(proc, 0, x);
			HalconAPI.StoreD(proc, 1, y);
			HalconAPI.StoreD(proc, 2, z);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HObjectModel3D(HImage x, HImage y, HImage z)
		{
			IntPtr proc = HalconAPI.PreCall(1093);
			HalconAPI.Store(proc, 1, x);
			HalconAPI.Store(proc, 2, y);
			HalconAPI.Store(proc, 3, z);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(x);
			GC.KeepAlive(y);
			GC.KeepAlive(z);
		}

		public HObjectModel3D(string fileName, HTuple scale, HTuple genParamName, HTuple genParamValue, out HTuple status)
		{
			IntPtr proc = HalconAPI.PreCall(1104);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, scale);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(scale);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out status);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HObjectModel3D(string fileName, string scale, string genParamName, string genParamValue, out string status)
		{
			IntPtr proc = HalconAPI.PreCall(1104);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, scale);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			err = HalconAPI.LoadS(proc, 1, err, out status);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeObjectModel3d();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HObjectModel3D(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeObjectModel3d(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeObjectModel3d().Serialize(stream);
		}

		public static HObjectModel3D Deserialize(Stream stream)
		{
			HObjectModel3D hObjectModel3D = new HObjectModel3D();
			hObjectModel3D.DeserializeObjectModel3d(HSerializedItem.Deserialize(stream));
			return hObjectModel3D;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HObjectModel3D Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeObjectModel3d();
			HObjectModel3D hObjectModel3D = new HObjectModel3D();
			hObjectModel3D.DeserializeObjectModel3d(hSerializedItem);
			hSerializedItem.Dispose();
			return hObjectModel3D;
		}

		public void GetSheetOfLightResultObjectModel3d(HSheetOfLightModel sheetOfLightModelID)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(380);
			HalconAPI.Store(proc, 0, sheetOfLightModelID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sheetOfLightModelID);
		}

		public static HObjectModel3D[] FitPrimitivesObjectModel3d(HObjectModel3D[] objectModel3D, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(411);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D FitPrimitivesObjectModel3d(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(411);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] SegmentObjectModel3d(HObjectModel3D[] objectModel3D, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(412);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D SegmentObjectModel3d(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(412);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] SurfaceNormalsObjectModel3d(HObjectModel3D[] objectModel3D, string method, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(515);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D SurfaceNormalsObjectModel3d(string method, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(515);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] SmoothObjectModel3d(HObjectModel3D[] objectModel3D, string method, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(516);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D SmoothObjectModel3d(string method, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(516);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] TriangulateObjectModel3d(HObjectModel3D[] objectModel3D, string method, HTuple genParamName, HTuple genParamValue, out HTuple information)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(517);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out information);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D TriangulateObjectModel3d(string method, HTuple genParamName, HTuple genParamValue, out int information)
		{
			IntPtr proc = HalconAPI.PreCall(517);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out information);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReconstructSurfaceStereo(HImage images, HStereoModel stereoModelID)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(521);
			HalconAPI.Store(proc, 1, images);
			HalconAPI.Store(proc, 0, stereoModelID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(images);
			GC.KeepAlive(stereoModelID);
		}

		public HTuple RefineDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, double relSamplingDistance, HObjectModel3D initialDeformationObjectModel3D, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1026);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, initialDeformationObjectModel3D);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(initialDeformationObjectModel3D);
			return result;
		}

		public double RefineDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, double relSamplingDistance, HObjectModel3D initialDeformationObjectModel3D, string genParamName, string genParamValue, out HDeformableSurfaceMatchingResult deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1026);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, initialDeformationObjectModel3D);
			HalconAPI.StoreS(proc, 4, genParamName);
			HalconAPI.StoreS(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(initialDeformationObjectModel3D);
			return result;
		}

		public HTuple FindDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, double relSamplingDistance, HTuple minScore, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1027);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(deformableSurfaceModel);
			return result;
		}

		public double FindDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, double relSamplingDistance, double minScore, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1027);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(deformableSurfaceModel);
			return result;
		}

		public static void AddDeformableSurfaceModelSample(HDeformableSurfaceModel deformableSurfaceModel, HObjectModel3D[] objectModel3D)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1030);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.Store(proc, 1, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(objectModel3D);
		}

		public void AddDeformableSurfaceModelSample(HDeformableSurfaceModel deformableSurfaceModel)
		{
			IntPtr proc = HalconAPI.PreCall(1030);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(deformableSurfaceModel);
		}

		public HDeformableSurfaceModel CreateDeformableSurfaceModel(double relSamplingDistance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1031);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableSurfaceModel result = null;
			err = HDeformableSurfaceModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableSurfaceModel CreateDeformableSurfaceModel(double relSamplingDistance, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1031);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HDeformableSurfaceModel result = null;
			err = HDeformableSurfaceModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HPose[] RefineSurfaceModelPose(HSurfaceModel surfaceModelID, HPose[] initialPose, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			HTuple hTuple = HData.ConcatArray(initialPose);
			IntPtr proc = HalconAPI.PreCall(1041);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 2, hTuple);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(surfaceModelID);
			return result;
		}

		public HPose RefineSurfaceModelPose(HSurfaceModel surfaceModelID, HPose initialPose, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(1041);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 2, initialPose);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(initialPose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(surfaceModelID);
			return result;
		}

		public HPose[] FindSurfaceModel(HSurfaceModel surfaceModelID, double relSamplingDistance, double keyPointFraction, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(1042);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.Store(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(surfaceModelID);
			return result;
		}

		public HPose FindSurfaceModel(HSurfaceModel surfaceModelID, double relSamplingDistance, double keyPointFraction, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(1042);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.StoreD(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(surfaceModelID);
			return result;
		}

		public HSurfaceModel CreateSurfaceModel(double relSamplingDistance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1044);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HSurfaceModel result = null;
			err = HSurfaceModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HSurfaceModel CreateSurfaceModel(double relSamplingDistance, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1044);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSurfaceModel result = null;
			err = HSurfaceModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] SimplifyObjectModel3d(HObjectModel3D[] objectModel3D, string method, HTuple amount, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1060);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, amount);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(amount);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D SimplifyObjectModel3d(string method, double amount, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1060);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.StoreD(proc, 2, amount);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DistanceObjectModel3d(HObjectModel3D objectModel3DTo, HPose pose, HTuple maxDistance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1061);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3DTo);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.Store(proc, 3, maxDistance);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.UnpinTuple(maxDistance);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3DTo);
		}

		public void DistanceObjectModel3d(HObjectModel3D objectModel3DTo, HPose pose, double maxDistance, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1061);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3DTo);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.StoreD(proc, 3, maxDistance);
			HalconAPI.StoreS(proc, 4, genParamName);
			HalconAPI.StoreS(proc, 5, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3DTo);
		}

		public static HObjectModel3D UnionObjectModel3d(HObjectModel3D[] objectModels3D, string method)
		{
			HTuple hTuple = HTool.ConcatArray(objectModels3D);
			IntPtr proc = HalconAPI.PreCall(1062);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModels3D);
			return result;
		}

		public HObjectModel3D UnionObjectModel3d(string method)
		{
			IntPtr proc = HalconAPI.PreCall(1062);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetObjectModel3dAttribMod(HTuple attribName, string attachExtAttribTo, HTuple attribValues)
		{
			IntPtr proc = HalconAPI.PreCall(1063);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, attribName);
			HalconAPI.StoreS(proc, 2, attachExtAttribTo);
			HalconAPI.Store(proc, 3, attribValues);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValues);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetObjectModel3dAttribMod(string attribName, string attachExtAttribTo, double attribValues)
		{
			IntPtr proc = HalconAPI.PreCall(1063);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, attribName);
			HalconAPI.StoreS(proc, 2, attachExtAttribTo);
			HalconAPI.StoreD(proc, 3, attribValues);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HObjectModel3D SetObjectModel3dAttrib(HTuple attribName, string attachExtAttribTo, HTuple attribValues)
		{
			IntPtr proc = HalconAPI.PreCall(1064);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, attribName);
			HalconAPI.StoreS(proc, 2, attachExtAttribTo);
			HalconAPI.Store(proc, 3, attribValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValues);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HObjectModel3D SetObjectModel3dAttrib(string attribName, string attachExtAttribTo, double attribValues)
		{
			IntPtr proc = HalconAPI.PreCall(1064);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, attribName);
			HalconAPI.StoreS(proc, 2, attachExtAttribTo);
			HalconAPI.StoreD(proc, 3, attribValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GenEmptyObjectModel3d()
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1065);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HObjectModel3D[] SampleObjectModel3d(HObjectModel3D[] objectModel3D, string method, HTuple sampleDistance, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1066);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, sampleDistance);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(sampleDistance);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D SampleObjectModel3d(string method, double sampleDistance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1066);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.StoreD(proc, 2, sampleDistance);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HHomMat3D[] RegisterObjectModel3dGlobal(HObjectModel3D[] objectModels3D, HHomMat3D[] homMats3D, HTuple from, HTuple to, HTuple genParamName, HTuple genParamValue, out HTuple scores)
		{
			HTuple hTuple = HTool.ConcatArray(objectModels3D);
			HTuple hTuple2 = HData.ConcatArray(homMats3D);
			IntPtr proc = HalconAPI.PreCall(1067);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.Store(proc, 2, from);
			HalconAPI.Store(proc, 3, to);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(from);
			HalconAPI.UnpinTuple(to);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out scores);
			HalconAPI.PostCall(proc, err);
			HHomMat3D[] result = HHomMat3D.SplitArray(data);
			GC.KeepAlive(objectModels3D);
			return result;
		}

		public HHomMat3D[] RegisterObjectModel3dGlobal(HHomMat3D[] homMats3D, string from, int to, HTuple genParamName, HTuple genParamValue, out HTuple scores)
		{
			HTuple hTuple = HData.ConcatArray(homMats3D);
			IntPtr proc = HalconAPI.PreCall(1067);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, hTuple);
			HalconAPI.StoreS(proc, 2, from);
			HalconAPI.StoreI(proc, 3, to);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out scores);
			HalconAPI.PostCall(proc, err);
			HHomMat3D[] result = HHomMat3D.SplitArray(data);
			GC.KeepAlive(this);
			return result;
		}

		public HPose RegisterObjectModel3dPair(HObjectModel3D objectModel3D2, string method, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1068);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D2);
			HalconAPI.StoreS(proc, 2, method);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D2);
			return result;
		}

		public HPose RegisterObjectModel3dPair(HObjectModel3D objectModel3D2, string method, string genParamName, double genParamValue, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1068);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D2);
			HalconAPI.StoreS(proc, 2, method);
			HalconAPI.StoreS(proc, 3, genParamName);
			HalconAPI.StoreD(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D2);
			return result;
		}

		public void GenObjectModel3dFromPoints(HTuple x, HTuple y, HTuple z)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1069);
			HalconAPI.Store(proc, 0, x);
			HalconAPI.Store(proc, 1, y);
			HalconAPI.Store(proc, 2, z);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(x);
			HalconAPI.UnpinTuple(y);
			HalconAPI.UnpinTuple(z);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenObjectModel3dFromPoints(double x, double y, double z)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1069);
			HalconAPI.StoreD(proc, 0, x);
			HalconAPI.StoreD(proc, 1, y);
			HalconAPI.StoreD(proc, 2, z);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HObjectModel3D[] GenBoxObjectModel3d(HPose[] pose, HTuple lengthX, HTuple lengthY, HTuple lengthZ)
		{
			HTuple hTuple = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1070);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, lengthX);
			HalconAPI.Store(proc, 2, lengthY);
			HalconAPI.Store(proc, 3, lengthZ);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(lengthX);
			HalconAPI.UnpinTuple(lengthY);
			HalconAPI.UnpinTuple(lengthZ);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public void GenBoxObjectModel3d(HPose pose, double lengthX, double lengthY, double lengthZ)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1070);
			HalconAPI.Store(proc, 0, pose);
			HalconAPI.StoreD(proc, 1, lengthX);
			HalconAPI.StoreD(proc, 2, lengthY);
			HalconAPI.StoreD(proc, 3, lengthZ);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenPlaneObjectModel3d(HPose pose, HTuple XExtent, HTuple YExtent)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1071);
			HalconAPI.Store(proc, 0, pose);
			HalconAPI.Store(proc, 1, XExtent);
			HalconAPI.Store(proc, 2, YExtent);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.UnpinTuple(XExtent);
			HalconAPI.UnpinTuple(YExtent);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenPlaneObjectModel3d(HPose pose, double XExtent, double YExtent)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1071);
			HalconAPI.Store(proc, 0, pose);
			HalconAPI.StoreD(proc, 1, XExtent);
			HalconAPI.StoreD(proc, 2, YExtent);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HObjectModel3D[] GenSphereObjectModel3dCenter(HTuple x, HTuple y, HTuple z, HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(1072);
			HalconAPI.Store(proc, 0, x);
			HalconAPI.Store(proc, 1, y);
			HalconAPI.Store(proc, 2, z);
			HalconAPI.Store(proc, 3, radius);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(x);
			HalconAPI.UnpinTuple(y);
			HalconAPI.UnpinTuple(z);
			HalconAPI.UnpinTuple(radius);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public void GenSphereObjectModel3dCenter(double x, double y, double z, double radius)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1072);
			HalconAPI.StoreD(proc, 0, x);
			HalconAPI.StoreD(proc, 1, y);
			HalconAPI.StoreD(proc, 2, z);
			HalconAPI.StoreD(proc, 3, radius);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HObjectModel3D[] GenSphereObjectModel3d(HPose[] pose, HTuple radius)
		{
			HTuple hTuple = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1073);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, radius);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(radius);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public void GenSphereObjectModel3d(HPose pose, double radius)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1073);
			HalconAPI.Store(proc, 0, pose);
			HalconAPI.StoreD(proc, 1, radius);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HObjectModel3D[] GenCylinderObjectModel3d(HPose[] pose, HTuple radius, HTuple minExtent, HTuple maxExtent)
		{
			HTuple hTuple = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1074);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, radius);
			HalconAPI.Store(proc, 2, minExtent);
			HalconAPI.Store(proc, 3, maxExtent);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(minExtent);
			HalconAPI.UnpinTuple(maxExtent);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public void GenCylinderObjectModel3d(HPose pose, double radius, double minExtent, double maxExtent)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1074);
			HalconAPI.Store(proc, 0, pose);
			HalconAPI.StoreD(proc, 1, radius);
			HalconAPI.StoreD(proc, 2, minExtent);
			HalconAPI.StoreD(proc, 3, maxExtent);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HPose[] SmallestBoundingBoxObjectModel3d(HObjectModel3D[] objectModel3D, string type, out HTuple length1, out HTuple length2, out HTuple length3)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1075);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, type);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out length1);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out length2);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out length3);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose SmallestBoundingBoxObjectModel3d(string type, out double length1, out double length2, out double length3)
		{
			IntPtr proc = HalconAPI.PreCall(1075);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, type);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out length1);
			err = HalconAPI.LoadD(proc, 2, err, out length2);
			err = HalconAPI.LoadD(proc, 3, err, out length3);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple SmallestSphereObjectModel3d(HObjectModel3D[] objectModel3D, out HTuple radius)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1076);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out radius);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HTuple SmallestSphereObjectModel3d(out double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1076);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out radius);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] IntersectPlaneObjectModel3d(HObjectModel3D[] objectModel3D, HPose[] plane)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(plane);
			IntPtr proc = HalconAPI.PreCall(1077);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D IntersectPlaneObjectModel3d(HPose plane)
		{
			IntPtr proc = HalconAPI.PreCall(1077);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, plane);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(plane);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] ConvexHullObjectModel3d(HObjectModel3D[] objectModel3D)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1078);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D ConvexHullObjectModel3d()
		{
			IntPtr proc = HalconAPI.PreCall(1078);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] SelectObjectModel3d(HObjectModel3D[] objectModel3D, HTuple feature, string operation, HTuple minValue, HTuple maxValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1079);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, feature);
			HalconAPI.StoreS(proc, 2, operation);
			HalconAPI.Store(proc, 3, minValue);
			HalconAPI.Store(proc, 4, maxValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(feature);
			HalconAPI.UnpinTuple(minValue);
			HalconAPI.UnpinTuple(maxValue);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D SelectObjectModel3d(string feature, string operation, double minValue, double maxValue)
		{
			IntPtr proc = HalconAPI.PreCall(1079);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, feature);
			HalconAPI.StoreS(proc, 2, operation);
			HalconAPI.StoreD(proc, 3, minValue);
			HalconAPI.StoreD(proc, 4, maxValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple AreaObjectModel3d(HObjectModel3D[] objectModel3D)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1080);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public double AreaObjectModel3d()
		{
			IntPtr proc = HalconAPI.PreCall(1080);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple MaxDiameterObjectModel3d(HObjectModel3D[] objectModel3D)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1081);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public double MaxDiameterObjectModel3d()
		{
			IntPtr proc = HalconAPI.PreCall(1081);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple MomentsObjectModel3d(HObjectModel3D[] objectModel3D, HTuple momentsToCalculate)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1082);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, momentsToCalculate);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(momentsToCalculate);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public double MomentsObjectModel3d(string momentsToCalculate)
		{
			IntPtr proc = HalconAPI.PreCall(1082);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, momentsToCalculate);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple VolumeObjectModel3dRelativeToPlane(HObjectModel3D[] objectModel3D, HPose[] plane, HTuple mode, HTuple useFaceOrientation)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(plane);
			IntPtr proc = HalconAPI.PreCall(1083);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.Store(proc, 2, mode);
			HalconAPI.Store(proc, 3, useFaceOrientation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(mode);
			HalconAPI.UnpinTuple(useFaceOrientation);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public double VolumeObjectModel3dRelativeToPlane(HPose plane, string mode, string useFaceOrientation)
		{
			IntPtr proc = HalconAPI.PreCall(1083);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, plane);
			HalconAPI.StoreS(proc, 2, mode);
			HalconAPI.StoreS(proc, 3, useFaceOrientation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(plane);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] ReduceObjectModel3dByView(HRegion region, HObjectModel3D[] objectModel3D, HCamPar camParam, HPose[] pose)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1084);
			HalconAPI.Store(proc, 1, region);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 2, hTuple2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(hTuple2);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(region);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D ReduceObjectModel3dByView(HRegion region, HCamPar camParam, HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(1084);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, region);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(pose);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			return result;
		}

		public static HObjectModel3D[] ConnectionObjectModel3d(HObjectModel3D[] objectModel3D, HTuple feature, HTuple value)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1085);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, feature);
			HalconAPI.Store(proc, 2, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(feature);
			HalconAPI.UnpinTuple(value);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D[] ConnectionObjectModel3d(string feature, double value)
		{
			IntPtr proc = HalconAPI.PreCall(1085);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, feature);
			HalconAPI.StoreD(proc, 2, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] SelectPointsObjectModel3d(HObjectModel3D[] objectModel3D, HTuple attrib, HTuple minValue, HTuple maxValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1086);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, attrib);
			HalconAPI.Store(proc, 2, minValue);
			HalconAPI.Store(proc, 3, maxValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(attrib);
			HalconAPI.UnpinTuple(minValue);
			HalconAPI.UnpinTuple(maxValue);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D SelectPointsObjectModel3d(string attrib, double minValue, double maxValue)
		{
			IntPtr proc = HalconAPI.PreCall(1086);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, attrib);
			HalconAPI.StoreD(proc, 2, minValue);
			HalconAPI.StoreD(proc, 3, maxValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple GetDispObjectModel3dInfo(HWindow windowHandle, HTuple row, HTuple column, HTuple information)
		{
			IntPtr proc = HalconAPI.PreCall(1087);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, information);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(information);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public static int GetDispObjectModel3dInfo(HWindow windowHandle, double row, double column, string information)
		{
			IntPtr proc = HalconAPI.PreCall(1087);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreS(proc, 3, information);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public static HImage RenderObjectModel3d(HObjectModel3D[] objectModel3D, HCamPar camParam, HPose[] pose, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1088);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 2, hTuple2);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HImage RenderObjectModel3d(HCamPar camParam, HPose pose, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1088);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static void DispObjectModel3d(HWindow windowHandle, HObjectModel3D[] objectModel3D, HCamPar camParam, HPose[] pose, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1089);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, hTuple);
			HalconAPI.Store(proc, 2, camParam);
			HalconAPI.Store(proc, 3, hTuple2);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(windowHandle);
			GC.KeepAlive(objectModel3D);
		}

		public void DispObjectModel3d(HWindow windowHandle, HCamPar camParam, HPose pose, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1089);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 2, camParam);
			HalconAPI.Store(proc, 3, pose);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public HObjectModel3D CopyObjectModel3d(HTuple attributes)
		{
			IntPtr proc = HalconAPI.PreCall(1090);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, attributes);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attributes);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HObjectModel3D CopyObjectModel3d(string attributes)
		{
			IntPtr proc = HalconAPI.PreCall(1090);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, attributes);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static void PrepareObjectModel3d(HObjectModel3D[] objectModel3D, string purpose, string overwriteData, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1091);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, purpose);
			HalconAPI.StoreS(proc, 2, overwriteData);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(objectModel3D);
		}

		public void PrepareObjectModel3d(string purpose, string overwriteData, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1091);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, purpose);
			HalconAPI.StoreS(proc, 2, overwriteData);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HImage ObjectModel3dToXyz(out HImage y, out HImage z, string type, HCamPar camParam, HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(1092);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, type);
			HalconAPI.Store(proc, 2, camParam);
			HalconAPI.Store(proc, 3, pose);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(pose);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out y);
			err = HImage.LoadNew(proc, 3, err, out z);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void XyzToObjectModel3d(HImage x, HImage y, HImage z)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1093);
			HalconAPI.Store(proc, 1, x);
			HalconAPI.Store(proc, 2, y);
			HalconAPI.Store(proc, 3, z);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(x);
			GC.KeepAlive(y);
			GC.KeepAlive(z);
		}

		public static HTuple GetObjectModel3dParams(HObjectModel3D[] objectModel3D, HTuple genParamName)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1094);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HTuple GetObjectModel3dParams(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1094);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ProjectObjectModel3d(HCamPar camParam, HPose pose, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1095);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ProjectObjectModel3d(HCamPar camParam, HPose pose, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1095);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.StoreS(proc, 3, genParamName);
			HalconAPI.StoreS(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(pose);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] RigidTransObjectModel3d(HObjectModel3D[] objectModel3D, HPose[] pose)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1096);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D RigidTransObjectModel3d(HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(1096);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, pose);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] ProjectiveTransObjectModel3d(HObjectModel3D[] objectModel3D, HHomMat3D homMat3D)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1097);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, homMat3D);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(homMat3D);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D ProjectiveTransObjectModel3d(HHomMat3D homMat3D)
		{
			IntPtr proc = HalconAPI.PreCall(1097);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, homMat3D);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat3D);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] AffineTransObjectModel3d(HObjectModel3D[] objectModel3D, HHomMat3D[] homMat3D)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(homMat3D);
			IntPtr proc = HalconAPI.PreCall(1098);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D AffineTransObjectModel3d(HHomMat3D homMat3D)
		{
			IntPtr proc = HalconAPI.PreCall(1098);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, homMat3D);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat3D);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HSerializedItem SerializeObjectModel3d()
		{
			IntPtr proc = HalconAPI.PreCall(1101);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializeObjectModel3d(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1102);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public void WriteObjectModel3d(string fileType, string fileName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1103);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileType);
			HalconAPI.StoreS(proc, 2, fileName);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteObjectModel3d(string fileType, string fileName, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1103);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileType);
			HalconAPI.StoreS(proc, 2, fileName);
			HalconAPI.StoreS(proc, 3, genParamName);
			HalconAPI.StoreS(proc, 4, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ReadObjectModel3d(string fileName, HTuple scale, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1104);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, scale);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(scale);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string ReadObjectModel3d(string fileName, string scale, string genParamName, string genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1104);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, scale);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			string result = null;
			err = HalconAPI.LoadS(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HObjectModel3D[] SceneFlowCalib(HImage imageRect1T1, HImage imageRect2T1, HImage imageRect1T2, HImage imageRect2T2, HImage disparity, HTuple smoothingFlow, HTuple smoothingDisparity, HTuple genParamName, HTuple genParamValue, HCamPar camParamRect1, HCamPar camParamRect2, HPose relPoseRect)
		{
			IntPtr proc = HalconAPI.PreCall(1481);
			HalconAPI.Store(proc, 1, imageRect1T1);
			HalconAPI.Store(proc, 2, imageRect2T1);
			HalconAPI.Store(proc, 3, imageRect1T2);
			HalconAPI.Store(proc, 4, imageRect2T2);
			HalconAPI.Store(proc, 5, disparity);
			HalconAPI.Store(proc, 0, smoothingFlow);
			HalconAPI.Store(proc, 1, smoothingDisparity);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.Store(proc, 4, camParamRect1);
			HalconAPI.Store(proc, 5, camParamRect2);
			HalconAPI.Store(proc, 6, relPoseRect);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(smoothingFlow);
			HalconAPI.UnpinTuple(smoothingDisparity);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(imageRect1T1);
			GC.KeepAlive(imageRect2T1);
			GC.KeepAlive(imageRect1T2);
			GC.KeepAlive(imageRect2T2);
			GC.KeepAlive(disparity);
			return result;
		}

		public void SceneFlowCalib(HImage imageRect1T1, HImage imageRect2T1, HImage imageRect1T2, HImage imageRect2T2, HImage disparity, double smoothingFlow, double smoothingDisparity, string genParamName, string genParamValue, HCamPar camParamRect1, HCamPar camParamRect2, HPose relPoseRect)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1481);
			HalconAPI.Store(proc, 1, imageRect1T1);
			HalconAPI.Store(proc, 2, imageRect2T1);
			HalconAPI.Store(proc, 3, imageRect1T2);
			HalconAPI.Store(proc, 4, imageRect2T2);
			HalconAPI.Store(proc, 5, disparity);
			HalconAPI.StoreD(proc, 0, smoothingFlow);
			HalconAPI.StoreD(proc, 1, smoothingDisparity);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.Store(proc, 4, camParamRect1);
			HalconAPI.Store(proc, 5, camParamRect2);
			HalconAPI.Store(proc, 6, relPoseRect);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1T1);
			GC.KeepAlive(imageRect2T1);
			GC.KeepAlive(imageRect1T2);
			GC.KeepAlive(imageRect2T2);
			GC.KeepAlive(disparity);
		}

		public HObjectModel3D EdgesObjectModel3d(HTuple minAmplitude, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2067);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, minAmplitude);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minAmplitude);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HObjectModel3D EdgesObjectModel3d(double minAmplitude, string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2067);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, minAmplitude);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HPose[] FindSurfaceModelImage(HImage image, HSurfaceModel surfaceModelID, double relSamplingDistance, double keyPointFraction, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(2069);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.Store(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(surfaceModelID);
			return result;
		}

		public HPose FindSurfaceModelImage(HImage image, HSurfaceModel surfaceModelID, double relSamplingDistance, double keyPointFraction, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(2069);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.StoreD(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(surfaceModelID);
			return result;
		}

		public HPose[] RefineSurfaceModelPoseImage(HImage image, HSurfaceModel surfaceModelID, HPose[] initialPose, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			HTuple hTuple = HData.ConcatArray(initialPose);
			IntPtr proc = HalconAPI.PreCall(2084);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 2, hTuple);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(surfaceModelID);
			return result;
		}

		public HPose RefineSurfaceModelPoseImage(HImage image, HSurfaceModel surfaceModelID, HPose initialPose, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(2084);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 2, initialPose);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(initialPose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(surfaceModelID);
			return result;
		}

		public static HObjectModel3D FuseObjectModel3d(HObjectModel3D[] objectModel3D, HTuple boundingBox, HTuple resolution, HTuple surfaceTolerance, HTuple minThickness, HTuple smoothing, HTuple normalDirection, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(2112);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, boundingBox);
			HalconAPI.Store(proc, 2, resolution);
			HalconAPI.Store(proc, 3, surfaceTolerance);
			HalconAPI.Store(proc, 4, minThickness);
			HalconAPI.Store(proc, 5, smoothing);
			HalconAPI.Store(proc, 6, normalDirection);
			HalconAPI.Store(proc, 7, genParamName);
			HalconAPI.Store(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(boundingBox);
			HalconAPI.UnpinTuple(resolution);
			HalconAPI.UnpinTuple(surfaceTolerance);
			HalconAPI.UnpinTuple(minThickness);
			HalconAPI.UnpinTuple(smoothing);
			HalconAPI.UnpinTuple(normalDirection);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D FuseObjectModel3d(HTuple boundingBox, double resolution, double surfaceTolerance, double minThickness, double smoothing, string normalDirection, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2112);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, boundingBox);
			HalconAPI.StoreD(proc, 2, resolution);
			HalconAPI.StoreD(proc, 3, surfaceTolerance);
			HalconAPI.StoreD(proc, 4, minThickness);
			HalconAPI.StoreD(proc, 5, smoothing);
			HalconAPI.StoreS(proc, 6, normalDirection);
			HalconAPI.Store(proc, 7, genParamName);
			HalconAPI.Store(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(boundingBox);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1100);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
