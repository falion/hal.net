using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;

namespace VisionConfig
{
	public class HSerializedItem : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSerializedItem()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSerializedItem(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSerializedItem obj)
		{
			obj = new HSerializedItem(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSerializedItem[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HSerializedItem[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HSerializedItem(hTuple[i].IP);
			}
			return err;
		}

		public HSerializedItem(IntPtr pointer, int size, string copy)
		{
			IntPtr proc = HalconAPI.PreCall(410);
			HalconAPI.StoreIP(proc, 0, pointer);
			HalconAPI.StoreI(proc, 1, size);
			HalconAPI.StoreS(proc, 2, copy);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSerializedItem(byte[] data)
		{
			GCHandle gCHandle = GCHandle.Alloc(data, GCHandleType.Pinned);
			IntPtr pointer = gCHandle.AddrOfPinnedObject();
			this.CreateSerializedItemPtr(pointer, data.Length, "true");
			gCHandle.Free();
		}

		public static implicit operator byte[](HSerializedItem item)
		{
			int num = 0;
			IntPtr serializedItemPtr = item.GetSerializedItemPtr(out num);
			byte[] array = new byte[num];
			Marshal.Copy(serializedItemPtr, array, 0, num);
			return array;
		}

		internal void Serialize(Stream stream)
		{
			byte[] array = this;
			stream.Write(array, 0, array.Length);
		}

		internal static HSerializedItem Deserialize(Stream stream)
		{
			BinaryReader binaryReader = new BinaryReader(stream);
			byte[] array = binaryReader.ReadBytes(16);
			ulong num = 0uL;
			if (array.Length >= 16 && !HalconAPI.IsFailure(HalconAPI.GetSerializedSize(array, out num)))
			{
				if (num > 2415918079u)
				{
					throw new HalconException("Input stream too large");
				}
				byte[] array2 = binaryReader.ReadBytes((int)num);
				if (array2.Length >= (int)num && !HalconAPI.IsFailure(HalconAPI.GetSerializedSize(array, out num)))
				{
					int num2 = (int)num + 16;
					byte[] array3 = new byte[num2];
					array.CopyTo(array3, 0);
					array2.CopyTo(array3, 16);
					return new HSerializedItem(array3);
				}
				throw new HalconException("Unexpected end of serialization data");
			}
			throw new HalconException("Input stream is no serialized HALCON object");
		}

		public void ReceiveSerializedItem(HSocket socket)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(403);
			HalconAPI.Store(proc, 0, socket);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(socket);
		}

		public void SendSerializedItem(HSocket socket)
		{
			IntPtr proc = HalconAPI.PreCall(404);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, socket);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(socket);
		}

		public void FwriteSerializedItem(HFile fileHandle)
		{
			IntPtr proc = HalconAPI.PreCall(405);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, fileHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(fileHandle);
		}

		public void FreadSerializedItem(HFile fileHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(406);
			HalconAPI.Store(proc, 0, fileHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(fileHandle);
		}

		public IntPtr GetSerializedItemPtr(out int size)
		{
			IntPtr proc = HalconAPI.PreCall(409);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			IntPtr result = default(IntPtr);
			err = HalconAPI.LoadIP(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out size);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void CreateSerializedItemPtr(IntPtr pointer, int size, string copy)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(410);
			HalconAPI.StoreIP(proc, 0, pointer);
			HalconAPI.StoreI(proc, 1, size);
			HalconAPI.StoreS(proc, 2, copy);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(408);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
