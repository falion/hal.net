using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HIOChannel : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HIOChannel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HIOChannel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HIOChannel obj)
		{
			obj = new HIOChannel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HIOChannel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HIOChannel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HIOChannel(hTuple[i].IP);
			}
			return err;
		}

		public HIOChannel(HIODevice IODeviceHandle, string IOChannelName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2016);
			HalconAPI.Store(proc, 0, IODeviceHandle);
			HalconAPI.StoreS(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(IODeviceHandle);
		}

		public static HTuple ControlIoChannel(HIOChannel[] IOChannelHandle, string paramAction, HTuple paramArgument)
		{
			HTuple hTuple = HTool.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2010);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, paramAction);
			HalconAPI.Store(proc, 2, paramArgument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(paramArgument);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(IOChannelHandle);
			return result;
		}

		public HTuple ControlIoChannel(string paramAction, HTuple paramArgument)
		{
			IntPtr proc = HalconAPI.PreCall(2010);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, paramAction);
			HalconAPI.Store(proc, 2, paramArgument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(paramArgument);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple WriteIoChannel(HIOChannel[] IOChannelHandle, HTuple value)
		{
			HTuple hTuple = HTool.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2011);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(value);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(IOChannelHandle);
			return result;
		}

		public HTuple WriteIoChannel(HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(2011);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(value);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple ReadIoChannel(HIOChannel[] IOChannelHandle, out HTuple status)
		{
			HTuple hTuple = HTool.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2012);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out status);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(IOChannelHandle);
			return result;
		}

		public HTuple ReadIoChannel(out HTuple status)
		{
			IntPtr proc = HalconAPI.PreCall(2012);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out status);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static void SetIoChannelParam(HIOChannel[] IOChannelHandle, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2013);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(IOChannelHandle);
		}

		public void SetIoChannelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2013);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static HTuple GetIoChannelParam(HIOChannel[] IOChannelHandle, HTuple genParamName)
		{
			HTuple hTuple = HTool.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2014);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(IOChannelHandle);
			return result;
		}

		public HTuple GetIoChannelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2014);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HIOChannel[] OpenIoChannel(HIODevice IODeviceHandle, HTuple IOChannelName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2016);
			HalconAPI.Store(proc, 0, IODeviceHandle);
			HalconAPI.Store(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(IOChannelName);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HIOChannel[] result = null;
			err = HIOChannel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(IODeviceHandle);
			return result;
		}

		public void OpenIoChannel(HIODevice IODeviceHandle, string IOChannelName, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2016);
			HalconAPI.Store(proc, 0, IODeviceHandle);
			HalconAPI.StoreS(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(IODeviceHandle);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(2015);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
