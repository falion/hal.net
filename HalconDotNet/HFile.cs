using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HFile : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFile()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFile(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFile obj)
		{
			obj = new HFile(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFile[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HFile[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HFile(hTuple[i].IP);
			}
			return err;
		}

		public HFile(string fileName, string fileType)
		{
			IntPtr proc = HalconAPI.PreCall(1659);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, fileType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenFile(string fileName, string fileType)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1659);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, fileType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FwriteString(HTuple stringVal)
		{
			IntPtr proc = HalconAPI.PreCall(1660);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, stringVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(stringVal);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void FwriteString(string stringVal)
		{
			IntPtr proc = HalconAPI.PreCall(1660);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, stringVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string FreadLine(out int isEOF)
		{
			IntPtr proc = HalconAPI.PreCall(1661);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out isEOF);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string FreadString(out int isEOF)
		{
			IntPtr proc = HalconAPI.PreCall(1662);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out isEOF);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string FreadChar()
		{
			IntPtr proc = HalconAPI.PreCall(1663);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void FnewLine()
		{
			IntPtr proc = HalconAPI.PreCall(1664);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1665);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
