using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms.Design;

namespace VisionConfig
{
	public class HWindowControlDesigner : ControlDesigner
	{
		private HWindowControl windowControl;

		public Bitmap LayoutBitmap
		{
			get
			{
				return null;
			}
			set
			{
				if (value != null)
				{
					HalconWindowLayoutDialog halconWindowLayoutDialog = new HalconWindowLayoutDialog(value.Size);
					halconWindowLayoutDialog.ShowDialog();
					if (!halconWindowLayoutDialog.resultCancel)
					{
						HWindowControl hWindowControl = this.windowControl;
						Size size = value.Size;
						int width = size.Width * halconWindowLayoutDialog.resultPercent / 100;
						size = value.Size;
						hWindowControl.WindowSize = new Size(width, size.Height * halconWindowLayoutDialog.resultPercent / 100);
						this.windowControl.ImagePart = new Rectangle(Point.Empty, value.Size);
					}
				}
			}
		}

		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
			this.windowControl = (HWindowControl)component;
		}

		protected override void PreFilterProperties(IDictionary properties)
		{
			base.PreFilterProperties(properties);
			Attribute[] attributes = new Attribute[3]
			{
				CategoryAttribute.Layout,
				DesignOnlyAttribute.Yes,
				new DescriptionAttribute("This design-time property allows you to configure Size and ImagePart by providing a reference image of the desired size.")
			};
			properties["LayoutBitmap"] = TypeDescriptor.CreateProperty(typeof(HWindowControlDesigner), "LayoutBitmap", typeof(Bitmap), attributes);
			properties.Remove("BorderStyle");
		}
	}
}
