using System;

namespace VisionConfig
{
	internal class HTupleMixed : HTupleImplementation
	{
		protected object[] o;

		public override object[] OArr
		{
			get
			{
				return this.o;
			}
			set
			{
				base.SetArray(value, false);
			}
		}

		public override HTupleType Type
		{
			get
			{
				return HTupleType.MIXED;
			}
		}

		protected override Array CreateArray(int size)
		{
			object[] array = new object[size];
			for (int i = 0; i < size; i++)
			{
				array[i] = 0;
			}
			return array;
		}

		protected override void NotifyArrayUpdate()
		{
			this.o = (object[])base.data;
		}

		public HTupleMixed(HTupleImplementation data)
			: this(data.ToOArr(), false)
		{
		}

		public HTupleMixed(object o)
			: this(new object[1]
			{
				o
			}, false)
		{
		}

		public HTupleMixed(object[] o, bool copy)
		{
			int num = 0;
			while (num < o.Length)
			{
				int objectType = HTupleImplementation.GetObjectType(o[num]);
				if (objectType != 15 && (objectType & 0x8000) <= 0)
				{
					num++;
					continue;
				}
				throw new HTupleAccessException("Encountered invalid data types when creating HTuple");
			}
			base.SetArray(o, copy);
		}

		public override HTupleElements GetElement(int index, HTuple parent)
		{
			return new HTupleElements(parent, this, index);
		}

		public override HTupleElements GetElements(int[] indices, HTuple parent)
		{
			if (indices != null && indices.Length != 0)
			{
				return new HTupleElements(parent, this, indices);
			}
			return new HTupleElements();
		}

		public override void SetElements(int[] indices, HTupleElements elements)
		{
			if (indices == null)
			{
				return;
			}
			if (indices.Length == 0)
			{
				return;
			}
			object[] oArr = elements.OArr;
			if (oArr.Length == indices.Length)
			{
				for (int i = 0; i < indices.Length; i++)
				{
					this.o[indices[i]] = oArr[i];
				}
				return;
			}
			if (oArr.Length == 1)
			{
				for (int j = 0; j < indices.Length; j++)
				{
					this.o[indices[j]] = oArr[0];
				}
				return;
			}
			throw new HTupleAccessException(this, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
		}

		public HTupleType GetElementType(int index)
		{
			return (HTupleType)HTupleImplementation.GetObjectType(this.o[index]);
		}

		public HTupleType GetElementType(int[] indices)
		{
			if (indices != null && indices.Length != 0)
			{
				HTupleType objectType = (HTupleType)HTupleImplementation.GetObjectType(this.o[indices[0]]);
				for (int i = 1; i < indices.Length; i++)
				{
					HTupleType objectType2 = (HTupleType)HTupleImplementation.GetObjectType(this.o[indices[i]]);
					if (objectType2 != objectType)
					{
						return HTupleType.MIXED;
					}
				}
				return objectType;
			}
			return HTupleType.EMPTY;
		}

		protected override void StoreData(IntPtr proc, IntPtr tuple)
		{
			for (int i = 0; i < base.iLength; i++)
			{
				switch (HTupleImplementation.GetObjectType(this.o[i]))
				{
				case 1:
					HalconAPI.HCkP(proc, HalconAPI.SetI(tuple, i, (int)this.o[i]));
					break;
				case 129:
					HalconAPI.HCkP(proc, HalconAPI.SetL(tuple, i, (long)this.o[i]));
					break;
				case 2:
					HalconAPI.HCkP(proc, HalconAPI.SetD(tuple, i, (double)this.o[i]));
					break;
				case 4:
					HalconAPI.HCkP(proc, HalconAPI.SetS(tuple, i, (string)this.o[i]));
					break;
				}
			}
		}

		public static int Load(IntPtr tuple, out HTupleMixed data)
		{
			int num = 2;
			int num2 = 0;
			HalconAPI.GetTupleLength(tuple, out num2);
			object[] array = new object[num2];
			for (int i = 0; i < num2; i++)
			{
				if (!HalconAPI.IsFailure(num))
				{
					HTupleType hTupleType = (HTupleType)0;
					HalconAPI.GetElementType(tuple, i, out hTupleType);
					switch (hTupleType)
					{
					case HTupleType.INTEGER:
						if (HalconAPI.isPlatform64)
						{
							long num4 = 0L;
							num = HalconAPI.GetL(tuple, i, out num4);
							array[i] = num4;
						}
						else
						{
							int num5 = 0;
							num = HalconAPI.GetI(tuple, i, out num5);
							array[i] = num5;
						}
						break;
					case HTupleType.DOUBLE:
					{
						double num3 = 0.0;
						num = HalconAPI.GetD(tuple, i, out num3);
						array[i] = num3;
						break;
					}
					case HTupleType.STRING:
					{
						string text = null;
						num = HalconAPI.GetS(tuple, i, out text);
						array[i] = text;
						break;
					}
					}
				}
			}
			data = new HTupleMixed(array, false);
			return num;
		}
	}
}
