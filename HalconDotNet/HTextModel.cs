using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HTextModel : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextModel obj)
		{
			obj = new HTextModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HTextModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HTextModel(hTuple[i].IP);
			}
			return err;
		}

		public HTextModel(string mode, HTuple OCRClassifier)
		{
			IntPtr proc = HalconAPI.PreCall(422);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, OCRClassifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(OCRClassifier);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTextModel(string mode, string OCRClassifier)
		{
			IntPtr proc = HalconAPI.PreCall(422);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreS(proc, 1, OCRClassifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTextModel()
		{
			IntPtr proc = HalconAPI.PreCall(423);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTextResult FindText(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(417);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTextResult result = null;
			err = HTextResult.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HTuple GetTextModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(418);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetTextModelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(419);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetTextModelParam(string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(419);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreI(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateTextModelReader(string mode, HTuple OCRClassifier)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(422);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, OCRClassifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(OCRClassifier);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateTextModelReader(string mode, string OCRClassifier)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(422);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreS(proc, 1, OCRClassifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateTextModel()
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(423);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(421);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
