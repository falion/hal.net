using System;
using System.ComponentModel;

namespace VisionConfig
{
	[Serializable]
	public class HXLDCont : HXLD
	{
		public new HXLDCont this[HTuple index]
		{
			get
			{
				return this.SelectObj(index);
			}
		}

		public HXLDCont()
			: base(HObjectBase.UNDEF, false)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDCont(IntPtr key)
			: this(key, true)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDCont(IntPtr key, bool copy)
			: base(key, copy)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDCont(HObject obj)
			: base(obj)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		private void AssertObjectClass()
		{
			HalconAPI.AssertObjectClass(base.key, "xld_cont");
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, int err, out HXLDCont obj)
		{
			obj = new HXLDCont(HObjectBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		public HXLDCont(HRegion regions, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(70);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
		}

		public HXLDCont(HTuple row, HTuple col)
		{
			IntPtr proc = HalconAPI.PreCall(72);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont UnionCotangentialContoursXld(double fitClippingLength, HTuple fitLength, double maxTangAngle, double maxDist, double maxDistPerp, double maxOverlap, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(0);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, fitClippingLength);
			HalconAPI.Store(proc, 1, fitLength);
			HalconAPI.StoreD(proc, 2, maxTangAngle);
			HalconAPI.StoreD(proc, 3, maxDist);
			HalconAPI.StoreD(proc, 4, maxDistPerp);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fitLength);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont UnionCotangentialContoursXld(double fitClippingLength, double fitLength, double maxTangAngle, double maxDist, double maxDistPerp, double maxOverlap, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(0);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, fitClippingLength);
			HalconAPI.StoreD(proc, 1, fitLength);
			HalconAPI.StoreD(proc, 2, maxTangAngle);
			HalconAPI.StoreD(proc, 3, maxDist);
			HalconAPI.StoreD(proc, 4, maxDistPerp);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont PolarTransContourXldInv(HTuple row, HTuple column, double angleStart, double angleEnd, HTuple radiusStart, HTuple radiusEnd, int widthIn, int heightIn, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.Store(proc, 4, radiusStart);
			HalconAPI.Store(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, widthIn);
			HalconAPI.StoreI(proc, 7, heightIn);
			HalconAPI.StoreI(proc, 8, width);
			HalconAPI.StoreI(proc, 9, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radiusStart);
			HalconAPI.UnpinTuple(radiusEnd);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont PolarTransContourXldInv(double row, double column, double angleStart, double angleEnd, double radiusStart, double radiusEnd, int widthIn, int heightIn, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.StoreD(proc, 4, radiusStart);
			HalconAPI.StoreD(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, widthIn);
			HalconAPI.StoreI(proc, 7, heightIn);
			HalconAPI.StoreI(proc, 8, width);
			HalconAPI.StoreI(proc, 9, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont PolarTransContourXld(HTuple row, HTuple column, double angleStart, double angleEnd, HTuple radiusStart, HTuple radiusEnd, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(2);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.Store(proc, 4, radiusStart);
			HalconAPI.Store(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radiusStart);
			HalconAPI.UnpinTuple(radiusEnd);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont PolarTransContourXld(double row, double column, double angleStart, double angleEnd, double radiusStart, double radiusEnd, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(2);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.StoreD(proc, 4, radiusStart);
			HalconAPI.StoreD(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GenContourNurbsXld(HTuple rows, HTuple cols, HTuple knots, HTuple weights, int degree, HTuple maxError, HTuple maxDistance)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(4);
			HalconAPI.Store(proc, 0, rows);
			HalconAPI.Store(proc, 1, cols);
			HalconAPI.Store(proc, 2, knots);
			HalconAPI.Store(proc, 3, weights);
			HalconAPI.StoreI(proc, 4, degree);
			HalconAPI.Store(proc, 5, maxError);
			HalconAPI.Store(proc, 6, maxDistance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(cols);
			HalconAPI.UnpinTuple(knots);
			HalconAPI.UnpinTuple(weights);
			HalconAPI.UnpinTuple(maxError);
			HalconAPI.UnpinTuple(maxDistance);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenContourNurbsXld(HTuple rows, HTuple cols, string knots, string weights, int degree, double maxError, double maxDistance)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(4);
			HalconAPI.Store(proc, 0, rows);
			HalconAPI.Store(proc, 1, cols);
			HalconAPI.StoreS(proc, 2, knots);
			HalconAPI.StoreS(proc, 3, weights);
			HalconAPI.StoreI(proc, 4, degree);
			HalconAPI.StoreD(proc, 5, maxError);
			HalconAPI.StoreD(proc, 6, maxDistance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(cols);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont Union2ClosedContoursXld(HXLDCont contours2)
		{
			IntPtr proc = HalconAPI.PreCall(6);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, contours2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours2);
			return result;
		}

		public HXLDCont SymmDifferenceClosedContoursXld(HXLDCont contours2)
		{
			IntPtr proc = HalconAPI.PreCall(8);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, contours2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours2);
			return result;
		}

		public HXLDCont DifferenceClosedContoursXld(HXLDCont sub)
		{
			IntPtr proc = HalconAPI.PreCall(10);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, sub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sub);
			return result;
		}

		public HXLDCont IntersectionClosedContoursXld(HXLDCont contours2)
		{
			IntPtr proc = HalconAPI.PreCall(12);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, contours2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours2);
			return result;
		}

		public HXLDCont UnionCocircularContoursXld(HTuple maxArcAngleDiff, HTuple maxArcOverlap, HTuple maxTangentAngle, HTuple maxDist, HTuple maxRadiusDiff, HTuple maxCenterDist, string mergeSmallContours, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(13);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, maxArcAngleDiff);
			HalconAPI.Store(proc, 1, maxArcOverlap);
			HalconAPI.Store(proc, 2, maxTangentAngle);
			HalconAPI.Store(proc, 3, maxDist);
			HalconAPI.Store(proc, 4, maxRadiusDiff);
			HalconAPI.Store(proc, 5, maxCenterDist);
			HalconAPI.StoreS(proc, 6, mergeSmallContours);
			HalconAPI.StoreI(proc, 7, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maxArcAngleDiff);
			HalconAPI.UnpinTuple(maxArcOverlap);
			HalconAPI.UnpinTuple(maxTangentAngle);
			HalconAPI.UnpinTuple(maxDist);
			HalconAPI.UnpinTuple(maxRadiusDiff);
			HalconAPI.UnpinTuple(maxCenterDist);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont UnionCocircularContoursXld(double maxArcAngleDiff, double maxArcOverlap, double maxTangentAngle, double maxDist, double maxRadiusDiff, double maxCenterDist, string mergeSmallContours, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(13);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxArcAngleDiff);
			HalconAPI.StoreD(proc, 1, maxArcOverlap);
			HalconAPI.StoreD(proc, 2, maxTangentAngle);
			HalconAPI.StoreD(proc, 3, maxDist);
			HalconAPI.StoreD(proc, 4, maxRadiusDiff);
			HalconAPI.StoreD(proc, 5, maxCenterDist);
			HalconAPI.StoreS(proc, 6, mergeSmallContours);
			HalconAPI.StoreI(proc, 7, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont CropContoursXld(HTuple row1, HTuple col1, HTuple row2, HTuple col2, string closeContours)
		{
			IntPtr proc = HalconAPI.PreCall(14);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, col1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, col2);
			HalconAPI.StoreS(proc, 4, closeContours);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(col1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(col2);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont CropContoursXld(double row1, double col1, double row2, double col2, string closeContours)
		{
			IntPtr proc = HalconAPI.PreCall(14);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, col1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, col2);
			HalconAPI.StoreS(proc, 4, closeContours);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GenCrossContourXld(HTuple row, HTuple col, HTuple size, double angle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(15);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.Store(proc, 2, size);
			HalconAPI.StoreD(proc, 3, angle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(size);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCrossContourXld(double row, double col, double size, double angle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(15);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, col);
			HalconAPI.StoreD(proc, 2, size);
			HalconAPI.StoreD(proc, 3, angle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont SortContoursXld(string sortMode, string order, string rowOrCol)
		{
			IntPtr proc = HalconAPI.PreCall(16);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, sortMode);
			HalconAPI.StoreS(proc, 1, order);
			HalconAPI.StoreS(proc, 2, rowOrCol);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont MergeContLineScanXld(HXLDCont prevConts, out HXLDCont prevMergedConts, int imageHeight, HTuple margin, string mergeBorder, int maxImagesCont)
		{
			IntPtr proc = HalconAPI.PreCall(17);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, prevConts);
			HalconAPI.StoreI(proc, 0, imageHeight);
			HalconAPI.Store(proc, 1, margin);
			HalconAPI.StoreS(proc, 2, mergeBorder);
			HalconAPI.StoreI(proc, 3, maxImagesCont);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(margin);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HXLDCont.LoadNew(proc, 2, err, out prevMergedConts);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(prevConts);
			return result;
		}

		public HXLDCont MergeContLineScanXld(HXLDCont prevConts, out HXLDCont prevMergedConts, int imageHeight, double margin, string mergeBorder, int maxImagesCont)
		{
			IntPtr proc = HalconAPI.PreCall(17);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, prevConts);
			HalconAPI.StoreI(proc, 0, imageHeight);
			HalconAPI.StoreD(proc, 1, margin);
			HalconAPI.StoreS(proc, 2, mergeBorder);
			HalconAPI.StoreI(proc, 3, maxImagesCont);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HXLDCont.LoadNew(proc, 2, err, out prevMergedConts);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(prevConts);
			return result;
		}

		public void ReadContourXldArcInfo(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(20);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteContourXldArcInfo(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(21);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HXLDCont GenParallelContourXld(string mode, HTuple distance)
		{
			IntPtr proc = HalconAPI.PreCall(23);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(distance);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont GenParallelContourXld(string mode, double distance)
		{
			IntPtr proc = HalconAPI.PreCall(23);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GenRectangle2ContourXld(HTuple row, HTuple column, HTuple phi, HTuple length1, HTuple length2)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(24);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, phi);
			HalconAPI.Store(proc, 3, length1);
			HalconAPI.Store(proc, 4, length2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(length1);
			HalconAPI.UnpinTuple(length2);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRectangle2ContourXld(double row, double column, double phi, double length1, double length2)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(24);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, length1);
			HalconAPI.StoreD(proc, 4, length2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DistRectangle2ContourPointsXld(int clippingEndPoints, double row, double column, double phi, double length1, double length2)
		{
			IntPtr proc = HalconAPI.PreCall(25);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, clippingEndPoints);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreD(proc, 4, length1);
			HalconAPI.StoreD(proc, 5, length2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void FitRectangle2ContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int iterations, double clippingFactor, out HTuple row, out HTuple column, out HTuple phi, out HTuple length1, out HTuple length2, out HTuple pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(26);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, iterations);
			HalconAPI.StoreD(proc, 5, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out length1);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out length2);
			err = HTuple.LoadNew(proc, 5, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitRectangle2ContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int iterations, double clippingFactor, out double row, out double column, out double phi, out double length1, out double length2, out string pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(26);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, iterations);
			HalconAPI.StoreD(proc, 5, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out length1);
			err = HalconAPI.LoadD(proc, 4, err, out length2);
			err = HalconAPI.LoadS(proc, 5, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont SegmentContourAttribXld(HTuple attribute, string operation, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(27);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, attribute);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.Store(proc, 2, min);
			HalconAPI.Store(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribute);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont SegmentContourAttribXld(string attribute, string operation, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(27);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, attribute);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.StoreD(proc, 2, min);
			HalconAPI.StoreD(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont SegmentContoursXld(string mode, int smoothCont, double maxLineDist1, double maxLineDist2)
		{
			IntPtr proc = HalconAPI.PreCall(28);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, smoothCont);
			HalconAPI.StoreD(proc, 2, maxLineDist1);
			HalconAPI.StoreD(proc, 3, maxLineDist2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void FitCircleContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int iterations, double clippingFactor, out HTuple row, out HTuple column, out HTuple radius, out HTuple startPhi, out HTuple endPhi, out HTuple pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(29);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, iterations);
			HalconAPI.StoreD(proc, 5, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out radius);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out startPhi);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out endPhi);
			err = HTuple.LoadNew(proc, 5, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitCircleContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int iterations, double clippingFactor, out double row, out double column, out double radius, out double startPhi, out double endPhi, out string pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(29);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, iterations);
			HalconAPI.StoreD(proc, 5, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out radius);
			err = HalconAPI.LoadD(proc, 3, err, out startPhi);
			err = HalconAPI.LoadD(proc, 4, err, out endPhi);
			err = HalconAPI.LoadS(proc, 5, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitLineContourXld(string algorithm, int maxNumPoints, int clippingEndPoints, int iterations, double clippingFactor, out HTuple rowBegin, out HTuple colBegin, out HTuple rowEnd, out HTuple colEnd, out HTuple nr, out HTuple nc, out HTuple dist)
		{
			IntPtr proc = HalconAPI.PreCall(30);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreI(proc, 2, clippingEndPoints);
			HalconAPI.StoreI(proc, 3, iterations);
			HalconAPI.StoreD(proc, 4, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowBegin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out colBegin);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out rowEnd);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out colEnd);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out nr);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out nc);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out dist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitLineContourXld(string algorithm, int maxNumPoints, int clippingEndPoints, int iterations, double clippingFactor, out double rowBegin, out double colBegin, out double rowEnd, out double colEnd, out double nr, out double nc, out double dist)
		{
			IntPtr proc = HalconAPI.PreCall(30);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreI(proc, 2, clippingEndPoints);
			HalconAPI.StoreI(proc, 3, iterations);
			HalconAPI.StoreD(proc, 4, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out rowBegin);
			err = HalconAPI.LoadD(proc, 1, err, out colBegin);
			err = HalconAPI.LoadD(proc, 2, err, out rowEnd);
			err = HalconAPI.LoadD(proc, 3, err, out colEnd);
			err = HalconAPI.LoadD(proc, 4, err, out nr);
			err = HalconAPI.LoadD(proc, 5, err, out nc);
			err = HalconAPI.LoadD(proc, 6, err, out dist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DistEllipseContourPointsXld(string distanceMode, int clippingEndPoints, double row, double column, double phi, double radius1, double radius2)
		{
			IntPtr proc = HalconAPI.PreCall(31);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, distanceMode);
			HalconAPI.StoreI(proc, 1, clippingEndPoints);
			HalconAPI.StoreD(proc, 2, row);
			HalconAPI.StoreD(proc, 3, column);
			HalconAPI.StoreD(proc, 4, phi);
			HalconAPI.StoreD(proc, 5, radius1);
			HalconAPI.StoreD(proc, 6, radius2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DistEllipseContourXld(string mode, int maxNumPoints, int clippingEndPoints, double row, double column, double phi, double radius1, double radius2, out HTuple minDist, out HTuple maxDist, out HTuple avgDist, out HTuple sigmaDist)
		{
			IntPtr proc = HalconAPI.PreCall(32);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreI(proc, 2, clippingEndPoints);
			HalconAPI.StoreD(proc, 3, row);
			HalconAPI.StoreD(proc, 4, column);
			HalconAPI.StoreD(proc, 5, phi);
			HalconAPI.StoreD(proc, 6, radius1);
			HalconAPI.StoreD(proc, 7, radius2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out minDist);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out maxDist);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out avgDist);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out sigmaDist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistEllipseContourXld(string mode, int maxNumPoints, int clippingEndPoints, double row, double column, double phi, double radius1, double radius2, out double minDist, out double maxDist, out double avgDist, out double sigmaDist)
		{
			IntPtr proc = HalconAPI.PreCall(32);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreI(proc, 2, clippingEndPoints);
			HalconAPI.StoreD(proc, 3, row);
			HalconAPI.StoreD(proc, 4, column);
			HalconAPI.StoreD(proc, 5, phi);
			HalconAPI.StoreD(proc, 6, radius1);
			HalconAPI.StoreD(proc, 7, radius2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out minDist);
			err = HalconAPI.LoadD(proc, 1, err, out maxDist);
			err = HalconAPI.LoadD(proc, 2, err, out avgDist);
			err = HalconAPI.LoadD(proc, 3, err, out sigmaDist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitEllipseContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int vossTabSize, int iterations, double clippingFactor, out HTuple row, out HTuple column, out HTuple phi, out HTuple radius1, out HTuple radius2, out HTuple startPhi, out HTuple endPhi, out HTuple pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(33);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, vossTabSize);
			HalconAPI.StoreI(proc, 5, iterations);
			HalconAPI.StoreD(proc, 6, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out radius1);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out radius2);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out startPhi);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out endPhi);
			err = HTuple.LoadNew(proc, 7, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitEllipseContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int vossTabSize, int iterations, double clippingFactor, out double row, out double column, out double phi, out double radius1, out double radius2, out double startPhi, out double endPhi, out string pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(33);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, vossTabSize);
			HalconAPI.StoreI(proc, 5, iterations);
			HalconAPI.StoreD(proc, 6, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out radius1);
			err = HalconAPI.LoadD(proc, 4, err, out radius2);
			err = HalconAPI.LoadD(proc, 5, err, out startPhi);
			err = HalconAPI.LoadD(proc, 6, err, out endPhi);
			err = HalconAPI.LoadS(proc, 7, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCircleContourXld(HTuple row, HTuple column, HTuple radius, HTuple startPhi, HTuple endPhi, HTuple pointOrder, double resolution)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(34);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.Store(proc, 3, startPhi);
			HalconAPI.Store(proc, 4, endPhi);
			HalconAPI.Store(proc, 5, pointOrder);
			HalconAPI.StoreD(proc, 6, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(startPhi);
			HalconAPI.UnpinTuple(endPhi);
			HalconAPI.UnpinTuple(pointOrder);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCircleContourXld(double row, double column, double radius, double startPhi, double endPhi, string pointOrder, double resolution)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(34);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, startPhi);
			HalconAPI.StoreD(proc, 4, endPhi);
			HalconAPI.StoreS(proc, 5, pointOrder);
			HalconAPI.StoreD(proc, 6, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenEllipseContourXld(HTuple row, HTuple column, HTuple phi, HTuple radius1, HTuple radius2, HTuple startPhi, HTuple endPhi, HTuple pointOrder, double resolution)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(35);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, phi);
			HalconAPI.Store(proc, 3, radius1);
			HalconAPI.Store(proc, 4, radius2);
			HalconAPI.Store(proc, 5, startPhi);
			HalconAPI.Store(proc, 6, endPhi);
			HalconAPI.Store(proc, 7, pointOrder);
			HalconAPI.StoreD(proc, 8, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(radius1);
			HalconAPI.UnpinTuple(radius2);
			HalconAPI.UnpinTuple(startPhi);
			HalconAPI.UnpinTuple(endPhi);
			HalconAPI.UnpinTuple(pointOrder);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenEllipseContourXld(double row, double column, double phi, double radius1, double radius2, double startPhi, double endPhi, string pointOrder, double resolution)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(35);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, radius1);
			HalconAPI.StoreD(proc, 4, radius2);
			HalconAPI.StoreD(proc, 5, startPhi);
			HalconAPI.StoreD(proc, 6, endPhi);
			HalconAPI.StoreS(proc, 7, pointOrder);
			HalconAPI.StoreD(proc, 8, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont AddNoiseWhiteContourXld(int numRegrPoints, double amp)
		{
			IntPtr proc = HalconAPI.PreCall(36);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numRegrPoints);
			HalconAPI.StoreD(proc, 1, amp);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPoly GenPolygonsXld(string type, HTuple alpha)
		{
			IntPtr proc = HalconAPI.PreCall(45);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.Store(proc, 1, alpha);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(alpha);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPoly GenPolygonsXld(string type, double alpha)
		{
			IntPtr proc = HalconAPI.PreCall(45);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ProjectiveTransContourXld(HHomMat2D homMat2D)
		{
			IntPtr proc = HalconAPI.PreCall(47);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont AffineTransContourXld(HHomMat2D homMat2D)
		{
			IntPtr proc = HalconAPI.PreCall(49);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont CloseContoursXld()
		{
			IntPtr proc = HalconAPI.PreCall(50);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ClipEndPointsContoursXld(string mode, HTuple length)
		{
			IntPtr proc = HalconAPI.PreCall(51);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, length);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(length);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ClipEndPointsContoursXld(string mode, double length)
		{
			IntPtr proc = HalconAPI.PreCall(51);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, length);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ClipContoursXld(int row1, int column1, int row2, int column2)
		{
			IntPtr proc = HalconAPI.PreCall(52);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row1);
			HalconAPI.StoreI(proc, 1, column1);
			HalconAPI.StoreI(proc, 2, row2);
			HalconAPI.StoreI(proc, 3, column2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont LocalMaxContoursXld(HImage image, HTuple minPercent, int minDiff, int distance)
		{
			IntPtr proc = HalconAPI.PreCall(53);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, minPercent);
			HalconAPI.StoreI(proc, 1, minDiff);
			HalconAPI.StoreI(proc, 2, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minPercent);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HXLDCont LocalMaxContoursXld(HImage image, int minPercent, int minDiff, int distance)
		{
			IntPtr proc = HalconAPI.PreCall(53);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreI(proc, 0, minPercent);
			HalconAPI.StoreI(proc, 1, minDiff);
			HalconAPI.StoreI(proc, 2, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HXLDCont UnionStraightContoursHistoXld(out HXLDCont selectedContours, int refLineStartRow, int refLineStartColumn, int refLineEndRow, int refLineEndColumn, int width, int maxWidth, int filterSize, out HTuple histoValues)
		{
			IntPtr proc = HalconAPI.PreCall(54);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, refLineStartRow);
			HalconAPI.StoreI(proc, 1, refLineStartColumn);
			HalconAPI.StoreI(proc, 2, refLineEndRow);
			HalconAPI.StoreI(proc, 3, refLineEndColumn);
			HalconAPI.StoreI(proc, 4, width);
			HalconAPI.StoreI(proc, 5, maxWidth);
			HalconAPI.StoreI(proc, 6, filterSize);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HXLDCont.LoadNew(proc, 2, err, out selectedContours);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out histoValues);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont UnionStraightContoursXld(double maxDist, double maxDiff, double percent, string mode, HTuple iterations)
		{
			IntPtr proc = HalconAPI.PreCall(55);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxDist);
			HalconAPI.StoreD(proc, 1, maxDiff);
			HalconAPI.StoreD(proc, 2, percent);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.Store(proc, 4, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(iterations);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont UnionStraightContoursXld(double maxDist, double maxDiff, double percent, string mode, string iterations)
		{
			IntPtr proc = HalconAPI.PreCall(55);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxDist);
			HalconAPI.StoreD(proc, 1, maxDiff);
			HalconAPI.StoreD(proc, 2, percent);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.StoreS(proc, 4, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont UnionCollinearContoursExtXld(double maxDistAbs, double maxDistRel, double maxShift, double maxAngle, double maxOverlap, double maxRegrError, double maxCosts, double weightDist, double weightShift, double weightAngle, double weightLink, double weightRegr, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(56);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxDistAbs);
			HalconAPI.StoreD(proc, 1, maxDistRel);
			HalconAPI.StoreD(proc, 2, maxShift);
			HalconAPI.StoreD(proc, 3, maxAngle);
			HalconAPI.StoreD(proc, 4, maxOverlap);
			HalconAPI.StoreD(proc, 5, maxRegrError);
			HalconAPI.StoreD(proc, 6, maxCosts);
			HalconAPI.StoreD(proc, 7, weightDist);
			HalconAPI.StoreD(proc, 8, weightShift);
			HalconAPI.StoreD(proc, 9, weightAngle);
			HalconAPI.StoreD(proc, 10, weightLink);
			HalconAPI.StoreD(proc, 11, weightRegr);
			HalconAPI.StoreS(proc, 12, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont UnionCollinearContoursXld(double maxDistAbs, double maxDistRel, double maxShift, double maxAngle, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(57);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxDistAbs);
			HalconAPI.StoreD(proc, 1, maxDistRel);
			HalconAPI.StoreD(proc, 2, maxShift);
			HalconAPI.StoreD(proc, 3, maxAngle);
			HalconAPI.StoreS(proc, 4, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont UnionAdjacentContoursXld(double maxDistAbs, double maxDistRel, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(58);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxDistAbs);
			HalconAPI.StoreD(proc, 1, maxDistRel);
			HalconAPI.StoreS(proc, 2, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont SelectContoursXld(string feature, double min1, double max1, double min2, double max2)
		{
			IntPtr proc = HalconAPI.PreCall(59);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, feature);
			HalconAPI.StoreD(proc, 1, min1);
			HalconAPI.StoreD(proc, 2, max1);
			HalconAPI.StoreD(proc, 3, min2);
			HalconAPI.StoreD(proc, 4, max2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetRegressParamsXld(out HTuple nx, out HTuple ny, out HTuple dist, out HTuple fpx, out HTuple fpy, out HTuple lpx, out HTuple lpy, out HTuple mean, out HTuple deviation)
		{
			IntPtr proc = HalconAPI.PreCall(60);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out nx);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out ny);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out dist);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out fpx);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out fpy);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out lpx);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out lpy);
			err = HTuple.LoadNew(proc, 8, HTupleType.DOUBLE, err, out mean);
			err = HTuple.LoadNew(proc, 9, HTupleType.DOUBLE, err, out deviation);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont RegressContoursXld(string mode, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(61);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetContourAngleXld(string angleMode, string calcMode, int lookaround)
		{
			IntPtr proc = HalconAPI.PreCall(62);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, angleMode);
			HalconAPI.StoreS(proc, 1, calcMode);
			HalconAPI.StoreI(proc, 2, lookaround);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont SmoothContoursXld(int numRegrPoints)
		{
			IntPtr proc = HalconAPI.PreCall(63);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numRegrPoints);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ContourPointNumXld()
		{
			IntPtr proc = HalconAPI.PreCall(64);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryContourGlobalAttribsXld()
		{
			IntPtr proc = HalconAPI.PreCall(65);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetContourGlobalAttribXld(HTuple name)
		{
			IntPtr proc = HalconAPI.PreCall(66);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, name);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(name);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetContourGlobalAttribXld(string name)
		{
			IntPtr proc = HalconAPI.PreCall(66);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryContourAttribsXld()
		{
			IntPtr proc = HalconAPI.PreCall(67);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetContourAttribXld(string name)
		{
			IntPtr proc = HalconAPI.PreCall(68);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetContourXld(out HTuple row, out HTuple col)
		{
			IntPtr proc = HalconAPI.PreCall(69);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out col);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenContourPolygonRoundedXld(HTuple row, HTuple col, HTuple radius, HTuple samplingInterval)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(71);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.Store(proc, 3, samplingInterval);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(samplingInterval);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenContourPolygonRoundedXld(HTuple row, HTuple col, HTuple radius, double samplingInterval)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(71);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, samplingInterval);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(radius);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenContourPolygonXld(HTuple row, HTuple col)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(72);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont ObjDiff(HXLDCont objectsSub)
		{
			IntPtr proc = HalconAPI.PreCall(573);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsSub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsSub);
			return result;
		}

		public new HXLDCont CopyObj(int index, int numObj)
		{
			IntPtr proc = HalconAPI.PreCall(583);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.StoreI(proc, 1, numObj);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ConcatObj(HXLDCont objects2)
		{
			IntPtr proc = HalconAPI.PreCall(584);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public new HXLDCont SelectObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDCont SelectObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int CompareObj(HXLDCont objects2, HTuple epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.Store(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(epsilon);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public int CompareObj(HXLDCont objects2, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.StoreD(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public int TestEqualObj(HXLDCont objects2)
		{
			IntPtr proc = HalconAPI.PreCall(591);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public HRegion GenRegionContourXld(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(597);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateAnisoShapeModelXld(HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleRMin, double scaleRMax, HTuple scaleRStep, double scaleCMin, double scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(935);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateAnisoShapeModelXld(int numLevels, double angleStart, double angleExtent, double angleStep, double scaleRMin, double scaleRMax, double scaleRStep, double scaleCMin, double scaleCMax, double scaleCStep, string optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(935);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateScaledShapeModelXld(HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleMin, double scaleMax, HTuple scaleStep, HTuple optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(936);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.Store(proc, 6, scaleStep);
			HalconAPI.Store(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleStep);
			HalconAPI.UnpinTuple(optimization);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateScaledShapeModelXld(int numLevels, double angleStart, double angleExtent, double angleStep, double scaleMin, double scaleMax, double scaleStep, string optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(936);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.StoreD(proc, 6, scaleStep);
			HalconAPI.StoreS(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateShapeModelXld(HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, HTuple optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(937);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.Store(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(optimization);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HShapeModel CreateShapeModelXld(int numLevels, double angleStart, double angleExtent, double angleStep, string optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(937);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HShapeModel result = null;
			err = HShapeModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreateLocalDeformableModelXld(HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(975);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreateLocalDeformableModelXld(int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(975);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreatePlanarCalibDeformableModelXld(HCamPar camParam, HPose referencePose, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(976);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreatePlanarCalibDeformableModelXld(HCamPar camParam, HPose referencePose, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(976);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreatePlanarUncalibDeformableModelXld(HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(977);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HDeformableModel CreatePlanarUncalibDeformableModelXld(int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(977);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLDCont meshes, int gridSpacing, HTuple rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.Store(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HXLDCont.LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLDCont meshes, int gridSpacing, string rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.StoreS(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HXLDCont.LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void DrawNurbsInterpMod(HWindow windowHandle, string rotate, string move, string scale, string keepRatio, string edit, int degree, HTuple rowsIn, HTuple colsIn, HTuple tangentsIn, out HTuple controlRows, out HTuple controlCols, out HTuple knots, out HTuple rows, out HTuple cols, out HTuple tangents)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1318);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreS(proc, 5, edit);
			HalconAPI.StoreI(proc, 6, degree);
			HalconAPI.Store(proc, 7, rowsIn);
			HalconAPI.Store(proc, 8, colsIn);
			HalconAPI.Store(proc, 9, tangentsIn);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowsIn);
			HalconAPI.UnpinTuple(colsIn);
			HalconAPI.UnpinTuple(tangentsIn);
			err = base.Load(proc, 1, err);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out controlRows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out controlCols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out knots);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out tangents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DrawNurbsInterp(HWindow windowHandle, string rotate, string move, string scale, string keepRatio, int degree, out HTuple controlRows, out HTuple controlCols, out HTuple knots, out HTuple rows, out HTuple cols, out HTuple tangents)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1319);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreI(proc, 5, degree);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out controlRows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out controlCols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out knots);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out tangents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DrawNurbsMod(HWindow windowHandle, string rotate, string move, string scale, string keepRatio, string edit, int degree, HTuple rowsIn, HTuple colsIn, HTuple weightsIn, out HTuple rows, out HTuple cols, out HTuple weights)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1320);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreS(proc, 5, edit);
			HalconAPI.StoreI(proc, 6, degree);
			HalconAPI.Store(proc, 7, rowsIn);
			HalconAPI.Store(proc, 8, colsIn);
			HalconAPI.Store(proc, 9, weightsIn);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowsIn);
			HalconAPI.UnpinTuple(colsIn);
			HalconAPI.UnpinTuple(weightsIn);
			err = base.Load(proc, 1, err);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out weights);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DrawNurbs(HWindow windowHandle, string rotate, string move, string scale, string keepRatio, int degree, out HTuple rows, out HTuple cols, out HTuple weights)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1321);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreI(proc, 5, degree);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out weights);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public HXLDCont DrawXldMod(HWindow windowHandle, string rotate, string move, string scale, string keepRatio, string edit)
		{
			IntPtr proc = HalconAPI.PreCall(1322);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreS(proc, 5, edit);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public void DrawXld(HWindow windowHandle, string rotate, string move, string scale, string keepRatio)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1323);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public HXLDCont DistanceContoursXld(HXLDCont contourTo, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1361);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, contourTo);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contourTo);
			return result;
		}

		public HTuple DistanceCcMin(HXLDCont contour2, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1362);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, contour2);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour2);
			return result;
		}

		public void DistanceCc(HXLDCont contour2, string mode, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1363);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, contour2);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour2);
		}

		public void DistanceCc(HXLDCont contour2, string mode, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1363);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, contour2);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour2);
		}

		public void DistanceSc(HTuple row1, HTuple column1, HTuple row2, HTuple column2, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1364);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistanceSc(double row1, double column1, double row2, double column2, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1364);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistanceLc(HTuple row1, HTuple column1, HTuple row2, HTuple column2, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1365);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistanceLc(double row1, double column1, double row2, double column2, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1365);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistancePc(HTuple row, HTuple column, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1366);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistancePc(double row, double column, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1366);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple ReadContourXldDxf(string fileName, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1636);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 1, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string ReadContourXldDxf(string fileName, string genParamName, double genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1636);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void WriteContourXldDxf(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1637);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public new HXLDCont SelectXldPoint(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDCont SelectXldPoint(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDCont SelectShapeXld(HTuple features, string operation, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.Store(proc, 2, min);
			HalconAPI.Store(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDCont SelectShapeXld(string features, string operation, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.StoreD(proc, 2, min);
			HalconAPI.StoreD(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDCont ShapeTransXld(string type)
		{
			IntPtr proc = HalconAPI.PreCall(1689);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont RadialDistortionSelfCalibration(int width, int height, double inlierThreshold, int randSeed, string distortionModel, string distortionCenter, double principalPointVar, out HCamPar cameraParam)
		{
			IntPtr proc = HalconAPI.PreCall(1904);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.StoreD(proc, 2, inlierThreshold);
			HalconAPI.StoreI(proc, 3, randSeed);
			HalconAPI.StoreS(proc, 4, distortionModel);
			HalconAPI.StoreS(proc, 5, distortionCenter);
			HalconAPI.StoreD(proc, 6, principalPointVar);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			err = HCamPar.LoadNew(proc, 0, err, out cameraParam);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ContourToWorldPlaneXld(HTuple cameraParam, HPose worldPose, HTuple scale)
		{
			IntPtr proc = HalconAPI.PreCall(1915);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.Store(proc, 2, scale);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(worldPose);
			HalconAPI.UnpinTuple(scale);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ContourToWorldPlaneXld(HTuple cameraParam, HPose worldPose, string scale)
		{
			IntPtr proc = HalconAPI.PreCall(1915);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.StoreS(proc, 2, scale);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(worldPose);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ChangeRadialDistortionContoursXld(HCamPar camParamIn, HCamPar camParamOut)
		{
			IntPtr proc = HalconAPI.PreCall(1922);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, camParamIn);
			HalconAPI.Store(proc, 1, camParamOut);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamIn);
			HalconAPI.UnpinTuple(camParamOut);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple DistanceCcMinPoints(HXLDCont contour2, string mode, out HTuple row1, out HTuple column1, out HTuple row2, out HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(2111);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, contour2);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out row1);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out column1);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out row2);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour2);
			return result;
		}

		public double DistanceCcMinPoints(HXLDCont contour2, string mode, out double row1, out double column1, out double row2, out double column2)
		{
			IntPtr proc = HalconAPI.PreCall(2111);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, contour2);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out row1);
			err = HalconAPI.LoadD(proc, 2, err, out column1);
			err = HalconAPI.LoadD(proc, 3, err, out row2);
			err = HalconAPI.LoadD(proc, 4, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour2);
			return result;
		}

		public HXLDCont InsertObj(HXLDCont objectsInsert, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2121);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsInsert);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsInsert);
			return result;
		}

		public new HXLDCont RemoveObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDCont RemoveObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDCont ReplaceObj(HXLDCont objectsReplace, HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return result;
		}

		public HXLDCont ReplaceObj(HXLDCont objectsReplace, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return result;
		}
	}
}
