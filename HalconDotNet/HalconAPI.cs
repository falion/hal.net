using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace VisionConfig
{
	[SuppressUnmanagedCodeSecurity]
	public class HalconAPI
	{
		public delegate int HFramegrabberCallback(IntPtr handle, IntPtr userContext, IntPtr context);

		public delegate void HProgressBarCallback(IntPtr id, string operatorName, double progress, string message);

		public delegate void HLowLevelErrorCallback(string err);

		public delegate void HClearProcCallBack(IntPtr ptr);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public delegate IntPtr HDevThreadInternalCallback(IntPtr devThread);

		private const string HalconDLL = "opencv_world400";

		private const CallingConvention HalconCall = CallingConvention.Cdecl;

		internal const int H_MSG_OK = 2;

		internal const int H_MSG_TRUE = 2;

		internal const int H_MSG_FALSE = 3;

		internal const int H_MSG_VOID = 4;

		internal const int H_MSG_FAIL = 5;

		public static readonly bool isPlatform64 = IntPtr.Size > 4;

		public static readonly bool isWindows = HalconAPI.testWindows();

		private HalconAPI()
		{
		}

		private static bool testWindows()
		{
			int platform = (int)Environment.OSVersion.Platform;
			if (platform != 4)
			{
				return platform != 128;
			}
			return false;
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIDoLicenseError")]
		public static extern void DoLicenseError(bool state);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIUseSpinLock")]
		public static extern void UseSpinLock(bool state);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIStartUpThreadPool")]
		public static extern void StartUpThreadPool(bool state);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICancelDraw")]
		public static extern void CancelDraw();

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIIsUTF8Encoding")]
		private static extern bool IsUTF8Encoding();

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIGetSerializedSize(IntPtr ptr, out ulong size);

		internal static int GetSerializedSize(byte[] header, out ulong size)
		{
			GCHandle gCHandle = GCHandle.Alloc(header, GCHandleType.Pinned);
			IntPtr ptr = gCHandle.AddrOfPinnedObject();
			int result = HalconAPI.HLIGetSerializedSize(ptr, out size);
			gCHandle.Free();
			return result;
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLILock")]
		internal static extern void Lock();

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIUnlock")]
		internal static extern void Unlock();

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXCreateHThreadContext(out IntPtr context);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXClearHThreadContext(IntPtr context);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXCreateHThread(IntPtr contextHandle, out IntPtr threadHandle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXClearHThread(IntPtr threadHandle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXExitHThread(IntPtr threadHandle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXStartHThreadDotNet(IntPtr threadHandle, HDevThreadInternalCallback proc, IntPtr data, out IntPtr threadId);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXPrepareDirectCall(IntPtr threadHandle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXJoinHThread(IntPtr threadId);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXThreadLockLocalVar(IntPtr threadHandle, out IntPtr referenceCount);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXThreadUnlockLocalVar(IntPtr threadHandle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXThreadLockGlobalVar(IntPtr threadHandle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXThreadUnlockGlobalVar(IntPtr threadHandle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICreateProcedure")]
		private static extern int CreateProcedure(int procIndex, out IntPtr proc);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICallProcedure")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int CallProcedure(IntPtr proc);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIDestroyProcedure")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int DestroyProcedure(IntPtr proc, int procResult);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		private static extern IntPtr HLIGetLogicalName(IntPtr proc);

		internal static string GetLogicalName(IntPtr proc)
		{
			return Marshal.PtrToStringAnsi(HalconAPI.HLIGetLogicalName(proc));
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLILogicalName")]
		private static extern IntPtr HLIGetLogicalName(int procIndex);

		internal static string GetLogicalName(int procIndex)
		{
			return Marshal.PtrToStringAnsi(HalconAPI.HLIGetLogicalName(procIndex));
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetProcIndex")]
		private static extern int GetProcIndex(IntPtr proc);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIGetErrorMessage(int err, IntPtr buffer);

		internal static string GetErrorMessage(int err)
		{
			IntPtr intPtr = Marshal.AllocHGlobal(1024);
			HalconAPI.HLIGetErrorMessage(err, intPtr);
			string result = HalconAPI.FromHalconEncoding(intPtr);
			Marshal.FreeHGlobal(intPtr);
			return result;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IntPtr PreCall(int procIndex)
		{
			IntPtr result = default(IntPtr);
			int num = HalconAPI.CreateProcedure(procIndex, out result);
			if (num != 2)
			{
				HOperatorException.throwInfo(num, "Could not create a new operator instance for id " + procIndex);
			}
			return result;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void PostCall(IntPtr proc, int procResult)
		{
			int procIndex = HalconAPI.GetProcIndex(proc);
			HalconAPI.HLIClearAllIOCT(proc);
			int err = HalconAPI.DestroyProcedure(proc, procResult);
			if (procIndex >= 0)
			{
				HOperatorException.throwOperator(err, procIndex);
				HOperatorException.throwOperator(procResult, procIndex);
			}
			else
			{
				HOperatorException.throwOperator(err, "Unknown");
				HOperatorException.throwOperator(procResult, "Unknown");
			}
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetInputObject")]
		internal static extern int SetInputObject(IntPtr proc, int parIndex, IntPtr key);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetOutputObject")]
		internal static extern int GetOutputObject(IntPtr proc, int parIndex, out IntPtr key);

		internal static void ClearObject(IntPtr key)
		{
			IntPtr proc = HalconAPI.PreCall(585);
			HalconAPI.HCkP(proc, HalconAPI.SetInputObject(proc, 1, key));
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLICopyObject(IntPtr keyIn, out IntPtr keyOut);

		internal static IntPtr CopyObject(IntPtr key)
		{
			IntPtr proc = HalconAPI.PreCall(583);
			HalconAPI.HCkP(proc, HalconAPI.SetInputObject(proc, 1, key));
			HalconAPI.StoreI(proc, 0, 1);
			HalconAPI.StoreI(proc, 1, -1);
			int num = HalconAPI.CallProcedure(proc);
			if (!HalconAPI.IsFailure(num))
			{
				num = HalconAPI.GetOutputObject(proc, 1, out key);
			}
			HalconAPI.PostCall(proc, num);
			return key;
		}

		internal static string GetObjClass(IntPtr key)
		{
			HTuple hTuple = "object";
			IntPtr proc = HalconAPI.PreCall(594);
			HalconAPI.HCkP(proc, HalconAPI.SetInputObject(proc, 1, key));
			HalconAPI.InitOCT(proc, 0);
			int num = HalconAPI.CallProcedure(proc);
			if (!HalconAPI.IsFailure(num))
			{
				num = HTuple.LoadNew(proc, 0, num, out hTuple);
			}
			HalconAPI.PostCall(proc, num);
			if (hTuple.Length <= 0)
			{
				return "any";
			}
			return hTuple.S;
		}

		internal static void AssertObjectClass(IntPtr key, string assertClass)
		{
			if (key != HObjectBase.UNDEF)
			{
				string objClass = HalconAPI.GetObjClass(key);
				if (!objClass.StartsWith(assertClass) && objClass != "any")
				{
					throw new HalconException("Iconic object type mismatch (expected " + assertClass + ", got " + objClass + ")");
				}
			}
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICreateTuple")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int CreateTuple(out IntPtr tuple);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIInitOCT")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int InitOCT(IntPtr proc, int parIndex);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int HLIClearAllIOCT(IntPtr proc);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIDestroyTuple")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int DestroyTuple(IntPtr tuple);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreTuple(IntPtr tupleHandle, HTuple tuple)
		{
			HTupleType type = (tuple.Type == HTupleType.LONG) ? HTupleType.INTEGER : tuple.Type;
			HalconAPI.HCkH(HalconAPI.CreateElementsOfType(tupleHandle, tuple.Length, type));
			switch (tuple.Type)
			{
			case HTupleType.INTEGER:
				HalconAPI.HCkH(HalconAPI.SetIArr(tupleHandle, tuple.IArr));
				break;
			case HTupleType.LONG:
				HalconAPI.HCkH(HalconAPI.SetLArr(tupleHandle, tuple.LArr));
				break;
			case HTupleType.DOUBLE:
				HalconAPI.HCkH(HalconAPI.SetDArr(tupleHandle, tuple.DArr));
				break;
			case HTupleType.STRING:
			{
				string[] sArr = tuple.SArr;
				for (int j = 0; j < tuple.Length; j++)
				{
					HalconAPI.HCkH(HalconAPI.SetS(tupleHandle, j, sArr[j]));
				}
				break;
			}
			case HTupleType.MIXED:
			{
				object[] oArr = tuple.data.OArr;
				for (int i = 0; i < tuple.Length; i++)
				{
					switch (HTupleImplementation.GetObjectType(oArr[i]))
					{
					case 1:
						HalconAPI.HCkH(HalconAPI.SetI(tupleHandle, i, (int)oArr[i]));
						break;
					case 129:
						HalconAPI.HCkH(HalconAPI.SetL(tupleHandle, i, (long)oArr[i]));
						break;
					case 2:
						HalconAPI.HCkH(HalconAPI.SetD(tupleHandle, i, (double)oArr[i]));
						break;
					case 4:
						HalconAPI.HCkH(HalconAPI.SetS(tupleHandle, i, (string)oArr[i]));
						break;
					}
				}
				break;
			}
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static HTuple LoadTuple(IntPtr tupleHandle)
		{
			HTupleImplementation data = null;
			HTupleImplementation.LoadData(tupleHandle, HTupleType.MIXED, out data);
			return new HTuple(data);
		}

		private static void HCkH(int err)
		{
			if (HalconAPI.IsFailure(err))
			{
				throw new HOperatorException(err);
			}
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetInputTuple")]
		internal static extern int GetInputTuple(IntPtr proc, int parIndex, out IntPtr tuple);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICreateElements")]
		internal static extern int CreateElements(IntPtr tuple, int length);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICreateElementsOfType")]
		internal static extern int CreateElementsOfType(IntPtr tuple, int length, HTupleType type);

		internal static int CreateInputTuple(IntPtr proc, int parIndex, int length, out IntPtr tuple)
		{
			int inputTuple = HalconAPI.GetInputTuple(proc, parIndex, out tuple);
			if (!HalconAPI.IsFailure(inputTuple))
			{
				return HalconAPI.CreateElements(tuple, length);
			}
			return inputTuple;
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetOutputTuple")]
		internal static extern int GetOutputTuple(IntPtr proc, int parIndex, out IntPtr tuple);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetTupleLength")]
		internal static extern int GetTupleLength(IntPtr tuple, out int length);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetTupleTypeScanElem")]
		internal static extern int GetTupleTypeScanElem(IntPtr tuple, out int type);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetElementType")]
		internal static extern int GetElementType(IntPtr tuple, int index, out HTupleType type);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetI")]
		internal static extern int SetI(IntPtr tuple, int index, int intValue);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetL")]
		internal static extern int SetL(IntPtr tuple, int index, long longValue);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetD")]
		internal static extern int SetD(IntPtr tuple, int index, double doubleValue);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HLISetS(IntPtr tuple, int index, IntPtr stringValue);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IntPtr ToHalconHGlobalEncoding(string dotnet)
		{
			if (!HalconAPI.IsUTF8Encoding())
			{
				return Marshal.StringToHGlobalAnsi(dotnet);
			}
			return HalconAPI.ToHGlobalUtf8Encoding(dotnet);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IntPtr ToHGlobalUtf8Encoding(string dotnet)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(dotnet);
			int num = Marshal.SizeOf(bytes.GetType().GetElementType()) * bytes.Length;
			IntPtr intPtr = Marshal.AllocHGlobal(num + 1);
			Marshal.Copy(bytes, 0, intPtr, bytes.Length);
			Marshal.WriteByte(intPtr, num, 0);
			return intPtr;
		}

		internal static int SetS(IntPtr tuple, int index, string dotnet_string)
		{
			IntPtr intPtr = HalconAPI.ToHalconHGlobalEncoding(dotnet_string);
			int result = HalconAPI.HLISetS(tuple, index, intPtr);
			Marshal.FreeHGlobal(intPtr);
			return result;
		}

		internal static int SetIP(IntPtr tuple, int index, IntPtr intPtrValue)
		{
			if (HalconAPI.isPlatform64)
			{
				return HalconAPI.SetL(tuple, index, intPtrValue.ToInt64());
			}
			return HalconAPI.SetI(tuple, index, intPtrValue.ToInt32());
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreI(IntPtr proc, int parIndex, int intValue)
		{
			IntPtr tuple = default(IntPtr);
			HalconAPI.HCkP(proc, HalconAPI.CreateInputTuple(proc, parIndex, 1, out tuple));
			HalconAPI.SetI(tuple, 0, intValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreL(IntPtr proc, int parIndex, long longValue)
		{
			IntPtr tuple = default(IntPtr);
			HalconAPI.HCkP(proc, HalconAPI.CreateInputTuple(proc, parIndex, 1, out tuple));
			HalconAPI.SetL(tuple, 0, longValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreD(IntPtr proc, int parIndex, double doubleValue)
		{
			IntPtr tuple = default(IntPtr);
			HalconAPI.HCkP(proc, HalconAPI.CreateInputTuple(proc, parIndex, 1, out tuple));
			HalconAPI.SetD(tuple, 0, doubleValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreS(IntPtr proc, int parIndex, string stringValue)
		{
			if (stringValue == null)
			{
				stringValue = "";
			}
			IntPtr tuple = default(IntPtr);
			HalconAPI.HCkP(proc, HalconAPI.CreateInputTuple(proc, parIndex, 1, out tuple));
			HalconAPI.HCkP(proc, HalconAPI.SetS(tuple, 0, stringValue));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreIP(IntPtr proc, int parIndex, IntPtr intPtrValue)
		{
			IntPtr tuple = default(IntPtr);
			HalconAPI.HCkP(proc, HalconAPI.CreateInputTuple(proc, parIndex, 1, out tuple));
			HalconAPI.SetIP(tuple, 0, intPtrValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void Store(IntPtr proc, int parIndex, HTuple tupleValue)
		{
			if (tupleValue == null)
			{
				tupleValue = new HTuple();
			}
			tupleValue.Store(proc, parIndex);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void Store(IntPtr proc, int parIndex, HObjectBase objectValue)
		{
			if (objectValue == null)
			{
				objectValue = new HObjectBase();
			}
			objectValue.Store(proc, parIndex);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void Store(IntPtr proc, int parIndex, HTool toolValue)
		{
			if (toolValue == null)
			{
				HalconAPI.StoreIP(proc, parIndex, HTool.UNDEF);
			}
			else
			{
				toolValue.Store(proc, parIndex);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void Store(IntPtr proc, int parIndex, HTool[] tools)
		{
			if (tools == null)
			{
				tools = new HTool[0];
			}
			HalconAPI.Store(proc, parIndex, HTool.ConcatArray(tools));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void Store(IntPtr proc, int parIndex, HData data)
		{
			if (data == null)
			{
				data = new HData();
			}
			data.Store(proc, parIndex);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void Store(IntPtr proc, int parIndex, HData[] data)
		{
			if (data == null)
			{
				data = new HData[0];
			}
			HalconAPI.Store(proc, parIndex, HData.ConcatArray(data));
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetIArr")]
		internal static extern int SetIArr(IntPtr tuple, int[] intArray);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetIArrPtr")]
		internal static extern int SetIArrPtr(IntPtr tuple, int[] intArray, int length);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetLArr")]
		internal static extern int SetLArr(IntPtr tuple, long[] longArray);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetLArrPtr")]
		internal static extern int SetLArrPtr(IntPtr tuple, long[] longArray, int length);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetDArr")]
		internal static extern int SetDArr(IntPtr tuple, double[] doubleArray);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetDArrPtr")]
		internal static extern int SetDArrPtr(IntPtr tuple, double[] doubleArray, int length);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetI")]
		internal static extern int GetI(IntPtr tuple, int index, out int intValue);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetL")]
		internal static extern int GetL(IntPtr tuple, int index, out long longValue);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetD")]
		internal static extern int GetD(IntPtr tuple, int index, out double doubleValue);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIGetS(IntPtr tuple, int index, out IntPtr stringPtr);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static string FromHalconEncoding(IntPtr halcon)
		{
			if (HalconAPI.IsUTF8Encoding())
			{
				int i;
				for (i = 0; Marshal.ReadByte(halcon, i) != 0; i++)
				{
				}
				byte[] array = new byte[i];
				Marshal.Copy(halcon, array, 0, array.Length);
				return Encoding.UTF8.GetString(array);
			}
			return Marshal.PtrToStringAnsi(halcon);
		}

		internal static int GetS(IntPtr tuple, int index, out string stringValue)
		{
			stringValue = string.Empty;
			IntPtr halcon = default(IntPtr);
			int num = HalconAPI.HLIGetS(tuple, index, out halcon);
			if (num != 2)
			{
				return num;
			}
			stringValue = HalconAPI.FromHalconEncoding(halcon);
			if (stringValue == null)
			{
				stringValue = "";
				return 5;
			}
			return 2;
		}

		internal static int GetIP(IntPtr tuple, int index, out IntPtr intPtrValue)
		{
			int result;
			if (HalconAPI.isPlatform64)
			{
				long value = 0L;
				result = HalconAPI.GetL(tuple, index, out value);
				intPtrValue = new IntPtr(value);
			}
			else
			{
				int value2 = 0;
				result = HalconAPI.GetI(tuple, index, out value2);
				intPtrValue = new IntPtr(value2);
			}
			return result;
		}

		private static int HCkSingle(IntPtr tuple, HTupleType expectedType)
		{
			int num = 0;
			if (tuple != IntPtr.Zero)
			{
				HalconAPI.GetTupleLength(tuple, out num);
			}
			if (num > 0)
			{
				HTupleType hTupleType = (HTupleType)0;
				HalconAPI.GetElementType(tuple, 0, out hTupleType);
				if (hTupleType != expectedType)
				{
					return 7002;
				}
				return 2;
			}
			return 7001;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadI(IntPtr proc, int parIndex, int err, out int intValue)
		{
			if (HalconAPI.IsFailure(err))
			{
				intValue = -1;
				return err;
			}
			IntPtr zero = IntPtr.Zero;
			HalconAPI.GetOutputTuple(proc, parIndex, out zero);
			err = HalconAPI.HCkSingle(zero, HTupleType.INTEGER);
			if (err != 2)
			{
				err = HalconAPI.HCkSingle(zero, HTupleType.DOUBLE);
				if (err != 2)
				{
					intValue = -1;
					return err;
				}
				double num = -1.0;
				err = HalconAPI.GetD(zero, 0, out num);
				intValue = (int)num;
				return err;
			}
			return HalconAPI.GetI(zero, 0, out intValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadL(IntPtr proc, int parIndex, int err, out long longValue)
		{
			if (HalconAPI.IsFailure(err))
			{
				longValue = -1L;
				return err;
			}
			IntPtr zero = IntPtr.Zero;
			HalconAPI.GetOutputTuple(proc, parIndex, out zero);
			err = HalconAPI.HCkSingle(zero, HTupleType.INTEGER);
			if (err != 2)
			{
				err = HalconAPI.HCkSingle(zero, HTupleType.DOUBLE);
				if (err != 2)
				{
					longValue = -1L;
					return err;
				}
				double num = -1.0;
				err = HalconAPI.GetD(zero, 0, out num);
				longValue = (long)num;
				return err;
			}
			return HalconAPI.GetL(zero, 0, out longValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadD(IntPtr proc, int parIndex, int err, out double doubleValue)
		{
			if (HalconAPI.IsFailure(err))
			{
				doubleValue = -1.0;
				return err;
			}
			IntPtr zero = IntPtr.Zero;
			HalconAPI.GetOutputTuple(proc, parIndex, out zero);
			err = HalconAPI.HCkSingle(zero, HTupleType.DOUBLE);
			if (err != 2)
			{
				err = HalconAPI.HCkSingle(zero, HTupleType.INTEGER);
				if (err != 2)
				{
					doubleValue = -1.0;
					return err;
				}
				int num = -1;
				err = HalconAPI.GetI(zero, 0, out num);
				doubleValue = (double)num;
				return err;
			}
			return HalconAPI.GetD(zero, 0, out doubleValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadS(IntPtr proc, int parIndex, int err, out string stringValue)
		{
			if (HalconAPI.IsFailure(err))
			{
				stringValue = "";
				return err;
			}
			IntPtr zero = IntPtr.Zero;
			HalconAPI.GetOutputTuple(proc, parIndex, out zero);
			err = HalconAPI.HCkSingle(zero, HTupleType.STRING);
			if (err != 2)
			{
				stringValue = "";
				return err;
			}
			return HalconAPI.GetS(zero, 0, out stringValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadIP(IntPtr proc, int parIndex, int err, out IntPtr intPtrValue)
		{
			if (HalconAPI.IsFailure(err))
			{
				intPtrValue = IntPtr.Zero;
				return err;
			}
			IntPtr tuple = default(IntPtr);
			HalconAPI.GetOutputTuple(proc, parIndex, out tuple);
			err = HalconAPI.HCkSingle(tuple, HTupleType.INTEGER);
			if (err != 2)
			{
				intPtrValue = IntPtr.Zero;
				return err;
			}
			return HalconAPI.GetIP(tuple, 0, out intPtrValue);
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetIArr")]
		internal static extern int GetIArr(IntPtr tuple, [Out] int[] intArray);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetLArr")]
		internal static extern int GetLArr(IntPtr tuple, [Out] long[] longArray);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetDArr")]
		internal static extern int GetDArr(IntPtr tuple, [Out] double[] doubleArray);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void UnpinTuple(HTuple tuple)
		{
			if (tuple != null)
			{
				tuple.UnpinTuple();
			}
		}

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackPush(long win_handle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackPop(out long win_handle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackGetActive(out long win_handle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackSetActive(long win_handle);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackIsOpen(out bool is_open);

		[DllImport("opencv_world400", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackCloseAll();

		internal static bool IsError(int err)
		{
			return err >= 1000;
		}

		internal static bool IsFailure(int err)
		{
			if (err != 2)
			{
				return err != 2;
			}
			return false;
		}

		internal static void HCkP(IntPtr proc, int err)
		{
			if (HalconAPI.IsFailure(err))
			{
				HalconAPI.PostCall(proc, err);
			}
		}
	}
}
