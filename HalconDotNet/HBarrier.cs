using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HBarrier : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBarrier()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBarrier(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HBarrier obj)
		{
			obj = new HBarrier(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HBarrier[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HBarrier[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HBarrier(hTuple[i].IP);
			}
			return err;
		}

		public HBarrier(HTuple attribName, HTuple attribValue, int teamSize)
		{
			IntPtr proc = HalconAPI.PreCall(552);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.StoreI(proc, 2, teamSize);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HBarrier(string attribName, string attribValue, int teamSize)
		{
			IntPtr proc = HalconAPI.PreCall(552);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.StoreI(proc, 2, teamSize);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WaitBarrier()
		{
			IntPtr proc = HalconAPI.PreCall(551);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateBarrier(HTuple attribName, HTuple attribValue, int teamSize)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(552);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.StoreI(proc, 2, teamSize);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateBarrier(string attribName, string attribValue, int teamSize)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(552);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.StoreI(proc, 2, teamSize);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(550);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
