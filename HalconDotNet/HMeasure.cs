using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HMeasure : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMeasure()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMeasure(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMeasure obj)
		{
			obj = new HMeasure(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMeasure[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HMeasure[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HMeasure(hTuple[i].IP);
			}
			return err;
		}

		public HMeasure(HTuple centerRow, HTuple centerCol, HTuple radius, HTuple angleStart, HTuple angleExtent, HTuple annulusRadius, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(838);
			HalconAPI.Store(proc, 0, centerRow);
			HalconAPI.Store(proc, 1, centerCol);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, annulusRadius);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(centerRow);
			HalconAPI.UnpinTuple(centerCol);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(annulusRadius);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMeasure(double centerRow, double centerCol, double radius, double angleStart, double angleExtent, double annulusRadius, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(838);
			HalconAPI.StoreD(proc, 0, centerRow);
			HalconAPI.StoreD(proc, 1, centerCol);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, angleStart);
			HalconAPI.StoreD(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, annulusRadius);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMeasure(HTuple row, HTuple column, HTuple phi, HTuple length1, HTuple length2, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(839);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, phi);
			HalconAPI.Store(proc, 3, length1);
			HalconAPI.Store(proc, 4, length2);
			HalconAPI.StoreI(proc, 5, width);
			HalconAPI.StoreI(proc, 6, height);
			HalconAPI.StoreS(proc, 7, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(length1);
			HalconAPI.UnpinTuple(length2);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMeasure(double row, double column, double phi, double length1, double length2, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(839);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, length1);
			HalconAPI.StoreD(proc, 4, length2);
			HalconAPI.StoreI(proc, 5, width);
			HalconAPI.StoreI(proc, 6, height);
			HalconAPI.StoreS(proc, 7, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeMeasure();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMeasure(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeMeasure(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeMeasure().Serialize(stream);
		}

		public static HMeasure Deserialize(Stream stream)
		{
			HMeasure hMeasure = new HMeasure();
			hMeasure.DeserializeMeasure(HSerializedItem.Deserialize(stream));
			return hMeasure;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HMeasure Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeMeasure();
			HMeasure hMeasure = new HMeasure();
			hMeasure.DeserializeMeasure(hSerializedItem);
			hSerializedItem.Dispose();
			return hMeasure;
		}

		public HSerializedItem SerializeMeasure()
		{
			IntPtr proc = HalconAPI.PreCall(821);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializeMeasure(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(822);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public void WriteMeasure(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(823);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadMeasure(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(824);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void MeasureThresh(HImage image, double sigma, double threshold, string select, out HTuple rowThresh, out HTuple columnThresh, out HTuple distance)
		{
			IntPtr proc = HalconAPI.PreCall(825);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.StoreS(proc, 3, select);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowThresh);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnThresh);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out distance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HTuple MeasureProjection(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(828);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void ResetFuzzyMeasure(string setType)
		{
			IntPtr proc = HalconAPI.PreCall(829);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, setType);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetFuzzyMeasureNormPair(HTuple pairSize, string setType, HFunction1D function)
		{
			IntPtr proc = HalconAPI.PreCall(830);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, pairSize);
			HalconAPI.StoreS(proc, 2, setType);
			HalconAPI.Store(proc, 3, function);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pairSize);
			HalconAPI.UnpinTuple(function);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetFuzzyMeasureNormPair(double pairSize, string setType, HFunction1D function)
		{
			IntPtr proc = HalconAPI.PreCall(830);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, pairSize);
			HalconAPI.StoreS(proc, 2, setType);
			HalconAPI.Store(proc, 3, function);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(function);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetFuzzyMeasure(string setType, HFunction1D function)
		{
			IntPtr proc = HalconAPI.PreCall(831);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, setType);
			HalconAPI.Store(proc, 2, function);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(function);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void FuzzyMeasurePairing(HImage image, double sigma, double ampThresh, double fuzzyThresh, string transition, string pairing, int numPairs, out HTuple rowEdgeFirst, out HTuple columnEdgeFirst, out HTuple amplitudeFirst, out HTuple rowEdgeSecond, out HTuple columnEdgeSecond, out HTuple amplitudeSecond, out HTuple rowPairCenter, out HTuple columnPairCenter, out HTuple fuzzyScore, out HTuple intraDistance)
		{
			IntPtr proc = HalconAPI.PreCall(832);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, ampThresh);
			HalconAPI.StoreD(proc, 3, fuzzyThresh);
			HalconAPI.StoreS(proc, 4, transition);
			HalconAPI.StoreS(proc, 5, pairing);
			HalconAPI.StoreI(proc, 6, numPairs);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowEdgeFirst);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnEdgeFirst);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out amplitudeFirst);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rowEdgeSecond);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out columnEdgeSecond);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out amplitudeSecond);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out rowPairCenter);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out columnPairCenter);
			err = HTuple.LoadNew(proc, 8, HTupleType.DOUBLE, err, out fuzzyScore);
			err = HTuple.LoadNew(proc, 9, HTupleType.DOUBLE, err, out intraDistance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void FuzzyMeasurePairs(HImage image, double sigma, double ampThresh, double fuzzyThresh, string transition, out HTuple rowEdgeFirst, out HTuple columnEdgeFirst, out HTuple amplitudeFirst, out HTuple rowEdgeSecond, out HTuple columnEdgeSecond, out HTuple amplitudeSecond, out HTuple rowEdgeCenter, out HTuple columnEdgeCenter, out HTuple fuzzyScore, out HTuple intraDistance, out HTuple interDistance)
		{
			IntPtr proc = HalconAPI.PreCall(833);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, ampThresh);
			HalconAPI.StoreD(proc, 3, fuzzyThresh);
			HalconAPI.StoreS(proc, 4, transition);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			HalconAPI.InitOCT(proc, 10);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowEdgeFirst);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnEdgeFirst);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out amplitudeFirst);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rowEdgeSecond);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out columnEdgeSecond);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out amplitudeSecond);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out rowEdgeCenter);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out columnEdgeCenter);
			err = HTuple.LoadNew(proc, 8, HTupleType.DOUBLE, err, out fuzzyScore);
			err = HTuple.LoadNew(proc, 9, HTupleType.DOUBLE, err, out intraDistance);
			err = HTuple.LoadNew(proc, 10, HTupleType.DOUBLE, err, out interDistance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void FuzzyMeasurePos(HImage image, double sigma, double ampThresh, double fuzzyThresh, string transition, out HTuple rowEdge, out HTuple columnEdge, out HTuple amplitude, out HTuple fuzzyScore, out HTuple distance)
		{
			IntPtr proc = HalconAPI.PreCall(834);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, ampThresh);
			HalconAPI.StoreD(proc, 3, fuzzyThresh);
			HalconAPI.StoreS(proc, 4, transition);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowEdge);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnEdge);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out amplitude);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out fuzzyScore);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out distance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void MeasurePairs(HImage image, double sigma, double threshold, string transition, string select, out HTuple rowEdgeFirst, out HTuple columnEdgeFirst, out HTuple amplitudeFirst, out HTuple rowEdgeSecond, out HTuple columnEdgeSecond, out HTuple amplitudeSecond, out HTuple intraDistance, out HTuple interDistance)
		{
			IntPtr proc = HalconAPI.PreCall(835);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.StoreS(proc, 3, transition);
			HalconAPI.StoreS(proc, 4, select);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowEdgeFirst);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnEdgeFirst);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out amplitudeFirst);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rowEdgeSecond);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out columnEdgeSecond);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out amplitudeSecond);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out intraDistance);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out interDistance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void MeasurePos(HImage image, double sigma, double threshold, string transition, string select, out HTuple rowEdge, out HTuple columnEdge, out HTuple amplitude, out HTuple distance)
		{
			IntPtr proc = HalconAPI.PreCall(836);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.StoreS(proc, 3, transition);
			HalconAPI.StoreS(proc, 4, select);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowEdge);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out columnEdge);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out amplitude);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out distance);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void TranslateMeasure(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(837);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TranslateMeasure(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(837);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GenMeasureArc(HTuple centerRow, HTuple centerCol, HTuple radius, HTuple angleStart, HTuple angleExtent, HTuple annulusRadius, int width, int height, string interpolation)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(838);
			HalconAPI.Store(proc, 0, centerRow);
			HalconAPI.Store(proc, 1, centerCol);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, annulusRadius);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(centerRow);
			HalconAPI.UnpinTuple(centerCol);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(annulusRadius);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenMeasureArc(double centerRow, double centerCol, double radius, double angleStart, double angleExtent, double annulusRadius, int width, int height, string interpolation)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(838);
			HalconAPI.StoreD(proc, 0, centerRow);
			HalconAPI.StoreD(proc, 1, centerCol);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, angleStart);
			HalconAPI.StoreD(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, annulusRadius);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenMeasureRectangle2(HTuple row, HTuple column, HTuple phi, HTuple length1, HTuple length2, int width, int height, string interpolation)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(839);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, phi);
			HalconAPI.Store(proc, 3, length1);
			HalconAPI.Store(proc, 4, length2);
			HalconAPI.StoreI(proc, 5, width);
			HalconAPI.StoreI(proc, 6, height);
			HalconAPI.StoreS(proc, 7, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(length1);
			HalconAPI.UnpinTuple(length2);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenMeasureRectangle2(double row, double column, double phi, double length1, double length2, int width, int height, string interpolation)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(839);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, length1);
			HalconAPI.StoreD(proc, 4, length2);
			HalconAPI.StoreI(proc, 5, width);
			HalconAPI.StoreI(proc, 6, height);
			HalconAPI.StoreS(proc, 7, interpolation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(827);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
