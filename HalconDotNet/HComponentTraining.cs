using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HComponentTraining : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComponentTraining()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComponentTraining(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HComponentTraining obj)
		{
			obj = new HComponentTraining(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HComponentTraining[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HComponentTraining[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HComponentTraining(hTuple[i].IP);
			}
			return err;
		}

		public HComponentTraining(HImage modelImage, HRegion initialComponents, HImage trainingImages, out HRegion modelComponents, HTuple contrastLow, HTuple contrastHigh, HTuple minSize, HTuple minScore, HTuple searchRowTol, HTuple searchColumnTol, HTuple searchAngleTol, string trainingEmphasis, string ambiguityCriterion, double maxContourOverlap, double clusterThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(1017);
			HalconAPI.Store(proc, 1, modelImage);
			HalconAPI.Store(proc, 2, initialComponents);
			HalconAPI.Store(proc, 3, trainingImages);
			HalconAPI.Store(proc, 0, contrastLow);
			HalconAPI.Store(proc, 1, contrastHigh);
			HalconAPI.Store(proc, 2, minSize);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, searchRowTol);
			HalconAPI.Store(proc, 5, searchColumnTol);
			HalconAPI.Store(proc, 6, searchAngleTol);
			HalconAPI.StoreS(proc, 7, trainingEmphasis);
			HalconAPI.StoreS(proc, 8, ambiguityCriterion);
			HalconAPI.StoreD(proc, 9, maxContourOverlap);
			HalconAPI.StoreD(proc, 10, clusterThreshold);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(contrastLow);
			HalconAPI.UnpinTuple(contrastHigh);
			HalconAPI.UnpinTuple(minSize);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(searchRowTol);
			HalconAPI.UnpinTuple(searchColumnTol);
			HalconAPI.UnpinTuple(searchAngleTol);
			err = base.Load(proc, 0, err);
			err = HRegion.LoadNew(proc, 1, err, out modelComponents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelImage);
			GC.KeepAlive(initialComponents);
			GC.KeepAlive(trainingImages);
		}

		public HComponentTraining(HImage modelImage, HRegion initialComponents, HImage trainingImages, out HRegion modelComponents, int contrastLow, int contrastHigh, int minSize, double minScore, int searchRowTol, int searchColumnTol, double searchAngleTol, string trainingEmphasis, string ambiguityCriterion, double maxContourOverlap, double clusterThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(1017);
			HalconAPI.Store(proc, 1, modelImage);
			HalconAPI.Store(proc, 2, initialComponents);
			HalconAPI.Store(proc, 3, trainingImages);
			HalconAPI.StoreI(proc, 0, contrastLow);
			HalconAPI.StoreI(proc, 1, contrastHigh);
			HalconAPI.StoreI(proc, 2, minSize);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, searchRowTol);
			HalconAPI.StoreI(proc, 5, searchColumnTol);
			HalconAPI.StoreD(proc, 6, searchAngleTol);
			HalconAPI.StoreS(proc, 7, trainingEmphasis);
			HalconAPI.StoreS(proc, 8, ambiguityCriterion);
			HalconAPI.StoreD(proc, 9, maxContourOverlap);
			HalconAPI.StoreD(proc, 10, clusterThreshold);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			err = HRegion.LoadNew(proc, 1, err, out modelComponents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelImage);
			GC.KeepAlive(initialComponents);
			GC.KeepAlive(trainingImages);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeTrainingComponents();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComponentTraining(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeTrainingComponents(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeTrainingComponents().Serialize(stream);
		}

		public static HComponentTraining Deserialize(Stream stream)
		{
			HComponentTraining hComponentTraining = new HComponentTraining();
			hComponentTraining.DeserializeTrainingComponents(HSerializedItem.Deserialize(stream));
			return hComponentTraining;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HComponentTraining Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeTrainingComponents();
			HComponentTraining hComponentTraining = new HComponentTraining();
			hComponentTraining.DeserializeTrainingComponents(hSerializedItem);
			hSerializedItem.Dispose();
			return hComponentTraining;
		}

		public HComponentModel CreateTrainedComponentModel(double angleStart, double angleExtent, HTuple minContrastComp, HTuple minScoreComp, HTuple numLevelsComp, HTuple angleStepComp, string optimizationComp, HTuple metricComp, HTuple pregenerationComp, out HTuple rootRanking)
		{
			IntPtr proc = HalconAPI.PreCall(1005);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, minContrastComp);
			HalconAPI.Store(proc, 4, minScoreComp);
			HalconAPI.Store(proc, 5, numLevelsComp);
			HalconAPI.Store(proc, 6, angleStepComp);
			HalconAPI.StoreS(proc, 7, optimizationComp);
			HalconAPI.Store(proc, 8, metricComp);
			HalconAPI.Store(proc, 9, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minContrastComp);
			HalconAPI.UnpinTuple(minScoreComp);
			HalconAPI.UnpinTuple(numLevelsComp);
			HalconAPI.UnpinTuple(angleStepComp);
			HalconAPI.UnpinTuple(metricComp);
			HalconAPI.UnpinTuple(pregenerationComp);
			HComponentModel result = null;
			err = HComponentModel.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out rootRanking);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HComponentModel CreateTrainedComponentModel(double angleStart, double angleExtent, int minContrastComp, double minScoreComp, int numLevelsComp, double angleStepComp, string optimizationComp, string metricComp, string pregenerationComp, out int rootRanking)
		{
			IntPtr proc = HalconAPI.PreCall(1005);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreI(proc, 3, minContrastComp);
			HalconAPI.StoreD(proc, 4, minScoreComp);
			HalconAPI.StoreI(proc, 5, numLevelsComp);
			HalconAPI.StoreD(proc, 6, angleStepComp);
			HalconAPI.StoreS(proc, 7, optimizationComp);
			HalconAPI.StoreS(proc, 8, metricComp);
			HalconAPI.StoreS(proc, 9, pregenerationComp);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HComponentModel result = null;
			err = HComponentModel.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out rootRanking);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion GetComponentRelations(int referenceComponent, HTuple image, out HTuple row, out HTuple column, out HTuple phi, out HTuple length1, out HTuple length2, out HTuple angleStart, out HTuple angleExtent)
		{
			IntPtr proc = HalconAPI.PreCall(1008);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, referenceComponent);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(image);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out length1);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out length2);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out angleStart);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out angleExtent);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion GetComponentRelations(int referenceComponent, string image, out double row, out double column, out double phi, out double length1, out double length2, out double angleStart, out double angleExtent)
		{
			IntPtr proc = HalconAPI.PreCall(1008);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, referenceComponent);
			HalconAPI.StoreS(proc, 2, image);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out length1);
			err = HalconAPI.LoadD(proc, 4, err, out length2);
			err = HalconAPI.LoadD(proc, 5, err, out angleStart);
			err = HalconAPI.LoadD(proc, 6, err, out angleExtent);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion GetTrainingComponents(HTuple components, HTuple image, string markOrientation, out HTuple row, out HTuple column, out HTuple angle, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1009);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, components);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 3, markOrientation);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(components);
			HalconAPI.UnpinTuple(image);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion GetTrainingComponents(string components, string image, string markOrientation, out double row, out double column, out double angle, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(1009);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, components);
			HalconAPI.StoreS(proc, 2, image);
			HalconAPI.StoreS(proc, 3, markOrientation);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out angle);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ModifyComponentRelations(HTuple referenceComponent, HTuple toleranceComponent, HTuple positionTolerance, HTuple angleTolerance)
		{
			IntPtr proc = HalconAPI.PreCall(1010);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, referenceComponent);
			HalconAPI.Store(proc, 2, toleranceComponent);
			HalconAPI.Store(proc, 3, positionTolerance);
			HalconAPI.Store(proc, 4, angleTolerance);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(referenceComponent);
			HalconAPI.UnpinTuple(toleranceComponent);
			HalconAPI.UnpinTuple(positionTolerance);
			HalconAPI.UnpinTuple(angleTolerance);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ModifyComponentRelations(string referenceComponent, string toleranceComponent, double positionTolerance, double angleTolerance)
		{
			IntPtr proc = HalconAPI.PreCall(1010);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, referenceComponent);
			HalconAPI.StoreS(proc, 2, toleranceComponent);
			HalconAPI.StoreD(proc, 3, positionTolerance);
			HalconAPI.StoreD(proc, 4, angleTolerance);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeTrainingComponents(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1011);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeTrainingComponents()
		{
			IntPtr proc = HalconAPI.PreCall(1012);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadTrainingComponents(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1013);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteTrainingComponents(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1014);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HRegion ClusterModelComponents(HImage trainingImages, string ambiguityCriterion, double maxContourOverlap, double clusterThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(1015);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, trainingImages);
			HalconAPI.StoreS(proc, 1, ambiguityCriterion);
			HalconAPI.StoreD(proc, 2, maxContourOverlap);
			HalconAPI.StoreD(proc, 3, clusterThreshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(trainingImages);
			return result;
		}

		public HRegion InspectClusteredComponents(string ambiguityCriterion, double maxContourOverlap, double clusterThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(1016);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, ambiguityCriterion);
			HalconAPI.StoreD(proc, 2, maxContourOverlap);
			HalconAPI.StoreD(proc, 3, clusterThreshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HRegion TrainModelComponents(HImage modelImage, HRegion initialComponents, HImage trainingImages, HTuple contrastLow, HTuple contrastHigh, HTuple minSize, HTuple minScore, HTuple searchRowTol, HTuple searchColumnTol, HTuple searchAngleTol, string trainingEmphasis, string ambiguityCriterion, double maxContourOverlap, double clusterThreshold)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1017);
			HalconAPI.Store(proc, 1, modelImage);
			HalconAPI.Store(proc, 2, initialComponents);
			HalconAPI.Store(proc, 3, trainingImages);
			HalconAPI.Store(proc, 0, contrastLow);
			HalconAPI.Store(proc, 1, contrastHigh);
			HalconAPI.Store(proc, 2, minSize);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, searchRowTol);
			HalconAPI.Store(proc, 5, searchColumnTol);
			HalconAPI.Store(proc, 6, searchAngleTol);
			HalconAPI.StoreS(proc, 7, trainingEmphasis);
			HalconAPI.StoreS(proc, 8, ambiguityCriterion);
			HalconAPI.StoreD(proc, 9, maxContourOverlap);
			HalconAPI.StoreD(proc, 10, clusterThreshold);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(contrastLow);
			HalconAPI.UnpinTuple(contrastHigh);
			HalconAPI.UnpinTuple(minSize);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(searchRowTol);
			HalconAPI.UnpinTuple(searchColumnTol);
			HalconAPI.UnpinTuple(searchAngleTol);
			err = base.Load(proc, 0, err);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelImage);
			GC.KeepAlive(initialComponents);
			GC.KeepAlive(trainingImages);
			return result;
		}

		public HRegion TrainModelComponents(HImage modelImage, HRegion initialComponents, HImage trainingImages, int contrastLow, int contrastHigh, int minSize, double minScore, int searchRowTol, int searchColumnTol, double searchAngleTol, string trainingEmphasis, string ambiguityCriterion, double maxContourOverlap, double clusterThreshold)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1017);
			HalconAPI.Store(proc, 1, modelImage);
			HalconAPI.Store(proc, 2, initialComponents);
			HalconAPI.Store(proc, 3, trainingImages);
			HalconAPI.StoreI(proc, 0, contrastLow);
			HalconAPI.StoreI(proc, 1, contrastHigh);
			HalconAPI.StoreI(proc, 2, minSize);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, searchRowTol);
			HalconAPI.StoreI(proc, 5, searchColumnTol);
			HalconAPI.StoreD(proc, 6, searchAngleTol);
			HalconAPI.StoreS(proc, 7, trainingEmphasis);
			HalconAPI.StoreS(proc, 8, ambiguityCriterion);
			HalconAPI.StoreD(proc, 9, maxContourOverlap);
			HalconAPI.StoreD(proc, 10, clusterThreshold);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modelImage);
			GC.KeepAlive(initialComponents);
			GC.KeepAlive(trainingImages);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1007);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
