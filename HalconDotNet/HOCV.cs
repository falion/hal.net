using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HOCV : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCV()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCV(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCV obj)
		{
			obj = new HOCV(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCV[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HOCV[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HOCV(hTuple[i].IP);
			}
			return err;
		}

		public HOCV(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(642);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCV(HTuple patternNames)
		{
			IntPtr proc = HalconAPI.PreCall(646);
			HalconAPI.Store(proc, 0, patternNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(patternNames);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeOcv();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCV(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeOcv(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeOcv().Serialize(stream);
		}

		public static HOCV Deserialize(Stream stream)
		{
			HOCV hOCV = new HOCV();
			hOCV.DeserializeOcv(HSerializedItem.Deserialize(stream));
			return hOCV;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HOCV Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeOcv();
			HOCV hOCV = new HOCV();
			hOCV.DeserializeOcv(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCV;
		}

		public HTuple DoOcvSimple(HImage pattern, HTuple patternName, string adaptPos, string adaptSize, string adaptAngle, string adaptGray, double threshold)
		{
			IntPtr proc = HalconAPI.PreCall(638);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, pattern);
			HalconAPI.Store(proc, 1, patternName);
			HalconAPI.StoreS(proc, 2, adaptPos);
			HalconAPI.StoreS(proc, 3, adaptSize);
			HalconAPI.StoreS(proc, 4, adaptAngle);
			HalconAPI.StoreS(proc, 5, adaptGray);
			HalconAPI.StoreD(proc, 6, threshold);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(patternName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
			return result;
		}

		public double DoOcvSimple(HImage pattern, string patternName, string adaptPos, string adaptSize, string adaptAngle, string adaptGray, double threshold)
		{
			IntPtr proc = HalconAPI.PreCall(638);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, pattern);
			HalconAPI.StoreS(proc, 1, patternName);
			HalconAPI.StoreS(proc, 2, adaptPos);
			HalconAPI.StoreS(proc, 3, adaptSize);
			HalconAPI.StoreS(proc, 4, adaptAngle);
			HalconAPI.StoreS(proc, 5, adaptGray);
			HalconAPI.StoreD(proc, 6, threshold);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
			return result;
		}

		public void TraindOcvProj(HImage pattern, HTuple name, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(639);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, pattern);
			HalconAPI.Store(proc, 1, name);
			HalconAPI.StoreS(proc, 2, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(name);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
		}

		public void TraindOcvProj(HImage pattern, string name, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(639);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, pattern);
			HalconAPI.StoreS(proc, 1, name);
			HalconAPI.StoreS(proc, 2, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
		}

		public void DeserializeOcv(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(640);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeOcv()
		{
			IntPtr proc = HalconAPI.PreCall(641);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadOcv(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(642);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteOcv(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(643);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateOcvProj(HTuple patternNames)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(646);
			HalconAPI.Store(proc, 0, patternNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(patternNames);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcvProj(string patternNames)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(646);
			HalconAPI.StoreS(proc, 0, patternNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(645);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
