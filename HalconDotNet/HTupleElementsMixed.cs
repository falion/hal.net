namespace VisionConfig
{
	internal class HTupleElementsMixed : HTupleElementsImplementation
	{
		internal HTupleElementsMixed(HTupleMixed source, int index)
			: base(source, index)
		{
		}

		internal HTupleElementsMixed(HTupleMixed source, int[] indices)
			: base(source, indices)
		{
		}

		public override int[] getI()
		{
			if (base.indices == null)
			{
				return null;
			}
			switch (this.getType())
			{
			case HTupleType.INTEGER:
			{
				int[] array = new int[base.indices.Length];
				for (int i = 0; i < base.indices.Length; i++)
				{
					array[i] = (int)base.source.OArr[base.indices[i]];
				}
				return array;
			}
			case HTupleType.LONG:
			{
				int[] array2 = new int[base.indices.Length];
				for (int j = 0; j < base.indices.Length; j++)
				{
					array2[j] = (int)(long)base.source.OArr[base.indices[j]];
				}
				return array2;
			}
			default:
				throw new HTupleAccessException(base.source, "Mixed tuple does not contain integer " + ((base.indices.Length == 1) ? ("value at index " + base.indices[0]) : "values at given indices"));
			}
		}

		public override long[] getL()
		{
			if (base.indices == null)
			{
				return null;
			}
			switch (this.getType())
			{
			case HTupleType.INTEGER:
			{
				long[] array = new long[base.indices.Length];
				for (int i = 0; i < base.indices.Length; i++)
				{
					array[i] = (int)base.source.OArr[base.indices[i]];
				}
				return array;
			}
			case HTupleType.LONG:
			{
				long[] array2 = new long[base.indices.Length];
				for (int j = 0; j < base.indices.Length; j++)
				{
					array2[j] = (long)base.source.OArr[base.indices[j]];
				}
				return array2;
			}
			default:
				throw new HTupleAccessException(base.source, "Mixed tuple does not contain integer " + ((base.indices.Length == 1) ? ("value at index " + base.indices[0]) : "values at given indices"));
			}
		}

		public override double[] getD()
		{
			if (base.indices == null)
			{
				return null;
			}
			switch (this.getType())
			{
			case HTupleType.DOUBLE:
			{
				double[] array = new double[base.indices.Length];
				for (int i = 0; i < base.indices.Length; i++)
				{
					array[i] = (double)base.source.OArr[base.indices[i]];
				}
				return array;
			}
			case HTupleType.INTEGER:
			{
				double[] array2 = new double[base.indices.Length];
				for (int j = 0; j < base.indices.Length; j++)
				{
					array2[j] = (double)(int)base.source.OArr[base.indices[j]];
				}
				return array2;
			}
			case HTupleType.LONG:
			{
				double[] array3 = new double[base.indices.Length];
				for (int k = 0; k < base.indices.Length; k++)
				{
					array3[k] = (double)(long)base.source.OArr[base.indices[k]];
				}
				return array3;
			}
			default:
				throw new HTupleAccessException(base.source, "Mixed tuple does not contain numeric " + ((base.indices.Length == 1) ? ("value at index " + base.indices[0]) : "values at given indices"));
			}
		}

		public override string[] getS()
		{
			if (base.indices == null)
			{
				return null;
			}
			HTupleType type = this.getType();
			if (type == HTupleType.STRING)
			{
				string[] array = new string[base.indices.Length];
				for (int i = 0; i < base.indices.Length; i++)
				{
					array[i] = (string)base.source.OArr[base.indices[i]];
				}
				return array;
			}
			throw new HTupleAccessException(base.source, "Mixed tuple does not contain string " + ((base.indices.Length == 1) ? ("value at index " + base.indices[0]) : "values at given indices"));
		}

		public override object[] getO()
		{
			if (base.indices == null)
			{
				return null;
			}
			object[] array = new object[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = base.source.OArr[base.indices[i]];
			}
			return array;
		}

		public override void setI(int[] i)
		{
			if (i != null)
			{
				if (i.Length > 1)
				{
					if (i.Length == base.indices.Length)
					{
						for (int j = 0; j < i.Length; j++)
						{
							base.source.OArr[base.indices[j]] = i[j];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int k = 0; k < i.Length; k++)
				{
					base.source.OArr[base.indices[k]] = i[0];
				}
			}
		}

		public override void setL(long[] l)
		{
			if (l != null)
			{
				if (l.Length > 1)
				{
					if (l.Length == base.indices.Length)
					{
						for (int i = 0; i < l.Length; i++)
						{
							base.source.OArr[base.indices[i]] = l[i];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int j = 0; j < l.Length; j++)
				{
					base.source.OArr[base.indices[j]] = l[0];
				}
			}
		}

		public override void setD(double[] d)
		{
			if (d != null)
			{
				if (d.Length > 1)
				{
					if (d.Length == base.indices.Length)
					{
						for (int i = 0; i < d.Length; i++)
						{
							base.source.OArr[base.indices[i]] = d[i];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int j = 0; j < d.Length; j++)
				{
					base.source.OArr[base.indices[j]] = d[0];
				}
			}
		}

		public override void setS(string[] s)
		{
			if (s != null)
			{
				if (s.Length > 1)
				{
					if (s.Length == base.indices.Length)
					{
						for (int i = 0; i < s.Length; i++)
						{
							base.source.OArr[base.indices[i]] = s[i];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int j = 0; j < s.Length; j++)
				{
					base.source.OArr[base.indices[j]] = s[0];
				}
			}
		}

		public override void setO(object[] o)
		{
			if (o != null)
			{
				if (o.Length > 1)
				{
					if (o.Length == base.indices.Length)
					{
						for (int i = 0; i < o.Length; i++)
						{
							base.source.OArr[base.indices[i]] = o[i];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int j = 0; j < o.Length; j++)
				{
					base.source.OArr[base.indices[j]] = o[0];
				}
			}
		}

		public override HTupleType getType()
		{
			return ((HTupleMixed)base.source).GetElementType(base.indices);
		}
	}
}
