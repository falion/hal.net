using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HGnuplot : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HGnuplot()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HGnuplot(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HGnuplot obj)
		{
			obj = new HGnuplot(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HGnuplot[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HGnuplot[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HGnuplot(hTuple[i].IP);
			}
			return err;
		}

		public void GnuplotPlotFunct1d(HFunction1D function)
		{
			IntPtr proc = HalconAPI.PreCall(1295);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, function);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(function);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GnuplotPlotCtrl(HTuple values)
		{
			IntPtr proc = HalconAPI.PreCall(1296);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, values);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(values);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GnuplotPlotImage(HImage image, int samplesX, int samplesY, HTuple viewRotX, HTuple viewRotZ, string hidden3D)
		{
			IntPtr proc = HalconAPI.PreCall(1297);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 1, samplesX);
			HalconAPI.StoreI(proc, 2, samplesY);
			HalconAPI.Store(proc, 3, viewRotX);
			HalconAPI.Store(proc, 4, viewRotZ);
			HalconAPI.StoreS(proc, 5, hidden3D);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(viewRotX);
			HalconAPI.UnpinTuple(viewRotZ);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void GnuplotPlotImage(HImage image, int samplesX, int samplesY, double viewRotX, double viewRotZ, string hidden3D)
		{
			IntPtr proc = HalconAPI.PreCall(1297);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 1, samplesX);
			HalconAPI.StoreI(proc, 2, samplesY);
			HalconAPI.StoreD(proc, 3, viewRotX);
			HalconAPI.StoreD(proc, 4, viewRotZ);
			HalconAPI.StoreS(proc, 5, hidden3D);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void GnuplotOpenFile(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1299);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GnuplotOpenPipe()
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1300);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1298);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
