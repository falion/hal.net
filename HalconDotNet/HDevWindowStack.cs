using System;
using System.ComponentModel;

namespace VisionConfig
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class HDevWindowStack : IDisposable
	{
		~HDevWindowStack()
		{
			try
			{
				this.Dispose(false);
			}
			catch (Exception)
			{
			}
		}

		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				GC.SuppressFinalize(this);
			}
			GC.KeepAlive(this);
		}

		void IDisposable.Dispose()
		{
			this.Dispose(true);
		}

		public virtual void Dispose()
		{
			this.Dispose(true);
		}

		public static void Push(HTuple win_handle)
		{
			int num = HalconAPI.HWindowStackPush(win_handle);
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::Push");
			}
		}

		public static HTuple Pop()
		{
			long l = 0L;
			int num = HalconAPI.HWindowStackPop(out l);
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::Pop");
			}
			return l;
		}

		public static HTuple GetActive()
		{
			long l = 0L;
			int num = HalconAPI.HWindowStackGetActive(out l);
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::GetActive");
			}
			return l;
		}

		public static void SetActive(HTuple win_handle)
		{
			int num = HalconAPI.HWindowStackSetActive(win_handle);
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::SetActive");
			}
		}

		public static bool IsOpen()
		{
			bool result = false;
			int num = HalconAPI.HWindowStackIsOpen(out result);
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::IsOpen");
			}
			return result;
		}

		public static void CloseAll()
		{
			int num = HalconAPI.HWindowStackCloseAll();
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::CloseAll");
			}
		}
	}
}
