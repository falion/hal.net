using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace VisionConfig
{
	[ToolboxBitmap(typeof(HSmartWindowControlWPF), "HWindowControlWPF.icon.bmp")]
	public class HSmartWindowControlWPF : UserControl, INotifyPropertyChanged, IComponentConnector
	{
		public class HMouseEventArgsWPF : EventArgs
		{
			private readonly double x;

			private readonly double y;

			private readonly double row;

			private readonly double column;

			private readonly int delta;

			private readonly MouseButton? button;

			public double X
			{
				get
				{
					return this.x;
				}
			}

			public double Y
			{
				get
				{
					return this.y;
				}
			}

			public double Row
			{
				get
				{
					return this.row;
				}
			}

			public double Column
			{
				get
				{
					return this.column;
				}
			}

			public int Delta
			{
				get
				{
					return this.delta;
				}
			}

			public MouseButton? Button
			{
				get
				{
					return this.button;
				}
			}

			internal HMouseEventArgsWPF(double x, double y, double row, double column, int delta, MouseButton? button)
			{
				this.x = x;
				this.y = y;
				this.row = row;
				this.column = column;
				this.delta = delta;
				this.button = button;
			}
		}

		public enum ZoomContent
		{
			Off,
			WheelForwardZoomsIn,
			WheelBackwardZoomsIn
		}

		public enum DrawingObjectsModifier
		{
			None,
			Shift,
			Ctrl,
			Alt
		}

		public delegate void HMouseEventHandlerWPF(object sender, HMouseEventArgsWPF e);

		public delegate void HErrorHandler(HalconException he);

		private const int HZoomFactorMin = 1;

		private const int HZoomFactorMax = 100;

		public static readonly DependencyProperty HMoveContentProperty = DependencyProperty.Register("HMoveContent", typeof(bool), typeof(HSmartWindowControlWPF), new PropertyMetadata(true));

		public static readonly DependencyProperty HZoomContentProperty = DependencyProperty.Register("HZoomContent", typeof(ZoomContent), typeof(HSmartWindowControlWPF), new PropertyMetadata(ZoomContent.WheelForwardZoomsIn));

		public static DependencyProperty HZoomFactorProperty = DependencyProperty.Register("HZoomFactor", typeof(double), typeof(HSmartWindowControlWPF), new PropertyMetadata(Math.Round(Math.Sqrt(2.0), 2)), HSmartWindowControlWPF.HZoomFactorValidation);

		public static DependencyProperty HDisplayCurrentObjectProperty = DependencyProperty.Register("HDisplayCurrentObject", typeof(HObject), typeof(HSmartWindowControlWPF), null);

		[Description("Displays the assigned HImage or HObject.")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Category("Miscellaneous")]
		private HObject _current_value;

		public static readonly DependencyProperty HDrawingObjectsModifierProperty = DependencyProperty.Register("HDrawingObjectsModifier", typeof(DrawingObjectsModifier), typeof(HSmartWindowControlWPF), new PropertyMetadata(DrawingObjectsModifier.None));

		public static readonly DependencyProperty HKeepAspectRatioProperty = DependencyProperty.Register("HKeepAspectRatio", typeof(bool), typeof(HSmartWindowControlWPF), new PropertyMetadata(true));

		public static readonly DependencyProperty HDoubleClickToFitContentProperty = DependencyProperty.Register("HDoubleClickToFitContent", typeof(bool), typeof(HSmartWindowControlWPF), new PropertyMetadata(true));

		public static readonly DependencyProperty HImagePartProperty = DependencyProperty.Register("HImagePart", typeof(Rect), typeof(HSmartWindowControlWPF), new PropertyMetadata(new Rect(0.0, 0.0, 640.0, 480.0), HSmartWindowControlWPF.OnHImagePartChanged));

		public static readonly DependencyProperty WindowSizeProperty = DependencyProperty.Register("WindowSize", typeof(System.Windows.Size), typeof(HSmartWindowControlWPF), new PropertyMetadata(new System.Windows.Size(320.0, 200.0)));

		private bool _left_button_down;

		private System.Windows.Point _last_position;

		private HWindow _hwindow;

		private double _dpiX;

		private double _dpiY;

		private HTuple _dump_params;

		private bool _delayed;

		private System.Drawing.Size _prevsize;

		private HObject _dumpimg;

		private WriteableBitmap _wbitmap;

		internal HSmartWindowControlWPF HWindowControlWPF;

		internal Viewbox WindowFrame;

		private bool _contentLoaded;

		public HWindow HalconWindow
		{
			get
			{
				if (this._delayed)
				{
					this.HInitializeWindow();
				}
				return this._hwindow;
			}
		}

		public IntPtr HalconID
		{
			get
			{
				if (this._delayed)
				{
					this.HInitializeWindow();
				}
				if (this._hwindow != null)
				{
					return this._hwindow.Handle;
				}
				return HTool.UNDEF;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Always)]
		[Description("If on, the content of the HSmartWindowControlWPF is moved when the mouse pointer is dragged.")]
		[Category("Behavior")]
		public bool HMoveContent
		{
			get
			{
				return (bool)base.GetValue(HSmartWindowControlWPF.HMoveContentProperty);
			}
			set
			{
				base.SetValue(HSmartWindowControlWPF.HMoveContentProperty, value);
			}
		}

		[Description("Controls the behavior of the mouse wheel.")]
		[Category("Behavior")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public ZoomContent HZoomContent
		{
			get
			{
				return (ZoomContent)base.GetValue(HSmartWindowControlWPF.HZoomContentProperty);
			}
			set
			{
				base.SetValue(HSmartWindowControlWPF.HZoomContentProperty, value);
			}
		}

		[Category("Behavior")]
		[Description("Controls the behavior of the zoom factor controlled by the mouse wheel.")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double HZoomFactor
		{
			get
			{
				return (double)base.GetValue(HSmartWindowControlWPF.HZoomFactorProperty);
			}
			set
			{
				if (value > 1.0 && value <= 100.0)
				{
					base.SetValue(HSmartWindowControlWPF.HZoomFactorProperty, value);
				}
			}
		}

		public HObject HDisplayCurrentObject
		{
			get
			{
				return (HObject)base.GetValue(HSmartWindowControlWPF.HDisplayCurrentObjectProperty);
			}
			set
			{
				this._current_value = value;
				this._hwindow.ClearWindow();
				if (value != null)
				{
					this._current_value.DispObj(this._hwindow);
					this.SetFullImagePart(null);
				}
				base.SetValue(HSmartWindowControlWPF.HDisplayCurrentObjectProperty, value);
				this.OnPropertyChanged("HDisplayCurrentObject");
			}
		}

		[Category("Behavior")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Description("Modifier key to interact with drawing objects. If a modifier key is selected, the user can only interact with drawing objects while keeping the modifier key pressed. This is especially useful when interacting with XLD drawing objects.")]
		public DrawingObjectsModifier HDrawingObjectsModifier
		{
			get
			{
				return (DrawingObjectsModifier)base.GetValue(HSmartWindowControlWPF.HDrawingObjectsModifierProperty);
			}
			set
			{
				base.SetValue(HSmartWindowControlWPF.HDrawingObjectsModifierProperty, value);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Always)]
		[Category("Behavior")]
		[Description("If on, the content of the HSmartWindowControlWPF keeps its aspect ratio when the control is resized or zoomed.")]
		public bool HKeepAspectRatio
		{
			get
			{
				return (bool)base.GetValue(HSmartWindowControlWPF.HKeepAspectRatioProperty);
			}
			set
			{
				base.SetValue(HSmartWindowControlWPF.HKeepAspectRatioProperty, value);
			}
		}

		[Description("If on, double clicking resizes the content of the HSmartWindowControlWPF to fit the size of the control.")]
		[Category("Behavior")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public bool HDoubleClickToFitContent
		{
			get
			{
				return (bool)base.GetValue(HSmartWindowControlWPF.HDoubleClickToFitContentProperty);
			}
			set
			{
				base.SetValue(HSmartWindowControlWPF.HDoubleClickToFitContentProperty, value);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Always)]
		[Category("Layout")]
		[Description("Visible image part (Column, Row, Width, Height).")]
		public Rect HImagePart
		{
			get
			{
				if (this._hwindow != null)
				{
					double num = 0.0;
					double num2 = 0.0;
					double num3 = 0.0;
					double num4 = 0.0;
					this.GetFloatPart(this._hwindow, out num, out num2, out num3, out num4);
					Rect rect = default(Rect);
					rect.X = num2;
					rect.Y = num;
					rect.Width = num4 - num2 + 1.0;
					rect.Height = num3 - num + 1.0;
					base.SetValue(HSmartWindowControlWPF.HImagePartProperty, rect);
					return rect;
				}
				return (Rect)base.GetValue(HSmartWindowControlWPF.HImagePartProperty);
			}
			set
			{
				if (this._hwindow != null)
				{
					this._hwindow.SetPart(value.Top, value.Left, value.Bottom - 1.0, value.Right - 1.0);
				}
				base.SetValue(HSmartWindowControlWPF.HImagePartProperty, value);
			}
		}

		public System.Windows.Size WindowSize
		{
			get
			{
				return (System.Windows.Size)base.GetValue(HSmartWindowControlWPF.WindowSizeProperty);
			}
			set
			{
				if (!(value.Width <= 0.0) && !(value.Height <= 0.0) && !this._delayed)
				{
					int num = 0;
					int num2 = 0;
					int width = 0;
					int height = 0;
					this._hwindow.GetWindowExtents(out num, out num2, out width, out height);
					try
					{
						HWindow hwindow = this._hwindow;
						System.Windows.Size windowSize = this.WindowSize;
						int width2 = (int)windowSize.Width;
						windowSize = this.WindowSize;
						hwindow.SetWindowExtents(0, 0, width2, (int)windowSize.Height);
						base.SetValue(HSmartWindowControlWPF.WindowSizeProperty, value);
					}
					catch (HalconException)
					{
						this._hwindow.SetWindowExtents(0, 0, width, height);
					}
				}
			}
		}

		[Category("Behavior")]
		[Description("Occurs after the HALCON window has been initialized.")]
		public event HInitWindowEventHandler HInitWindow;

		public event HMouseEventHandlerWPF HMouseDown;

		public event HMouseEventHandlerWPF HMouseUp;

		public event HMouseEventHandlerWPF HMouseWheel;

		public event HMouseEventHandlerWPF HMouseMove;

		public event HMouseEventHandlerWPF HMouseDoubleClick;

		public event HErrorHandler HErrorNotify;

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnHInitWindow()
		{
			if (this.HInitWindow != null)
			{
				this.HInitWindow(this, new EventArgs());
			}
		}

		private static bool HZoomFactorValidation(object value)
		{
			if ((double)value > 1.0)
			{
				return (double)value <= 100.0;
			}
			return false;
		}

		private static void OnHImagePartChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			((HSmartWindowControlWPF)sender).HImagePart = (Rect)e.NewValue;
		}

		public HSmartWindowControlWPF()
		{
			this.InitializeComponent();
			this.HInitializeWindow();
		}

		private void HInitializeWindow()
		{
			System.Windows.Size renderSize = base.RenderSize;
			int delayed;
			if (!(renderSize.Width <= 0.0))
			{
				renderSize = base.RenderSize;
				delayed = ((renderSize.Height <= 0.0) ? 1 : 0);
			}
			else
			{
				delayed = 1;
			}
			this._delayed = ((byte)delayed != 0);
			if (this._delayed && base.Parent != null)
			{
				FrameworkElement frameworkElement = base.Parent as FrameworkElement;
				if (frameworkElement != null)
				{
					base.RenderSize = frameworkElement.RenderSize;
					renderSize = base.RenderSize;
					int delayed2;
					if (!(renderSize.Width <= 0.0))
					{
						renderSize = base.RenderSize;
						delayed2 = ((renderSize.Height <= 0.0) ? 1 : 0);
					}
					else
					{
						delayed2 = 1;
					}
					this._delayed = ((byte)delayed2 != 0);
				}
			}
			if (!this._delayed)
			{
				System.Windows.Size renderSize2 = base.RenderSize;
				base.Dispatcher.ShutdownStarted += this.Dispatcher_ShutdownStarted;
				Rect hImagePart = this.HImagePart;
				this._hwindow = new HWindow(0, 0, (int)renderSize2.Width, (int)renderSize2.Height, 0, "buffer", "");
				this._hwindow.SetWindowParam("graphics_stack", "true");
				this._hwindow.SetPart(hImagePart.Top, hImagePart.Left, hImagePart.Bottom - 1.0, hImagePart.Right - 1.0);
				this._prevsize.Width = (int)base.ActualWidth;
				this._prevsize.Height = (int)base.ActualHeight;
				using (Graphics graphics = Graphics.FromHwnd(IntPtr.Zero))
				{
					this._dpiX = (double)graphics.DpiX;
					this._dpiY = (double)graphics.DpiY;
				}
				this._dump_params = new HTuple(this.HalconID);
				this._dump_params = this._dump_params.TupleConcat("interleaved");
				this._hwindow.OnContentUpdate(delegate
				{
					base.Dispatcher.BeginInvoke(new Action(base.InvalidateVisual));
				});
				if (this.HInitWindow != null)
				{
					this.OnHInitWindow();
				}
				base.DataContext = this;
			}
		}

		private System.Drawing.Size AspectRatioSize(System.Drawing.Size imgsize)
		{
			return default(System.Drawing.Size);
		}

		private void Dispatcher_ShutdownStarted(object sender, EventArgs e)
		{
			this._hwindow.Dispose();
		}

		private Matrix GetVisualTransform(Visual v)
		{
			if (v != null)
			{
				Matrix matrix = Matrix.Identity;
				Transform transform = VisualTreeHelper.GetTransform(v);
				if (transform != null)
				{
					Matrix value = transform.Value;
					matrix = Matrix.Multiply(matrix, value);
				}
				Vector offset = VisualTreeHelper.GetOffset(v);
				matrix.Translate(offset.X, offset.Y);
				return matrix;
			}
			return Matrix.Identity;
		}

		private System.Windows.Point TryApplyVisualTransform(System.Windows.Point point, Visual v, bool inverse, bool throwOnError, out bool success)
		{
			success = true;
			if (v != null)
			{
				Matrix visualTransform = this.GetVisualTransform(v);
				if (inverse)
				{
					if (!throwOnError && !visualTransform.HasInverse)
					{
						success = false;
						return new System.Windows.Point(0.0, 0.0);
					}
					visualTransform.Invert();
				}
				point = visualTransform.Transform(point);
			}
			return point;
		}

		private System.Windows.Point ApplyVisualTransform(System.Windows.Point point, Visual v, bool inverse)
		{
			bool flag = true;
			return this.TryApplyVisualTransform(point, v, inverse, true, out flag);
		}

		private System.Windows.Point GetPixelOffset()
		{
			System.Windows.Point point = default(System.Windows.Point);
			PresentationSource presentationSource = PresentationSource.FromVisual(this);
			if (presentationSource != null)
			{
				Visual rootVisual = presentationSource.RootVisual;
				if (((FrameworkElement)rootVisual).ActualHeight > 0.0 && ((FrameworkElement)rootVisual).ActualWidth > 0.0)
				{
					point = base.TransformToAncestor(rootVisual).Transform(point);
					point = this.ApplyVisualTransform(point, rootVisual, false);
					Matrix matrix = presentationSource.CompositionTarget.TransformToDevice;
					point = matrix.Transform(point);
					point.X = Math.Round(point.X);
					point.Y = Math.Round(point.Y);
					matrix = presentationSource.CompositionTarget.TransformFromDevice;
					point = matrix.Transform(point);
					point = this.ApplyVisualTransform(point, rootVisual, true);
					point = rootVisual.TransformToDescendant(this).Transform(point);
				}
			}
			return point;
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
			bool flag = false;
			if (DesignerProperties.GetIsInDesignMode(this))
			{
				Assembly executingAssembly = Assembly.GetExecutingAssembly();
				Stream manifestResourceStream = executingAssembly.GetManifestResourceStream("HalconDotNet.halcon_icon_48.png");
				if (manifestResourceStream == null)
				{
					drawingContext.DrawRectangle(new SolidColorBrush(Colors.Black), null, new Rect(0.0, 0.0, base.ActualWidth, base.ActualHeight));
				}
				else
				{
					try
					{
						BitmapImage bitmapImage = new BitmapImage();
						bitmapImage.BeginInit();
						bitmapImage.StreamSource = manifestResourceStream;
						bitmapImage.EndInit();
						int num = (int)base.ActualWidth / 2 - 24;
						int num2 = (int)base.ActualHeight / 2 - 24;
						drawingContext.DrawRectangle(new SolidColorBrush(Colors.Black), null, new Rect(0.0, 0.0, base.ActualWidth, base.ActualHeight));
						if (base.ActualWidth > 48.0 && base.ActualHeight > 48.0)
						{
							drawingContext.DrawImage(bitmapImage, new Rect((double)num, (double)num2, 48.0, 48.0));
						}
					}
					catch
					{
					}
				}
			}
			else
			{
				if (this._hwindow == null || this._delayed)
				{
					this.HInitializeWindow();
				}
				if (this._hwindow == null)
				{
					base.Measure(new System.Windows.Size(double.PositiveInfinity, double.PositiveInfinity));
				}
				else
				{
					int num3 = 0;
					int num4 = 0;
					int num5 = 0;
					int num6 = 0;
					this._hwindow.GetWindowExtents(out num3, out num4, out num5, out num6);
					if (num5 != (int)base.ActualWidth || num6 != (int)base.ActualHeight)
					{
						this.WindowSize = new System.Windows.Size(base.ActualWidth, base.ActualHeight);
						flag = true;
					}
					if (this.HKeepAspectRatio & flag)
					{
						this.calculate_part(this.HalconID, this._prevsize.Width, this._prevsize.Height);
						this.SetFullImagePart(null);
					}
					this._prevsize.Width = (int)base.ActualWidth;
					this._prevsize.Height = (int)base.ActualHeight;
					if (this._dumpimg != null)
					{
						this._dumpimg.Dispose();
					}
					HOperatorSet.DumpWindowImage(out this._dumpimg, this._dump_params);
					HTuple hTuple = null;
					HTuple hTuple2 = null;
					HTuple hTuple3 = null;
					HTuple hTuple4 = null;
					HOperatorSet.GetImagePointer1(this._dumpimg, out hTuple, out hTuple2, out hTuple3, out hTuple4);
					IntPtr buffer = (IntPtr)hTuple.L;
					if (this._wbitmap == null || (int)(hTuple3 / 4) != (int)this._wbitmap.Width || (int)hTuple4 != (int)this._wbitmap.Height)
					{
						this._wbitmap = new WriteableBitmap(hTuple3 / 4, hTuple4, this._dpiX, this._dpiY, PixelFormats.Bgra32, null);
					}
					this._wbitmap.Lock();
					this._wbitmap.WritePixels(new Int32Rect(0, 0, hTuple3 / 4, hTuple4), buffer, hTuple3 * hTuple4, hTuple3);
					this._wbitmap.Unlock();
					System.Windows.Point pixelOffset = this.GetPixelOffset();
					drawingContext.DrawImage(this._wbitmap, new Rect(pixelOffset.X, pixelOffset.Y, hTuple3 / 4, hTuple4));
				}
			}
		}

		private HMouseEventArgsWPF ToHMouse(int x, int y, MouseEventArgs e, int delta)
		{
			MouseButton? button = null;
			double row = 0.0;
			double column = 0.0;
			this._hwindow.ConvertCoordinatesWindowToImage((double)y, (double)x, out row, out column);
			if (e != null)
			{
				if (e.LeftButton == MouseButtonState.Pressed)
				{
					button = MouseButton.Left;
				}
				else if (e.RightButton == MouseButtonState.Pressed)
				{
					button = MouseButton.Right;
				}
				else if (e.MiddleButton == MouseButtonState.Pressed)
				{
					button = MouseButton.Middle;
				}
				else if (e.XButton1 == MouseButtonState.Pressed)
				{
					button = MouseButton.XButton1;
				}
				else if (e.XButton2 == MouseButtonState.Pressed)
				{
					button = MouseButton.XButton2;
				}
			}
			return new HMouseEventArgsWPF((double)x, (double)y, row, column, delta, button);
		}

		private void GetFloatPart(HWindow window, out double l1, out double c1, out double l2, out double c2)
		{
			HTuple t = null;
			HTuple t2 = null;
			HTuple t3 = null;
			HTuple t4 = null;
			window.GetPart(out t, out t2, out t3, out t4);
			l1 = t;
			c1 = t2;
			l2 = t3;
			c2 = t4;
		}

		public void HZoomWindowContents(double x, double y, int Delta)
		{
			HTuple homMat2D = null;
			HOperatorSet.HomMat2dIdentity(out homMat2D);
			double d = 0.0;
			double d2 = 0.0;
			this.ConvertWindowsCoordinatesToHImage(y, x, out d, out d2);
			double num = (Delta < 0) ? this.HZoomFactor : (1.0 / this.HZoomFactor);
			if (this.HZoomContent == ZoomContent.WheelBackwardZoomsIn)
			{
				num = 1.0 / num;
			}
			for (int num2 = Math.Abs(Delta) / 120; num2 > 1; num2--)
			{
				num *= ((Delta < 0) ? this.HZoomFactor : (1.0 / this.HZoomFactor));
			}
			HTuple homMat2D2 = null;
			HOperatorSet.HomMat2dScale(homMat2D, (HTuple)num, (HTuple)num, (HTuple)d2, (HTuple)d, out homMat2D2);
			double num3 = 0.0;
			double num4 = 0.0;
			double num5 = 0.0;
			double num6 = 0.0;
			this.GetFloatPart(this._hwindow, out num3, out num4, out num5, out num6);
			HTuple hTuple = null;
			HTuple hTuple2 = null;
			HOperatorSet.AffineTransPoint2d(homMat2D2, (HTuple)num4, (HTuple)num3, out hTuple, out hTuple2);
			HTuple hTuple3 = null;
			HTuple hTuple4 = null;
			HOperatorSet.AffineTransPoint2d(homMat2D2, (HTuple)num6, (HTuple)num5, out hTuple3, out hTuple4);
			try
			{
				this.HImagePart = new Rect(hTuple.D, hTuple2.D, hTuple3.D - hTuple.D + 1.0, hTuple4.D - hTuple2.D + 1.0);
			}
			catch (Exception)
			{
				try
				{
					this.HImagePart = new Rect(num4, num3, num6 - num4 + 1.0, num5 - num3 + 1.0);
				}
				catch (HalconException)
				{
				}
			}
		}

		private void HWPFWindow_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			HMouseEventArgsWPF e2 = null;
			try
			{
				if (this.HZoomContent != 0)
				{
					this._last_position = this.GetPosition(e);
					this.HZoomWindowContents(this._last_position.X, this._last_position.Y, e.Delta);
				}
				e2 = this.ToHMouse((int)this._last_position.X, (int)this._last_position.Y, null, e.Delta);
			}
			catch (HalconException he)
			{
				if (this.HErrorNotify != null)
				{
					this.HErrorNotify(he);
				}
			}
			if (this.HMouseWheel != null)
			{
				this.HMouseWheel(this, e2);
			}
		}

		private void ConvertWindowsCoordinatesToHImage(double wr, double wc, out double ir, out double ic)
		{
			this._hwindow.ConvertCoordinatesWindowToImage(wr, wc, out ir, out ic);
		}

		private int MouseEventToInt(MouseEventArgs e)
		{
			if (e.LeftButton != MouseButtonState.Pressed && e.LeftButton != 0)
			{
				if (e.RightButton != MouseButtonState.Pressed && e.RightButton != 0)
				{
					if (e.MiddleButton != MouseButtonState.Pressed && e.MiddleButton != 0)
					{
						return 0;
					}
					return 2;
				}
				return 4;
			}
			return 1;
		}

		private bool InteractingWithDrawingObjs()
		{
			switch (this.HDrawingObjectsModifier)
			{
			case DrawingObjectsModifier.Shift:
				if (!Keyboard.IsKeyDown(Key.LeftShift))
				{
					return Keyboard.IsKeyDown(Key.RightShift);
				}
				return true;
			case DrawingObjectsModifier.Ctrl:
				if (!Keyboard.IsKeyDown(Key.LeftCtrl))
				{
					return Keyboard.IsKeyDown(Key.RightCtrl);
				}
				return true;
			case DrawingObjectsModifier.Alt:
				if (!Keyboard.IsKeyDown(Key.LeftAlt))
				{
					return Keyboard.IsKeyDown(Key.RightAlt);
				}
				return true;
			default:
				return true;
			}
		}

		public void HShiftWindowContents(double dx, double dy)
		{
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			this._hwindow.GetWindowExtents(out num, out num2, out num3, out num4);
			double num5 = 0.0;
			double num6 = 0.0;
			double num7 = 0.0;
			double num8 = 0.0;
			this.GetFloatPart(this._hwindow, out num5, out num6, out num7, out num8);
			double num9 = (num8 - num6 + 1.0) / (double)num3;
			double num10 = (num7 - num5 + 1.0) / (double)num4;
			try
			{
				double num11 = num5 + dy * num10;
				double num12 = num7 + dy * num10;
				double num13 = num6 + dx * num9;
				double num14 = num8 + dx * num9;
				this.HImagePart = new Rect(num13, num11, num14 - num13 + 1.0, num12 - num11 + 1.0);
			}
			catch (HalconException)
			{
				this.HImagePart = new Rect(num6, num5, num8 - num6 + 1.0, num7 - num5 + 1.0);
			}
		}

		private void HWPFWindow_MouseMove(object sender, MouseEventArgs e)
		{
			HMouseEventArgsWPF e2 = null;
			try
			{
				if (this._delayed)
				{
					return;
				}
				System.Windows.Point position = this.GetPosition(e);
				bool flag = false;
				if (this._left_button_down && this.InteractingWithDrawingObjs())
				{
					double d = 0.0;
					double d2 = 0.0;
					this.ConvertWindowsCoordinatesToHImage(position.Y, position.X, out d, out d2);
					HTuple hTuple = this._hwindow.SendMouseDragEvent(d, d2, this.MouseEventToInt(e));
					flag = hTuple[0].S.Equals("true");
				}
				if (this._left_button_down && !flag && this.HMoveContent)
				{
					this.HShiftWindowContents(this._last_position.X - position.X, this._last_position.Y - position.Y);
				}
				this._last_position = position;
				e2 = this.ToHMouse((int)this._last_position.X, (int)this._last_position.Y, e, 0);
			}
			catch (HalconException he)
			{
				if (this.HErrorNotify != null)
				{
					this.HErrorNotify(he);
				}
			}
			if (this.HMouseMove != null)
			{
				this.HMouseMove(this, e2);
			}
		}

		private System.Windows.Point GetPosition(MouseEventArgs e)
		{
			return e.GetPosition(this);
		}

		private void HWPFWindow_MouseDown(object sender, MouseButtonEventArgs e)
		{
			HMouseEventArgsWPF e2 = null;
			try
			{
				if (this._delayed)
				{
					return;
				}
				this._left_button_down = (e.LeftButton == MouseButtonState.Pressed);
				this._last_position = this.GetPosition(e);
				if (this.InteractingWithDrawingObjs())
				{
					double d = 0.0;
					double d2 = 0.0;
					this.ConvertWindowsCoordinatesToHImage(this._last_position.Y, this._last_position.X, out d, out d2);
					this._hwindow.SendMouseDownEvent(d, d2, this.MouseEventToInt(e));
				}
				e2 = this.ToHMouse((int)this._last_position.X, (int)this._last_position.Y, e, 0);
			}
			catch (HalconException he)
			{
				if (this.HErrorNotify != null)
				{
					this.HErrorNotify(he);
				}
			}
			if (this.HMouseDown != null)
			{
				this.HMouseDown(this, e2);
			}
		}

		private Rect Part2Rect()
		{
			HTuple hTuple = null;
			HTuple hTuple2 = null;
			HTuple t = null;
			HTuple t2 = null;
			this._hwindow.GetPart(out hTuple, out hTuple2, out t, out t2);
			return new Rect(hTuple2, hTuple, t2 - hTuple2 + 1, t - hTuple + 1);
		}

		public void SetFullImagePart(HImage reference = null)
		{
			if (base.ActualHeight != 0.0 && base.ActualWidth != 0.0)
			{
				if (reference != null)
				{
					int num = 0;
					int num2 = 0;
					reference.GetImageSize(out num, out num2);
					this.HImagePart = new Rect(0.0, 0.0, (double)(num - 1), (double)(num2 - 1));
				}
				else if (this.HKeepAspectRatio)
				{
					this._hwindow.SetPart(0, 0, -2, -2);
					this.HImagePart = this.Part2Rect();
				}
				else
				{
					this._hwindow.SetPart(0, 0, -1, -1);
					this.HImagePart = this.Part2Rect();
				}
			}
		}

		private void HWPFWindow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			HMouseEventArgsWPF e2 = null;
			try
			{
				bool flag = false;
				this._last_position = this.GetPosition(e);
				if (e.ChangedButton == MouseButton.Left && this.InteractingWithDrawingObjs())
				{
					double d = 0.0;
					double d2 = 0.0;
					this.ConvertWindowsCoordinatesToHImage(this._last_position.Y, this._last_position.X, out d, out d2);
					HTuple hTuple = this._hwindow.SendMouseDoubleClickEvent(d, d2, this.MouseEventToInt(e));
					flag = hTuple[0].S.Equals("true");
				}
				if (!flag && this.HDoubleClickToFitContent)
				{
					this.SetFullImagePart(null);
				}
				e2 = this.ToHMouse((int)this._last_position.X, (int)this._last_position.Y, e, 0);
			}
			catch (HalconException he)
			{
				if (this.HErrorNotify != null)
				{
					this.HErrorNotify(he);
				}
			}
			if (this.HMouseDoubleClick != null)
			{
				this.HMouseDoubleClick(this, e2);
			}
		}

		private void HWPFWindow_MouseUp(object sender, MouseButtonEventArgs e)
		{
			HMouseEventArgsWPF e2 = null;
			try
			{
				this._left_button_down = false;
				this._last_position = this.GetPosition(e);
				if (this.InteractingWithDrawingObjs())
				{
					double d = 0.0;
					double d2 = 0.0;
					this.ConvertWindowsCoordinatesToHImage(this._last_position.Y, this._last_position.X, out d, out d2);
					this._hwindow.SendMouseUpEvent(d, d2, this.MouseEventToInt(e));
				}
				e2 = this.ToHMouse((int)this._last_position.X, (int)this._last_position.Y, e, 0);
			}
			catch (HalconException he)
			{
				if (this.HErrorNotify != null)
				{
					this.HErrorNotify(he);
				}
			}
			if (this.HMouseUp != null)
			{
				this.HMouseUp(this, e2);
			}
		}

		private bool calculate_part(HTuple hv_WindowHandle, HTuple hv_WindowWidth, HTuple hv_WindowHeight)
		{
			HTuple hTuple = null;
			HTuple hTuple2 = null;
			HTuple hTuple3 = null;
			HTuple hTuple4 = null;
			HTuple hTuple5 = null;
			HTuple hTuple6 = null;
			HTuple hTuple7 = null;
			HTuple hTuple8 = null;
			HTuple t = null;
			HTuple t2 = null;
			HTuple hTuple9 = null;
			HTuple hTuple10 = null;
			HTuple hTuple11 = null;
			HTuple homMat2D = null;
			HTuple homMat2D2 = null;
			HTuple hTuple12 = null;
			HTuple hTuple13 = null;
			bool result = true;
			HOperatorSet.GetPart(hv_WindowHandle, out hTuple, out hTuple2, out hTuple3, out hTuple4);
			try
			{
				hTuple5 = hTuple4 - hTuple2 + 1;
				hTuple6 = hTuple3 - hTuple + 1;
				HTuple hTuple14 = hTuple5 / hTuple6.TupleReal();
				HOperatorSet.GetWindowExtents(hv_WindowHandle, out hTuple7, out hTuple8, out t, out t2);
				hTuple9 = t / hv_WindowWidth.TupleReal();
				hTuple10 = t2 / hv_WindowHeight.TupleReal();
				hTuple11 = new HTuple();
				hTuple11 = hTuple11.TupleConcat((hTuple + hTuple3) * 0.5);
				hTuple11 = hTuple11.TupleConcat((hTuple2 + hTuple4) * 0.5);
				HOperatorSet.HomMat2dIdentity(out homMat2D);
				HOperatorSet.HomMat2dScale(homMat2D, hTuple9, hTuple10, hTuple11.TupleSelect(1), hTuple11.TupleSelect(0), out homMat2D2);
				HOperatorSet.AffineTransPoint2d(homMat2D2, hTuple2.TupleConcat(hTuple4), hTuple.TupleConcat(hTuple3), out hTuple12, out hTuple13);
				this.HImagePart = new Rect(hTuple12.TupleSelect(0), hTuple13.TupleSelect(0), hTuple12.TupleSelect(1) - hTuple12.TupleSelect(0) + 1, hTuple13.TupleSelect(1) - hTuple13.TupleSelect(0) + 1);
				return result;
			}
			catch (HalconException)
			{
				this.HImagePart = new Rect(hTuple2, hTuple, hTuple4 - hTuple2 + 1, hTuple3 - hTuple + 1);
				return false;
			}
		}

		private void HWPFWindow_MouseLeave(object sender, MouseEventArgs e)
		{
			this._left_button_down = false;
		}

		protected internal void OnPropertyChanged(string propertyname)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!this._contentLoaded)
			{
				this._contentLoaded = true;
				Uri resourceLocator = new Uri("/halcondotnet;component/hsmartwindowcontrolwpf/hsmartwindowcontrolwpf.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				this.HWindowControlWPF = (HSmartWindowControlWPF)target;
				this.HWindowControlWPF.MouseWheel += this.HWPFWindow_MouseWheel;
				this.HWindowControlWPF.MouseMove += this.HWPFWindow_MouseMove;
				this.HWindowControlWPF.MouseDown += this.HWPFWindow_MouseDown;
				this.HWindowControlWPF.MouseUp += this.HWPFWindow_MouseUp;
				this.HWindowControlWPF.MouseDoubleClick += this.HWPFWindow_MouseDoubleClick;
				this.HWindowControlWPF.MouseLeave += this.HWPFWindow_MouseLeave;
				break;
			case 2:
				this.WindowFrame = (Viewbox)target;
				this.WindowFrame.MouseLeave += this.HWPFWindow_MouseLeave;
				break;
			default:
				this._contentLoaded = true;
				break;
			}
		}
	}
}
