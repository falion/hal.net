using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HSurfaceModel : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSurfaceModel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSurfaceModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSurfaceModel obj)
		{
			obj = new HSurfaceModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSurfaceModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HSurfaceModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HSurfaceModel(hTuple[i].IP);
			}
			return err;
		}

		public HSurfaceModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1039);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1044);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public HSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1044);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeSurfaceModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSurfaceModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeSurfaceModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeSurfaceModel().Serialize(stream);
		}

		public static HSurfaceModel Deserialize(Stream stream)
		{
			HSurfaceModel hSurfaceModel = new HSurfaceModel();
			hSurfaceModel.DeserializeSurfaceModel(HSerializedItem.Deserialize(stream));
			return hSurfaceModel;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HSurfaceModel Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeSurfaceModel();
			HSurfaceModel hSurfaceModel = new HSurfaceModel();
			hSurfaceModel.DeserializeSurfaceModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hSurfaceModel;
		}

		public void DeserializeSurfaceModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1037);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeSurfaceModel()
		{
			IntPtr proc = HalconAPI.PreCall(1038);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadSurfaceModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1039);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteSurfaceModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1040);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HPose[] RefineSurfaceModelPose(HObjectModel3D objectModel3D, HPose[] initialPose, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			HTuple hTuple = HData.ConcatArray(initialPose);
			IntPtr proc = HalconAPI.PreCall(1041);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 2, hTuple);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose RefineSurfaceModelPose(HObjectModel3D objectModel3D, HPose initialPose, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(1041);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 2, initialPose);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(initialPose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose[] FindSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, double keyPointFraction, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(1042);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.Store(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose FindSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, double keyPointFraction, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(1042);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.StoreD(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HTuple GetSurfaceModelParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1043);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetSurfaceModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1043);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void CreateSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1044);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public void CreateSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, string genParamName, string genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1044);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public HPose[] FindSurfaceModelImage(HImage image, HObjectModel3D objectModel3D, double relSamplingDistance, double keyPointFraction, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(2069);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.Store(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose FindSurfaceModelImage(HImage image, HObjectModel3D objectModel3D, double relSamplingDistance, double keyPointFraction, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(2069);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.StoreD(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose[] RefineSurfaceModelPoseImage(HImage image, HObjectModel3D objectModel3D, HPose[] initialPose, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			HTuple hTuple = HData.ConcatArray(initialPose);
			IntPtr proc = HalconAPI.PreCall(2084);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 2, hTuple);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(data);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose RefineSurfaceModelPoseImage(HImage image, HObjectModel3D objectModel3D, HPose initialPose, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(2084);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 2, initialPose);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(initialPose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = HSurfaceMatchingResult.LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public void SetSurfaceModelParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2097);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetSurfaceModelParam(string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2097);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1036);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
