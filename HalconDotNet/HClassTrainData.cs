using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HClassTrainData : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassTrainData()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassTrainData(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassTrainData obj)
		{
			obj = new HClassTrainData(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassTrainData[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HClassTrainData[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HClassTrainData(hTuple[i].IP);
			}
			return err;
		}

		public HClassTrainData(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1781);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassTrainData(int numDim)
		{
			IntPtr proc = HalconAPI.PreCall(1798);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeClassTrainData();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassTrainData(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeClassTrainData(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeClassTrainData().Serialize(stream);
		}

		public static HClassTrainData Deserialize(Stream stream)
		{
			HClassTrainData hClassTrainData = new HClassTrainData();
			hClassTrainData.DeserializeClassTrainData(HSerializedItem.Deserialize(stream));
			return hClassTrainData;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HClassTrainData Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeClassTrainData();
			HClassTrainData hClassTrainData = new HClassTrainData();
			hClassTrainData.DeserializeClassTrainData(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassTrainData;
		}

		public void DeserializeClassTrainData(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1779);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeClassTrainData()
		{
			IntPtr proc = HalconAPI.PreCall(1780);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadClassTrainData(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1781);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteClassTrainData(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1782);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HClassTrainData SelectSubFeatureClassTrainData(HTuple subFeatureIndices)
		{
			IntPtr proc = HalconAPI.PreCall(1783);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, subFeatureIndices);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subFeatureIndices);
			HClassTrainData result = null;
			err = HClassTrainData.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetFeatureLengthsClassTrainData(HTuple subFeatureLength, HTuple names)
		{
			IntPtr proc = HalconAPI.PreCall(1784);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, subFeatureLength);
			HalconAPI.Store(proc, 2, names);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subFeatureLength);
			HalconAPI.UnpinTuple(names);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GetClassTrainDataGmm(HClassGmm GMMHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1785);
			HalconAPI.Store(proc, 0, GMMHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(GMMHandle);
		}

		public void AddClassTrainDataGmm(HClassGmm GMMHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1786);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, GMMHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(GMMHandle);
		}

		public void GetClassTrainDataMlp(HClassMlp MLPHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1787);
			HalconAPI.Store(proc, 0, MLPHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(MLPHandle);
		}

		public void AddClassTrainDataMlp(HClassMlp MLPHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1788);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, MLPHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(MLPHandle);
		}

		public void GetClassTrainDataKnn(HClassKnn KNNHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1789);
			HalconAPI.Store(proc, 0, KNNHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(KNNHandle);
		}

		public void AddClassTrainDataKnn(HClassKnn KNNHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1790);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, KNNHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(KNNHandle);
		}

		public void GetClassTrainDataSvm(HClassSvm SVMHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1791);
			HalconAPI.Store(proc, 0, SVMHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SVMHandle);
		}

		public void AddClassTrainDataSvm(HClassSvm SVMHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1792);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, SVMHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(SVMHandle);
		}

		public int GetSampleNumClassTrainData()
		{
			IntPtr proc = HalconAPI.PreCall(1793);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetSampleClassTrainData(int indexSample, out int classID)
		{
			IntPtr proc = HalconAPI.PreCall(1794);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, indexSample);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out classID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void AddSampleClassTrainData(string order, HTuple features, HTuple classID)
		{
			IntPtr proc = HalconAPI.PreCall(1797);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, order);
			HalconAPI.Store(proc, 2, features);
			HalconAPI.Store(proc, 3, classID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(classID);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateClassTrainData(int numDim)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1798);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassMlp SelectFeatureSetMlp(string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1799);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HClassMlp result = null;
			err = HClassMlp.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HClassMlp SelectFeatureSetMlp(string selectionMethod, string genParamName, double genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1799);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HClassMlp result = null;
			err = HClassMlp.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HClassSvm SelectFeatureSetSvm(string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1800);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HClassSvm result = null;
			err = HClassSvm.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HClassSvm SelectFeatureSetSvm(string selectionMethod, string genParamName, double genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1800);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HClassSvm result = null;
			err = HClassSvm.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HClassGmm SelectFeatureSetGmm(string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1801);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HClassGmm result = null;
			err = HClassGmm.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HClassGmm SelectFeatureSetGmm(string selectionMethod, string genParamName, double genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1801);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HClassGmm result = null;
			err = HClassGmm.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HClassKnn SelectFeatureSetKnn(string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1802);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HClassKnn result = null;
			err = HClassKnn.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HClassKnn SelectFeatureSetKnn(string selectionMethod, string genParamName, double genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1802);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HClassKnn result = null;
			err = HClassKnn.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1796);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
