using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HSheetOfLightModel : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSheetOfLightModel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSheetOfLightModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSheetOfLightModel obj)
		{
			obj = new HSheetOfLightModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSheetOfLightModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HSheetOfLightModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HSheetOfLightModel(hTuple[i].IP);
			}
			return err;
		}

		public HSheetOfLightModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(374);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSheetOfLightModel(HRegion profileRegion, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(391);
			HalconAPI.Store(proc, 1, profileRegion);
			HalconAPI.Store(proc, 0, genParamName);
			HalconAPI.Store(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(profileRegion);
		}

		public HSheetOfLightModel(HRegion profileRegion, string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(391);
			HalconAPI.Store(proc, 1, profileRegion);
			HalconAPI.StoreS(proc, 0, genParamName);
			HalconAPI.StoreI(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(profileRegion);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeSheetOfLightModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSheetOfLightModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeSheetOfLightModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeSheetOfLightModel().Serialize(stream);
		}

		public static HSheetOfLightModel Deserialize(Stream stream)
		{
			HSheetOfLightModel hSheetOfLightModel = new HSheetOfLightModel();
			hSheetOfLightModel.DeserializeSheetOfLightModel(HSerializedItem.Deserialize(stream));
			return hSheetOfLightModel;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HSheetOfLightModel Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeSheetOfLightModel();
			HSheetOfLightModel hSheetOfLightModel = new HSheetOfLightModel();
			hSheetOfLightModel.DeserializeSheetOfLightModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hSheetOfLightModel;
		}

		public void ReadSheetOfLightModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(374);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteSheetOfLightModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(375);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeSheetOfLightModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(376);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeSheetOfLightModel()
		{
			IntPtr proc = HalconAPI.PreCall(377);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double CalibrateSheetOfLight()
		{
			IntPtr proc = HalconAPI.PreCall(379);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HObjectModel3D GetSheetOfLightResultObjectModel3d()
		{
			IntPtr proc = HalconAPI.PreCall(380);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GetSheetOfLightResult(HTuple resultName)
		{
			IntPtr proc = HalconAPI.PreCall(381);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultName);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GetSheetOfLightResult(string resultName)
		{
			IntPtr proc = HalconAPI.PreCall(381);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ApplySheetOfLightCalibration(HImage disparity)
		{
			IntPtr proc = HalconAPI.PreCall(382);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, disparity);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(disparity);
		}

		public void SetProfileSheetOfLight(HImage profileDisparityImage, HTuple movementPoses)
		{
			IntPtr proc = HalconAPI.PreCall(383);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, profileDisparityImage);
			HalconAPI.Store(proc, 1, movementPoses);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(movementPoses);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(profileDisparityImage);
		}

		public void MeasureProfileSheetOfLight(HImage profileImage, HTuple movementPose)
		{
			IntPtr proc = HalconAPI.PreCall(384);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, profileImage);
			HalconAPI.Store(proc, 1, movementPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(movementPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(profileImage);
		}

		public void SetSheetOfLightParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(385);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetSheetOfLightParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(385);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetSheetOfLightParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(386);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QuerySheetOfLightParams(string queryName)
		{
			IntPtr proc = HalconAPI.PreCall(387);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, queryName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ResetSheetOfLightModel()
		{
			IntPtr proc = HalconAPI.PreCall(388);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateSheetOfLightModel(HRegion profileRegion, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(391);
			HalconAPI.Store(proc, 1, profileRegion);
			HalconAPI.Store(proc, 0, genParamName);
			HalconAPI.Store(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(profileRegion);
		}

		public void CreateSheetOfLightModel(HRegion profileRegion, string genParamName, int genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(391);
			HalconAPI.Store(proc, 1, profileRegion);
			HalconAPI.StoreS(proc, 0, genParamName);
			HalconAPI.StoreI(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(profileRegion);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(390);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
