using System;
using System.ComponentModel;

namespace VisionConfig
{
	[Serializable]
	public class HXLDPoly : HXLD
	{
		public new HXLDPoly this[HTuple index]
		{
			get
			{
				return this.SelectObj(index);
			}
		}

		public HXLDPoly()
			: base(HObjectBase.UNDEF, false)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDPoly(IntPtr key)
			: this(key, true)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDPoly(IntPtr key, bool copy)
			: base(key, copy)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDPoly(HObject obj)
			: base(obj)
		{
			this.AssertObjectClass();
			GC.KeepAlive(this);
		}

		private void AssertObjectClass()
		{
			HalconAPI.AssertObjectClass(base.key, "xld_poly");
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, int err, out HXLDPoly obj)
		{
			obj = new HXLDPoly(HObjectBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		public HXLDPoly Union2ClosedPolygonsXld(HXLDPoly polygons2)
		{
			IntPtr proc = HalconAPI.PreCall(5);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, polygons2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(polygons2);
			return result;
		}

		public HXLDPoly SymmDifferenceClosedPolygonsXld(HXLDPoly polygons2)
		{
			IntPtr proc = HalconAPI.PreCall(7);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, polygons2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(polygons2);
			return result;
		}

		public HXLDPoly DifferenceClosedPolygonsXld(HXLDPoly sub)
		{
			IntPtr proc = HalconAPI.PreCall(9);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, sub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sub);
			return result;
		}

		public HXLDPoly IntersectionClosedPolygonsXld(HXLDPoly polygons2)
		{
			IntPtr proc = HalconAPI.PreCall(11);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, polygons2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(polygons2);
			return result;
		}

		public void ReadPolygonXldArcInfo(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(18);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WritePolygonXldArcInfo(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(19);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HXLDPoly CombineRoadsXld(HXLDModPara modParallels, HXLDExtPara extParallels, HXLDPoly centerLines, HTuple maxAngleParallel, HTuple maxAngleColinear, HTuple maxDistanceParallel, HTuple maxDistanceColinear)
		{
			IntPtr proc = HalconAPI.PreCall(37);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, modParallels);
			HalconAPI.Store(proc, 3, extParallels);
			HalconAPI.Store(proc, 4, centerLines);
			HalconAPI.Store(proc, 0, maxAngleParallel);
			HalconAPI.Store(proc, 1, maxAngleColinear);
			HalconAPI.Store(proc, 2, maxDistanceParallel);
			HalconAPI.Store(proc, 3, maxDistanceColinear);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maxAngleParallel);
			HalconAPI.UnpinTuple(maxAngleColinear);
			HalconAPI.UnpinTuple(maxDistanceParallel);
			HalconAPI.UnpinTuple(maxDistanceColinear);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modParallels);
			GC.KeepAlive(extParallels);
			GC.KeepAlive(centerLines);
			return result;
		}

		public HXLDPoly CombineRoadsXld(HXLDModPara modParallels, HXLDExtPara extParallels, HXLDPoly centerLines, double maxAngleParallel, double maxAngleColinear, double maxDistanceParallel, double maxDistanceColinear)
		{
			IntPtr proc = HalconAPI.PreCall(37);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, modParallels);
			HalconAPI.Store(proc, 3, extParallels);
			HalconAPI.Store(proc, 4, centerLines);
			HalconAPI.StoreD(proc, 0, maxAngleParallel);
			HalconAPI.StoreD(proc, 1, maxAngleColinear);
			HalconAPI.StoreD(proc, 2, maxDistanceParallel);
			HalconAPI.StoreD(proc, 3, maxDistanceColinear);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(modParallels);
			GC.KeepAlive(extParallels);
			GC.KeepAlive(centerLines);
			return result;
		}

		public HXLDPara GenParallelsXld(HTuple len, HTuple dist, HTuple alpha, string merge)
		{
			IntPtr proc = HalconAPI.PreCall(42);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, len);
			HalconAPI.Store(proc, 1, dist);
			HalconAPI.Store(proc, 2, alpha);
			HalconAPI.StoreS(proc, 3, merge);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(len);
			HalconAPI.UnpinTuple(dist);
			HalconAPI.UnpinTuple(alpha);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPara GenParallelsXld(double len, double dist, double alpha, string merge)
		{
			IntPtr proc = HalconAPI.PreCall(42);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, len);
			HalconAPI.StoreD(proc, 1, dist);
			HalconAPI.StoreD(proc, 2, alpha);
			HalconAPI.StoreS(proc, 3, merge);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPara result = null;
			err = HXLDPara.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetLinesXld(out HTuple beginRow, out HTuple beginCol, out HTuple endRow, out HTuple endCol, out HTuple length, out HTuple phi)
		{
			IntPtr proc = HalconAPI.PreCall(43);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out beginRow);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out beginCol);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out endRow);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out endCol);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out length);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetPolygonXld(out HTuple row, out HTuple col, out HTuple length, out HTuple phi)
		{
			IntPtr proc = HalconAPI.PreCall(44);
			base.Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out col);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out length);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont SplitContoursXld(string mode, int weight, int smooth)
		{
			IntPtr proc = HalconAPI.PreCall(46);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, weight);
			HalconAPI.StoreI(proc, 2, smooth);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPoly AffineTransPolygonXld(HHomMat2D homMat2D)
		{
			IntPtr proc = HalconAPI.PreCall(48);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPoly ObjDiff(HXLDPoly objectsSub)
		{
			IntPtr proc = HalconAPI.PreCall(573);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsSub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsSub);
			return result;
		}

		public new HXLDPoly CopyObj(int index, int numObj)
		{
			IntPtr proc = HalconAPI.PreCall(583);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.StoreI(proc, 1, numObj);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPoly ConcatObj(HXLDPoly objects2)
		{
			IntPtr proc = HalconAPI.PreCall(584);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public new HXLDPoly SelectObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPoly SelectObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int CompareObj(HXLDPoly objects2, HTuple epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.Store(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(epsilon);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public int CompareObj(HXLDPoly objects2, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.StoreD(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public int TestEqualObj(HXLDPoly objects2)
		{
			IntPtr proc = HalconAPI.PreCall(591);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return result;
		}

		public HRegion GenRegionPolygonXld(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(596);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLDPoly meshes, int gridSpacing, HTuple rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.Store(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HXLDPoly.LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLDPoly meshes, int gridSpacing, string rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.StoreS(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HXLDPoly.LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HTuple ReadPolygonXldDxf(string fileName, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1634);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 1, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public string ReadPolygonXldDxf(string fileName, string genParamName, double genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1634);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void WritePolygonXldDxf(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1635);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public new HXLDPoly SelectXldPoint(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPoly SelectXldPoint(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			base.Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPoly SelectShapeXld(HTuple features, string operation, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.Store(proc, 2, min);
			HalconAPI.Store(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPoly SelectShapeXld(string features, string operation, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.StoreD(proc, 2, min);
			HalconAPI.StoreD(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPoly ShapeTransXld(string type)
		{
			IntPtr proc = HalconAPI.PreCall(1689);
			base.Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPoly InsertObj(HXLDPoly objectsInsert, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2121);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsInsert);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsInsert);
			return result;
		}

		public new HXLDPoly RemoveObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public new HXLDPoly RemoveObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			base.Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HXLDPoly ReplaceObj(HXLDPoly objectsReplace, HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return result;
		}

		public HXLDPoly ReplaceObj(HXLDPoly objectsReplace, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDPoly result = null;
			err = HXLDPoly.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return result;
		}
	}
}
