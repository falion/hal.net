using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace VisionConfig
{
	internal class HTupleInt64 : HTupleImplementation
	{
		protected long[] l;

		public override long[] LArr
		{
			get
			{
				return this.l;
			}
			set
			{
				base.SetArray(value, false);
			}
		}

		public override HTupleType Type
		{
			get
			{
				return HTupleType.LONG;
			}
		}

		protected override Array CreateArray(int size)
		{
			return new long[size];
		}

		protected override void NotifyArrayUpdate()
		{
			this.l = (long[])base.data;
		}

		internal override void PinTuple()
		{
			Monitor.Enter(this);
			if (base.pinCount == 0)
			{
				base.pinHandle = GCHandle.Alloc(this.l, GCHandleType.Pinned);
			}
			base.pinCount++;
			Monitor.Exit(this);
		}

		public HTupleInt64(long l)
		{
			base.SetArray(new long[1]
			{
				l
			}, false);
		}

		public HTupleInt64(long[] l, bool copy)
		{
			base.SetArray(l, copy);
		}

		public override HTupleElements GetElement(int index, HTuple parent)
		{
			return new HTupleElements(parent, this, index);
		}

		public override HTupleElements GetElements(int[] indices, HTuple parent)
		{
			if (indices != null && indices.Length != 0)
			{
				return new HTupleElements(parent, this, indices);
			}
			return new HTupleElements();
		}

		public override void SetElements(int[] indices, HTupleElements elements)
		{
			if (indices == null)
			{
				return;
			}
			if (indices.Length == 0)
			{
				return;
			}
			long[] lArr = elements.LArr;
			if (lArr.Length == indices.Length)
			{
				for (int i = 0; i < indices.Length; i++)
				{
					this.l[indices[i]] = lArr[i];
				}
				return;
			}
			if (lArr.Length == 1)
			{
				for (int j = 0; j < indices.Length; j++)
				{
					this.l[indices[j]] = lArr[0];
				}
				return;
			}
			throw new HTupleAccessException(this, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
		}

		public override int[] ToIArr()
		{
			int[] array = new int[base.iLength];
			for (int i = 0; i < base.iLength; i++)
			{
				array[i] = (int)this.l[i];
			}
			return array;
		}

		public override long[] ToLArr()
		{
			return (long[])base.ToArray(base.typeL);
		}

		public override double[] ToDArr()
		{
			return (double[])base.ToArray(base.typeD);
		}

		public override float[] ToFArr()
		{
			return (float[])base.ToArray(base.typeF);
		}

		public override IntPtr[] ToIPArr()
		{
			if (!HalconAPI.isPlatform64)
			{
				base.ToIPArr();
			}
			IntPtr[] array = new IntPtr[base.iLength];
			for (int i = 0; i < base.iLength; i++)
			{
				array[i] = new IntPtr(this.l[i]);
			}
			return array;
		}

		public override void Store(IntPtr proc, int parIndex)
		{
			IntPtr tuple = default(IntPtr);
			HalconAPI.HCkP(proc, HalconAPI.GetInputTuple(proc, parIndex, out tuple));
			this.StoreData(proc, tuple);
		}

		protected override void StoreData(IntPtr proc, IntPtr tuple)
		{
			this.PinTuple();
			if (!HalconAPI.isPlatform64)
			{
				HalconAPI.HCkP(proc, HalconAPI.CreateElements(tuple, base.Length));
				for (int i = 0; i < base.Length; i++)
				{
					HalconAPI.SetL(tuple, i, this.l[i]);
				}
			}
			else
			{
				HalconAPI.SetLArrPtr(tuple, this.l, base.iLength);
			}
		}

		public static int Load(IntPtr tuple, out HTupleInt64 data)
		{
			int num = 0;
			HalconAPI.GetTupleLength(tuple, out num);
			long[] longArray = new long[num];
			int lArr = HalconAPI.GetLArr(tuple, longArray);
			data = new HTupleInt64(longArray, false);
			return lArr;
		}
	}
}
