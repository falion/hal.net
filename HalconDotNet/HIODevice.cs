using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HIODevice : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HIODevice()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HIODevice(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HIODevice obj)
		{
			obj = new HIODevice(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HIODevice[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HIODevice[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HIODevice(hTuple[i].IP);
			}
			return err;
		}

		public HIODevice(string IOInterfaceName, HTuple IODeviceName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2022);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.Store(proc, 1, IODeviceName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(IODeviceName);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HIOChannel[] OpenIoChannel(HTuple IOChannelName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2016);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(IOChannelName);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HIOChannel[] result = null;
			err = HIOChannel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HIOChannel OpenIoChannel(string IOChannelName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2016);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HIOChannel result = null;
			err = HIOChannel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryIoDevice(HTuple IOChannelName, HTuple query)
		{
			IntPtr proc = HalconAPI.PreCall(2017);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(IOChannelName);
			HalconAPI.UnpinTuple(query);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple QueryIoDevice(string IOChannelName, HTuple query)
		{
			IntPtr proc = HalconAPI.PreCall(2017);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(query);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ControlIoDevice(string action, HTuple argument)
		{
			IntPtr proc = HalconAPI.PreCall(2018);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, action);
			HalconAPI.Store(proc, 2, argument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(argument);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ControlIoDevice(string action, string argument)
		{
			IntPtr proc = HalconAPI.PreCall(2018);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, action);
			HalconAPI.StoreS(proc, 2, argument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetIoDeviceParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2019);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetIoDeviceParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2019);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetIoDeviceParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2020);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetIoDeviceParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2020);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void OpenIoDevice(string IOInterfaceName, HTuple IODeviceName, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(2022);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.Store(proc, 1, IODeviceName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(IODeviceName);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HTuple ControlIoInterface(string IOInterfaceName, string action, HTuple argument)
		{
			IntPtr proc = HalconAPI.PreCall(2023);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.StoreS(proc, 1, action);
			HalconAPI.Store(proc, 2, argument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(argument);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple ControlIoInterface(string IOInterfaceName, string action, string argument)
		{
			IntPtr proc = HalconAPI.PreCall(2023);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.StoreS(proc, 1, action);
			HalconAPI.StoreS(proc, 2, argument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple QueryIoInterface(string IOInterfaceName, HTuple query)
		{
			IntPtr proc = HalconAPI.PreCall(2024);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.Store(proc, 1, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(query);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple QueryIoInterface(string IOInterfaceName, string query)
		{
			IntPtr proc = HalconAPI.PreCall(2024);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.StoreS(proc, 1, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(2021);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
