using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace VisionConfig
{
	internal class HTupleInt32 : HTupleImplementation
	{
		protected int[] i;

		public override int[] IArr
		{
			get
			{
				return this.i;
			}
			set
			{
				base.SetArray(value, false);
			}
		}

		public override HTupleType Type
		{
			get
			{
				return HTupleType.INTEGER;
			}
		}

		protected override Array CreateArray(int size)
		{
			return new int[size];
		}

		protected override void NotifyArrayUpdate()
		{
			this.i = (int[])base.data;
		}

		internal override void PinTuple()
		{
			Monitor.Enter(this);
			if (base.pinCount == 0)
			{
				base.pinHandle = GCHandle.Alloc(this.i, GCHandleType.Pinned);
			}
			base.pinCount++;
			Monitor.Exit(this);
		}

		public HTupleInt32(int i)
		{
			base.SetArray(new int[1]
			{
				i
			}, false);
		}

		public HTupleInt32(int[] i, bool copy)
		{
			base.SetArray(i, copy);
		}

		public override HTupleElements GetElement(int index, HTuple parent)
		{
			return new HTupleElements(parent, this, index);
		}

		public override HTupleElements GetElements(int[] indices, HTuple parent)
		{
			if (indices != null && indices.Length != 0)
			{
				return new HTupleElements(parent, this, indices);
			}
			return new HTupleElements();
		}

		public override void SetElements(int[] indices, HTupleElements elements)
		{
			if (indices == null)
			{
				return;
			}
			if (indices.Length == 0)
			{
				return;
			}
			int[] iArr = elements.IArr;
			if (iArr.Length == indices.Length)
			{
				for (int i = 0; i < indices.Length; i++)
				{
					this.i[indices[i]] = iArr[i];
				}
				return;
			}
			if (iArr.Length == 1)
			{
				for (int j = 0; j < indices.Length; j++)
				{
					this.i[indices[j]] = iArr[0];
				}
				return;
			}
			throw new HTupleAccessException(this, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
		}

		public override int[] ToIArr()
		{
			return (int[])base.ToArray(base.typeI);
		}

		public override long[] ToLArr()
		{
			return (long[])base.ToArray(base.typeL);
		}

		public override double[] ToDArr()
		{
			return (double[])base.ToArray(base.typeD);
		}

		public override float[] ToFArr()
		{
			return (float[])base.ToArray(base.typeF);
		}

		public override IntPtr[] ToIPArr()
		{
			if (HalconAPI.isPlatform64)
			{
				base.ToIPArr();
			}
			IntPtr[] array = new IntPtr[base.iLength];
			for (int i = 0; i < base.iLength; i++)
			{
				array[i] = new IntPtr(this.i[i]);
			}
			return array;
		}

		public override void Store(IntPtr proc, int parIndex)
		{
			IntPtr tuple = default(IntPtr);
			HalconAPI.HCkP(proc, HalconAPI.GetInputTuple(proc, parIndex, out tuple));
			this.StoreData(proc, tuple);
		}

		protected override void StoreData(IntPtr proc, IntPtr tuple)
		{
			this.PinTuple();
			if (HalconAPI.isPlatform64)
			{
				HalconAPI.HCkP(proc, HalconAPI.CreateElements(tuple, base.Length));
				for (int i = 0; i < base.Length; i++)
				{
					HalconAPI.SetI(tuple, i, this.i[i]);
				}
			}
			else
			{
				HalconAPI.SetIArrPtr(tuple, this.i, base.iLength);
			}
		}

		public static int Load(IntPtr tuple, out HTupleInt32 data)
		{
			int num = 0;
			HalconAPI.GetTupleLength(tuple, out num);
			int[] intArray = new int[num];
			int iArr = HalconAPI.GetIArr(tuple, intArray);
			data = new HTupleInt32(intArray, false);
			return iArr;
		}
	}
}
