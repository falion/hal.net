using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HDlClassifierResult : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifierResult()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifierResult(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlClassifierResult obj)
		{
			obj = new HDlClassifierResult(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlClassifierResult[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HDlClassifierResult[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HDlClassifierResult(hTuple[i].IP);
			}
			return err;
		}

		public HDlClassifierResult(HImage images, HDlClassifier DLClassifierHandle)
		{
			IntPtr proc = HalconAPI.PreCall(2102);
			HalconAPI.Store(proc, 1, images);
			HalconAPI.Store(proc, 0, DLClassifierHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(images);
			GC.KeepAlive(DLClassifierHandle);
		}

		public HTuple GetDlClassifierResult(HTuple index, HTuple genResultName)
		{
			IntPtr proc = HalconAPI.PreCall(2115);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, genResultName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(genResultName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetDlClassifierResult(string index, string genResultName)
		{
			IntPtr proc = HalconAPI.PreCall(2115);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.StoreS(proc, 2, genResultName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(2104);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
