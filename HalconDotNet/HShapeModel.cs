using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HShapeModel : HTool, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HShapeModel()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HShapeModel(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HShapeModel obj)
		{
			obj = new HShapeModel(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HShapeModel[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HShapeModel[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HShapeModel(hTuple[i].IP);
			}
			return err;
		}

		public HShapeModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(917);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HShapeModel(HXLDCont contours, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleRMin, double scaleRMax, HTuple scaleRStep, double scaleCMin, double scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(935);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public HShapeModel(HXLDCont contours, int numLevels, double angleStart, double angleExtent, double angleStep, double scaleRMin, double scaleRMax, double scaleRStep, double scaleCMin, double scaleCMax, double scaleCStep, string optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(935);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public HShapeModel(HXLDCont contours, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleMin, double scaleMax, HTuple scaleStep, HTuple optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(936);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.Store(proc, 6, scaleStep);
			HalconAPI.Store(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleStep);
			HalconAPI.UnpinTuple(optimization);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public HShapeModel(HXLDCont contours, int numLevels, double angleStart, double angleExtent, double angleStep, double scaleMin, double scaleMax, double scaleStep, string optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(936);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.StoreD(proc, 6, scaleStep);
			HalconAPI.StoreS(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public HShapeModel(HXLDCont contours, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, HTuple optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(937);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.Store(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(optimization);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public HShapeModel(HXLDCont contours, int numLevels, double angleStart, double angleExtent, double angleStep, string optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(937);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public HShapeModel(HImage template, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleRMin, double scaleRMax, HTuple scaleRStep, double scaleCMin, double scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(938);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.Store(proc, 13, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HShapeModel(HImage template, int numLevels, double angleStart, double angleExtent, double angleStep, double scaleRMin, double scaleRMax, double scaleRStep, double scaleCMin, double scaleCMax, double scaleCStep, string optimization, string metric, int contrast, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(938);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, contrast);
			HalconAPI.StoreI(proc, 13, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HShapeModel(HImage template, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleMin, double scaleMax, HTuple scaleStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(939);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.Store(proc, 6, scaleStep);
			HalconAPI.Store(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.Store(proc, 9, contrast);
			HalconAPI.Store(proc, 10, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HShapeModel(HImage template, int numLevels, double angleStart, double angleExtent, double angleStep, double scaleMin, double scaleMax, double scaleStep, string optimization, string metric, int contrast, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(939);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.StoreD(proc, 6, scaleStep);
			HalconAPI.StoreS(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, contrast);
			HalconAPI.StoreI(proc, 10, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HShapeModel(HImage template, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(940);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.Store(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.Store(proc, 6, contrast);
			HalconAPI.Store(proc, 7, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HShapeModel(HImage template, int numLevels, double angleStart, double angleExtent, double angleStep, string optimization, string metric, int contrast, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(940);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, contrast);
			HalconAPI.StoreI(proc, 7, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializeShapeModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HShapeModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializeShapeModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializeShapeModel().Serialize(stream);
		}

		public static HShapeModel Deserialize(Stream stream)
		{
			HShapeModel hShapeModel = new HShapeModel();
			hShapeModel.DeserializeShapeModel(HSerializedItem.Deserialize(stream));
			return hShapeModel;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HShapeModel Clone()
		{
			HSerializedItem hSerializedItem = this.SerializeShapeModel();
			HShapeModel hShapeModel = new HShapeModel();
			hShapeModel.DeserializeShapeModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hShapeModel;
		}

		public void DeserializeShapeModel(HSerializedItem serializedItemHandle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(916);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public void ReadShapeModel(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(917);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem SerializeShapeModel()
		{
			IntPtr proc = HalconAPI.PreCall(918);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void WriteShapeModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(919);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HXLDCont GetShapeModelContours(int level)
		{
			IntPtr proc = HalconAPI.PreCall(922);
			base.Store(proc, 0);
			HalconAPI.StoreI(proc, 1, level);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetShapeModelParams(out double angleStart, out double angleExtent, out double angleStep, out HTuple scaleMin, out HTuple scaleMax, out HTuple scaleStep, out string metric, out int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(924);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out angleStart);
			err = HalconAPI.LoadD(proc, 2, err, out angleExtent);
			err = HalconAPI.LoadD(proc, 3, err, out angleStep);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out scaleMin);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out scaleMax);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out scaleStep);
			err = HalconAPI.LoadS(proc, 7, err, out metric);
			err = HalconAPI.LoadI(proc, 8, err, out minContrast);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetShapeModelParams(out double angleStart, out double angleExtent, out double angleStep, out double scaleMin, out double scaleMax, out double scaleStep, out string metric, out int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(924);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			err = HalconAPI.LoadD(proc, 1, err, out angleStart);
			err = HalconAPI.LoadD(proc, 2, err, out angleExtent);
			err = HalconAPI.LoadD(proc, 3, err, out angleStep);
			err = HalconAPI.LoadD(proc, 4, err, out scaleMin);
			err = HalconAPI.LoadD(proc, 5, err, out scaleMax);
			err = HalconAPI.LoadD(proc, 6, err, out scaleStep);
			err = HalconAPI.LoadS(proc, 7, err, out metric);
			err = HalconAPI.LoadI(proc, 8, err, out minContrast);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void GetShapeModelOrigin(out double row, out double column)
		{
			IntPtr proc = HalconAPI.PreCall(925);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SetShapeModelOrigin(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(926);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void FindAnisoShapeModels(HImage image, HShapeModel[] modelIDs, HTuple angleStart, HTuple angleExtent, HTuple scaleRMin, HTuple scaleRMax, HTuple scaleCMin, HTuple scaleCMax, HTuple minScore, HTuple numMatches, HTuple maxOverlap, HTuple subPixel, HTuple numLevels, HTuple greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scaleR, out HTuple scaleC, out HTuple score, out HTuple model)
		{
			HTuple hTuple = HTool.ConcatArray(modelIDs);
			IntPtr proc = HalconAPI.PreCall(927);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, scaleRMin);
			HalconAPI.Store(proc, 4, scaleRMax);
			HalconAPI.Store(proc, 5, scaleCMin);
			HalconAPI.Store(proc, 6, scaleCMax);
			HalconAPI.Store(proc, 7, minScore);
			HalconAPI.Store(proc, 8, numMatches);
			HalconAPI.Store(proc, 9, maxOverlap);
			HalconAPI.Store(proc, 10, subPixel);
			HalconAPI.Store(proc, 11, numLevels);
			HalconAPI.Store(proc, 12, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMin);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMin);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(numMatches);
			HalconAPI.UnpinTuple(maxOverlap);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(greediness);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scaleR);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out scaleC);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 6, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(image);
			GC.KeepAlive(modelIDs);
		}

		public void FindAnisoShapeModels(HImage image, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scaleR, out HTuple scaleC, out HTuple score, out HTuple model)
		{
			IntPtr proc = HalconAPI.PreCall(927);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.StoreS(proc, 10, subPixel);
			HalconAPI.StoreI(proc, 11, numLevels);
			HalconAPI.StoreD(proc, 12, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scaleR);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out scaleC);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 6, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public static void FindScaledShapeModels(HImage image, HShapeModel[] modelIDs, HTuple angleStart, HTuple angleExtent, HTuple scaleMin, HTuple scaleMax, HTuple minScore, HTuple numMatches, HTuple maxOverlap, HTuple subPixel, HTuple numLevels, HTuple greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scale, out HTuple score, out HTuple model)
		{
			HTuple hTuple = HTool.ConcatArray(modelIDs);
			IntPtr proc = HalconAPI.PreCall(928);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, scaleMin);
			HalconAPI.Store(proc, 4, scaleMax);
			HalconAPI.Store(proc, 5, minScore);
			HalconAPI.Store(proc, 6, numMatches);
			HalconAPI.Store(proc, 7, maxOverlap);
			HalconAPI.Store(proc, 8, subPixel);
			HalconAPI.Store(proc, 9, numLevels);
			HalconAPI.Store(proc, 10, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleMin);
			HalconAPI.UnpinTuple(scaleMax);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(numMatches);
			HalconAPI.UnpinTuple(maxOverlap);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(greediness);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scale);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(image);
			GC.KeepAlive(modelIDs);
		}

		public void FindScaledShapeModels(HImage image, double angleStart, double angleExtent, double scaleMin, double scaleMax, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scale, out HTuple score, out HTuple model)
		{
			IntPtr proc = HalconAPI.PreCall(928);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleMin);
			HalconAPI.StoreD(proc, 4, scaleMax);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.StoreD(proc, 7, maxOverlap);
			HalconAPI.StoreS(proc, 8, subPixel);
			HalconAPI.StoreI(proc, 9, numLevels);
			HalconAPI.StoreD(proc, 10, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scale);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public static void FindShapeModels(HImage image, HShapeModel[] modelIDs, HTuple angleStart, HTuple angleExtent, HTuple minScore, HTuple numMatches, HTuple maxOverlap, HTuple subPixel, HTuple numLevels, HTuple greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple score, out HTuple model)
		{
			HTuple hTuple = HTool.ConcatArray(modelIDs);
			IntPtr proc = HalconAPI.PreCall(929);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, numMatches);
			HalconAPI.Store(proc, 5, maxOverlap);
			HalconAPI.Store(proc, 6, subPixel);
			HalconAPI.Store(proc, 7, numLevels);
			HalconAPI.Store(proc, 8, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(numMatches);
			HalconAPI.UnpinTuple(maxOverlap);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(greediness);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(image);
			GC.KeepAlive(modelIDs);
		}

		public void FindShapeModels(HImage image, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple score, out HTuple model)
		{
			IntPtr proc = HalconAPI.PreCall(929);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, subPixel);
			HalconAPI.StoreI(proc, 7, numLevels);
			HalconAPI.StoreD(proc, 8, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void FindAnisoShapeModel(HImage image, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, HTuple subPixel, HTuple numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scaleR, out HTuple scaleC, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(930);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.Store(proc, 10, subPixel);
			HalconAPI.Store(proc, 11, numLevels);
			HalconAPI.StoreD(proc, 12, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scaleR);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out scaleC);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void FindAnisoShapeModel(HImage image, double angleStart, double angleExtent, double scaleRMin, double scaleRMax, double scaleCMin, double scaleCMax, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scaleR, out HTuple scaleC, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(930);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleRMin);
			HalconAPI.StoreD(proc, 4, scaleRMax);
			HalconAPI.StoreD(proc, 5, scaleCMin);
			HalconAPI.StoreD(proc, 6, scaleCMax);
			HalconAPI.StoreD(proc, 7, minScore);
			HalconAPI.StoreI(proc, 8, numMatches);
			HalconAPI.StoreD(proc, 9, maxOverlap);
			HalconAPI.StoreS(proc, 10, subPixel);
			HalconAPI.StoreI(proc, 11, numLevels);
			HalconAPI.StoreD(proc, 12, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scaleR);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out scaleC);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void FindScaledShapeModel(HImage image, double angleStart, double angleExtent, double scaleMin, double scaleMax, double minScore, int numMatches, double maxOverlap, HTuple subPixel, HTuple numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scale, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(931);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleMin);
			HalconAPI.StoreD(proc, 4, scaleMax);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.StoreD(proc, 7, maxOverlap);
			HalconAPI.Store(proc, 8, subPixel);
			HalconAPI.Store(proc, 9, numLevels);
			HalconAPI.StoreD(proc, 10, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scale);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void FindScaledShapeModel(HImage image, double angleStart, double angleExtent, double scaleMin, double scaleMax, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple scale, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(931);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, scaleMin);
			HalconAPI.StoreD(proc, 4, scaleMax);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.StoreD(proc, 7, maxOverlap);
			HalconAPI.StoreS(proc, 8, subPixel);
			HalconAPI.StoreI(proc, 9, numLevels);
			HalconAPI.StoreD(proc, 10, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out scale);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void FindShapeModel(HImage image, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, HTuple subPixel, HTuple numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(932);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.Store(proc, 6, subPixel);
			HalconAPI.Store(proc, 7, numLevels);
			HalconAPI.StoreD(proc, 8, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void FindShapeModel(HImage image, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, double greediness, out HTuple row, out HTuple column, out HTuple angle, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(932);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, subPixel);
			HalconAPI.StoreI(proc, 7, numLevels);
			HalconAPI.StoreD(proc, 8, greediness);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void SetShapeModelMetric(HImage image, HHomMat2D homMat2D, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(933);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, homMat2D);
			HalconAPI.StoreS(proc, 2, metric);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void SetShapeModelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(934);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateAnisoShapeModelXld(HXLDCont contours, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleRMin, double scaleRMax, HTuple scaleRStep, double scaleCMin, double scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(935);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreateAnisoShapeModelXld(HXLDCont contours, int numLevels, double angleStart, double angleExtent, double angleStep, double scaleRMin, double scaleRMax, double scaleRStep, double scaleCMin, double scaleCMax, double scaleCStep, string optimization, string metric, int minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(935);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreateScaledShapeModelXld(HXLDCont contours, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleMin, double scaleMax, HTuple scaleStep, HTuple optimization, string metric, int minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(936);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.Store(proc, 6, scaleStep);
			HalconAPI.Store(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleStep);
			HalconAPI.UnpinTuple(optimization);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreateScaledShapeModelXld(HXLDCont contours, int numLevels, double angleStart, double angleExtent, double angleStep, double scaleMin, double scaleMax, double scaleStep, string optimization, string metric, int minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(936);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.StoreD(proc, 6, scaleStep);
			HalconAPI.StoreS(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreateShapeModelXld(HXLDCont contours, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, HTuple optimization, string metric, int minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(937);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.Store(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(optimization);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreateShapeModelXld(HXLDCont contours, int numLevels, double angleStart, double angleExtent, double angleStep, string optimization, string metric, int minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(937);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
		}

		public void CreateAnisoShapeModel(HImage template, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleRMin, double scaleRMax, HTuple scaleRStep, double scaleCMin, double scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(938);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.Store(proc, 12, contrast);
			HalconAPI.Store(proc, 13, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreateAnisoShapeModel(HImage template, int numLevels, double angleStart, double angleExtent, double angleStep, double scaleRMin, double scaleRMax, double scaleRStep, double scaleCMin, double scaleCMax, double scaleCStep, string optimization, string metric, int contrast, int minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(938);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, contrast);
			HalconAPI.StoreI(proc, 13, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreateScaledShapeModel(HImage template, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleMin, double scaleMax, HTuple scaleStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(939);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.Store(proc, 6, scaleStep);
			HalconAPI.Store(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.Store(proc, 9, contrast);
			HalconAPI.Store(proc, 10, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreateScaledShapeModel(HImage template, int numLevels, double angleStart, double angleExtent, double angleStep, double scaleMin, double scaleMax, double scaleStep, string optimization, string metric, int contrast, int minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(939);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.StoreD(proc, 6, scaleStep);
			HalconAPI.StoreS(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, contrast);
			HalconAPI.StoreI(proc, 10, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreateShapeModel(HImage template, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(940);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.Store(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.Store(proc, 6, contrast);
			HalconAPI.Store(proc, 7, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreateShapeModel(HImage template, int numLevels, double angleStart, double angleExtent, double angleStep, string optimization, string metric, int contrast, int minContrast)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(940);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, contrast);
			HalconAPI.StoreI(proc, 7, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(921);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
