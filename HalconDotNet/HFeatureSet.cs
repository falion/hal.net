using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HFeatureSet : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFeatureSet()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFeatureSet(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFeatureSet obj)
		{
			obj = new HFeatureSet(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFeatureSet[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HFeatureSet[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HFeatureSet(hTuple[i].IP);
			}
			return err;
		}

		public HFeatureSet(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1888);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ReadSampset(string fileName)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1888);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void LearnSampsetBox(HClassBox classifHandle, string outfile, int NSamples, double stopError, int errorN)
		{
			IntPtr proc = HalconAPI.PreCall(1890);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, classifHandle);
			HalconAPI.StoreS(proc, 2, outfile);
			HalconAPI.StoreI(proc, 3, NSamples);
			HalconAPI.StoreD(proc, 4, stopError);
			HalconAPI.StoreI(proc, 5, errorN);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classifHandle);
		}

		public double TestSampsetBox(HClassBox classifHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1897);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, classifHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classifHandle);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1893);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
