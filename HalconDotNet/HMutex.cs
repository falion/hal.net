using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HMutex : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMutex()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMutex(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMutex obj)
		{
			obj = new HMutex(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMutex[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HMutex[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HMutex(hTuple[i].IP);
			}
			return err;
		}

		public HMutex(HTuple attribName, HTuple attribValue)
		{
			IntPtr proc = HalconAPI.PreCall(564);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMutex(string attribName, string attribValue)
		{
			IntPtr proc = HalconAPI.PreCall(564);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void UnlockMutex()
		{
			IntPtr proc = HalconAPI.PreCall(561);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int TryLockMutex()
		{
			IntPtr proc = HalconAPI.PreCall(562);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void LockMutex()
		{
			IntPtr proc = HalconAPI.PreCall(563);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateMutex(HTuple attribName, HTuple attribValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(564);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateMutex(string attribName, string attribValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(564);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(560);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
