using System;

namespace VisionConfig
{
	public class HInfo
	{
		public static HTuple QueryOperatorInfo()
		{
			IntPtr proc = HalconAPI.PreCall(1108);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple QueryParamInfo()
		{
			IntPtr proc = HalconAPI.PreCall(1109);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple GetOperatorName(string pattern)
		{
			IntPtr proc = HalconAPI.PreCall(1110);
			HalconAPI.StoreS(proc, 0, pattern);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple GetParamTypes(string operatorName, out HTuple outpCtrlParType)
		{
			IntPtr proc = HalconAPI.PreCall(1111);
			HalconAPI.StoreS(proc, 0, operatorName);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out outpCtrlParType);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static string GetParamNum(string operatorName, out int inpObjPar, out int outpObjPar, out int inpCtrlPar, out int outpCtrlPar, out string type)
		{
			IntPtr proc = HalconAPI.PreCall(1112);
			HalconAPI.StoreS(proc, 0, operatorName);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadI(proc, 1, err, out inpObjPar);
			err = HalconAPI.LoadI(proc, 2, err, out outpObjPar);
			err = HalconAPI.LoadI(proc, 3, err, out inpCtrlPar);
			err = HalconAPI.LoadI(proc, 4, err, out outpCtrlPar);
			err = HalconAPI.LoadS(proc, 5, err, out type);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple GetParamNames(string operatorName, out HTuple outpObjPar, out HTuple inpCtrlPar, out HTuple outpCtrlPar)
		{
			IntPtr proc = HalconAPI.PreCall(1113);
			HalconAPI.StoreS(proc, 0, operatorName);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out outpObjPar);
			err = HTuple.LoadNew(proc, 2, err, out inpCtrlPar);
			err = HTuple.LoadNew(proc, 3, err, out outpCtrlPar);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple GetOperatorInfo(string operatorName, string slot)
		{
			IntPtr proc = HalconAPI.PreCall(1114);
			HalconAPI.StoreS(proc, 0, operatorName);
			HalconAPI.StoreS(proc, 1, slot);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple GetParamInfo(string operatorName, string paramName, string slot)
		{
			IntPtr proc = HalconAPI.PreCall(1115);
			HalconAPI.StoreS(proc, 0, operatorName);
			HalconAPI.StoreS(proc, 1, paramName);
			HalconAPI.StoreS(proc, 2, slot);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple SearchOperator(string keyword)
		{
			IntPtr proc = HalconAPI.PreCall(1116);
			HalconAPI.StoreS(proc, 0, keyword);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple GetKeywords(string operatorName)
		{
			IntPtr proc = HalconAPI.PreCall(1117);
			HalconAPI.StoreS(proc, 0, operatorName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple GetChapterInfo(HTuple chapter)
		{
			IntPtr proc = HalconAPI.PreCall(1118);
			HalconAPI.Store(proc, 0, chapter);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(chapter);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple GetChapterInfo(string chapter)
		{
			IntPtr proc = HalconAPI.PreCall(1118);
			HalconAPI.StoreS(proc, 0, chapter);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HTuple QueryWindowType()
		{
			IntPtr proc = HalconAPI.PreCall(1177);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static string GetComprise(HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1251);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public static HTuple QueryShape()
		{
			IntPtr proc = HalconAPI.PreCall(1252);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static void QueryLineWidth(out int min, out int max)
		{
			IntPtr proc = HalconAPI.PreCall(1254);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out min);
			err = HalconAPI.LoadI(proc, 1, err, out max);
			HalconAPI.PostCall(proc, err);
		}

		public static HTuple QueryColored()
		{
			IntPtr proc = HalconAPI.PreCall(1257);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out result);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static string InfoFramegrabber(string name, string query, out HTuple valueList)
		{
			IntPtr proc = HalconAPI.PreCall(2034);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.StoreS(proc, 1, query);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out valueList);
			HalconAPI.PostCall(proc, err);
			return result;
		}
	}
}
