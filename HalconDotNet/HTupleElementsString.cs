namespace VisionConfig
{
	internal class HTupleElementsString : HTupleElementsImplementation
	{
		internal HTupleElementsString(HTupleString source, int index)
			: base(source, index)
		{
		}

		internal HTupleElementsString(HTupleString source, int[] indices)
			: base(source, indices)
		{
		}

		public override string[] getS()
		{
			if (base.indices == null)
			{
				return null;
			}
			string[] array = new string[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = base.source.SArr[base.indices[i]];
			}
			return array;
		}

		public override void setS(string[] s)
		{
			if (s != null)
			{
				if (s.Length > 1)
				{
					if (s.Length == base.indices.Length)
					{
						for (int i = 0; i < s.Length; i++)
						{
							base.source.SArr[base.indices[i]] = s[i];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int j = 0; j < s.Length; j++)
				{
					base.source.SArr[base.indices[j]] = s[0];
				}
			}
		}

		public override object[] getO()
		{
			if (base.indices == null)
			{
				return null;
			}
			object[] array = new object[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = base.source.SArr[base.indices[i]];
			}
			return array;
		}

		public override HTupleType getType()
		{
			return HTupleType.STRING;
		}
	}
}
