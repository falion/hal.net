using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HSocket : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSocket()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSocket(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSocket obj)
		{
			obj = new HSocket(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSocket[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HSocket[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HSocket(hTuple[i].IP);
			}
			return err;
		}

		public HSocket(string hostName, int port, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(342);
			HalconAPI.StoreS(proc, 0, hostName);
			HalconAPI.StoreI(proc, 1, port);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSocket(string hostName, int port, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(342);
			HalconAPI.StoreS(proc, 0, hostName);
			HalconAPI.StoreI(proc, 1, port);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSocket(int port, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(343);
			HalconAPI.StoreI(proc, 0, port);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSocket(int port, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(343);
			HalconAPI.StoreI(proc, 0, port);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage ReceiveImage()
		{
			IntPtr proc = HalconAPI.PreCall(325);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SendImage(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(326);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HRegion ReceiveRegion()
		{
			IntPtr proc = HalconAPI.PreCall(327);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HRegion result = null;
			err = HRegion.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SendRegion(HRegion region)
		{
			IntPtr proc = HalconAPI.PreCall(328);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, region);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
		}

		public HXLD ReceiveXld()
		{
			IntPtr proc = HalconAPI.PreCall(329);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HXLD result = null;
			err = HXLD.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SendXld(HXLD XLD)
		{
			IntPtr proc = HalconAPI.PreCall(330);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, XLD);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(XLD);
		}

		public HTuple ReceiveTuple()
		{
			IntPtr proc = HalconAPI.PreCall(331);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SendTuple(HTuple tuple)
		{
			IntPtr proc = HalconAPI.PreCall(332);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, tuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(tuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SendTuple(string tuple)
		{
			IntPtr proc = HalconAPI.PreCall(332);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, tuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ReceiveData(HTuple format, out HTuple from)
		{
			IntPtr proc = HalconAPI.PreCall(333);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, format);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(format);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, err, out from);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple ReceiveData(string format, out string from)
		{
			IntPtr proc = HalconAPI.PreCall(333);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, format);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			err = HalconAPI.LoadS(proc, 1, err, out from);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SendData(string format, HTuple data, HTuple to)
		{
			IntPtr proc = HalconAPI.PreCall(334);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, format);
			HalconAPI.Store(proc, 2, data);
			HalconAPI.Store(proc, 3, to);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(data);
			HalconAPI.UnpinTuple(to);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SendData(string format, string data, string to)
		{
			IntPtr proc = HalconAPI.PreCall(334);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, format);
			HalconAPI.StoreS(proc, 2, data);
			HalconAPI.StoreS(proc, 3, to);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetSocketParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(335);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetSocketParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(335);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetSocketParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(336);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetSocketParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(336);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string GetNextSocketDataType()
		{
			IntPtr proc = HalconAPI.PreCall(337);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public int GetSocketDescriptor()
		{
			IntPtr proc = HalconAPI.PreCall(338);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			int result = 0;
			err = HalconAPI.LoadI(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static void CloseAllSockets()
		{
			IntPtr proc = HalconAPI.PreCall(339);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public HSocket SocketAcceptConnect(string wait)
		{
			IntPtr proc = HalconAPI.PreCall(341);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, wait);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSocket result = null;
			err = HSocket.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void OpenSocketConnect(string hostName, int port, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(342);
			HalconAPI.StoreS(proc, 0, hostName);
			HalconAPI.StoreI(proc, 1, port);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenSocketConnect(string hostName, int port, string genParamName, string genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(342);
			HalconAPI.StoreS(proc, 0, hostName);
			HalconAPI.StoreI(proc, 1, port);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenSocketAccept(int port, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(343);
			HalconAPI.StoreI(proc, 0, port);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenSocketAccept(int port, string genParamName, string genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(343);
			HalconAPI.StoreI(proc, 0, port);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem ReceiveSerializedItem()
		{
			IntPtr proc = HalconAPI.PreCall(403);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SendSerializedItem(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(404);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, serializedItemHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(340);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
