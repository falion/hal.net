using System;
using System.ComponentModel;

namespace VisionConfig
{
	public class HDeformableSurfaceMatchingResult : HTool
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceMatchingResult()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceMatchingResult(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDeformableSurfaceMatchingResult obj)
		{
			obj = new HDeformableSurfaceMatchingResult(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDeformableSurfaceMatchingResult[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HDeformableSurfaceMatchingResult[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HDeformableSurfaceMatchingResult(hTuple[i].IP);
			}
			return err;
		}

		public HTuple GetDeformableSurfaceMatchingResult(HTuple resultName, HTuple resultIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1019);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, resultName);
			HalconAPI.Store(proc, 2, resultIndex);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultName);
			HalconAPI.UnpinTuple(resultIndex);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetDeformableSurfaceMatchingResult(string resultName, int resultIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1019);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, resultName);
			HalconAPI.StoreI(proc, 2, resultIndex);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple RefineDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, HObjectModel3D objectModel3D, double relSamplingDistance, HObjectModel3D initialDeformationObjectModel3D, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1026);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, initialDeformationObjectModel3D);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(objectModel3D);
			GC.KeepAlive(initialDeformationObjectModel3D);
			return result;
		}

		public double RefineDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, HObjectModel3D objectModel3D, double relSamplingDistance, HObjectModel3D initialDeformationObjectModel3D, string genParamName, string genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1026);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, initialDeformationObjectModel3D);
			HalconAPI.StoreS(proc, 4, genParamName);
			HalconAPI.StoreS(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 1, err);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(objectModel3D);
			GC.KeepAlive(initialDeformationObjectModel3D);
			return result;
		}

		public static HTuple FindDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, HObjectModel3D objectModel3D, double relSamplingDistance, HTuple minScore, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1027);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public double FindDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, HObjectModel3D objectModel3D, double relSamplingDistance, double minScore, HTuple genParamName, HTuple genParamValue)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1027);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = base.Load(proc, 1, err);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1020);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
