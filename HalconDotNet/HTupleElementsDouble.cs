namespace VisionConfig
{
	internal class HTupleElementsDouble : HTupleElementsImplementation
	{
		internal HTupleElementsDouble(HTupleDouble source, int index)
			: base(source, index)
		{
		}

		internal HTupleElementsDouble(HTupleDouble source, int[] indices)
			: base(source, indices)
		{
		}

		public override double[] getD()
		{
			if (base.indices == null)
			{
				return null;
			}
			double[] array = new double[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = base.source.DArr[base.indices[i]];
			}
			return array;
		}

		public override void setD(double[] d)
		{
			if (d != null)
			{
				if (d.Length > 1)
				{
					if (d.Length == base.indices.Length)
					{
						for (int i = 0; i < d.Length; i++)
						{
							base.source.DArr[base.indices[i]] = d[i];
						}
						return;
					}
					throw new HTupleAccessException(base.source, "Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
				}
				for (int j = 0; j < d.Length; j++)
				{
					base.source.DArr[base.indices[j]] = d[0];
				}
			}
		}

		public override object[] getO()
		{
			if (base.indices == null)
			{
				return null;
			}
			object[] array = new object[base.indices.Length];
			for (int i = 0; i < base.indices.Length; i++)
			{
				array[i] = base.source.DArr[base.indices[i]];
			}
			return array;
		}

		public override HTupleType getType()
		{
			return HTupleType.DOUBLE;
		}
	}
}
