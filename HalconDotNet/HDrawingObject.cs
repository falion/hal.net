using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace VisionConfig
{
	public class HDrawingObject : HTool
	{
		public delegate void HDrawingObjectCallback(IntPtr drawid, IntPtr windowHandle, string type);

		public delegate void HDrawingObjectCallbackClass(HDrawingObject drawid, HWindow window, string type);

		public enum HDrawingObjectType
		{
			RECTANGLE1,
			RECTANGLE2,
			CIRCLE,
			ELLIPSE,
			CIRCLE_SECTOR,
			ELLIPSE_SECTOR,
			LINE,
			XLD_CONTOUR,
			TEXT
		}

		private HDrawingObjectCallback onresize;

		private HDrawingObjectCallback onattach;

		private HDrawingObjectCallback ondetach;

		private HDrawingObjectCallback ondrag;

		private HDrawingObjectCallback onselect;

		public long ID
		{
			get
			{
				return base.Handle.ToInt64();
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDrawingObject()
			: base(HTool.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDrawingObject(IntPtr handle)
			: base(handle)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDrawingObject obj)
		{
			obj = new HDrawingObject(HTool.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDrawingObject[] obj)
		{
			HTuple hTuple = null;
			err = HTuple.LoadNew(proc, parIndex, err, out hTuple);
			obj = new HDrawingObject[hTuple.Length];
			for (int i = 0; i < hTuple.Length; i++)
			{
				obj[i] = new HDrawingObject(hTuple[i].IP);
			}
			return err;
		}

		public HDrawingObject(double row, double column, double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1311);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDrawingObject(double row, double column, double phi, double length1, double length2)
		{
			IntPtr proc = HalconAPI.PreCall(1313);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, length1);
			HalconAPI.StoreD(proc, 4, length2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDrawingObject(double row1, double column1, double row2, double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1314);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected IntPtr DelegateToCallbackPointer(HDrawingObjectCallback c)
		{
			return Marshal.GetFunctionPointerForDelegate(c);
		}

		protected IntPtr DelegateToCallbackPointer(HDrawingObjectCallbackClass c, string evt)
		{
			HDrawingObjectCallback hDrawingObjectCallback = delegate(IntPtr drawid, IntPtr window, string type)
			{
				HDrawingObject hDrawingObject = new HDrawingObject(drawid);
				HWindow hWindow = new HWindow(window);
				hDrawingObject.Detach();
				hWindow.Detach();
				c(hDrawingObject, hWindow, type);
			};
			GC.KeepAlive(hDrawingObjectCallback);
			switch (evt)
			{
			case "on_resize":
				this.onresize = hDrawingObjectCallback;
				break;
			case "on_attach":
				this.onattach = hDrawingObjectCallback;
				break;
			case "on_detach":
				this.ondetach = hDrawingObjectCallback;
				break;
			case "on_drag":
				this.ondrag = hDrawingObjectCallback;
				break;
			case "on_select":
				this.onselect = hDrawingObjectCallback;
				break;
			}
			return Marshal.GetFunctionPointerForDelegate(hDrawingObjectCallback);
		}

		public void OnResize(HDrawingObjectCallback f)
		{
			this.SetDrawingObjectCallback("on_resize", Marshal.GetFunctionPointerForDelegate(f));
		}

		public void OnAttach(HDrawingObjectCallback f)
		{
			this.SetDrawingObjectCallback("on_attach", Marshal.GetFunctionPointerForDelegate(f));
		}

		public void OnDetach(HDrawingObjectCallback f)
		{
			this.SetDrawingObjectCallback("on_detach", Marshal.GetFunctionPointerForDelegate(f));
		}

		public void OnDrag(HDrawingObjectCallback f)
		{
			this.SetDrawingObjectCallback("on_drag", Marshal.GetFunctionPointerForDelegate(f));
		}

		public void OnSelect(HDrawingObjectCallback f)
		{
			this.SetDrawingObjectCallback("on_select", Marshal.GetFunctionPointerForDelegate(f));
		}

		public void OnResize(HDrawingObjectCallbackClass f)
		{
			this.SetDrawingObjectCallback("on_resize", this.DelegateToCallbackPointer(f, "on_resize"));
		}

		public void OnDrag(HDrawingObjectCallbackClass f)
		{
			this.SetDrawingObjectCallback("on_drag", this.DelegateToCallbackPointer(f, "on_drag"));
		}

		public void OnSelect(HDrawingObjectCallbackClass f)
		{
			this.SetDrawingObjectCallback("on_select", this.DelegateToCallbackPointer(f, "on_select"));
		}

		public void OnAttach(HDrawingObjectCallbackClass f)
		{
			this.SetDrawingObjectCallback("on_attach", this.DelegateToCallbackPointer(f, "on_attach"));
		}

		public void OnDetach(HDrawingObjectCallbackClass f)
		{
			this.SetDrawingObjectCallback("on_detach", this.DelegateToCallbackPointer(f, "on_detach"));
		}

		public static HDrawingObject CreateDrawingObject(HDrawingObjectType type, params HTuple[] values)
		{
			HTuple hTuple = null;
			switch (type)
			{
			case HDrawingObjectType.RECTANGLE1:
				HOperatorSet.CreateDrawingObjectRectangle1(values[0], values[1], values[2], values[3], out hTuple);
				break;
			case HDrawingObjectType.RECTANGLE2:
				HOperatorSet.CreateDrawingObjectRectangle2(values[0], values[1], values[2], values[3], values[4], out hTuple);
				break;
			case HDrawingObjectType.CIRCLE:
				HOperatorSet.CreateDrawingObjectCircle(values[0], values[1], values[2], out hTuple);
				break;
			case HDrawingObjectType.ELLIPSE:
				HOperatorSet.CreateDrawingObjectEllipse(values[0], values[1], values[2], values[3], values[4], out hTuple);
				break;
			case HDrawingObjectType.CIRCLE_SECTOR:
				HOperatorSet.CreateDrawingObjectCircleSector(values[0], values[1], values[2], values[3], values[4], out hTuple);
				break;
			case HDrawingObjectType.ELLIPSE_SECTOR:
				HOperatorSet.CreateDrawingObjectEllipseSector(values[0], values[1], values[2], values[3], values[4], values[5], values[6], out hTuple);
				break;
			case HDrawingObjectType.LINE:
				HOperatorSet.CreateDrawingObjectLine(values[0], values[1], values[2], values[3], out hTuple);
				break;
			case HDrawingObjectType.XLD_CONTOUR:
				if (values.Length != 2)
				{
					throw new HalconException("Invalid number of parameters");
				}
				if (values[0].Length != values[1].Length)
				{
					throw new HalconException("The length of the input tuples must be identical");
				}
				HOperatorSet.CreateDrawingObjectXld((HTuple)values[0].DArr, (HTuple)values[1].DArr, out hTuple);
				break;
			case HDrawingObjectType.TEXT:
				HOperatorSet.CreateDrawingObjectText(values[0], values[1], values[2], out hTuple);
				break;
			}
			return new HDrawingObject(new IntPtr(hTuple.L));
		}

		public void SetDrawingObjectCallback(HTuple drawObjectEvent, HTuple callbackFunction)
		{
			IntPtr proc = HalconAPI.PreCall(1162);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, drawObjectEvent);
			HalconAPI.Store(proc, 2, callbackFunction);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(drawObjectEvent);
			HalconAPI.UnpinTuple(callbackFunction);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDrawingObjectCallback(string drawObjectEvent, IntPtr callbackFunction)
		{
			IntPtr proc = HalconAPI.PreCall(1162);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, drawObjectEvent);
			HalconAPI.StoreIP(proc, 2, callbackFunction);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void DetachBackgroundFromWindow(HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1163);
			HalconAPI.Store(proc, 0, windowHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(windowHandle);
		}

		public static void AttachBackgroundToWindow(HImage image, HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1164);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, windowHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(image);
			GC.KeepAlive(windowHandle);
		}

		public void CreateDrawingObjectText(int row, int column, string stringVal)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1301);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreS(proc, 2, stringVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HObject GetDrawingObjectIconic()
		{
			IntPtr proc = HalconAPI.PreCall(1302);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HObject result = null;
			err = HObject.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetDrawingObjectParams(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1304);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDrawingObjectParams(string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1304);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetDrawingObjectParams(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1305);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple GetDrawingObjectParams(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1305);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void SetDrawingObjectXld(HXLDCont contour)
		{
			IntPtr proc = HalconAPI.PreCall(1306);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, contour);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
		}

		public void CreateDrawingObjectXld(HTuple row, HTuple column)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1307);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectCircleSector(double row, double column, double radius, double startAngle, double endAngle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1308);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, startAngle);
			HalconAPI.StoreD(proc, 4, endAngle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectEllipseSector(double row, double column, double phi, double radius1, double radius2, double startAngle, double endAngle)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1309);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, radius1);
			HalconAPI.StoreD(proc, 4, radius2);
			HalconAPI.StoreD(proc, 5, startAngle);
			HalconAPI.StoreD(proc, 6, endAngle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectLine(double row1, double column1, double row2, double column2)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1310);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectCircle(double row, double column, double radius)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1311);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectEllipse(double row, double column, double phi, double radius1, double radius2)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1312);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, radius1);
			HalconAPI.StoreD(proc, 4, radius2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectRectangle2(double row, double column, double phi, double length1, double length2)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1313);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, length1);
			HalconAPI.StoreD(proc, 4, length2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectRectangle1(double row1, double column1, double row2, double column2)
		{
			this.Dispose();
			IntPtr proc = HalconAPI.PreCall(1314);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static string SendMouseDoubleClickEvent(HWindow windowHandle, HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2088);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public static string SendMouseDoubleClickEvent(HWindow windowHandle, int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2088);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public static string SendMouseDownEvent(HWindow windowHandle, HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2089);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public static string SendMouseDownEvent(HWindow windowHandle, int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2089);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public static string SendMouseDragEvent(HWindow windowHandle, HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2090);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public static string SendMouseDragEvent(HWindow windowHandle, int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2090);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public static string SendMouseUpEvent(HWindow windowHandle, HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2091);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		public static string SendMouseUpEvent(HWindow windowHandle, int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2091);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return result;
		}

		protected override void ClearHandleResource()
		{
			IntPtr proc = HalconAPI.PreCall(1303);
			base.Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
