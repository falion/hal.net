using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace VisionConfig
{
	[Serializable]
	public class HPose : HData, ISerializable, ICloneable
	{
		private const int FIXEDSIZE = 7;

		public HPose()
		{
		}

		public HPose(HTuple tuple)
			: base(tuple)
		{
		}

		internal HPose(HData data)
			: base(data)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, HTupleType type, int err, out HPose obj)
		{
			HTuple t = null;
			err = HTuple.LoadNew(proc, parIndex, err, out t);
			obj = new HPose(new HData(t));
			return err;
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HPose obj)
		{
			return HPose.LoadNew(proc, parIndex, HTupleType.MIXED, err, out obj);
		}

		internal static HPose[] SplitArray(HTuple data)
		{
			int num = data.Length / 7;
			HPose[] array = new HPose[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = new HPose(new HData(data.TupleSelectRange(i * 7, (i + 1) * 7 - 1)));
			}
			return array;
		}

		public HPose(double transX, double transY, double transZ, double rotX, double rotY, double rotZ, string orderOfTransform, string orderOfRotation, string viewOfTransform)
		{
			IntPtr proc = HalconAPI.PreCall(1921);
			HalconAPI.StoreD(proc, 0, transX);
			HalconAPI.StoreD(proc, 1, transY);
			HalconAPI.StoreD(proc, 2, transZ);
			HalconAPI.StoreD(proc, 3, rotX);
			HalconAPI.StoreD(proc, 4, rotY);
			HalconAPI.StoreD(proc, 5, rotZ);
			HalconAPI.StoreS(proc, 6, orderOfTransform);
			HalconAPI.StoreS(proc, 7, orderOfRotation);
			HalconAPI.StoreS(proc, 8, viewOfTransform);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = this.SerializePose();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HPose(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			this.DeserializePose(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			this.SerializePose().Serialize(stream);
		}

		public static HPose Deserialize(Stream stream)
		{
			HPose hPose = new HPose();
			hPose.DeserializePose(HSerializedItem.Deserialize(stream));
			return hPose;
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		public HPose Clone()
		{
			HSerializedItem hSerializedItem = this.SerializePose();
			HPose hPose = new HPose();
			hPose.DeserializePose(hSerializedItem);
			hSerializedItem.Dispose();
			return hPose;
		}

		public static implicit operator HHomMat3D(HPose pose)
		{
			return pose.PoseToHomMat3d();
		}

		public static HPose PoseAverage(HPose[] poses, HTuple weights, string mode, HTuple sigmaT, HTuple sigmaR, out HTuple quality)
		{
			HTuple hTuple = HData.ConcatArray(poses);
			IntPtr proc = HalconAPI.PreCall(221);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, weights);
			HalconAPI.StoreS(proc, 2, mode);
			HalconAPI.Store(proc, 3, sigmaT);
			HalconAPI.Store(proc, 4, sigmaR);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(weights);
			HalconAPI.UnpinTuple(sigmaT);
			HalconAPI.UnpinTuple(sigmaR);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out quality);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HPose PoseAverage(HPose[] poses, HTuple weights, string mode, double sigmaT, double sigmaR, out HTuple quality)
		{
			HTuple hTuple = HData.ConcatArray(poses);
			IntPtr proc = HalconAPI.PreCall(221);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, weights);
			HalconAPI.StoreS(proc, 2, mode);
			HalconAPI.StoreD(proc, 3, sigmaT);
			HalconAPI.StoreD(proc, 4, sigmaR);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(weights);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out quality);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HPose[] PoseInvert(HPose[] pose)
		{
			HTuple hTuple = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(227);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			return HPose.SplitArray(data);
		}

		public HPose PoseInvert()
		{
			IntPtr proc = HalconAPI.PreCall(227);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HPose[] PoseCompose(HPose[] poseLeft, HPose[] poseRight)
		{
			HTuple hTuple = HData.ConcatArray(poseLeft);
			HTuple hTuple2 = HData.ConcatArray(poseRight);
			IntPtr proc = HalconAPI.PreCall(228);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			return HPose.SplitArray(data);
		}

		public HPose PoseCompose(HPose poseRight)
		{
			IntPtr proc = HalconAPI.PreCall(228);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, poseRight);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(poseRight);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage BinocularDistanceMs(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, int minDisparity, int maxDisparity, int surfaceSmoothing, int edgeSmoothing, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(346);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.StoreI(proc, 3, minDisparity);
			HalconAPI.StoreI(proc, 4, maxDisparity);
			HalconAPI.StoreI(proc, 5, surfaceSmoothing);
			HalconAPI.StoreI(proc, 6, edgeSmoothing);
			HalconAPI.Store(proc, 7, genParamName);
			HalconAPI.Store(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDistanceMs(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, int minDisparity, int maxDisparity, int surfaceSmoothing, int edgeSmoothing, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(346);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.StoreI(proc, 3, minDisparity);
			HalconAPI.StoreI(proc, 4, maxDisparity);
			HalconAPI.StoreI(proc, 5, surfaceSmoothing);
			HalconAPI.StoreI(proc, 6, edgeSmoothing);
			HalconAPI.StoreS(proc, 7, genParamName);
			HalconAPI.StoreS(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDistanceMg(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, double grayConstancy, double gradientConstancy, double smoothness, double initialGuess, string calculateScore, HTuple MGParamName, HTuple MGParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(348);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.StoreD(proc, 3, grayConstancy);
			HalconAPI.StoreD(proc, 4, gradientConstancy);
			HalconAPI.StoreD(proc, 5, smoothness);
			HalconAPI.StoreD(proc, 6, initialGuess);
			HalconAPI.StoreS(proc, 7, calculateScore);
			HalconAPI.Store(proc, 8, MGParamName);
			HalconAPI.Store(proc, 9, MGParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(MGParamName);
			HalconAPI.UnpinTuple(MGParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDistanceMg(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, double grayConstancy, double gradientConstancy, double smoothness, double initialGuess, string calculateScore, string MGParamName, string MGParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(348);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.StoreD(proc, 3, grayConstancy);
			HalconAPI.StoreD(proc, 4, gradientConstancy);
			HalconAPI.StoreD(proc, 5, smoothness);
			HalconAPI.StoreD(proc, 6, initialGuess);
			HalconAPI.StoreS(proc, 7, calculateScore);
			HalconAPI.StoreS(proc, 8, MGParamName);
			HalconAPI.StoreS(proc, 9, MGParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HHomMat2D RelPoseToFundamentalMatrix(HTuple covRelPose, HCamPar camPar1, HCamPar camPar2, out HTuple covFMat)
		{
			IntPtr proc = HalconAPI.PreCall(353);
			base.Store(proc, 0);
			HalconAPI.Store(proc, 1, covRelPose);
			HalconAPI.Store(proc, 2, camPar1);
			HalconAPI.Store(proc, 3, camPar2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(covRelPose);
			HalconAPI.UnpinTuple(camPar1);
			HalconAPI.UnpinTuple(camPar2);
			HHomMat2D result = null;
			err = HHomMat2D.LoadNew(proc, 0, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covFMat);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple VectorToRelPose(HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple covRR1, HTuple covRC1, HTuple covCC1, HTuple covRR2, HTuple covRC2, HTuple covCC2, HCamPar camPar1, HCamPar camPar2, string method, out HTuple error, out HTuple x, out HTuple y, out HTuple z, out HTuple covXYZ)
		{
			IntPtr proc = HalconAPI.PreCall(355);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, covRR1);
			HalconAPI.Store(proc, 5, covRC1);
			HalconAPI.Store(proc, 6, covCC1);
			HalconAPI.Store(proc, 7, covRR2);
			HalconAPI.Store(proc, 8, covRC2);
			HalconAPI.Store(proc, 9, covCC2);
			HalconAPI.Store(proc, 10, camPar1);
			HalconAPI.Store(proc, 11, camPar2);
			HalconAPI.StoreS(proc, 12, method);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(covRR1);
			HalconAPI.UnpinTuple(covRC1);
			HalconAPI.UnpinTuple(covCC1);
			HalconAPI.UnpinTuple(covRR2);
			HalconAPI.UnpinTuple(covRC2);
			HalconAPI.UnpinTuple(covCC2);
			HalconAPI.UnpinTuple(camPar1);
			HalconAPI.UnpinTuple(camPar2);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out covXYZ);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple VectorToRelPose(HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple covRR1, HTuple covRC1, HTuple covCC1, HTuple covRR2, HTuple covRC2, HTuple covCC2, HCamPar camPar1, HCamPar camPar2, string method, out double error, out HTuple x, out HTuple y, out HTuple z, out HTuple covXYZ)
		{
			IntPtr proc = HalconAPI.PreCall(355);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, covRR1);
			HalconAPI.Store(proc, 5, covRC1);
			HalconAPI.Store(proc, 6, covCC1);
			HalconAPI.Store(proc, 7, covRR2);
			HalconAPI.Store(proc, 8, covRC2);
			HalconAPI.Store(proc, 9, covCC2);
			HalconAPI.Store(proc, 10, camPar1);
			HalconAPI.Store(proc, 11, camPar2);
			HalconAPI.StoreS(proc, 12, method);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(covRR1);
			HalconAPI.UnpinTuple(covRC1);
			HalconAPI.UnpinTuple(covCC1);
			HalconAPI.UnpinTuple(covRR2);
			HalconAPI.UnpinTuple(covRC2);
			HalconAPI.UnpinTuple(covCC2);
			HalconAPI.UnpinTuple(camPar1);
			HalconAPI.UnpinTuple(camPar2);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out covXYZ);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple MatchRelPoseRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HCamPar camPar1, HCamPar camPar2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out HTuple error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(359);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, camPar1);
			HalconAPI.Store(proc, 5, camPar2);
			HalconAPI.StoreS(proc, 6, grayMatchMethod);
			HalconAPI.StoreI(proc, 7, maskSize);
			HalconAPI.StoreI(proc, 8, rowMove);
			HalconAPI.StoreI(proc, 9, colMove);
			HalconAPI.StoreI(proc, 10, rowTolerance);
			HalconAPI.StoreI(proc, 11, colTolerance);
			HalconAPI.Store(proc, 12, rotation);
			HalconAPI.Store(proc, 13, matchThreshold);
			HalconAPI.StoreS(proc, 14, estimationMethod);
			HalconAPI.Store(proc, 15, distanceThreshold);
			HalconAPI.StoreI(proc, 16, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(camPar1);
			HalconAPI.UnpinTuple(camPar2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HTuple MatchRelPoseRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HCamPar camPar1, HCamPar camPar2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(359);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, camPar1);
			HalconAPI.Store(proc, 5, camPar2);
			HalconAPI.StoreS(proc, 6, grayMatchMethod);
			HalconAPI.StoreI(proc, 7, maskSize);
			HalconAPI.StoreI(proc, 8, rowMove);
			HalconAPI.StoreI(proc, 9, colMove);
			HalconAPI.StoreI(proc, 10, rowTolerance);
			HalconAPI.StoreI(proc, 11, colTolerance);
			HalconAPI.StoreD(proc, 12, rotation);
			HalconAPI.StoreI(proc, 13, matchThreshold);
			HalconAPI.StoreS(proc, 14, estimationMethod);
			HalconAPI.StoreD(proc, 15, distanceThreshold);
			HalconAPI.StoreI(proc, 16, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(camPar1);
			HalconAPI.UnpinTuple(camPar2);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out result);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return result;
		}

		public HImage BinocularDistance(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, string method, int maskWidth, int maskHeight, HTuple textureThresh, int minDisparity, int maxDisparity, int numLevels, HTuple scoreThresh, HTuple filter, HTuple subDistance)
		{
			IntPtr proc = HalconAPI.PreCall(362);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.StoreS(proc, 3, method);
			HalconAPI.StoreI(proc, 4, maskWidth);
			HalconAPI.StoreI(proc, 5, maskHeight);
			HalconAPI.Store(proc, 6, textureThresh);
			HalconAPI.StoreI(proc, 7, minDisparity);
			HalconAPI.StoreI(proc, 8, maxDisparity);
			HalconAPI.StoreI(proc, 9, numLevels);
			HalconAPI.Store(proc, 10, scoreThresh);
			HalconAPI.Store(proc, 11, filter);
			HalconAPI.Store(proc, 12, subDistance);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(textureThresh);
			HalconAPI.UnpinTuple(scoreThresh);
			HalconAPI.UnpinTuple(filter);
			HalconAPI.UnpinTuple(subDistance);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public HImage BinocularDistance(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect1, HCamPar camParamRect2, string method, int maskWidth, int maskHeight, double textureThresh, int minDisparity, int maxDisparity, int numLevels, double scoreThresh, string filter, string subDistance)
		{
			IntPtr proc = HalconAPI.PreCall(362);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.StoreS(proc, 3, method);
			HalconAPI.StoreI(proc, 4, maskWidth);
			HalconAPI.StoreI(proc, 5, maskHeight);
			HalconAPI.StoreD(proc, 6, textureThresh);
			HalconAPI.StoreI(proc, 7, minDisparity);
			HalconAPI.StoreI(proc, 8, maxDisparity);
			HalconAPI.StoreI(proc, 9, numLevels);
			HalconAPI.StoreD(proc, 10, scoreThresh);
			HalconAPI.StoreS(proc, 11, filter);
			HalconAPI.StoreS(proc, 12, subDistance);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return result;
		}

		public void IntersectLinesOfSight(HCamPar camParam1, HCamPar camParam2, HTuple row1, HTuple col1, HTuple row2, HTuple col2, out HTuple x, out HTuple y, out HTuple z, out HTuple dist)
		{
			IntPtr proc = HalconAPI.PreCall(364);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, camParam1);
			HalconAPI.Store(proc, 1, camParam2);
			HalconAPI.Store(proc, 3, row1);
			HalconAPI.Store(proc, 4, col1);
			HalconAPI.Store(proc, 5, row2);
			HalconAPI.Store(proc, 6, col2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam1);
			HalconAPI.UnpinTuple(camParam2);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(col1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(col2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out dist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void IntersectLinesOfSight(HCamPar camParam1, HCamPar camParam2, double row1, double col1, double row2, double col2, out double x, out double y, out double z, out double dist)
		{
			IntPtr proc = HalconAPI.PreCall(364);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, camParam1);
			HalconAPI.Store(proc, 1, camParam2);
			HalconAPI.StoreD(proc, 3, row1);
			HalconAPI.StoreD(proc, 4, col1);
			HalconAPI.StoreD(proc, 5, row2);
			HalconAPI.StoreD(proc, 6, col2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam1);
			HalconAPI.UnpinTuple(camParam2);
			err = HalconAPI.LoadD(proc, 0, err, out x);
			err = HalconAPI.LoadD(proc, 1, err, out y);
			err = HalconAPI.LoadD(proc, 2, err, out z);
			err = HalconAPI.LoadD(proc, 3, err, out dist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage DisparityImageToXyz(HImage disparity, out HImage y, out HImage z, HCamPar camParamRect1, HCamPar camParamRect2)
		{
			IntPtr proc = HalconAPI.PreCall(365);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, disparity);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out y);
			err = HImage.LoadNew(proc, 3, err, out z);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(disparity);
			return result;
		}

		public void DisparityToPoint3d(HCamPar camParamRect1, HCamPar camParamRect2, HTuple row1, HTuple col1, HTuple disparity, out HTuple x, out HTuple y, out HTuple z)
		{
			IntPtr proc = HalconAPI.PreCall(366);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 3, row1);
			HalconAPI.Store(proc, 4, col1);
			HalconAPI.Store(proc, 5, disparity);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(col1);
			HalconAPI.UnpinTuple(disparity);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out z);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DisparityToPoint3d(HCamPar camParamRect1, HCamPar camParamRect2, double row1, double col1, double disparity, out double x, out double y, out double z)
		{
			IntPtr proc = HalconAPI.PreCall(366);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.StoreD(proc, 3, row1);
			HalconAPI.StoreD(proc, 4, col1);
			HalconAPI.StoreD(proc, 5, disparity);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			err = HalconAPI.LoadD(proc, 0, err, out x);
			err = HalconAPI.LoadD(proc, 1, err, out y);
			err = HalconAPI.LoadD(proc, 2, err, out z);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DisparityToDistance(HCamPar camParamRect1, HCamPar camParamRect2, HTuple disparity)
		{
			IntPtr proc = HalconAPI.PreCall(367);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 3, disparity);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(disparity);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double DisparityToDistance(HCamPar camParamRect1, HCamPar camParamRect2, double disparity)
		{
			IntPtr proc = HalconAPI.PreCall(367);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.StoreD(proc, 3, disparity);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple DistanceToDisparity(HCamPar camParamRect1, HCamPar camParamRect2, HTuple distance)
		{
			IntPtr proc = HalconAPI.PreCall(368);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 3, distance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(distance);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double DistanceToDisparity(HCamPar camParamRect1, HCamPar camParamRect2, double distance)
		{
			IntPtr proc = HalconAPI.PreCall(368);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, camParamRect1);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.StoreD(proc, 3, distance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenBinocularRectificationMap(out HImage map2, HCamPar camParam1, HCamPar camParam2, double subSampling, string method, string mapType, out HCamPar camParamRect1, out HCamPar camParamRect2, out HPose camPoseRect1, out HPose camPoseRect2, out HPose relPoseRect)
		{
			IntPtr proc = HalconAPI.PreCall(369);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, camParam1);
			HalconAPI.Store(proc, 1, camParam2);
			HalconAPI.StoreD(proc, 3, subSampling);
			HalconAPI.StoreS(proc, 4, method);
			HalconAPI.StoreS(proc, 5, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam1);
			HalconAPI.UnpinTuple(camParam2);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			err = HImage.LoadNew(proc, 2, err, out map2);
			err = HCamPar.LoadNew(proc, 0, err, out camParamRect1);
			err = HCamPar.LoadNew(proc, 1, err, out camParamRect2);
			err = HPose.LoadNew(proc, 2, err, out camPoseRect1);
			err = HPose.LoadNew(proc, 3, err, out camPoseRect2);
			err = HPose.LoadNew(proc, 4, err, out relPoseRect);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HCamPar BinocularCalibration(HTuple NX, HTuple NY, HTuple NZ, HTuple NRow1, HTuple NCol1, HTuple NRow2, HTuple NCol2, HCamPar startCamParam1, HCamPar startCamParam2, HPose[] NStartPose1, HPose[] NStartPose2, HTuple estimateParams, out HCamPar camParam2, out HPose[] NFinalPose1, out HPose[] NFinalPose2, out HPose relPose, out HTuple errors)
		{
			HTuple hTuple = HData.ConcatArray(NStartPose1);
			HTuple hTuple2 = HData.ConcatArray(NStartPose2);
			IntPtr proc = HalconAPI.PreCall(370);
			HalconAPI.Store(proc, 0, NX);
			HalconAPI.Store(proc, 1, NY);
			HalconAPI.Store(proc, 2, NZ);
			HalconAPI.Store(proc, 3, NRow1);
			HalconAPI.Store(proc, 4, NCol1);
			HalconAPI.Store(proc, 5, NRow2);
			HalconAPI.Store(proc, 6, NCol2);
			HalconAPI.Store(proc, 7, startCamParam1);
			HalconAPI.Store(proc, 8, startCamParam2);
			HalconAPI.Store(proc, 9, hTuple);
			HalconAPI.Store(proc, 10, hTuple2);
			HalconAPI.Store(proc, 11, estimateParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(NX);
			HalconAPI.UnpinTuple(NY);
			HalconAPI.UnpinTuple(NZ);
			HalconAPI.UnpinTuple(NRow1);
			HalconAPI.UnpinTuple(NCol1);
			HalconAPI.UnpinTuple(NRow2);
			HalconAPI.UnpinTuple(NCol2);
			HalconAPI.UnpinTuple(startCamParam1);
			HalconAPI.UnpinTuple(startCamParam2);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(estimateParams);
			HCamPar result = null;
			err = HCamPar.LoadNew(proc, 0, err, out result);
			err = HCamPar.LoadNew(proc, 1, err, out camParam2);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 2, err, out data);
			HTuple data2 = null;
			err = HTuple.LoadNew(proc, 3, err, out data2);
			err = HPose.LoadNew(proc, 4, err, out relPose);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out errors);
			HalconAPI.PostCall(proc, err);
			NFinalPose1 = HPose.SplitArray(data);
			NFinalPose2 = HPose.SplitArray(data2);
			return result;
		}

		public HCamPar BinocularCalibration(HTuple NX, HTuple NY, HTuple NZ, HTuple NRow1, HTuple NCol1, HTuple NRow2, HTuple NCol2, HCamPar startCamParam1, HCamPar startCamParam2, HPose NStartPose2, HTuple estimateParams, out HCamPar camParam2, out HPose NFinalPose1, out HPose NFinalPose2, out HPose relPose, out double errors)
		{
			IntPtr proc = HalconAPI.PreCall(370);
			base.Store(proc, 9);
			HalconAPI.Store(proc, 0, NX);
			HalconAPI.Store(proc, 1, NY);
			HalconAPI.Store(proc, 2, NZ);
			HalconAPI.Store(proc, 3, NRow1);
			HalconAPI.Store(proc, 4, NCol1);
			HalconAPI.Store(proc, 5, NRow2);
			HalconAPI.Store(proc, 6, NCol2);
			HalconAPI.Store(proc, 7, startCamParam1);
			HalconAPI.Store(proc, 8, startCamParam2);
			HalconAPI.Store(proc, 10, NStartPose2);
			HalconAPI.Store(proc, 11, estimateParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(NX);
			HalconAPI.UnpinTuple(NY);
			HalconAPI.UnpinTuple(NZ);
			HalconAPI.UnpinTuple(NRow1);
			HalconAPI.UnpinTuple(NCol1);
			HalconAPI.UnpinTuple(NRow2);
			HalconAPI.UnpinTuple(NCol2);
			HalconAPI.UnpinTuple(startCamParam1);
			HalconAPI.UnpinTuple(startCamParam2);
			HalconAPI.UnpinTuple(NStartPose2);
			HalconAPI.UnpinTuple(estimateParams);
			HCamPar result = null;
			err = HCamPar.LoadNew(proc, 0, err, out result);
			err = HCamPar.LoadNew(proc, 1, err, out camParam2);
			err = HPose.LoadNew(proc, 2, err, out NFinalPose1);
			err = HPose.LoadNew(proc, 3, err, out NFinalPose2);
			err = HPose.LoadNew(proc, 4, err, out relPose);
			err = HalconAPI.LoadD(proc, 5, err, out errors);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HTuple FindCalibDescriptorModel(HImage image, HDescriptorModel modelID, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, HTuple minScore, int numMatches, HCamPar camParam, HTuple scoreType, out HPose[] pose)
		{
			IntPtr proc = HalconAPI.PreCall(948);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.Store(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.Store(proc, 7, camParam);
			HalconAPI.Store(proc, 8, scoreType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(scoreType);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			pose = HPose.SplitArray(data);
			GC.KeepAlive(image);
			GC.KeepAlive(modelID);
			return result;
		}

		public double FindCalibDescriptorModel(HImage image, HDescriptorModel modelID, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, double minScore, int numMatches, HCamPar camParam, string scoreType)
		{
			IntPtr proc = HalconAPI.PreCall(948);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.Store(proc, 7, camParam);
			HalconAPI.StoreS(proc, 8, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HalconAPI.UnpinTuple(camParam);
			err = base.Load(proc, 0, err);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(modelID);
			return result;
		}

		public HDescriptorModel CreateCalibDescriptorModel(HImage template, HCamPar camParam, string detectorType, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, int seed)
		{
			IntPtr proc = HalconAPI.PreCall(952);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.StoreS(proc, 2, detectorType);
			HalconAPI.Store(proc, 3, detectorParamName);
			HalconAPI.Store(proc, 4, detectorParamValue);
			HalconAPI.Store(proc, 5, descriptorParamName);
			HalconAPI.Store(proc, 6, descriptorParamValue);
			HalconAPI.StoreI(proc, 7, seed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HDescriptorModel result = null;
			err = HDescriptorModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
			return result;
		}

		public HDeformableModel CreatePlanarCalibDeformableModelXld(HXLDCont contours, HCamPar camParam, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(976);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
			return result;
		}

		public HDeformableModel CreatePlanarCalibDeformableModelXld(HXLDCont contours, HCamPar camParam, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(976);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
			return result;
		}

		public HDeformableModel CreatePlanarCalibDeformableModel(HImage template, HCamPar camParam, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(979);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.Store(proc, 14, contrast);
			HalconAPI.Store(proc, 15, minContrast);
			HalconAPI.Store(proc, 16, genParamName);
			HalconAPI.Store(proc, 17, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
			return result;
		}

		public HDeformableModel CreatePlanarCalibDeformableModel(HImage template, HCamPar camParam, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, HTuple contrast, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(979);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.Store(proc, 14, contrast);
			HalconAPI.StoreI(proc, 15, minContrast);
			HalconAPI.Store(proc, 16, genParamName);
			HalconAPI.Store(proc, 17, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HDeformableModel result = null;
			err = HDeformableModel.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
			return result;
		}

		public static HPose[] CreateCamPoseLookAtPoint(HTuple camPosX, HTuple camPosY, HTuple camPosZ, HTuple lookAtX, HTuple lookAtY, HTuple lookAtZ, HTuple refPlaneNormal, HTuple camRoll)
		{
			IntPtr proc = HalconAPI.PreCall(1045);
			HalconAPI.Store(proc, 0, camPosX);
			HalconAPI.Store(proc, 1, camPosY);
			HalconAPI.Store(proc, 2, camPosZ);
			HalconAPI.Store(proc, 3, lookAtX);
			HalconAPI.Store(proc, 4, lookAtY);
			HalconAPI.Store(proc, 5, lookAtZ);
			HalconAPI.Store(proc, 6, refPlaneNormal);
			HalconAPI.Store(proc, 7, camRoll);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camPosX);
			HalconAPI.UnpinTuple(camPosY);
			HalconAPI.UnpinTuple(camPosZ);
			HalconAPI.UnpinTuple(lookAtX);
			HalconAPI.UnpinTuple(lookAtY);
			HalconAPI.UnpinTuple(lookAtZ);
			HalconAPI.UnpinTuple(refPlaneNormal);
			HalconAPI.UnpinTuple(camRoll);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			return HPose.SplitArray(data);
		}

		public void CreateCamPoseLookAtPoint(double camPosX, double camPosY, double camPosZ, double lookAtX, double lookAtY, double lookAtZ, HTuple refPlaneNormal, double camRoll)
		{
			IntPtr proc = HalconAPI.PreCall(1045);
			HalconAPI.StoreD(proc, 0, camPosX);
			HalconAPI.StoreD(proc, 1, camPosY);
			HalconAPI.StoreD(proc, 2, camPosZ);
			HalconAPI.StoreD(proc, 3, lookAtX);
			HalconAPI.StoreD(proc, 4, lookAtY);
			HalconAPI.StoreD(proc, 5, lookAtZ);
			HalconAPI.Store(proc, 6, refPlaneNormal);
			HalconAPI.StoreD(proc, 7, camRoll);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(refPlaneNormal);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HPose TransPoseShapeModel3d(HShapeModel3D shapeModel3DID, string transformation)
		{
			IntPtr proc = HalconAPI.PreCall(1054);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, shapeModel3DID);
			HalconAPI.StoreS(proc, 2, transformation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(shapeModel3DID);
			return result;
		}

		public HXLDCont ProjectShapeModel3d(HShapeModel3D shapeModel3DID, HCamPar camParam, string hiddenSurfaceRemoval, HTuple minFaceAngle)
		{
			IntPtr proc = HalconAPI.PreCall(1055);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, shapeModel3DID);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.StoreS(proc, 3, hiddenSurfaceRemoval);
			HalconAPI.Store(proc, 4, minFaceAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(minFaceAngle);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(shapeModel3DID);
			return result;
		}

		public HXLDCont ProjectShapeModel3d(HShapeModel3D shapeModel3DID, HCamPar camParam, string hiddenSurfaceRemoval, double minFaceAngle)
		{
			IntPtr proc = HalconAPI.PreCall(1055);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, shapeModel3DID);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.StoreS(proc, 3, hiddenSurfaceRemoval);
			HalconAPI.StoreD(proc, 4, minFaceAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(shapeModel3DID);
			return result;
		}

		public static HObjectModel3D[] ReduceObjectModel3dByView(HRegion region, HObjectModel3D[] objectModel3D, HCamPar camParam, HPose[] pose)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1084);
			HalconAPI.Store(proc, 1, region);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 2, hTuple2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(hTuple2);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(region);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D ReduceObjectModel3dByView(HRegion region, HObjectModel3D objectModel3D, HCamPar camParam)
		{
			IntPtr proc = HalconAPI.PreCall(1084);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 1, region);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public static HImage RenderObjectModel3d(HObjectModel3D[] objectModel3D, HCamPar camParam, HPose[] pose, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1088);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 2, hTuple2);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HImage RenderObjectModel3d(HObjectModel3D objectModel3D, HCamPar camParam, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1088);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public static void DispObjectModel3d(HWindow windowHandle, HObjectModel3D[] objectModel3D, HCamPar camParam, HPose[] pose, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HTool.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1089);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, hTuple);
			HalconAPI.Store(proc, 2, camParam);
			HalconAPI.Store(proc, 3, hTuple2);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(windowHandle);
			GC.KeepAlive(objectModel3D);
		}

		public void DispObjectModel3d(HWindow windowHandle, HObjectModel3D objectModel3D, HCamPar camParam, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1089);
			base.Store(proc, 3);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 2, camParam);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
			GC.KeepAlive(objectModel3D);
		}

		public HXLDCont ProjectObjectModel3d(HObjectModel3D objectModel3D, HCamPar camParam, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1095);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HXLDCont ProjectObjectModel3d(HObjectModel3D objectModel3D, HCamPar camParam, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1095);
			base.Store(proc, 2);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.StoreS(proc, 3, genParamName);
			HalconAPI.StoreS(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParam);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HObjectModel3D[] SceneFlowCalib(HImage imageRect1T1, HImage imageRect2T1, HImage imageRect1T2, HImage imageRect2T2, HImage disparity, HTuple smoothingFlow, HTuple smoothingDisparity, HTuple genParamName, HTuple genParamValue, HCamPar camParamRect1, HCamPar camParamRect2)
		{
			IntPtr proc = HalconAPI.PreCall(1481);
			base.Store(proc, 6);
			HalconAPI.Store(proc, 1, imageRect1T1);
			HalconAPI.Store(proc, 2, imageRect2T1);
			HalconAPI.Store(proc, 3, imageRect1T2);
			HalconAPI.Store(proc, 4, imageRect2T2);
			HalconAPI.Store(proc, 5, disparity);
			HalconAPI.Store(proc, 0, smoothingFlow);
			HalconAPI.Store(proc, 1, smoothingDisparity);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.Store(proc, 4, camParamRect1);
			HalconAPI.Store(proc, 5, camParamRect2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(smoothingFlow);
			HalconAPI.UnpinTuple(smoothingDisparity);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HObjectModel3D[] result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1T1);
			GC.KeepAlive(imageRect2T1);
			GC.KeepAlive(imageRect1T2);
			GC.KeepAlive(imageRect2T2);
			GC.KeepAlive(disparity);
			return result;
		}

		public HObjectModel3D SceneFlowCalib(HImage imageRect1T1, HImage imageRect2T1, HImage imageRect1T2, HImage imageRect2T2, HImage disparity, double smoothingFlow, double smoothingDisparity, string genParamName, string genParamValue, HCamPar camParamRect1, HCamPar camParamRect2)
		{
			IntPtr proc = HalconAPI.PreCall(1481);
			base.Store(proc, 6);
			HalconAPI.Store(proc, 1, imageRect1T1);
			HalconAPI.Store(proc, 2, imageRect2T1);
			HalconAPI.Store(proc, 3, imageRect1T2);
			HalconAPI.Store(proc, 4, imageRect2T2);
			HalconAPI.Store(proc, 5, disparity);
			HalconAPI.StoreD(proc, 0, smoothingFlow);
			HalconAPI.StoreD(proc, 1, smoothingDisparity);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.Store(proc, 4, camParamRect1);
			HalconAPI.Store(proc, 5, camParamRect2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect1);
			HalconAPI.UnpinTuple(camParamRect2);
			HObjectModel3D result = null;
			err = HObjectModel3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1T1);
			GC.KeepAlive(imageRect2T1);
			GC.KeepAlive(imageRect1T2);
			GC.KeepAlive(imageRect2T2);
			GC.KeepAlive(disparity);
			return result;
		}

		public HTuple VectorToPose(HTuple worldX, HTuple worldY, HTuple worldZ, HTuple imageRow, HTuple imageColumn, HCamPar cameraParam, string method, HTuple qualityType)
		{
			IntPtr proc = HalconAPI.PreCall(1902);
			HalconAPI.Store(proc, 0, worldX);
			HalconAPI.Store(proc, 1, worldY);
			HalconAPI.Store(proc, 2, worldZ);
			HalconAPI.Store(proc, 3, imageRow);
			HalconAPI.Store(proc, 4, imageColumn);
			HalconAPI.Store(proc, 5, cameraParam);
			HalconAPI.StoreS(proc, 6, method);
			HalconAPI.Store(proc, 7, qualityType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(worldX);
			HalconAPI.UnpinTuple(worldY);
			HalconAPI.UnpinTuple(worldZ);
			HalconAPI.UnpinTuple(imageRow);
			HalconAPI.UnpinTuple(imageColumn);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(qualityType);
			err = base.Load(proc, 0, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public double VectorToPose(HTuple worldX, HTuple worldY, HTuple worldZ, HTuple imageRow, HTuple imageColumn, HCamPar cameraParam, string method, string qualityType)
		{
			IntPtr proc = HalconAPI.PreCall(1902);
			HalconAPI.Store(proc, 0, worldX);
			HalconAPI.Store(proc, 1, worldY);
			HalconAPI.Store(proc, 2, worldZ);
			HalconAPI.Store(proc, 3, imageRow);
			HalconAPI.Store(proc, 4, imageColumn);
			HalconAPI.Store(proc, 5, cameraParam);
			HalconAPI.StoreS(proc, 6, method);
			HalconAPI.StoreS(proc, 7, qualityType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(worldX);
			HalconAPI.UnpinTuple(worldY);
			HalconAPI.UnpinTuple(worldZ);
			HalconAPI.UnpinTuple(imageRow);
			HalconAPI.UnpinTuple(imageColumn);
			HalconAPI.UnpinTuple(cameraParam);
			err = base.Load(proc, 0, err);
			double result = 0.0;
			err = HalconAPI.LoadD(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenImageToWorldPlaneMap(HCamPar cameraParam, int widthIn, int heightIn, int widthMapped, int heightMapped, HTuple scale, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1913);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.StoreI(proc, 2, widthIn);
			HalconAPI.StoreI(proc, 3, heightIn);
			HalconAPI.StoreI(proc, 4, widthMapped);
			HalconAPI.StoreI(proc, 5, heightMapped);
			HalconAPI.Store(proc, 6, scale);
			HalconAPI.StoreS(proc, 7, mapType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(scale);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage GenImageToWorldPlaneMap(HCamPar cameraParam, int widthIn, int heightIn, int widthMapped, int heightMapped, string scale, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1913);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.StoreI(proc, 2, widthIn);
			HalconAPI.StoreI(proc, 3, heightIn);
			HalconAPI.StoreI(proc, 4, widthMapped);
			HalconAPI.StoreI(proc, 5, heightMapped);
			HalconAPI.StoreS(proc, 6, scale);
			HalconAPI.StoreS(proc, 7, mapType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(cameraParam);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HImage ImageToWorldPlane(HImage image, HCamPar cameraParam, int width, int height, HTuple scale, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1914);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.Store(proc, 4, scale);
			HalconAPI.StoreS(proc, 5, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(scale);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HImage ImageToWorldPlane(HImage image, HCamPar cameraParam, int width, int height, string scale, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1914);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.StoreS(proc, 4, scale);
			HalconAPI.StoreS(proc, 5, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(cameraParam);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HXLDCont ContourToWorldPlaneXld(HXLDCont contours, HTuple cameraParam, HTuple scale)
		{
			IntPtr proc = HalconAPI.PreCall(1915);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 2, scale);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(scale);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
			return result;
		}

		public HXLDCont ContourToWorldPlaneXld(HXLDCont contours, HTuple cameraParam, string scale)
		{
			IntPtr proc = HalconAPI.PreCall(1915);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.StoreS(proc, 2, scale);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(cameraParam);
			HXLDCont result = null;
			err = HXLDCont.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
			return result;
		}

		public HPose SetOriginPose(double DX, double DY, double DZ)
		{
			IntPtr proc = HalconAPI.PreCall(1917);
			base.Store(proc, 0);
			HalconAPI.StoreD(proc, 1, DX);
			HalconAPI.StoreD(proc, 2, DY);
			HalconAPI.StoreD(proc, 3, DZ);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HPose HandEyeCalibration(HTuple x, HTuple y, HTuple z, HTuple row, HTuple col, HTuple numPoints, HPose[] robotPoses, HCamPar cameraParam, string method, HTuple qualityType, out HPose calibrationPose, out HTuple quality)
		{
			HTuple hTuple = HData.ConcatArray(robotPoses);
			IntPtr proc = HalconAPI.PreCall(1918);
			HalconAPI.Store(proc, 0, x);
			HalconAPI.Store(proc, 1, y);
			HalconAPI.Store(proc, 2, z);
			HalconAPI.Store(proc, 3, row);
			HalconAPI.Store(proc, 4, col);
			HalconAPI.Store(proc, 5, numPoints);
			HalconAPI.Store(proc, 6, hTuple);
			HalconAPI.Store(proc, 7, cameraParam);
			HalconAPI.StoreS(proc, 8, method);
			HalconAPI.Store(proc, 9, qualityType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(x);
			HalconAPI.UnpinTuple(y);
			HalconAPI.UnpinTuple(z);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(numPoints);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(qualityType);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HPose.LoadNew(proc, 1, err, out calibrationPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out quality);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public static HPose HandEyeCalibration(HTuple x, HTuple y, HTuple z, HTuple row, HTuple col, HTuple numPoints, HPose[] robotPoses, HCamPar cameraParam, string method, string qualityType, out HPose calibrationPose, out double quality)
		{
			HTuple hTuple = HData.ConcatArray(robotPoses);
			IntPtr proc = HalconAPI.PreCall(1918);
			HalconAPI.Store(proc, 0, x);
			HalconAPI.Store(proc, 1, y);
			HalconAPI.Store(proc, 2, z);
			HalconAPI.Store(proc, 3, row);
			HalconAPI.Store(proc, 4, col);
			HalconAPI.Store(proc, 5, numPoints);
			HalconAPI.Store(proc, 6, hTuple);
			HalconAPI.Store(proc, 7, cameraParam);
			HalconAPI.StoreS(proc, 8, method);
			HalconAPI.StoreS(proc, 9, qualityType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(x);
			HalconAPI.UnpinTuple(y);
			HalconAPI.UnpinTuple(z);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(numPoints);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(cameraParam);
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			err = HPose.LoadNew(proc, 1, err, out calibrationPose);
			err = HalconAPI.LoadD(proc, 2, err, out quality);
			HalconAPI.PostCall(proc, err);
			return result;
		}

		public string GetPoseType(out string orderOfRotation, out string viewOfTransform)
		{
			IntPtr proc = HalconAPI.PreCall(1919);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			string result = null;
			err = HalconAPI.LoadS(proc, 0, err, out result);
			err = HalconAPI.LoadS(proc, 1, err, out orderOfRotation);
			err = HalconAPI.LoadS(proc, 2, err, out viewOfTransform);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HPose ConvertPoseType(string orderOfTransform, string orderOfRotation, string viewOfTransform)
		{
			IntPtr proc = HalconAPI.PreCall(1920);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, orderOfTransform);
			HalconAPI.StoreS(proc, 2, orderOfRotation);
			HalconAPI.StoreS(proc, 3, viewOfTransform);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HPose result = null;
			err = HPose.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void CreatePose(double transX, double transY, double transZ, double rotX, double rotY, double rotZ, string orderOfTransform, string orderOfRotation, string viewOfTransform)
		{
			IntPtr proc = HalconAPI.PreCall(1921);
			HalconAPI.StoreD(proc, 0, transX);
			HalconAPI.StoreD(proc, 1, transY);
			HalconAPI.StoreD(proc, 2, transZ);
			HalconAPI.StoreD(proc, 3, rotX);
			HalconAPI.StoreD(proc, 4, rotY);
			HalconAPI.StoreD(proc, 5, rotZ);
			HalconAPI.StoreS(proc, 6, orderOfTransform);
			HalconAPI.StoreS(proc, 7, orderOfRotation);
			HalconAPI.StoreS(proc, 8, viewOfTransform);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HHomMat3D CamParPoseToHomMat3d(HCamPar cameraParam)
		{
			IntPtr proc = HalconAPI.PreCall(1933);
			base.Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(cameraParam);
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HHomMat3D PoseToHomMat3d()
		{
			IntPtr proc = HalconAPI.PreCall(1935);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HHomMat3D result = null;
			err = HHomMat3D.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void DeserializePose(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1938);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializePose()
		{
			IntPtr proc = HalconAPI.PreCall(1939);
			base.Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HSerializedItem result = null;
			err = HSerializedItem.LoadNew(proc, 0, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public void ReadPose(string poseFile)
		{
			IntPtr proc = HalconAPI.PreCall(1940);
			HalconAPI.StoreS(proc, 0, poseFile);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = base.Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WritePose(string poseFile)
		{
			IntPtr proc = HalconAPI.PreCall(1941);
			base.Store(proc, 0);
			HalconAPI.StoreS(proc, 1, poseFile);
			int procResult = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HImage SimCaltab(string calPlateDescr, HCamPar cameraParam, int grayBackground, int grayPlate, int grayMarks, double scaleFac)
		{
			IntPtr proc = HalconAPI.PreCall(1944);
			base.Store(proc, 2);
			HalconAPI.StoreS(proc, 0, calPlateDescr);
			HalconAPI.Store(proc, 1, cameraParam);
			HalconAPI.StoreI(proc, 3, grayBackground);
			HalconAPI.StoreI(proc, 4, grayPlate);
			HalconAPI.StoreI(proc, 5, grayMarks);
			HalconAPI.StoreD(proc, 6, scaleFac);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(cameraParam);
			HImage result = null;
			err = HImage.LoadNew(proc, 1, err, out result);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public static HCamPar CameraCalibration(HTuple NX, HTuple NY, HTuple NZ, HTuple NRow, HTuple NCol, HCamPar startCamParam, HPose[] NStartPose, HTuple estimateParams, out HPose[] NFinalPose, out HTuple errors)
		{
			HTuple hTuple = HData.ConcatArray(NStartPose);
			IntPtr proc = HalconAPI.PreCall(1946);
			HalconAPI.Store(proc, 0, NX);
			HalconAPI.Store(proc, 1, NY);
			HalconAPI.Store(proc, 2, NZ);
			HalconAPI.Store(proc, 3, NRow);
			HalconAPI.Store(proc, 4, NCol);
			HalconAPI.Store(proc, 5, startCamParam);
			HalconAPI.Store(proc, 6, hTuple);
			HalconAPI.Store(proc, 7, estimateParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(NX);
			HalconAPI.UnpinTuple(NY);
			HalconAPI.UnpinTuple(NZ);
			HalconAPI.UnpinTuple(NRow);
			HalconAPI.UnpinTuple(NCol);
			HalconAPI.UnpinTuple(startCamParam);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(estimateParams);
			HCamPar result = null;
			err = HCamPar.LoadNew(proc, 0, err, out result);
			HTuple data = null;
			err = HTuple.LoadNew(proc, 1, err, out data);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out errors);
			HalconAPI.PostCall(proc, err);
			NFinalPose = HPose.SplitArray(data);
			return result;
		}

		public HCamPar CameraCalibration(HTuple NX, HTuple NY, HTuple NZ, HTuple NRow, HTuple NCol, HCamPar startCamParam, HTuple estimateParams, out HPose NFinalPose, out double errors)
		{
			IntPtr proc = HalconAPI.PreCall(1946);
			base.Store(proc, 6);
			HalconAPI.Store(proc, 0, NX);
			HalconAPI.Store(proc, 1, NY);
			HalconAPI.Store(proc, 2, NZ);
			HalconAPI.Store(proc, 3, NRow);
			HalconAPI.Store(proc, 4, NCol);
			HalconAPI.Store(proc, 5, startCamParam);
			HalconAPI.Store(proc, 7, estimateParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			base.UnpinTuple();
			HalconAPI.UnpinTuple(NX);
			HalconAPI.UnpinTuple(NY);
			HalconAPI.UnpinTuple(NZ);
			HalconAPI.UnpinTuple(NRow);
			HalconAPI.UnpinTuple(NCol);
			HalconAPI.UnpinTuple(startCamParam);
			HalconAPI.UnpinTuple(estimateParams);
			HCamPar result = null;
			err = HCamPar.LoadNew(proc, 0, err, out result);
			err = HPose.LoadNew(proc, 1, err, out NFinalPose);
			err = HalconAPI.LoadD(proc, 2, err, out errors);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return result;
		}

		public HTuple FindMarksAndPose(HImage image, HRegion calPlateRegion, string calPlateDescr, HCamPar startCamParam, int startThresh, int deltaThresh, int minThresh, double alpha, double minContLength, double maxDiamMarks, out HTuple CCoord)
		{
			IntPtr proc = HalconAPI.PreCall(1947);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 2, calPlateRegion);
			HalconAPI.StoreS(proc, 0, calPlateDescr);
			HalconAPI.Store(proc, 1, startCamParam);
			HalconAPI.StoreI(proc, 2, startThresh);
			HalconAPI.StoreI(proc, 3, deltaThresh);
			HalconAPI.StoreI(proc, 4, minThresh);
			HalconAPI.StoreD(proc, 5, alpha);
			HalconAPI.StoreD(proc, 6, minContLength);
			HalconAPI.StoreD(proc, 7, maxDiamMarks);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(startCamParam);
			err = base.Load(proc, 2, err);
			HTuple result = null;
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out result);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out CCoord);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(calPlateRegion);
			return result;
		}

		public static void SetCameraSetupCamParam(HCameraSetupModel cameraSetupModelID, HTuple cameraIdx, HTuple cameraType, HCamPar cameraParam, HTuple cameraPose)
		{
			IntPtr proc = HalconAPI.PreCall(1957);
			HalconAPI.Store(proc, 0, cameraSetupModelID);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.Store(proc, 2, cameraType);
			HalconAPI.Store(proc, 3, cameraParam);
			HalconAPI.Store(proc, 4, cameraPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraType);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(cameraPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(cameraSetupModelID);
		}

		public static void SetCameraSetupCamParam(HCameraSetupModel cameraSetupModelID, HTuple cameraIdx, string cameraType, HCamPar cameraParam, HTuple cameraPose)
		{
			IntPtr proc = HalconAPI.PreCall(1957);
			HalconAPI.Store(proc, 0, cameraSetupModelID);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, cameraType);
			HalconAPI.Store(proc, 3, cameraParam);
			HalconAPI.Store(proc, 4, cameraPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(cameraPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(cameraSetupModelID);
		}
	}
}
