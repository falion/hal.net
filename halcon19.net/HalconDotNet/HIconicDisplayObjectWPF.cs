using System.ComponentModel;
using System.Windows;

namespace HalconDotNet
{
	public class HIconicDisplayObjectWPF : HDisplayObjectWPF
	{
		public static readonly DependencyProperty IconicObjectProperty = DependencyProperty.Register("IconicObject", typeof(HObject), typeof(HIconicDisplayObjectWPF), new PropertyMetadata(null, OnPropertyChanged));

		public static readonly DependencyProperty HColorProperty = DependencyProperty.Register("HColor", typeof(string), typeof(HIconicDisplayObjectWPF), new PropertyMetadata(null, OnPropertyChanged));

		public static readonly DependencyProperty HColoredProperty = DependencyProperty.Register("HColored", typeof(int?), typeof(HIconicDisplayObjectWPF), new PropertyMetadata(null, OnPropertyChanged));

		public static readonly DependencyProperty HLineStyleProperty = DependencyProperty.Register("LineStyle", typeof(HLineStyleWPF), typeof(HIconicDisplayObjectWPF), new PropertyMetadata(null, OnPropertyChanged));

		public static readonly DependencyProperty HLineWidthProperty = DependencyProperty.Register("LineWidth", typeof(double?), typeof(HIconicDisplayObjectWPF), new PropertyMetadata(null, OnPropertyChanged));

		public static readonly DependencyProperty HDrawProperty = DependencyProperty.Register("HDraw", typeof(string), typeof(HIconicDisplayObjectWPF), new PropertyMetadata(null, OnPropertyChanged));

		private HTuple _tempLineStyle;

		private HTuple _tempLineWidth;

		private HTuple _tempDraw;

		public HObject IconicObject
		{
			get
			{
				return (HObject)GetValue(IconicObjectProperty);
			}
			set
			{
				SetValue(IconicObjectProperty, value);
			}
		}

		[Category("Appearance")]
		[Description("Color for displaying HRegion and HXLDCont objects. If HColor is set, HColored is set to null.")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public string HColor
		{
			get
			{
				return (string)GetValue(HColorProperty);
			}
			set
			{
				SetValue(HColorProperty, value);
			}
		}

		[Description("Set of colors for displaying HRegion and HXLDCont objects. If HColored is set, HColor is set to null.")]
		[Category("Appearance")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public int? HColored
		{
			get
			{
				return (int?)GetValue(HColoredProperty);
			}
			set
			{
				SetValue(HColoredProperty, value);
			}
		}

		[Category("Appearance")]
		[Description("Output pattern of the margin of HRegion objects and of HXLDCont objects.")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public HLineStyleWPF LineStyle
		{
			get
			{
				return (HLineStyleWPF)GetValue(HLineStyleProperty);
			}
			set
			{
				SetValue(HLineStyleProperty, value);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Always)]
		[Description("Line width of the margin of HRegion objects and of HXLDCont objects.")]
		[Category("Appearance")]
		public double? LineWidth
		{
			get
			{
				return (double?)GetValue(HLineWidthProperty);
			}
			set
			{
				SetValue(HLineWidthProperty, value);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Always)]
		[Category("Appearance")]
		[Description("Fill mode of HRegion objects.")]
		public string HDraw
		{
			get
			{
				return (string)GetValue(HDrawProperty);
			}
			set
			{
				SetValue(HDrawProperty, value);
			}
		}

		public HIconicDisplayObjectWPF(HObject obj)
		{
			IconicObject = obj;
		}

		public HIconicDisplayObjectWPF()
		{
		}

		private static void OnPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
		{
			HIconicDisplayObjectWPF hIconicDisplayObjectWPF = source as HIconicDisplayObjectWPF;
			if (hIconicDisplayObjectWPF != null)
			{
				switch (e.Property.Name)
				{
				case "HColor":
					hIconicDisplayObjectWPF.HColored = null;
					break;
				case "HColored":
					hIconicDisplayObjectWPF.HColor = null;
					break;
				}
				hIconicDisplayObjectWPF.ParentHSmartWindowControlWPF?.NotifyItemsChanged();
			}
		}

		public override void Display(HWindow hWindow)
		{
			if (IconicObject != null && IconicObject.IsInitialized())
			{
				if (HColor != null)
				{
					hWindow.SetColor(HColor);
				}
				if (HColored.HasValue)
				{
					hWindow.SetColored(HColored ?? 0);
				}
				if (HDraw != null)
				{
					_tempDraw = hWindow.GetDraw();
					hWindow.SetDraw(HDraw);
				}
				if (LineStyle != null)
				{
					_tempLineStyle = hWindow.GetLineStyle();
					hWindow.SetLineStyle(LineStyle);
				}
				if (LineWidth.HasValue)
				{
					_tempLineWidth = hWindow.GetLineWidth();
					hWindow.SetLineWidth(LineWidth ?? 1.0);
				}
				IconicObject.DispObj(hWindow);
				if (LineStyle != null)
				{
					hWindow.SetLineStyle(_tempLineStyle);
					_tempLineStyle.Dispose();
				}
				if (LineWidth.HasValue)
				{
					hWindow.SetLineWidth(_tempLineWidth);
					_tempLineWidth.Dispose();
				}
				if (HDraw != null)
				{
					hWindow.SetDraw(_tempDraw);
					_tempDraw.Dispose();
				}
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing && IconicObject != null)
				{
					IconicObject.Dispose();
				}
				disposed = true;
			}
		}
	}
}
