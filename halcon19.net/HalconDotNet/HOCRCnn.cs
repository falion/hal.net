using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HOCRCnn : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRCnn()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRCnn(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRCnn(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("ocr_cnn");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRCnn obj)
		{
			obj = new HOCRCnn(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRCnn[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HOCRCnn[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HOCRCnn(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HOCRCnn(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(2082);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeOcrClassCnn();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRCnn(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeOcrClassCnn(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeOcrClassCnn();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HOCRCnn Deserialize(Stream stream)
		{
			HOCRCnn hOCRCnn = new HOCRCnn();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hOCRCnn.DeserializeOcrClassCnn(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRCnn;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HOCRCnn Clone()
		{
			HSerializedItem hSerializedItem = SerializeOcrClassCnn();
			HOCRCnn hOCRCnn = new HOCRCnn();
			hOCRCnn.DeserializeOcrClassCnn(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRCnn;
		}

		public static void ClearOcrClassCnn(HOCRCnn[] OCRHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(OCRHandle);
			IntPtr proc = HalconAPI.PreCall(2046);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(OCRHandle);
		}

		public void ClearOcrClassCnn()
		{
			IntPtr proc = HalconAPI.PreCall(2046);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeOcrClassCnn(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2053);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HTuple DoOcrMultiClassCnn(HRegion character, HImage image, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2056);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DoOcrMultiClassCnn(HRegion character, HImage image, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2056);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return stringValue;
		}

		public HTuple DoOcrSingleClassCnn(HRegion character, HImage image, HTuple num, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2057);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DoOcrSingleClassCnn(HRegion character, HImage image, HTuple num, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2057);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return stringValue;
		}

		public HTuple DoOcrWordCnn(HRegion character, HImage image, string expression, int numAlternatives, int numCorrections, out HTuple confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(2058);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DoOcrWordCnn(HRegion character, HImage image, string expression, int numAlternatives, int numCorrections, out double confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(2058);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return stringValue;
		}

		public HTuple GetParamsOcrClassCnn(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2072);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetParamsOcrClassCnn(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2072);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple QueryParamsOcrClassCnn()
		{
			IntPtr proc = HalconAPI.PreCall(2081);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void ReadOcrClassCnn(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2082);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem SerializeOcrClassCnn()
		{
			IntPtr proc = HalconAPI.PreCall(2093);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}
	}
}
