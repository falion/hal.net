using System;
using System.Windows.Forms;

namespace HalconDotNet
{
	public class HMouseEventArgs : EventArgs
	{
		private readonly MouseButtons button;

		private readonly int clicks;

		private readonly double x;

		private readonly double y;

		private readonly int delta;

		public MouseButtons Button => button;

		public int Clicks => clicks;

		public double X => x;

		public double Y => y;

		public int Delta => delta;

		internal HMouseEventArgs(MouseButtons button, int clicks, double x, double y, int delta)
		{
			this.button = button;
			this.clicks = clicks;
			this.x = x;
			this.y = y;
			this.delta = delta;
		}
	}
}
