using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HDeformableSurfaceMatchingResult : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceMatchingResult()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceMatchingResult(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceMatchingResult(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("deformable_surface_matching_result");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDeformableSurfaceMatchingResult obj)
		{
			obj = new HDeformableSurfaceMatchingResult(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDeformableSurfaceMatchingResult[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDeformableSurfaceMatchingResult[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HDeformableSurfaceMatchingResult(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HTuple GetDeformableSurfaceMatchingResult(HTuple resultName, HTuple resultIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1019);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, resultName);
			HalconAPI.Store(proc, 2, resultIndex);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultName);
			HalconAPI.UnpinTuple(resultIndex);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDeformableSurfaceMatchingResult(string resultName, int resultIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1019);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, resultName);
			HalconAPI.StoreI(proc, 2, resultIndex);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public static void ClearDeformableSurfaceMatchingResult(HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			HTuple hTuple = HHandleBase.ConcatArray(deformableSurfaceMatchingResult);
			IntPtr proc = HalconAPI.PreCall(1020);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(deformableSurfaceMatchingResult);
		}

		public void ClearDeformableSurfaceMatchingResult()
		{
			IntPtr proc = HalconAPI.PreCall(1020);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static HTuple RefineDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, HObjectModel3D objectModel3D, double relSamplingDistance, HObjectModel3D initialDeformationObjectModel3D, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1026);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, initialDeformationObjectModel3D);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(objectModel3D);
			GC.KeepAlive(initialDeformationObjectModel3D);
			return tuple;
		}

		public double RefineDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, HObjectModel3D objectModel3D, double relSamplingDistance, HObjectModel3D initialDeformationObjectModel3D, string genParamName, string genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1026);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, initialDeformationObjectModel3D);
			HalconAPI.StoreS(proc, 4, genParamName);
			HalconAPI.StoreS(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(objectModel3D);
			GC.KeepAlive(initialDeformationObjectModel3D);
			return doubleValue;
		}

		public static HTuple FindDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, HObjectModel3D objectModel3D, double relSamplingDistance, HTuple minScore, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1027);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(objectModel3D);
			return tuple;
		}

		public double FindDeformableSurfaceModel(HDeformableSurfaceModel deformableSurfaceModel, HObjectModel3D objectModel3D, double relSamplingDistance, double minScore, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1027);
			HalconAPI.Store(proc, 0, deformableSurfaceModel);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 1, err);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(deformableSurfaceModel);
			GC.KeepAlive(objectModel3D);
			return doubleValue;
		}
	}
}
