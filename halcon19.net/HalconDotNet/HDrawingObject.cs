using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace HalconDotNet
{
	public class HDrawingObject : HHandle
	{
		public delegate void HDrawingObjectCallback(IntPtr drawid, IntPtr windowHandle, string type);

		public delegate void HDrawingObjectCallbackClass(HDrawingObject drawid, HWindow window, string type);

		public enum HDrawingObjectType
		{
			RECTANGLE1,
			RECTANGLE2,
			CIRCLE,
			ELLIPSE,
			CIRCLE_SECTOR,
			ELLIPSE_SECTOR,
			LINE,
			XLD_CONTOUR,
			TEXT
		}

		private HDrawingObjectCallback onresize;

		private HDrawingObjectCallback onattach;

		private HDrawingObjectCallback ondetach;

		private HDrawingObjectCallback ondrag;

		private HDrawingObjectCallback onselect;

		public long ID => base.Handle.ToInt64();

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDrawingObject()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDrawingObject(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDrawingObject(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("drawing_object");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDrawingObject obj)
		{
			obj = new HDrawingObject(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDrawingObject[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDrawingObject[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HDrawingObject(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HDrawingObject(double row, double column, double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1311);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDrawingObject(double row, double column, double phi, double length1, double length2)
		{
			IntPtr proc = HalconAPI.PreCall(1313);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, length1);
			HalconAPI.StoreD(proc, 4, length2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDrawingObject(double row1, double column1, double row2, double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1314);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		protected IntPtr DelegateToCallbackPointer(HDrawingObjectCallback c)
		{
			return Marshal.GetFunctionPointerForDelegate(c);
		}

		protected IntPtr DelegateToCallbackPointer(HDrawingObjectCallbackClass c, string evt)
		{
			HDrawingObjectCallback hDrawingObjectCallback = delegate(IntPtr drawid, IntPtr window, string type)
			{
				HDrawingObject hDrawingObject = new HDrawingObject(drawid);
				HWindow hWindow = new HWindow(window);
				hDrawingObject.Detach();
				hWindow.Detach();
				c(hDrawingObject, hWindow, type);
			};
			GC.KeepAlive(hDrawingObjectCallback);
			GC.SuppressFinalize(hDrawingObjectCallback);
			switch (evt)
			{
			case "on_resize":
				onresize = hDrawingObjectCallback;
				break;
			case "on_attach":
				onattach = hDrawingObjectCallback;
				break;
			case "on_detach":
				ondetach = hDrawingObjectCallback;
				break;
			case "on_drag":
				ondrag = hDrawingObjectCallback;
				break;
			case "on_select":
				onselect = hDrawingObjectCallback;
				break;
			}
			return Marshal.GetFunctionPointerForDelegate(hDrawingObjectCallback);
		}

		public void OnResize(HDrawingObjectCallback f)
		{
			SetDrawingObjectCallback("on_resize", Marshal.GetFunctionPointerForDelegate(f));
		}

		public void OnAttach(HDrawingObjectCallback f)
		{
			SetDrawingObjectCallback("on_attach", Marshal.GetFunctionPointerForDelegate(f));
		}

		public void OnDetach(HDrawingObjectCallback f)
		{
			SetDrawingObjectCallback("on_detach", Marshal.GetFunctionPointerForDelegate(f));
		}

		public void OnDrag(HDrawingObjectCallback f)
		{
			SetDrawingObjectCallback("on_drag", Marshal.GetFunctionPointerForDelegate(f));
		}

		public void OnSelect(HDrawingObjectCallback f)
		{
			SetDrawingObjectCallback("on_select", Marshal.GetFunctionPointerForDelegate(f));
		}

		public void OnResize(HDrawingObjectCallbackClass f)
		{
			SetDrawingObjectCallback("on_resize", DelegateToCallbackPointer(f, "on_resize"));
		}

		public void OnDrag(HDrawingObjectCallbackClass f)
		{
			SetDrawingObjectCallback("on_drag", DelegateToCallbackPointer(f, "on_drag"));
		}

		public void OnSelect(HDrawingObjectCallbackClass f)
		{
			SetDrawingObjectCallback("on_select", DelegateToCallbackPointer(f, "on_select"));
		}

		public void OnAttach(HDrawingObjectCallbackClass f)
		{
			SetDrawingObjectCallback("on_attach", DelegateToCallbackPointer(f, "on_attach"));
		}

		public void OnDetach(HDrawingObjectCallbackClass f)
		{
			SetDrawingObjectCallback("on_detach", DelegateToCallbackPointer(f, "on_detach"));
		}

		public static HDrawingObject CreateDrawingObject(HDrawingObjectType type, params HTuple[] values)
		{
			HDrawingObject hDrawingObject = new HDrawingObject();
			switch (type)
			{
			case HDrawingObjectType.RECTANGLE1:
				hDrawingObject.CreateDrawingObjectRectangle1(values[0], values[1], values[2], values[3]);
				break;
			case HDrawingObjectType.RECTANGLE2:
				hDrawingObject.CreateDrawingObjectRectangle2(values[0], values[1], values[2], values[3], values[4]);
				break;
			case HDrawingObjectType.CIRCLE:
				hDrawingObject.CreateDrawingObjectCircle(values[0], values[1], values[2]);
				break;
			case HDrawingObjectType.ELLIPSE:
				hDrawingObject.CreateDrawingObjectEllipse(values[0], values[1], values[2], values[3], values[4]);
				break;
			case HDrawingObjectType.CIRCLE_SECTOR:
				hDrawingObject.CreateDrawingObjectCircleSector(values[0], values[1], values[2], values[3], values[4]);
				break;
			case HDrawingObjectType.ELLIPSE_SECTOR:
				hDrawingObject.CreateDrawingObjectEllipseSector(values[0], values[1], values[2], values[3], values[4], values[5], values[6]);
				break;
			case HDrawingObjectType.LINE:
				hDrawingObject.CreateDrawingObjectLine(values[0], values[1], values[2], values[3]);
				break;
			case HDrawingObjectType.XLD_CONTOUR:
				if (values.Length != 2)
				{
					throw new HalconException("Invalid number of parameters");
				}
				if (values[0].Length != values[1].Length)
				{
					throw new HalconException("The length of the input tuples must be identical");
				}
				hDrawingObject.CreateDrawingObjectXld(values[0].DArr, values[1].DArr);
				break;
			case HDrawingObjectType.TEXT:
				hDrawingObject.CreateDrawingObjectText(values[0], values[1], values[2]);
				break;
			}
			return hDrawingObject;
		}

		public void SetDrawingObjectCallback(HTuple drawObjectEvent, HTuple callbackFunction)
		{
			IntPtr proc = HalconAPI.PreCall(1162);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, drawObjectEvent);
			HalconAPI.Store(proc, 2, callbackFunction);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(drawObjectEvent);
			HalconAPI.UnpinTuple(callbackFunction);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDrawingObjectCallback(string drawObjectEvent, IntPtr callbackFunction)
		{
			IntPtr proc = HalconAPI.PreCall(1162);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, drawObjectEvent);
			HalconAPI.StoreIP(proc, 2, callbackFunction);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void DetachBackgroundFromWindow(HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1163);
			HalconAPI.Store(proc, 0, windowHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(windowHandle);
		}

		public static void AttachBackgroundToWindow(HImage image, HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1164);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, windowHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(image);
			GC.KeepAlive(windowHandle);
		}

		public void CreateDrawingObjectText(int row, int column, string stringVal)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1301);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreS(proc, 2, stringVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HObject GetDrawingObjectIconic()
		{
			IntPtr proc = HalconAPI.PreCall(1302);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ClearDrawingObject()
		{
			IntPtr proc = HalconAPI.PreCall(1303);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDrawingObjectParams(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1304);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDrawingObjectParams(string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1304);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetDrawingObjectParams(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1305);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDrawingObjectParams(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1305);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetDrawingObjectXld(HXLDCont contour)
		{
			IntPtr proc = HalconAPI.PreCall(1306);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, contour);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
		}

		public void CreateDrawingObjectXld(HTuple row, HTuple column)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1307);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectCircleSector(double row, double column, double radius, double startAngle, double endAngle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1308);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, startAngle);
			HalconAPI.StoreD(proc, 4, endAngle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectEllipseSector(double row, double column, double phi, double radius1, double radius2, double startAngle, double endAngle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1309);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, radius1);
			HalconAPI.StoreD(proc, 4, radius2);
			HalconAPI.StoreD(proc, 5, startAngle);
			HalconAPI.StoreD(proc, 6, endAngle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectLine(double row1, double column1, double row2, double column2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1310);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectCircle(double row, double column, double radius)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1311);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectEllipse(double row, double column, double phi, double radius1, double radius2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1312);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, radius1);
			HalconAPI.StoreD(proc, 4, radius2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectRectangle2(double row, double column, double phi, double length1, double length2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1313);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, length1);
			HalconAPI.StoreD(proc, 4, length2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDrawingObjectRectangle1(double row1, double column1, double row2, double column2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1314);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static string SendMouseDoubleClickEvent(HWindow windowHandle, HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2088);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return stringValue;
		}

		public static string SendMouseDoubleClickEvent(HWindow windowHandle, int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2088);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return stringValue;
		}

		public static string SendMouseDownEvent(HWindow windowHandle, HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2089);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return stringValue;
		}

		public static string SendMouseDownEvent(HWindow windowHandle, int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2089);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return stringValue;
		}

		public static string SendMouseDragEvent(HWindow windowHandle, HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2090);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return stringValue;
		}

		public static string SendMouseDragEvent(HWindow windowHandle, int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2090);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return stringValue;
		}

		public static string SendMouseUpEvent(HWindow windowHandle, HTuple row, HTuple column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2091);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return stringValue;
		}

		public static string SendMouseUpEvent(HWindow windowHandle, int row, int column, int button)
		{
			IntPtr proc = HalconAPI.PreCall(2091);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, button);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(windowHandle);
			return stringValue;
		}
	}
}
