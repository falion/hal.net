using System.ComponentModel;
using System.Windows;

namespace HalconDotNet
{
	public class HMessageDisplayObjectWPF : HDisplayObjectWPF
	{
		public static readonly DependencyProperty HFontProperty = DependencyProperty.Register("HFont", typeof(string), typeof(HMessageDisplayObjectWPF), new PropertyMetadata(null, OnPropertyChanged));

		public static readonly DependencyProperty MessageTextProperty = DependencyProperty.Register("HMessageText", typeof(string), typeof(HMessageDisplayObjectWPF), new PropertyMetadata(null, OnPropertyChanged));

		public static readonly DependencyProperty CoordinateSystemProperty = DependencyProperty.Register("HCoordinateSystem", typeof(string), typeof(HMessageDisplayObjectWPF), new PropertyMetadata("window", OnPropertyChanged));

		private HTuple _row = new HTuple("top");

		public static readonly DependencyProperty RowProperty = DependencyProperty.Register("HRow", typeof(string), typeof(HMessageDisplayObjectWPF), new PropertyMetadata("top", OnPropertyChanged));

		private HTuple _column = new HTuple("left");

		public static readonly DependencyProperty ColProperty = DependencyProperty.Register("HColumn", typeof(string), typeof(HMessageDisplayObjectWPF), new PropertyMetadata("left", OnPropertyChanged));

		public static readonly DependencyProperty HColorProperty = DependencyProperty.Register("HColor", typeof(string), typeof(HMessageDisplayObjectWPF), new PropertyMetadata("black"));

		private string _tmpFont;

		[Description("Font of the displayes message.")]
		[Category("Appearance")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public string HFont
		{
			get
			{
				return (string)GetValue(HFontProperty);
			}
			set
			{
				SetValue(HFontProperty, value);
			}
		}

		[Description("Text to display in the message.")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Category("Appearance")]
		public string HMessageText
		{
			get
			{
				return (string)GetValue(MessageTextProperty);
			}
			set
			{
				SetValue(MessageTextProperty, value);
			}
		}

		[Description("Coordinate system for the HRow and HColumn properties. Can either be 'window' or 'image'.")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Category("Appearance")]
		public string HCoordinateSystem
		{
			get
			{
				return (string)GetValue(CoordinateSystemProperty);
			}
			set
			{
				SetValue(CoordinateSystemProperty, value);
			}
		}

		[Category("Appearance")]
		[Description("Row coordinate of the message. Can either be 'top', 'middle', 'bottom' or a double or an int")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public string HRow
		{
			get
			{
				return (string)GetValue(RowProperty);
			}
			set
			{
				SetValue(RowProperty, value);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Always)]
		[Category("Appearance")]
		[Description("HColumn coordinate of the message. Can either be 'left', 'center', 'right' or a double or an int")]
		public string HColumn
		{
			get
			{
				return (string)GetValue(ColProperty);
			}
			set
			{
				SetValue(ColProperty, value);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Always)]
		[Category("Appearance")]
		[Description("Color of the text to display in the message.")]
		public string HColor
		{
			get
			{
				return (string)GetValue(HColorProperty);
			}
			set
			{
				SetValue(HColorProperty, value);
			}
		}

		public HMessageDisplayObjectWPF(HTuple msg)
		{
			HMessageText = msg;
		}

		public HMessageDisplayObjectWPF()
		{
		}

		protected override void Dispose(bool disposing)
		{
			if (!disposed)
			{
				disposed = true;
			}
		}

		private static void OnPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
		{
			HMessageDisplayObjectWPF hMessageDisplayObjectWPF = source as HMessageDisplayObjectWPF;
			if (hMessageDisplayObjectWPF == null)
			{
				return;
			}
			double result;
			switch (e.Property.Name)
			{
			case "HRow":
				if (double.TryParse((string)e.NewValue, out result))
				{
					hMessageDisplayObjectWPF._row = new HTuple(result);
				}
				else
				{
					hMessageDisplayObjectWPF._row = new HTuple((string)e.NewValue);
				}
				break;
			case "HColumn":
				if (double.TryParse((string)e.NewValue, out result))
				{
					hMessageDisplayObjectWPF._column = new HTuple(result);
				}
				else
				{
					hMessageDisplayObjectWPF._column = new HTuple((string)e.NewValue);
				}
				break;
			}
			hMessageDisplayObjectWPF.ParentHSmartWindowControlWPF?.NotifyItemsChanged();
		}

		public override void Display(HWindow hWindow)
		{
			if (HMessageText != null)
			{
				if (HFont != null)
				{
					_tmpFont = hWindow.GetFont();
					hWindow.SetFont(HFont);
				}
				hWindow.DispText(new HTuple(HMessageText), HCoordinateSystem, _row, _column, new HTuple(HColor), new HTuple(), new HTuple());
				if (HFont != null)
				{
					hWindow.SetFont(_tmpFont);
				}
			}
		}
	}
}
