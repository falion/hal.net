namespace HalconDotNet
{
	internal class HDevOutputParamVector : HDevOutputParam
	{
		protected HVector mVector;

		protected HTuple mIndex;

		public HDevOutputParamVector(HVector vector, HTuple index, bool global)
			: base(global)
		{
			mVector = vector;
			mIndex = index.Clone();
		}

		public override void StoreIconicParamObject(HObject obj)
		{
			HObjectVector hObjectVector = (HObjectVector)mVector;
			for (int i = 0; i < mIndex.Length; i++)
			{
				hObjectVector = hObjectVector[mIndex[i]];
			}
			hObjectVector.O.TransferOwnership(obj);
		}

		public override void StoreCtrlParamTuple(HTuple tuple)
		{
			HTupleVector hTupleVector = (HTupleVector)mVector;
			for (int i = 0; i < mIndex.Length; i++)
			{
				hTupleVector = hTupleVector[mIndex[i]];
			}
			hTupleVector.T.TransferOwnership(tuple);
		}

		private void StoreParamVector(HVector vector)
		{
			HVector hVector = mVector;
			for (int i = 0; i < mIndex.Length; i++)
			{
				hVector = hVector[mIndex[i]];
			}
			hVector.TransferOwnership(vector);
		}

		public override void StoreIconicParamVector(HObjectVector vector)
		{
			StoreParamVector(vector);
		}

		public override void StoreCtrlParamVector(HTupleVector vector)
		{
			StoreParamVector(vector);
		}
	}
}
