using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HVariationModel : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HVariationModel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HVariationModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HVariationModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("variation_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HVariationModel obj)
		{
			obj = new HVariationModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HVariationModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HVariationModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HVariationModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HVariationModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(83);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HVariationModel(int width, int height, string type, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(95);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.StoreS(proc, 2, type);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeVariationModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HVariationModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeVariationModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeVariationModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HVariationModel Deserialize(Stream stream)
		{
			HVariationModel hVariationModel = new HVariationModel();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hVariationModel.DeserializeVariationModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hVariationModel;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HVariationModel Clone()
		{
			HSerializedItem hSerializedItem = SerializeVariationModel();
			HVariationModel hVariationModel = new HVariationModel();
			hVariationModel.DeserializeVariationModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hVariationModel;
		}

		public void DeserializeVariationModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(81);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeVariationModel()
		{
			IntPtr proc = HalconAPI.PreCall(82);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadVariationModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(83);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteVariationModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(84);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HImage GetThreshImagesVariationModel(out HImage maxImage)
		{
			IntPtr proc = HalconAPI.PreCall(85);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out maxImage);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage GetVariationModel(out HImage varImage)
		{
			IntPtr proc = HalconAPI.PreCall(86);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out varImage);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion CompareExtVariationModel(HImage image, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(87);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HRegion CompareVariationModel(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(88);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void PrepareDirectVariationModel(HImage refImage, HImage varImage, HTuple absThreshold, HTuple varThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(89);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, refImage);
			HalconAPI.Store(proc, 2, varImage);
			HalconAPI.Store(proc, 1, absThreshold);
			HalconAPI.Store(proc, 2, varThreshold);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(absThreshold);
			HalconAPI.UnpinTuple(varThreshold);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(refImage);
			GC.KeepAlive(varImage);
		}

		public void PrepareDirectVariationModel(HImage refImage, HImage varImage, double absThreshold, double varThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(89);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, refImage);
			HalconAPI.Store(proc, 2, varImage);
			HalconAPI.StoreD(proc, 1, absThreshold);
			HalconAPI.StoreD(proc, 2, varThreshold);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(refImage);
			GC.KeepAlive(varImage);
		}

		public void PrepareVariationModel(HTuple absThreshold, HTuple varThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(90);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, absThreshold);
			HalconAPI.Store(proc, 2, varThreshold);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(absThreshold);
			HalconAPI.UnpinTuple(varThreshold);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void PrepareVariationModel(double absThreshold, double varThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(90);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, absThreshold);
			HalconAPI.StoreD(proc, 2, varThreshold);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TrainVariationModel(HImage images)
		{
			IntPtr proc = HalconAPI.PreCall(91);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, images);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(images);
		}

		public void ClearVariationModel()
		{
			IntPtr proc = HalconAPI.PreCall(93);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ClearTrainDataVariationModel()
		{
			IntPtr proc = HalconAPI.PreCall(94);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateVariationModel(int width, int height, string type, string mode)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(95);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.StoreS(proc, 2, type);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
