using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HCameraSetupModel : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCameraSetupModel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCameraSetupModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCameraSetupModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("camera_setup_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCameraSetupModel obj)
		{
			obj = new HCameraSetupModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCameraSetupModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HCameraSetupModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HCameraSetupModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HCameraSetupModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1954);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HCameraSetupModel(int numCameras)
		{
			IntPtr proc = HalconAPI.PreCall(1958);
			HalconAPI.StoreI(proc, 0, numCameras);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeCameraSetupModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCameraSetupModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeCameraSetupModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeCameraSetupModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HCameraSetupModel Deserialize(Stream stream)
		{
			HCameraSetupModel hCameraSetupModel = new HCameraSetupModel();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hCameraSetupModel.DeserializeCameraSetupModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hCameraSetupModel;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HCameraSetupModel Clone()
		{
			HSerializedItem hSerializedItem = SerializeCameraSetupModel();
			HCameraSetupModel hCameraSetupModel = new HCameraSetupModel();
			hCameraSetupModel.DeserializeCameraSetupModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hCameraSetupModel;
		}

		public HStereoModel CreateStereoModel(string method, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(527);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HStereoModel.LoadNew(proc, 0, err, out HStereoModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HStereoModel CreateStereoModel(string method, string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(527);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, method);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			err = HStereoModel.LoadNew(proc, 0, err, out HStereoModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ClearCameraSetupModel()
		{
			IntPtr proc = HalconAPI.PreCall(1950);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HSerializedItem SerializeCameraSetupModel()
		{
			IntPtr proc = HalconAPI.PreCall(1951);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void DeserializeCameraSetupModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1952);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public void WriteCameraSetupModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1953);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadCameraSetupModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1954);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetCameraSetupParam(HTuple cameraIdx, string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1955);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetCameraSetupParam(int cameraIdx, string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1955);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetCameraSetupParam(HTuple cameraIdx, string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1956);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCameraSetupParam(int cameraIdx, string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1956);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCameraSetupCamParam(HTuple cameraIdx, HTuple cameraType, HCamPar cameraParam, HTuple cameraPose)
		{
			IntPtr proc = HalconAPI.PreCall(1957);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.Store(proc, 2, cameraType);
			HalconAPI.Store(proc, 3, cameraParam);
			HalconAPI.Store(proc, 4, cameraPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraType);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(cameraPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCameraSetupCamParam(HTuple cameraIdx, string cameraType, HCamPar cameraParam, HTuple cameraPose)
		{
			IntPtr proc = HalconAPI.PreCall(1957);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, cameraType);
			HalconAPI.Store(proc, 3, cameraParam);
			HalconAPI.Store(proc, 4, cameraPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(cameraPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateCameraSetupModel(int numCameras)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1958);
			HalconAPI.StoreI(proc, 0, numCameras);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
