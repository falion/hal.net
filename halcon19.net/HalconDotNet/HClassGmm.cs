using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HClassGmm : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassGmm()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassGmm(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassGmm(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("class_gmm");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassGmm obj)
		{
			obj = new HClassGmm(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassGmm[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HClassGmm[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HClassGmm(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HClassGmm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1828);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassGmm(int numDim, int numClasses, HTuple numCenters, string covarType, string preprocessing, int numComponents, int randSeed)
		{
			IntPtr proc = HalconAPI.PreCall(1840);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.Store(proc, 2, numCenters);
			HalconAPI.StoreS(proc, 3, covarType);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numCenters);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassGmm(int numDim, int numClasses, int numCenters, string covarType, string preprocessing, int numComponents, int randSeed)
		{
			IntPtr proc = HalconAPI.PreCall(1840);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.StoreI(proc, 2, numCenters);
			HalconAPI.StoreS(proc, 3, covarType);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeClassGmm();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassGmm(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeClassGmm(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeClassGmm();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HClassGmm Deserialize(Stream stream)
		{
			HClassGmm hClassGmm = new HClassGmm();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hClassGmm.DeserializeClassGmm(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassGmm;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HClassGmm Clone()
		{
			HSerializedItem hSerializedItem = SerializeClassGmm();
			HClassGmm hClassGmm = new HClassGmm();
			hClassGmm.DeserializeClassGmm(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassGmm;
		}

		public HRegion ClassifyImageClassGmm(HImage image, double rejectionThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(431);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, rejectionThreshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void AddSamplesImageClassGmm(HImage image, HRegion classRegions, double randomize)
		{
			IntPtr proc = HalconAPI.PreCall(432);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 2, classRegions);
			HalconAPI.StoreD(proc, 1, randomize);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(classRegions);
		}

		public HClassTrainData GetClassTrainDataGmm()
		{
			IntPtr proc = HalconAPI.PreCall(1785);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HClassTrainData.LoadNew(proc, 0, err, out HClassTrainData obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void AddClassTrainDataGmm(HClassTrainData classTrainDataHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1786);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, classTrainDataHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
		}

		public HTuple SelectFeatureSetGmm(HClassTrainData classTrainDataHandle, string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1801);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return tuple;
		}

		public HTuple SelectFeatureSetGmm(HClassTrainData classTrainDataHandle, string selectionMethod, string genParamName, double genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1801);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return tuple;
		}

		public HClassLUT CreateClassLutGmm(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1820);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HClassLUT.LoadNew(proc, 0, err, out HClassLUT obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public static void ClearClassGmm(HClassGmm[] GMMHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(GMMHandle);
			IntPtr proc = HalconAPI.PreCall(1824);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(GMMHandle);
		}

		public void ClearClassGmm()
		{
			IntPtr proc = HalconAPI.PreCall(1824);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void ClearSamplesClassGmm(HClassGmm[] GMMHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(GMMHandle);
			IntPtr proc = HalconAPI.PreCall(1825);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(GMMHandle);
		}

		public void ClearSamplesClassGmm()
		{
			IntPtr proc = HalconAPI.PreCall(1825);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeClassGmm(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1826);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeClassGmm()
		{
			IntPtr proc = HalconAPI.PreCall(1827);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadClassGmm(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1828);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteClassGmm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1829);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadSamplesClassGmm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1830);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteSamplesClassGmm(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1831);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ClassifyClassGmm(HTuple features, int num, out HTuple classProb, out HTuple density, out HTuple KSigmaProb)
		{
			IntPtr proc = HalconAPI.PreCall(1832);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.StoreI(proc, 2, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out classProb);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out density);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out KSigmaProb);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple EvaluateClassGmm(HTuple features, out double density, out double KSigmaProb)
		{
			IntPtr proc = HalconAPI.PreCall(1833);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HalconAPI.LoadD(proc, 1, err, out density);
			err = HalconAPI.LoadD(proc, 2, err, out KSigmaProb);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TrainClassGmm(int maxIter, double threshold, string classPriors, double regularize, out HTuple iter)
		{
			IntPtr proc = HalconAPI.PreCall(1834);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, maxIter);
			HalconAPI.StoreD(proc, 2, threshold);
			HalconAPI.StoreS(proc, 3, classPriors);
			HalconAPI.StoreD(proc, 4, regularize);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out iter);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetPrepInfoClassGmm(string preprocessing, out HTuple cumInformationCont)
		{
			IntPtr proc = HalconAPI.PreCall(1835);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, preprocessing);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cumInformationCont);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int GetSampleNumClassGmm()
		{
			IntPtr proc = HalconAPI.PreCall(1836);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple GetSampleClassGmm(int numSample, out int classID)
		{
			IntPtr proc = HalconAPI.PreCall(1837);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, numSample);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HalconAPI.LoadI(proc, 1, err, out classID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void AddSampleClassGmm(HTuple features, int classID, double randomize)
		{
			IntPtr proc = HalconAPI.PreCall(1838);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.StoreI(proc, 2, classID);
			HalconAPI.StoreD(proc, 3, randomize);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int GetParamsClassGmm(out int numClasses, out HTuple minCenters, out HTuple maxCenters, out string covarType)
		{
			IntPtr proc = HalconAPI.PreCall(1839);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadI(proc, 1, err, out numClasses);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out minCenters);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out maxCenters);
			err = HalconAPI.LoadS(proc, 4, err, out covarType);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public void CreateClassGmm(int numDim, int numClasses, HTuple numCenters, string covarType, string preprocessing, int numComponents, int randSeed)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1840);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.Store(proc, 2, numCenters);
			HalconAPI.StoreS(proc, 3, covarType);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numCenters);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateClassGmm(int numDim, int numClasses, int numCenters, string covarType, string preprocessing, int numComponents, int randSeed)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1840);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.StoreI(proc, 2, numCenters);
			HalconAPI.StoreS(proc, 3, covarType);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
