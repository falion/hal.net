using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace HalconDotNet
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class HDevDisposeHelper : IDisposable
	{
		protected List<IDisposable> mDisposables;

		public HDevDisposeHelper()
		{
			mDisposables = new List<IDisposable>();
		}

		public IDisposable Add(IDisposable disposable)
		{
			for (int i = 0; i < mDisposables.Count; i++)
			{
				if (mDisposables[i] == disposable)
				{
					return disposable;
				}
			}
			mDisposables.Add(disposable);
			return disposable;
		}

		public IDisposable Take(IDisposable disposable)
		{
			for (int i = 0; i < mDisposables.Count; i++)
			{
				if (mDisposables[i] == disposable)
				{
					mDisposables.RemoveAt(i);
					return disposable;
				}
			}
			return disposable;
		}

		void IDisposable.Dispose()
		{
			for (int i = 0; i < mDisposables.Count; i++)
			{
				mDisposables[i].Dispose();
			}
			mDisposables.Clear();
		}

		public HObjectVector Add(HObjectVector disposable)
		{
			return (HObjectVector)Add((IDisposable)disposable);
		}

		public HObjectVector Take(HObjectVector disposable)
		{
			return (HObjectVector)Take((IDisposable)disposable);
		}

		public HObject Add(HObject disposable)
		{
			return (HObject)Add((IDisposable)disposable);
		}

		public HObject Take(HObject disposable)
		{
			return (HObject)Take((IDisposable)disposable);
		}

		public HTupleVector Add(HTupleVector disposable)
		{
			return (HTupleVector)Add((IDisposable)disposable);
		}

		public HTupleVector Take(HTupleVector disposable)
		{
			return (HTupleVector)Take((IDisposable)disposable);
		}

		public HTuple Add(HTuple disposable)
		{
			return (HTuple)Add((IDisposable)disposable);
		}

		public HTuple Take(HTuple disposable)
		{
			return (HTuple)Take((IDisposable)disposable);
		}
	}
}
