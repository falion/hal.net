using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HScene3D : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HScene3D(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HScene3D(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("scene_3d");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HScene3D obj)
		{
			obj = new HScene3D(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HScene3D[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HScene3D[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HScene3D(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HScene3D()
		{
			IntPtr proc = HalconAPI.PreCall(1220);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetDisplayScene3dInfo(HWindow windowHandle, HTuple row, HTuple column, HTuple information)
		{
			IntPtr proc = HalconAPI.PreCall(1204);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.Store(proc, 4, information);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(information);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
			return tuple;
		}

		public int GetDisplayScene3dInfo(HWindow windowHandle, double row, double column, string information)
		{
			IntPtr proc = HalconAPI.PreCall(1204);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreD(proc, 2, row);
			HalconAPI.StoreD(proc, 3, column);
			HalconAPI.StoreS(proc, 4, information);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
			return intValue;
		}

		public void SetScene3dToWorldPose(HPose toWorldPose)
		{
			IntPtr proc = HalconAPI.PreCall(1205);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, toWorldPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(toWorldPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1206);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1206);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dLightParam(int lightIndex, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1207);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, lightIndex);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dLightParam(int lightIndex, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1207);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, lightIndex);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dInstancePose(HTuple instanceIndex, HPose[] pose)
		{
			HTuple hTuple = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1208);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, instanceIndex);
			HalconAPI.Store(proc, 2, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(instanceIndex);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dInstancePose(int instanceIndex, HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(1208);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, instanceIndex);
			HalconAPI.Store(proc, 2, pose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dInstanceParam(HTuple instanceIndex, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1209);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, instanceIndex);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(instanceIndex);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dInstanceParam(int instanceIndex, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1209);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, instanceIndex);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dCameraPose(int cameraIndex, HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(1210);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIndex);
			HalconAPI.Store(proc, 2, pose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HImage RenderScene3d(int cameraIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1211);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIndex);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void RemoveScene3dLight(int lightIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1212);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, lightIndex);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void RemoveScene3dInstance(HTuple instanceIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1213);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, instanceIndex);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(instanceIndex);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void RemoveScene3dInstance(int instanceIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1213);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, instanceIndex);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void RemoveScene3dCamera(int cameraIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1214);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIndex);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DisplayScene3d(HWindow windowHandle, HTuple cameraIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1215);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 2, cameraIndex);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIndex);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DisplayScene3d(HWindow windowHandle, string cameraIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1215);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 2, cameraIndex);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public int AddScene3dLight(HTuple lightPosition, string lightKind)
		{
			IntPtr proc = HalconAPI.PreCall(1216);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, lightPosition);
			HalconAPI.StoreS(proc, 2, lightKind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(lightPosition);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int AddScene3dInstance(HObjectModel3D[] objectModel3D, HPose[] pose)
		{
			HTuple hTuple = HHandleBase.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1217);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, hTuple);
			HalconAPI.Store(proc, 2, hTuple2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return intValue;
		}

		public int AddScene3dInstance(HObjectModel3D objectModel3D, HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(1217);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return intValue;
		}

		public int AddScene3dCamera(HCamPar cameraParam)
		{
			IntPtr proc = HalconAPI.PreCall(1218);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraParam);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public static void ClearScene3d(HScene3D[] scene3D)
		{
			HTuple hTuple = HHandleBase.ConcatArray(scene3D);
			IntPtr proc = HalconAPI.PreCall(1219);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(scene3D);
		}

		public void ClearScene3d()
		{
			IntPtr proc = HalconAPI.PreCall(1219);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateScene3d()
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1220);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public int AddScene3dLabel(HTuple text, HTuple referencePoint, HTuple position, HTuple relatesTo)
		{
			IntPtr proc = HalconAPI.PreCall(2040);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, text);
			HalconAPI.Store(proc, 2, referencePoint);
			HalconAPI.Store(proc, 3, position);
			HalconAPI.Store(proc, 4, relatesTo);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(text);
			HalconAPI.UnpinTuple(referencePoint);
			HalconAPI.UnpinTuple(position);
			HalconAPI.UnpinTuple(relatesTo);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int AddScene3dLabel(string text, HTuple referencePoint, HTuple position, HTuple relatesTo)
		{
			IntPtr proc = HalconAPI.PreCall(2040);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, text);
			HalconAPI.Store(proc, 2, referencePoint);
			HalconAPI.Store(proc, 3, position);
			HalconAPI.Store(proc, 4, relatesTo);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(referencePoint);
			HalconAPI.UnpinTuple(position);
			HalconAPI.UnpinTuple(relatesTo);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public void RemoveScene3dLabel(HTuple labelIndex)
		{
			IntPtr proc = HalconAPI.PreCall(2041);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, labelIndex);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(labelIndex);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void RemoveScene3dLabel(int labelIndex)
		{
			IntPtr proc = HalconAPI.PreCall(2041);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, labelIndex);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dLabelParam(HTuple labelIndex, string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2042);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, labelIndex);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(labelIndex);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetScene3dLabelParam(int labelIndex, string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2042);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, labelIndex);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
