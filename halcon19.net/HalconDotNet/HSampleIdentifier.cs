using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HSampleIdentifier : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSampleIdentifier()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSampleIdentifier(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSampleIdentifier(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("sample_identifier");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSampleIdentifier obj)
		{
			obj = new HSampleIdentifier(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSampleIdentifier[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HSampleIdentifier[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HSampleIdentifier(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HSampleIdentifier(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(901);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSampleIdentifier(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(915);
			HalconAPI.Store(proc, 0, genParamName);
			HalconAPI.Store(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeSampleIdentifier();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSampleIdentifier(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeSampleIdentifier(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeSampleIdentifier();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HSampleIdentifier Deserialize(Stream stream)
		{
			HSampleIdentifier hSampleIdentifier = new HSampleIdentifier();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hSampleIdentifier.DeserializeSampleIdentifier(hSerializedItem);
			hSerializedItem.Dispose();
			return hSampleIdentifier;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HSampleIdentifier Clone()
		{
			HSerializedItem hSerializedItem = SerializeSampleIdentifier();
			HSampleIdentifier hSampleIdentifier = new HSampleIdentifier();
			hSampleIdentifier.DeserializeSampleIdentifier(hSerializedItem);
			hSerializedItem.Dispose();
			return hSampleIdentifier;
		}

		public void ClearSampleIdentifier()
		{
			IntPtr proc = HalconAPI.PreCall(899);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeSampleIdentifier(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(900);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public void ReadSampleIdentifier(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(901);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem SerializeSampleIdentifier()
		{
			IntPtr proc = HalconAPI.PreCall(902);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void WriteSampleIdentifier(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(903);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ApplySampleIdentifier(HImage image, int numResults, double ratingThreshold, HTuple genParamName, HTuple genParamValue, out HTuple rating)
		{
			IntPtr proc = HalconAPI.PreCall(904);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 1, numResults);
			HalconAPI.StoreD(proc, 2, ratingThreshold);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out rating);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public int ApplySampleIdentifier(HImage image, int numResults, double ratingThreshold, HTuple genParamName, HTuple genParamValue, out double rating)
		{
			IntPtr proc = HalconAPI.PreCall(904);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 1, numResults);
			HalconAPI.StoreD(proc, 2, ratingThreshold);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadD(proc, 1, err, out rating);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return intValue;
		}

		public HTuple GetSampleIdentifierParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(905);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetSampleIdentifierParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(906);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetSampleIdentifierParam(string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(906);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetSampleIdentifierObjectInfo(HTuple objectIdx, HTuple infoName)
		{
			IntPtr proc = HalconAPI.PreCall(907);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, infoName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectIdx);
			HalconAPI.UnpinTuple(infoName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetSampleIdentifierObjectInfo(int objectIdx, string infoName)
		{
			IntPtr proc = HalconAPI.PreCall(907);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, objectIdx);
			HalconAPI.StoreS(proc, 2, infoName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetSampleIdentifierObjectInfo(HTuple objectIdx, string infoName, HTuple infoValue)
		{
			IntPtr proc = HalconAPI.PreCall(908);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectIdx);
			HalconAPI.StoreS(proc, 2, infoName);
			HalconAPI.Store(proc, 3, infoValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectIdx);
			HalconAPI.UnpinTuple(infoValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetSampleIdentifierObjectInfo(int objectIdx, string infoName, string infoValue)
		{
			IntPtr proc = HalconAPI.PreCall(908);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, objectIdx);
			HalconAPI.StoreS(proc, 2, infoName);
			HalconAPI.StoreS(proc, 3, infoValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void RemoveSampleIdentifierTrainingData(HTuple objectIdx, HTuple objectSampleIdx)
		{
			IntPtr proc = HalconAPI.PreCall(909);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, objectSampleIdx);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectIdx);
			HalconAPI.UnpinTuple(objectSampleIdx);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void RemoveSampleIdentifierTrainingData(int objectIdx, int objectSampleIdx)
		{
			IntPtr proc = HalconAPI.PreCall(909);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, objectIdx);
			HalconAPI.StoreI(proc, 2, objectSampleIdx);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void RemoveSampleIdentifierPreparationData(HTuple objectIdx, HTuple objectSampleIdx)
		{
			IntPtr proc = HalconAPI.PreCall(910);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, objectSampleIdx);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectIdx);
			HalconAPI.UnpinTuple(objectSampleIdx);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void RemoveSampleIdentifierPreparationData(int objectIdx, int objectSampleIdx)
		{
			IntPtr proc = HalconAPI.PreCall(910);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, objectIdx);
			HalconAPI.StoreI(proc, 2, objectSampleIdx);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TrainSampleIdentifier(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(911);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int AddSampleIdentifierTrainingData(HImage sampleImage, HTuple objectIdx, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(912);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, sampleImage);
			HalconAPI.Store(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectIdx);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampleImage);
			return intValue;
		}

		public int AddSampleIdentifierTrainingData(HImage sampleImage, int objectIdx, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(912);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, sampleImage);
			HalconAPI.StoreI(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampleImage);
			return intValue;
		}

		public void PrepareSampleIdentifier(string removePreparationData, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(913);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, removePreparationData);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int AddSampleIdentifierPreparationData(HImage sampleImage, HTuple objectIdx, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(914);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, sampleImage);
			HalconAPI.Store(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectIdx);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampleImage);
			return intValue;
		}

		public int AddSampleIdentifierPreparationData(HImage sampleImage, int objectIdx, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(914);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, sampleImage);
			HalconAPI.StoreI(proc, 1, objectIdx);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampleImage);
			return intValue;
		}

		public void CreateSampleIdentifier(HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(915);
			HalconAPI.Store(proc, 0, genParamName);
			HalconAPI.Store(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
