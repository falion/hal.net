using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace HalconDotNet
{
	[SuppressUnmanagedCodeSecurity]
	public class HalconAPI
	{
		public delegate int HFramegrabberCallback(IntPtr handle, IntPtr userContext, IntPtr context);

		public delegate void HProgressBarCallback(IntPtr id, string operatorName, double progress, string message);

		public delegate void HLowLevelErrorCallback(string err);

		public delegate void HClearProcCallBack(IntPtr ptr);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public delegate IntPtr HDevThreadInternalCallback(IntPtr devThread);

		private const string HalconDLL = "halcon";

		private const CallingConvention HalconCall = CallingConvention.Cdecl;

		internal const int H_MSG_OK = 2;

		internal const int H_MSG_TRUE = 2;

		internal const int H_MSG_FALSE = 3;

		internal const int H_MSG_VOID = 4;

		internal const int H_MSG_FAIL = 5;

		public static readonly bool isPlatform64 = IntPtr.Size > 4;

		public static readonly bool isWindows = testWindows();

		private HalconAPI()
		{
		}

		private static bool testWindows()
		{
			int platform = (int)Environment.OSVersion.Platform;
			if (platform != 4)
			{
				return platform != 128;
			}
			return false;
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIDoLicenseError")]
		public static extern void DoLicenseError(bool state);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIUseSpinLock")]
		public static extern void UseSpinLock(bool state);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIStartUpThreadPool")]
		public static extern void StartUpThreadPool(bool state);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICancelDraw")]
		public static extern void CancelDraw();

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIIsUTF8Encoding")]
		private static extern bool IsUTF8Encoding();

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIGetSerializedSize(IntPtr ptr, out ulong size);

		internal static int GetSerializedSize(byte[] header, out ulong size)
		{
			GCHandle gCHandle = GCHandle.Alloc(header, GCHandleType.Pinned);
			IntPtr ptr = gCHandle.AddrOfPinnedObject();
			int result = HLIGetSerializedSize(ptr, out size);
			gCHandle.Free();
			return result;
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLILock")]
		internal static extern void Lock();

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIUnlock")]
		internal static extern void Unlock();

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXCreateHThreadContext(out IntPtr context);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXClearHThreadContext(IntPtr context);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXCreateHThread(IntPtr contextHandle, out IntPtr threadHandle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXClearHThread(IntPtr threadHandle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXExitHThread(IntPtr threadHandle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXStartHThreadDotNet(IntPtr threadHandle, HDevThreadInternalCallback proc, IntPtr data, out IntPtr threadId);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXPrepareDirectCall(IntPtr threadHandle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXJoinHThread(IntPtr threadId);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXThreadLockLocalVar(IntPtr threadHandle, out IntPtr referenceCount);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXThreadUnlockLocalVar(IntPtr threadHandle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXThreadLockGlobalVar(IntPtr threadHandle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HXThreadUnlockGlobalVar(IntPtr threadHandle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICreateProcedure")]
		private static extern int CreateProcedure(int procIndex, out IntPtr proc);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICallProcedure")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int CallProcedure(IntPtr proc);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIDestroyProcedure")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int DestroyProcedure(IntPtr proc, int procResult);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern IntPtr HLIGetLogicalName(IntPtr proc);

		internal static string GetLogicalName(IntPtr proc)
		{
			return Marshal.PtrToStringAnsi(HLIGetLogicalName(proc));
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLILogicalName")]
		private static extern IntPtr HLIGetLogicalName(int procIndex);

		internal static string GetLogicalName(int procIndex)
		{
			return Marshal.PtrToStringAnsi(HLIGetLogicalName(procIndex));
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetProcIndex")]
		private static extern int GetProcIndex(IntPtr proc);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIGetErrorMessage(int err, IntPtr buffer);

		internal static string GetErrorMessage(int err)
		{
			IntPtr intPtr = Marshal.AllocHGlobal(1024);
			HLIGetErrorMessage(err, intPtr);
			string result = FromHalconEncoding(intPtr, force_utf8: false);
			Marshal.FreeHGlobal(intPtr);
			return result;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IntPtr PreCall(int procIndex)
		{
			IntPtr proc;
			int num = CreateProcedure(procIndex, out proc);
			if (num != 2)
			{
				HOperatorException.throwInfo(num, "Could not create a new operator instance for id " + procIndex);
			}
			return proc;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void PostCall(IntPtr proc, int procResult)
		{
			int procIndex = GetProcIndex(proc);
			HLIClearAllIOCT(proc);
			int err = DestroyProcedure(proc, procResult);
			if (procIndex >= 0)
			{
				HOperatorException.throwOperator(err, procIndex);
				HOperatorException.throwOperator(procResult, procIndex);
			}
			else
			{
				HOperatorException.throwOperator(err, "Unknown");
				HOperatorException.throwOperator(procResult, "Unknown");
			}
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetInputObject")]
		internal static extern int SetInputObject(IntPtr proc, int parIndex, IntPtr key);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetOutputObject")]
		internal static extern int GetOutputObject(IntPtr proc, int parIndex, out IntPtr key);

		internal static void ClearObject(IntPtr key)
		{
			IntPtr proc = PreCall(585);
			HCkP(proc, SetInputObject(proc, 1, key));
			int procResult = CallProcedure(proc);
			PostCall(proc, procResult);
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLICopyObject(IntPtr keyIn, out IntPtr keyOut);

		internal static IntPtr CopyObject(IntPtr key)
		{
			IntPtr proc = PreCall(583);
			HCkP(proc, SetInputObject(proc, 1, key));
			StoreI(proc, 0, 1);
			StoreI(proc, 1, -1);
			int num = CallProcedure(proc);
			if (!IsFailure(num))
			{
				num = GetOutputObject(proc, 1, out key);
			}
			PostCall(proc, num);
			return key;
		}

		internal static string GetObjClass(IntPtr key)
		{
			HTuple tuple = "object";
			IntPtr proc = PreCall(594);
			HCkP(proc, SetInputObject(proc, 1, key));
			InitOCT(proc, 0);
			int num = CallProcedure(proc);
			if (!IsFailure(num))
			{
				num = HTuple.LoadNew(proc, 0, num, out tuple);
			}
			PostCall(proc, num);
			if (tuple.Length <= 0)
			{
				return "any";
			}
			return tuple.S;
		}

		internal static void AssertObjectClass(IntPtr key, string assertClass)
		{
			if (key != HObjectBase.UNDEF)
			{
				string objClass = GetObjClass(key);
				if (!objClass.StartsWith(assertClass) && objClass != "any")
				{
					throw new HalconException("Iconic object type mismatch (expected " + assertClass + ", got " + objClass + ")");
				}
			}
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICreateTuple")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int CreateTuple(out IntPtr tuple);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIInitOCT")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int InitOCT(IntPtr proc, int parIndex);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int HLIClearAllIOCT(IntPtr proc);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIDestroyTuple")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static extern int DestroyTuple(IntPtr tuple);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreTuple(IntPtr tupleHandle, HTuple tuple)
		{
			HTupleType type = (tuple.Type == HTupleType.LONG) ? HTupleType.INTEGER : tuple.Type;
			HCkH(CreateElementsOfType(tupleHandle, tuple.Length, type));
			switch (tuple.Type)
			{
			case HTupleType.INTEGER:
				HCkH(SetIArr(tupleHandle, tuple.IArr));
				break;
			case HTupleType.LONG:
				HCkH(SetLArr(tupleHandle, tuple.LArr));
				break;
			case HTupleType.DOUBLE:
				HCkH(SetDArr(tupleHandle, tuple.DArr));
				break;
			case HTupleType.STRING:
			{
				string[] sArr = tuple.SArr;
				for (int k = 0; k < tuple.Length; k++)
				{
					HCkH(SetS(tupleHandle, k, sArr[k], force_utf8: true));
				}
				break;
			}
			case HTupleType.HANDLE:
			{
				HHandle[] hArr = tuple.HArr;
				for (int j = 0; j < tuple.Length; j++)
				{
					HCkH(SetH(tupleHandle, j, hArr[j]));
				}
				break;
			}
			case HTupleType.MIXED:
			{
				object[] oArr = tuple.data.OArr;
				for (int i = 0; i < tuple.Length; i++)
				{
					switch (HTupleImplementation.GetObjectType(oArr[i]))
					{
					case 1:
						HCkH(SetI(tupleHandle, i, (int)oArr[i]));
						break;
					case 129:
						HCkH(SetL(tupleHandle, i, (long)oArr[i]));
						break;
					case 2:
						HCkH(SetD(tupleHandle, i, (double)oArr[i]));
						break;
					case 4:
						HCkH(SetS(tupleHandle, i, (string)oArr[i], force_utf8: true));
						break;
					case 16:
						HCkH(SetH(tupleHandle, i, (HHandle)oArr[i]));
						break;
					}
				}
				break;
			}
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static HTuple LoadTuple(IntPtr tupleHandle)
		{
			HTupleImplementation.LoadData(tupleHandle, HTupleType.MIXED, out HTupleImplementation data, force_utf8: true);
			return new HTuple(data);
		}

		private static void HCkH(int err)
		{
			if (IsFailure(err))
			{
				throw new HOperatorException(err);
			}
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetInputTuple")]
		internal static extern int GetInputTuple(IntPtr proc, int parIndex, out IntPtr tuple);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLICreateElementsOfType")]
		internal static extern int CreateElementsOfType(IntPtr tuple, int length, HTupleType type);

		internal static int CreateInputTuple(IntPtr proc, int parIndex, int length, HTupleType type, out IntPtr tuple)
		{
			int inputTuple = GetInputTuple(proc, parIndex, out tuple);
			if (!IsFailure(inputTuple))
			{
				return CreateElementsOfType(tuple, length, type);
			}
			return inputTuple;
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetOutputTuple")]
		internal static extern int GetOutputTuple(IntPtr proc, int parIndex, bool handleType, out IntPtr tuple);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetTupleLength")]
		internal static extern int GetTupleLength(IntPtr tuple, out int length);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetTupleTypeScanElem")]
		internal static extern int GetTupleTypeScanElem(IntPtr tuple, out int type);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetElementType")]
		internal static extern int GetElementType(IntPtr tuple, int index, out HTupleType type);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetI")]
		internal static extern int SetI(IntPtr tuple, int index, int intValue);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetL")]
		internal static extern int SetL(IntPtr tuple, int index, long longValue);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetD")]
		internal static extern int SetD(IntPtr tuple, int index, double doubleValue);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HLISetS(IntPtr tuple, int index, IntPtr stringValue);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetH")]
		internal static extern int SetH(IntPtr tuple, int index, IntPtr handleValue);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLICopyHandle(IntPtr handle, out IntPtr handleCopy);

		internal static IntPtr CopyHandle(IntPtr handle)
		{
			HCkH(HLICopyHandle(handle, out IntPtr handleCopy));
			return handleCopy;
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIClearHandle")]
		internal static extern int ClearHandle(IntPtr handle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIAcquireExternalOwnership(IntPtr handle, out IntPtr handleLong);

		internal static IntPtr AcquireExternalOwnership(IntPtr handle)
		{
			HCkH(HLIAcquireExternalOwnership(handle, out IntPtr handleLong));
			return handleLong;
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIReleaseExternalOwnership")]
		internal static extern int ReleaseExternalOwnership(IntPtr handle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIHandleToHlong(IntPtr handle, out IntPtr handleLong);

		internal static IntPtr HandleToHlong(IntPtr handle)
		{
			HCkH(HLIHandleToHlong(handle, out IntPtr handleLong));
			return handleLong;
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIHandleIsValid(IntPtr handle, out bool is_valid);

		internal static bool HandleIsValid(IntPtr handle)
		{
			HCkH(HLIHandleIsValid(handle, out bool is_valid));
			return is_valid;
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIHGetGV_LegacyHandleMode(out bool is_valid);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool IsLegacyHandleMode()
		{
			HCkH(HLIHGetGV_LegacyHandleMode(out bool is_valid));
			return is_valid;
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIGetHandleSemType(IntPtr handle, out IntPtr sem_type);

		internal static string GetHandleSemType(IntPtr handle)
		{
			HCkH(HLIGetHandleSemType(handle, out IntPtr sem_type));
			return FromHalconEncoding(sem_type, force_utf8: false);
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLITestEqualHandle")]
		internal static extern bool TestEqualHandle(IntPtr handle1, IntPtr handle);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IntPtr ToHalconHGlobalEncoding(string dotnet, bool force_utf8)
		{
			if (!force_utf8 && !IsUTF8Encoding())
			{
				return Marshal.StringToHGlobalAnsi(dotnet);
			}
			return ToHGlobalUtf8Encoding(dotnet);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IntPtr ToHGlobalUtf8Encoding(string dotnet)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(dotnet);
			int num = Marshal.SizeOf(bytes.GetType().GetElementType()) * bytes.Length;
			IntPtr intPtr = Marshal.AllocHGlobal(num + 1);
			Marshal.Copy(bytes, 0, intPtr, bytes.Length);
			Marshal.WriteByte(intPtr, num, 0);
			return intPtr;
		}

		internal static int SetS(IntPtr tuple, int index, string dotnet_string, bool force_utf8)
		{
			IntPtr intPtr = ToHalconHGlobalEncoding(dotnet_string, force_utf8);
			int result = HLISetS(tuple, index, intPtr);
			Marshal.FreeHGlobal(intPtr);
			return result;
		}

		internal static int SetIP(IntPtr tuple, int index, IntPtr intPtrValue)
		{
			if (isPlatform64)
			{
				return SetL(tuple, index, intPtrValue.ToInt64());
			}
			return SetI(tuple, index, intPtrValue.ToInt32());
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreI(IntPtr proc, int parIndex, int intValue)
		{
			HCkP(proc, CreateInputTuple(proc, parIndex, 1, HTupleType.INTEGER, out IntPtr tuple));
			SetI(tuple, 0, intValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreL(IntPtr proc, int parIndex, long longValue)
		{
			HCkP(proc, CreateInputTuple(proc, parIndex, 1, HTupleType.INTEGER, out IntPtr tuple));
			SetL(tuple, 0, longValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreD(IntPtr proc, int parIndex, double doubleValue)
		{
			HCkP(proc, CreateInputTuple(proc, parIndex, 1, HTupleType.DOUBLE, out IntPtr tuple));
			SetD(tuple, 0, doubleValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreS(IntPtr proc, int parIndex, string stringValue)
		{
			if (stringValue == null)
			{
				stringValue = "";
			}
			HCkP(proc, CreateInputTuple(proc, parIndex, 1, HTupleType.STRING, out IntPtr tuple));
			HCkP(proc, SetS(tuple, 0, stringValue, force_utf8: false));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreH(IntPtr proc, int parIndex, IntPtr handleValue)
		{
			HCkP(proc, CreateInputTuple(proc, parIndex, 1, HTupleType.HANDLE, out IntPtr tuple));
			HCkP(proc, SetH(tuple, 0, handleValue));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void StoreIP(IntPtr proc, int parIndex, IntPtr intPtrValue)
		{
			HCkP(proc, CreateInputTuple(proc, parIndex, 1, HTupleType.INTEGER, out IntPtr tuple));
			SetIP(tuple, 0, intPtrValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void Store(IntPtr proc, int parIndex, HTuple tupleValue)
		{
			if (tupleValue == null)
			{
				tupleValue = new HTuple();
			}
			tupleValue.Store(proc, parIndex);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void Store(IntPtr proc, int parIndex, HHandle handleValue)
		{
			if (handleValue == null)
			{
				handleValue = new HHandle();
			}
			handleValue.Store(proc, parIndex);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void Store(IntPtr proc, int parIndex, HObjectBase objectValue)
		{
			if (objectValue == null)
			{
				objectValue = new HObjectBase();
			}
			objectValue.Store(proc, parIndex);
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetIArr")]
		internal static extern int SetIArr(IntPtr tuple, int[] intArray);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetIArrPtr")]
		internal static extern int SetIArrPtr(IntPtr tuple, int[] intArray, int length);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetLArr")]
		internal static extern int SetLArr(IntPtr tuple, long[] longArray);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetLArrPtr")]
		internal static extern int SetLArrPtr(IntPtr tuple, long[] longArray, int length);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetDArr")]
		internal static extern int SetDArr(IntPtr tuple, double[] doubleArray);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLISetDArrPtr")]
		internal static extern int SetDArrPtr(IntPtr tuple, double[] doubleArray, int length);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetI")]
		internal static extern int GetI(IntPtr tuple, int index, out int intValue);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetL")]
		internal static extern int GetL(IntPtr tuple, int index, out long longValue);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HLIGetH(IntPtr tuple, int index, out IntPtr longValue);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetD")]
		internal static extern int GetD(IntPtr tuple, int index, out double doubleValue);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		private static extern int HLIGetS(IntPtr tuple, int index, out IntPtr stringPtr);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static string FromHalconEncoding(IntPtr halcon, bool force_utf8)
		{
			if (force_utf8 || IsUTF8Encoding())
			{
				int i;
				for (i = 0; Marshal.ReadByte(halcon, i) != 0; i++)
				{
				}
				byte[] array = new byte[i];
				Marshal.Copy(halcon, array, 0, array.Length);
				return Encoding.UTF8.GetString(array);
			}
			return Marshal.PtrToStringAnsi(halcon);
		}

		internal static int GetS(IntPtr tuple, int index, out string stringValue, bool force_utf8)
		{
			stringValue = string.Empty;
			IntPtr stringPtr;
			int num = HLIGetS(tuple, index, out stringPtr);
			if (num != 2)
			{
				return num;
			}
			stringValue = FromHalconEncoding(stringPtr, force_utf8);
			if (stringValue == null)
			{
				stringValue = "";
				return 5;
			}
			return 2;
		}

		internal static int GetH(IntPtr tuple, int index, out HHandle handle)
		{
			IntPtr longValue;
			int result = HLIGetH(tuple, index, out longValue);
			handle = new HHandle(longValue);
			return result;
		}

		internal static int GetIP(IntPtr tuple, int index, out IntPtr intPtrValue)
		{
			int result;
			if (isPlatform64)
			{
				result = GetL(tuple, index, out long longValue);
				intPtrValue = new IntPtr(longValue);
			}
			else
			{
				result = GetI(tuple, index, out int intValue);
				intPtrValue = new IntPtr(intValue);
			}
			return result;
		}

		private static int HCkSingle(IntPtr tuple, HTupleType expectedType)
		{
			int length = 0;
			if (tuple != IntPtr.Zero)
			{
				GetTupleLength(tuple, out length);
			}
			if (length > 0)
			{
				GetElementType(tuple, 0, out HTupleType type);
				if (type != expectedType)
				{
					return 7002;
				}
				return 2;
			}
			return 7001;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadI(IntPtr proc, int parIndex, int err, out int intValue)
		{
			if (IsFailure(err))
			{
				intValue = -1;
				return err;
			}
			IntPtr tuple = IntPtr.Zero;
			GetOutputTuple(proc, parIndex,false,out tuple);
			err = HCkSingle(tuple, HTupleType.INTEGER);
			if (err != 2)
			{
				err = HCkSingle(tuple, HTupleType.DOUBLE);
				if (err != 2)
				{
					intValue = -1;
					return err;
				}
				double doubleValue = -1.0;
				err = GetD(tuple, 0, out doubleValue);
				intValue = (int)doubleValue;
				return err;
			}
			return GetI(tuple, 0, out intValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadL(IntPtr proc, int parIndex, int err, out long longValue)
		{
			if (IsFailure(err))
			{
				longValue = -1L;
				return err;
			}
			IntPtr tuple = IntPtr.Zero;
			GetOutputTuple(proc, parIndex, false, out tuple);
			err = HCkSingle(tuple, HTupleType.INTEGER);
			if (err != 2)
			{
				err = HCkSingle(tuple, HTupleType.DOUBLE);
				if (err != 2)
				{
					longValue = -1L;
					return err;
				}
				double doubleValue = -1.0;
				err = GetD(tuple, 0, out doubleValue);
				longValue = (long)doubleValue;
				return err;
			}
			return GetL(tuple, 0, out longValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadD(IntPtr proc, int parIndex, int err, out double doubleValue)
		{
			if (IsFailure(err))
			{
				doubleValue = -1.0;
				return err;
			}
			IntPtr tuple = IntPtr.Zero;
			GetOutputTuple(proc, parIndex, false, out tuple);
			err = HCkSingle(tuple, HTupleType.DOUBLE);
			if (err != 2)
			{
				err = HCkSingle(tuple, HTupleType.INTEGER);
				if (err != 2)
				{
					doubleValue = -1.0;
					return err;
				}
				int intValue = -1;
				err = GetI(tuple, 0, out intValue);
				doubleValue = intValue;
				return err;
			}
			return GetD(tuple, 0, out doubleValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadS(IntPtr proc, int parIndex, int err, out string stringValue)
		{
			if (IsFailure(err))
			{
				stringValue = "";
				return err;
			}
			IntPtr tuple = IntPtr.Zero;
			GetOutputTuple(proc, parIndex,false, out tuple);
			err = HCkSingle(tuple, HTupleType.STRING);
			if (err != 2)
			{
				stringValue = "";
				return err;
			}
			return GetS(tuple, 0, out stringValue, force_utf8: false);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadIP(IntPtr proc, int parIndex, int err, out IntPtr intPtrValue)
		{
			if (IsFailure(err))
			{
				intPtrValue = IntPtr.Zero;
				return err;
			}
			GetOutputTuple(proc, parIndex, false, out IntPtr tuple);
			err = HCkSingle(tuple, HTupleType.INTEGER);
			if (err != 2)
			{
				intPtrValue = IntPtr.Zero;
				return err;
			}
			return GetIP(tuple, 0, out intPtrValue);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadH(IntPtr proc, int parIndex, int err, out HHandle handleValue)
		{
			if (IsFailure(err))
			{
				handleValue = new HHandle();
				return err;
			}
			GetOutputTuple(proc, parIndex,true, out IntPtr tuple);
			err = HCkSingle(tuple, HTupleType.HANDLE);
			if (err != 2)
			{
				handleValue = new HHandle();
				return err;
			}
			return GetH(tuple, 0, out handleValue);
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetIArr")]
		internal static extern int GetIArr(IntPtr tuple, [Out] int[] intArray);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetLArr")]
		internal static extern int GetLArr(IntPtr tuple, [Out] long[] longArray);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl, EntryPoint = "HLIGetDArr")]
		internal static extern int GetDArr(IntPtr tuple, [Out] double[] doubleArray);

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void UnpinTuple(HTuple tuple)
		{
			tuple?.UnpinTuple();
		}

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackPush(IntPtr win_handle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackPop();

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackGetActive(out IntPtr win_handle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackSetActive(IntPtr win_handle);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackIsOpen(out bool is_open);

		[DllImport("halcon", CallingConvention = CallingConvention.Cdecl)]
		internal static extern int HWindowStackCloseAll();

		internal static bool IsError(int err)
		{
			return err >= 1000;
		}

		internal static bool IsFailure(int err)
		{
			if (err != 2)
			{
				return err != 2;
			}
			return false;
		}

		internal static void HCkP(IntPtr proc, int err)
		{
			if (IsFailure(err))
			{
				PostCall(proc, err);
			}
		}
	}
}
