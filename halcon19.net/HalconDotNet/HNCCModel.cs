using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HNCCModel : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HNCCModel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HNCCModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HNCCModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("ncc_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HNCCModel obj)
		{
			obj = new HNCCModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HNCCModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HNCCModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HNCCModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HNCCModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(985);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HNCCModel(HImage template, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(993);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, metric);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HNCCModel(HImage template, int numLevels, double angleStart, double angleExtent, double angleStep, string metric)
		{
			IntPtr proc = HalconAPI.PreCall(993);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, metric);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeNccModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HNCCModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeNccModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeNccModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HNCCModel Deserialize(Stream stream)
		{
			HNCCModel hNCCModel = new HNCCModel();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hNCCModel.DeserializeNccModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hNCCModel;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HNCCModel Clone()
		{
			HSerializedItem hSerializedItem = SerializeNccModel();
			HNCCModel hNCCModel = new HNCCModel();
			hNCCModel.DeserializeNccModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hNCCModel;
		}

		public void ClearNccModel()
		{
			IntPtr proc = HalconAPI.PreCall(982);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeNccModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(983);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeNccModel()
		{
			IntPtr proc = HalconAPI.PreCall(984);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadNccModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(985);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteNccModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(986);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static HTuple DetermineNccModelParams(HImage template, HTuple numLevels, double angleStart, double angleExtent, string metric, HTuple parameters, out HTuple parameterValue)
		{
			IntPtr proc = HalconAPI.PreCall(987);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreS(proc, 3, metric);
			HalconAPI.Store(proc, 4, parameters);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(parameters);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out parameterValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(template);
			return tuple;
		}

		public static HTuple DetermineNccModelParams(HImage template, int numLevels, double angleStart, double angleExtent, string metric, string parameters, out HTuple parameterValue)
		{
			IntPtr proc = HalconAPI.PreCall(987);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreS(proc, 3, metric);
			HalconAPI.StoreS(proc, 4, parameters);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out parameterValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(template);
			return tuple;
		}

		public int GetNccModelParams(out double angleStart, out double angleExtent, out double angleStep, out string metric)
		{
			IntPtr proc = HalconAPI.PreCall(988);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadD(proc, 1, err, out angleStart);
			err = HalconAPI.LoadD(proc, 2, err, out angleExtent);
			err = HalconAPI.LoadD(proc, 3, err, out angleStep);
			err = HalconAPI.LoadS(proc, 4, err, out metric);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public void GetNccModelOrigin(out double row, out double column)
		{
			IntPtr proc = HalconAPI.PreCall(989);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SetNccModelOrigin(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(990);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void FindNccModel(HImage image, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, string subPixel, HTuple numLevels, out HTuple row, out HTuple column, out HTuple angle, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(991);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, subPixel);
			HalconAPI.Store(proc, 7, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void FindNccModel(HImage image, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, out HTuple row, out HTuple column, out HTuple angle, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(991);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, subPixel);
			HalconAPI.StoreI(proc, 7, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void SetNccModelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(992);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateNccModel(HImage template, HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, string metric)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(993);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, metric);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreateNccModel(HImage template, int numLevels, double angleStart, double angleExtent, double angleStep, string metric)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(993);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, metric);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public static void FindNccModels(HImage image, HNCCModel[] modelIDs, HTuple angleStart, HTuple angleExtent, HTuple minScore, HTuple numMatches, HTuple maxOverlap, HTuple subPixel, HTuple numLevels, out HTuple row, out HTuple column, out HTuple angle, out HTuple score, out HTuple model)
		{
			HTuple hTuple = HHandleBase.ConcatArray(modelIDs);
			IntPtr proc = HalconAPI.PreCall(2068);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, numMatches);
			HalconAPI.Store(proc, 5, maxOverlap);
			HalconAPI.Store(proc, 6, subPixel);
			HalconAPI.Store(proc, 7, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(numMatches);
			HalconAPI.UnpinTuple(maxOverlap);
			HalconAPI.UnpinTuple(subPixel);
			HalconAPI.UnpinTuple(numLevels);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(image);
			GC.KeepAlive(modelIDs);
		}

		public void FindNccModels(HImage image, double angleStart, double angleExtent, double minScore, int numMatches, double maxOverlap, string subPixel, int numLevels, out HTuple row, out HTuple column, out HTuple angle, out HTuple score, out HTuple model)
		{
			IntPtr proc = HalconAPI.PreCall(2068);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreI(proc, 4, numMatches);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, subPixel);
			HalconAPI.StoreI(proc, 7, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out score);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out model);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HRegion GetNccModelRegion()
		{
			IntPtr proc = HalconAPI.PreCall(2071);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}
	}
}
