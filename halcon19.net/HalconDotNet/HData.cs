using System;

namespace HalconDotNet
{
	public class HData
	{
		internal HTuple tuple;

		public HTuple RawData
		{
			get
			{
				return tuple;
			}
			set
			{
				tuple = new HTuple(value);
			}
		}

		public HTupleElements this[int index]
		{
			get
			{
				return tuple[index];
			}
			set
			{
				tuple[index] = value;
			}
		}

		internal HData()
		{
			tuple = new HTuple();
		}

		internal HData(HTuple t)
		{
			tuple = t;
		}

		internal HData(HData data)
		{
			tuple = data.tuple;
		}

		internal static HTuple ConcatArray(HData[] data)
		{
			HTuple hTuple = new HTuple();
			for (int i = 0; i < data.Length; i++)
			{
				hTuple = hTuple.TupleConcat(data[i].tuple);
			}
			return hTuple;
		}

		internal void UnpinTuple()
		{
			tuple.UnpinTuple();
		}

		internal void Store(IntPtr proc, int parIndex)
		{
			tuple.Store(proc, parIndex);
		}

		internal int Load(IntPtr proc, int parIndex, int err)
		{
			return tuple.Load(proc, parIndex, err);
		}

		internal int Load(IntPtr proc, int parIndex, HTupleType type, int err)
		{
			return tuple.Load(proc, parIndex, type, err);
		}

		public static implicit operator HTuple(HData data)
		{
			return data.tuple;
		}
	}
}
