using System;

namespace HalconDotNet
{
	public class HTupleVector : HVector
	{
		private HTuple mTuple;

		public HTuple T
		{
			get
			{
				AssertDimension(0);
				return mTuple;
			}
			set
			{
				AssertDimension(0);
				if (value == null)
				{
					throw new HVectorAccessException("Null tuple not allowed in vector");
				}
				mTuple.Dispose();
				mTuple = new HTuple(value);
			}
		}

		public new HTupleVector this[int index]
		{
			get
			{
				return (HTupleVector)base[index];
			}
			set
			{
				base[index] = value;
			}
		}

		public HTupleVector(int dimension)
			: base(dimension)
		{
			mTuple = ((dimension <= 0) ? new HTuple() : null);
		}

		public HTupleVector(HTuple tuple)
			: base(0)
		{
			if (tuple == null)
			{
				throw new HVectorAccessException("Null tuple not allowed in vector");
			}
			mTuple = new HTuple(tuple);
		}

		public HTupleVector(HTuple tuple, int blockSize)
			: base(1)
		{
			if (blockSize <= 0)
			{
				throw new HVectorAccessException("Invalid block size in vector constructor");
			}
			for (int i = 0; (double)i < Math.Ceiling((double)tuple.Length / (double)blockSize); i++)
			{
				int i2 = i * blockSize;
				int i3 = Math.Min((i + 1) * blockSize, tuple.Length) - 1;
				this[i] = new HTupleVector(tuple.TupleSelectRange(i2, i3));
			}
		}

		public HTupleVector(HTupleVector vector)
			: base(vector)
		{
			if (mDimension <= 0)
			{
				mTuple = new HTuple(vector.mTuple);
			}
		}

		protected override HVector GetDefaultElement()
		{
			return new HTupleVector(mDimension - 1);
		}

		public new HTupleVector At(int index)
		{
			return (HTupleVector)base.At(index);
		}

		protected override bool EqualsImpl(HVector vector)
		{
			if (mDimension >= 1)
			{
				return base.EqualsImpl(vector);
			}
			return ((HTupleVector)vector).T.TupleEqual(T);
		}

		public bool VectorEqual(HTupleVector vector)
		{
			return EqualsImpl(vector);
		}

		public HTupleVector Concat(HTupleVector vector)
		{
			return (HTupleVector)ConcatImpl(vector, append: false, clone: true);
		}

		public HTupleVector Append(HTupleVector vector)
		{
			return (HTupleVector)ConcatImpl(vector, append: true, clone: true);
		}

		public HTupleVector Insert(int index, HTupleVector vector)
		{
			InsertImpl(index, vector, clone: true);
			return this;
		}

		public new HTupleVector Remove(int index)
		{
			RemoveImpl(index);
			return this;
		}

		public new HTupleVector Clear()
		{
			ClearImpl();
			return this;
		}

		public new HTupleVector Clone()
		{
			return (HTupleVector)CloneImpl();
		}

		protected override HVector CloneImpl()
		{
			return new HTupleVector(this);
		}

		protected override void DisposeLeafObject()
		{
			if (mDimension <= 0)
			{
				mTuple.Dispose();
			}
		}

		public HTuple ConvertVectorToTuple()
		{
			if (mDimension > 0)
			{
				HTuple hTuple = new HTuple();
				for (int i = 0; i < base.Length; i++)
				{
					hTuple.Append(this[i].ConvertVectorToTuple());
				}
				return hTuple;
			}
			return mTuple;
		}

		public override string ToString()
		{
			if (mDimension <= 0)
			{
				return mTuple.ToString();
			}
			return base.ToString();
		}
	}
}
