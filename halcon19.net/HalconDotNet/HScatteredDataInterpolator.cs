using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HScatteredDataInterpolator : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HScatteredDataInterpolator()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HScatteredDataInterpolator(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HScatteredDataInterpolator(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("scattered_data_interpolator");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HScatteredDataInterpolator obj)
		{
			obj = new HScatteredDataInterpolator(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HScatteredDataInterpolator[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HScatteredDataInterpolator[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HScatteredDataInterpolator(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HScatteredDataInterpolator(string method, HTuple rows, HTuple columns, HTuple values, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(292);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.Store(proc, 1, rows);
			HalconAPI.Store(proc, 2, columns);
			HalconAPI.Store(proc, 3, values);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(columns);
			HalconAPI.UnpinTuple(values);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static void ClearScatteredDataInterpolator(HScatteredDataInterpolator[] scatteredDataInterpolatorHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(scatteredDataInterpolatorHandle);
			IntPtr proc = HalconAPI.PreCall(290);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(scatteredDataInterpolatorHandle);
		}

		public void ClearScatteredDataInterpolator()
		{
			IntPtr proc = HalconAPI.PreCall(290);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple InterpolateScatteredData(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(291);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double InterpolateScatteredData(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(291);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public void CreateScatteredDataInterpolator(string method, HTuple rows, HTuple columns, HTuple values, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(292);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.Store(proc, 1, rows);
			HalconAPI.Store(proc, 2, columns);
			HalconAPI.Store(proc, 3, values);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(columns);
			HalconAPI.UnpinTuple(values);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
