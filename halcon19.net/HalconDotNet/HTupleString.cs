using System;

namespace HalconDotNet
{
	internal class HTupleString : HTupleImplementation
	{
		protected string[] s;

		public override string[] SArr
		{
			get
			{
				return s;
			}
			set
			{
				SetArray(value, copy: false);
			}
		}

		public override HTupleType Type => HTupleType.STRING;

		protected override Array CreateArray(int size)
		{
			string[] array = new string[size];
			for (int i = 0; i < size; i++)
			{
				array[i] = "";
			}
			return array;
		}

		protected override void NotifyArrayUpdate()
		{
			s = (string[])data;
		}

		public HTupleString(string s)
		{
			SetArray(new string[1]
			{
				s
			}, copy: false);
		}

		public HTupleString(string[] s, bool copy)
		{
			SetArray(s, copy);
		}

		public override HTupleElements GetElement(int index, HTuple parent)
		{
			return new HTupleElements(parent, this, index);
		}

		public override HTupleElements GetElements(int[] indices, HTuple parent)
		{
			if (indices == null || indices.Length == 0)
			{
				return new HTupleElements();
			}
			return new HTupleElements(parent, this, indices);
		}

		public override void SetElements(int[] indices, HTupleElements elements)
		{
			HTupleElementsString hTupleElementsString = new HTupleElementsString(this, indices);
			hTupleElementsString.setS(elements.SArr);
		}

		public override string[] ToSArr()
		{
			return (string[])ToArray(typeS);
		}

		public override int CopyToSArr(string[] dst, int offset)
		{
			Array.Copy(s, 0, dst, offset, iLength);
			return iLength;
		}

		public override int CopyToOArr(object[] dst, int offset)
		{
			for (int i = 0; i < iLength; i++)
			{
				dst[i + offset] = s[i];
			}
			return iLength;
		}

		public override int CopyFrom(HTupleImplementation impl, int offset)
		{
			return impl.CopyToSArr(s, offset);
		}

		protected override void StoreData(IntPtr proc, IntPtr tuple)
		{
			for (int i = 0; i < iLength; i++)
			{
				HalconAPI.HCkP(proc, HalconAPI.SetS(tuple, i, s[i], force_utf8: false));
			}
		}

		public static int Load(IntPtr tuple, out HTupleString data, bool force_utf8)
		{
			int num = 2;
			HalconAPI.GetTupleLength(tuple, out int length);
			string[] array = new string[length];
			for (int i = 0; i < length; i++)
			{
				if (!HalconAPI.IsFailure(num))
				{
					num = HalconAPI.GetS(tuple, i, out array[i], force_utf8);
				}
			}
			data = new HTupleString(array, copy: false);
			return num;
		}
	}
}
