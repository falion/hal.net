using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace HalconDotNet
{
	internal class HalconWindowLayoutDialog : Form
	{
		private RadioButton radioSizeFull;

		private Button buttonOK;

		private Button buttonCancel;

		private Label labelSize;

		private RadioButton radioSizeHalf;

		private RadioButton radioSizeQuarter;

		private Label labelReference;

		private Label labelInfo;

		private Container components;

		public bool resultCancel = true;

		public int resultPercent = 100;

		public HalconWindowLayoutDialog()
		{
			InitializeComponent();
		}

		public HalconWindowLayoutDialog(Size referenceSize)
			: this()
		{
			labelReference.Text = labelReference.Text + " " + referenceSize.Width + " x " + referenceSize.Height;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			labelReference = new System.Windows.Forms.Label();
			radioSizeFull = new System.Windows.Forms.RadioButton();
			buttonOK = new System.Windows.Forms.Button();
			buttonCancel = new System.Windows.Forms.Button();
			labelSize = new System.Windows.Forms.Label();
			labelInfo = new System.Windows.Forms.Label();
			radioSizeHalf = new System.Windows.Forms.RadioButton();
			radioSizeQuarter = new System.Windows.Forms.RadioButton();
			SuspendLayout();
			labelReference.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
			labelReference.Location = new System.Drawing.Point(8, 8);
			labelReference.Name = "labelReference";
			labelReference.Size = new System.Drawing.Size(216, 16);
			labelReference.TabIndex = 1;
			labelReference.Text = "Reference Image Size:";
			radioSizeFull.Checked = true;
			radioSizeFull.Location = new System.Drawing.Point(16, 56);
			radioSizeFull.Name = "radioSizeFull";
			radioSizeFull.Size = new System.Drawing.Size(56, 16);
			radioSizeFull.TabIndex = 2;
			radioSizeFull.TabStop = true;
			radioSizeFull.Text = "100%";
			buttonOK.Location = new System.Drawing.Point(8, 116);
			buttonOK.Name = "buttonOK";
			buttonOK.Size = new System.Drawing.Size(104, 24);
			buttonOK.TabIndex = 3;
			buttonOK.Text = "OK";
			buttonOK.Click += new System.EventHandler(buttonOK_Click);
			buttonCancel.Location = new System.Drawing.Point(120, 116);
			buttonCancel.Name = "buttonCancel";
			buttonCancel.Size = new System.Drawing.Size(104, 24);
			buttonCancel.TabIndex = 4;
			buttonCancel.Text = "Cancel";
			buttonCancel.Click += new System.EventHandler(buttonCancel_Click);
			labelSize.Location = new System.Drawing.Point(8, 32);
			labelSize.Name = "labelSize";
			labelSize.Size = new System.Drawing.Size(216, 16);
			labelSize.TabIndex = 5;
			labelSize.Text = "Choose a Window Size:";
			labelInfo.Location = new System.Drawing.Point(8, 80);
			labelInfo.Name = "labelInfo";
			labelInfo.Size = new System.Drawing.Size(216, 28);
			labelInfo.TabIndex = 6;
			labelInfo.Text = "The default ImagePart will be adapted to make the entire image fit into this window.";
			radioSizeHalf.Location = new System.Drawing.Point(84, 56);
			radioSizeHalf.Name = "radioSizeHalf";
			radioSizeHalf.Size = new System.Drawing.Size(56, 16);
			radioSizeHalf.TabIndex = 7;
			radioSizeHalf.Text = "50%";
			radioSizeQuarter.Location = new System.Drawing.Point(152, 56);
			radioSizeQuarter.Name = "radioSizeQuarter";
			radioSizeQuarter.Size = new System.Drawing.Size(56, 16);
			radioSizeQuarter.TabIndex = 8;
			radioSizeQuarter.Text = "25%";
			AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			base.ClientSize = new System.Drawing.Size(230, 147);
			base.Controls.AddRange(new System.Windows.Forms.Control[8]
			{
				radioSizeQuarter,
				radioSizeHalf,
				labelInfo,
				labelSize,
				buttonCancel,
				buttonOK,
				radioSizeFull,
				labelReference
			});
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			base.Name = "HalconWindowLayoutDialog";
			Text = "HALCON Window Layout";
			ResumeLayout(false);
		}

		private void buttonOK_Click(object sender, EventArgs e)
		{
			if (radioSizeHalf.Checked)
			{
				resultPercent = 50;
			}
			else if (radioSizeQuarter.Checked)
			{
				resultPercent = 25;
			}
			resultCancel = false;
			Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
