namespace HalconDotNet
{
	internal class HDevInputParamObject : HDevInputParam
	{
		protected HObject mObject;

		public HDevInputParamObject(HObject obj)
		{
			mObject = obj.CopyObj(1, -1);
		}

		public override HObject GetIconicParamObject()
		{
			return mObject;
		}

		public override void Dispose()
		{
			if (mObject != null)
			{
				mObject.Dispose();
				mObject = null;
			}
		}
	}
}
