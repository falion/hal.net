namespace HalconDotNet
{
	internal class HDevInputParam
	{
		private static HalconException NI()
		{
			return new HalconException("Unexpected parameter type in exported parallelization code");
		}

		public virtual HObject GetIconicParamObject()
		{
			throw NI();
		}

		public virtual HObjectVector GetIconicParamVector()
		{
			throw NI();
		}

		public virtual HTuple GetCtrlParamTuple()
		{
			throw NI();
		}

		public virtual HTupleVector GetCtrlParamVector()
		{
			throw NI();
		}

		public virtual void Dispose()
		{
		}
	}
}
