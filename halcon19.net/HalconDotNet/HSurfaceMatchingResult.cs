using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HSurfaceMatchingResult : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSurfaceMatchingResult()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSurfaceMatchingResult(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSurfaceMatchingResult(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("surface_matching_result");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSurfaceMatchingResult obj)
		{
			obj = new HSurfaceMatchingResult(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSurfaceMatchingResult[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HSurfaceMatchingResult[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HSurfaceMatchingResult(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HTuple GetSurfaceMatchingResult(HTuple resultName, int resultIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1032);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, resultName);
			HalconAPI.StoreI(proc, 2, resultIndex);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetSurfaceMatchingResult(string resultName, int resultIndex)
		{
			IntPtr proc = HalconAPI.PreCall(1032);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, resultName);
			HalconAPI.StoreI(proc, 2, resultIndex);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public static void ClearSurfaceMatchingResult(HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			HTuple hTuple = HHandleBase.ConcatArray(surfaceMatchingResultID);
			IntPtr proc = HalconAPI.PreCall(1034);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(surfaceMatchingResultID);
		}

		public void ClearSurfaceMatchingResult()
		{
			IntPtr proc = HalconAPI.PreCall(1034);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static HPose[] RefineSurfaceModelPose(HSurfaceModel surfaceModelID, HObjectModel3D objectModel3D, HPose[] initialPose, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			HTuple hTuple = HData.ConcatArray(initialPose);
			IntPtr proc = HalconAPI.PreCall(1041);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 2, hTuple);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(tuple);
			GC.KeepAlive(surfaceModelID);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose RefineSurfaceModelPose(HSurfaceModel surfaceModelID, HObjectModel3D objectModel3D, HPose initialPose, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1041);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 2, initialPose);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(initialPose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 2, err);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(surfaceModelID);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public static HPose[] FindSurfaceModel(HSurfaceModel surfaceModelID, HObjectModel3D objectModel3D, double relSamplingDistance, double keyPointFraction, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(1042);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.Store(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(tuple);
			GC.KeepAlive(surfaceModelID);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose FindSurfaceModel(HSurfaceModel surfaceModelID, HObjectModel3D objectModel3D, double relSamplingDistance, double keyPointFraction, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1042);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.StoreD(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 2, err);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(surfaceModelID);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public static HPose[] FindSurfaceModelImage(HImage image, HSurfaceModel surfaceModelID, HObjectModel3D objectModel3D, double relSamplingDistance, double keyPointFraction, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			IntPtr proc = HalconAPI.PreCall(2069);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.Store(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(tuple);
			GC.KeepAlive(image);
			GC.KeepAlive(surfaceModelID);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose FindSurfaceModelImage(HImage image, HSurfaceModel surfaceModelID, HObjectModel3D objectModel3D, double relSamplingDistance, double keyPointFraction, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2069);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, keyPointFraction);
			HalconAPI.StoreD(proc, 4, minScore);
			HalconAPI.StoreS(proc, 5, returnResultHandle);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 2, err);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(surfaceModelID);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public static HPose[] RefineSurfaceModelPoseImage(HImage image, HSurfaceModel surfaceModelID, HObjectModel3D objectModel3D, HPose[] initialPose, HTuple minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score, out HSurfaceMatchingResult[] surfaceMatchingResultID)
		{
			HTuple hTuple = HData.ConcatArray(initialPose);
			IntPtr proc = HalconAPI.PreCall(2084);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 2, hTuple);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			err = LoadNew(proc, 2, err, out surfaceMatchingResultID);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(tuple);
			GC.KeepAlive(image);
			GC.KeepAlive(surfaceModelID);
			GC.KeepAlive(objectModel3D);
			return result;
		}

		public HPose RefineSurfaceModelPoseImage(HImage image, HSurfaceModel surfaceModelID, HObjectModel3D objectModel3D, HPose initialPose, double minScore, string returnResultHandle, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2084);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, surfaceModelID);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 2, initialPose);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.StoreS(proc, 4, returnResultHandle);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(initialPose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 2, err);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(surfaceModelID);
			GC.KeepAlive(objectModel3D);
			return obj;
		}
	}
}
