using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HTemplate : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTemplate()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTemplate(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTemplate(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("template");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTemplate obj)
		{
			obj = new HTemplate(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTemplate[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HTemplate[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HTemplate(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HTemplate(HImage template, int numLevel, double angleStart, double angleExtend, double angleStep, string optimize, string grayValues)
		{
			IntPtr proc = HalconAPI.PreCall(1488);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevel);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, optimize);
			HalconAPI.StoreS(proc, 5, grayValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HTemplate(HImage template, int firstError, int numLevel, string optimize, string grayValues)
		{
			IntPtr proc = HalconAPI.PreCall(1489);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, firstError);
			HalconAPI.StoreI(proc, 1, numLevel);
			HalconAPI.StoreS(proc, 2, optimize);
			HalconAPI.StoreS(proc, 3, grayValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HTemplate(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1493);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeTemplate();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTemplate(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeTemplate(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeTemplate();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HTemplate Deserialize(Stream stream)
		{
			HTemplate hTemplate = new HTemplate();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hTemplate.DeserializeTemplate(hSerializedItem);
			hSerializedItem.Dispose();
			return hTemplate;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HTemplate Clone()
		{
			HSerializedItem hSerializedItem = SerializeTemplate();
			HTemplate hTemplate = new HTemplate();
			hTemplate.DeserializeTemplate(hSerializedItem);
			hSerializedItem.Dispose();
			return hTemplate;
		}

		public void CreateTemplateRot(HImage template, int numLevel, double angleStart, double angleExtend, double angleStep, string optimize, string grayValues)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1488);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, numLevel);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, optimize);
			HalconAPI.StoreS(proc, 5, grayValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreateTemplate(HImage template, int firstError, int numLevel, string optimize, string grayValues)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1489);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreI(proc, 0, firstError);
			HalconAPI.StoreI(proc, 1, numLevel);
			HalconAPI.StoreS(proc, 2, optimize);
			HalconAPI.StoreS(proc, 3, grayValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HSerializedItem SerializeTemplate()
		{
			IntPtr proc = HalconAPI.PreCall(1490);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void DeserializeTemplate(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1491);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public void WriteTemplate(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1492);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadTemplate(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1493);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ClearTemplate()
		{
			IntPtr proc = HalconAPI.PreCall(1495);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetOffsetTemplate(int grayOffset)
		{
			IntPtr proc = HalconAPI.PreCall(1496);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, grayOffset);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetReferenceTemplate(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(1497);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AdaptTemplate(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(1498);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HRegion FastMatchMg(HImage image, double maxError, HTuple numLevel)
		{
			IntPtr proc = HalconAPI.PreCall(1499);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.Store(proc, 2, numLevel);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevel);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HRegion FastMatchMg(HImage image, double maxError, int numLevel)
		{
			IntPtr proc = HalconAPI.PreCall(1499);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreI(proc, 2, numLevel);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void BestMatchPreMg(HImage imagePyramid, double maxError, string subPixel, int numLevels, HTuple whichLevels, out double row, out double column, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1500);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, imagePyramid);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.StoreI(proc, 3, numLevels);
			HalconAPI.Store(proc, 4, whichLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(whichLevels);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imagePyramid);
		}

		public void BestMatchPreMg(HImage imagePyramid, double maxError, string subPixel, int numLevels, int whichLevels, out double row, out double column, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1500);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, imagePyramid);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.StoreI(proc, 3, numLevels);
			HalconAPI.StoreI(proc, 4, whichLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imagePyramid);
		}

		public void BestMatchMg(HImage image, double maxError, string subPixel, int numLevels, HTuple whichLevels, out double row, out double column, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1501);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.StoreI(proc, 3, numLevels);
			HalconAPI.Store(proc, 4, whichLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(whichLevels);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void BestMatchMg(HImage image, double maxError, string subPixel, int numLevels, int whichLevels, out double row, out double column, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1501);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.StoreI(proc, 3, numLevels);
			HalconAPI.StoreI(proc, 4, whichLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HRegion FastMatch(HImage image, double maxError)
		{
			IntPtr proc = HalconAPI.PreCall(1502);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void BestMatchRotMg(HImage image, double angleStart, double angleExtend, double maxError, string subPixel, int numLevels, out HTuple row, out HTuple column, out HTuple angle, out HTuple error)
		{
			IntPtr proc = HalconAPI.PreCall(1503);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, maxError);
			HalconAPI.StoreS(proc, 4, subPixel);
			HalconAPI.StoreI(proc, 5, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void BestMatchRotMg(HImage image, double angleStart, double angleExtend, double maxError, string subPixel, int numLevels, out double row, out double column, out double angle, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1503);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, maxError);
			HalconAPI.StoreS(proc, 4, subPixel);
			HalconAPI.StoreI(proc, 5, numLevels);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out angle);
			err = HalconAPI.LoadD(proc, 3, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void BestMatchRot(HImage image, double angleStart, double angleExtend, double maxError, string subPixel, out HTuple row, out HTuple column, out HTuple angle, out HTuple error)
		{
			IntPtr proc = HalconAPI.PreCall(1504);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, maxError);
			HalconAPI.StoreS(proc, 4, subPixel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out angle);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void BestMatchRot(HImage image, double angleStart, double angleExtend, double maxError, string subPixel, out double row, out double column, out double angle, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1504);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtend);
			HalconAPI.StoreD(proc, 3, maxError);
			HalconAPI.StoreS(proc, 4, subPixel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out angle);
			err = HalconAPI.LoadD(proc, 3, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void BestMatch(HImage image, double maxError, string subPixel, out HTuple row, out HTuple column, out HTuple error)
		{
			IntPtr proc = HalconAPI.PreCall(1505);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void BestMatch(HImage image, double maxError, string subPixel, out double row, out double column, out double error)
		{
			IntPtr proc = HalconAPI.PreCall(1505);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, maxError);
			HalconAPI.StoreS(proc, 2, subPixel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}
	}
}
