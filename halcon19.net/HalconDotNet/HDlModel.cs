using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HDlModel : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlModel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("dl_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlModel obj)
		{
			obj = new HDlModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDlModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HDlModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HDlModel(string backbone, int numClasses, HDict DLModelDetectionParam)
		{
			IntPtr proc = HalconAPI.PreCall(2150);
			HalconAPI.StoreS(proc, 0, backbone);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.Store(proc, 2, DLModelDetectionParam);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(DLModelDetectionParam);
		}

		public HDlModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(2163);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeDlModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeDlModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeDlModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HDlModel Deserialize(Stream stream)
		{
			HDlModel hDlModel = new HDlModel();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hDlModel.DeserializeDlModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDlModel;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HDlModel Clone()
		{
			HSerializedItem hSerializedItem = SerializeDlModel();
			HDlModel hDlModel = new HDlModel();
			hDlModel.DeserializeDlModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDlModel;
		}

		public HDict[] ApplyDlModel(HDict[] DLSampleBatch, HTuple outputs)
		{
			HTuple hTuple = HHandleBase.ConcatArray(DLSampleBatch);
			IntPtr proc = HalconAPI.PreCall(2146);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, hTuple);
			HalconAPI.Store(proc, 2, outputs);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(outputs);
			err = HDict.LoadNew(proc, 0, err, out HDict[] obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(DLSampleBatch);
			return obj;
		}

		public static void ClearDlModel(HDlModel[] DLModelHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(DLModelHandle);
			IntPtr proc = HalconAPI.PreCall(2147);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(DLModelHandle);
		}

		public void ClearDlModel()
		{
			IntPtr proc = HalconAPI.PreCall(2147);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateDlModelDetection(string backbone, int numClasses, HDict DLModelDetectionParam)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2150);
			HalconAPI.StoreS(proc, 0, backbone);
			HalconAPI.StoreI(proc, 1, numClasses);
			HalconAPI.Store(proc, 2, DLModelDetectionParam);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(DLModelDetectionParam);
		}

		public void DeserializeDlModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2151);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HTuple GetDlModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2156);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void ReadDlModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2163);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem SerializeDlModel()
		{
			IntPtr proc = HalconAPI.PreCall(2168);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SetDlModelParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2171);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDlModelParam(string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2171);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HDict TrainDlModelBatch(HDict DLSampleBatch)
		{
			IntPtr proc = HalconAPI.PreCall(2172);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, DLSampleBatch);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HDict.LoadNew(proc, 0, err, out HDict obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(DLSampleBatch);
			return obj;
		}

		public void WriteDlModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(2174);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
