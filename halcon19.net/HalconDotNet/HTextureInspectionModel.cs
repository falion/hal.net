using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HTextureInspectionModel : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionModel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("texture_inspection_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextureInspectionModel obj)
		{
			obj = new HTextureInspectionModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextureInspectionModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HTextureInspectionModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HTextureInspectionModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HTextureInspectionModel(string modelType)
		{
			IntPtr proc = HalconAPI.PreCall(2051);
			HalconAPI.StoreS(proc, 0, modelType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeTextureInspectionModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeTextureInspectionModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeTextureInspectionModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HTextureInspectionModel Deserialize(Stream stream)
		{
			HTextureInspectionModel hTextureInspectionModel = new HTextureInspectionModel();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hTextureInspectionModel.DeserializeTextureInspectionModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hTextureInspectionModel;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HTextureInspectionModel Clone()
		{
			HSerializedItem hSerializedItem = SerializeTextureInspectionModel();
			HTextureInspectionModel hTextureInspectionModel = new HTextureInspectionModel();
			hTextureInspectionModel.DeserializeTextureInspectionModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hTextureInspectionModel;
		}

		public HTuple AddTextureInspectionModelImage(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(2043);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HRegion ApplyTextureInspectionModel(HImage image, out HTextureInspectionResult textureInspectionResultID)
		{
			IntPtr proc = HalconAPI.PreCall(2044);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			err = HTextureInspectionResult.LoadNew(proc, 0, err, out textureInspectionResultID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public static void ClearTextureInspectionModel(HTextureInspectionModel[] textureInspectionModel)
		{
			HTuple hTuple = HHandleBase.ConcatArray(textureInspectionModel);
			IntPtr proc = HalconAPI.PreCall(2047);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(textureInspectionModel);
		}

		public void ClearTextureInspectionModel()
		{
			IntPtr proc = HalconAPI.PreCall(2047);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateTextureInspectionModel(string modelType)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2051);
			HalconAPI.StoreS(proc, 0, modelType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DeserializeTextureInspectionModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2054);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HImage GetTextureInspectionModelImage()
		{
			IntPtr proc = HalconAPI.PreCall(2075);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetTextureInspectionModelParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2076);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetTextureInspectionModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2076);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void ReadTextureInspectionModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2083);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HTuple RemoveTextureInspectionModelImage(HTextureInspectionModel[] textureInspectionModel, HTuple indices)
		{
			HTuple hTuple = HHandleBase.ConcatArray(textureInspectionModel);
			IntPtr proc = HalconAPI.PreCall(2085);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, indices);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(indices);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(textureInspectionModel);
			return tuple;
		}

		public HTuple RemoveTextureInspectionModelImage(HTuple indices)
		{
			IntPtr proc = HalconAPI.PreCall(2085);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, indices);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(indices);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HSerializedItem SerializeTextureInspectionModel()
		{
			IntPtr proc = HalconAPI.PreCall(2094);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SetTextureInspectionModelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2098);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetTextureInspectionModelParam(string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2098);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreI(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TrainTextureInspectionModel()
		{
			IntPtr proc = HalconAPI.PreCall(2099);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteTextureInspectionModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(2100);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
