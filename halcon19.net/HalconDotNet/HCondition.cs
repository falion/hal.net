using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HCondition : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCondition()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCondition(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCondition(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("condition");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCondition obj)
		{
			obj = new HCondition(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCondition[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HCondition[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HCondition(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HCondition(HTuple attribName, HTuple attribValue)
		{
			IntPtr proc = HalconAPI.PreCall(548);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HCondition(string attribName, string attribValue)
		{
			IntPtr proc = HalconAPI.PreCall(548);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ClearCondition()
		{
			IntPtr proc = HalconAPI.PreCall(543);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void BroadcastCondition()
		{
			IntPtr proc = HalconAPI.PreCall(544);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SignalCondition()
		{
			IntPtr proc = HalconAPI.PreCall(545);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TimedWaitCondition(HMutex mutexHandle, int timeout)
		{
			IntPtr proc = HalconAPI.PreCall(546);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, mutexHandle);
			HalconAPI.StoreI(proc, 2, timeout);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(mutexHandle);
		}

		public void WaitCondition(HMutex mutexHandle)
		{
			IntPtr proc = HalconAPI.PreCall(547);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, mutexHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(mutexHandle);
		}

		public void CreateCondition(HTuple attribName, HTuple attribValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(548);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateCondition(string attribName, string attribValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(548);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
