using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HMessage : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMessage(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMessage(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("message");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMessage obj)
		{
			obj = new HMessage(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMessage[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HMessage[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HMessage(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HMessage()
		{
			IntPtr proc = HalconAPI.PreCall(541);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMessage(string fileName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2164);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMessage(string fileName, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2164);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetMessageParam(string genParamName, HTuple key)
		{
			IntPtr proc = HalconAPI.PreCall(534);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetMessageParam(string genParamName, string key)
		{
			IntPtr proc = HalconAPI.PreCall(534);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetMessageParam(string genParamName, HTuple key, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(535);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, key);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMessageParam(string genParamName, string key, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(535);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, key);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HObject GetMessageObj(HTuple key)
		{
			IntPtr proc = HalconAPI.PreCall(536);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, key);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HObject GetMessageObj(string key)
		{
			IntPtr proc = HalconAPI.PreCall(536);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, key);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SetMessageObj(HObject objectData, HTuple key)
		{
			IntPtr proc = HalconAPI.PreCall(537);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectData);
			HalconAPI.Store(proc, 1, key);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectData);
		}

		public void SetMessageObj(HObject objectData, string key)
		{
			IntPtr proc = HalconAPI.PreCall(537);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectData);
			HalconAPI.StoreS(proc, 1, key);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectData);
		}

		public HTuple GetMessageTuple(HTuple key)
		{
			IntPtr proc = HalconAPI.PreCall(538);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetMessageTuple(string key)
		{
			IntPtr proc = HalconAPI.PreCall(538);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetMessageTuple(HTuple key, HTuple tupleData)
		{
			IntPtr proc = HalconAPI.PreCall(539);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, key);
			HalconAPI.Store(proc, 2, tupleData);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			HalconAPI.UnpinTuple(tupleData);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMessageTuple(string key, HTuple tupleData)
		{
			IntPtr proc = HalconAPI.PreCall(539);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, key);
			HalconAPI.Store(proc, 2, tupleData);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(tupleData);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void ClearMessage(HMessage[] messageHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(messageHandle);
			IntPtr proc = HalconAPI.PreCall(540);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(messageHandle);
		}

		public void ClearMessage()
		{
			IntPtr proc = HalconAPI.PreCall(540);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateMessage()
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(541);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ReadMessage(string fileName, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2164);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ReadMessage(string fileName, string genParamName, string genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2164);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteMessage(string fileName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2175);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteMessage(string fileName, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2175);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
