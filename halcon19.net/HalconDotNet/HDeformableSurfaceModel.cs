using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HDeformableSurfaceModel : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceModel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("deformable_surface_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDeformableSurfaceModel obj)
		{
			obj = new HDeformableSurfaceModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDeformableSurfaceModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDeformableSurfaceModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HDeformableSurfaceModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HDeformableSurfaceModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1024);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1031);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public HDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1031);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeDeformableSurfaceModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDeformableSurfaceModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeDeformableSurfaceModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeDeformableSurfaceModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HDeformableSurfaceModel Deserialize(Stream stream)
		{
			HDeformableSurfaceModel hDeformableSurfaceModel = new HDeformableSurfaceModel();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hDeformableSurfaceModel.DeserializeDeformableSurfaceModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDeformableSurfaceModel;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HDeformableSurfaceModel Clone()
		{
			HSerializedItem hSerializedItem = SerializeDeformableSurfaceModel();
			HDeformableSurfaceModel hDeformableSurfaceModel = new HDeformableSurfaceModel();
			hDeformableSurfaceModel.DeserializeDeformableSurfaceModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDeformableSurfaceModel;
		}

		public static void ClearDeformableSurfaceModel(HDeformableSurfaceModel[] deformableSurfaceModel)
		{
			HTuple hTuple = HHandleBase.ConcatArray(deformableSurfaceModel);
			IntPtr proc = HalconAPI.PreCall(1021);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(deformableSurfaceModel);
		}

		public void ClearDeformableSurfaceModel()
		{
			IntPtr proc = HalconAPI.PreCall(1021);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeDeformableSurfaceModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1022);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeDeformableSurfaceModel()
		{
			IntPtr proc = HalconAPI.PreCall(1023);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadDeformableSurfaceModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1024);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteDeformableSurfaceModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1025);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple RefineDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HObjectModel3D initialDeformationObjectModel3D, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1026);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, initialDeformationObjectModel3D);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			GC.KeepAlive(initialDeformationObjectModel3D);
			return tuple;
		}

		public double RefineDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HObjectModel3D initialDeformationObjectModel3D, string genParamName, string genParamValue, out HDeformableSurfaceMatchingResult deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1026);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, initialDeformationObjectModel3D);
			HalconAPI.StoreS(proc, 4, genParamName);
			HalconAPI.StoreS(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			GC.KeepAlive(initialDeformationObjectModel3D);
			return doubleValue;
		}

		public HTuple FindDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HTuple minScore, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult[] deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1027);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.Store(proc, 3, minScore);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return tuple;
		}

		public double FindDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, double minScore, HTuple genParamName, HTuple genParamValue, out HDeformableSurfaceMatchingResult deformableSurfaceMatchingResult)
		{
			IntPtr proc = HalconAPI.PreCall(1027);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.StoreD(proc, 2, relSamplingDistance);
			HalconAPI.StoreD(proc, 3, minScore);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HDeformableSurfaceMatchingResult.LoadNew(proc, 1, err, out deformableSurfaceMatchingResult);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return doubleValue;
		}

		public HTuple GetDeformableSurfaceModelParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1028);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDeformableSurfaceModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1028);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple AddDeformableSurfaceModelReferencePoint(HTuple referencePointX, HTuple referencePointY, HTuple referencePointZ)
		{
			IntPtr proc = HalconAPI.PreCall(1029);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, referencePointX);
			HalconAPI.Store(proc, 2, referencePointY);
			HalconAPI.Store(proc, 3, referencePointZ);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(referencePointX);
			HalconAPI.UnpinTuple(referencePointY);
			HalconAPI.UnpinTuple(referencePointZ);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int AddDeformableSurfaceModelReferencePoint(double referencePointX, double referencePointY, double referencePointZ)
		{
			IntPtr proc = HalconAPI.PreCall(1029);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, referencePointX);
			HalconAPI.StoreD(proc, 2, referencePointY);
			HalconAPI.StoreD(proc, 3, referencePointZ);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public void AddDeformableSurfaceModelSample(HObjectModel3D[] objectModel3D)
		{
			HTuple hTuple = HHandleBase.ConcatArray(objectModel3D);
			IntPtr proc = HalconAPI.PreCall(1030);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public void AddDeformableSurfaceModelSample(HObjectModel3D objectModel3D)
		{
			IntPtr proc = HalconAPI.PreCall(1030);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectModel3D);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public void CreateDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1031);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public void CreateDeformableSurfaceModel(HObjectModel3D objectModel3D, double relSamplingDistance, string genParamName, string genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1031);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 1, relSamplingDistance);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}
	}
}
