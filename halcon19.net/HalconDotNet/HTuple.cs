using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HTuple : ISerializable, ICloneable, IDisposable
	{
		private delegate void NativeInt2To1(int[] in1, int[] in2, int[] buffer);

		private delegate void NativeLong2To1(long[] in1, long[] in2, long[] buffer);

		private delegate void NativeDouble2To1(double[] in1, double[] in2, double[] buffer);

		private enum ResultSize
		{
			EQUAL,
			SUM
		}

		internal HTupleImplementation data;

		private static NativeInt2To1 addInt = NativeIntAdd;

		private static NativeLong2To1 addLong = NativeLongAdd;

		private static NativeDouble2To1 addDouble = NativeDoubleAdd;

		private static NativeInt2To1 subInt = NativeIntSub;

		private static NativeLong2To1 subLong = NativeLongSub;

		private static NativeDouble2To1 subDouble = NativeDoubleSub;

		private static NativeInt2To1 multInt = NativeIntMult;

		private static NativeLong2To1 multLong = NativeLongMult;

		private static NativeDouble2To1 multDouble = NativeDoubleMult;

		public HTupleType Type => data.Type;

		public int Length => data.Length;

		public HTupleElements this[int[] indices]
		{
			get
			{
				foreach (int num in indices)
				{
					if (num < 0 || num >= data.Length)
					{
						throw new HTupleAccessException("Index out of range");
					}
				}
				return data.GetElements(indices, this);
			}
			set
			{
				if (indices.Length == 0)
				{
					if (value.Length > 1)
					{
						throw new HTupleAccessException("Input parameter 2 ('Value') must have one element or the same number of elements as parameter 1 ('Index')");
					}
					return;
				}
				foreach (int num in indices)
				{
					if (num < 0)
					{
						throw new HTupleAccessException("Index out of range");
					}
				}
				if (data.Type == HTupleType.EMPTY)
				{
					switch (value.Type)
					{
					case HTupleType.INTEGER:
						data = new HTupleInt32(0);
						break;
					case HTupleType.LONG:
						data = new HTupleInt64(0L);
						break;
					case HTupleType.DOUBLE:
						data = new HTupleDouble(0.0);
						break;
					case HTupleType.STRING:
						data = new HTupleString("");
						break;
					case HTupleType.HANDLE:
						data = new HTupleHandle(null);
						break;
					case HTupleType.MIXED:
						data = new HTupleMixed(0);
						break;
					default:
						throw new HTupleAccessException("Inconsistent tuple state encountered");
					}
				}
				data.AssertSize(indices);
				if (value.Type != data.Type)
				{
					ConvertToMixed();
				}
				try
				{
					data.SetElements(indices, value);
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					data.SetElements(indices, value);
				}
			}
		}

		public HTupleElements this[int index]
		{
			get
			{
				if (index < 0 || index >= data.Length)
				{
					throw new HTupleAccessException("Index out of range");
				}
				return data.GetElement(index, this);
			}
			set
			{
				this[new int[1]
				{
					index
				}] = value;
			}
		}

		public HTupleElements this[HTuple indices]
		{
			get
			{
				return this[GetIndicesFromTuple(indices)];
			}
			set
			{
				this[GetIndicesFromTuple(indices)] = value;
			}
		}

		public int[] IArr
		{
			get
			{
				return data.IArr;
			}
			set
			{
				if (Type == HTupleType.INTEGER)
				{
					data.IArr = value;
				}
				else
				{
					data = new HTupleInt32(value, copy: false);
				}
			}
		}

		public long[] LArr
		{
			get
			{
				return data.LArr;
			}
			set
			{
				if (Type == HTupleType.LONG)
				{
					data.LArr = value;
				}
				else
				{
					data = new HTupleInt64(value, copy: false);
				}
			}
		}

		public double[] DArr
		{
			get
			{
				return data.DArr;
			}
			set
			{
				if (Type == HTupleType.DOUBLE)
				{
					data.DArr = value;
				}
				else
				{
					data = new HTupleDouble(value, copy: false);
				}
			}
		}

		public string[] SArr
		{
			get
			{
				return data.SArr;
			}
			set
			{
				if (Type == HTupleType.STRING)
				{
					data.SArr = value;
				}
				else
				{
					data = new HTupleString(value, copy: false);
				}
			}
		}

		public HHandle[] HArr
		{
			get
			{
				return data.HArr;
			}
			set
			{
				if (Type == HTupleType.HANDLE)
				{
					data.HArr = value;
				}
				else
				{
					data = new HTupleHandle(value, copy: false);
				}
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public object[] OArr => data.OArr;

		public int I
		{
			get
			{
				return this[0].I;
			}
			set
			{
				this[0].I = value;
			}
		}

		public long L
		{
			get
			{
				return this[0].L;
			}
			set
			{
				this[0].L = value;
			}
		}

		public double D
		{
			get
			{
				return this[0].D;
			}
			set
			{
				this[0].D = value;
			}
		}

		public string S
		{
			get
			{
				return this[0].S;
			}
			set
			{
				this[0].S = value;
			}
		}

		public HHandle H
		{
			get
			{
				return this[0].H;
			}
			set
			{
				this[0].H = value;
			}
		}

		public object O
		{
			get
			{
				return this[0].O;
			}
			set
			{
				this[0].O = value;
			}
		}

		public IntPtr IP
		{
			get
			{
				return this[0].IP;
			}
			set
			{
				this[0].IP = value;
			}
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeTuple();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTuple(SerializationInfo info, StreamingContext context)
		{
			byte[] array = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(array);
			HTuple hTuple = DeserializeTuple(hSerializedItem);
			data = hTuple.data;
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeTuple();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public static HTuple Deserialize(Stream stream)
		{
			HTuple hTuple = new HTuple();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hTuple = DeserializeTuple(hSerializedItem);
			hSerializedItem.Dispose();
			return hTuple;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public HTuple TupleUnion(HTuple set2)
		{
			IntPtr proc = HalconAPI.PreCall(96);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, set2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(set2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIntersection(HTuple set2)
		{
			IntPtr proc = HalconAPI.PreCall(97);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, set2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(set2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleDifference(HTuple set2)
		{
			IntPtr proc = HalconAPI.PreCall(98);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, set2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(set2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSymmdiff(HTuple set2)
		{
			IntPtr proc = HalconAPI.PreCall(99);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, set2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(set2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsStringElem()
		{
			IntPtr proc = HalconAPI.PreCall(100);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsRealElem()
		{
			IntPtr proc = HalconAPI.PreCall(101);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsIntElem()
		{
			IntPtr proc = HalconAPI.PreCall(102);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleTypeElem()
		{
			IntPtr proc = HalconAPI.PreCall(103);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsMixed()
		{
			IntPtr proc = HalconAPI.PreCall(104);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsString()
		{
			IntPtr proc = HalconAPI.PreCall(105);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsReal()
		{
			IntPtr proc = HalconAPI.PreCall(106);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsInt()
		{
			IntPtr proc = HalconAPI.PreCall(107);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleType()
		{
			IntPtr proc = HalconAPI.PreCall(108);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleHistoRange(HTuple min, HTuple max, HTuple numBins, out HTuple binSize)
		{
			IntPtr proc = HalconAPI.PreCall(109);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, min);
			HalconAPI.Store(proc, 2, max);
			HalconAPI.Store(proc, 3, numBins);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			HalconAPI.UnpinTuple(numBins);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = LoadNew(proc, 1, HTupleType.DOUBLE, err, out binSize);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleRegexpSelect(HTuple expression)
		{
			IntPtr proc = HalconAPI.PreCall(110);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, expression);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(expression);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleRegexpTest(HTuple expression)
		{
			IntPtr proc = HalconAPI.PreCall(111);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, expression);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(expression);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleRegexpReplace(HTuple expression, HTuple replace)
		{
			IntPtr proc = HalconAPI.PreCall(112);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, expression);
			HalconAPI.Store(proc, 2, replace);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(expression);
			HalconAPI.UnpinTuple(replace);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleRegexpMatch(HTuple expression)
		{
			IntPtr proc = HalconAPI.PreCall(113);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, expression);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(expression);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public static HTuple TupleRand(HTuple length)
		{
			IntPtr proc = HalconAPI.PreCall(114);
			HalconAPI.Store(proc, 0, length);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(length);
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		private HTuple TupleLengthOp()
		{
			IntPtr proc = HalconAPI.PreCall(115);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSgn()
		{
			IntPtr proc = HalconAPI.PreCall(116);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleMax2(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(117);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleMin2(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(118);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleMax()
		{
			IntPtr proc = HalconAPI.PreCall(119);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleMin()
		{
			IntPtr proc = HalconAPI.PreCall(120);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleCumul()
		{
			IntPtr proc = HalconAPI.PreCall(121);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSelectRank(HTuple rankIndex)
		{
			IntPtr proc = HalconAPI.PreCall(122);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, rankIndex);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(rankIndex);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleMedian()
		{
			IntPtr proc = HalconAPI.PreCall(123);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSum()
		{
			IntPtr proc = HalconAPI.PreCall(124);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleMean()
		{
			IntPtr proc = HalconAPI.PreCall(125);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleDeviation()
		{
			IntPtr proc = HalconAPI.PreCall(126);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleUniq()
		{
			IntPtr proc = HalconAPI.PreCall(127);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleFindLast(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(128);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleFindFirst(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(129);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleFind(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(130);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSortIndex()
		{
			IntPtr proc = HalconAPI.PreCall(131);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSort()
		{
			IntPtr proc = HalconAPI.PreCall(132);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleInverse()
		{
			IntPtr proc = HalconAPI.PreCall(133);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		private HTuple TupleConcatOp(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(134);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSelectRange(HTuple leftindex, HTuple rightindex)
		{
			IntPtr proc = HalconAPI.PreCall(135);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, leftindex);
			HalconAPI.Store(proc, 2, rightindex);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(leftindex);
			HalconAPI.UnpinTuple(rightindex);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleLastN(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(136);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleFirstN(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(137);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleInsert(HTuple index, HTuple insertTuple)
		{
			IntPtr proc = HalconAPI.PreCall(138);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, insertTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(insertTuple);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleReplace(HTuple index, HTuple replaceTuple)
		{
			IntPtr proc = HalconAPI.PreCall(139);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, replaceTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(replaceTuple);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleRemove(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(140);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSelectMask(HTuple mask)
		{
			IntPtr proc = HalconAPI.PreCall(141);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, mask);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(mask);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSelect(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(142);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleStrBitSelect(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(143);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public static HTuple TupleGenSequence(HTuple start, HTuple end, HTuple step)
		{
			IntPtr proc = HalconAPI.PreCall(144);
			HalconAPI.Store(proc, 0, start);
			HalconAPI.Store(proc, 1, end);
			HalconAPI.Store(proc, 2, step);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(start);
			HalconAPI.UnpinTuple(end);
			HalconAPI.UnpinTuple(step);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple TupleGenConst(HTuple length, HTuple constVal)
		{
			IntPtr proc = HalconAPI.PreCall(145);
			HalconAPI.Store(proc, 0, length);
			HalconAPI.Store(proc, 1, constVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(length);
			HalconAPI.UnpinTuple(constVal);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public HTuple TupleEnvironment()
		{
			IntPtr proc = HalconAPI.PreCall(146);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSplit(HTuple separator)
		{
			IntPtr proc = HalconAPI.PreCall(147);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, separator);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(separator);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSubstr(HTuple position1, HTuple position2)
		{
			IntPtr proc = HalconAPI.PreCall(148);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, position1);
			HalconAPI.Store(proc, 2, position2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(position1);
			HalconAPI.UnpinTuple(position2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleStrLastN(HTuple position)
		{
			IntPtr proc = HalconAPI.PreCall(149);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, position);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(position);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleStrFirstN(HTuple position)
		{
			IntPtr proc = HalconAPI.PreCall(150);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, position);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(position);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleStrrchr(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(151);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleStrchr(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(152);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleStrrstr(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(153);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleStrstr(HTuple toFind)
		{
			IntPtr proc = HalconAPI.PreCall(154);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, toFind);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(toFind);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleStrlen()
		{
			IntPtr proc = HalconAPI.PreCall(155);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleLessEqualElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(156);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleLessElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(157);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleGreaterEqualElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(158);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleGreaterElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(159);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleNotEqualElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(160);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleEqualElem(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(161);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleLessEqual(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(162);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleLess(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(163);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleGreaterEqual(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(164);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleGreater(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(165);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleNotEqual(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(166);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleEqual(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(167);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleNot()
		{
			IntPtr proc = HalconAPI.PreCall(168);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleXor(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(169);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleOr(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(170);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleAnd(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(171);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleBnot()
		{
			IntPtr proc = HalconAPI.PreCall(172);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleBxor(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(173);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleBor(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(174);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleBand(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(175);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleRsh(HTuple shift)
		{
			IntPtr proc = HalconAPI.PreCall(176);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, shift);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(shift);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleLsh(HTuple shift)
		{
			IntPtr proc = HalconAPI.PreCall(177);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, shift);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(shift);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleChrt()
		{
			IntPtr proc = HalconAPI.PreCall(178);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleOrds()
		{
			IntPtr proc = HalconAPI.PreCall(179);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleChr()
		{
			IntPtr proc = HalconAPI.PreCall(180);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleOrd()
		{
			IntPtr proc = HalconAPI.PreCall(181);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleString(HTuple format)
		{
			IntPtr proc = HalconAPI.PreCall(182);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, format);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(format);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsNumber()
		{
			IntPtr proc = HalconAPI.PreCall(183);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleNumber()
		{
			IntPtr proc = HalconAPI.PreCall(184);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleRound()
		{
			IntPtr proc = HalconAPI.PreCall(185);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleInt()
		{
			IntPtr proc = HalconAPI.PreCall(186);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleReal()
		{
			IntPtr proc = HalconAPI.PreCall(187);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleLdexp(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(188);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleFmod(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(189);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleMod(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(190);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleCeil()
		{
			IntPtr proc = HalconAPI.PreCall(191);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleFloor()
		{
			IntPtr proc = HalconAPI.PreCall(192);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TuplePow(HTuple t2)
		{
			IntPtr proc = HalconAPI.PreCall(193);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, t2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(t2);
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleLog10()
		{
			IntPtr proc = HalconAPI.PreCall(194);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleLog()
		{
			IntPtr proc = HalconAPI.PreCall(195);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleExp()
		{
			IntPtr proc = HalconAPI.PreCall(196);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleTanh()
		{
			IntPtr proc = HalconAPI.PreCall(197);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleCosh()
		{
			IntPtr proc = HalconAPI.PreCall(198);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSinh()
		{
			IntPtr proc = HalconAPI.PreCall(199);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleRad()
		{
			IntPtr proc = HalconAPI.PreCall(200);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleDeg()
		{
			IntPtr proc = HalconAPI.PreCall(201);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleAtan2(HTuple x)
		{
			IntPtr proc = HalconAPI.PreCall(202);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, x);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(x);
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleAtan()
		{
			IntPtr proc = HalconAPI.PreCall(203);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleAcos()
		{
			IntPtr proc = HalconAPI.PreCall(204);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleAsin()
		{
			IntPtr proc = HalconAPI.PreCall(205);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleTan()
		{
			IntPtr proc = HalconAPI.PreCall(206);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleCos()
		{
			IntPtr proc = HalconAPI.PreCall(207);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSin()
		{
			IntPtr proc = HalconAPI.PreCall(208);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleFabs()
		{
			IntPtr proc = HalconAPI.PreCall(209);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSqrt()
		{
			IntPtr proc = HalconAPI.PreCall(210);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleAbs()
		{
			IntPtr proc = HalconAPI.PreCall(211);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleNeg()
		{
			IntPtr proc = HalconAPI.PreCall(212);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleDiv(HTuple q2)
		{
			IntPtr proc = HalconAPI.PreCall(213);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, q2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(q2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		private HTuple TupleMultOp(HTuple p2)
		{
			IntPtr proc = HalconAPI.PreCall(214);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, p2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(p2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		private HTuple TupleSubOp(HTuple d2)
		{
			IntPtr proc = HalconAPI.PreCall(215);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, d2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(d2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		private HTuple TupleAddOp(HTuple s2)
		{
			IntPtr proc = HalconAPI.PreCall(216);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, s2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(s2);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public static HTuple DeserializeTuple(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(217);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(serializedItemHandle);
			return tuple;
		}

		public HSerializedItem SerializeTuple()
		{
			IntPtr proc = HalconAPI.PreCall(218);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void WriteTuple(HTuple fileName)
		{
			IntPtr proc = HalconAPI.PreCall(219);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(fileName);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static HTuple ReadTuple(HTuple fileName)
		{
			IntPtr proc = HalconAPI.PreCall(220);
			HalconAPI.Store(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fileName);
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public void ClearHandle()
		{
			IntPtr proc = HalconAPI.PreCall(2134);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple TupleIsHandle()
		{
			IntPtr proc = HalconAPI.PreCall(2139);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsHandleElem()
		{
			IntPtr proc = HalconAPI.PreCall(2140);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsSerializable()
		{
			IntPtr proc = HalconAPI.PreCall(2141);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsSerializableElem()
		{
			IntPtr proc = HalconAPI.PreCall(2142);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleIsValidHandle()
		{
			IntPtr proc = HalconAPI.PreCall(2143);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSemType()
		{
			IntPtr proc = HalconAPI.PreCall(2144);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple TupleSemTypeElem()
		{
			IntPtr proc = HalconAPI.PreCall(2145);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple()
		{
			data = HTupleVoid.EMPTY;
		}

		public HTuple(bool b)
			: this(new IntPtr(b ? 1 : 0))
		{
		}

		public HTuple(int i)
		{
			data = new HTupleInt32(i);
		}

		public HTuple(params int[] i)
		{
			data = new HTupleInt32(i, copy: true);
		}

		public HTuple(long l)
		{
			data = new HTupleInt64(l);
		}

		public HTuple(params long[] l)
		{
			data = new HTupleInt64(l, copy: true);
		}

		public HTuple(IntPtr ip)
			: this(new IntPtr[1]
			{
				ip
			})
		{
		}

		public HTuple(params IntPtr[] ip)
		{
			if (HalconAPI.isPlatform64)
			{
				long[] array = new long[ip.Length];
				for (int i = 0; i < ip.Length; i++)
				{
					array[i] = ip[i].ToInt64();
				}
				data = new HTupleInt64(array, copy: false);
			}
			else
			{
				int[] array2 = new int[ip.Length];
				for (int j = 0; j < ip.Length; j++)
				{
					array2[j] = ip[j].ToInt32();
				}
				data = new HTupleInt32(array2, copy: false);
			}
		}

		internal HTuple(int i, bool platformSize)
		{
			if (platformSize && HalconAPI.isPlatform64)
			{
				data = new HTupleInt64(i);
			}
			else
			{
				data = new HTupleInt32(i);
			}
		}

		public HTuple(double d)
		{
			data = new HTupleDouble(d);
		}

		public HTuple(params double[] d)
		{
			data = new HTupleDouble(d, copy: true);
		}

		public HTuple(float f)
		{
			data = new HTupleDouble(f);
		}

		public HTuple(params float[] f)
		{
			data = new HTupleDouble(f);
		}

		public HTuple(string s)
		{
			data = new HTupleString(s);
		}

		public HTuple(params string[] s)
		{
			data = new HTupleString(s, copy: true);
		}

		public HTuple(HHandle h)
		{
			data = new HTupleHandle(h);
		}

		public HTuple(params HHandle[] h)
		{
			data = new HTupleHandle(h, copy: true);
		}

		internal HTuple(object o)
		{
			data = new HTupleMixed(o);
		}

		public HTuple(params object[] o)
		{
			data = new HTupleMixed(o, copy: true);
		}

		public HTuple(HTuple t)
		{
			switch (t.Type)
			{
			case HTupleType.EMPTY:
				data = HTupleVoid.EMPTY;
				break;
			case HTupleType.INTEGER:
				data = new HTupleInt32(t.ToIArr(), copy: false);
				break;
			case HTupleType.LONG:
				data = new HTupleInt64(t.ToLArr(), copy: false);
				break;
			case HTupleType.DOUBLE:
				data = new HTupleDouble(t.ToDArr(), copy: false);
				break;
			case HTupleType.STRING:
				data = new HTupleString(t.ToSArr(), copy: false);
				break;
			case HTupleType.HANDLE:
				data = new HTupleHandle(t.ToHArr(), copy: false);
				break;
			case HTupleType.MIXED:
				data = new HTupleMixed(t.ToOArr(), copy: false);
				break;
			default:
				throw new HTupleAccessException("Inconsistent tuple state encountered");
			}
		}

		public HTuple(params HTuple[] t)
			: this()
		{
			HTuple hTuple = new HTuple();
			hTuple = hTuple.TupleConcat(t);
			TransferOwnership(hTuple);
		}

		internal HTuple(HTupleImplementation data)
		{
			this.data = data;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void TransferOwnership(HTuple source)
		{
			if (!object.ReferenceEquals(source, this))
			{
				if (source == null)
				{
					data = HTupleVoid.EMPTY;
					return;
				}
				data = source.data;
				source.data = HTupleVoid.EMPTY;
			}
		}

		public HTuple Clone()
		{
			if (Type == HTupleType.HANDLE || Type == HTupleType.MIXED)
			{
				using (HSerializedItem serializedItemHandle = SerializeTuple())
				{
					return DeserializeTuple(serializedItemHandle);
				}
			}
			return new HTuple(this);
		}

		public void Dispose()
		{
			data.Dispose();
		}

		public void UnpinTuple()
		{
			data.UnpinTuple();
		}

		internal static int[] GetIndicesFromTuple(HTuple indices)
		{
			if (indices.Type == HTupleType.LONG || indices.Type == HTupleType.INTEGER)
			{
				return indices.ToIArr();
			}
			int[] array = new int[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				if (indices[i].Type == HTupleType.INTEGER)
				{
					array[i] = indices[i].I;
					continue;
				}
				if (indices[i].Type == HTupleType.LONG)
				{
					array[i] = indices[i].I;
					continue;
				}
				if (indices[i].Type == HTupleType.DOUBLE)
				{
					double d = indices[i].D;
					int num = (int)d;
					if ((double)num != d)
					{
						throw new HTupleAccessException("Index has fractional part");
					}
					array[i] = num;
					continue;
				}
				throw new HTupleAccessException("Invalid index type");
			}
			return array;
		}

		private void ConvertToMixed()
		{
			if (!(data is HTupleMixed))
			{
				HTupleImplementation hTupleImplementation = new HTupleMixed(data);
				data.Dispose();
				data = hTupleImplementation;
			}
		}

		internal HTupleElementsMixed ConvertToMixed(int[] indices)
		{
			ConvertToMixed();
			return new HTupleElementsMixed((HTupleMixed)data, indices);
		}

		public int[] ToIArr()
		{
			return data.ToIArr();
		}

		public long[] ToLArr()
		{
			return data.ToLArr();
		}

		public double[] ToDArr()
		{
			return data.ToDArr();
		}

		public string[] ToSArr()
		{
			return data.ToSArr();
		}

		public HHandle[] ToHArr()
		{
			return data.ToHArr();
		}

		public object[] ToOArr()
		{
			return data.ToOArr();
		}

		public float[] ToFArr()
		{
			return data.ToFArr();
		}

		public IntPtr[] ToIPArr()
		{
			return data.ToIPArr();
		}

		public static implicit operator HTupleElements(HTuple t)
		{
			if (t.Length == 1)
			{
				return t[0];
			}
			int[] array = new int[t.Length];
			for (int i = 0; i < t.Length; i++)
			{
				array[i] = i;
			}
			return t[array];
		}

		public static implicit operator bool(HTuple t)
		{
			return t[0];
		}

		public static implicit operator int(HTuple t)
		{
			return t[0];
		}

		public static implicit operator long(HTuple t)
		{
			return t[0];
		}

		public static implicit operator double(HTuple t)
		{
			return t[0];
		}

		public static implicit operator string(HTuple t)
		{
			return t[0];
		}

		public static implicit operator IntPtr(HTuple t)
		{
			return t[0];
		}

		public static implicit operator int[](HTuple t)
		{
			return t.ToIArr();
		}

		public static implicit operator long[](HTuple t)
		{
			return t.ToLArr();
		}

		public static implicit operator double[](HTuple t)
		{
			return t.ToDArr();
		}

		public static implicit operator string[](HTuple t)
		{
			return t.ToSArr();
		}

		public static implicit operator HHandle[](HTuple t)
		{
			return t.ToHArr();
		}

		public static implicit operator IntPtr[](HTuple t)
		{
			return t.ToIPArr();
		}

		public static implicit operator HTuple(HTupleElements e)
		{
			switch (e.Type)
			{
			case HTupleType.INTEGER:
				return new HTuple(e.IArr);
			case HTupleType.LONG:
				return new HTuple(e.LArr);
			case HTupleType.DOUBLE:
				return new HTuple(e.DArr);
			case HTupleType.STRING:
				return new HTuple(e.SArr);
			case HTupleType.HANDLE:
				return new HTuple(e.HArr);
			case HTupleType.MIXED:
				return new HTuple(e.OArr);
			case HTupleType.EMPTY:
				return new HTuple();
			default:
				throw new HTupleAccessException("Inconsistent tuple state encountered");
			}
		}

		public static implicit operator HTuple(int i)
		{
			return new HTuple(i);
		}

		public static implicit operator HTuple(int[] i)
		{
			return new HTuple(i);
		}

		public static implicit operator HTuple(long l)
		{
			return new HTuple(l);
		}

		public static implicit operator HTuple(long[] l)
		{
			return new HTuple(l);
		}

		public static implicit operator HTuple(double d)
		{
			return new HTuple(d);
		}

		public static implicit operator HTuple(double[] d)
		{
			return new HTuple(d);
		}

		public static implicit operator HTuple(string s)
		{
			return new HTuple(s);
		}

		public static implicit operator HTuple(string[] s)
		{
			return new HTuple(s);
		}

		public static implicit operator HTuple(HHandle h)
		{
			return new HTuple(h);
		}

		public static implicit operator HTuple(HHandle[] h)
		{
			return new HTuple(h);
		}

		public static implicit operator HTuple(object[] o)
		{
			return new HTuple(o);
		}

		public static implicit operator HTuple(IntPtr ip)
		{
			return new HTuple(ip);
		}

		public static implicit operator HTuple(IntPtr[] ip)
		{
			return new HTuple(ip);
		}

		internal void Store(IntPtr proc, int parIndex)
		{
			data.Store(proc, parIndex);
		}

		internal int Load(IntPtr proc, int parIndex, HTupleType type, int err)
		{
			if (HalconAPI.IsFailure(err))
			{
				data = HTupleVoid.EMPTY;
				return err;
			}
			return HTupleImplementation.Load(proc, parIndex, type, out data);
		}

		internal int Load(IntPtr proc, int parIndex, int err)
		{
			return Load(proc, parIndex, HTupleType.MIXED, err);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, HTupleType type, int err, out HTuple tuple)
		{
			tuple = new HTuple();
			return tuple.Load(proc, parIndex, type, err);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, int err, out HTuple tuple)
		{
			tuple = new HTuple();
			return tuple.Load(proc, parIndex, HTupleType.MIXED, err);
		}

		public override string ToString()
		{
			object[] array = ToOArr();
			string text = "";
			if (Length != 1)
			{
				text += "[";
			}
			for (int i = 0; i < array.Length; i++)
			{
				text += ((i > 0) ? ", " : "");
				text = ((this[i].Type != HTupleType.STRING) ? (text + array[i].ToString()) : (text + "\"" + array[i].ToString() + "\""));
				(array[i] as IDisposable)?.Dispose();
			}
			if (Length != 1)
			{
				text += "]";
			}
			return text;
		}

		public static HTuple operator +(HTuple t1, HTuple t2)
		{
			return t1.TupleAdd(t2);
		}

		public static HTuple operator +(HTuple t1, int t2)
		{
			return t1 + (HTuple)t2;
		}

		public static HTuple operator +(HTuple t1, long t2)
		{
			return t1 + (HTuple)t2;
		}

		public static HTuple operator +(HTuple t1, float t2)
		{
			return t1 + (HTuple)t2;
		}

		public static HTuple operator +(HTuple t1, double t2)
		{
			return t1 + (HTuple)t2;
		}

		public static HTuple operator +(HTuple t1, string t2)
		{
			return t1 + (HTuple)t2;
		}

		public static HTuple operator +(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 + t3;
			}
		}

		public static HTuple operator -(HTuple t1, HTuple t2)
		{
			return t1.TupleSub(t2);
		}

		public static HTuple operator -(HTuple t1, int t2)
		{
			return t1 - (HTuple)t2;
		}

		public static HTuple operator -(HTuple t1, long t2)
		{
			return t1 - (HTuple)t2;
		}

		public static HTuple operator -(HTuple t1, float t2)
		{
			return t1 - (HTuple)t2;
		}

		public static HTuple operator -(HTuple t1, double t2)
		{
			return t1 - (HTuple)t2;
		}

		public static HTuple operator -(HTuple t1, string t2)
		{
			return t1 - (HTuple)t2;
		}

		public static HTuple operator -(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 - t3;
			}
		}

		public static HTuple operator *(HTuple t1, HTuple t2)
		{
			return t1.TupleMult(t2);
		}

		public static HTuple operator *(HTuple t1, int t2)
		{
			return t1 * (HTuple)t2;
		}

		public static HTuple operator *(HTuple t1, long t2)
		{
			return t1 * (HTuple)t2;
		}

		public static HTuple operator *(HTuple t1, float t2)
		{
			return t1 * (HTuple)t2;
		}

		public static HTuple operator *(HTuple t1, double t2)
		{
			return t1 * (HTuple)t2;
		}

		public static HTuple operator *(HTuple t1, string t2)
		{
			return t1 * (HTuple)t2;
		}

		public static HTuple operator *(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 * t3;
			}
		}

		public static HTuple operator /(HTuple t1, HTuple t2)
		{
			return t1.TupleDiv(t2);
		}

		public static HTuple operator /(HTuple t1, int t2)
		{
			return t1 / (HTuple)t2;
		}

		public static HTuple operator /(HTuple t1, long t2)
		{
			return t1 / (HTuple)t2;
		}

		public static HTuple operator /(HTuple t1, float t2)
		{
			return t1 / (HTuple)t2;
		}

		public static HTuple operator /(HTuple t1, double t2)
		{
			return t1 / (HTuple)t2;
		}

		public static HTuple operator /(HTuple t1, string t2)
		{
			return t1 / (HTuple)t2;
		}

		public static HTuple operator /(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 / t3;
			}
		}

		public static HTuple operator %(HTuple t1, HTuple t2)
		{
			return t1.TupleMod(t2);
		}

		public static HTuple operator %(HTuple t1, int t2)
		{
			return t1 % (HTuple)t2;
		}

		public static HTuple operator %(HTuple t1, long t2)
		{
			return t1 % (HTuple)t2;
		}

		public static HTuple operator %(HTuple t1, float t2)
		{
			return t1 % (HTuple)t2;
		}

		public static HTuple operator %(HTuple t1, double t2)
		{
			return t1 % (HTuple)t2;
		}

		public static HTuple operator %(HTuple t1, string t2)
		{
			return t1 % (HTuple)t2;
		}

		public static HTuple operator %(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 % t3;
			}
		}

		public static HTuple operator &(HTuple t1, HTuple t2)
		{
			return t1.TupleAnd(t2);
		}

		public static HTuple operator &(HTuple t1, int t2)
		{
			return t1 & (HTuple)t2;
		}

		public static HTuple operator &(HTuple t1, long t2)
		{
			return t1 & (HTuple)t2;
		}

		public static HTuple operator &(HTuple t1, float t2)
		{
			return t1 & (HTuple)t2;
		}

		public static HTuple operator &(HTuple t1, double t2)
		{
			return t1 & (HTuple)t2;
		}

		public static HTuple operator &(HTuple t1, string t2)
		{
			return t1 & (HTuple)t2;
		}

		public static HTuple operator &(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 & t3;
			}
		}

		public static HTuple operator |(HTuple t1, HTuple t2)
		{
			return t1.TupleOr(t2);
		}

		public static HTuple operator |(HTuple t1, int t2)
		{
			return t1 | (HTuple)t2;
		}

		public static HTuple operator |(HTuple t1, long t2)
		{
			return t1 | (HTuple)t2;
		}

		public static HTuple operator |(HTuple t1, float t2)
		{
			return t1 | (HTuple)t2;
		}

		public static HTuple operator |(HTuple t1, double t2)
		{
			return t1 | (HTuple)t2;
		}

		public static HTuple operator |(HTuple t1, string t2)
		{
			return t1 | (HTuple)t2;
		}

		public static HTuple operator |(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 | t3;
			}
		}

		public static HTuple operator ^(HTuple t1, HTuple t2)
		{
			return t1.TupleXor(t2);
		}

		public static HTuple operator ^(HTuple t1, int t2)
		{
			return t1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTuple t1, long t2)
		{
			return t1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTuple t1, float t2)
		{
			return t1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTuple t1, double t2)
		{
			return t1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTuple t1, string t2)
		{
			return t1 ^ (HTuple)t2;
		}

		public static HTuple operator ^(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 ^ t3;
			}
		}

		public static bool operator false(HTuple t)
		{
			return !t;
		}

		public static bool operator true(HTuple t)
		{
			return t;
		}

		public static HTuple operator <<(HTuple t1, int shift)
		{
			return t1.TupleLsh(shift);
		}

		public static HTuple operator >>(HTuple t1, int shift)
		{
			return t1.TupleRsh(shift);
		}

		public static bool operator <(HTuple t1, HTuple t2)
		{
			return (int)t1.TupleLess(t2) != 0;
		}

		public static bool operator <(HTuple t1, int t2)
		{
			return t1 < (HTuple)t2;
		}

		public static bool operator <(HTuple t1, long t2)
		{
			return t1 < (HTuple)t2;
		}

		public static bool operator <(HTuple t1, float t2)
		{
			return t1 < (HTuple)t2;
		}

		public static bool operator <(HTuple t1, double t2)
		{
			return t1 < (HTuple)t2;
		}

		public static bool operator <(HTuple t1, string t2)
		{
			return t1 < (HTuple)t2;
		}

		public static bool operator <(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 < t3;
			}
		}

		public static bool operator >(HTuple t1, HTuple t2)
		{
			return (int)t1.TupleGreater(t2) != 0;
		}

		public static bool operator >(HTuple t1, int t2)
		{
			return t1 > (HTuple)t2;
		}

		public static bool operator >(HTuple t1, long t2)
		{
			return t1 > (HTuple)t2;
		}

		public static bool operator >(HTuple t1, float t2)
		{
			return t1 > (HTuple)t2;
		}

		public static bool operator >(HTuple t1, double t2)
		{
			return t1 > (HTuple)t2;
		}

		public static bool operator >(HTuple t1, string t2)
		{
			return t1 > (HTuple)t2;
		}

		public static bool operator >(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 > t3;
			}
		}

		public static bool operator >=(HTuple t1, HTuple t2)
		{
			return !(t1 < t2);
		}

		public static bool operator >=(HTuple t1, int t2)
		{
			return t1 >= (HTuple)t2;
		}

		public static bool operator >=(HTuple t1, long t2)
		{
			return t1 >= (HTuple)t2;
		}

		public static bool operator >=(HTuple t1, float t2)
		{
			return t1 >= (HTuple)t2;
		}

		public static bool operator >=(HTuple t1, double t2)
		{
			return t1 >= (HTuple)t2;
		}

		public static bool operator >=(HTuple t1, string t2)
		{
			return t1 >= (HTuple)t2;
		}

		public static bool operator >=(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 >= t3;
			}
		}

		public static bool operator <=(HTuple t1, HTuple t2)
		{
			return !(t1 > t2);
		}

		public static bool operator <=(HTuple t1, int t2)
		{
			return t1 <= (HTuple)t2;
		}

		public static bool operator <=(HTuple t1, long t2)
		{
			return t1 <= (HTuple)t2;
		}

		public static bool operator <=(HTuple t1, float t2)
		{
			return t1 <= (HTuple)t2;
		}

		public static bool operator <=(HTuple t1, double t2)
		{
			return t1 <= (HTuple)t2;
		}

		public static bool operator <=(HTuple t1, string t2)
		{
			return t1 <= (HTuple)t2;
		}

		public static bool operator <=(HTuple t1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)t2)
			{
				return t1 <= t3;
			}
		}

		public static HTuple operator -(HTuple t)
		{
			return t.TupleNeg();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool Continue(HTuple final_value, HTuple increment)
		{
			if (increment[0] < 0.0)
			{
				return this[0].D >= final_value[0].D;
			}
			return this[0].D <= final_value[0].D;
		}

		public void Append(HTuple tuple)
		{
			HTupleImplementation hTupleImplementation = data;
			data = TupleConcat(tuple).data;
			hTupleImplementation.Dispose();
		}

		private bool ProcessNative2To1(HTuple t2, ResultSize type, out HTuple result, NativeInt2To1 opInt, NativeLong2To1 opLong, NativeDouble2To1 opDouble)
		{
			int num = (type == ResultSize.EQUAL) ? Length : (Length + t2.Length);
			if (num == 0)
			{
				result = new HTuple();
				return true;
			}
			if (Type == t2.Type && (Length == t2.Length || type == ResultSize.SUM))
			{
				if (Type == HTupleType.DOUBLE && opDouble != null)
				{
					double[] dArr = DArr;
					double[] dArr2 = t2.DArr;
					double[] array = new double[num];
					array[0] = Length;
					opDouble(dArr, dArr2, array);
					result = new HTuple(new HTupleDouble(array, copy: false));
					return true;
				}
				if (Type == HTupleType.INTEGER && opInt != null)
				{
					int[] iArr = IArr;
					int[] iArr2 = t2.IArr;
					int[] array2 = new int[num];
					array2[0] = Length;
					opInt(iArr, iArr2, array2);
					result = new HTuple(new HTupleInt32(array2, copy: false));
					return true;
				}
				if (Type == HTupleType.LONG && opLong != null)
				{
					long[] lArr = LArr;
					long[] lArr2 = t2.LArr;
					long[] array3 = new long[num];
					array3[0] = Length;
					opLong(lArr, lArr2, array3);
					result = new HTuple(new HTupleInt64(array3, copy: false));
					return true;
				}
			}
			result = null;
			return false;
		}

		private static void NativeIntAdd(int[] in1, int[] in2, int[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] + in2[i];
			}
		}

		private static void NativeLongAdd(long[] in1, long[] in2, long[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] + in2[i];
			}
		}

		private static void NativeDoubleAdd(double[] in1, double[] in2, double[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] + in2[i];
			}
		}

		private static void NativeIntSub(int[] in1, int[] in2, int[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] - in2[i];
			}
		}

		private static void NativeLongSub(long[] in1, long[] in2, long[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] - in2[i];
			}
		}

		private static void NativeDoubleSub(double[] in1, double[] in2, double[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] - in2[i];
			}
		}

		private static void NativeIntMult(int[] in1, int[] in2, int[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] * in2[i];
			}
		}

		private static void NativeLongMult(long[] in1, long[] in2, long[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] * in2[i];
			}
		}

		private static void NativeDoubleMult(double[] in1, double[] in2, double[] buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = in1[i] * in2[i];
			}
		}

		public int TupleLength()
		{
			return Length;
		}

		public HTuple TupleAdd(HTuple s2)
		{
			if (!ProcessNative2To1(s2, ResultSize.EQUAL, out HTuple result, addInt, addLong, addDouble))
			{
				return TupleAddOp(s2);
			}
			return result;
		}

		public HTuple TupleSub(HTuple d2)
		{
			if (!ProcessNative2To1(d2, ResultSize.EQUAL, out HTuple result, subInt, subLong, subDouble))
			{
				return TupleSubOp(d2);
			}
			return result;
		}

		public HTuple TupleMult(HTuple p2)
		{
			if (!ProcessNative2To1(p2, ResultSize.EQUAL, out HTuple result, multInt, multLong, multDouble))
			{
				return TupleMultOp(p2);
			}
			return result;
		}

		public HTuple TupleConcat(params HTuple[] tuples)
		{
			HTupleType hTupleType = Type;
			int num = Length;
			for (int i = 0; i < tuples.Length; i++)
			{
				if (hTupleType == HTupleType.EMPTY)
				{
					hTupleType = tuples[i].Type;
				}
				else if (hTupleType != tuples[i].Type && tuples[i].Type != HTupleType.EMPTY)
				{
					hTupleType = HTupleType.MIXED;
				}
				num += tuples[i].Length;
			}
			if ((hTupleType == HTupleType.MIXED || hTupleType == HTupleType.HANDLE) && HalconAPI.IsLegacyHandleMode())
			{
				HTuple hTuple = this;
				for (int j = 0; j < tuples.Length; j++)
				{
					HTuple hTuple2 = hTuple;
					hTuple = hTuple.TupleConcat(tuples[j]);
					if (j > 0)
					{
						hTuple2.Dispose();
					}
				}
				return hTuple;
			}
			HTupleImplementation hTupleImplementation = HTupleImplementation.CreateInstanceForType(hTupleType, num);
			int num2 = hTupleImplementation.CopyFrom(data, 0);
			for (int k = 0; k < tuples.Length; k++)
			{
				num2 += hTupleImplementation.CopyFrom(tuples[k].data, num2);
			}
			return new HTuple(hTupleImplementation);
		}

		public HTuple TupleConcat(HTuple t2)
		{
			HTupleType hTupleType = Type;
			int size = Length + t2.Length;
			if (hTupleType == HTupleType.EMPTY)
			{
				hTupleType = t2.Type;
			}
			else if (hTupleType != t2.Type && t2.Type != HTupleType.EMPTY)
			{
				hTupleType = HTupleType.MIXED;
			}
			if ((hTupleType == HTupleType.MIXED || hTupleType == HTupleType.HANDLE) && HalconAPI.IsLegacyHandleMode())
			{
				return TupleConcatOp(t2);
			}
			HTupleImplementation hTupleImplementation = HTupleImplementation.CreateInstanceForType(hTupleType, size);
			hTupleImplementation.CopyFrom(t2.data, hTupleImplementation.CopyFrom(data, 0));
			return new HTuple(hTupleImplementation);
		}
	}
}
