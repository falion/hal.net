using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HDescriptorModel : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDescriptorModel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDescriptorModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDescriptorModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("descriptor_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDescriptorModel obj)
		{
			obj = new HDescriptorModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDescriptorModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDescriptorModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HDescriptorModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HDescriptorModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(946);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDescriptorModel(HImage template, HCamPar camParam, HPose referencePose, string detectorType, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, int seed)
		{
			IntPtr proc = HalconAPI.PreCall(952);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreS(proc, 2, detectorType);
			HalconAPI.Store(proc, 3, detectorParamName);
			HalconAPI.Store(proc, 4, detectorParamValue);
			HalconAPI.Store(proc, 5, descriptorParamName);
			HalconAPI.Store(proc, 6, descriptorParamValue);
			HalconAPI.StoreI(proc, 7, seed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HDescriptorModel(HImage template, string detectorType, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, int seed)
		{
			IntPtr proc = HalconAPI.PreCall(953);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreS(proc, 0, detectorType);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.StoreI(proc, 5, seed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeDescriptorModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDescriptorModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeDescriptorModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeDescriptorModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HDescriptorModel Deserialize(Stream stream)
		{
			HDescriptorModel hDescriptorModel = new HDescriptorModel();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hDescriptorModel.DeserializeDescriptorModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDescriptorModel;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HDescriptorModel Clone()
		{
			HSerializedItem hSerializedItem = SerializeDescriptorModel();
			HDescriptorModel hDescriptorModel = new HDescriptorModel();
			hDescriptorModel.DeserializeDescriptorModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDescriptorModel;
		}

		public static void ClearDescriptorModel(HDescriptorModel[] modelID)
		{
			HTuple hTuple = HHandleBase.ConcatArray(modelID);
			IntPtr proc = HalconAPI.PreCall(943);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(modelID);
		}

		public void ClearDescriptorModel()
		{
			IntPtr proc = HalconAPI.PreCall(943);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeDescriptorModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(944);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeDescriptorModel()
		{
			IntPtr proc = HalconAPI.PreCall(945);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadDescriptorModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(946);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteDescriptorModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(947);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HPose[] FindCalibDescriptorModel(HImage image, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, HTuple minScore, int numMatches, HCamPar camParam, HTuple scoreType, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(948);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.Store(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.Store(proc, 7, camParam);
			HalconAPI.Store(proc, 8, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(scoreType);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out score);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(tuple);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HPose FindCalibDescriptorModel(HImage image, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, double minScore, int numMatches, HCamPar camParam, string scoreType, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(948);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.Store(proc, 7, camParam);
			HalconAPI.StoreS(proc, 8, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HalconAPI.UnpinTuple(camParam);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HalconAPI.LoadD(proc, 1, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HHomMat2D[] FindUncalibDescriptorModel(HImage image, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, HTuple minScore, int numMatches, HTuple scoreType, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(949);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.Store(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.Store(proc, 7, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(scoreType);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out score);
			HalconAPI.PostCall(proc, err);
			HHomMat2D[] result = HHomMat2D.SplitArray(tuple);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public HHomMat2D FindUncalibDescriptorModel(HImage image, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, double minScore, int numMatches, string scoreType, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(949);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.StoreS(proc, 7, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			err = HHomMat2D.LoadNew(proc, 0, err, out HHomMat2D obj);
			err = HalconAPI.LoadD(proc, 1, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void GetDescriptorModelPoints(string set, HTuple subset, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(950);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, set);
			HalconAPI.Store(proc, 2, subset);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subset);
			err = HTuple.LoadNew(proc, 0, err, out row);
			err = HTuple.LoadNew(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetDescriptorModelPoints(string set, int subset, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(950);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, set);
			HalconAPI.StoreI(proc, 2, subset);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out row);
			err = HTuple.LoadNew(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public string GetDescriptorModelParams(out HTuple detectorParamName, out HTuple detectorParamValue, out HTuple descriptorParamName, out HTuple descriptorParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(951);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HTuple.LoadNew(proc, 1, err, out detectorParamName);
			err = HTuple.LoadNew(proc, 2, err, out detectorParamValue);
			err = HTuple.LoadNew(proc, 3, err, out descriptorParamName);
			err = HTuple.LoadNew(proc, 4, err, out descriptorParamValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return stringValue;
		}

		public void CreateCalibDescriptorModel(HImage template, HCamPar camParam, HPose referencePose, string detectorType, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, int seed)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(952);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreS(proc, 2, detectorType);
			HalconAPI.Store(proc, 3, detectorParamName);
			HalconAPI.Store(proc, 4, detectorParamValue);
			HalconAPI.Store(proc, 5, descriptorParamName);
			HalconAPI.Store(proc, 6, descriptorParamValue);
			HalconAPI.StoreI(proc, 7, seed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public void CreateUncalibDescriptorModel(HImage template, string detectorType, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, int seed)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(953);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.StoreS(proc, 0, detectorType);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.StoreI(proc, 5, seed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
		}

		public HTuple GetDescriptorModelResults(HTuple objectID, string resultNames)
		{
			IntPtr proc = HalconAPI.PreCall(954);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectID);
			HalconAPI.StoreS(proc, 2, resultNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectID);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDescriptorModelResults(int objectID, string resultNames)
		{
			IntPtr proc = HalconAPI.PreCall(954);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, objectID);
			HalconAPI.StoreS(proc, 2, resultNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void GetDescriptorModelOrigin(out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(955);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out row);
			err = HTuple.LoadNew(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetDescriptorModelOrigin(out double row, out double column)
		{
			IntPtr proc = HalconAPI.PreCall(955);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SetDescriptorModelOrigin(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(956);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDescriptorModelOrigin(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(956);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
