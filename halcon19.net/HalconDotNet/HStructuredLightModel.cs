using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HStructuredLightModel : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HStructuredLightModel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HStructuredLightModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HStructuredLightModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("structured_light_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HStructuredLightModel obj)
		{
			obj = new HStructuredLightModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HStructuredLightModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HStructuredLightModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HStructuredLightModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HStructuredLightModel(string modelType)
		{
			IntPtr proc = HalconAPI.PreCall(2107);
			HalconAPI.StoreS(proc, 0, modelType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeStructuredLightModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HStructuredLightModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeStructuredLightModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeStructuredLightModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HStructuredLightModel Deserialize(Stream stream)
		{
			HStructuredLightModel hStructuredLightModel = new HStructuredLightModel();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hStructuredLightModel.DeserializeStructuredLightModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hStructuredLightModel;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HStructuredLightModel Clone()
		{
			HSerializedItem hSerializedItem = SerializeStructuredLightModel();
			HStructuredLightModel hStructuredLightModel = new HStructuredLightModel();
			hStructuredLightModel.DeserializeStructuredLightModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hStructuredLightModel;
		}

		public static void ClearStructuredLightModel(HStructuredLightModel[] structuredLightModel)
		{
			HTuple hTuple = HHandleBase.ConcatArray(structuredLightModel);
			IntPtr proc = HalconAPI.PreCall(2106);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(structuredLightModel);
		}

		public void ClearStructuredLightModel()
		{
			IntPtr proc = HalconAPI.PreCall(2106);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateStructuredLightModel(string modelType)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2107);
			HalconAPI.StoreS(proc, 0, modelType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DecodeStructuredLightPattern(HImage cameraImages)
		{
			IntPtr proc = HalconAPI.PreCall(2108);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraImages);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(cameraImages);
		}

		public void DeserializeStructuredLightModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2110);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HImage GenStructuredLightPattern()
		{
			IntPtr proc = HalconAPI.PreCall(2113);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetStructuredLightModelParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2117);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetStructuredLightModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2117);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HObject GetStructuredLightObject(HTuple objectName)
		{
			IntPtr proc = HalconAPI.PreCall(2118);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objectName);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HObject GetStructuredLightObject(string objectName)
		{
			IntPtr proc = HalconAPI.PreCall(2118);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadStructuredLightModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2123);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem SerializeStructuredLightModel()
		{
			IntPtr proc = HalconAPI.PreCall(2127);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SetStructuredLightModelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2130);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetStructuredLightModelParam(string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2130);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreI(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteStructuredLightModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(2133);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
