using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HDict : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDict(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDict(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("dict");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDict obj)
		{
			obj = new HDict(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDict[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDict[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HDict(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HDict()
		{
			IntPtr proc = HalconAPI.PreCall(2149);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDict(string fileName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2162);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDict(string fileName, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2162);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDict CopyDict(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2148);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = LoadNew(proc, 0, err, out HDict obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HDict CopyDict(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2148);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HDict obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void CreateDict()
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2149);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HObject GetDictObject(HTuple key)
		{
			IntPtr proc = HalconAPI.PreCall(2153);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, key);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HObject GetDictObject(string key)
		{
			IntPtr proc = HalconAPI.PreCall(2153);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, key);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetDictParam(string genParamName, HTuple key)
		{
			IntPtr proc = HalconAPI.PreCall(2154);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDictParam(string genParamName, string key)
		{
			IntPtr proc = HalconAPI.PreCall(2154);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDictTuple(HTuple key)
		{
			IntPtr proc = HalconAPI.PreCall(2155);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDictTuple(string key)
		{
			IntPtr proc = HalconAPI.PreCall(2155);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, key);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void ReadDict(string fileName, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2162);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ReadDict(string fileName, string genParamName, string genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2162);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void RemoveDictKey(HTuple key)
		{
			IntPtr proc = HalconAPI.PreCall(2165);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, key);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void RemoveDictKey(string key)
		{
			IntPtr proc = HalconAPI.PreCall(2165);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, key);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDictObject(HObject objectVal, HTuple key)
		{
			IntPtr proc = HalconAPI.PreCall(2169);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectVal);
			HalconAPI.Store(proc, 1, key);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectVal);
		}

		public void SetDictObject(HObject objectVal, string key)
		{
			IntPtr proc = HalconAPI.PreCall(2169);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, objectVal);
			HalconAPI.StoreS(proc, 1, key);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(objectVal);
		}

		public void SetDictTuple(HTuple key, HTuple tuple)
		{
			IntPtr proc = HalconAPI.PreCall(2170);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, key);
			HalconAPI.Store(proc, 2, tuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(key);
			HalconAPI.UnpinTuple(tuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDictTuple(string key, HTuple tuple)
		{
			IntPtr proc = HalconAPI.PreCall(2170);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, key);
			HalconAPI.Store(proc, 2, tuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(tuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteDict(string fileName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2173);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteDict(string fileName, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2173);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
