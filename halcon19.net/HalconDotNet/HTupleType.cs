namespace HalconDotNet
{
	public enum HTupleType
	{
		EMPTY = 0x1F,
		INTEGER = 1,
		LONG = 129,
		DOUBLE = 2,
		STRING = 4,
		HANDLE = 0x10,
		MIXED = 8
	}
}
