using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HClassLUT : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassLUT()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassLUT(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassLUT(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("class_lut");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassLUT obj)
		{
			obj = new HClassLUT(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassLUT[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HClassLUT[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HClassLUT(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HClassLUT(HClassKnn KNNHandle, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1819);
			HalconAPI.Store(proc, 0, KNNHandle);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(KNNHandle);
		}

		public HClassLUT(HClassGmm GMMHandle, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1820);
			HalconAPI.Store(proc, 0, GMMHandle);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(GMMHandle);
		}

		public HClassLUT(HClassSvm SVMHandle, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1821);
			HalconAPI.Store(proc, 0, SVMHandle);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SVMHandle);
		}

		public HClassLUT(HClassMlp MLPHandle, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1822);
			HalconAPI.Store(proc, 0, MLPHandle);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(MLPHandle);
		}

		public HRegion ClassifyImageClassLut(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(428);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public static void ClearClassLut(HClassLUT[] classLUTHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(classLUTHandle);
			IntPtr proc = HalconAPI.PreCall(1818);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(classLUTHandle);
		}

		public void ClearClassLut()
		{
			IntPtr proc = HalconAPI.PreCall(1818);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateClassLutKnn(HClassKnn KNNHandle, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1819);
			HalconAPI.Store(proc, 0, KNNHandle);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(KNNHandle);
		}

		public void CreateClassLutGmm(HClassGmm GMMHandle, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1820);
			HalconAPI.Store(proc, 0, GMMHandle);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(GMMHandle);
		}

		public void CreateClassLutSvm(HClassSvm SVMHandle, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1821);
			HalconAPI.Store(proc, 0, SVMHandle);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SVMHandle);
		}

		public void CreateClassLutMlp(HClassMlp MLPHandle, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1822);
			HalconAPI.Store(proc, 0, MLPHandle);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(MLPHandle);
		}
	}
}
