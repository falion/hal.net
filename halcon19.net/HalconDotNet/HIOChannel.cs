using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HIOChannel : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HIOChannel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HIOChannel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HIOChannel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("io_channel");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HIOChannel obj)
		{
			obj = new HIOChannel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HIOChannel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HIOChannel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HIOChannel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HIOChannel(HIODevice IODeviceHandle, string IOChannelName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2016);
			HalconAPI.Store(proc, 0, IODeviceHandle);
			HalconAPI.StoreS(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(IODeviceHandle);
		}

		public static HTuple ControlIoChannel(HIOChannel[] IOChannelHandle, string paramAction, HTuple paramArgument)
		{
			HTuple hTuple = HHandleBase.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2010);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.StoreS(proc, 1, paramAction);
			HalconAPI.Store(proc, 2, paramArgument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(paramArgument);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(IOChannelHandle);
			return tuple;
		}

		public HTuple ControlIoChannel(string paramAction, HTuple paramArgument)
		{
			IntPtr proc = HalconAPI.PreCall(2010);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, paramAction);
			HalconAPI.Store(proc, 2, paramArgument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(paramArgument);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public static HTuple WriteIoChannel(HIOChannel[] IOChannelHandle, HTuple value)
		{
			HTuple hTuple = HHandleBase.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2011);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(value);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(IOChannelHandle);
			return tuple;
		}

		public HTuple WriteIoChannel(HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(2011);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(value);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public static HTuple ReadIoChannel(HIOChannel[] IOChannelHandle, out HTuple status)
		{
			HTuple hTuple = HHandleBase.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2012);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out status);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(IOChannelHandle);
			return tuple;
		}

		public HTuple ReadIoChannel(out HTuple status)
		{
			IntPtr proc = HalconAPI.PreCall(2012);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out status);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public static void SetIoChannelParam(HIOChannel[] IOChannelHandle, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HHandleBase.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2013);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(IOChannelHandle);
		}

		public void SetIoChannelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2013);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static HTuple GetIoChannelParam(HIOChannel[] IOChannelHandle, HTuple genParamName)
		{
			HTuple hTuple = HHandleBase.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2014);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(IOChannelHandle);
			return tuple;
		}

		public HTuple GetIoChannelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2014);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public static void CloseIoChannel(HIOChannel[] IOChannelHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(IOChannelHandle);
			IntPtr proc = HalconAPI.PreCall(2015);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(IOChannelHandle);
		}

		public void CloseIoChannel()
		{
			IntPtr proc = HalconAPI.PreCall(2015);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static HIOChannel[] OpenIoChannel(HIODevice IODeviceHandle, HTuple IOChannelName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2016);
			HalconAPI.Store(proc, 0, IODeviceHandle);
			HalconAPI.Store(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(IOChannelName);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = LoadNew(proc, 0, err, out HIOChannel[] obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(IODeviceHandle);
			return obj;
		}

		public void OpenIoChannel(HIODevice IODeviceHandle, string IOChannelName, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2016);
			HalconAPI.Store(proc, 0, IODeviceHandle);
			HalconAPI.StoreS(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(IODeviceHandle);
		}
	}
}
