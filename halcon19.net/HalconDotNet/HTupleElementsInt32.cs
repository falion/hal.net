namespace HalconDotNet
{
	internal class HTupleElementsInt32 : HTupleElementsImplementation
	{
		internal HTupleElementsInt32(HTupleInt32 source, int index)
			: base(source, index)
		{
		}

		internal HTupleElementsInt32(HTupleInt32 source, int[] indices)
			: base(source, indices)
		{
		}

		public override int[] getI()
		{
			if (indices == null)
			{
				return null;
			}
			int[] array = new int[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				array[i] = source.IArr[indices[i]];
			}
			return array;
		}

		public override void setI(int[] i)
		{
			if (IsValidArrayForSetX(i))
			{
				bool flag = i.Length == 1;
				for (int j = 0; j < indices.Length; j++)
				{
					source.IArr[indices[j]] = i[(!flag) ? j : 0];
				}
			}
		}

		public override long[] getL()
		{
			if (indices == null)
			{
				return null;
			}
			long[] array = new long[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				array[i] = source.IArr[indices[i]];
			}
			return array;
		}

		public override void setL(long[] l)
		{
			if (IsValidArrayForSetX(l))
			{
				bool flag = l.Length == 1;
				for (int i = 0; i < indices.Length; i++)
				{
					source.IArr[indices[i]] = (int)l[(!flag) ? i : 0];
				}
			}
		}

		public override double[] getD()
		{
			if (indices == null)
			{
				return null;
			}
			double[] array = new double[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				array[i] = source.IArr[indices[i]];
			}
			return array;
		}

		public override object[] getO()
		{
			if (indices == null)
			{
				return null;
			}
			object[] array = new object[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				array[i] = source.IArr[indices[i]];
			}
			return array;
		}

		public override HTupleType getType()
		{
			return HTupleType.INTEGER;
		}
	}
}
