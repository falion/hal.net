using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HSerial : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSerial()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSerial(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSerial(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("serial");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSerial obj)
		{
			obj = new HSerial(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSerial[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HSerial[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HSerial(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HSerial(string portName)
		{
			IntPtr proc = HalconAPI.PreCall(314);
			HalconAPI.StoreS(proc, 0, portName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ClearSerial(string channel)
		{
			IntPtr proc = HalconAPI.PreCall(307);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, channel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteSerial(HTuple data)
		{
			IntPtr proc = HalconAPI.PreCall(308);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, data);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(data);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteSerial(int data)
		{
			IntPtr proc = HalconAPI.PreCall(308);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, data);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ReadSerial(int numCharacters)
		{
			IntPtr proc = HalconAPI.PreCall(309);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, numCharacters);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int GetSerialParam(out int dataBits, out string flowControl, out string parity, out int stopBits, out int totalTimeOut, out int interCharTimeOut)
		{
			IntPtr proc = HalconAPI.PreCall(310);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadI(proc, 1, err, out dataBits);
			err = HalconAPI.LoadS(proc, 2, err, out flowControl);
			err = HalconAPI.LoadS(proc, 3, err, out parity);
			err = HalconAPI.LoadI(proc, 4, err, out stopBits);
			err = HalconAPI.LoadI(proc, 5, err, out totalTimeOut);
			err = HalconAPI.LoadI(proc, 6, err, out interCharTimeOut);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public void SetSerialParam(HTuple baudRate, HTuple dataBits, string flowControl, string parity, HTuple stopBits, HTuple totalTimeOut, HTuple interCharTimeOut)
		{
			IntPtr proc = HalconAPI.PreCall(311);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, baudRate);
			HalconAPI.Store(proc, 2, dataBits);
			HalconAPI.StoreS(proc, 3, flowControl);
			HalconAPI.StoreS(proc, 4, parity);
			HalconAPI.Store(proc, 5, stopBits);
			HalconAPI.Store(proc, 6, totalTimeOut);
			HalconAPI.Store(proc, 7, interCharTimeOut);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(baudRate);
			HalconAPI.UnpinTuple(dataBits);
			HalconAPI.UnpinTuple(stopBits);
			HalconAPI.UnpinTuple(totalTimeOut);
			HalconAPI.UnpinTuple(interCharTimeOut);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetSerialParam(int baudRate, int dataBits, string flowControl, string parity, int stopBits, int totalTimeOut, int interCharTimeOut)
		{
			IntPtr proc = HalconAPI.PreCall(311);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, baudRate);
			HalconAPI.StoreI(proc, 2, dataBits);
			HalconAPI.StoreS(proc, 3, flowControl);
			HalconAPI.StoreS(proc, 4, parity);
			HalconAPI.StoreI(proc, 5, stopBits);
			HalconAPI.StoreI(proc, 6, totalTimeOut);
			HalconAPI.StoreI(proc, 7, interCharTimeOut);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CloseSerial()
		{
			IntPtr proc = HalconAPI.PreCall(313);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void OpenSerial(string portName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(314);
			HalconAPI.StoreS(proc, 0, portName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
