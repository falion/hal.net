namespace HalconDotNet
{
	internal class HDevInputParamVector : HDevInputParam
	{
		protected HVector mVector;

		public HDevInputParamVector(HVector vector)
		{
			mVector = vector.Clone();
		}

		public override HTupleVector GetCtrlParamVector()
		{
			return (HTupleVector)mVector;
		}

		public override HObjectVector GetIconicParamVector()
		{
			return (HObjectVector)mVector;
		}

		public override void Dispose()
		{
			if (mVector != null)
			{
				mVector.Dispose();
				mVector = null;
			}
		}
	}
}
