using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HDualQuaternion : HData, ISerializable, ICloneable
	{
		private const int FIXEDSIZE = 8;

		public HDualQuaternion()
		{
		}

		public HDualQuaternion(HTuple tuple)
			: base(tuple)
		{
		}

		internal HDualQuaternion(HData data)
			: base(data)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, HTupleType type, int err, out HDualQuaternion obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDualQuaternion(new HData(tuple));
			return err;
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDualQuaternion obj)
		{
			return LoadNew(proc, parIndex, HTupleType.MIXED, err, out obj);
		}

		internal static HDualQuaternion[] SplitArray(HTuple data)
		{
			int num = data.Length / 8;
			HDualQuaternion[] array = new HDualQuaternion[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = new HDualQuaternion(new HData(data.TupleSelectRange(i * 8, (i + 1) * 8 - 1)));
			}
			return array;
		}

		public HDualQuaternion(HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(2080);
			HalconAPI.Store(proc, 0, pose);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDualQuaternion(string screwFormat, HTuple axisDirectionX, HTuple axisDirectionY, HTuple axisDirectionZ, HTuple axisMomentOrPointX, HTuple axisMomentOrPointY, HTuple axisMomentOrPointZ, HTuple rotation, HTuple translation)
		{
			IntPtr proc = HalconAPI.PreCall(2086);
			HalconAPI.StoreS(proc, 0, screwFormat);
			HalconAPI.Store(proc, 1, axisDirectionX);
			HalconAPI.Store(proc, 2, axisDirectionY);
			HalconAPI.Store(proc, 3, axisDirectionZ);
			HalconAPI.Store(proc, 4, axisMomentOrPointX);
			HalconAPI.Store(proc, 5, axisMomentOrPointY);
			HalconAPI.Store(proc, 6, axisMomentOrPointZ);
			HalconAPI.Store(proc, 7, rotation);
			HalconAPI.Store(proc, 8, translation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(axisDirectionX);
			HalconAPI.UnpinTuple(axisDirectionY);
			HalconAPI.UnpinTuple(axisDirectionZ);
			HalconAPI.UnpinTuple(axisMomentOrPointX);
			HalconAPI.UnpinTuple(axisMomentOrPointY);
			HalconAPI.UnpinTuple(axisMomentOrPointZ);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(translation);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDualQuaternion(string screwFormat, double axisDirectionX, double axisDirectionY, double axisDirectionZ, double axisMomentOrPointX, double axisMomentOrPointY, double axisMomentOrPointZ, double rotation, double translation)
		{
			IntPtr proc = HalconAPI.PreCall(2086);
			HalconAPI.StoreS(proc, 0, screwFormat);
			HalconAPI.StoreD(proc, 1, axisDirectionX);
			HalconAPI.StoreD(proc, 2, axisDirectionY);
			HalconAPI.StoreD(proc, 3, axisDirectionZ);
			HalconAPI.StoreD(proc, 4, axisMomentOrPointX);
			HalconAPI.StoreD(proc, 5, axisMomentOrPointY);
			HalconAPI.StoreD(proc, 6, axisMomentOrPointZ);
			HalconAPI.StoreD(proc, 7, rotation);
			HalconAPI.StoreD(proc, 8, translation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeDualQuat();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDualQuaternion(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeDualQuat(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeDualQuat();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public static HDualQuaternion Deserialize(Stream stream)
		{
			HDualQuaternion hDualQuaternion = new HDualQuaternion();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hDualQuaternion.DeserializeDualQuat(hSerializedItem);
			hSerializedItem.Dispose();
			return hDualQuaternion;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public HDualQuaternion Clone()
		{
			HSerializedItem hSerializedItem = SerializeDualQuat();
			HDualQuaternion hDualQuaternion = new HDualQuaternion();
			hDualQuaternion.DeserializeDualQuat(hSerializedItem);
			hSerializedItem.Dispose();
			return hDualQuaternion;
		}

		public static HDualQuaternion operator *(HDualQuaternion left, HDualQuaternion right)
		{
			return left.DualQuatCompose(right);
		}

		public static implicit operator HPose(HDualQuaternion dualQuaternion)
		{
			return dualQuaternion.DualQuatToPose();
		}

		public static implicit operator HHomMat3D(HDualQuaternion dualQuaternion)
		{
			return dualQuaternion.DualQuatToHomMat3d();
		}

		public static HDualQuaternion operator ~(HDualQuaternion dualQuaternion)
		{
			return dualQuaternion.DualQuatConjugate();
		}

		public HDualQuaternion(double realW, double realX, double realY, double realZ, double dualW, double dualX, double dualY, double dualZ)
			: base(new HTuple(realW, realX, realY, realZ, dualW, dualX, dualY, dualZ))
		{
		}

		public HDualQuaternion(HQuaternion quat1, HQuaternion quat2)
			: base(quat1.RawData.TupleConcat(quat2.RawData))
		{
		}

		public void DeserializeDualQuat(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(2052);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public static HDualQuaternion[] DualQuatCompose(HDualQuaternion[] dualQuaternionLeft, HDualQuaternion[] dualQuaternionRight)
		{
			HTuple hTuple = HData.ConcatArray(dualQuaternionLeft);
			HTuple hTuple2 = HData.ConcatArray(dualQuaternionRight);
			IntPtr proc = HalconAPI.PreCall(2059);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 1, hTuple2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return SplitArray(tuple);
		}

		public HDualQuaternion DualQuatCompose(HDualQuaternion dualQuaternionRight)
		{
			IntPtr proc = HalconAPI.PreCall(2059);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, dualQuaternionRight);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(dualQuaternionRight);
			err = LoadNew(proc, 0, err, out HDualQuaternion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public static HDualQuaternion[] DualQuatConjugate(HDualQuaternion[] dualQuaternion)
		{
			HTuple hTuple = HData.ConcatArray(dualQuaternion);
			IntPtr proc = HalconAPI.PreCall(2060);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return SplitArray(tuple);
		}

		public HDualQuaternion DualQuatConjugate()
		{
			IntPtr proc = HalconAPI.PreCall(2060);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HDualQuaternion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HDualQuaternion[] DualQuatInterpolate(HDualQuaternion dualQuaternionEnd, HTuple interpPos)
		{
			IntPtr proc = HalconAPI.PreCall(2061);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, dualQuaternionEnd);
			HalconAPI.Store(proc, 2, interpPos);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(dualQuaternionEnd);
			HalconAPI.UnpinTuple(interpPos);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			HDualQuaternion[] result = SplitArray(tuple);
			GC.KeepAlive(this);
			return result;
		}

		public HDualQuaternion DualQuatInterpolate(HDualQuaternion dualQuaternionEnd, double interpPos)
		{
			IntPtr proc = HalconAPI.PreCall(2061);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, dualQuaternionEnd);
			HalconAPI.StoreD(proc, 2, interpPos);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(dualQuaternionEnd);
			err = LoadNew(proc, 0, err, out HDualQuaternion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public static HDualQuaternion[] DualQuatNormalize(HDualQuaternion[] dualQuaternion)
		{
			HTuple hTuple = HData.ConcatArray(dualQuaternion);
			IntPtr proc = HalconAPI.PreCall(2062);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return SplitArray(tuple);
		}

		public HDualQuaternion DualQuatNormalize()
		{
			IntPtr proc = HalconAPI.PreCall(2062);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HDualQuaternion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HHomMat3D DualQuatToHomMat3d()
		{
			IntPtr proc = HalconAPI.PreCall(2063);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HHomMat3D.LoadNew(proc, 0, err, out HHomMat3D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public static HPose[] DualQuatToPose(HDualQuaternion[] dualQuaternion)
		{
			HTuple hTuple = HData.ConcatArray(dualQuaternion);
			IntPtr proc = HalconAPI.PreCall(2064);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return HPose.SplitArray(tuple);
		}

		public HPose DualQuatToPose()
		{
			IntPtr proc = HalconAPI.PreCall(2064);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void DualQuatToScrew(string screwFormat, out double axisDirectionX, out double axisDirectionY, out double axisDirectionZ, out double axisMomentOrPointX, out double axisMomentOrPointY, out double axisMomentOrPointZ, out double rotation, out double translation)
		{
			IntPtr proc = HalconAPI.PreCall(2065);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, screwFormat);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HalconAPI.LoadD(proc, 0, err, out axisDirectionX);
			err = HalconAPI.LoadD(proc, 1, err, out axisDirectionY);
			err = HalconAPI.LoadD(proc, 2, err, out axisDirectionZ);
			err = HalconAPI.LoadD(proc, 3, err, out axisMomentOrPointX);
			err = HalconAPI.LoadD(proc, 4, err, out axisMomentOrPointY);
			err = HalconAPI.LoadD(proc, 5, err, out axisMomentOrPointZ);
			err = HalconAPI.LoadD(proc, 6, err, out rotation);
			err = HalconAPI.LoadD(proc, 7, err, out translation);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DualQuatTransLine3d(string lineFormat, HTuple lineDirectionX, HTuple lineDirectionY, HTuple lineDirectionZ, HTuple lineMomentOrPointX, HTuple lineMomentOrPointY, HTuple lineMomentOrPointZ, out HTuple transLineDirectionX, out HTuple transLineDirectionY, out HTuple transLineDirectionZ, out HTuple transLineMomentOrPointX, out HTuple transLineMomentOrPointY, out HTuple transLineMomentOrPointZ)
		{
			IntPtr proc = HalconAPI.PreCall(2066);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, lineFormat);
			HalconAPI.Store(proc, 2, lineDirectionX);
			HalconAPI.Store(proc, 3, lineDirectionY);
			HalconAPI.Store(proc, 4, lineDirectionZ);
			HalconAPI.Store(proc, 5, lineMomentOrPointX);
			HalconAPI.Store(proc, 6, lineMomentOrPointY);
			HalconAPI.Store(proc, 7, lineMomentOrPointZ);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(lineDirectionX);
			HalconAPI.UnpinTuple(lineDirectionY);
			HalconAPI.UnpinTuple(lineDirectionZ);
			HalconAPI.UnpinTuple(lineMomentOrPointX);
			HalconAPI.UnpinTuple(lineMomentOrPointY);
			HalconAPI.UnpinTuple(lineMomentOrPointZ);
			err = HTuple.LoadNew(proc, 0, err, out transLineDirectionX);
			err = HTuple.LoadNew(proc, 1, err, out transLineDirectionY);
			err = HTuple.LoadNew(proc, 2, err, out transLineDirectionZ);
			err = HTuple.LoadNew(proc, 3, err, out transLineMomentOrPointX);
			err = HTuple.LoadNew(proc, 4, err, out transLineMomentOrPointY);
			err = HTuple.LoadNew(proc, 5, err, out transLineMomentOrPointZ);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DualQuatTransLine3d(string lineFormat, double lineDirectionX, double lineDirectionY, double lineDirectionZ, double lineMomentOrPointX, double lineMomentOrPointY, double lineMomentOrPointZ, out double transLineDirectionX, out double transLineDirectionY, out double transLineDirectionZ, out double transLineMomentOrPointX, out double transLineMomentOrPointY, out double transLineMomentOrPointZ)
		{
			IntPtr proc = HalconAPI.PreCall(2066);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, lineFormat);
			HalconAPI.StoreD(proc, 2, lineDirectionX);
			HalconAPI.StoreD(proc, 3, lineDirectionY);
			HalconAPI.StoreD(proc, 4, lineDirectionZ);
			HalconAPI.StoreD(proc, 5, lineMomentOrPointX);
			HalconAPI.StoreD(proc, 6, lineMomentOrPointY);
			HalconAPI.StoreD(proc, 7, lineMomentOrPointZ);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HalconAPI.LoadD(proc, 0, err, out transLineDirectionX);
			err = HalconAPI.LoadD(proc, 1, err, out transLineDirectionY);
			err = HalconAPI.LoadD(proc, 2, err, out transLineDirectionZ);
			err = HalconAPI.LoadD(proc, 3, err, out transLineMomentOrPointX);
			err = HalconAPI.LoadD(proc, 4, err, out transLineMomentOrPointY);
			err = HalconAPI.LoadD(proc, 5, err, out transLineMomentOrPointZ);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HDualQuaternion[] PoseToDualQuat(HPose[] pose)
		{
			HTuple hTuple = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(2080);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return SplitArray(tuple);
		}

		public void PoseToDualQuat(HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(2080);
			HalconAPI.Store(proc, 0, pose);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ScrewToDualQuat(string screwFormat, HTuple axisDirectionX, HTuple axisDirectionY, HTuple axisDirectionZ, HTuple axisMomentOrPointX, HTuple axisMomentOrPointY, HTuple axisMomentOrPointZ, HTuple rotation, HTuple translation)
		{
			IntPtr proc = HalconAPI.PreCall(2086);
			HalconAPI.StoreS(proc, 0, screwFormat);
			HalconAPI.Store(proc, 1, axisDirectionX);
			HalconAPI.Store(proc, 2, axisDirectionY);
			HalconAPI.Store(proc, 3, axisDirectionZ);
			HalconAPI.Store(proc, 4, axisMomentOrPointX);
			HalconAPI.Store(proc, 5, axisMomentOrPointY);
			HalconAPI.Store(proc, 6, axisMomentOrPointZ);
			HalconAPI.Store(proc, 7, rotation);
			HalconAPI.Store(proc, 8, translation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(axisDirectionX);
			HalconAPI.UnpinTuple(axisDirectionY);
			HalconAPI.UnpinTuple(axisDirectionZ);
			HalconAPI.UnpinTuple(axisMomentOrPointX);
			HalconAPI.UnpinTuple(axisMomentOrPointY);
			HalconAPI.UnpinTuple(axisMomentOrPointZ);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(translation);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ScrewToDualQuat(string screwFormat, double axisDirectionX, double axisDirectionY, double axisDirectionZ, double axisMomentOrPointX, double axisMomentOrPointY, double axisMomentOrPointZ, double rotation, double translation)
		{
			IntPtr proc = HalconAPI.PreCall(2086);
			HalconAPI.StoreS(proc, 0, screwFormat);
			HalconAPI.StoreD(proc, 1, axisDirectionX);
			HalconAPI.StoreD(proc, 2, axisDirectionY);
			HalconAPI.StoreD(proc, 3, axisDirectionZ);
			HalconAPI.StoreD(proc, 4, axisMomentOrPointX);
			HalconAPI.StoreD(proc, 5, axisMomentOrPointY);
			HalconAPI.StoreD(proc, 6, axisMomentOrPointZ);
			HalconAPI.StoreD(proc, 7, rotation);
			HalconAPI.StoreD(proc, 8, translation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem SerializeDualQuat()
		{
			IntPtr proc = HalconAPI.PreCall(2092);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}
	}
}
