using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HOCRBox : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRBox()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRBox(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRBox(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("ocr_box");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRBox obj)
		{
			obj = new HOCRBox(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRBox[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HOCRBox[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HOCRBox(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HOCRBox(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(712);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCRBox(int widthPattern, int heightPattern, int interpolation, HTuple features, HTuple character)
		{
			IntPtr proc = HalconAPI.PreCall(716);
			HalconAPI.StoreI(proc, 0, widthPattern);
			HalconAPI.StoreI(proc, 1, heightPattern);
			HalconAPI.StoreI(proc, 2, interpolation);
			HalconAPI.Store(proc, 3, features);
			HalconAPI.Store(proc, 4, character);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(character);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCRBox(int widthPattern, int heightPattern, int interpolation, string features, HTuple character)
		{
			IntPtr proc = HalconAPI.PreCall(716);
			HalconAPI.StoreI(proc, 0, widthPattern);
			HalconAPI.StoreI(proc, 1, heightPattern);
			HalconAPI.StoreI(proc, 2, interpolation);
			HalconAPI.StoreS(proc, 3, features);
			HalconAPI.Store(proc, 4, character);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(character);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeOcr();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRBox(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeOcr(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeOcr();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HOCRBox Deserialize(Stream stream)
		{
			HOCRBox hOCRBox = new HOCRBox();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hOCRBox.DeserializeOcr(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRBox;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HOCRBox Clone()
		{
			HSerializedItem hSerializedItem = SerializeOcr();
			HOCRBox hOCRBox = new HOCRBox();
			hOCRBox.DeserializeOcr(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRBox;
		}

		public HSerializedItem SerializeOcr()
		{
			IntPtr proc = HalconAPI.PreCall(709);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void DeserializeOcr(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(710);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public void WriteOcr(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(711);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadOcr(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(712);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DoOcrSingle(HRegion character, HImage image, out HTuple confidences)
		{
			IntPtr proc = HalconAPI.PreCall(713);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidences);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple DoOcrMulti(HRegion character, HImage image, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(714);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DoOcrMulti(HRegion character, HImage image, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(714);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return stringValue;
		}

		public void InfoOcrClassBox(out int widthPattern, out int heightPattern, out int interpolation, out int widthMaxChar, out int heightMaxChar, out HTuple features, out HTuple characters)
		{
			IntPtr proc = HalconAPI.PreCall(715);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out widthPattern);
			err = HalconAPI.LoadI(proc, 1, err, out heightPattern);
			err = HalconAPI.LoadI(proc, 2, err, out interpolation);
			err = HalconAPI.LoadI(proc, 3, err, out widthMaxChar);
			err = HalconAPI.LoadI(proc, 4, err, out heightMaxChar);
			err = HTuple.LoadNew(proc, 5, err, out features);
			err = HTuple.LoadNew(proc, 6, err, out characters);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcrClassBox(int widthPattern, int heightPattern, int interpolation, HTuple features, HTuple character)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(716);
			HalconAPI.StoreI(proc, 0, widthPattern);
			HalconAPI.StoreI(proc, 1, heightPattern);
			HalconAPI.StoreI(proc, 2, interpolation);
			HalconAPI.Store(proc, 3, features);
			HalconAPI.Store(proc, 4, character);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(character);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcrClassBox(int widthPattern, int heightPattern, int interpolation, string features, HTuple character)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(716);
			HalconAPI.StoreI(proc, 0, widthPattern);
			HalconAPI.StoreI(proc, 1, heightPattern);
			HalconAPI.StoreI(proc, 2, interpolation);
			HalconAPI.StoreS(proc, 3, features);
			HalconAPI.Store(proc, 4, character);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(character);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public double TraindOcrClassBox(HRegion character, HImage image, HTuple classVal)
		{
			IntPtr proc = HalconAPI.PreCall(717);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(classVal);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public double TraindOcrClassBox(HRegion character, HImage image, string classVal)
		{
			IntPtr proc = HalconAPI.PreCall(717);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public double TrainfOcrClassBox(HTuple trainingFile)
		{
			IntPtr proc = HalconAPI.PreCall(718);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, trainingFile);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public double TrainfOcrClassBox(string trainingFile)
		{
			IntPtr proc = HalconAPI.PreCall(718);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, trainingFile);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public void OcrChangeChar(HTuple character)
		{
			IntPtr proc = HalconAPI.PreCall(721);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(character);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CloseOcr()
		{
			IntPtr proc = HalconAPI.PreCall(722);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple TestdOcrClassBox(HRegion character, HImage image, HTuple classVal)
		{
			IntPtr proc = HalconAPI.PreCall(725);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(classVal);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public double TestdOcrClassBox(HRegion character, HImage image, string classVal)
		{
			IntPtr proc = HalconAPI.PreCall(725);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public HTuple OcrGetFeatures(HImage character)
		{
			IntPtr proc = HalconAPI.PreCall(727);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			return tuple;
		}
	}
}
