using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HMatrix : HHandle, ISerializable, ICloneable
	{
		public double this[int row, int column]
		{
			get
			{
				return GetValueMatrix(row, column);
			}
			set
			{
				SetValueMatrix(row, column, value);
			}
		}

		public int NumRows
		{
			get
			{
				GetSizeMatrix(out int rows, out int _);
				return rows;
			}
		}

		public int NumColumns
		{
			get
			{
				GetSizeMatrix(out int _, out int columns);
				return columns;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMatrix()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMatrix(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMatrix(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("matrix");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMatrix obj)
		{
			obj = new HMatrix(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMatrix[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HMatrix[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HMatrix(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HMatrix(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(842);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMatrix(int rows, int columns, HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(897);
			HalconAPI.StoreI(proc, 0, rows);
			HalconAPI.StoreI(proc, 1, columns);
			HalconAPI.Store(proc, 2, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(value);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMatrix(int rows, int columns, double value)
		{
			IntPtr proc = HalconAPI.PreCall(897);
			HalconAPI.StoreI(proc, 0, rows);
			HalconAPI.StoreI(proc, 1, columns);
			HalconAPI.StoreD(proc, 2, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeMatrix();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMatrix(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeMatrix(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeMatrix();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HMatrix Deserialize(Stream stream)
		{
			HMatrix hMatrix = new HMatrix();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hMatrix.DeserializeMatrix(hSerializedItem);
			hSerializedItem.Dispose();
			return hMatrix;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HMatrix Clone()
		{
			HSerializedItem hSerializedItem = SerializeMatrix();
			HMatrix hMatrix = new HMatrix();
			hMatrix.DeserializeMatrix(hSerializedItem);
			hSerializedItem.Dispose();
			return hMatrix;
		}

		public static HMatrix operator -(HMatrix matrix)
		{
			return matrix.ScaleMatrix(-1.0);
		}

		public static HMatrix operator +(HMatrix matrix1, HMatrix matrix2)
		{
			return matrix1.AddMatrix(matrix2);
		}

		public static HMatrix operator -(HMatrix matrix1, HMatrix matrix2)
		{
			return matrix1.SubMatrix(matrix2);
		}

		public static HMatrix operator *(HMatrix matrix1, HMatrix matrix2)
		{
			return matrix1.MultMatrix(matrix2, "AB");
		}

		public static HMatrix operator *(double factor, HMatrix matrix)
		{
			return matrix.ScaleMatrix(factor);
		}

		public static HMatrix operator *(HMatrix matrix, double factor)
		{
			return matrix.ScaleMatrix(factor);
		}

		public static HMatrix operator /(HMatrix matrix1, HMatrix matrix2)
		{
			return matrix2.SolveMatrix("general", 0.0, matrix1);
		}

		public void DeserializeMatrix(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(840);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(841);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadMatrix(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(842);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteMatrix(string fileFormat, string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(843);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileFormat);
			HalconAPI.StoreS(proc, 2, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix OrthogonalDecomposeMatrix(string decompositionType, string outputMatricesType, string computeOrthogonal, out HMatrix matrixTriangularID)
		{
			IntPtr proc = HalconAPI.PreCall(844);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, decompositionType);
			HalconAPI.StoreS(proc, 2, outputMatricesType);
			HalconAPI.StoreS(proc, 3, computeOrthogonal);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			err = LoadNew(proc, 1, err, out matrixTriangularID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HMatrix DecomposeMatrix(string matrixType, out HMatrix matrix2ID)
		{
			IntPtr proc = HalconAPI.PreCall(845);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			err = LoadNew(proc, 1, err, out matrix2ID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HMatrix SvdMatrix(string SVDType, string computeSingularVectors, out HMatrix matrixSID, out HMatrix matrixVID)
		{
			IntPtr proc = HalconAPI.PreCall(846);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, SVDType);
			HalconAPI.StoreS(proc, 2, computeSingularVectors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			err = LoadNew(proc, 1, err, out matrixSID);
			err = LoadNew(proc, 2, err, out matrixVID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void GeneralizedEigenvaluesGeneralMatrix(HMatrix matrixBID, string computeEigenvectors, out HMatrix eigenvaluesRealID, out HMatrix eigenvaluesImagID, out HMatrix eigenvectorsRealID, out HMatrix eigenvectorsImagID)
		{
			IntPtr proc = HalconAPI.PreCall(847);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.StoreS(proc, 2, computeEigenvectors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out eigenvaluesRealID);
			err = LoadNew(proc, 1, err, out eigenvaluesImagID);
			err = LoadNew(proc, 2, err, out eigenvectorsRealID);
			err = LoadNew(proc, 3, err, out eigenvectorsImagID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix GeneralizedEigenvaluesSymmetricMatrix(HMatrix matrixBID, string computeEigenvectors, out HMatrix eigenvectorsID)
		{
			IntPtr proc = HalconAPI.PreCall(848);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.StoreS(proc, 2, computeEigenvectors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			err = LoadNew(proc, 1, err, out eigenvectorsID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return obj;
		}

		public void EigenvaluesGeneralMatrix(string computeEigenvectors, out HMatrix eigenvaluesRealID, out HMatrix eigenvaluesImagID, out HMatrix eigenvectorsRealID, out HMatrix eigenvectorsImagID)
		{
			IntPtr proc = HalconAPI.PreCall(849);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, computeEigenvectors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out eigenvaluesRealID);
			err = LoadNew(proc, 1, err, out eigenvaluesImagID);
			err = LoadNew(proc, 2, err, out eigenvectorsRealID);
			err = LoadNew(proc, 3, err, out eigenvectorsImagID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMatrix EigenvaluesSymmetricMatrix(string computeEigenvectors, out HMatrix eigenvectorsID)
		{
			IntPtr proc = HalconAPI.PreCall(850);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, computeEigenvectors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			err = LoadNew(proc, 1, err, out eigenvectorsID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HMatrix SolveMatrix(string matrixLHSType, double epsilon, HMatrix matrixRHSID)
		{
			IntPtr proc = HalconAPI.PreCall(851);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixLHSType);
			HalconAPI.StoreD(proc, 2, epsilon);
			HalconAPI.Store(proc, 3, matrixRHSID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixRHSID);
			return obj;
		}

		public double DeterminantMatrix(string matrixType)
		{
			IntPtr proc = HalconAPI.PreCall(852);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public void InvertMatrixMod(string matrixType, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(853);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.StoreD(proc, 2, epsilon);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix InvertMatrix(string matrixType, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(854);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.StoreD(proc, 2, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void TransposeMatrixMod()
		{
			IntPtr proc = HalconAPI.PreCall(855);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix TransposeMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(856);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HMatrix MaxMatrix(string maxType)
		{
			IntPtr proc = HalconAPI.PreCall(857);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, maxType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HMatrix MinMatrix(string minType)
		{
			IntPtr proc = HalconAPI.PreCall(858);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, minType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void PowMatrixMod(string matrixType, HTuple power)
		{
			IntPtr proc = HalconAPI.PreCall(859);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.Store(proc, 2, power);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(power);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void PowMatrixMod(string matrixType, double power)
		{
			IntPtr proc = HalconAPI.PreCall(859);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.StoreD(proc, 2, power);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix PowMatrix(string matrixType, HTuple power)
		{
			IntPtr proc = HalconAPI.PreCall(860);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.Store(proc, 2, power);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(power);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HMatrix PowMatrix(string matrixType, double power)
		{
			IntPtr proc = HalconAPI.PreCall(860);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, matrixType);
			HalconAPI.StoreD(proc, 2, power);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void PowElementMatrixMod(HMatrix matrixExpID)
		{
			IntPtr proc = HalconAPI.PreCall(861);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixExpID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixExpID);
		}

		public HMatrix PowElementMatrix(HMatrix matrixExpID)
		{
			IntPtr proc = HalconAPI.PreCall(862);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixExpID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixExpID);
			return obj;
		}

		public void PowScalarElementMatrixMod(HTuple power)
		{
			IntPtr proc = HalconAPI.PreCall(863);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, power);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(power);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void PowScalarElementMatrixMod(double power)
		{
			IntPtr proc = HalconAPI.PreCall(863);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, power);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix PowScalarElementMatrix(HTuple power)
		{
			IntPtr proc = HalconAPI.PreCall(864);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, power);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(power);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HMatrix PowScalarElementMatrix(double power)
		{
			IntPtr proc = HalconAPI.PreCall(864);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, power);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SqrtMatrixMod()
		{
			IntPtr proc = HalconAPI.PreCall(865);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix SqrtMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(866);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void AbsMatrixMod()
		{
			IntPtr proc = HalconAPI.PreCall(867);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix AbsMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(868);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public double NormMatrix(string normType)
		{
			IntPtr proc = HalconAPI.PreCall(869);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, normType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HMatrix MeanMatrix(string meanType)
		{
			IntPtr proc = HalconAPI.PreCall(870);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, meanType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HMatrix SumMatrix(string sumType)
		{
			IntPtr proc = HalconAPI.PreCall(871);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, sumType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void DivElementMatrixMod(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(872);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix DivElementMatrix(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(873);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return obj;
		}

		public void MultElementMatrixMod(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(874);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix MultElementMatrix(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(875);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return obj;
		}

		public void ScaleMatrixMod(HTuple factor)
		{
			IntPtr proc = HalconAPI.PreCall(876);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, factor);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(factor);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ScaleMatrixMod(double factor)
		{
			IntPtr proc = HalconAPI.PreCall(876);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, factor);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HMatrix ScaleMatrix(HTuple factor)
		{
			IntPtr proc = HalconAPI.PreCall(877);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, factor);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(factor);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HMatrix ScaleMatrix(double factor)
		{
			IntPtr proc = HalconAPI.PreCall(877);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, factor);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SubMatrixMod(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(878);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix SubMatrix(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(879);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return obj;
		}

		public void AddMatrixMod(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(880);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix AddMatrix(HMatrix matrixBID)
		{
			IntPtr proc = HalconAPI.PreCall(881);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return obj;
		}

		public void MultMatrixMod(HMatrix matrixBID, string multType)
		{
			IntPtr proc = HalconAPI.PreCall(882);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.StoreS(proc, 2, multType);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
		}

		public HMatrix MultMatrix(HMatrix matrixBID, string multType)
		{
			IntPtr proc = HalconAPI.PreCall(883);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixBID);
			HalconAPI.StoreS(proc, 2, multType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixBID);
			return obj;
		}

		public void GetSizeMatrix(out int rows, out int columns)
		{
			IntPtr proc = HalconAPI.PreCall(884);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out rows);
			err = HalconAPI.LoadI(proc, 1, err, out columns);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMatrix RepeatMatrix(int rows, int columns)
		{
			IntPtr proc = HalconAPI.PreCall(885);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, rows);
			HalconAPI.StoreI(proc, 2, columns);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HMatrix CopyMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(886);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SetDiagonalMatrix(HMatrix vectorID, int diagonal)
		{
			IntPtr proc = HalconAPI.PreCall(887);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, vectorID);
			HalconAPI.StoreI(proc, 2, diagonal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(vectorID);
		}

		public HMatrix GetDiagonalMatrix(int diagonal)
		{
			IntPtr proc = HalconAPI.PreCall(888);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, diagonal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SetSubMatrix(HMatrix matrixSubID, int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(889);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, matrixSubID);
			HalconAPI.StoreI(proc, 2, row);
			HalconAPI.StoreI(proc, 3, column);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(matrixSubID);
		}

		public HMatrix GetSubMatrix(int row, int column, int rowsSub, int columnsSub)
		{
			IntPtr proc = HalconAPI.PreCall(890);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, rowsSub);
			HalconAPI.StoreI(proc, 4, columnsSub);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HMatrix obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SetFullMatrix(HTuple values)
		{
			IntPtr proc = HalconAPI.PreCall(891);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, values);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(values);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetFullMatrix(double values)
		{
			IntPtr proc = HalconAPI.PreCall(891);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, values);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetFullMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(892);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetValueMatrix(HTuple row, HTuple column, HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(893);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(value);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetValueMatrix(int row, int column, double value)
		{
			IntPtr proc = HalconAPI.PreCall(893);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreD(proc, 3, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetValueMatrix(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(894);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double GetValueMatrix(int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(894);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public static void ClearMatrix(HMatrix[] matrixID)
		{
			HTuple hTuple = HHandleBase.ConcatArray(matrixID);
			IntPtr proc = HalconAPI.PreCall(896);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(matrixID);
		}

		public void ClearMatrix()
		{
			IntPtr proc = HalconAPI.PreCall(896);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateMatrix(int rows, int columns, HTuple value)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(897);
			HalconAPI.StoreI(proc, 0, rows);
			HalconAPI.StoreI(proc, 1, columns);
			HalconAPI.Store(proc, 2, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(value);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateMatrix(int rows, int columns, double value)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(897);
			HalconAPI.StoreI(proc, 0, rows);
			HalconAPI.StoreI(proc, 1, columns);
			HalconAPI.StoreD(proc, 2, value);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
