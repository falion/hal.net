using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace HalconDotNet
{
	[ToolboxBitmap(typeof(HWindowControl))]
	public class HSmartWindowControl : UserControl
	{
		public delegate void HErrorHandler(HalconException he);

		public enum DrawingObjectsModifier
		{
			None,
			Shift,
			Ctrl,
			Alt
		}

		public enum ZoomContent
		{
			Off,
			WheelForwardZoomsIn,
			WheelBackwardZoomsIn
		}

		private const string positionDescription = " The position is returned in the image coordinate system.";

		private HWindow _hwindow;

		private Point _last_position = new Point(0, 0);

		private HObject _netimg = new HObject();

		private Size _prevsize = default(Size);

		private HTuple _dump_params;

		private bool _left_button_down;

		private Rectangle _part = new Rectangle(0, 0, 640, 480);

		private DrawingObjectsModifier _drawingObjectsModifier;

		private bool _automove = true;

		private bool _keepaspectratio = true;

		private ZoomContent _zooming = ZoomContent.WheelForwardZoomsIn;

		private bool _resetpart = true;

		private IContainer components;

		private PictureBox WindowFrame;

		protected override Size DefaultSize => new Size(320, 240);

		[Browsable(false)]
		public HWindow HalconWindow
		{
			get
			{
				if (_hwindow == null && base.Width > 0 && base.Height > 0)
				{
					CreateHWindow();
				}
				return _hwindow;
			}
		}

		[Browsable(false)]
		public IntPtr HalconID
		{
			get
			{
				if (_hwindow != null)
				{
					return _hwindow.Handle;
				}
				return IntPtr.Zero;
			}
		}

		private static bool RunningInDesignerMode
		{
			get
			{
				bool flag = LicenseManager.UsageMode == LicenseUsageMode.Designtime;
				if (!flag)
				{
					using (Process process = Process.GetCurrentProcess())
					{
						string fileDescription = process.MainModule.FileVersionInfo.FileDescription;
						if (fileDescription != null && fileDescription.ToLowerInvariant().Contains("microsoft visual studio"))
						{
							return true;
						}
						return process.ProcessName.ToLowerInvariant().Contains("devenv");
					}
				}
				return flag;
			}
		}

		[Description("Size of the HALCON window in pixels.")]
		[Category("Layout")]
		public Size WindowSize
		{
			get
			{
				if (RunningInDesignerMode || _hwindow == null)
				{
					return base.Size;
				}
				_hwindow.GetWindowExtents(out int _, out int _, out int width, out int height);
				return new Size(width, height);
			}
			set
			{
				if (value.Width > 0 && value.Height > 0)
				{
					base.Size = new Size(value.Width, value.Height);
				}
			}
		}

		[Description("Visible image part (Column, Row, Width, Height).")]
		[Category("Layout")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public Rectangle HImagePart
		{
			get
			{
				if (_hwindow != null)
				{
					_hwindow.GetPart(out int row, out int column, out int row2, out int column2);
					return new Rectangle(column, row, column2 - column + 1, row2 - row + 1);
				}
				return _part;
			}
			set
			{
				if (RunningInDesignerMode)
				{
					_part = value;
				}
				else if (value.Right > 0 && value.Width > 0)
				{
					if (_hwindow != null)
					{
						try
						{
							_hwindow.SetPart(value.Top, value.Left, value.Top + value.Height - 1, value.Left + value.Width - 1);
							_part = value;
						}
						catch (HalconException)
						{
						}
					}
					else
					{
						_part = value;
					}
				}
			}
		}

		[Category("Behavior")]
		[Description("Modifier key to interact with drawing objects. If a modifier key is selected, the user can only interact with drawing objects while keeping the modifier key pressed. This is especially useful when interacting with XLD drawing objects.")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public DrawingObjectsModifier HDrawingObjectsModifier
		{
			get
			{
				return _drawingObjectsModifier;
			}
			set
			{
				_drawingObjectsModifier = value;
			}
		}

		[Description("If on, the content of the HSmartWindowControl is moved when the mouse pointer is dragged.")]
		[Category("Behavior")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public bool HMoveContent
		{
			get
			{
				return _automove;
			}
			set
			{
				_automove = value;
			}
		}

		[Category("Behavior")]
		[Description("If on, the content of the HSmartWindowControl keeps its aspect ratio when the control is resized or zoomed.")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public bool HKeepAspectRatio
		{
			get
			{
				return _keepaspectratio;
			}
			set
			{
				_keepaspectratio = value;
			}
		}

		[Description("Controls the behavior of the mouse wheel.")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Category("Behavior")]
		public ZoomContent HZoomContent
		{
			get
			{
				return _zooming;
			}
			set
			{
				_zooming = value;
			}
		}

		[Description("If on, double clicking resizes the content of the HSmartWindowControl to fit the size of the control. ")]
		[Category("Behavior")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public bool HDoubleClickToFitContent
		{
			get
			{
				return _resetpart;
			}
			set
			{
				_resetpart = value;
			}
		}

		[Category("Mouse")]
		[Description("Occurs when the mouse is moved over the HALCON window. Note that delta is meaningless here. The position is returned in the image coordinate system.")]
		public event HMouseEventHandler HMouseMove;

		[Category("Mouse")]
		[Description("Occurs when a button is pressed over the HALCON window. Note that delta is meaningless here. The position is returned in the image coordinate system.")]
		public event HMouseEventHandler HMouseDown;

		[Category("Mouse")]
		[Description("Occurs when a button is released over the HALCON window. Note that delta is meaningless here. The position is returned in the image coordinate system.")]
		public event HMouseEventHandler HMouseUp;

		[Category("Mouse")]
		[Description("Occurs when a button is double-clicked over a HALCON window. Note that delta is meaningless here. The position is returned in the image coordinate system.")]
		public event HMouseEventHandler HMouseDoubleClick;

		[Description("Occurs when the wheel is used over a HALCON window while it has focus. Note that button is meaningless here. The position is returned in the image coordinate system.")]
		[Category("Mouse")]
		public event HMouseEventHandler HMouseWheel;

		[Description("Occurs after the HALCON window has been initialized.")]
		[Category("Behavior")]
		public event HInitWindowEventHandler HInitWindow;

		public event HErrorHandler HErrorNotify;

		public HSmartWindowControl()
		{
			InitializeComponent();
		}

		public void SetFullImagePart(HImage reference = null)
		{
			if (reference != null)
			{
				reference.GetImageSize(out int width, out int height);
				_hwindow.SetPart(0, 0, width - 1, height - 1);
			}
			else if (HKeepAspectRatio)
			{
				_hwindow.SetPart(0, 0, -2, -2);
			}
			else
			{
				_hwindow.SetPart(0, 0, -1, -1);
			}
		}

		private int HWindowCallback(IntPtr context)
		{
			if (base.InvokeRequired)
			{
				BeginInvoke((MethodInvoker)delegate
				{
					Invalidate();
				});
			}
			else
			{
				Invalidate();
			}
			return 2;
		}

		private void ctrl_Click(object sender, EventArgs e)
		{
			InvokeOnClick(this, EventArgs.Empty);
		}

		protected override void OnControlAdded(ControlEventArgs e)
		{
			base.OnControlAdded(e);
			if (!RunningInDesignerMode)
			{
				e.Control.Click += ctrl_Click;
				e.Control.GotFocus += Control_GotFocus;
				e.Control.LostFocus += Control_LostFocus;
				e.Control.MouseEnter += Control_MouseEnter;
				e.Control.MouseLeave += Control_MouseLeave;
				e.Control.MouseHover += Control_MouseHover;
				e.Control.SizeChanged += Control_SizeChanged;
				e.Control.KeyDown += Control_KeyDown;
				e.Control.KeyPress += Control_KeyPress;
				e.Control.KeyUp += Control_KeyUp;
				e.Control.Resize += Control_Resize;
			}
		}

		private void Control_Resize(object sender, EventArgs e)
		{
			OnResize(e);
		}

		private void Control_KeyUp(object sender, KeyEventArgs e)
		{
			OnKeyUp(e);
		}

		private void Control_KeyPress(object sender, KeyPressEventArgs e)
		{
			OnKeyPress(e);
		}

		private void Control_KeyDown(object sender, KeyEventArgs e)
		{
			OnKeyDown(e);
		}

		private void Control_SizeChanged(object sender, EventArgs e)
		{
			OnSizeChanged(e);
		}

		private void Control_MouseHover(object sender, EventArgs e)
		{
			OnMouseHover(e);
		}

		private void Control_MouseLeave(object sender, EventArgs e)
		{
			OnMouseLeave(e);
		}

		private void Control_MouseEnter(object sender, EventArgs e)
		{
			OnMouseEnter(e);
		}

		private void Control_LostFocus(object sender, EventArgs e)
		{
			InvokeLostFocus(this, EventArgs.Empty);
		}

		private void Control_GotFocus(object sender, EventArgs e)
		{
			InvokeGotFocus(this, EventArgs.Empty);
		}

		protected override void OnControlRemoved(ControlEventArgs e)
		{
			if (!RunningInDesignerMode)
			{
				e.Control.Click -= ctrl_Click;
				e.Control.GotFocus -= Control_GotFocus;
				e.Control.LostFocus -= Control_LostFocus;
				e.Control.MouseEnter -= Control_MouseEnter;
				e.Control.MouseLeave -= Control_MouseLeave;
				e.Control.MouseHover -= Control_MouseHover;
				e.Control.SizeChanged -= Control_SizeChanged;
				e.Control.KeyDown -= Control_KeyDown;
				e.Control.KeyPress -= Control_KeyPress;
				e.Control.KeyUp -= Control_KeyUp;
				e.Control.Resize -= Control_Resize;
			}
			base.OnControlRemoved(e);
		}

		private void CreateHWindow()
		{
			if (!RunningInDesignerMode)
			{
				_hwindow = new HWindow(0, 0, base.Width, base.Height, "", "buffer", "");
				_hwindow.SetPart(_part.Top, _part.Left, _part.Top + _part.Height - 1, _part.Left + _part.Width - 1);
				_hwindow.SetWindowParam("graphics_stack", "true");
				_prevsize.Width = base.Width;
				_prevsize.Height = base.Height;
				_dump_params = new HTuple(_hwindow);
				_dump_params[1] = "interleaved";
				_hwindow.OnContentUpdate(HWindowCallback);
				base.SizeChanged += HSmartWindowControl_SizeChanged;
				if (this.HInitWindow != null)
				{
					this.HInitWindow(this, new EventArgs());
				}
			}
		}

		private void HSmartWindowControl_SizeChanged(object sender, EventArgs e)
		{
			WindowFrame.Size = base.Size;
			if (HKeepAspectRatio)
			{
				using (HTuple hv_WindowHandle = new HTuple(_hwindow))
				{
					calculate_part(hv_WindowHandle, _prevsize.Width, _prevsize.Height);
				}
			}
		}

		private void HSmartWindowControl_Load(object sender, EventArgs e)
		{
			if (_hwindow == null)
			{
				CreateHWindow();
			}
		}

		private void GetFloatPart(HWindow window, out double l1, out double c1, out double l2, out double c2)
		{
			window.GetPart(out HTuple row, out HTuple column, out HTuple row2, out HTuple column2);
			l1 = row;
			c1 = column;
			l2 = row2;
			c2 = column2;
		}

		public static Image HalconToWinFormsImage(HImage himage)
		{
			HImage hImage = himage.InterleaveChannels("argb", "match", 255);
			string type;
			int width;
			int height;
			IntPtr imagePointer = hImage.GetImagePointer1(out type, out width, out height);
			Bitmap bitmap = new Bitmap(width / 4, height, width, PixelFormat.Format32bppPArgb, imagePointer);
			Image result;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				bitmap.Save(memoryStream, ImageFormat.Bmp);
				memoryStream.Position = 0L;
				result = Image.FromStream(memoryStream);
			}
			hImage.Dispose();
			bitmap.Dispose();
			return result;
		}

		private bool InteractingWithDrawingObjs()
		{
			switch (HDrawingObjectsModifier)
			{
			case DrawingObjectsModifier.Shift:
				return Control.ModifierKeys == Keys.Shift;
			case DrawingObjectsModifier.Ctrl:
				return Control.ModifierKeys == Keys.Control;
			case DrawingObjectsModifier.Alt:
				return Control.ModifierKeys == Keys.Alt;
			default:
				return true;
			}
		}

		private int MouseEventToInt(MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				return 1;
			}
			if (e.Button == MouseButtons.Right)
			{
				return 4;
			}
			if (e.Button == MouseButtons.Middle)
			{
				return 2;
			}
			return 0;
		}

		private HMouseEventArgs ToHMouse(MouseEventArgs e)
		{
			_hwindow.ConvertCoordinatesWindowToImage(e.Y, e.X, out double rowImage, out double columnImage);
			return new HMouseEventArgs(e.Button, e.Clicks, columnImage, rowImage, e.Delta);
		}

		private void WindowFrame_MouseDown(object sender, MouseEventArgs e)
		{
			HMouseEventArgs e2 = null;
			try
			{
				if (e.Button == MouseButtons.Left)
				{
					_left_button_down = true;
					_last_position.X = e.X;
					_last_position.Y = e.Y;
				}
				if (InteractingWithDrawingObjs())
				{
					_hwindow.ConvertCoordinatesWindowToImage(e.Y, e.X, out double rowImage, out double columnImage);
					_hwindow.SendMouseDownEvent(rowImage, columnImage, MouseEventToInt(e));
				}
				e2 = ToHMouse(e);
			}
			catch (HalconException he)
			{
				if (this.HErrorNotify != null)
				{
					this.HErrorNotify(he);
				}
			}
			if (this.HMouseDown != null)
			{
				this.HMouseDown(this, e2);
			}
		}

		public void HShiftWindowContents(double dx, double dy)
		{
			GetFloatPart(_hwindow, out double l, out double c, out double l2, out double c2);
			_hwindow.GetWindowExtents(out int _, out int _, out int width, out int height);
			double num = (c2 - c + 1.0) / (double)width;
			double num2 = (l2 - l + 1.0) / (double)height;
			try
			{
				_hwindow.SetPart(l + dy * num2, c + dx * num, l2 + dy * num2, c2 + dx * num);
			}
			catch (HalconException)
			{
			}
		}

		private void WindowFrame_MouseMove(object sender, MouseEventArgs e)
		{
			HMouseEventArgs e2 = null;
			try
			{
				bool flag = false;
				if (_left_button_down && InteractingWithDrawingObjs())
				{
					_hwindow.ConvertCoordinatesWindowToImage(e.Y, e.X, out double rowImage, out double columnImage);
					HTuple hTuple = _hwindow.SendMouseDragEvent(rowImage, columnImage, MouseEventToInt(e));
					flag = hTuple[0].S.Equals("true");
				}
				if (!flag && _left_button_down && HMoveContent)
				{
					HShiftWindowContents(_last_position.X - e.X, _last_position.Y - e.Y);
				}
				_last_position.X = e.X;
				_last_position.Y = e.Y;
				e2 = ToHMouse(e);
			}
			catch (HalconException he)
			{
				if (this.HErrorNotify != null)
				{
					this.HErrorNotify(he);
				}
			}
			if (this.HMouseMove != null)
			{
				this.HMouseMove(this, e2);
			}
		}

		private void WindowFrame_MouseUp(object sender, MouseEventArgs e)
		{
			HMouseEventArgs e2 = null;
			try
			{
				if (e.Button == MouseButtons.Left)
				{
					_hwindow.ConvertCoordinatesWindowToImage(e.Y, e.X, out double rowImage, out double columnImage);
					_hwindow.SendMouseUpEvent(rowImage, columnImage, MouseEventToInt(e));
					_left_button_down = false;
				}
				_last_position.X = e.X;
				_last_position.Y = e.Y;
				e2 = ToHMouse(e);
			}
			catch (HalconException he)
			{
				if (this.HErrorNotify != null)
				{
					this.HErrorNotify(he);
				}
			}
			if (this.HMouseUp != null)
			{
				this.HMouseUp(this, e2);
			}
		}

		private void WindowFrame_DoubleClick(object sender, EventArgs e)
		{
			HMouseEventArgs e2 = null;
			try
			{
				bool flag = false;
				MouseEventArgs mouseEventArgs = (MouseEventArgs)e;
				_last_position.X = mouseEventArgs.X;
				_last_position.Y = mouseEventArgs.Y;
				if (mouseEventArgs.Button == MouseButtons.Left && InteractingWithDrawingObjs())
				{
					_hwindow.ConvertCoordinatesWindowToImage(_last_position.Y, _last_position.X, out double rowImage, out double columnImage);
					HTuple hTuple = _hwindow.SendMouseDoubleClickEvent(rowImage, columnImage, MouseEventToInt(mouseEventArgs));
					flag = hTuple[0].S.Equals("true");
				}
				if (!flag && HDoubleClickToFitContent)
				{
					SetFullImagePart();
				}
				e2 = ToHMouse(mouseEventArgs);
			}
			catch (HalconException he)
			{
				if (this.HErrorNotify != null)
				{
					this.HErrorNotify(he);
				}
			}
			if (this.HMouseDoubleClick != null)
			{
				this.HMouseDoubleClick(this, e2);
			}
		}

		private void WindowFrame_MouseLeave(object sender, EventArgs e)
		{
			_left_button_down = false;
		}

		public void HSmartWindowControl_MouseWheel(object sender, MouseEventArgs e)
		{
			HMouseEventArgs e2 = null;
			try
			{
				if (_zooming != 0)
				{
					HOperatorSet.HomMat2dIdentity(out HTuple homMat2DIdentity);
					Point point = PointToClient(Cursor.Position);
					_hwindow.ConvertCoordinatesWindowToImage(point.Y, point.X, out double rowImage, out double columnImage);
					double num = (e.Delta < 0) ? Math.Sqrt(2.0) : (1.0 / Math.Sqrt(2.0));
					if (HZoomContent == ZoomContent.WheelBackwardZoomsIn)
					{
						num = 1.0 / num;
					}
					for (int num2 = Math.Abs(e.Delta) / 120; num2 > 1; num2--)
					{
						num *= ((e.Delta < 0) ? Math.Sqrt(2.0) : (1.0 / Math.Sqrt(2.0)));
					}
					HOperatorSet.HomMat2dScale(homMat2DIdentity, num, num, columnImage, rowImage, out HTuple homMat2DScale);
					GetFloatPart(_hwindow, out double l, out double c, out double l2, out double c2);
					HOperatorSet.AffineTransPoint2d(homMat2DScale, c, l, out HTuple qx, out HTuple qy);
					HOperatorSet.AffineTransPoint2d(homMat2DScale, c2, l2, out HTuple qx2, out HTuple qy2);
					e2 = ToHMouse(e);
					try
					{
						_hwindow.SetPart(qy.D, qx.D, qy2.D, qx2.D);
					}
					catch (Exception)
					{
						_hwindow.SetPart(l, c, l2, c2);
					}
				}
				else
				{
					e2 = ToHMouse(e);
				}
			}
			catch (HalconException he)
			{
				if (this.HErrorNotify != null)
				{
					this.HErrorNotify(he);
				}
			}
			if (this.HMouseWheel != null)
			{
				this.HMouseWheel(this, e2);
			}
		}

		private void HSmartWindowControl_Paint(object sender, PaintEventArgs e)
		{
			if (_hwindow != null && !RunningInDesignerMode)
			{
				bool flag = false;
				_hwindow.GetWindowExtents(out int _, out int _, out int width, out int height);
				if (base.Width > 0 && base.Height > 0 && (width != base.Width || height != base.Height))
				{
					WindowFrame.Width = base.Width;
					WindowFrame.Height = base.Height;
					_hwindow.GetWindowExtents(out int _, out int _, out int width2, out int height2);
					try
					{
						_hwindow.SetWindowExtents(0, 0, base.Width, base.Height);
						flag = true;
					}
					catch (HalconException)
					{
						_hwindow.SetWindowExtents(0, 0, width2, height2);
					}
				}
				if (HKeepAspectRatio && flag)
				{
					using (HTuple hv_WindowHandle = new HTuple(_hwindow))
					{
						calculate_part(hv_WindowHandle, _prevsize.Width, _prevsize.Height);
					}
				}
				_prevsize.Width = WindowFrame.Width;
				_prevsize.Height = WindowFrame.Height;
				_netimg.Dispose();
				HOperatorSet.DumpWindowImage(out _netimg, _dump_params);
				HOperatorSet.GetImagePointer1(_netimg, out HTuple pointer, out HTuple _, out HTuple width3, out HTuple height3);
				Bitmap image = new Bitmap(width3 / 4, height3, width3, PixelFormat.Format32bppPArgb, (IntPtr)pointer.L);
				WindowFrame.Image = image;
			}
		}

		private bool calculate_part(HTuple hv_WindowHandle, HTuple hv_WindowWidth, HTuple hv_WindowHeight)
		{
			HTuple row = null;
			HTuple column = null;
			HTuple row2 = null;
			HTuple column2 = null;
			HTuple hTuple = null;
			HTuple hTuple2 = null;
			HTuple row3 = null;
			HTuple column3 = null;
			HTuple width = null;
			HTuple height = null;
			HTuple hTuple3 = null;
			HTuple hTuple4 = null;
			HTuple hTuple5 = null;
			HTuple homMat2DIdentity = null;
			HTuple homMat2DScale = null;
			HTuple qx = null;
			HTuple qy = null;
			bool result = true;
			HOperatorSet.GetPart(hv_WindowHandle, out row, out column, out row2, out column2);
			try
			{
				hTuple = column2 - column + 1;
				hTuple2 = row2 - row + 1;
				HTuple hTuple6 = hTuple / hTuple2.TupleReal();
				HOperatorSet.GetWindowExtents(hv_WindowHandle, out row3, out column3, out width, out height);
				hTuple3 = width / hv_WindowWidth.TupleReal();
				hTuple4 = height / hv_WindowHeight.TupleReal();
				hTuple5 = new HTuple();
				hTuple5 = hTuple5.TupleConcat((row + row2) * 0.5);
				hTuple5 = hTuple5.TupleConcat((column + column2) * 0.5);
				HOperatorSet.HomMat2dIdentity(out homMat2DIdentity);
				HOperatorSet.HomMat2dScale(homMat2DIdentity, hTuple3, hTuple4, hTuple5.TupleSelect(1), hTuple5.TupleSelect(0), out homMat2DScale);
				HOperatorSet.AffineTransPoint2d(homMat2DScale, column.TupleConcat(column2), row.TupleConcat(row2), out qx, out qy);
				HOperatorSet.SetPart(hv_WindowHandle, qy.TupleSelect(0), qx.TupleSelect(0), qy.TupleSelect(1), qx.TupleSelect(1));
				return result;
			}
			catch (HalconException)
			{
				HOperatorSet.SetPart(hv_WindowHandle, row, column, row2, column2);
				return false;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (_hwindow != null)
				{
					_hwindow.Dispose();
				}
				if (_dump_params != null)
				{
					_dump_params.Dispose();
				}
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			WindowFrame = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)WindowFrame).BeginInit();
			SuspendLayout();
			WindowFrame.BackColor = System.Drawing.SystemColors.Desktop;
			WindowFrame.Dock = System.Windows.Forms.DockStyle.Fill;
			WindowFrame.Location = new System.Drawing.Point(0, 0);
			WindowFrame.Margin = new System.Windows.Forms.Padding(0);
			WindowFrame.Name = "WindowFrame";
			WindowFrame.Size = new System.Drawing.Size(512, 512);
			WindowFrame.TabIndex = 0;
			WindowFrame.TabStop = false;
			WindowFrame.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(WindowFrame_DoubleClick);
			WindowFrame.MouseDown += new System.Windows.Forms.MouseEventHandler(WindowFrame_MouseDown);
			WindowFrame.MouseMove += new System.Windows.Forms.MouseEventHandler(WindowFrame_MouseMove);
			WindowFrame.MouseUp += new System.Windows.Forms.MouseEventHandler(WindowFrame_MouseUp);
			WindowFrame.MouseLeave += new System.EventHandler(WindowFrame_MouseLeave);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			base.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
			base.Controls.Add(WindowFrame);
			base.Margin = new System.Windows.Forms.Padding(0);
			base.Name = "HSmartWindowControl";
			base.Size = new System.Drawing.Size(512, 512);
			base.Load += new System.EventHandler(HSmartWindowControl_Load);
			base.Paint += new System.Windows.Forms.PaintEventHandler(HSmartWindowControl_Paint);
			((System.ComponentModel.ISupportInitialize)WindowFrame).EndInit();
			ResumeLayout(false);
		}
	}
}
