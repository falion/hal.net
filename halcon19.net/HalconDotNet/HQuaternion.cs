using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HQuaternion : HData, ISerializable, ICloneable
	{
		private const int FIXEDSIZE = 4;

		public HQuaternion()
		{
		}

		public HQuaternion(HTuple tuple)
			: base(tuple)
		{
		}

		internal HQuaternion(HData data)
			: base(data)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, HTupleType type, int err, out HQuaternion obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HQuaternion(new HData(tuple));
			return err;
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HQuaternion obj)
		{
			return LoadNew(proc, parIndex, HTupleType.MIXED, err, out obj);
		}

		internal static HQuaternion[] SplitArray(HTuple data)
		{
			int num = data.Length / 4;
			HQuaternion[] array = new HQuaternion[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = new HQuaternion(new HData(data.TupleSelectRange(i * 4, (i + 1) * 4 - 1)));
			}
			return array;
		}

		public HQuaternion(HTuple axisX, HTuple axisY, HTuple axisZ, HTuple angle)
		{
			IntPtr proc = HalconAPI.PreCall(225);
			HalconAPI.Store(proc, 0, axisX);
			HalconAPI.Store(proc, 1, axisY);
			HalconAPI.Store(proc, 2, axisZ);
			HalconAPI.Store(proc, 3, angle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(axisX);
			HalconAPI.UnpinTuple(axisY);
			HalconAPI.UnpinTuple(axisZ);
			HalconAPI.UnpinTuple(angle);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HQuaternion(double axisX, double axisY, double axisZ, double angle)
		{
			IntPtr proc = HalconAPI.PreCall(225);
			HalconAPI.StoreD(proc, 0, axisX);
			HalconAPI.StoreD(proc, 1, axisY);
			HalconAPI.StoreD(proc, 2, axisZ);
			HalconAPI.StoreD(proc, 3, angle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeQuat();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HQuaternion(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeQuat(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeQuat();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public static HQuaternion Deserialize(Stream stream)
		{
			HQuaternion hQuaternion = new HQuaternion();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hQuaternion.DeserializeQuat(hSerializedItem);
			hSerializedItem.Dispose();
			return hQuaternion;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public HQuaternion Clone()
		{
			HSerializedItem hSerializedItem = SerializeQuat();
			HQuaternion hQuaternion = new HQuaternion();
			hQuaternion.DeserializeQuat(hSerializedItem);
			hSerializedItem.Dispose();
			return hQuaternion;
		}

		public static HQuaternion operator *(HQuaternion left, HQuaternion right)
		{
			return left.QuatCompose(right);
		}

		public static implicit operator HPose(HQuaternion quaternion)
		{
			return quaternion.QuatToPose();
		}

		public static implicit operator HHomMat3D(HQuaternion quaternion)
		{
			return quaternion.QuatToHomMat3d();
		}

		public static HQuaternion operator ~(HQuaternion quaternion)
		{
			return quaternion.QuatConjugate();
		}

		public double QuatRotatePoint3d(double px, double py, double pz, out double qy, out double qz)
		{
			IntPtr proc = HalconAPI.PreCall(222);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, px);
			HalconAPI.StoreD(proc, 2, py);
			HalconAPI.StoreD(proc, 3, pz);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out qy);
			err = HalconAPI.LoadD(proc, 2, err, out qz);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HQuaternion QuatConjugate()
		{
			IntPtr proc = HalconAPI.PreCall(223);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HQuaternion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HQuaternion QuatNormalize()
		{
			IntPtr proc = HalconAPI.PreCall(224);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HQuaternion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void AxisAngleToQuat(HTuple axisX, HTuple axisY, HTuple axisZ, HTuple angle)
		{
			IntPtr proc = HalconAPI.PreCall(225);
			HalconAPI.Store(proc, 0, axisX);
			HalconAPI.Store(proc, 1, axisY);
			HalconAPI.Store(proc, 2, axisZ);
			HalconAPI.Store(proc, 3, angle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(axisX);
			HalconAPI.UnpinTuple(axisY);
			HalconAPI.UnpinTuple(axisZ);
			HalconAPI.UnpinTuple(angle);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void AxisAngleToQuat(double axisX, double axisY, double axisZ, double angle)
		{
			IntPtr proc = HalconAPI.PreCall(225);
			HalconAPI.StoreD(proc, 0, axisX);
			HalconAPI.StoreD(proc, 1, axisY);
			HalconAPI.StoreD(proc, 2, axisZ);
			HalconAPI.StoreD(proc, 3, angle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HPose QuatToPose()
		{
			IntPtr proc = HalconAPI.PreCall(226);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HHomMat3D QuatToHomMat3d()
		{
			IntPtr proc = HalconAPI.PreCall(229);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HHomMat3D.LoadNew(proc, 0, err, out HHomMat3D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public static HQuaternion[] PoseToQuat(HPose[] pose)
		{
			HTuple hTuple = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(230);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return SplitArray(tuple);
		}

		public void PoseToQuat(HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(230);
			HalconAPI.Store(proc, 0, pose);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pose);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HQuaternion QuatInterpolate(HQuaternion quaternionEnd, HTuple interpPos)
		{
			IntPtr proc = HalconAPI.PreCall(231);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, quaternionEnd);
			HalconAPI.Store(proc, 2, interpPos);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(quaternionEnd);
			HalconAPI.UnpinTuple(interpPos);
			err = LoadNew(proc, 0, err, out HQuaternion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HQuaternion QuatCompose(HQuaternion quaternionRight)
		{
			IntPtr proc = HalconAPI.PreCall(232);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, quaternionRight);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(quaternionRight);
			err = LoadNew(proc, 0, err, out HQuaternion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void DeserializeQuat(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(237);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeQuat()
		{
			IntPtr proc = HalconAPI.PreCall(238);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}
	}
}
