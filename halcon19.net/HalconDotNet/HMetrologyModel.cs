using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HMetrologyModel : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMetrologyModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMetrologyModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("metrology_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMetrologyModel obj)
		{
			obj = new HMetrologyModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMetrologyModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HMetrologyModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HMetrologyModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HMetrologyModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(798);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMetrologyModel()
		{
			IntPtr proc = HalconAPI.PreCall(820);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeMetrologyModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMetrologyModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeMetrologyModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeMetrologyModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HMetrologyModel Deserialize(Stream stream)
		{
			HMetrologyModel hMetrologyModel = new HMetrologyModel();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hMetrologyModel.DeserializeMetrologyModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hMetrologyModel;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HMetrologyModel Clone()
		{
			HSerializedItem hSerializedItem = SerializeMetrologyModel();
			HMetrologyModel hMetrologyModel = new HMetrologyModel();
			hMetrologyModel.DeserializeMetrologyModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hMetrologyModel;
		}

		public HXLDCont GetMetrologyObjectModelContour(HTuple index, double resolution)
		{
			IntPtr proc = HalconAPI.PreCall(788);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.StoreD(proc, 2, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont GetMetrologyObjectModelContour(int index, double resolution)
		{
			IntPtr proc = HalconAPI.PreCall(788);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, index);
			HalconAPI.StoreD(proc, 2, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont GetMetrologyObjectResultContour(HTuple index, HTuple instance, double resolution)
		{
			IntPtr proc = HalconAPI.PreCall(789);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, instance);
			HalconAPI.StoreD(proc, 3, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(instance);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont GetMetrologyObjectResultContour(int index, string instance, double resolution)
		{
			IntPtr proc = HalconAPI.PreCall(789);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, index);
			HalconAPI.StoreS(proc, 2, instance);
			HalconAPI.StoreD(proc, 3, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void AlignMetrologyModel(HTuple row, HTuple column, HTuple angle)
		{
			IntPtr proc = HalconAPI.PreCall(790);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, angle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(angle);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AlignMetrologyModel(double row, double column, double angle)
		{
			IntPtr proc = HalconAPI.PreCall(790);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, angle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int AddMetrologyObjectGeneric(HTuple shape, HTuple shapeParam, HTuple measureLength1, HTuple measureLength2, HTuple measureSigma, HTuple measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(791);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, shape);
			HalconAPI.Store(proc, 2, shapeParam);
			HalconAPI.Store(proc, 3, measureLength1);
			HalconAPI.Store(proc, 4, measureLength2);
			HalconAPI.Store(proc, 5, measureSigma);
			HalconAPI.Store(proc, 6, measureThreshold);
			HalconAPI.Store(proc, 7, genParamName);
			HalconAPI.Store(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(shape);
			HalconAPI.UnpinTuple(shapeParam);
			HalconAPI.UnpinTuple(measureLength1);
			HalconAPI.UnpinTuple(measureLength2);
			HalconAPI.UnpinTuple(measureSigma);
			HalconAPI.UnpinTuple(measureThreshold);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int AddMetrologyObjectGeneric(string shape, HTuple shapeParam, double measureLength1, double measureLength2, double measureSigma, double measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(791);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, shape);
			HalconAPI.Store(proc, 2, shapeParam);
			HalconAPI.StoreD(proc, 3, measureLength1);
			HalconAPI.StoreD(proc, 4, measureLength2);
			HalconAPI.StoreD(proc, 5, measureSigma);
			HalconAPI.StoreD(proc, 6, measureThreshold);
			HalconAPI.Store(proc, 7, genParamName);
			HalconAPI.Store(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(shapeParam);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple GetMetrologyModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(792);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetMetrologyModelParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(793);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMetrologyModelParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(793);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeMetrologyModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(794);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeMetrologyModel()
		{
			IntPtr proc = HalconAPI.PreCall(795);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void TransformMetrologyObject(HTuple index, HTuple row, HTuple column, HTuple phi, HTuple mode)
		{
			IntPtr proc = HalconAPI.PreCall(796);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.Store(proc, 4, phi);
			HalconAPI.Store(proc, 5, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(mode);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TransformMetrologyObject(string index, double row, double column, double phi, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(796);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.StoreD(proc, 2, row);
			HalconAPI.StoreD(proc, 3, column);
			HalconAPI.StoreD(proc, 4, phi);
			HalconAPI.StoreS(proc, 5, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteMetrologyModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(797);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadMetrologyModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(798);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public int CopyMetrologyModel(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(799);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int CopyMetrologyModel(string index)
		{
			IntPtr proc = HalconAPI.PreCall(799);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple CopyMetrologyObject(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(800);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int CopyMetrologyObject(string index)
		{
			IntPtr proc = HalconAPI.PreCall(800);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple GetMetrologyObjectNumInstances(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(801);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double GetMetrologyObjectNumInstances(int index)
		{
			IntPtr proc = HalconAPI.PreCall(801);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple GetMetrologyObjectResult(HTuple index, HTuple instance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(802);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, instance);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(instance);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetMetrologyObjectResult(int index, string instance, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(802);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, index);
			HalconAPI.StoreS(proc, 2, instance);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HXLDCont GetMetrologyObjectMeasures(HTuple index, string transition, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(803);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.StoreS(proc, 2, transition);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont GetMetrologyObjectMeasures(string index, string transition, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(803);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.StoreS(proc, 2, transition);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ApplyMetrologyModel(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(804);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HTuple GetMetrologyObjectIndices()
		{
			IntPtr proc = HalconAPI.PreCall(805);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void ResetMetrologyObjectFuzzyParam(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(806);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ResetMetrologyObjectFuzzyParam(string index)
		{
			IntPtr proc = HalconAPI.PreCall(806);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ResetMetrologyObjectParam(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(807);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ResetMetrologyObjectParam(string index)
		{
			IntPtr proc = HalconAPI.PreCall(807);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetMetrologyObjectFuzzyParam(HTuple index, HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(808);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetMetrologyObjectFuzzyParam(string index, HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(808);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetMetrologyObjectParam(HTuple index, HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(809);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetMetrologyObjectParam(string index, HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(809);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetMetrologyObjectFuzzyParam(HTuple index, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(810);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMetrologyObjectFuzzyParam(string index, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(810);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMetrologyObjectParam(HTuple index, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(811);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMetrologyObjectParam(string index, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(811);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int AddMetrologyObjectRectangle2Measure(HTuple row, HTuple column, HTuple phi, HTuple length1, HTuple length2, HTuple measureLength1, HTuple measureLength2, HTuple measureSigma, HTuple measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(812);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, phi);
			HalconAPI.Store(proc, 4, length1);
			HalconAPI.Store(proc, 5, length2);
			HalconAPI.Store(proc, 6, measureLength1);
			HalconAPI.Store(proc, 7, measureLength2);
			HalconAPI.Store(proc, 8, measureSigma);
			HalconAPI.Store(proc, 9, measureThreshold);
			HalconAPI.Store(proc, 10, genParamName);
			HalconAPI.Store(proc, 11, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(length1);
			HalconAPI.UnpinTuple(length2);
			HalconAPI.UnpinTuple(measureLength1);
			HalconAPI.UnpinTuple(measureLength2);
			HalconAPI.UnpinTuple(measureSigma);
			HalconAPI.UnpinTuple(measureThreshold);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int AddMetrologyObjectRectangle2Measure(double row, double column, double phi, double length1, double length2, double measureLength1, double measureLength2, double measureSigma, double measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(812);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreD(proc, 4, length1);
			HalconAPI.StoreD(proc, 5, length2);
			HalconAPI.StoreD(proc, 6, measureLength1);
			HalconAPI.StoreD(proc, 7, measureLength2);
			HalconAPI.StoreD(proc, 8, measureSigma);
			HalconAPI.StoreD(proc, 9, measureThreshold);
			HalconAPI.Store(proc, 10, genParamName);
			HalconAPI.Store(proc, 11, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int AddMetrologyObjectLineMeasure(HTuple rowBegin, HTuple columnBegin, HTuple rowEnd, HTuple columnEnd, HTuple measureLength1, HTuple measureLength2, HTuple measureSigma, HTuple measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(813);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, rowBegin);
			HalconAPI.Store(proc, 2, columnBegin);
			HalconAPI.Store(proc, 3, rowEnd);
			HalconAPI.Store(proc, 4, columnEnd);
			HalconAPI.Store(proc, 5, measureLength1);
			HalconAPI.Store(proc, 6, measureLength2);
			HalconAPI.Store(proc, 7, measureSigma);
			HalconAPI.Store(proc, 8, measureThreshold);
			HalconAPI.Store(proc, 9, genParamName);
			HalconAPI.Store(proc, 10, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowBegin);
			HalconAPI.UnpinTuple(columnBegin);
			HalconAPI.UnpinTuple(rowEnd);
			HalconAPI.UnpinTuple(columnEnd);
			HalconAPI.UnpinTuple(measureLength1);
			HalconAPI.UnpinTuple(measureLength2);
			HalconAPI.UnpinTuple(measureSigma);
			HalconAPI.UnpinTuple(measureThreshold);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int AddMetrologyObjectLineMeasure(double rowBegin, double columnBegin, double rowEnd, double columnEnd, double measureLength1, double measureLength2, double measureSigma, double measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(813);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, rowBegin);
			HalconAPI.StoreD(proc, 2, columnBegin);
			HalconAPI.StoreD(proc, 3, rowEnd);
			HalconAPI.StoreD(proc, 4, columnEnd);
			HalconAPI.StoreD(proc, 5, measureLength1);
			HalconAPI.StoreD(proc, 6, measureLength2);
			HalconAPI.StoreD(proc, 7, measureSigma);
			HalconAPI.StoreD(proc, 8, measureThreshold);
			HalconAPI.Store(proc, 9, genParamName);
			HalconAPI.Store(proc, 10, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int AddMetrologyObjectEllipseMeasure(HTuple row, HTuple column, HTuple phi, HTuple radius1, HTuple radius2, HTuple measureLength1, HTuple measureLength2, HTuple measureSigma, HTuple measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(814);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, phi);
			HalconAPI.Store(proc, 4, radius1);
			HalconAPI.Store(proc, 5, radius2);
			HalconAPI.Store(proc, 6, measureLength1);
			HalconAPI.Store(proc, 7, measureLength2);
			HalconAPI.Store(proc, 8, measureSigma);
			HalconAPI.Store(proc, 9, measureThreshold);
			HalconAPI.Store(proc, 10, genParamName);
			HalconAPI.Store(proc, 11, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(radius1);
			HalconAPI.UnpinTuple(radius2);
			HalconAPI.UnpinTuple(measureLength1);
			HalconAPI.UnpinTuple(measureLength2);
			HalconAPI.UnpinTuple(measureSigma);
			HalconAPI.UnpinTuple(measureThreshold);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int AddMetrologyObjectEllipseMeasure(double row, double column, double phi, double radius1, double radius2, double measureLength1, double measureLength2, double measureSigma, double measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(814);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreD(proc, 4, radius1);
			HalconAPI.StoreD(proc, 5, radius2);
			HalconAPI.StoreD(proc, 6, measureLength1);
			HalconAPI.StoreD(proc, 7, measureLength2);
			HalconAPI.StoreD(proc, 8, measureSigma);
			HalconAPI.StoreD(proc, 9, measureThreshold);
			HalconAPI.Store(proc, 10, genParamName);
			HalconAPI.Store(proc, 11, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int AddMetrologyObjectCircleMeasure(HTuple row, HTuple column, HTuple radius, HTuple measureLength1, HTuple measureLength2, HTuple measureSigma, HTuple measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(815);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, row);
			HalconAPI.Store(proc, 2, column);
			HalconAPI.Store(proc, 3, radius);
			HalconAPI.Store(proc, 4, measureLength1);
			HalconAPI.Store(proc, 5, measureLength2);
			HalconAPI.Store(proc, 6, measureSigma);
			HalconAPI.Store(proc, 7, measureThreshold);
			HalconAPI.Store(proc, 8, genParamName);
			HalconAPI.Store(proc, 9, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(measureLength1);
			HalconAPI.UnpinTuple(measureLength2);
			HalconAPI.UnpinTuple(measureSigma);
			HalconAPI.UnpinTuple(measureThreshold);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int AddMetrologyObjectCircleMeasure(double row, double column, double radius, double measureLength1, double measureLength2, double measureSigma, double measureThreshold, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(815);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, radius);
			HalconAPI.StoreD(proc, 4, measureLength1);
			HalconAPI.StoreD(proc, 5, measureLength2);
			HalconAPI.StoreD(proc, 6, measureSigma);
			HalconAPI.StoreD(proc, 7, measureThreshold);
			HalconAPI.Store(proc, 8, genParamName);
			HalconAPI.Store(proc, 9, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public void ClearMetrologyModel()
		{
			IntPtr proc = HalconAPI.PreCall(817);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ClearMetrologyObject(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(818);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ClearMetrologyObject(string index)
		{
			IntPtr proc = HalconAPI.PreCall(818);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetMetrologyModelImageSize(int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(819);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateMetrologyModel()
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(820);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
