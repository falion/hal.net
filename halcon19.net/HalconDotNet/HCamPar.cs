using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HCamPar : HData, ISerializable, ICloneable
	{
		public HCamPar()
		{
		}

		public HCamPar(HTuple tuple)
			: base(tuple)
		{
		}

		internal HCamPar(HData data)
			: base(data)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, HTupleType type, int err, out HCamPar obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HCamPar(new HData(tuple));
			return err;
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCamPar obj)
		{
			return LoadNew(proc, parIndex, HTupleType.MIXED, err, out obj);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeCamPar();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCamPar(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeCamPar(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeCamPar();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public static HCamPar Deserialize(Stream stream)
		{
			HCamPar hCamPar = new HCamPar();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hCamPar.DeserializeCamPar(hSerializedItem);
			hSerializedItem.Dispose();
			return hCamPar;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public HCamPar Clone()
		{
			HSerializedItem hSerializedItem = SerializeCamPar();
			HCamPar hCamPar = new HCamPar();
			hCamPar.DeserializeCamPar(hSerializedItem);
			hSerializedItem.Dispose();
			return hCamPar;
		}

		public HImage BinocularDistanceMs(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect2, HPose relPoseRect, int minDisparity, int maxDisparity, int surfaceSmoothing, int edgeSmoothing, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(346);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreI(proc, 3, minDisparity);
			HalconAPI.StoreI(proc, 4, maxDisparity);
			HalconAPI.StoreI(proc, 5, surfaceSmoothing);
			HalconAPI.StoreI(proc, 6, edgeSmoothing);
			HalconAPI.Store(proc, 7, genParamName);
			HalconAPI.Store(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return obj;
		}

		public HImage BinocularDistanceMs(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect2, HPose relPoseRect, int minDisparity, int maxDisparity, int surfaceSmoothing, int edgeSmoothing, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(346);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreI(proc, 3, minDisparity);
			HalconAPI.StoreI(proc, 4, maxDisparity);
			HalconAPI.StoreI(proc, 5, surfaceSmoothing);
			HalconAPI.StoreI(proc, 6, edgeSmoothing);
			HalconAPI.StoreS(proc, 7, genParamName);
			HalconAPI.StoreS(proc, 8, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return obj;
		}

		public HImage BinocularDistanceMg(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect2, HPose relPoseRect, double grayConstancy, double gradientConstancy, double smoothness, double initialGuess, string calculateScore, HTuple MGParamName, HTuple MGParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(348);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreD(proc, 3, grayConstancy);
			HalconAPI.StoreD(proc, 4, gradientConstancy);
			HalconAPI.StoreD(proc, 5, smoothness);
			HalconAPI.StoreD(proc, 6, initialGuess);
			HalconAPI.StoreS(proc, 7, calculateScore);
			HalconAPI.Store(proc, 8, MGParamName);
			HalconAPI.Store(proc, 9, MGParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HalconAPI.UnpinTuple(MGParamName);
			HalconAPI.UnpinTuple(MGParamValue);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return obj;
		}

		public HImage BinocularDistanceMg(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect2, HPose relPoseRect, double grayConstancy, double gradientConstancy, double smoothness, double initialGuess, string calculateScore, string MGParamName, string MGParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(348);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreD(proc, 3, grayConstancy);
			HalconAPI.StoreD(proc, 4, gradientConstancy);
			HalconAPI.StoreD(proc, 5, smoothness);
			HalconAPI.StoreD(proc, 6, initialGuess);
			HalconAPI.StoreS(proc, 7, calculateScore);
			HalconAPI.StoreS(proc, 8, MGParamName);
			HalconAPI.StoreS(proc, 9, MGParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return obj;
		}

		public HHomMat2D RelPoseToFundamentalMatrix(HPose relPose, HTuple covRelPose, HCamPar camPar2, out HTuple covFMat)
		{
			IntPtr proc = HalconAPI.PreCall(353);
			Store(proc, 2);
			HalconAPI.Store(proc, 0, relPose);
			HalconAPI.Store(proc, 1, covRelPose);
			HalconAPI.Store(proc, 3, camPar2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(relPose);
			HalconAPI.UnpinTuple(covRelPose);
			HalconAPI.UnpinTuple(camPar2);
			err = HHomMat2D.LoadNew(proc, 0, err, out HHomMat2D obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covFMat);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HPose VectorToRelPose(HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple covRR1, HTuple covRC1, HTuple covCC1, HTuple covRR2, HTuple covRC2, HTuple covCC2, HCamPar camPar2, string method, out HTuple covRelPose, out HTuple error, out HTuple x, out HTuple y, out HTuple z, out HTuple covXYZ)
		{
			IntPtr proc = HalconAPI.PreCall(355);
			Store(proc, 10);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, covRR1);
			HalconAPI.Store(proc, 5, covRC1);
			HalconAPI.Store(proc, 6, covCC1);
			HalconAPI.Store(proc, 7, covRR2);
			HalconAPI.Store(proc, 8, covRC2);
			HalconAPI.Store(proc, 9, covCC2);
			HalconAPI.Store(proc, 11, camPar2);
			HalconAPI.StoreS(proc, 12, method);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(covRR1);
			HalconAPI.UnpinTuple(covRC1);
			HalconAPI.UnpinTuple(covCC1);
			HalconAPI.UnpinTuple(covRR2);
			HalconAPI.UnpinTuple(covRC2);
			HalconAPI.UnpinTuple(covCC2);
			HalconAPI.UnpinTuple(camPar2);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covRelPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out covXYZ);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HPose VectorToRelPose(HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HTuple covRR1, HTuple covRC1, HTuple covCC1, HTuple covRR2, HTuple covRC2, HTuple covCC2, HCamPar camPar2, string method, out HTuple covRelPose, out double error, out HTuple x, out HTuple y, out HTuple z, out HTuple covXYZ)
		{
			IntPtr proc = HalconAPI.PreCall(355);
			Store(proc, 10);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 4, covRR1);
			HalconAPI.Store(proc, 5, covRC1);
			HalconAPI.Store(proc, 6, covCC1);
			HalconAPI.Store(proc, 7, covRR2);
			HalconAPI.Store(proc, 8, covRC2);
			HalconAPI.Store(proc, 9, covCC2);
			HalconAPI.Store(proc, 11, camPar2);
			HalconAPI.StoreS(proc, 12, method);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(covRR1);
			HalconAPI.UnpinTuple(covRC1);
			HalconAPI.UnpinTuple(covCC1);
			HalconAPI.UnpinTuple(covRR2);
			HalconAPI.UnpinTuple(covRC2);
			HalconAPI.UnpinTuple(covCC2);
			HalconAPI.UnpinTuple(camPar2);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covRelPose);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out covXYZ);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HPose MatchRelPoseRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HCamPar camPar2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, HTuple rotation, HTuple matchThreshold, string estimationMethod, HTuple distanceThreshold, int randSeed, out HTuple covRelPose, out HTuple error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(359);
			Store(proc, 4);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 5, camPar2);
			HalconAPI.StoreS(proc, 6, grayMatchMethod);
			HalconAPI.StoreI(proc, 7, maskSize);
			HalconAPI.StoreI(proc, 8, rowMove);
			HalconAPI.StoreI(proc, 9, colMove);
			HalconAPI.StoreI(proc, 10, rowTolerance);
			HalconAPI.StoreI(proc, 11, colTolerance);
			HalconAPI.Store(proc, 12, rotation);
			HalconAPI.Store(proc, 13, matchThreshold);
			HalconAPI.StoreS(proc, 14, estimationMethod);
			HalconAPI.Store(proc, 15, distanceThreshold);
			HalconAPI.StoreI(proc, 16, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(camPar2);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(matchThreshold);
			HalconAPI.UnpinTuple(distanceThreshold);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covRelPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return obj;
		}

		public HPose MatchRelPoseRansac(HImage image1, HImage image2, HTuple rows1, HTuple cols1, HTuple rows2, HTuple cols2, HCamPar camPar2, string grayMatchMethod, int maskSize, int rowMove, int colMove, int rowTolerance, int colTolerance, double rotation, int matchThreshold, string estimationMethod, double distanceThreshold, int randSeed, out HTuple covRelPose, out double error, out HTuple points1, out HTuple points2)
		{
			IntPtr proc = HalconAPI.PreCall(359);
			Store(proc, 4);
			HalconAPI.Store(proc, 1, image1);
			HalconAPI.Store(proc, 2, image2);
			HalconAPI.Store(proc, 0, rows1);
			HalconAPI.Store(proc, 1, cols1);
			HalconAPI.Store(proc, 2, rows2);
			HalconAPI.Store(proc, 3, cols2);
			HalconAPI.Store(proc, 5, camPar2);
			HalconAPI.StoreS(proc, 6, grayMatchMethod);
			HalconAPI.StoreI(proc, 7, maskSize);
			HalconAPI.StoreI(proc, 8, rowMove);
			HalconAPI.StoreI(proc, 9, colMove);
			HalconAPI.StoreI(proc, 10, rowTolerance);
			HalconAPI.StoreI(proc, 11, colTolerance);
			HalconAPI.StoreD(proc, 12, rotation);
			HalconAPI.StoreI(proc, 13, matchThreshold);
			HalconAPI.StoreS(proc, 14, estimationMethod);
			HalconAPI.StoreD(proc, 15, distanceThreshold);
			HalconAPI.StoreI(proc, 16, randSeed);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(rows1);
			HalconAPI.UnpinTuple(cols1);
			HalconAPI.UnpinTuple(rows2);
			HalconAPI.UnpinTuple(cols2);
			HalconAPI.UnpinTuple(camPar2);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covRelPose);
			err = HalconAPI.LoadD(proc, 2, err, out error);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out points1);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out points2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image1);
			GC.KeepAlive(image2);
			return obj;
		}

		public HImage BinocularDistance(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect2, HPose relPoseRect, string method, int maskWidth, int maskHeight, HTuple textureThresh, int minDisparity, int maxDisparity, int numLevels, HTuple scoreThresh, HTuple filter, HTuple subDistance)
		{
			IntPtr proc = HalconAPI.PreCall(362);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreS(proc, 3, method);
			HalconAPI.StoreI(proc, 4, maskWidth);
			HalconAPI.StoreI(proc, 5, maskHeight);
			HalconAPI.Store(proc, 6, textureThresh);
			HalconAPI.StoreI(proc, 7, minDisparity);
			HalconAPI.StoreI(proc, 8, maxDisparity);
			HalconAPI.StoreI(proc, 9, numLevels);
			HalconAPI.Store(proc, 10, scoreThresh);
			HalconAPI.Store(proc, 11, filter);
			HalconAPI.Store(proc, 12, subDistance);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HalconAPI.UnpinTuple(textureThresh);
			HalconAPI.UnpinTuple(scoreThresh);
			HalconAPI.UnpinTuple(filter);
			HalconAPI.UnpinTuple(subDistance);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return obj;
		}

		public HImage BinocularDistance(HImage imageRect1, HImage imageRect2, out HImage score, HCamPar camParamRect2, HPose relPoseRect, string method, int maskWidth, int maskHeight, double textureThresh, int minDisparity, int maxDisparity, int numLevels, double scoreThresh, string filter, string subDistance)
		{
			IntPtr proc = HalconAPI.PreCall(362);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, imageRect1);
			HalconAPI.Store(proc, 2, imageRect2);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreS(proc, 3, method);
			HalconAPI.StoreI(proc, 4, maskWidth);
			HalconAPI.StoreI(proc, 5, maskHeight);
			HalconAPI.StoreD(proc, 6, textureThresh);
			HalconAPI.StoreI(proc, 7, minDisparity);
			HalconAPI.StoreI(proc, 8, maxDisparity);
			HalconAPI.StoreI(proc, 9, numLevels);
			HalconAPI.StoreD(proc, 10, scoreThresh);
			HalconAPI.StoreS(proc, 11, filter);
			HalconAPI.StoreS(proc, 12, subDistance);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1);
			GC.KeepAlive(imageRect2);
			return obj;
		}

		public void IntersectLinesOfSight(HCamPar camParam2, HPose relPose, HTuple row1, HTuple col1, HTuple row2, HTuple col2, out HTuple x, out HTuple y, out HTuple z, out HTuple dist)
		{
			IntPtr proc = HalconAPI.PreCall(364);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParam2);
			HalconAPI.Store(proc, 2, relPose);
			HalconAPI.Store(proc, 3, row1);
			HalconAPI.Store(proc, 4, col1);
			HalconAPI.Store(proc, 5, row2);
			HalconAPI.Store(proc, 6, col2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParam2);
			HalconAPI.UnpinTuple(relPose);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(col1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(col2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out z);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out dist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void IntersectLinesOfSight(HCamPar camParam2, HPose relPose, double row1, double col1, double row2, double col2, out double x, out double y, out double z, out double dist)
		{
			IntPtr proc = HalconAPI.PreCall(364);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParam2);
			HalconAPI.Store(proc, 2, relPose);
			HalconAPI.StoreD(proc, 3, row1);
			HalconAPI.StoreD(proc, 4, col1);
			HalconAPI.StoreD(proc, 5, row2);
			HalconAPI.StoreD(proc, 6, col2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParam2);
			HalconAPI.UnpinTuple(relPose);
			err = HalconAPI.LoadD(proc, 0, err, out x);
			err = HalconAPI.LoadD(proc, 1, err, out y);
			err = HalconAPI.LoadD(proc, 2, err, out z);
			err = HalconAPI.LoadD(proc, 3, err, out dist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage DisparityImageToXyz(HImage disparity, out HImage y, out HImage z, HCamPar camParamRect2, HPose relPoseRect)
		{
			IntPtr proc = HalconAPI.PreCall(365);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, disparity);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out y);
			err = HImage.LoadNew(proc, 3, err, out z);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(disparity);
			return obj;
		}

		public void DisparityToPoint3d(HCamPar camParamRect2, HPose relPoseRect, HTuple row1, HTuple col1, HTuple disparity, out HTuple x, out HTuple y, out HTuple z)
		{
			IntPtr proc = HalconAPI.PreCall(366);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.Store(proc, 3, row1);
			HalconAPI.Store(proc, 4, col1);
			HalconAPI.Store(proc, 5, disparity);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(col1);
			HalconAPI.UnpinTuple(disparity);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out z);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DisparityToPoint3d(HCamPar camParamRect2, HPose relPoseRect, double row1, double col1, double disparity, out double x, out double y, out double z)
		{
			IntPtr proc = HalconAPI.PreCall(366);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreD(proc, 3, row1);
			HalconAPI.StoreD(proc, 4, col1);
			HalconAPI.StoreD(proc, 5, disparity);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			err = HalconAPI.LoadD(proc, 0, err, out x);
			err = HalconAPI.LoadD(proc, 1, err, out y);
			err = HalconAPI.LoadD(proc, 2, err, out z);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DisparityToDistance(HCamPar camParamRect2, HPose relPoseRect, HTuple disparity)
		{
			IntPtr proc = HalconAPI.PreCall(367);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.Store(proc, 3, disparity);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HalconAPI.UnpinTuple(disparity);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double DisparityToDistance(HCamPar camParamRect2, HPose relPoseRect, double disparity)
		{
			IntPtr proc = HalconAPI.PreCall(367);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreD(proc, 3, disparity);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple DistanceToDisparity(HCamPar camParamRect2, HPose relPoseRect, HTuple distance)
		{
			IntPtr proc = HalconAPI.PreCall(368);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.Store(proc, 3, distance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			HalconAPI.UnpinTuple(distance);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double DistanceToDisparity(HCamPar camParamRect2, HPose relPoseRect, double distance)
		{
			IntPtr proc = HalconAPI.PreCall(368);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParamRect2);
			HalconAPI.Store(proc, 2, relPoseRect);
			HalconAPI.StoreD(proc, 3, distance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HImage GenBinocularRectificationMap(out HImage map2, HCamPar camParam2, HPose relPose, double subSampling, string method, string mapType, out HCamPar camParamRect1, out HCamPar camParamRect2, out HPose camPoseRect1, out HPose camPoseRect2, out HPose relPoseRect)
		{
			IntPtr proc = HalconAPI.PreCall(369);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParam2);
			HalconAPI.Store(proc, 2, relPose);
			HalconAPI.StoreD(proc, 3, subSampling);
			HalconAPI.StoreS(proc, 4, method);
			HalconAPI.StoreS(proc, 5, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParam2);
			HalconAPI.UnpinTuple(relPose);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out map2);
			err = LoadNew(proc, 0, err, out camParamRect1);
			err = LoadNew(proc, 1, err, out camParamRect2);
			err = HPose.LoadNew(proc, 2, err, out camPoseRect1);
			err = HPose.LoadNew(proc, 3, err, out camPoseRect2);
			err = HPose.LoadNew(proc, 4, err, out relPoseRect);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HCamPar BinocularCalibration(HTuple NX, HTuple NY, HTuple NZ, HTuple NRow1, HTuple NCol1, HTuple NRow2, HTuple NCol2, HCamPar startCamParam2, HPose[] NStartPose1, HPose[] NStartPose2, HTuple estimateParams, out HCamPar camParam2, out HPose[] NFinalPose1, out HPose[] NFinalPose2, out HPose relPose, out HTuple errors)
		{
			HTuple hTuple = HData.ConcatArray(NStartPose1);
			HTuple hTuple2 = HData.ConcatArray(NStartPose2);
			IntPtr proc = HalconAPI.PreCall(370);
			Store(proc, 7);
			HalconAPI.Store(proc, 0, NX);
			HalconAPI.Store(proc, 1, NY);
			HalconAPI.Store(proc, 2, NZ);
			HalconAPI.Store(proc, 3, NRow1);
			HalconAPI.Store(proc, 4, NCol1);
			HalconAPI.Store(proc, 5, NRow2);
			HalconAPI.Store(proc, 6, NCol2);
			HalconAPI.Store(proc, 8, startCamParam2);
			HalconAPI.Store(proc, 9, hTuple);
			HalconAPI.Store(proc, 10, hTuple2);
			HalconAPI.Store(proc, 11, estimateParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(NX);
			HalconAPI.UnpinTuple(NY);
			HalconAPI.UnpinTuple(NZ);
			HalconAPI.UnpinTuple(NRow1);
			HalconAPI.UnpinTuple(NCol1);
			HalconAPI.UnpinTuple(NRow2);
			HalconAPI.UnpinTuple(NCol2);
			HalconAPI.UnpinTuple(startCamParam2);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(estimateParams);
			err = LoadNew(proc, 0, err, out HCamPar obj);
			err = LoadNew(proc, 1, err, out camParam2);
			err = HTuple.LoadNew(proc, 2, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 3, err, out HTuple tuple2);
			err = HPose.LoadNew(proc, 4, err, out relPose);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out errors);
			HalconAPI.PostCall(proc, err);
			NFinalPose1 = HPose.SplitArray(tuple);
			NFinalPose2 = HPose.SplitArray(tuple2);
			GC.KeepAlive(this);
			return obj;
		}

		public HCamPar BinocularCalibration(HTuple NX, HTuple NY, HTuple NZ, HTuple NRow1, HTuple NCol1, HTuple NRow2, HTuple NCol2, HCamPar startCamParam2, HPose NStartPose1, HPose NStartPose2, HTuple estimateParams, out HCamPar camParam2, out HPose NFinalPose1, out HPose NFinalPose2, out HPose relPose, out double errors)
		{
			IntPtr proc = HalconAPI.PreCall(370);
			Store(proc, 7);
			HalconAPI.Store(proc, 0, NX);
			HalconAPI.Store(proc, 1, NY);
			HalconAPI.Store(proc, 2, NZ);
			HalconAPI.Store(proc, 3, NRow1);
			HalconAPI.Store(proc, 4, NCol1);
			HalconAPI.Store(proc, 5, NRow2);
			HalconAPI.Store(proc, 6, NCol2);
			HalconAPI.Store(proc, 8, startCamParam2);
			HalconAPI.Store(proc, 9, NStartPose1);
			HalconAPI.Store(proc, 10, NStartPose2);
			HalconAPI.Store(proc, 11, estimateParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(NX);
			HalconAPI.UnpinTuple(NY);
			HalconAPI.UnpinTuple(NZ);
			HalconAPI.UnpinTuple(NRow1);
			HalconAPI.UnpinTuple(NCol1);
			HalconAPI.UnpinTuple(NRow2);
			HalconAPI.UnpinTuple(NCol2);
			HalconAPI.UnpinTuple(startCamParam2);
			HalconAPI.UnpinTuple(NStartPose1);
			HalconAPI.UnpinTuple(NStartPose2);
			HalconAPI.UnpinTuple(estimateParams);
			err = LoadNew(proc, 0, err, out HCamPar obj);
			err = LoadNew(proc, 1, err, out camParam2);
			err = HPose.LoadNew(proc, 2, err, out NFinalPose1);
			err = HPose.LoadNew(proc, 3, err, out NFinalPose2);
			err = HPose.LoadNew(proc, 4, err, out relPose);
			err = HalconAPI.LoadD(proc, 5, err, out errors);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HPose[] FindCalibDescriptorModel(HImage image, HDescriptorModel modelID, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, HTuple minScore, int numMatches, HTuple scoreType, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(948);
			Store(proc, 7);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.Store(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.Store(proc, 8, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			HalconAPI.UnpinTuple(minScore);
			HalconAPI.UnpinTuple(scoreType);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out score);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(tuple);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(modelID);
			return result;
		}

		public HPose FindCalibDescriptorModel(HImage image, HDescriptorModel modelID, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, double minScore, int numMatches, string scoreType, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(948);
			Store(proc, 7);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, modelID);
			HalconAPI.Store(proc, 1, detectorParamName);
			HalconAPI.Store(proc, 2, detectorParamValue);
			HalconAPI.Store(proc, 3, descriptorParamName);
			HalconAPI.Store(proc, 4, descriptorParamValue);
			HalconAPI.StoreD(proc, 5, minScore);
			HalconAPI.StoreI(proc, 6, numMatches);
			HalconAPI.StoreS(proc, 8, scoreType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HalconAPI.LoadD(proc, 1, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(modelID);
			return obj;
		}

		public HDescriptorModel CreateCalibDescriptorModel(HImage template, HPose referencePose, string detectorType, HTuple detectorParamName, HTuple detectorParamValue, HTuple descriptorParamName, HTuple descriptorParamValue, int seed)
		{
			IntPtr proc = HalconAPI.PreCall(952);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreS(proc, 2, detectorType);
			HalconAPI.Store(proc, 3, detectorParamName);
			HalconAPI.Store(proc, 4, detectorParamValue);
			HalconAPI.Store(proc, 5, descriptorParamName);
			HalconAPI.Store(proc, 6, descriptorParamValue);
			HalconAPI.StoreI(proc, 7, seed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(detectorParamName);
			HalconAPI.UnpinTuple(detectorParamValue);
			HalconAPI.UnpinTuple(descriptorParamName);
			HalconAPI.UnpinTuple(descriptorParamValue);
			err = HDescriptorModel.LoadNew(proc, 0, err, out HDescriptorModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
			return obj;
		}

		public HDeformableModel CreatePlanarCalibDeformableModelXld(HXLDCont contours, HPose referencePose, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(976);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HDeformableModel.LoadNew(proc, 0, err, out HDeformableModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
			return obj;
		}

		public HDeformableModel CreatePlanarCalibDeformableModelXld(HXLDCont contours, HPose referencePose, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(976);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HDeformableModel.LoadNew(proc, 0, err, out HDeformableModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
			return obj;
		}

		public HDeformableModel CreatePlanarCalibDeformableModel(HImage template, HPose referencePose, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, HTuple contrast, HTuple minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(979);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.Store(proc, 14, contrast);
			HalconAPI.Store(proc, 15, minContrast);
			HalconAPI.Store(proc, 16, genParamName);
			HalconAPI.Store(proc, 17, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(minContrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HDeformableModel.LoadNew(proc, 0, err, out HDeformableModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
			return obj;
		}

		public HDeformableModel CreatePlanarCalibDeformableModel(HImage template, HPose referencePose, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, HTuple contrast, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(979);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, template);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.Store(proc, 14, contrast);
			HalconAPI.StoreI(proc, 15, minContrast);
			HalconAPI.Store(proc, 16, genParamName);
			HalconAPI.Store(proc, 17, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(contrast);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HDeformableModel.LoadNew(proc, 0, err, out HDeformableModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(template);
			return obj;
		}

		public HXLDCont ProjectShapeModel3d(HShapeModel3D shapeModel3DID, HPose pose, string hiddenSurfaceRemoval, HTuple minFaceAngle)
		{
			IntPtr proc = HalconAPI.PreCall(1055);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, shapeModel3DID);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.StoreS(proc, 3, hiddenSurfaceRemoval);
			HalconAPI.Store(proc, 4, minFaceAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(pose);
			HalconAPI.UnpinTuple(minFaceAngle);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(shapeModel3DID);
			return obj;
		}

		public HXLDCont ProjectShapeModel3d(HShapeModel3D shapeModel3DID, HPose pose, string hiddenSurfaceRemoval, double minFaceAngle)
		{
			IntPtr proc = HalconAPI.PreCall(1055);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, shapeModel3DID);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.StoreS(proc, 3, hiddenSurfaceRemoval);
			HalconAPI.StoreD(proc, 4, minFaceAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(pose);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(shapeModel3DID);
			return obj;
		}

		public HShapeModel3D CreateShapeModel3d(HObjectModel3D objectModel3D, double refRotX, double refRotY, double refRotZ, string orderOfRotation, double longitudeMin, double longitudeMax, double latitudeMin, double latitudeMax, double camRollMin, double camRollMax, double distMin, double distMax, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1059);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 2, refRotX);
			HalconAPI.StoreD(proc, 3, refRotY);
			HalconAPI.StoreD(proc, 4, refRotZ);
			HalconAPI.StoreS(proc, 5, orderOfRotation);
			HalconAPI.StoreD(proc, 6, longitudeMin);
			HalconAPI.StoreD(proc, 7, longitudeMax);
			HalconAPI.StoreD(proc, 8, latitudeMin);
			HalconAPI.StoreD(proc, 9, latitudeMax);
			HalconAPI.StoreD(proc, 10, camRollMin);
			HalconAPI.StoreD(proc, 11, camRollMax);
			HalconAPI.StoreD(proc, 12, distMin);
			HalconAPI.StoreD(proc, 13, distMax);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HShapeModel3D.LoadNew(proc, 0, err, out HShapeModel3D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public HShapeModel3D CreateShapeModel3d(HObjectModel3D objectModel3D, double refRotX, double refRotY, double refRotZ, string orderOfRotation, double longitudeMin, double longitudeMax, double latitudeMin, double latitudeMax, double camRollMin, double camRollMax, double distMin, double distMax, int minContrast, string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1059);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreD(proc, 2, refRotX);
			HalconAPI.StoreD(proc, 3, refRotY);
			HalconAPI.StoreD(proc, 4, refRotZ);
			HalconAPI.StoreS(proc, 5, orderOfRotation);
			HalconAPI.StoreD(proc, 6, longitudeMin);
			HalconAPI.StoreD(proc, 7, longitudeMax);
			HalconAPI.StoreD(proc, 8, latitudeMin);
			HalconAPI.StoreD(proc, 9, latitudeMax);
			HalconAPI.StoreD(proc, 10, camRollMin);
			HalconAPI.StoreD(proc, 11, camRollMax);
			HalconAPI.StoreD(proc, 12, distMin);
			HalconAPI.StoreD(proc, 13, distMax);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.StoreS(proc, 15, genParamName);
			HalconAPI.StoreI(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HShapeModel3D.LoadNew(proc, 0, err, out HShapeModel3D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public HObjectModel3D[] ReduceObjectModel3dByView(HRegion region, HObjectModel3D[] objectModel3D, HPose[] pose)
		{
			HTuple hTuple = HHandleBase.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1084);
			Store(proc, 1);
			HalconAPI.Store(proc, 1, region);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 2, hTuple2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			err = HObjectModel3D.LoadNew(proc, 0, err, out HObjectModel3D[] obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public HObjectModel3D ReduceObjectModel3dByView(HRegion region, HObjectModel3D objectModel3D, HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(1084);
			Store(proc, 1);
			HalconAPI.Store(proc, 1, region);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(pose);
			err = HObjectModel3D.LoadNew(proc, 0, err, out HObjectModel3D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public HImage RenderObjectModel3d(HObjectModel3D[] objectModel3D, HPose[] pose, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HHandleBase.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1088);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, hTuple);
			HalconAPI.Store(proc, 2, hTuple2);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public HImage RenderObjectModel3d(HObjectModel3D objectModel3D, HPose pose, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1088);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(pose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public void DispObjectModel3d(HWindow windowHandle, HObjectModel3D[] objectModel3D, HPose[] pose, HTuple genParamName, HTuple genParamValue)
		{
			HTuple hTuple = HHandleBase.ConcatArray(objectModel3D);
			HTuple hTuple2 = HData.ConcatArray(pose);
			IntPtr proc = HalconAPI.PreCall(1089);
			Store(proc, 2);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, hTuple);
			HalconAPI.Store(proc, 3, hTuple2);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(hTuple2);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
			GC.KeepAlive(objectModel3D);
		}

		public void DispObjectModel3d(HWindow windowHandle, HObjectModel3D objectModel3D, HPose pose, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1089);
			Store(proc, 2);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.Store(proc, 1, objectModel3D);
			HalconAPI.Store(proc, 3, pose);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(pose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
			GC.KeepAlive(objectModel3D);
		}

		public HImage ObjectModel3dToXyz(out HImage y, out HImage z, HObjectModel3D objectModel3D, string type, HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(1092);
			Store(proc, 2);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.StoreS(proc, 1, type);
			HalconAPI.Store(proc, 3, pose);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(pose);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out y);
			err = HImage.LoadNew(proc, 3, err, out z);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public HXLDCont ProjectObjectModel3d(HObjectModel3D objectModel3D, HPose pose, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1095);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(pose);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public HXLDCont ProjectObjectModel3d(HObjectModel3D objectModel3D, HPose pose, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1095);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.StoreS(proc, 3, genParamName);
			HalconAPI.StoreS(proc, 4, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(pose);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
			return obj;
		}

		public int AddScene3dCamera(HScene3D scene3D)
		{
			IntPtr proc = HalconAPI.PreCall(1218);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, scene3D);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(scene3D);
			return intValue;
		}

		public HObjectModel3D[] SceneFlowCalib(HImage imageRect1T1, HImage imageRect2T1, HImage imageRect1T2, HImage imageRect2T2, HImage disparity, HTuple smoothingFlow, HTuple smoothingDisparity, HTuple genParamName, HTuple genParamValue, HCamPar camParamRect2, HPose relPoseRect)
		{
			IntPtr proc = HalconAPI.PreCall(1481);
			Store(proc, 4);
			HalconAPI.Store(proc, 1, imageRect1T1);
			HalconAPI.Store(proc, 2, imageRect2T1);
			HalconAPI.Store(proc, 3, imageRect1T2);
			HalconAPI.Store(proc, 4, imageRect2T2);
			HalconAPI.Store(proc, 5, disparity);
			HalconAPI.Store(proc, 0, smoothingFlow);
			HalconAPI.Store(proc, 1, smoothingDisparity);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.Store(proc, 5, camParamRect2);
			HalconAPI.Store(proc, 6, relPoseRect);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(smoothingFlow);
			HalconAPI.UnpinTuple(smoothingDisparity);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			err = HObjectModel3D.LoadNew(proc, 0, err, out HObjectModel3D[] obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1T1);
			GC.KeepAlive(imageRect2T1);
			GC.KeepAlive(imageRect1T2);
			GC.KeepAlive(imageRect2T2);
			GC.KeepAlive(disparity);
			return obj;
		}

		public HObjectModel3D SceneFlowCalib(HImage imageRect1T1, HImage imageRect2T1, HImage imageRect1T2, HImage imageRect2T2, HImage disparity, double smoothingFlow, double smoothingDisparity, string genParamName, string genParamValue, HCamPar camParamRect2, HPose relPoseRect)
		{
			IntPtr proc = HalconAPI.PreCall(1481);
			Store(proc, 4);
			HalconAPI.Store(proc, 1, imageRect1T1);
			HalconAPI.Store(proc, 2, imageRect2T1);
			HalconAPI.Store(proc, 3, imageRect1T2);
			HalconAPI.Store(proc, 4, imageRect2T2);
			HalconAPI.Store(proc, 5, disparity);
			HalconAPI.StoreD(proc, 0, smoothingFlow);
			HalconAPI.StoreD(proc, 1, smoothingDisparity);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.Store(proc, 5, camParamRect2);
			HalconAPI.Store(proc, 6, relPoseRect);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamRect2);
			HalconAPI.UnpinTuple(relPoseRect);
			err = HObjectModel3D.LoadNew(proc, 0, err, out HObjectModel3D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageRect1T1);
			GC.KeepAlive(imageRect2T1);
			GC.KeepAlive(imageRect1T2);
			GC.KeepAlive(imageRect2T2);
			GC.KeepAlive(disparity);
			return obj;
		}

		public HPose VectorToPose(HTuple worldX, HTuple worldY, HTuple worldZ, HTuple imageRow, HTuple imageColumn, string method, HTuple qualityType, out HTuple quality)
		{
			IntPtr proc = HalconAPI.PreCall(1902);
			Store(proc, 5);
			HalconAPI.Store(proc, 0, worldX);
			HalconAPI.Store(proc, 1, worldY);
			HalconAPI.Store(proc, 2, worldZ);
			HalconAPI.Store(proc, 3, imageRow);
			HalconAPI.Store(proc, 4, imageColumn);
			HalconAPI.StoreS(proc, 6, method);
			HalconAPI.Store(proc, 7, qualityType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(worldX);
			HalconAPI.UnpinTuple(worldY);
			HalconAPI.UnpinTuple(worldZ);
			HalconAPI.UnpinTuple(imageRow);
			HalconAPI.UnpinTuple(imageColumn);
			HalconAPI.UnpinTuple(qualityType);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, err, out quality);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HPose VectorToPose(HTuple worldX, HTuple worldY, HTuple worldZ, HTuple imageRow, HTuple imageColumn, string method, string qualityType, out double quality)
		{
			IntPtr proc = HalconAPI.PreCall(1902);
			Store(proc, 5);
			HalconAPI.Store(proc, 0, worldX);
			HalconAPI.Store(proc, 1, worldY);
			HalconAPI.Store(proc, 2, worldZ);
			HalconAPI.Store(proc, 3, imageRow);
			HalconAPI.Store(proc, 4, imageColumn);
			HalconAPI.StoreS(proc, 6, method);
			HalconAPI.StoreS(proc, 7, qualityType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(worldX);
			HalconAPI.UnpinTuple(worldY);
			HalconAPI.UnpinTuple(worldZ);
			HalconAPI.UnpinTuple(imageRow);
			HalconAPI.UnpinTuple(imageColumn);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HalconAPI.LoadD(proc, 1, err, out quality);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont RadialDistortionSelfCalibration(HXLDCont contours, int width, int height, double inlierThreshold, int randSeed, string distortionModel, string distortionCenter, double principalPointVar)
		{
			IntPtr proc = HalconAPI.PreCall(1904);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.StoreD(proc, 2, inlierThreshold);
			HalconAPI.StoreI(proc, 3, randSeed);
			HalconAPI.StoreS(proc, 4, distortionModel);
			HalconAPI.StoreS(proc, 5, distortionCenter);
			HalconAPI.StoreD(proc, 6, principalPointVar);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
			return obj;
		}

		public HHomMat2D CamParToCamMat(out int imageWidth, out int imageHeight)
		{
			IntPtr proc = HalconAPI.PreCall(1905);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HHomMat2D.LoadNew(proc, 0, err, out HHomMat2D obj);
			err = HalconAPI.LoadI(proc, 1, err, out imageWidth);
			err = HalconAPI.LoadI(proc, 2, err, out imageHeight);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void CamMatToCamPar(HHomMat2D cameraMatrix, double kappa, int imageWidth, int imageHeight)
		{
			IntPtr proc = HalconAPI.PreCall(1906);
			HalconAPI.Store(proc, 0, cameraMatrix);
			HalconAPI.StoreD(proc, 1, kappa);
			HalconAPI.StoreI(proc, 2, imageWidth);
			HalconAPI.StoreI(proc, 3, imageHeight);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraMatrix);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HPose[] GetRectanglePose(HXLD contour, HTuple width, HTuple height, string weightingMode, double clippingFactor, out HTuple covPose, out HTuple error)
		{
			IntPtr proc = HalconAPI.PreCall(1908);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.Store(proc, 1, width);
			HalconAPI.Store(proc, 2, height);
			HalconAPI.StoreS(proc, 3, weightingMode);
			HalconAPI.StoreD(proc, 4, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(width);
			HalconAPI.UnpinTuple(height);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(tuple);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
			return result;
		}

		public HPose GetRectanglePose(HXLD contour, double width, double height, string weightingMode, double clippingFactor, out HTuple covPose, out HTuple error)
		{
			IntPtr proc = HalconAPI.PreCall(1908);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.StoreD(proc, 1, width);
			HalconAPI.StoreD(proc, 2, height);
			HalconAPI.StoreS(proc, 3, weightingMode);
			HalconAPI.StoreD(proc, 4, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
			return obj;
		}

		public HTuple GetCirclePose(HXLD contour, HTuple radius, string outputType, out HTuple pose2)
		{
			IntPtr proc = HalconAPI.PreCall(1909);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.Store(proc, 1, radius);
			HalconAPI.StoreS(proc, 2, outputType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(radius);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out pose2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
			return tuple;
		}

		public HTuple GetCirclePose(HXLD contour, double radius, string outputType, out HTuple pose2)
		{
			IntPtr proc = HalconAPI.PreCall(1909);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.StoreD(proc, 1, radius);
			HalconAPI.StoreS(proc, 2, outputType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out pose2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
			return tuple;
		}

		public HImage GenRadialDistortionMap(HCamPar camParamOut, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1912);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParamOut);
			HalconAPI.StoreS(proc, 2, mapType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamOut);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage GenImageToWorldPlaneMap(HPose worldPose, int widthIn, int heightIn, int widthMapped, int heightMapped, HTuple scale, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1913);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.StoreI(proc, 2, widthIn);
			HalconAPI.StoreI(proc, 3, heightIn);
			HalconAPI.StoreI(proc, 4, widthMapped);
			HalconAPI.StoreI(proc, 5, heightMapped);
			HalconAPI.Store(proc, 6, scale);
			HalconAPI.StoreS(proc, 7, mapType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(worldPose);
			HalconAPI.UnpinTuple(scale);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage GenImageToWorldPlaneMap(HPose worldPose, int widthIn, int heightIn, int widthMapped, int heightMapped, string scale, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1913);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.StoreI(proc, 2, widthIn);
			HalconAPI.StoreI(proc, 3, heightIn);
			HalconAPI.StoreI(proc, 4, widthMapped);
			HalconAPI.StoreI(proc, 5, heightMapped);
			HalconAPI.StoreS(proc, 6, scale);
			HalconAPI.StoreS(proc, 7, mapType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(worldPose);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage ImageToWorldPlane(HImage image, HPose worldPose, int width, int height, HTuple scale, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1914);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.Store(proc, 4, scale);
			HalconAPI.StoreS(proc, 5, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(worldPose);
			HalconAPI.UnpinTuple(scale);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HImage ImageToWorldPlane(HImage image, HPose worldPose, int width, int height, string scale, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(1914);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.StoreS(proc, 4, scale);
			HalconAPI.StoreS(proc, 5, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(worldPose);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void ImagePointsToWorldPlane(HPose worldPose, HTuple rows, HTuple cols, HTuple scale, out HTuple x, out HTuple y)
		{
			IntPtr proc = HalconAPI.PreCall(1916);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.Store(proc, 2, rows);
			HalconAPI.Store(proc, 3, cols);
			HalconAPI.Store(proc, 4, scale);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(worldPose);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(cols);
			HalconAPI.UnpinTuple(scale);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ImagePointsToWorldPlane(HPose worldPose, HTuple rows, HTuple cols, string scale, out HTuple x, out HTuple y)
		{
			IntPtr proc = HalconAPI.PreCall(1916);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.Store(proc, 2, rows);
			HalconAPI.Store(proc, 3, cols);
			HalconAPI.StoreS(proc, 4, scale);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(worldPose);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(cols);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HPose HandEyeCalibration(HTuple x, HTuple y, HTuple z, HTuple row, HTuple col, HTuple numPoints, HPose[] robotPoses, string method, HTuple qualityType, out HPose calibrationPose, out HTuple quality)
		{
			HTuple hTuple = HData.ConcatArray(robotPoses);
			IntPtr proc = HalconAPI.PreCall(1918);
			Store(proc, 7);
			HalconAPI.Store(proc, 0, x);
			HalconAPI.Store(proc, 1, y);
			HalconAPI.Store(proc, 2, z);
			HalconAPI.Store(proc, 3, row);
			HalconAPI.Store(proc, 4, col);
			HalconAPI.Store(proc, 5, numPoints);
			HalconAPI.Store(proc, 6, hTuple);
			HalconAPI.StoreS(proc, 8, method);
			HalconAPI.Store(proc, 9, qualityType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(x);
			HalconAPI.UnpinTuple(y);
			HalconAPI.UnpinTuple(z);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(numPoints);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(qualityType);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HPose.LoadNew(proc, 1, err, out calibrationPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out quality);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HPose HandEyeCalibration(HTuple x, HTuple y, HTuple z, HTuple row, HTuple col, HTuple numPoints, HPose[] robotPoses, string method, string qualityType, out HPose calibrationPose, out double quality)
		{
			HTuple hTuple = HData.ConcatArray(robotPoses);
			IntPtr proc = HalconAPI.PreCall(1918);
			Store(proc, 7);
			HalconAPI.Store(proc, 0, x);
			HalconAPI.Store(proc, 1, y);
			HalconAPI.Store(proc, 2, z);
			HalconAPI.Store(proc, 3, row);
			HalconAPI.Store(proc, 4, col);
			HalconAPI.Store(proc, 5, numPoints);
			HalconAPI.Store(proc, 6, hTuple);
			HalconAPI.StoreS(proc, 8, method);
			HalconAPI.StoreS(proc, 9, qualityType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(x);
			HalconAPI.UnpinTuple(y);
			HalconAPI.UnpinTuple(z);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(numPoints);
			HalconAPI.UnpinTuple(hTuple);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HPose.LoadNew(proc, 1, err, out calibrationPose);
			err = HalconAPI.LoadD(proc, 2, err, out quality);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ChangeRadialDistortionContoursXld(HXLDCont contours, HCamPar camParamOut)
		{
			IntPtr proc = HalconAPI.PreCall(1922);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, contours);
			HalconAPI.Store(proc, 1, camParamOut);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamOut);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours);
			return obj;
		}

		public void ChangeRadialDistortionPoints(HTuple row, HTuple col, HCamPar camParamOut, out HTuple rowChanged, out HTuple colChanged)
		{
			IntPtr proc = HalconAPI.PreCall(1923);
			Store(proc, 2);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.Store(proc, 3, camParamOut);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(camParamOut);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowChanged);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out colChanged);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage ChangeRadialDistortionImage(HImage image, HRegion region, HCamPar camParamOut)
		{
			IntPtr proc = HalconAPI.PreCall(1924);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 2, region);
			HalconAPI.Store(proc, 1, camParamOut);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(camParamOut);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(region);
			return obj;
		}

		public HCamPar ChangeRadialDistortionCamPar(string mode, HTuple distortionCoeffs)
		{
			IntPtr proc = HalconAPI.PreCall(1925);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 2, distortionCoeffs);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(distortionCoeffs);
			err = LoadNew(proc, 0, err, out HCamPar obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HCamPar ChangeRadialDistortionCamPar(string mode, double distortionCoeffs)
		{
			IntPtr proc = HalconAPI.PreCall(1925);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 2, distortionCoeffs);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HCamPar obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void GetLineOfSight(HTuple row, HTuple column, out HTuple PX, out HTuple PY, out HTuple PZ, out HTuple QX, out HTuple QY, out HTuple QZ)
		{
			IntPtr proc = HalconAPI.PreCall(1929);
			Store(proc, 2);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out PX);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out PY);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out PZ);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out QX);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out QY);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out QZ);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void Project3dPoint(HTuple x, HTuple y, HTuple z, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1932);
			Store(proc, 3);
			HalconAPI.Store(proc, 0, x);
			HalconAPI.Store(proc, 1, y);
			HalconAPI.Store(proc, 2, z);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(x);
			HalconAPI.UnpinTuple(y);
			HalconAPI.UnpinTuple(z);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HHomMat3D CamParPoseToHomMat3d(HPose pose)
		{
			IntPtr proc = HalconAPI.PreCall(1933);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, pose);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(pose);
			err = HHomMat3D.LoadNew(proc, 0, err, out HHomMat3D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void DeserializeCamPar(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1936);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeCamPar()
		{
			IntPtr proc = HalconAPI.PreCall(1937);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadCamPar(string camParFile)
		{
			IntPtr proc = HalconAPI.PreCall(1942);
			HalconAPI.StoreS(proc, 0, camParFile);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteCamPar(string camParFile)
		{
			IntPtr proc = HalconAPI.PreCall(1943);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, camParFile);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HImage SimCaltab(string calPlateDescr, HPose calPlatePose, int grayBackground, int grayPlate, int grayMarks, double scaleFac)
		{
			IntPtr proc = HalconAPI.PreCall(1944);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, calPlateDescr);
			HalconAPI.Store(proc, 2, calPlatePose);
			HalconAPI.StoreI(proc, 3, grayBackground);
			HalconAPI.StoreI(proc, 4, grayPlate);
			HalconAPI.StoreI(proc, 5, grayMarks);
			HalconAPI.StoreD(proc, 6, scaleFac);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(calPlatePose);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void DispCaltab(HWindow windowHandle, string calPlateDescr, HPose calPlatePose, double scaleFac)
		{
			IntPtr proc = HalconAPI.PreCall(1945);
			Store(proc, 2);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, calPlateDescr);
			HalconAPI.Store(proc, 3, calPlatePose);
			HalconAPI.StoreD(proc, 4, scaleFac);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(calPlatePose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public HCamPar CameraCalibration(HTuple NX, HTuple NY, HTuple NZ, HTuple NRow, HTuple NCol, HPose[] NStartPose, HTuple estimateParams, out HPose[] NFinalPose, out HTuple errors)
		{
			HTuple hTuple = HData.ConcatArray(NStartPose);
			IntPtr proc = HalconAPI.PreCall(1946);
			Store(proc, 5);
			HalconAPI.Store(proc, 0, NX);
			HalconAPI.Store(proc, 1, NY);
			HalconAPI.Store(proc, 2, NZ);
			HalconAPI.Store(proc, 3, NRow);
			HalconAPI.Store(proc, 4, NCol);
			HalconAPI.Store(proc, 6, hTuple);
			HalconAPI.Store(proc, 7, estimateParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(NX);
			HalconAPI.UnpinTuple(NY);
			HalconAPI.UnpinTuple(NZ);
			HalconAPI.UnpinTuple(NRow);
			HalconAPI.UnpinTuple(NCol);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.UnpinTuple(estimateParams);
			err = LoadNew(proc, 0, err, out HCamPar obj);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out errors);
			HalconAPI.PostCall(proc, err);
			NFinalPose = HPose.SplitArray(tuple);
			GC.KeepAlive(this);
			return obj;
		}

		public HCamPar CameraCalibration(HTuple NX, HTuple NY, HTuple NZ, HTuple NRow, HTuple NCol, HPose NStartPose, HTuple estimateParams, out HPose NFinalPose, out double errors)
		{
			IntPtr proc = HalconAPI.PreCall(1946);
			Store(proc, 5);
			HalconAPI.Store(proc, 0, NX);
			HalconAPI.Store(proc, 1, NY);
			HalconAPI.Store(proc, 2, NZ);
			HalconAPI.Store(proc, 3, NRow);
			HalconAPI.Store(proc, 4, NCol);
			HalconAPI.Store(proc, 6, NStartPose);
			HalconAPI.Store(proc, 7, estimateParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(NX);
			HalconAPI.UnpinTuple(NY);
			HalconAPI.UnpinTuple(NZ);
			HalconAPI.UnpinTuple(NRow);
			HalconAPI.UnpinTuple(NCol);
			HalconAPI.UnpinTuple(NStartPose);
			HalconAPI.UnpinTuple(estimateParams);
			err = LoadNew(proc, 0, err, out HCamPar obj);
			err = HPose.LoadNew(proc, 1, err, out NFinalPose);
			err = HalconAPI.LoadD(proc, 2, err, out errors);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple FindMarksAndPose(HImage image, HRegion calPlateRegion, string calPlateDescr, int startThresh, int deltaThresh, int minThresh, double alpha, double minContLength, double maxDiamMarks, out HTuple CCoord, out HPose startPose)
		{
			IntPtr proc = HalconAPI.PreCall(1947);
			Store(proc, 1);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 2, calPlateRegion);
			HalconAPI.StoreS(proc, 0, calPlateDescr);
			HalconAPI.StoreI(proc, 2, startThresh);
			HalconAPI.StoreI(proc, 3, deltaThresh);
			HalconAPI.StoreI(proc, 4, minThresh);
			HalconAPI.StoreD(proc, 5, alpha);
			HalconAPI.StoreD(proc, 6, minContLength);
			HalconAPI.StoreD(proc, 7, maxDiamMarks);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out CCoord);
			err = HPose.LoadNew(proc, 2, err, out startPose);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(calPlateRegion);
			return tuple;
		}

		public void SetCameraSetupCamParam(HCameraSetupModel cameraSetupModelID, HTuple cameraIdx, HTuple cameraType, HTuple cameraPose)
		{
			IntPtr proc = HalconAPI.PreCall(1957);
			Store(proc, 3);
			HalconAPI.Store(proc, 0, cameraSetupModelID);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.Store(proc, 2, cameraType);
			HalconAPI.Store(proc, 4, cameraPose);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraType);
			HalconAPI.UnpinTuple(cameraPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(cameraSetupModelID);
		}

		public void SetCameraSetupCamParam(HCameraSetupModel cameraSetupModelID, HTuple cameraIdx, string cameraType, HTuple cameraPose)
		{
			IntPtr proc = HalconAPI.PreCall(1957);
			Store(proc, 3);
			HalconAPI.Store(proc, 0, cameraSetupModelID);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, cameraType);
			HalconAPI.Store(proc, 4, cameraPose);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(cameraSetupModelID);
		}

		public void SetCalibDataCamParam(HCalibData calibDataID, HTuple cameraIdx, HTuple cameraType)
		{
			IntPtr proc = HalconAPI.PreCall(1979);
			Store(proc, 3);
			HalconAPI.Store(proc, 0, calibDataID);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.Store(proc, 2, cameraType);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraType);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(calibDataID);
		}

		public void SetCalibDataCamParam(HCalibData calibDataID, HTuple cameraIdx, string cameraType)
		{
			IntPtr proc = HalconAPI.PreCall(1979);
			Store(proc, 3);
			HalconAPI.Store(proc, 0, calibDataID);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, cameraType);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(calibDataID);
		}
	}
}
