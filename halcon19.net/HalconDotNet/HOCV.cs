using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HOCV : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCV()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCV(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCV(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("ocv");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCV obj)
		{
			obj = new HOCV(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCV[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HOCV[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HOCV(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HOCV(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(642);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCV(HTuple patternNames)
		{
			IntPtr proc = HalconAPI.PreCall(646);
			HalconAPI.Store(proc, 0, patternNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(patternNames);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeOcv();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCV(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeOcv(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeOcv();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HOCV Deserialize(Stream stream)
		{
			HOCV hOCV = new HOCV();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hOCV.DeserializeOcv(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCV;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HOCV Clone()
		{
			HSerializedItem hSerializedItem = SerializeOcv();
			HOCV hOCV = new HOCV();
			hOCV.DeserializeOcv(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCV;
		}

		public HTuple DoOcvSimple(HImage pattern, HTuple patternName, string adaptPos, string adaptSize, string adaptAngle, string adaptGray, double threshold)
		{
			IntPtr proc = HalconAPI.PreCall(638);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, pattern);
			HalconAPI.Store(proc, 1, patternName);
			HalconAPI.StoreS(proc, 2, adaptPos);
			HalconAPI.StoreS(proc, 3, adaptSize);
			HalconAPI.StoreS(proc, 4, adaptAngle);
			HalconAPI.StoreS(proc, 5, adaptGray);
			HalconAPI.StoreD(proc, 6, threshold);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(patternName);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
			return tuple;
		}

		public double DoOcvSimple(HImage pattern, string patternName, string adaptPos, string adaptSize, string adaptAngle, string adaptGray, double threshold)
		{
			IntPtr proc = HalconAPI.PreCall(638);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, pattern);
			HalconAPI.StoreS(proc, 1, patternName);
			HalconAPI.StoreS(proc, 2, adaptPos);
			HalconAPI.StoreS(proc, 3, adaptSize);
			HalconAPI.StoreS(proc, 4, adaptAngle);
			HalconAPI.StoreS(proc, 5, adaptGray);
			HalconAPI.StoreD(proc, 6, threshold);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
			return doubleValue;
		}

		public void TraindOcvProj(HImage pattern, HTuple name, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(639);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, pattern);
			HalconAPI.Store(proc, 1, name);
			HalconAPI.StoreS(proc, 2, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(name);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
		}

		public void TraindOcvProj(HImage pattern, string name, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(639);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, pattern);
			HalconAPI.StoreS(proc, 1, name);
			HalconAPI.StoreS(proc, 2, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
		}

		public void DeserializeOcv(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(640);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeOcv()
		{
			IntPtr proc = HalconAPI.PreCall(641);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadOcv(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(642);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteOcv(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(643);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CloseOcv()
		{
			IntPtr proc = HalconAPI.PreCall(645);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateOcvProj(HTuple patternNames)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(646);
			HalconAPI.Store(proc, 0, patternNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(patternNames);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcvProj(string patternNames)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(646);
			HalconAPI.StoreS(proc, 0, patternNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
