using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HRegion : HObject, ISerializable, ICloneable
	{
		public new HRegion this[HTuple index] => SelectObj(index);

		public HTuple Area
		{
			get
			{
				HTuple row;
				HTuple column;
				return AreaCenter(out row, out column);
			}
		}

		public HTuple Row
		{
			get
			{
				AreaCenter(out HTuple row, out HTuple _);
				return row;
			}
		}

		public HTuple Column
		{
			get
			{
				AreaCenter(out HTuple _, out HTuple column);
				return column;
			}
		}

		public HRegion()
			: base(HObjectBase.UNDEF, copy: false)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HRegion(IntPtr key)
			: this(key, copy: true)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HRegion(IntPtr key, bool copy)
			: base(key, copy)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HRegion(HObject obj)
			: base(obj)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		private void AssertObjectClass()
		{
			HalconAPI.AssertObjectClass(key, "region");
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, int err, out HRegion obj)
		{
			obj = new HRegion(HObjectBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		public HRegion(HTuple row1, HTuple column1, HTuple row2, HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(603);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion(double row1, double column1, double row2, double column2)
		{
			IntPtr proc = HalconAPI.PreCall(603);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion(HTuple row, HTuple column, HTuple phi, HTuple radius1, HTuple radius2, HTuple startAngle, HTuple endAngle)
		{
			IntPtr proc = HalconAPI.PreCall(608);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, phi);
			HalconAPI.Store(proc, 3, radius1);
			HalconAPI.Store(proc, 4, radius2);
			HalconAPI.Store(proc, 5, startAngle);
			HalconAPI.Store(proc, 6, endAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(radius1);
			HalconAPI.UnpinTuple(radius2);
			HalconAPI.UnpinTuple(startAngle);
			HalconAPI.UnpinTuple(endAngle);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion(double row, double column, double phi, double radius1, double radius2, double startAngle, double endAngle)
		{
			IntPtr proc = HalconAPI.PreCall(608);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, radius1);
			HalconAPI.StoreD(proc, 4, radius2);
			HalconAPI.StoreD(proc, 5, startAngle);
			HalconAPI.StoreD(proc, 6, endAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion(HTuple row, HTuple column, HTuple radius, HTuple startAngle, HTuple endAngle)
		{
			IntPtr proc = HalconAPI.PreCall(610);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.Store(proc, 3, startAngle);
			HalconAPI.Store(proc, 4, endAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(startAngle);
			HalconAPI.UnpinTuple(endAngle);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion(double row, double column, double radius, double startAngle, double endAngle)
		{
			IntPtr proc = HalconAPI.PreCall(610);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, startAngle);
			HalconAPI.StoreD(proc, 4, endAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion(HTuple row, HTuple column, HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(611);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radius);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion(double row, double column, double radius)
		{
			IntPtr proc = HalconAPI.PreCall(611);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeRegion();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HRegion(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeRegion(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeRegion();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HRegion Deserialize(Stream stream)
		{
			HRegion hRegion = new HRegion();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hRegion.DeserializeRegion(hSerializedItem);
			hSerializedItem.Dispose();
			return hRegion;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HRegion Clone()
		{
			HSerializedItem hSerializedItem = SerializeRegion();
			HRegion hRegion = new HRegion();
			hRegion.DeserializeRegion(hSerializedItem);
			hSerializedItem.Dispose();
			return hRegion;
		}

		public static HRegion operator &(HRegion region1, HRegion region2)
		{
			return region1.Intersection(region2);
		}

		public static HRegion operator |(HRegion region1, HRegion region2)
		{
			return region1.Union2(region2);
		}

		public static HRegion operator /(HRegion region1, HRegion region2)
		{
			return region1.Difference(region2);
		}

		public static HRegion operator !(HRegion region)
		{
			return region.Complement();
		}

		public static HRegion operator &(HRegion region, HImage image)
		{
			return region.Intersection(image.GetDomain());
		}

		public static bool operator <=(HRegion region1, HRegion region2)
		{
			int num = region1.CountObj();
			int num2 = region2.CountObj();
			if (num == 1 && num2 == 1)
			{
				return (int)(region1 / region2).Area == 0;
			}
			return false;
		}

		public static bool operator >=(HRegion region1, HRegion region2)
		{
			return region2 <= region1;
		}

		public static HRegion operator +(HRegion region1, HRegion region2)
		{
			return region1.MinkowskiAdd1(region2, 1);
		}

		public static HRegion operator -(HRegion region1, HRegion region2)
		{
			return region1.MinkowskiSub1(region2, 1);
		}

		public static HRegion operator +(HRegion region, double radius)
		{
			return region.DilationCircle(radius);
		}

		public static HRegion operator +(double radius, HRegion region)
		{
			return region.DilationCircle(radius);
		}

		public static HRegion operator -(HRegion region, double radius)
		{
			return region.ErosionCircle(radius);
		}

		public static HRegion operator +(HRegion region, Point p)
		{
			return region.MoveRegion(p.Y, p.X);
		}

		public static HRegion operator *(HRegion region, double factor)
		{
			return region.ZoomRegion(factor, factor);
		}

		public static HRegion operator *(double factor, HRegion region)
		{
			return region.ZoomRegion(factor, factor);
		}

		public static HRegion operator -(HRegion region)
		{
			return region.TransposeRegion(0, 0);
		}

		public static implicit operator HRegion(HXLDCont xld)
		{
			return xld.GenRegionContourXld("filled");
		}

		public static implicit operator HRegion(HXLDPoly xld)
		{
			return xld.GenRegionPolygonXld("filled");
		}

		public static implicit operator HXLDCont(HRegion region)
		{
			return region.GenContourRegionXld("border");
		}

		public HXLDCont GenContourRegionXld(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(70);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont GenContoursSkeletonXld(int length, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(73);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, length);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReceiveRegion(HSocket socket)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(327);
			HalconAPI.Store(proc, 0, socket);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(socket);
		}

		public void SendRegion(HSocket socket)
		{
			IntPtr proc = HalconAPI.PreCall(328);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, socket);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(socket);
		}

		public HSheetOfLightModel CreateSheetOfLightModel(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(391);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, genParamName);
			HalconAPI.Store(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HSheetOfLightModel.LoadNew(proc, 0, err, out HSheetOfLightModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HSheetOfLightModel CreateSheetOfLightModel(string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(391);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, genParamName);
			HalconAPI.StoreI(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSheetOfLightModel.LoadNew(proc, 0, err, out HSheetOfLightModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion SelectCharacters(string dotPrint, string strokeWidth, HTuple charWidth, HTuple charHeight, string punctuation, string diacriticMarks, string partitionMethod, string partitionLines, string fragmentDistance, string connectFragments, int clutterSizeMax, string stopAfter)
		{
			IntPtr proc = HalconAPI.PreCall(424);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, dotPrint);
			HalconAPI.StoreS(proc, 1, strokeWidth);
			HalconAPI.Store(proc, 2, charWidth);
			HalconAPI.Store(proc, 3, charHeight);
			HalconAPI.StoreS(proc, 4, punctuation);
			HalconAPI.StoreS(proc, 5, diacriticMarks);
			HalconAPI.StoreS(proc, 6, partitionMethod);
			HalconAPI.StoreS(proc, 7, partitionLines);
			HalconAPI.StoreS(proc, 8, fragmentDistance);
			HalconAPI.StoreS(proc, 9, connectFragments);
			HalconAPI.StoreI(proc, 10, clutterSizeMax);
			HalconAPI.StoreS(proc, 11, stopAfter);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(charWidth);
			HalconAPI.UnpinTuple(charHeight);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage SegmentCharacters(HImage image, out HRegion regionForeground, string method, string eliminateLines, string dotPrint, string strokeWidth, HTuple charWidth, HTuple charHeight, int thresholdOffset, int contrast, out HTuple usedThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(425);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.StoreS(proc, 1, eliminateLines);
			HalconAPI.StoreS(proc, 2, dotPrint);
			HalconAPI.StoreS(proc, 3, strokeWidth);
			HalconAPI.Store(proc, 4, charWidth);
			HalconAPI.Store(proc, 5, charHeight);
			HalconAPI.StoreI(proc, 6, thresholdOffset);
			HalconAPI.StoreI(proc, 7, contrast);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(charWidth);
			HalconAPI.UnpinTuple(charHeight);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = LoadNew(proc, 2, err, out regionForeground);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out usedThreshold);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HImage SegmentCharacters(HImage image, out HRegion regionForeground, string method, string eliminateLines, string dotPrint, string strokeWidth, HTuple charWidth, HTuple charHeight, int thresholdOffset, int contrast, out int usedThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(425);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, method);
			HalconAPI.StoreS(proc, 1, eliminateLines);
			HalconAPI.StoreS(proc, 2, dotPrint);
			HalconAPI.StoreS(proc, 3, strokeWidth);
			HalconAPI.Store(proc, 4, charWidth);
			HalconAPI.Store(proc, 5, charHeight);
			HalconAPI.StoreI(proc, 6, thresholdOffset);
			HalconAPI.StoreI(proc, 7, contrast);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(charWidth);
			HalconAPI.UnpinTuple(charHeight);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = LoadNew(proc, 2, err, out regionForeground);
			err = HalconAPI.LoadI(proc, 0, err, out usedThreshold);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HTuple TextLineSlant(HImage image, int charHeight, double slantFrom, double slantTo)
		{
			IntPtr proc = HalconAPI.PreCall(426);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreI(proc, 0, charHeight);
			HalconAPI.StoreD(proc, 1, slantFrom);
			HalconAPI.StoreD(proc, 2, slantTo);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple TextLineOrientation(HImage image, int charHeight, double orientationFrom, double orientationTo)
		{
			IntPtr proc = HalconAPI.PreCall(427);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreI(proc, 0, charHeight);
			HalconAPI.StoreD(proc, 1, orientationFrom);
			HalconAPI.StoreD(proc, 2, orientationTo);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple LearnNdimNorm(HRegion background, HImage image, string metric, HTuple distance, HTuple minNumberPercent, out HTuple center, out double quality)
		{
			IntPtr proc = HalconAPI.PreCall(437);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, background);
			HalconAPI.Store(proc, 3, image);
			HalconAPI.StoreS(proc, 0, metric);
			HalconAPI.Store(proc, 1, distance);
			HalconAPI.Store(proc, 2, minNumberPercent);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(distance);
			HalconAPI.UnpinTuple(minNumberPercent);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out center);
			err = HalconAPI.LoadD(proc, 2, err, out quality);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(background);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple LearnNdimNorm(HRegion background, HImage image, string metric, double distance, double minNumberPercent, out HTuple center, out double quality)
		{
			IntPtr proc = HalconAPI.PreCall(437);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, background);
			HalconAPI.Store(proc, 3, image);
			HalconAPI.StoreS(proc, 0, metric);
			HalconAPI.StoreD(proc, 1, distance);
			HalconAPI.StoreD(proc, 2, minNumberPercent);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out center);
			err = HalconAPI.LoadD(proc, 2, err, out quality);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(background);
			GC.KeepAlive(image);
			return tuple;
		}

		public void LearnNdimBox(HRegion background, HImage multiChannelImage, HClassBox classifHandle)
		{
			IntPtr proc = HalconAPI.PreCall(438);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, background);
			HalconAPI.Store(proc, 3, multiChannelImage);
			HalconAPI.Store(proc, 0, classifHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(background);
			GC.KeepAlive(multiChannelImage);
			GC.KeepAlive(classifHandle);
		}

		public HRegion PolarTransRegionInv(HTuple row, HTuple column, double angleStart, double angleEnd, HTuple radiusStart, HTuple radiusEnd, int widthIn, int heightIn, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(475);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.Store(proc, 4, radiusStart);
			HalconAPI.Store(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, widthIn);
			HalconAPI.StoreI(proc, 7, heightIn);
			HalconAPI.StoreI(proc, 8, width);
			HalconAPI.StoreI(proc, 9, height);
			HalconAPI.StoreS(proc, 10, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radiusStart);
			HalconAPI.UnpinTuple(radiusEnd);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion PolarTransRegionInv(double row, double column, double angleStart, double angleEnd, double radiusStart, double radiusEnd, int widthIn, int heightIn, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(475);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.StoreD(proc, 4, radiusStart);
			HalconAPI.StoreD(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, widthIn);
			HalconAPI.StoreI(proc, 7, heightIn);
			HalconAPI.StoreI(proc, 8, width);
			HalconAPI.StoreI(proc, 9, height);
			HalconAPI.StoreS(proc, 10, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion PolarTransRegion(HTuple row, HTuple column, double angleStart, double angleEnd, HTuple radiusStart, HTuple radiusEnd, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(476);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.Store(proc, 4, radiusStart);
			HalconAPI.Store(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radiusStart);
			HalconAPI.UnpinTuple(radiusEnd);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion PolarTransRegion(double row, double column, double angleStart, double angleEnd, double radiusStart, double radiusEnd, int width, int height, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(476);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.StoreD(proc, 4, radiusStart);
			HalconAPI.StoreD(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.StoreS(proc, 8, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion MergeRegionsLineScan(HRegion prevRegions, out HRegion prevMergedRegions, int imageHeight, string mergeBorder, int maxImagesRegion)
		{
			IntPtr proc = HalconAPI.PreCall(477);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, prevRegions);
			HalconAPI.StoreI(proc, 0, imageHeight);
			HalconAPI.StoreS(proc, 1, mergeBorder);
			HalconAPI.StoreI(proc, 2, maxImagesRegion);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			err = LoadNew(proc, 2, err, out prevMergedRegions);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(prevRegions);
			return obj;
		}

		public HRegion PartitionRectangle(double width, double height)
		{
			IntPtr proc = HalconAPI.PreCall(478);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, width);
			HalconAPI.StoreD(proc, 1, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion PartitionDynamic(double distance, double percent)
		{
			IntPtr proc = HalconAPI.PreCall(479);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, distance);
			HalconAPI.StoreD(proc, 1, percent);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage RegionToLabel(string type, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(480);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, width);
			HalconAPI.StoreI(proc, 2, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage RegionToBin(int foregroundGray, int backgroundGray, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(481);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, foregroundGray);
			HalconAPI.StoreI(proc, 1, backgroundGray);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Union2(HRegion region2)
		{
			IntPtr proc = HalconAPI.PreCall(482);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, region2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region2);
			return obj;
		}

		public HRegion Union1()
		{
			IntPtr proc = HalconAPI.PreCall(483);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage ClosestPointTransform(out HImage closestPoints, string metric, string foreground, string closestPointMode, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(484);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, metric);
			HalconAPI.StoreS(proc, 1, foreground);
			HalconAPI.StoreS(proc, 2, closestPointMode);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HImage.LoadNew(proc, 2, err, out closestPoints);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage DistanceTransform(string metric, string foreground, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(485);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, metric);
			HalconAPI.StoreS(proc, 1, foreground);
			HalconAPI.StoreI(proc, 2, width);
			HalconAPI.StoreI(proc, 3, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Skeleton()
		{
			IntPtr proc = HalconAPI.PreCall(486);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ProjectiveTransRegion(HHomMat2D homMat2D, string interpolation)
		{
			IntPtr proc = HalconAPI.PreCall(487);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.StoreS(proc, 1, interpolation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion AffineTransRegion(HHomMat2D homMat2D, string interpolate)
		{
			IntPtr proc = HalconAPI.PreCall(488);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.StoreS(proc, 1, interpolate);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion MirrorRegion(string mode, int widthHeight)
		{
			IntPtr proc = HalconAPI.PreCall(489);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, widthHeight);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ZoomRegion(double scaleWidth, double scaleHeight)
		{
			IntPtr proc = HalconAPI.PreCall(490);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, scaleWidth);
			HalconAPI.StoreD(proc, 1, scaleHeight);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion MoveRegion(int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(491);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion JunctionsSkeleton(out HRegion juncPoints)
		{
			IntPtr proc = HalconAPI.PreCall(492);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			err = LoadNew(proc, 2, err, out juncPoints);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Intersection(HRegion region2)
		{
			IntPtr proc = HalconAPI.PreCall(493);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, region2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region2);
			return obj;
		}

		public HRegion Interjacent(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(494);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion FillUp()
		{
			IntPtr proc = HalconAPI.PreCall(495);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion FillUpShape(string feature, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(496);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, feature);
			HalconAPI.Store(proc, 1, min);
			HalconAPI.Store(proc, 2, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion FillUpShape(string feature, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(496);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, feature);
			HalconAPI.StoreD(proc, 1, min);
			HalconAPI.StoreD(proc, 2, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ExpandRegion(HRegion forbiddenArea, HTuple iterations, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(497);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, forbiddenArea);
			HalconAPI.Store(proc, 0, iterations);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(iterations);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(forbiddenArea);
			return obj;
		}

		public HRegion ExpandRegion(HRegion forbiddenArea, int iterations, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(497);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, forbiddenArea);
			HalconAPI.StoreI(proc, 0, iterations);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(forbiddenArea);
			return obj;
		}

		public HRegion ClipRegionRel(int top, int bottom, int left, int right)
		{
			IntPtr proc = HalconAPI.PreCall(498);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, top);
			HalconAPI.StoreI(proc, 1, bottom);
			HalconAPI.StoreI(proc, 2, left);
			HalconAPI.StoreI(proc, 3, right);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ClipRegion(int row1, int column1, int row2, int column2)
		{
			IntPtr proc = HalconAPI.PreCall(499);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row1);
			HalconAPI.StoreI(proc, 1, column1);
			HalconAPI.StoreI(proc, 2, row2);
			HalconAPI.StoreI(proc, 3, column2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion RankRegion(int width, int height, int number)
		{
			IntPtr proc = HalconAPI.PreCall(500);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.StoreI(proc, 2, number);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Connection()
		{
			IntPtr proc = HalconAPI.PreCall(501);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion SymmDifference(HRegion region2)
		{
			IntPtr proc = HalconAPI.PreCall(502);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, region2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region2);
			return obj;
		}

		public HRegion Difference(HRegion sub)
		{
			IntPtr proc = HalconAPI.PreCall(503);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, sub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sub);
			return obj;
		}

		public HRegion Complement()
		{
			IntPtr proc = HalconAPI.PreCall(504);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion BackgroundSeg()
		{
			IntPtr proc = HalconAPI.PreCall(505);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion HammingChangeRegion(int width, int height, int distance)
		{
			IntPtr proc = HalconAPI.PreCall(506);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.StoreI(proc, 2, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion RemoveNoiseRegion(string type)
		{
			IntPtr proc = HalconAPI.PreCall(507);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ShapeTrans(string type)
		{
			IntPtr proc = HalconAPI.PreCall(508);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ExpandGray(HImage image, HRegion forbiddenArea, HTuple iterations, string mode, HTuple threshold)
		{
			IntPtr proc = HalconAPI.PreCall(509);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 3, forbiddenArea);
			HalconAPI.Store(proc, 0, iterations);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.Store(proc, 2, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(iterations);
			HalconAPI.UnpinTuple(threshold);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(forbiddenArea);
			return obj;
		}

		public HRegion ExpandGray(HImage image, HRegion forbiddenArea, string iterations, string mode, int threshold)
		{
			IntPtr proc = HalconAPI.PreCall(509);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 3, forbiddenArea);
			HalconAPI.StoreS(proc, 0, iterations);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.StoreI(proc, 2, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(forbiddenArea);
			return obj;
		}

		public HRegion ExpandGrayRef(HImage image, HRegion forbiddenArea, HTuple iterations, string mode, HTuple refGray, HTuple threshold)
		{
			IntPtr proc = HalconAPI.PreCall(510);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 3, forbiddenArea);
			HalconAPI.Store(proc, 0, iterations);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.Store(proc, 2, refGray);
			HalconAPI.Store(proc, 3, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(iterations);
			HalconAPI.UnpinTuple(refGray);
			HalconAPI.UnpinTuple(threshold);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(forbiddenArea);
			return obj;
		}

		public HRegion ExpandGrayRef(HImage image, HRegion forbiddenArea, string iterations, string mode, int refGray, int threshold)
		{
			IntPtr proc = HalconAPI.PreCall(510);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 3, forbiddenArea);
			HalconAPI.StoreS(proc, 0, iterations);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.StoreI(proc, 2, refGray);
			HalconAPI.StoreI(proc, 3, threshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(forbiddenArea);
			return obj;
		}

		public void SplitSkeletonLines(int maxDistance, out HTuple beginRow, out HTuple beginCol, out HTuple endRow, out HTuple endCol)
		{
			IntPtr proc = HalconAPI.PreCall(511);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maxDistance);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out beginRow);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out beginCol);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out endRow);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out endCol);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion SplitSkeletonRegion(int maxDistance)
		{
			IntPtr proc = HalconAPI.PreCall(512);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, maxDistance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void GenRegionHisto(HTuple histogram, int row, int column, int scale)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(513);
			HalconAPI.Store(proc, 0, histogram);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.StoreI(proc, 3, scale);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(histogram);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion EliminateRuns(int elimShorter, int elimLonger)
		{
			IntPtr proc = HalconAPI.PreCall(514);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, elimShorter);
			HalconAPI.StoreI(proc, 1, elimLonger);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ObjDiff(HRegion objectsSub)
		{
			IntPtr proc = HalconAPI.PreCall(573);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsSub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsSub);
			return obj;
		}

		public HImage PaintRegion(HImage image, HTuple grayval, string type)
		{
			IntPtr proc = HalconAPI.PreCall(576);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, grayval);
			HalconAPI.StoreS(proc, 1, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(grayval);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HImage PaintRegion(HImage image, double grayval, string type)
		{
			IntPtr proc = HalconAPI.PreCall(576);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreD(proc, 0, grayval);
			HalconAPI.StoreS(proc, 1, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void OverpaintRegion(HImage image, HTuple grayval, string type)
		{
			IntPtr proc = HalconAPI.PreCall(577);
			Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, grayval);
			HalconAPI.StoreS(proc, 1, type);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(grayval);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void OverpaintRegion(HImage image, double grayval, string type)
		{
			IntPtr proc = HalconAPI.PreCall(577);
			Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 0, grayval);
			HalconAPI.StoreS(proc, 1, type);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public new HRegion CopyObj(int index, int numObj)
		{
			IntPtr proc = HalconAPI.PreCall(583);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.StoreI(proc, 1, numObj);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ConcatObj(HRegion objects2)
		{
			IntPtr proc = HalconAPI.PreCall(584);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return obj;
		}

		public new HRegion SelectObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HRegion SelectObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public int CompareObj(HRegion objects2, HTuple epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.Store(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(epsilon);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public int CompareObj(HRegion objects2, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.StoreD(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public HTuple TestSubsetRegion(HRegion region2)
		{
			IntPtr proc = HalconAPI.PreCall(589);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, region2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(region2);
			return tuple;
		}

		public int TestEqualRegion(HRegion regions2)
		{
			IntPtr proc = HalconAPI.PreCall(590);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return intValue;
		}

		public int TestEqualObj(HRegion objects2)
		{
			IntPtr proc = HalconAPI.PreCall(591);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public void GenRegionPolygonFilled(HTuple rows, HTuple columns)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(598);
			HalconAPI.Store(proc, 0, rows);
			HalconAPI.Store(proc, 1, columns);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(columns);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRegionPolygon(HTuple rows, HTuple columns)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(599);
			HalconAPI.Store(proc, 0, rows);
			HalconAPI.Store(proc, 1, columns);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(columns);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRegionPoints(HTuple rows, HTuple columns)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(600);
			HalconAPI.Store(proc, 0, rows);
			HalconAPI.Store(proc, 1, columns);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(columns);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRegionPoints(int rows, int columns)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(600);
			HalconAPI.StoreI(proc, 0, rows);
			HalconAPI.StoreI(proc, 1, columns);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRegionRuns(HTuple row, HTuple columnBegin, HTuple columnEnd)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(601);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, columnBegin);
			HalconAPI.Store(proc, 2, columnEnd);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(columnBegin);
			HalconAPI.UnpinTuple(columnEnd);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRegionRuns(int row, int columnBegin, int columnEnd)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(601);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, columnBegin);
			HalconAPI.StoreI(proc, 2, columnEnd);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRectangle2(HTuple row, HTuple column, HTuple phi, HTuple length1, HTuple length2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(602);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, phi);
			HalconAPI.Store(proc, 3, length1);
			HalconAPI.Store(proc, 4, length2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(length1);
			HalconAPI.UnpinTuple(length2);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRectangle2(double row, double column, double phi, double length1, double length2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(602);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, length1);
			HalconAPI.StoreD(proc, 4, length2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRectangle1(HTuple row1, HTuple column1, HTuple row2, HTuple column2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(603);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRectangle1(double row1, double column1, double row2, double column2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(603);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRandomRegion(int width, int height)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(604);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenEllipseSector(HTuple row, HTuple column, HTuple phi, HTuple radius1, HTuple radius2, HTuple startAngle, HTuple endAngle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(608);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, phi);
			HalconAPI.Store(proc, 3, radius1);
			HalconAPI.Store(proc, 4, radius2);
			HalconAPI.Store(proc, 5, startAngle);
			HalconAPI.Store(proc, 6, endAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(radius1);
			HalconAPI.UnpinTuple(radius2);
			HalconAPI.UnpinTuple(startAngle);
			HalconAPI.UnpinTuple(endAngle);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenEllipseSector(double row, double column, double phi, double radius1, double radius2, double startAngle, double endAngle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(608);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, radius1);
			HalconAPI.StoreD(proc, 4, radius2);
			HalconAPI.StoreD(proc, 5, startAngle);
			HalconAPI.StoreD(proc, 6, endAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenEllipse(HTuple row, HTuple column, HTuple phi, HTuple radius1, HTuple radius2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(609);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, phi);
			HalconAPI.Store(proc, 3, radius1);
			HalconAPI.Store(proc, 4, radius2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(radius1);
			HalconAPI.UnpinTuple(radius2);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenEllipse(double row, double column, double phi, double radius1, double radius2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(609);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, radius1);
			HalconAPI.StoreD(proc, 4, radius2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCircleSector(HTuple row, HTuple column, HTuple radius, HTuple startAngle, HTuple endAngle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(610);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.Store(proc, 3, startAngle);
			HalconAPI.Store(proc, 4, endAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(startAngle);
			HalconAPI.UnpinTuple(endAngle);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCircleSector(double row, double column, double radius, double startAngle, double endAngle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(610);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, startAngle);
			HalconAPI.StoreD(proc, 4, endAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCircle(HTuple row, HTuple column, HTuple radius)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(611);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radius);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCircle(double row, double column, double radius)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(611);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCheckerRegion(int widthRegion, int heightRegion, int widthPattern, int heightPattern)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(612);
			HalconAPI.StoreI(proc, 0, widthRegion);
			HalconAPI.StoreI(proc, 1, heightRegion);
			HalconAPI.StoreI(proc, 2, widthPattern);
			HalconAPI.StoreI(proc, 3, heightPattern);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenGridRegion(HTuple rowSteps, HTuple columnSteps, string type, int width, int height)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(613);
			HalconAPI.Store(proc, 0, rowSteps);
			HalconAPI.Store(proc, 1, columnSteps);
			HalconAPI.StoreS(proc, 2, type);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowSteps);
			HalconAPI.UnpinTuple(columnSteps);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenGridRegion(int rowSteps, int columnSteps, string type, int width, int height)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(613);
			HalconAPI.StoreI(proc, 0, rowSteps);
			HalconAPI.StoreI(proc, 1, columnSteps);
			HalconAPI.StoreS(proc, 2, type);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRandomRegions(string type, HTuple widthMin, HTuple widthMax, HTuple heightMin, HTuple heightMax, HTuple phiMin, HTuple phiMax, int numRegions, int width, int height)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(614);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.Store(proc, 1, widthMin);
			HalconAPI.Store(proc, 2, widthMax);
			HalconAPI.Store(proc, 3, heightMin);
			HalconAPI.Store(proc, 4, heightMax);
			HalconAPI.Store(proc, 5, phiMin);
			HalconAPI.Store(proc, 6, phiMax);
			HalconAPI.StoreI(proc, 7, numRegions);
			HalconAPI.StoreI(proc, 8, width);
			HalconAPI.StoreI(proc, 9, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(widthMin);
			HalconAPI.UnpinTuple(widthMax);
			HalconAPI.UnpinTuple(heightMin);
			HalconAPI.UnpinTuple(heightMax);
			HalconAPI.UnpinTuple(phiMin);
			HalconAPI.UnpinTuple(phiMax);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRandomRegions(string type, double widthMin, double widthMax, double heightMin, double heightMax, double phiMin, double phiMax, int numRegions, int width, int height)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(614);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreD(proc, 1, widthMin);
			HalconAPI.StoreD(proc, 2, widthMax);
			HalconAPI.StoreD(proc, 3, heightMin);
			HalconAPI.StoreD(proc, 4, heightMax);
			HalconAPI.StoreD(proc, 5, phiMin);
			HalconAPI.StoreD(proc, 6, phiMax);
			HalconAPI.StoreI(proc, 7, numRegions);
			HalconAPI.StoreI(proc, 8, width);
			HalconAPI.StoreI(proc, 9, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRegionHline(HTuple orientation, HTuple distance)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(615);
			HalconAPI.Store(proc, 0, orientation);
			HalconAPI.Store(proc, 1, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(orientation);
			HalconAPI.UnpinTuple(distance);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRegionHline(double orientation, double distance)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(615);
			HalconAPI.StoreD(proc, 0, orientation);
			HalconAPI.StoreD(proc, 1, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRegionLine(HTuple beginRow, HTuple beginCol, HTuple endRow, HTuple endCol)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(616);
			HalconAPI.Store(proc, 0, beginRow);
			HalconAPI.Store(proc, 1, beginCol);
			HalconAPI.Store(proc, 2, endRow);
			HalconAPI.Store(proc, 3, endCol);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(beginRow);
			HalconAPI.UnpinTuple(beginCol);
			HalconAPI.UnpinTuple(endRow);
			HalconAPI.UnpinTuple(endCol);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRegionLine(int beginRow, int beginCol, int endRow, int endCol)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(616);
			HalconAPI.StoreI(proc, 0, beginRow);
			HalconAPI.StoreI(proc, 1, beginCol);
			HalconAPI.StoreI(proc, 2, endRow);
			HalconAPI.StoreI(proc, 3, endCol);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenEmptyRegion()
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(618);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetRegionThickness(out HTuple histogramm)
		{
			IntPtr proc = HalconAPI.PreCall(631);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out histogramm);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void GetRegionPolygon(HTuple tolerance, out HTuple rows, out HTuple columns)
		{
			IntPtr proc = HalconAPI.PreCall(632);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, tolerance);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(tolerance);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out columns);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetRegionPolygon(double tolerance, out HTuple rows, out HTuple columns)
		{
			IntPtr proc = HalconAPI.PreCall(632);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, tolerance);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out columns);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetRegionPoints(out HTuple rows, out HTuple columns)
		{
			IntPtr proc = HalconAPI.PreCall(633);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out columns);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetRegionContour(out HTuple rows, out HTuple columns)
		{
			IntPtr proc = HalconAPI.PreCall(634);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out columns);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetRegionRuns(out HTuple row, out HTuple columnBegin, out HTuple columnEnd)
		{
			IntPtr proc = HalconAPI.PreCall(635);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out columnBegin);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out columnEnd);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetRegionChain(out int row, out int column, out HTuple chain)
		{
			IntPtr proc = HalconAPI.PreCall(636);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out row);
			err = HalconAPI.LoadI(proc, 1, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out chain);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetRegionConvex(out HTuple rows, out HTuple columns)
		{
			IntPtr proc = HalconAPI.PreCall(637);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out columns);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DoOcrWordKnn(HImage image, HOCRKnn OCRHandle, string expression, int numAlternatives, int numCorrections, out HTuple confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(647);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public string DoOcrWordKnn(HImage image, HOCRKnn OCRHandle, string expression, int numAlternatives, int numCorrections, out double confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(647);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return stringValue;
		}

		public HTuple DoOcrMultiClassKnn(HImage image, HOCRKnn OCRHandle, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(658);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public string DoOcrMultiClassKnn(HImage image, HOCRKnn OCRHandle, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(658);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return stringValue;
		}

		public HTuple DoOcrSingleClassKnn(HImage image, HOCRKnn OCRHandle, HTuple numClasses, HTuple numNeighbors, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(659);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.Store(proc, 1, numClasses);
			HalconAPI.Store(proc, 2, numNeighbors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numClasses);
			HalconAPI.UnpinTuple(numNeighbors);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public string DoOcrSingleClassKnn(HImage image, HOCRKnn OCRHandle, HTuple numClasses, HTuple numNeighbors, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(659);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.Store(proc, 1, numClasses);
			HalconAPI.Store(proc, 2, numNeighbors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numClasses);
			HalconAPI.UnpinTuple(numNeighbors);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return stringValue;
		}

		public HTuple DoOcrWordSvm(HImage image, HOCRSvm OCRHandle, string expression, int numAlternatives, int numCorrections, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(679);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HalconAPI.LoadS(proc, 1, err, out word);
			err = HalconAPI.LoadD(proc, 2, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public HTuple DoOcrMultiClassSvm(HImage image, HOCRSvm OCRHandle)
		{
			IntPtr proc = HalconAPI.PreCall(680);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public HTuple DoOcrSingleClassSvm(HImage image, HOCRSvm OCRHandle, HTuple num)
		{
			IntPtr proc = HalconAPI.PreCall(681);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public HTuple DoOcrWordMlp(HImage image, HOCRMlp OCRHandle, string expression, int numAlternatives, int numCorrections, out HTuple confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(697);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public string DoOcrWordMlp(HImage image, HOCRMlp OCRHandle, string expression, int numAlternatives, int numCorrections, out double confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(697);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return stringValue;
		}

		public HTuple DoOcrMultiClassMlp(HImage image, HOCRMlp OCRHandle, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(698);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public string DoOcrMultiClassMlp(HImage image, HOCRMlp OCRHandle, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(698);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return stringValue;
		}

		public HTuple DoOcrSingleClassMlp(HImage image, HOCRMlp OCRHandle, HTuple num, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(699);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public string DoOcrSingleClassMlp(HImage image, HOCRMlp OCRHandle, HTuple num, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(699);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return stringValue;
		}

		public HTuple DoOcrSingle(HImage image, HOCRBox ocrHandle, out HTuple confidences)
		{
			IntPtr proc = HalconAPI.PreCall(713);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, ocrHandle);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidences);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(ocrHandle);
			return tuple;
		}

		public HTuple DoOcrMulti(HImage image, HOCRBox ocrHandle, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(714);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, ocrHandle);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(ocrHandle);
			return tuple;
		}

		public string DoOcrMulti(HImage image, HOCRBox ocrHandle, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(714);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, ocrHandle);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(ocrHandle);
			return stringValue;
		}

		public double TraindOcrClassBox(HImage image, HOCRBox ocrHandle, HTuple classVal)
		{
			IntPtr proc = HalconAPI.PreCall(717);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, ocrHandle);
			HalconAPI.Store(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(classVal);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(ocrHandle);
			return doubleValue;
		}

		public double TraindOcrClassBox(HImage image, HOCRBox ocrHandle, string classVal)
		{
			IntPtr proc = HalconAPI.PreCall(717);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, ocrHandle);
			HalconAPI.StoreS(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(ocrHandle);
			return doubleValue;
		}

		public static void ProtectOcrTrainf(string trainingFile, HTuple password, string trainingFileProtected)
		{
			IntPtr proc = HalconAPI.PreCall(719);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.Store(proc, 1, password);
			HalconAPI.StoreS(proc, 2, trainingFileProtected);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(password);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void ProtectOcrTrainf(string trainingFile, string password, string trainingFileProtected)
		{
			IntPtr proc = HalconAPI.PreCall(719);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.StoreS(proc, 1, password);
			HalconAPI.StoreS(proc, 2, trainingFileProtected);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public void WriteOcrTrainf(HImage image, HTuple classVal, string trainingFile)
		{
			IntPtr proc = HalconAPI.PreCall(720);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, classVal);
			HalconAPI.StoreS(proc, 1, trainingFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(classVal);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void WriteOcrTrainf(HImage image, string classVal, string trainingFile)
		{
			IntPtr proc = HalconAPI.PreCall(720);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, classVal);
			HalconAPI.StoreS(proc, 1, trainingFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HRegion SortRegion(string sortMode, string order, string rowOrCol)
		{
			IntPtr proc = HalconAPI.PreCall(723);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, sortMode);
			HalconAPI.StoreS(proc, 1, order);
			HalconAPI.StoreS(proc, 2, rowOrCol);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple TestdOcrClassBox(HImage image, HOCRBox ocrHandle, HTuple classVal)
		{
			IntPtr proc = HalconAPI.PreCall(725);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, ocrHandle);
			HalconAPI.Store(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(classVal);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(ocrHandle);
			return tuple;
		}

		public double TestdOcrClassBox(HImage image, HOCRBox ocrHandle, string classVal)
		{
			IntPtr proc = HalconAPI.PreCall(725);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, ocrHandle);
			HalconAPI.StoreS(proc, 1, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(ocrHandle);
			return doubleValue;
		}

		public void AppendOcrTrainf(HImage image, HTuple classVal, string trainingFile)
		{
			IntPtr proc = HalconAPI.PreCall(730);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, classVal);
			HalconAPI.StoreS(proc, 1, trainingFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(classVal);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void AppendOcrTrainf(HImage image, string classVal, string trainingFile)
		{
			IntPtr proc = HalconAPI.PreCall(730);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, classVal);
			HalconAPI.StoreS(proc, 1, trainingFile);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HRegion Pruning(int length)
		{
			IntPtr proc = HalconAPI.PreCall(735);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, length);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Boundary(string boundaryType)
		{
			IntPtr proc = HalconAPI.PreCall(736);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, boundaryType);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Fitting(HRegion structElements)
		{
			IntPtr proc = HalconAPI.PreCall(737);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElements);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElements);
			return obj;
		}

		public void GenStructElements(string type, int row, int column)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(738);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion TransposeRegion(int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(739);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ThinningSeq(string golayElement, HTuple iterations)
		{
			IntPtr proc = HalconAPI.PreCall(740);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.Store(proc, 1, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(iterations);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ThinningSeq(string golayElement, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(740);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ThinningGolay(string golayElement, int rotation)
		{
			IntPtr proc = HalconAPI.PreCall(741);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, rotation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Thinning(HRegion structElement1, HRegion structElement2, int row, int column, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(742);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement1);
			HalconAPI.Store(proc, 3, structElement2);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement1);
			GC.KeepAlive(structElement2);
			return obj;
		}

		public HRegion ThickeningSeq(string golayElement, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(743);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ThickeningGolay(string golayElement, int rotation)
		{
			IntPtr proc = HalconAPI.PreCall(744);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, rotation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Thickening(HRegion structElement1, HRegion structElement2, int row, int column, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(745);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement1);
			HalconAPI.Store(proc, 3, structElement2);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement1);
			GC.KeepAlive(structElement2);
			return obj;
		}

		public HRegion HitOrMissSeq(string golayElement)
		{
			IntPtr proc = HalconAPI.PreCall(746);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion HitOrMissGolay(string golayElement, int rotation)
		{
			IntPtr proc = HalconAPI.PreCall(747);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, rotation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion HitOrMiss(HRegion structElement1, HRegion structElement2, int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(748);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement1);
			HalconAPI.Store(proc, 3, structElement2);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement1);
			GC.KeepAlive(structElement2);
			return obj;
		}

		public HRegion GolayElements(string golayElement, int rotation, int row, int column)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(749);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, rotation);
			HalconAPI.StoreI(proc, 2, row);
			HalconAPI.StoreI(proc, 3, column);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			err = LoadNew(proc, 2, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion MorphSkiz(HTuple iterations1, HTuple iterations2)
		{
			IntPtr proc = HalconAPI.PreCall(750);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, iterations1);
			HalconAPI.Store(proc, 1, iterations2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(iterations1);
			HalconAPI.UnpinTuple(iterations2);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion MorphSkiz(int iterations1, int iterations2)
		{
			IntPtr proc = HalconAPI.PreCall(750);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, iterations1);
			HalconAPI.StoreI(proc, 1, iterations2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion MorphSkeleton()
		{
			IntPtr proc = HalconAPI.PreCall(751);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion MorphHat(HRegion structElement)
		{
			IntPtr proc = HalconAPI.PreCall(752);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion BottomHat(HRegion structElement)
		{
			IntPtr proc = HalconAPI.PreCall(753);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion TopHat(HRegion structElement)
		{
			IntPtr proc = HalconAPI.PreCall(754);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion MinkowskiSub2(HRegion structElement, int row, int column, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(755);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion MinkowskiSub1(HRegion structElement, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(756);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.StoreI(proc, 0, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion MinkowskiAdd2(HRegion structElement, int row, int column, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(757);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion MinkowskiAdd1(HRegion structElement, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(758);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.StoreI(proc, 0, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion ClosingRectangle1(int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(759);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ClosingGolay(string golayElement, int rotation)
		{
			IntPtr proc = HalconAPI.PreCall(760);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, rotation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ClosingCircle(HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(761);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(radius);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ClosingCircle(double radius)
		{
			IntPtr proc = HalconAPI.PreCall(761);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Closing(HRegion structElement)
		{
			IntPtr proc = HalconAPI.PreCall(762);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion OpeningSeg(HRegion structElement)
		{
			IntPtr proc = HalconAPI.PreCall(763);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion OpeningGolay(string golayElement, int rotation)
		{
			IntPtr proc = HalconAPI.PreCall(764);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, rotation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion OpeningRectangle1(int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(765);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion OpeningCircle(HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(766);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(radius);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion OpeningCircle(double radius)
		{
			IntPtr proc = HalconAPI.PreCall(766);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Opening(HRegion structElement)
		{
			IntPtr proc = HalconAPI.PreCall(767);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion ErosionSeq(string golayElement, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(768);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ErosionGolay(string golayElement, int iterations, int rotation)
		{
			IntPtr proc = HalconAPI.PreCall(769);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreI(proc, 2, rotation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ErosionRectangle1(int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(770);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ErosionCircle(HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(771);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(radius);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ErosionCircle(double radius)
		{
			IntPtr proc = HalconAPI.PreCall(771);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Erosion2(HRegion structElement, int row, int column, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(772);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion Erosion1(HRegion structElement, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(773);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.StoreI(proc, 0, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion DilationSeq(string golayElement, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(774);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion DilationGolay(string golayElement, int iterations, int rotation)
		{
			IntPtr proc = HalconAPI.PreCall(775);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, golayElement);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreI(proc, 2, rotation);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion DilationRectangle1(int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(776);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion DilationCircle(HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(777);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(radius);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion DilationCircle(double radius)
		{
			IntPtr proc = HalconAPI.PreCall(777);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion Dilation2(HRegion structElement, int row, int column, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(778);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.StoreI(proc, 2, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HRegion Dilation1(HRegion structElement, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(779);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, structElement);
			HalconAPI.StoreI(proc, 0, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(structElement);
			return obj;
		}

		public HImage AddChannels(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(1144);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HRegion HoughCircles(HTuple radius, HTuple percent, HTuple mode)
		{
			IntPtr proc = HalconAPI.PreCall(1149);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, radius);
			HalconAPI.Store(proc, 1, percent);
			HalconAPI.Store(proc, 2, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(percent);
			HalconAPI.UnpinTuple(mode);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion HoughCircles(int radius, int percent, int mode)
		{
			IntPtr proc = HalconAPI.PreCall(1149);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, radius);
			HalconAPI.StoreI(proc, 1, percent);
			HalconAPI.StoreI(proc, 2, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage HoughCircleTrans(HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(1150);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(radius);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage HoughCircleTrans(int radius)
		{
			IntPtr proc = HalconAPI.PreCall(1150);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, radius);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple HoughLines(int angleResolution, int threshold, int angleGap, int distGap, out HTuple dist)
		{
			IntPtr proc = HalconAPI.PreCall(1153);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, angleResolution);
			HalconAPI.StoreI(proc, 1, threshold);
			HalconAPI.StoreI(proc, 2, angleGap);
			HalconAPI.StoreI(proc, 3, distGap);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out dist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HImage HoughLineTrans(int angleResolution)
		{
			IntPtr proc = HalconAPI.PreCall(1154);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, angleResolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion SelectMatchingLines(HTuple angleIn, HTuple distIn, int lineWidth, int thresh, out HTuple angleOut, out HTuple distOut)
		{
			IntPtr proc = HalconAPI.PreCall(1155);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, angleIn);
			HalconAPI.Store(proc, 1, distIn);
			HalconAPI.StoreI(proc, 2, lineWidth);
			HalconAPI.StoreI(proc, 3, thresh);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleIn);
			HalconAPI.UnpinTuple(distIn);
			err = LoadNew(proc, 1, err, out HRegion obj);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out angleOut);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distOut);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion SelectMatchingLines(double angleIn, double distIn, int lineWidth, int thresh, out double angleOut, out double distOut)
		{
			IntPtr proc = HalconAPI.PreCall(1155);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, angleIn);
			HalconAPI.StoreD(proc, 1, distIn);
			HalconAPI.StoreI(proc, 2, lineWidth);
			HalconAPI.StoreI(proc, 3, thresh);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			err = HalconAPI.LoadD(proc, 0, err, out angleOut);
			err = HalconAPI.LoadD(proc, 1, err, out distOut);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void GetIcon(HWindow windowHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1260);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void SetIcon(HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1261);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DispRegion(HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1262);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public HRegion DragRegion3(HRegion maskRegion, HWindow windowHandle, int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(1315);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, maskRegion);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(maskRegion);
			GC.KeepAlive(windowHandle);
			return obj;
		}

		public HRegion DragRegion2(HWindow windowHandle, int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(1316);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
			return obj;
		}

		public HRegion DragRegion1(HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1317);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
			return obj;
		}

		public void DrawRegion(HWindow windowHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1336);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DrawPolygon(HWindow windowHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1337);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DistanceSr(HTuple row1, HTuple column1, HTuple row2, HTuple column2, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1367);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistanceSr(double row1, double column1, double row2, double column2, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1367);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistanceLr(HTuple row1, HTuple column1, HTuple row2, HTuple column2, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1368);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistanceLr(double row1, double column1, double row2, double column2, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1368);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistancePr(HTuple row, HTuple column, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1369);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistancePr(double row, double column, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1369);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple NoiseDistributionMean(HImage image, int filterSize)
		{
			IntPtr proc = HalconAPI.PreCall(1440);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreI(proc, 0, filterSize);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple FuzzyEntropy(HImage image, int apar, int cpar)
		{
			IntPtr proc = HalconAPI.PreCall(1457);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreI(proc, 0, apar);
			HalconAPI.StoreI(proc, 1, cpar);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple FuzzyPerimeter(HImage image, int apar, int cpar)
		{
			IntPtr proc = HalconAPI.PreCall(1458);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreI(proc, 0, apar);
			HalconAPI.StoreI(proc, 1, cpar);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HImage RegionToMean(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(1476);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HRegion CloseEdgesLength(HImage gradient, int minAmplitude, int maxGapLength)
		{
			IntPtr proc = HalconAPI.PreCall(1573);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, gradient);
			HalconAPI.StoreI(proc, 0, minAmplitude);
			HalconAPI.StoreI(proc, 1, maxGapLength);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(gradient);
			return obj;
		}

		public HRegion CloseEdges(HImage edgeImage, int minAmplitude)
		{
			IntPtr proc = HalconAPI.PreCall(1574);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, edgeImage);
			HalconAPI.StoreI(proc, 0, minAmplitude);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(edgeImage);
			return obj;
		}

		public void DeserializeRegion(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1652);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeRegion()
		{
			IntPtr proc = HalconAPI.PreCall(1653);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void WriteRegion(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1654);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadRegion(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1657);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple MomentsRegionCentralInvar(out HTuple PSI2, out HTuple PSI3, out HTuple PSI4)
		{
			IntPtr proc = HalconAPI.PreCall(1694);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out PSI2);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out PSI3);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out PSI4);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsRegionCentralInvar(out double PSI2, out double PSI3, out double PSI4)
		{
			IntPtr proc = HalconAPI.PreCall(1694);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out PSI2);
			err = HalconAPI.LoadD(proc, 2, err, out PSI3);
			err = HalconAPI.LoadD(proc, 3, err, out PSI4);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple MomentsRegionCentral(out HTuple i2, out HTuple i3, out HTuple i4)
		{
			IntPtr proc = HalconAPI.PreCall(1695);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out i2);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out i3);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out i4);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsRegionCentral(out double i2, out double i3, out double i4)
		{
			IntPtr proc = HalconAPI.PreCall(1695);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out i2);
			err = HalconAPI.LoadD(proc, 2, err, out i3);
			err = HalconAPI.LoadD(proc, 3, err, out i4);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple MomentsRegion3rdInvar(out HTuple m12, out HTuple m03, out HTuple m30)
		{
			IntPtr proc = HalconAPI.PreCall(1696);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out m12);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out m03);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out m30);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsRegion3rdInvar(out double m12, out double m03, out double m30)
		{
			IntPtr proc = HalconAPI.PreCall(1696);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out m12);
			err = HalconAPI.LoadD(proc, 2, err, out m03);
			err = HalconAPI.LoadD(proc, 3, err, out m30);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple MomentsRegion3rd(out HTuple m12, out HTuple m03, out HTuple m30)
		{
			IntPtr proc = HalconAPI.PreCall(1697);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out m12);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out m03);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out m30);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsRegion3rd(out double m12, out double m03, out double m30)
		{
			IntPtr proc = HalconAPI.PreCall(1697);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out m12);
			err = HalconAPI.LoadD(proc, 2, err, out m03);
			err = HalconAPI.LoadD(proc, 3, err, out m30);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public void SmallestRectangle2(out HTuple row, out HTuple column, out HTuple phi, out HTuple length1, out HTuple length2)
		{
			IntPtr proc = HalconAPI.PreCall(1698);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out length1);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out length2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SmallestRectangle2(out double row, out double column, out double phi, out double length1, out double length2)
		{
			IntPtr proc = HalconAPI.PreCall(1698);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out length1);
			err = HalconAPI.LoadD(proc, 4, err, out length2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SmallestRectangle1(out HTuple row1, out HTuple column1, out HTuple row2, out HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1699);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out row1);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out column1);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out row2);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SmallestRectangle1(out int row1, out int column1, out int row2, out int column2)
		{
			IntPtr proc = HalconAPI.PreCall(1699);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out row1);
			err = HalconAPI.LoadI(proc, 1, err, out column1);
			err = HalconAPI.LoadI(proc, 2, err, out row2);
			err = HalconAPI.LoadI(proc, 3, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SmallestCircle(out HTuple row, out HTuple column, out HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(1700);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out radius);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SmallestCircle(out double row, out double column, out double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1700);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out radius);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HRegion SelectShapeProto(HRegion pattern, HTuple feature, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(1701);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, pattern);
			HalconAPI.Store(proc, 0, feature);
			HalconAPI.Store(proc, 1, min);
			HalconAPI.Store(proc, 2, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(feature);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
			return obj;
		}

		public HRegion SelectShapeProto(HRegion pattern, string feature, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(1701);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, pattern);
			HalconAPI.StoreS(proc, 0, feature);
			HalconAPI.StoreD(proc, 1, min);
			HalconAPI.StoreD(proc, 2, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(pattern);
			return obj;
		}

		public HTuple RegionFeatures(HTuple features)
		{
			IntPtr proc = HalconAPI.PreCall(1702);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, features);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double RegionFeatures(string features)
		{
			IntPtr proc = HalconAPI.PreCall(1702);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, features);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HRegion SelectShape(HTuple features, string operation, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(1703);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.Store(proc, 2, min);
			HalconAPI.Store(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion SelectShape(string features, string operation, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(1703);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.StoreD(proc, 2, min);
			HalconAPI.StoreD(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple RunlengthFeatures(out HTuple KFactor, out HTuple LFactor, out HTuple meanLength, out HTuple bytes)
		{
			IntPtr proc = HalconAPI.PreCall(1704);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out KFactor);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out LFactor);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out meanLength);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out bytes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int RunlengthFeatures(out double KFactor, out double LFactor, out double meanLength, out int bytes)
		{
			IntPtr proc = HalconAPI.PreCall(1704);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadD(proc, 1, err, out KFactor);
			err = HalconAPI.LoadD(proc, 2, err, out LFactor);
			err = HalconAPI.LoadD(proc, 3, err, out meanLength);
			err = HalconAPI.LoadI(proc, 4, err, out bytes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple FindNeighbors(HRegion regions2, int maxDistance, out HTuple regionIndex2)
		{
			IntPtr proc = HalconAPI.PreCall(1705);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.StoreI(proc, 0, maxDistance);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out regionIndex2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return tuple;
		}

		public HTuple MomentsRegion2ndRelInvar(out HTuple PHI2)
		{
			IntPtr proc = HalconAPI.PreCall(1706);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out PHI2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsRegion2ndRelInvar(out double PHI2)
		{
			IntPtr proc = HalconAPI.PreCall(1706);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out PHI2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple MomentsRegion2ndInvar(out HTuple m20, out HTuple m02)
		{
			IntPtr proc = HalconAPI.PreCall(1707);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out m20);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out m02);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsRegion2ndInvar(out double m20, out double m02)
		{
			IntPtr proc = HalconAPI.PreCall(1707);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out m20);
			err = HalconAPI.LoadD(proc, 2, err, out m02);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple MomentsRegion2nd(out HTuple m20, out HTuple m02, out HTuple ia, out HTuple ib)
		{
			IntPtr proc = HalconAPI.PreCall(1708);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out m20);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out m02);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out ia);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out ib);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsRegion2nd(out double m20, out double m02, out double ia, out double ib)
		{
			IntPtr proc = HalconAPI.PreCall(1708);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out m20);
			err = HalconAPI.LoadD(proc, 2, err, out m02);
			err = HalconAPI.LoadD(proc, 3, err, out ia);
			err = HalconAPI.LoadD(proc, 4, err, out ib);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple DistanceRrMin(HRegion regions2, out HTuple row1, out HTuple column1, out HTuple row2, out HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1709);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out row1);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out column1);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out row2);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return tuple;
		}

		public double DistanceRrMin(HRegion regions2, out int row1, out int column1, out int row2, out int column2)
		{
			IntPtr proc = HalconAPI.PreCall(1709);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadI(proc, 1, err, out row1);
			err = HalconAPI.LoadI(proc, 2, err, out column1);
			err = HalconAPI.LoadI(proc, 3, err, out row2);
			err = HalconAPI.LoadI(proc, 4, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return doubleValue;
		}

		public HTuple DistanceRrMinDil(HRegion regions2)
		{
			IntPtr proc = HalconAPI.PreCall(1710);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return tuple;
		}

		public void DiameterRegion(out HTuple row1, out HTuple column1, out HTuple row2, out HTuple column2, out HTuple diameter)
		{
			IntPtr proc = HalconAPI.PreCall(1711);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out row1);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out column1);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out row2);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out column2);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out diameter);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DiameterRegion(out int row1, out int column1, out int row2, out int column2, out double diameter)
		{
			IntPtr proc = HalconAPI.PreCall(1711);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out row1);
			err = HalconAPI.LoadI(proc, 1, err, out column1);
			err = HalconAPI.LoadI(proc, 2, err, out row2);
			err = HalconAPI.LoadI(proc, 3, err, out column2);
			err = HalconAPI.LoadD(proc, 4, err, out diameter);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public int TestRegionPoint(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1712);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int TestRegionPoint(int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(1712);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple GetRegionIndex(int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(1713);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HRegion SelectRegionPoint(int row, int column)
		{
			IntPtr proc = HalconAPI.PreCall(1714);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row);
			HalconAPI.StoreI(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion SelectShapeStd(string shape, double percent)
		{
			IntPtr proc = HalconAPI.PreCall(1715);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, shape);
			HalconAPI.StoreD(proc, 1, percent);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple HammingDistanceNorm(HRegion regions2, HTuple norm, out HTuple similarity)
		{
			IntPtr proc = HalconAPI.PreCall(1716);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.Store(proc, 0, norm);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(norm);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out similarity);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return tuple;
		}

		public int HammingDistanceNorm(HRegion regions2, string norm, out double similarity)
		{
			IntPtr proc = HalconAPI.PreCall(1716);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.StoreS(proc, 0, norm);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadD(proc, 1, err, out similarity);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return intValue;
		}

		public HTuple HammingDistance(HRegion regions2, out HTuple similarity)
		{
			IntPtr proc = HalconAPI.PreCall(1717);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out similarity);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return tuple;
		}

		public int HammingDistance(HRegion regions2, out double similarity)
		{
			IntPtr proc = HalconAPI.PreCall(1717);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadD(proc, 1, err, out similarity);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return intValue;
		}

		public HTuple Eccentricity(out HTuple bulkiness, out HTuple structureFactor)
		{
			IntPtr proc = HalconAPI.PreCall(1718);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out bulkiness);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out structureFactor);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double Eccentricity(out double bulkiness, out double structureFactor)
		{
			IntPtr proc = HalconAPI.PreCall(1718);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out bulkiness);
			err = HalconAPI.LoadD(proc, 2, err, out structureFactor);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple EulerNumber()
		{
			IntPtr proc = HalconAPI.PreCall(1719);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple OrientationRegion()
		{
			IntPtr proc = HalconAPI.PreCall(1720);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple EllipticAxis(out HTuple rb, out HTuple phi)
		{
			IntPtr proc = HalconAPI.PreCall(1721);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out rb);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double EllipticAxis(out double rb, out double phi)
		{
			IntPtr proc = HalconAPI.PreCall(1721);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out rb);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple SelectRegionSpatial(HRegion regions2, string direction, out HTuple regionIndex2)
		{
			IntPtr proc = HalconAPI.PreCall(1722);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.StoreS(proc, 0, direction);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out regionIndex2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return tuple;
		}

		public HTuple SpatialRelation(HRegion regions2, int percent, out HTuple regionIndex2, out HTuple relation1, out HTuple relation2)
		{
			IntPtr proc = HalconAPI.PreCall(1723);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, regions2);
			HalconAPI.StoreI(proc, 0, percent);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out regionIndex2);
			err = HTuple.LoadNew(proc, 2, err, out relation1);
			err = HTuple.LoadNew(proc, 3, err, out relation2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions2);
			return tuple;
		}

		public HTuple Convexity()
		{
			IntPtr proc = HalconAPI.PreCall(1724);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple Contlength()
		{
			IntPtr proc = HalconAPI.PreCall(1725);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple ConnectAndHoles(out HTuple numHoles)
		{
			IntPtr proc = HalconAPI.PreCall(1726);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out numHoles);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int ConnectAndHoles(out int numHoles)
		{
			IntPtr proc = HalconAPI.PreCall(1726);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadI(proc, 1, err, out numHoles);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple Rectangularity()
		{
			IntPtr proc = HalconAPI.PreCall(1727);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple Compactness()
		{
			IntPtr proc = HalconAPI.PreCall(1728);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple Circularity()
		{
			IntPtr proc = HalconAPI.PreCall(1729);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple AreaHoles()
		{
			IntPtr proc = HalconAPI.PreCall(1730);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple AreaCenter(out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1731);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int AreaCenter(out double row, out double column)
		{
			IntPtr proc = HalconAPI.PreCall(1731);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadD(proc, 1, err, out row);
			err = HalconAPI.LoadD(proc, 2, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple RunlengthDistribution(out HTuple background)
		{
			IntPtr proc = HalconAPI.PreCall(1732);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out background);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple Roundness(out HTuple sigma, out HTuple roundness, out HTuple sides)
		{
			IntPtr proc = HalconAPI.PreCall(1733);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out sigma);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out roundness);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out sides);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double Roundness(out double sigma, out double roundness, out double sides)
		{
			IntPtr proc = HalconAPI.PreCall(1733);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out sigma);
			err = HalconAPI.LoadD(proc, 2, err, out roundness);
			err = HalconAPI.LoadD(proc, 3, err, out sides);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public void InnerRectangle1(out HTuple row1, out HTuple column1, out HTuple row2, out HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1734);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out row1);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out column1);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out row2);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void InnerRectangle1(out int row1, out int column1, out int row2, out int column2)
		{
			IntPtr proc = HalconAPI.PreCall(1734);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out row1);
			err = HalconAPI.LoadI(proc, 1, err, out column1);
			err = HalconAPI.LoadI(proc, 2, err, out row2);
			err = HalconAPI.LoadI(proc, 3, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void InnerCircle(out HTuple row, out HTuple column, out HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(1735);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out radius);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void InnerCircle(out double row, out double column, out double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1735);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out radius);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple FitSurfaceFirstOrder(HImage image, string algorithm, int iterations, double clippingFactor, out HTuple beta, out HTuple gamma)
		{
			IntPtr proc = HalconAPI.PreCall(1743);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreD(proc, 2, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out beta);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out gamma);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public double FitSurfaceFirstOrder(HImage image, string algorithm, int iterations, double clippingFactor, out double beta, out double gamma)
		{
			IntPtr proc = HalconAPI.PreCall(1743);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreD(proc, 2, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out beta);
			err = HalconAPI.LoadD(proc, 2, err, out gamma);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public HTuple FitSurfaceSecondOrder(HImage image, string algorithm, int iterations, double clippingFactor, out HTuple beta, out HTuple gamma, out HTuple delta, out HTuple epsilon, out HTuple zeta)
		{
			IntPtr proc = HalconAPI.PreCall(1744);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreD(proc, 2, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out beta);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out gamma);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out delta);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out epsilon);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out zeta);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public double FitSurfaceSecondOrder(HImage image, string algorithm, int iterations, double clippingFactor, out double beta, out double gamma, out double delta, out double epsilon, out double zeta)
		{
			IntPtr proc = HalconAPI.PreCall(1744);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.StoreD(proc, 2, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out beta);
			err = HalconAPI.LoadD(proc, 2, err, out gamma);
			err = HalconAPI.LoadD(proc, 3, err, out delta);
			err = HalconAPI.LoadD(proc, 4, err, out epsilon);
			err = HalconAPI.LoadD(proc, 5, err, out zeta);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public HTuple ShapeHistoPoint(HImage image, string feature, int row, int column, out HTuple relativeHisto)
		{
			IntPtr proc = HalconAPI.PreCall(1747);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, feature);
			HalconAPI.StoreI(proc, 1, row);
			HalconAPI.StoreI(proc, 2, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out relativeHisto);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple ShapeHistoAll(HImage image, string feature, out HTuple relativeHisto)
		{
			IntPtr proc = HalconAPI.PreCall(1748);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, feature);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out relativeHisto);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple GrayFeatures(HImage image, HTuple features)
		{
			IntPtr proc = HalconAPI.PreCall(1749);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, features);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public double GrayFeatures(HImage image, string features)
		{
			IntPtr proc = HalconAPI.PreCall(1749);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, features);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public HRegion SelectGray(HImage image, HTuple features, string operation, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(1750);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.Store(proc, 2, min);
			HalconAPI.Store(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HRegion SelectGray(HImage image, string features, string operation, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(1750);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.StoreD(proc, 2, min);
			HalconAPI.StoreD(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void MinMaxGray(HImage image, HTuple percent, out HTuple min, out HTuple max, out HTuple range)
		{
			IntPtr proc = HalconAPI.PreCall(1751);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, percent);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(percent);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out min);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out max);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out range);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void MinMaxGray(HImage image, double percent, out double min, out double max, out double range)
		{
			IntPtr proc = HalconAPI.PreCall(1751);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreD(proc, 0, percent);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out min);
			err = HalconAPI.LoadD(proc, 1, err, out max);
			err = HalconAPI.LoadD(proc, 2, err, out range);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HTuple Intensity(HImage image, out HTuple deviation)
		{
			IntPtr proc = HalconAPI.PreCall(1752);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out deviation);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public double Intensity(HImage image, out double deviation)
		{
			IntPtr proc = HalconAPI.PreCall(1752);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out deviation);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public HTuple GrayHistoRange(HImage image, HTuple min, HTuple max, int numBins, out double binSize)
		{
			IntPtr proc = HalconAPI.PreCall(1753);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, min);
			HalconAPI.Store(proc, 1, max);
			HalconAPI.StoreI(proc, 2, numBins);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HalconAPI.LoadD(proc, 1, err, out binSize);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public int GrayHistoRange(HImage image, double min, double max, int numBins, out double binSize)
		{
			IntPtr proc = HalconAPI.PreCall(1753);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreD(proc, 0, min);
			HalconAPI.StoreD(proc, 1, max);
			HalconAPI.StoreI(proc, 2, numBins);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadD(proc, 1, err, out binSize);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return intValue;
		}

		public HImage Histo2dim(HImage imageCol, HImage imageRow)
		{
			IntPtr proc = HalconAPI.PreCall(1754);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, imageCol);
			HalconAPI.Store(proc, 3, imageRow);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(imageCol);
			GC.KeepAlive(imageRow);
			return obj;
		}

		public HTuple GrayHistoAbs(HImage image, HTuple quantization)
		{
			IntPtr proc = HalconAPI.PreCall(1755);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, quantization);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(quantization);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple GrayHistoAbs(HImage image, double quantization)
		{
			IntPtr proc = HalconAPI.PreCall(1755);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreD(proc, 0, quantization);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple GrayHisto(HImage image, out HTuple relativeHisto)
		{
			IntPtr proc = HalconAPI.PreCall(1756);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out relativeHisto);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple EntropyGray(HImage image, out HTuple anisotropy)
		{
			IntPtr proc = HalconAPI.PreCall(1757);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out anisotropy);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public double EntropyGray(HImage image, out double anisotropy)
		{
			IntPtr proc = HalconAPI.PreCall(1757);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out anisotropy);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public HTuple CoocFeatureImage(HImage image, int ldGray, HTuple direction, out HTuple correlation, out HTuple homogeneity, out HTuple contrast)
		{
			IntPtr proc = HalconAPI.PreCall(1759);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreI(proc, 0, ldGray);
			HalconAPI.Store(proc, 1, direction);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(direction);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out correlation);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out homogeneity);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out contrast);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public double CoocFeatureImage(HImage image, int ldGray, int direction, out double correlation, out double homogeneity, out double contrast)
		{
			IntPtr proc = HalconAPI.PreCall(1759);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreI(proc, 0, ldGray);
			HalconAPI.StoreI(proc, 1, direction);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out correlation);
			err = HalconAPI.LoadD(proc, 2, err, out homogeneity);
			err = HalconAPI.LoadD(proc, 3, err, out contrast);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public HImage GenCoocMatrix(HImage image, int ldGray, int direction)
		{
			IntPtr proc = HalconAPI.PreCall(1760);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreI(proc, 0, ldGray);
			HalconAPI.StoreI(proc, 1, direction);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void MomentsGrayPlane(HImage image, out HTuple MRow, out HTuple MCol, out HTuple alpha, out HTuple beta, out HTuple mean)
		{
			IntPtr proc = HalconAPI.PreCall(1761);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out MRow);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out MCol);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out alpha);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out beta);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out mean);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void MomentsGrayPlane(HImage image, out double MRow, out double MCol, out double alpha, out double beta, out double mean)
		{
			IntPtr proc = HalconAPI.PreCall(1761);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out MRow);
			err = HalconAPI.LoadD(proc, 1, err, out MCol);
			err = HalconAPI.LoadD(proc, 2, err, out alpha);
			err = HalconAPI.LoadD(proc, 3, err, out beta);
			err = HalconAPI.LoadD(proc, 4, err, out mean);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HTuple PlaneDeviation(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(1762);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HTuple EllipticAxisGray(HImage image, out HTuple rb, out HTuple phi)
		{
			IntPtr proc = HalconAPI.PreCall(1763);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out rb);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public double EllipticAxisGray(HImage image, out double rb, out double phi)
		{
			IntPtr proc = HalconAPI.PreCall(1763);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out rb);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public HTuple AreaCenterGray(HImage image, out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1764);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public double AreaCenterGray(HImage image, out double row, out double column)
		{
			IntPtr proc = HalconAPI.PreCall(1764);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out row);
			err = HalconAPI.LoadD(proc, 2, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return doubleValue;
		}

		public HTuple GrayProjections(HImage image, string mode, out HTuple vertProjection)
		{
			IntPtr proc = HalconAPI.PreCall(1765);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out vertProjection);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public HImage GrabDataAsync(out HXLDCont contours, HFramegrabber acqHandle, double maxDelay, out HTuple data)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2029);
			HalconAPI.Store(proc, 0, acqHandle);
			HalconAPI.StoreD(proc, 1, maxDelay);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 2, err);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(acqHandle);
			return obj;
		}

		public HImage GrabDataAsync(out HXLDCont contours, HFramegrabber acqHandle, double maxDelay, out string data)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2029);
			HalconAPI.Store(proc, 0, acqHandle);
			HalconAPI.StoreD(proc, 1, maxDelay);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 2, err);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HalconAPI.LoadS(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(acqHandle);
			return obj;
		}

		public HImage GrabData(out HXLDCont contours, HFramegrabber acqHandle, out HTuple data)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2030);
			HalconAPI.Store(proc, 0, acqHandle);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 2, err);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(acqHandle);
			return obj;
		}

		public HImage GrabData(out HXLDCont contours, HFramegrabber acqHandle, out string data)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2030);
			HalconAPI.Store(proc, 0, acqHandle);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 2, err);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HalconAPI.LoadS(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(acqHandle);
			return obj;
		}

		public HTuple DoOcrMultiClassCnn(HImage image, HOCRCnn OCRHandle, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2056);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public string DoOcrMultiClassCnn(HImage image, HOCRCnn OCRHandle, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2056);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return stringValue;
		}

		public HTuple DoOcrSingleClassCnn(HImage image, HOCRCnn OCRHandle, HTuple num, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2057);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public string DoOcrSingleClassCnn(HImage image, HOCRCnn OCRHandle, HTuple num, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(2057);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return stringValue;
		}

		public HTuple DoOcrWordCnn(HImage image, HOCRCnn OCRHandle, string expression, int numAlternatives, int numCorrections, out HTuple confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(2058);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return tuple;
		}

		public string DoOcrWordCnn(HImage image, HOCRCnn OCRHandle, string expression, int numAlternatives, int numCorrections, out double confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(2058);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, OCRHandle);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(OCRHandle);
			return stringValue;
		}

		public HTuple HeightWidthRatio(out HTuple width, out HTuple ratio)
		{
			IntPtr proc = HalconAPI.PreCall(2119);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out width);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out ratio);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int HeightWidthRatio(out int width, out double ratio)
		{
			IntPtr proc = HalconAPI.PreCall(2119);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadI(proc, 1, err, out width);
			err = HalconAPI.LoadD(proc, 2, err, out ratio);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HRegion InsertObj(HRegion objectsInsert, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2121);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsInsert);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsInsert);
			return obj;
		}

		public new HRegion RemoveObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HRegion RemoveObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HRegion ReplaceObj(HRegion objectsReplace, HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return obj;
		}

		public HRegion ReplaceObj(HRegion objectsReplace, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return obj;
		}
	}
}
