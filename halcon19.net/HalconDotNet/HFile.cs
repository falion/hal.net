using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HFile : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFile()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFile(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFile(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("file");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFile obj)
		{
			obj = new HFile(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFile[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HFile[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HFile(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HFile(string fileName, HTuple fileType)
		{
			IntPtr proc = HalconAPI.PreCall(1659);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, fileType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fileType);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HFile(string fileName, string fileType)
		{
			IntPtr proc = HalconAPI.PreCall(1659);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, fileType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenFile(string fileName, HTuple fileType)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1659);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, fileType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fileType);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenFile(string fileName, string fileType)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1659);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, fileType);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FwriteString(HTuple stringVal)
		{
			IntPtr proc = HalconAPI.PreCall(1660);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, stringVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(stringVal);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void FwriteString(string stringVal)
		{
			IntPtr proc = HalconAPI.PreCall(1660);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, stringVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string FreadLine(out int isEOF)
		{
			IntPtr proc = HalconAPI.PreCall(1661);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadI(proc, 1, err, out isEOF);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return stringValue;
		}

		public string FreadString(out int isEOF)
		{
			IntPtr proc = HalconAPI.PreCall(1662);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadI(proc, 1, err, out isEOF);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return stringValue;
		}

		public string FreadChar()
		{
			IntPtr proc = HalconAPI.PreCall(1663);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return stringValue;
		}

		public void FnewLine()
		{
			IntPtr proc = HalconAPI.PreCall(1664);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void CloseFile(HFile[] fileHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(fileHandle);
			IntPtr proc = HalconAPI.PreCall(1665);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(fileHandle);
		}

		public void CloseFile()
		{
			IntPtr proc = HalconAPI.PreCall(1665);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
