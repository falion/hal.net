using System;

namespace HalconDotNet
{
	public class HTupleElements
	{
		private HTuple parent;

		private HTupleElementsImplementation elements;

		public int I
		{
			get
			{
				return elements.I[0];
			}
			set
			{
				int[] i = new int[1]
				{
					value
				};
				try
				{
					elements.I = i;
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					elements.I = i;
				}
			}
		}

		public int[] IArr
		{
			get
			{
				return elements.I;
			}
			set
			{
				try
				{
					elements.I = value;
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					elements.I = value;
				}
			}
		}

		public long L
		{
			get
			{
				return elements.L[0];
			}
			set
			{
				long[] l = new long[1]
				{
					value
				};
				try
				{
					elements.L = l;
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					elements.L = l;
				}
			}
		}

		public long[] LArr
		{
			get
			{
				return elements.L;
			}
			set
			{
				try
				{
					elements.L = value;
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					elements.L = value;
				}
			}
		}

		public double D
		{
			get
			{
				return elements.D[0];
			}
			set
			{
				double[] d = new double[1]
				{
					value
				};
				try
				{
					elements.D = d;
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					elements.D = d;
				}
			}
		}

		public double[] DArr
		{
			get
			{
				return elements.D;
			}
			set
			{
				try
				{
					elements.D = value;
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					elements.D = value;
				}
			}
		}

		public string S
		{
			get
			{
				return elements.S[0];
			}
			set
			{
				string[] s = new string[1]
				{
					value
				};
				try
				{
					elements.S = s;
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					elements.S = s;
				}
			}
		}

		public string[] SArr
		{
			get
			{
				return elements.S;
			}
			set
			{
				try
				{
					elements.S = value;
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					elements.S = value;
				}
			}
		}

		public HHandle H
		{
			get
			{
				return elements.H[0];
			}
			set
			{
				HHandle[] h = new HHandle[1]
				{
					value
				};
				try
				{
					elements.H = h;
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					elements.H = h;
				}
			}
		}

		public HHandle[] HArr
		{
			get
			{
				return elements.H;
			}
			set
			{
				try
				{
					elements.H = value;
				}
				catch (HTupleAccessException)
				{
					ConvertToMixed();
					elements.H = value;
				}
			}
		}

		public object O
		{
			get
			{
				return elements.O[0];
			}
			set
			{
				if (elements is HTupleElementsMixed)
				{
					elements.O[0] = value;
					return;
				}
				switch (HTupleImplementation.GetObjectType(value))
				{
				case 1:
					I = (int)value;
					break;
				case 129:
					L = (long)value;
					break;
				case 2:
					D = (double)value;
					break;
				case 32898:
					F = (float)value;
					break;
				case 4:
					S = (string)value;
					break;
				case 16:
					H = (HHandle)value;
					break;
				case 32900:
					IP = (IntPtr)value;
					break;
				default:
					throw new HTupleAccessException("Attempting to assign object containing invalid type");
				}
			}
		}

		public object[] OArr
		{
			get
			{
				return elements.O;
			}
			set
			{
				if (elements is HTupleElementsMixed)
				{
					elements.O = value;
					return;
				}
				switch (HTupleImplementation.GetObjectsType(value))
				{
				case 1:
					IArr = Array.ConvertAll(value, ObjectToInt);
					break;
				case 129:
					LArr = Array.ConvertAll(value, ObjectToLong);
					break;
				case 2:
					DArr = Array.ConvertAll(value, ObjectToDouble);
					break;
				case 32898:
					FArr = Array.ConvertAll(value, ObjectToFloat);
					break;
				case 4:
					SArr = Array.ConvertAll(value, ObjectToString);
					break;
				case 16:
					HArr = Array.ConvertAll(value, ObjectToHandle);
					break;
				case 32900:
					IPArr = Array.ConvertAll(value, ObjectToIntPtr);
					break;
				default:
					throw new HTupleAccessException("Attempting to assign object containing invalid type");
				}
			}
		}

		public float F
		{
			get
			{
				return (float)D;
			}
			set
			{
				D = value;
			}
		}

		public float[] FArr
		{
			get
			{
				double[] dArr = DArr;
				float[] array = new float[dArr.Length];
				for (int i = 0; i < dArr.Length; i++)
				{
					array[i] = (float)dArr[i];
				}
				return array;
			}
			set
			{
				double[] array = new double[value.Length];
				for (int i = 0; i < value.Length; i++)
				{
					array[i] = value[i];
				}
				DArr = array;
			}
		}

		public IntPtr IP
		{
			get
			{
				if (HalconAPI.isPlatform64)
				{
					if (Type == HTupleType.LONG || Type == HTupleType.HANDLE)
					{
						return new IntPtr(L);
					}
				}
				else if (Type == HTupleType.INTEGER || Type == HTupleType.HANDLE)
				{
					return new IntPtr(I);
				}
				throw new HTupleAccessException("Value does not represent a pointer on this platform");
			}
			set
			{
				if (Type == HTupleType.HANDLE)
				{
					value = H.Handle;
				}
				if (HalconAPI.isPlatform64)
				{
					L = value.ToInt64();
				}
				else
				{
					I = value.ToInt32();
				}
			}
		}

		public IntPtr[] IPArr
		{
			get
			{
				if (HalconAPI.isPlatform64 && Type == HTupleType.LONG)
				{
					IntPtr[] array = new IntPtr[LArr.Length];
					for (int i = 0; i < LArr.Length; i++)
					{
						array[i] = new IntPtr(LArr[i]);
					}
					return array;
				}
				if (Type == HTupleType.INTEGER)
				{
					IntPtr[] array2 = new IntPtr[IArr.Length];
					for (int j = 0; j < IArr.Length; j++)
					{
						array2[j] = new IntPtr(IArr[j]);
					}
					return array2;
				}
				throw new HTupleAccessException("Value does not represent a pointer on this platform");
			}
			set
			{
				if (HalconAPI.isPlatform64)
				{
					long[] array = new long[value.Length];
					for (int i = 0; i < value.Length; i++)
					{
						array[i] = value[i].ToInt64();
					}
					LArr = array;
				}
				else
				{
					int[] array2 = new int[value.Length];
					for (int j = 0; j < value.Length; j++)
					{
						array2[j] = value[j].ToInt32();
					}
					IArr = array2;
				}
			}
		}

		public HTupleType Type => elements.Type;

		internal int Length => elements.Length;

		internal HTupleElements()
		{
			parent = null;
			elements = new HTupleElementsImplementation();
		}

		internal HTupleElements(HTuple parent, HTupleInt32 source, int index)
		{
			this.parent = parent;
			elements = new HTupleElementsInt32(source, index);
		}

		internal HTupleElements(HTuple parent, HTupleInt32 source, int[] indices)
		{
			this.parent = parent;
			elements = new HTupleElementsInt32(source, indices);
		}

		internal HTupleElements(HTuple parent, HTupleInt64 tupleImp, int index)
		{
			this.parent = parent;
			elements = new HTupleElementsInt64(tupleImp, index);
		}

		internal HTupleElements(HTuple parent, HTupleInt64 tupleImp, int[] indices)
		{
			this.parent = parent;
			elements = new HTupleElementsInt64(tupleImp, indices);
		}

		internal HTupleElements(HTuple parent, HTupleDouble tupleImp, int index)
		{
			this.parent = parent;
			elements = new HTupleElementsDouble(tupleImp, index);
		}

		internal HTupleElements(HTuple parent, HTupleDouble tupleImp, int[] indices)
		{
			this.parent = parent;
			elements = new HTupleElementsDouble(tupleImp, indices);
		}

		internal HTupleElements(HTuple parent, HTupleString tupleImp, int index)
		{
			this.parent = parent;
			elements = new HTupleElementsString(tupleImp, index);
		}

		internal HTupleElements(HTuple parent, HTupleString tupleImp, int[] indices)
		{
			this.parent = parent;
			elements = new HTupleElementsString(tupleImp, indices);
		}

		internal HTupleElements(HTuple parent, HTupleHandle tupleImp, int index)
		{
			this.parent = parent;
			elements = new HTupleElementsHandle(tupleImp, index);
		}

		internal HTupleElements(HTuple parent, HTupleHandle tupleImp, int[] indices)
		{
			this.parent = parent;
			elements = new HTupleElementsHandle(tupleImp, indices);
		}

		internal HTupleElements(HTuple parent, HTupleMixed tupleImp, int index)
		{
			this.parent = parent;
			elements = new HTupleElementsMixed(tupleImp, index);
		}

		internal HTupleElements(HTuple parent, HTupleMixed tupleImp, int[] indices)
		{
			this.parent = parent;
			elements = new HTupleElementsMixed(tupleImp, indices);
		}

		public static int ObjectToInt(object o)
		{
			return (int)o;
		}

		public static long ObjectToLong(object o)
		{
			return (long)o;
		}

		public static double ObjectToDouble(object o)
		{
			return (double)o;
		}

		public static float ObjectToFloat(object o)
		{
			return (float)o;
		}

		public static string ObjectToString(object o)
		{
			return (string)o;
		}

		public static HHandle ObjectToHandle(object o)
		{
			return (HHandle)o;
		}

		public static IntPtr ObjectToIntPtr(object o)
		{
			return (IntPtr)o;
		}

		internal void ConvertToMixed()
		{
			if (elements is HTupleElementsMixed)
			{
				throw new HTupleAccessException();
			}
			elements = parent.ConvertToMixed(elements.getIndices());
		}

		public static HTuple operator +(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 + t4;
				}
			}
		}

		public static HTuple operator +(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 + t4;
				}
			}
		}

		public static HTuple operator +(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 + t4;
				}
			}
		}

		public static HTuple operator +(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 + t4;
				}
			}
		}

		public static HTuple operator +(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 + t4;
				}
			}
		}

		public static HTuple operator +(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 + t4;
				}
			}
		}

		public static HTuple operator +(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 + t2;
			}
		}

		public static HTuple operator -(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 - t4;
				}
			}
		}

		public static HTuple operator -(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 - t4;
				}
			}
		}

		public static HTuple operator -(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 - t4;
				}
			}
		}

		public static HTuple operator -(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 - t4;
				}
			}
		}

		public static HTuple operator -(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 - t4;
				}
			}
		}

		public static HTuple operator -(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 - t4;
				}
			}
		}

		public static HTuple operator -(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 - t2;
			}
		}

		public static HTuple operator *(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 * t4;
				}
			}
		}

		public static HTuple operator *(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 * t4;
				}
			}
		}

		public static HTuple operator *(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 * t4;
				}
			}
		}

		public static HTuple operator *(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 * t4;
				}
			}
		}

		public static HTuple operator *(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 * t4;
				}
			}
		}

		public static HTuple operator *(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 * t4;
				}
			}
		}

		public static HTuple operator *(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 * t2;
			}
		}

		public static HTuple operator /(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 / t4;
				}
			}
		}

		public static HTuple operator /(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 / t4;
				}
			}
		}

		public static HTuple operator /(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 / t4;
				}
			}
		}

		public static HTuple operator /(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 / t4;
				}
			}
		}

		public static HTuple operator /(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 / t4;
				}
			}
		}

		public static HTuple operator /(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 / t4;
				}
			}
		}

		public static HTuple operator /(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 / t2;
			}
		}

		public static HTuple operator %(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 % t4;
				}
			}
		}

		public static HTuple operator %(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 % t4;
				}
			}
		}

		public static HTuple operator %(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 % t4;
				}
			}
		}

		public static HTuple operator %(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 % t4;
				}
			}
		}

		public static HTuple operator %(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 % t4;
				}
			}
		}

		public static HTuple operator %(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 % t4;
				}
			}
		}

		public static HTuple operator %(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 % t2;
			}
		}

		public static HTuple operator &(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 & t4;
				}
			}
		}

		public static HTuple operator &(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 & t4;
				}
			}
		}

		public static HTuple operator &(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 & t4;
				}
			}
		}

		public static HTuple operator &(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 & t4;
				}
			}
		}

		public static HTuple operator &(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 & t4;
				}
			}
		}

		public static HTuple operator &(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 & t4;
				}
			}
		}

		public static HTuple operator &(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 & t2;
			}
		}

		public static HTuple operator |(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 | t4;
				}
			}
		}

		public static HTuple operator |(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 | t4;
				}
			}
		}

		public static HTuple operator |(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 | t4;
				}
			}
		}

		public static HTuple operator |(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 | t4;
				}
			}
		}

		public static HTuple operator |(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 | t4;
				}
			}
		}

		public static HTuple operator |(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 | t4;
				}
			}
		}

		public static HTuple operator |(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 | t2;
			}
		}

		public static HTuple operator ^(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 ^ t4;
				}
			}
		}

		public static HTuple operator ^(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 ^ t4;
				}
			}
		}

		public static HTuple operator ^(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 ^ t4;
				}
			}
		}

		public static HTuple operator ^(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 ^ t4;
				}
			}
		}

		public static HTuple operator ^(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 ^ t4;
				}
			}
		}

		public static HTuple operator ^(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 ^ t4;
				}
			}
		}

		public static HTuple operator ^(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 ^ t2;
			}
		}

		public static bool operator <(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 < t4;
				}
			}
		}

		public static bool operator <(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 < t4;
				}
			}
		}

		public static bool operator <(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 < t4;
				}
			}
		}

		public static bool operator <(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 < t4;
				}
			}
		}

		public static bool operator <(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 < t4;
				}
			}
		}

		public static bool operator <(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 < t4;
				}
			}
		}

		public static bool operator <(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 < t2;
			}
		}

		public static bool operator >(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 > t4;
				}
			}
		}

		public static bool operator >(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 > t4;
				}
			}
		}

		public static bool operator >(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 > t4;
				}
			}
		}

		public static bool operator >(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 > t4;
				}
			}
		}

		public static bool operator >(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 > t4;
				}
			}
		}

		public static bool operator >(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 > t4;
				}
			}
		}

		public static bool operator >(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 > t2;
			}
		}

		public static bool operator <=(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 <= t4;
				}
			}
		}

		public static bool operator <=(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 <= t4;
				}
			}
		}

		public static bool operator <=(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 <= t4;
				}
			}
		}

		public static bool operator <=(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 <= t4;
				}
			}
		}

		public static bool operator <=(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 <= t4;
				}
			}
		}

		public static bool operator <=(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 <= t4;
				}
			}
		}

		public static bool operator <=(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 <= t2;
			}
		}

		public static bool operator >=(HTupleElements e1, int t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 >= t4;
				}
			}
		}

		public static bool operator >=(HTupleElements e1, long t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 >= t4;
				}
			}
		}

		public static bool operator >=(HTupleElements e1, float t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 >= t4;
				}
			}
		}

		public static bool operator >=(HTupleElements e1, double t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 >= t4;
				}
			}
		}

		public static bool operator >=(HTupleElements e1, string t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 >= t4;
				}
			}
		}

		public static bool operator >=(HTupleElements e1, HTupleElements t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				using (HTuple t4 = (HTuple)t2)
				{
					return t3 >= t4;
				}
			}
		}

		public static bool operator >=(HTupleElements e1, HTuple t2)
		{
			using (HTuple t3 = (HTuple)e1)
			{
				return t3 >= t2;
			}
		}

		public static implicit operator bool(HTupleElements hte)
		{
			return hte.I != 0;
		}

		public static implicit operator int(HTupleElements hte)
		{
			return hte.I;
		}

		public static implicit operator long(HTupleElements hte)
		{
			return hte.L;
		}

		public static implicit operator IntPtr(HTupleElements hte)
		{
			return hte.IP;
		}

		public static implicit operator double(HTupleElements hte)
		{
			return hte.D;
		}

		public static implicit operator string(HTupleElements hte)
		{
			return hte.S;
		}

		public static implicit operator HTupleElements(int i)
		{
			return new HTuple(i)[0];
		}

		public static implicit operator HTupleElements(long l)
		{
			return new HTuple(l)[0];
		}

		public static implicit operator HTupleElements(IntPtr ip)
		{
			return new HTuple(ip)[0];
		}

		public static implicit operator HTupleElements(double d)
		{
			return new HTuple(d)[0];
		}

		public static implicit operator HTupleElements(string s)
		{
			return new HTuple(s)[0];
		}

		public static implicit operator HTupleElements(HHandle h)
		{
			HTupleHandle data = new HTupleHandle(new HHandle[1]
			{
				h
			}, copy: false);
			return new HTuple(data);
		}
	}
}
