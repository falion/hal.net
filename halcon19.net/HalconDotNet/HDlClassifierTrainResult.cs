using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HDlClassifierTrainResult : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifierTrainResult()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifierTrainResult(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifierTrainResult(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("dl_classifier_train_result");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlClassifierTrainResult obj)
		{
			obj = new HDlClassifierTrainResult(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlClassifierTrainResult[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDlClassifierTrainResult[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HDlClassifierTrainResult(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HDlClassifierTrainResult(HImage batchImages, HDlClassifier DLClassifierHandle, HTuple batchLabels)
		{
			IntPtr proc = HalconAPI.PreCall(2131);
			HalconAPI.Store(proc, 1, batchImages);
			HalconAPI.Store(proc, 0, DLClassifierHandle);
			HalconAPI.Store(proc, 1, batchLabels);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(batchLabels);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(batchImages);
			GC.KeepAlive(DLClassifierHandle);
		}

		public static void ClearDlClassifierTrainResult(HDlClassifierTrainResult[] DLClassifierTrainResultHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(DLClassifierTrainResultHandle);
			IntPtr proc = HalconAPI.PreCall(2105);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(DLClassifierTrainResultHandle);
		}

		public void ClearDlClassifierTrainResult()
		{
			IntPtr proc = HalconAPI.PreCall(2105);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetDlClassifierTrainResult(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2116);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDlClassifierTrainResult(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2116);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void TrainDlClassifierBatch(HImage batchImages, HDlClassifier DLClassifierHandle, HTuple batchLabels)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2131);
			HalconAPI.Store(proc, 1, batchImages);
			HalconAPI.Store(proc, 0, DLClassifierHandle);
			HalconAPI.Store(proc, 1, batchLabels);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(batchLabels);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(batchImages);
			GC.KeepAlive(DLClassifierHandle);
		}
	}
}
