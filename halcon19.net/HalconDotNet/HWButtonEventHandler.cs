using System.Windows.Input;

namespace HalconDotNet
{
	internal delegate void HWButtonEventHandler(int x, int y, MouseButton button, MouseButtonState state);
}
