using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HShapeModel3D : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HShapeModel3D()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HShapeModel3D(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HShapeModel3D(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("shape_model_3d");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HShapeModel3D obj)
		{
			obj = new HShapeModel3D(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HShapeModel3D[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HShapeModel3D[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HShapeModel3D(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HShapeModel3D(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1052);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HShapeModel3D(HObjectModel3D objectModel3D, HCamPar camParam, double refRotX, double refRotY, double refRotZ, string orderOfRotation, double longitudeMin, double longitudeMax, double latitudeMin, double latitudeMax, double camRollMin, double camRollMax, double distMin, double distMax, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1059);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.StoreD(proc, 2, refRotX);
			HalconAPI.StoreD(proc, 3, refRotY);
			HalconAPI.StoreD(proc, 4, refRotZ);
			HalconAPI.StoreS(proc, 5, orderOfRotation);
			HalconAPI.StoreD(proc, 6, longitudeMin);
			HalconAPI.StoreD(proc, 7, longitudeMax);
			HalconAPI.StoreD(proc, 8, latitudeMin);
			HalconAPI.StoreD(proc, 9, latitudeMax);
			HalconAPI.StoreD(proc, 10, camRollMin);
			HalconAPI.StoreD(proc, 11, camRollMax);
			HalconAPI.StoreD(proc, 12, distMin);
			HalconAPI.StoreD(proc, 13, distMax);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public HShapeModel3D(HObjectModel3D objectModel3D, HCamPar camParam, double refRotX, double refRotY, double refRotZ, string orderOfRotation, double longitudeMin, double longitudeMax, double latitudeMin, double latitudeMax, double camRollMin, double camRollMax, double distMin, double distMax, int minContrast, string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1059);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.StoreD(proc, 2, refRotX);
			HalconAPI.StoreD(proc, 3, refRotY);
			HalconAPI.StoreD(proc, 4, refRotZ);
			HalconAPI.StoreS(proc, 5, orderOfRotation);
			HalconAPI.StoreD(proc, 6, longitudeMin);
			HalconAPI.StoreD(proc, 7, longitudeMax);
			HalconAPI.StoreD(proc, 8, latitudeMin);
			HalconAPI.StoreD(proc, 9, latitudeMax);
			HalconAPI.StoreD(proc, 10, camRollMin);
			HalconAPI.StoreD(proc, 11, camRollMax);
			HalconAPI.StoreD(proc, 12, distMin);
			HalconAPI.StoreD(proc, 13, distMax);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.StoreS(proc, 15, genParamName);
			HalconAPI.StoreI(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeShapeModel3d();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HShapeModel3D(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeShapeModel3d(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeShapeModel3d();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HShapeModel3D Deserialize(Stream stream)
		{
			HShapeModel3D hShapeModel3D = new HShapeModel3D();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hShapeModel3D.DeserializeShapeModel3d(hSerializedItem);
			hSerializedItem.Dispose();
			return hShapeModel3D;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HShapeModel3D Clone()
		{
			HSerializedItem hSerializedItem = SerializeShapeModel3d();
			HShapeModel3D hShapeModel3D = new HShapeModel3D();
			hShapeModel3D.DeserializeShapeModel3d(hSerializedItem);
			hSerializedItem.Dispose();
			return hShapeModel3D;
		}

		public static void ClearShapeModel3d(HShapeModel3D[] shapeModel3DID)
		{
			HTuple hTuple = HHandleBase.ConcatArray(shapeModel3DID);
			IntPtr proc = HalconAPI.PreCall(1049);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(shapeModel3DID);
		}

		public void ClearShapeModel3d()
		{
			IntPtr proc = HalconAPI.PreCall(1049);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeShapeModel3d(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1050);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeShapeModel3d()
		{
			IntPtr proc = HalconAPI.PreCall(1051);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadShapeModel3d(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1052);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteShapeModel3d(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1053);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HPose TransPoseShapeModel3d(HPose poseIn, string transformation)
		{
			IntPtr proc = HalconAPI.PreCall(1054);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, poseIn);
			HalconAPI.StoreS(proc, 2, transformation);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(poseIn);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ProjectShapeModel3d(HCamPar camParam, HPose pose, string hiddenSurfaceRemoval, HTuple minFaceAngle)
		{
			IntPtr proc = HalconAPI.PreCall(1055);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.StoreS(proc, 3, hiddenSurfaceRemoval);
			HalconAPI.Store(proc, 4, minFaceAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.UnpinTuple(minFaceAngle);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ProjectShapeModel3d(HCamPar camParam, HPose pose, string hiddenSurfaceRemoval, double minFaceAngle)
		{
			IntPtr proc = HalconAPI.PreCall(1055);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.Store(proc, 2, pose);
			HalconAPI.StoreS(proc, 3, hiddenSurfaceRemoval);
			HalconAPI.StoreD(proc, 4, minFaceAngle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(pose);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont GetShapeModel3dContours(int level, int view, out HPose viewPose)
		{
			IntPtr proc = HalconAPI.PreCall(1056);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, level);
			HalconAPI.StoreI(proc, 2, view);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			err = HPose.LoadNew(proc, 0, err, out viewPose);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetShapeModel3dParams(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1057);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetShapeModel3dParams(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1057);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HPose[] FindShapeModel3d(HImage image, double minScore, double greediness, HTuple numLevels, HTuple genParamName, HTuple genParamValue, out HTuple covPose, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1058);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, minScore);
			HalconAPI.StoreD(proc, 2, greediness);
			HalconAPI.Store(proc, 3, numLevels);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(tuple);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return result;
		}

		public void CreateShapeModel3d(HObjectModel3D objectModel3D, HCamPar camParam, double refRotX, double refRotY, double refRotZ, string orderOfRotation, double longitudeMin, double longitudeMax, double latitudeMin, double latitudeMax, double camRollMin, double camRollMax, double distMin, double distMax, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1059);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.StoreD(proc, 2, refRotX);
			HalconAPI.StoreD(proc, 3, refRotY);
			HalconAPI.StoreD(proc, 4, refRotZ);
			HalconAPI.StoreS(proc, 5, orderOfRotation);
			HalconAPI.StoreD(proc, 6, longitudeMin);
			HalconAPI.StoreD(proc, 7, longitudeMax);
			HalconAPI.StoreD(proc, 8, latitudeMin);
			HalconAPI.StoreD(proc, 9, latitudeMax);
			HalconAPI.StoreD(proc, 10, camRollMin);
			HalconAPI.StoreD(proc, 11, camRollMax);
			HalconAPI.StoreD(proc, 12, distMin);
			HalconAPI.StoreD(proc, 13, distMax);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}

		public void CreateShapeModel3d(HObjectModel3D objectModel3D, HCamPar camParam, double refRotX, double refRotY, double refRotZ, string orderOfRotation, double longitudeMin, double longitudeMax, double latitudeMin, double latitudeMax, double camRollMin, double camRollMax, double distMin, double distMax, int minContrast, string genParamName, int genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1059);
			HalconAPI.Store(proc, 0, objectModel3D);
			HalconAPI.Store(proc, 1, camParam);
			HalconAPI.StoreD(proc, 2, refRotX);
			HalconAPI.StoreD(proc, 3, refRotY);
			HalconAPI.StoreD(proc, 4, refRotZ);
			HalconAPI.StoreS(proc, 5, orderOfRotation);
			HalconAPI.StoreD(proc, 6, longitudeMin);
			HalconAPI.StoreD(proc, 7, longitudeMax);
			HalconAPI.StoreD(proc, 8, latitudeMin);
			HalconAPI.StoreD(proc, 9, latitudeMax);
			HalconAPI.StoreD(proc, 10, camRollMin);
			HalconAPI.StoreD(proc, 11, camRollMax);
			HalconAPI.StoreD(proc, 12, distMin);
			HalconAPI.StoreD(proc, 13, distMax);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.StoreS(proc, 15, genParamName);
			HalconAPI.StoreI(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectModel3D);
		}
	}
}
