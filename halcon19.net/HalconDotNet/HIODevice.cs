using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HIODevice : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HIODevice()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HIODevice(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HIODevice(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("io_device");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HIODevice obj)
		{
			obj = new HIODevice(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HIODevice[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HIODevice[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HIODevice(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HIODevice(string IOInterfaceName, HTuple IODeviceName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2022);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.Store(proc, 1, IODeviceName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(IODeviceName);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HIOChannel[] OpenIoChannel(HTuple IOChannelName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2016);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(IOChannelName);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HIOChannel.LoadNew(proc, 0, err, out HIOChannel[] obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HIOChannel OpenIoChannel(string IOChannelName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2016);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HIOChannel.LoadNew(proc, 0, err, out HIOChannel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple QueryIoDevice(HTuple IOChannelName, HTuple query)
		{
			IntPtr proc = HalconAPI.PreCall(2017);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(IOChannelName);
			HalconAPI.UnpinTuple(query);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple QueryIoDevice(string IOChannelName, HTuple query)
		{
			IntPtr proc = HalconAPI.PreCall(2017);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, IOChannelName);
			HalconAPI.Store(proc, 2, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(query);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple ControlIoDevice(string action, HTuple argument)
		{
			IntPtr proc = HalconAPI.PreCall(2018);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, action);
			HalconAPI.Store(proc, 2, argument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(argument);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple ControlIoDevice(string action, string argument)
		{
			IntPtr proc = HalconAPI.PreCall(2018);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, action);
			HalconAPI.StoreS(proc, 2, argument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetIoDeviceParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2019);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetIoDeviceParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2019);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetIoDeviceParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2020);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetIoDeviceParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2020);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void CloseIoDevice()
		{
			IntPtr proc = HalconAPI.PreCall(2021);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void OpenIoDevice(string IOInterfaceName, HTuple IODeviceName, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2022);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.Store(proc, 1, IODeviceName);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(IODeviceName);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HTuple ControlIoInterface(string IOInterfaceName, string action, HTuple argument)
		{
			IntPtr proc = HalconAPI.PreCall(2023);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.StoreS(proc, 1, action);
			HalconAPI.Store(proc, 2, argument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(argument);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple ControlIoInterface(string IOInterfaceName, string action, string argument)
		{
			IntPtr proc = HalconAPI.PreCall(2023);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.StoreS(proc, 1, action);
			HalconAPI.StoreS(proc, 2, argument);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple QueryIoInterface(string IOInterfaceName, HTuple query)
		{
			IntPtr proc = HalconAPI.PreCall(2024);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.Store(proc, 1, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(query);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple QueryIoInterface(string IOInterfaceName, string query)
		{
			IntPtr proc = HalconAPI.PreCall(2024);
			HalconAPI.StoreS(proc, 0, IOInterfaceName);
			HalconAPI.StoreS(proc, 1, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}
	}
}
