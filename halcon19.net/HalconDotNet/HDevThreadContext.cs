using System;
using System.ComponentModel;

namespace HalconDotNet
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class HDevThreadContext : IDisposable
	{
		private IntPtr mContextHandle;

		public IntPtr Handle => mContextHandle;

		public HDevThreadContext()
		{
			HDevThread.HCkHLib(HalconAPI.HXCreateHThreadContext(out mContextHandle));
		}

		public void Dispose()
		{
			if (mContextHandle != IntPtr.Zero)
			{
				HDevThread.HCkHLib(HalconAPI.HXClearHThreadContext(mContextHandle));
				mContextHandle = IntPtr.Zero;
			}
		}
	}
}
