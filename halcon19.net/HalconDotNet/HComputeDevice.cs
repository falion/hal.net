using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HComputeDevice : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComputeDevice()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComputeDevice(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HComputeDevice(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("compute_device");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HComputeDevice obj)
		{
			obj = new HComputeDevice(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HComputeDevice[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HComputeDevice[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HComputeDevice(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HComputeDevice(int deviceIdentifier)
		{
			IntPtr proc = HalconAPI.PreCall(304);
			HalconAPI.StoreI(proc, 0, deviceIdentifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetComputeDeviceParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(296);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetComputeDeviceParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(297);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetComputeDeviceParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(297);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void ReleaseAllComputeDevices()
		{
			IntPtr proc = HalconAPI.PreCall(298);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public void ReleaseComputeDevice()
		{
			IntPtr proc = HalconAPI.PreCall(299);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void DeactivateAllComputeDevices()
		{
			IntPtr proc = HalconAPI.PreCall(300);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public void DeactivateComputeDevice()
		{
			IntPtr proc = HalconAPI.PreCall(301);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ActivateComputeDevice()
		{
			IntPtr proc = HalconAPI.PreCall(302);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void InitComputeDevice(HTuple operators)
		{
			IntPtr proc = HalconAPI.PreCall(303);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, operators);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(operators);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void OpenComputeDevice(int deviceIdentifier)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(304);
			HalconAPI.StoreI(proc, 0, deviceIdentifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HTuple GetComputeDeviceInfo(int deviceIdentifier, string infoName)
		{
			IntPtr proc = HalconAPI.PreCall(305);
			HalconAPI.StoreI(proc, 0, deviceIdentifier);
			HalconAPI.StoreS(proc, 1, infoName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple QueryAvailableComputeDevices()
		{
			IntPtr proc = HalconAPI.PreCall(306);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}
	}
}
