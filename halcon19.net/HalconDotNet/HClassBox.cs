using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HClassBox : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassBox(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassBox(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("class_box");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassBox obj)
		{
			obj = new HClassBox(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassBox[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HClassBox[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HClassBox(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HClassBox()
		{
			IntPtr proc = HalconAPI.PreCall(1895);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeClassBox();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassBox(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeClassBox(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeClassBox();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HClassBox Deserialize(Stream stream)
		{
			HClassBox hClassBox = new HClassBox();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hClassBox.DeserializeClassBox(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassBox;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HClassBox Clone()
		{
			HSerializedItem hSerializedItem = SerializeClassBox();
			HClassBox hClassBox = new HClassBox();
			hClassBox.DeserializeClassBox(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassBox;
		}

		public void LearnNdimBox(HRegion foreground, HRegion background, HImage multiChannelImage)
		{
			IntPtr proc = HalconAPI.PreCall(438);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, foreground);
			HalconAPI.Store(proc, 2, background);
			HalconAPI.Store(proc, 3, multiChannelImage);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(foreground);
			GC.KeepAlive(background);
			GC.KeepAlive(multiChannelImage);
		}

		public HRegion ClassNdimBox(HImage multiChannelImage)
		{
			IntPtr proc = HalconAPI.PreCall(439);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, multiChannelImage);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(multiChannelImage);
			return obj;
		}

		public void DeserializeClassBox(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1884);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, serializedItemHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeClassBox()
		{
			IntPtr proc = HalconAPI.PreCall(1885);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void WriteClassBox(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1886);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetClassBoxParam(string flag, HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(1887);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, flag);
			HalconAPI.Store(proc, 2, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(value);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetClassBoxParam(string flag, double value)
		{
			IntPtr proc = HalconAPI.PreCall(1887);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, flag);
			HalconAPI.StoreD(proc, 2, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadClassBox(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1889);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void LearnSampsetBox(HFeatureSet sampKey, string outfile, int NSamples, double stopError, int errorN)
		{
			IntPtr proc = HalconAPI.PreCall(1890);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, sampKey);
			HalconAPI.StoreS(proc, 2, outfile);
			HalconAPI.StoreI(proc, 3, NSamples);
			HalconAPI.StoreD(proc, 4, stopError);
			HalconAPI.StoreI(proc, 5, errorN);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(sampKey);
		}

		public void LearnClassBox(HTuple features, int classVal)
		{
			IntPtr proc = HalconAPI.PreCall(1891);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.StoreI(proc, 2, classVal);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetClassBoxParam(string flag)
		{
			IntPtr proc = HalconAPI.PreCall(1892);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, flag);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void CloseClassBox()
		{
			IntPtr proc = HalconAPI.PreCall(1894);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateClassBox()
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1895);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DescriptClassBox(int dimensions, out HTuple boxIdx, out HTuple boxLowerBound, out HTuple boxHigherBound, out HTuple boxNumSamplesTrain, out HTuple boxNumSamplesWrong)
		{
			IntPtr proc = HalconAPI.PreCall(1896);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, dimensions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out boxIdx);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out boxLowerBound);
			err = HTuple.LoadNew(proc, 3, HTupleType.INTEGER, err, out boxHigherBound);
			err = HTuple.LoadNew(proc, 4, HTupleType.INTEGER, err, out boxNumSamplesTrain);
			err = HTuple.LoadNew(proc, 5, HTupleType.INTEGER, err, out boxNumSamplesWrong);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int DescriptClassBox(int dimensions, out int boxIdx, out int boxLowerBound, out int boxHigherBound, out int boxNumSamplesTrain, out int boxNumSamplesWrong)
		{
			IntPtr proc = HalconAPI.PreCall(1896);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, dimensions);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadI(proc, 1, err, out boxIdx);
			err = HalconAPI.LoadI(proc, 2, err, out boxLowerBound);
			err = HalconAPI.LoadI(proc, 3, err, out boxHigherBound);
			err = HalconAPI.LoadI(proc, 4, err, out boxNumSamplesTrain);
			err = HalconAPI.LoadI(proc, 5, err, out boxNumSamplesWrong);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public double TestSampsetBox(HFeatureSet sampKey)
		{
			IntPtr proc = HalconAPI.PreCall(1897);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, sampKey);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sampKey);
			return doubleValue;
		}

		public int EnquireRejectClassBox(HTuple featureList)
		{
			IntPtr proc = HalconAPI.PreCall(1898);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, featureList);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(featureList);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public int EnquireClassBox(HTuple featureList)
		{
			IntPtr proc = HalconAPI.PreCall(1899);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, featureList);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(featureList);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}
	}
}
