using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HOCRKnn : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRKnn()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRKnn(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRKnn(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("ocr_knn");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRKnn obj)
		{
			obj = new HOCRKnn(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRKnn[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HOCRKnn[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HOCRKnn(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HOCRKnn(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(650);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCRKnn(int widthCharacter, int heightCharacter, string interpolation, HTuple features, HTuple characters, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(654);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.Store(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(characters);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCRKnn(int widthCharacter, int heightCharacter, string interpolation, string features, HTuple characters, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(654);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.StoreS(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(characters);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeOcrClassKnn();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRKnn(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeOcrClassKnn(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeOcrClassKnn();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HOCRKnn Deserialize(Stream stream)
		{
			HOCRKnn hOCRKnn = new HOCRKnn();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hOCRKnn.DeserializeOcrClassKnn(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRKnn;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HOCRKnn Clone()
		{
			HSerializedItem hSerializedItem = SerializeOcrClassKnn();
			HOCRKnn hOCRKnn = new HOCRKnn();
			hOCRKnn.DeserializeOcrClassKnn(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRKnn;
		}

		public HTuple DoOcrWordKnn(HRegion character, HImage image, string expression, int numAlternatives, int numCorrections, out HTuple confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(647);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DoOcrWordKnn(HRegion character, HImage image, string expression, int numAlternatives, int numCorrections, out double confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(647);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return stringValue;
		}

		public void DeserializeOcrClassKnn(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(648);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeOcrClassKnn()
		{
			IntPtr proc = HalconAPI.PreCall(649);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadOcrClassKnn(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(650);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteOcrClassKnn(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(651);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ClearOcrClassKnn()
		{
			IntPtr proc = HalconAPI.PreCall(653);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateOcrClassKnn(int widthCharacter, int heightCharacter, string interpolation, HTuple features, HTuple characters, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(654);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.Store(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(characters);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcrClassKnn(int widthCharacter, int heightCharacter, string interpolation, string features, HTuple characters, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(654);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.StoreS(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(characters);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void TrainfOcrClassKnn(HTuple trainingFile, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(655);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, trainingFile);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void TrainfOcrClassKnn(string trainingFile, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(655);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, trainingFile);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetFeaturesOcrClassKnn(HImage character, string transform)
		{
			IntPtr proc = HalconAPI.PreCall(656);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.StoreS(proc, 1, transform);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			return tuple;
		}

		public void GetParamsOcrClassKnn(out int widthCharacter, out int heightCharacter, out string interpolation, out HTuple features, out HTuple characters, out string preprocessing, out int numTrees)
		{
			IntPtr proc = HalconAPI.PreCall(657);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out widthCharacter);
			err = HalconAPI.LoadI(proc, 1, err, out heightCharacter);
			err = HalconAPI.LoadS(proc, 2, err, out interpolation);
			err = HTuple.LoadNew(proc, 3, err, out features);
			err = HTuple.LoadNew(proc, 4, err, out characters);
			err = HalconAPI.LoadS(proc, 5, err, out preprocessing);
			err = HalconAPI.LoadI(proc, 6, err, out numTrees);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetParamsOcrClassKnn(out int widthCharacter, out int heightCharacter, out string interpolation, out string features, out HTuple characters, out string preprocessing, out int numTrees)
		{
			IntPtr proc = HalconAPI.PreCall(657);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out widthCharacter);
			err = HalconAPI.LoadI(proc, 1, err, out heightCharacter);
			err = HalconAPI.LoadS(proc, 2, err, out interpolation);
			err = HalconAPI.LoadS(proc, 3, err, out features);
			err = HTuple.LoadNew(proc, 4, err, out characters);
			err = HalconAPI.LoadS(proc, 5, err, out preprocessing);
			err = HalconAPI.LoadI(proc, 6, err, out numTrees);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DoOcrMultiClassKnn(HRegion character, HImage image, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(658);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DoOcrMultiClassKnn(HRegion character, HImage image, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(658);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return stringValue;
		}

		public HTuple DoOcrSingleClassKnn(HRegion character, HImage image, HTuple numClasses, HTuple numNeighbors, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(659);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, numClasses);
			HalconAPI.Store(proc, 2, numNeighbors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numClasses);
			HalconAPI.UnpinTuple(numNeighbors);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DoOcrSingleClassKnn(HRegion character, HImage image, HTuple numClasses, HTuple numNeighbors, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(659);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, numClasses);
			HalconAPI.Store(proc, 2, numNeighbors);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numClasses);
			HalconAPI.UnpinTuple(numNeighbors);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return stringValue;
		}

		public HTuple SelectFeatureSetTrainfKnn(HTuple trainingFile, HTuple featureList, string selectionMethod, int width, int height, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(660);
			HalconAPI.Store(proc, 0, trainingFile);
			HalconAPI.Store(proc, 1, featureList);
			HalconAPI.StoreS(proc, 2, selectionMethod);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(featureList);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple SelectFeatureSetTrainfKnn(string trainingFile, string featureList, string selectionMethod, int width, int height, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(660);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.StoreS(proc, 1, featureList);
			HalconAPI.StoreS(proc, 2, selectionMethod);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}
	}
}
