using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HTextResult : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextResult()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextResult(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextResult(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("text_result");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextResult obj)
		{
			obj = new HTextResult(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextResult[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HTextResult[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HTextResult(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HTextResult(HImage image, HTextModel textModel)
		{
			IntPtr proc = HalconAPI.PreCall(417);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, textModel);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(textModel);
		}

		public static void ClearTextResult(HTextResult[] textResultID)
		{
			HTuple hTuple = HHandleBase.ConcatArray(textResultID);
			IntPtr proc = HalconAPI.PreCall(414);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(textResultID);
		}

		public void ClearTextResult()
		{
			IntPtr proc = HalconAPI.PreCall(414);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HObject GetTextObject(HTuple resultName)
		{
			IntPtr proc = HalconAPI.PreCall(415);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultName);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HObject GetTextObject(string resultName)
		{
			IntPtr proc = HalconAPI.PreCall(415);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetTextResult(HTuple resultName)
		{
			IntPtr proc = HalconAPI.PreCall(416);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetTextResult(string resultName)
		{
			IntPtr proc = HalconAPI.PreCall(416);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void FindText(HImage image, HTextModel textModel)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(417);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, textModel);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(textModel);
		}
	}
}
