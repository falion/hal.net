using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HSocket : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSocket()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSocket(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSocket(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("socket");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSocket obj)
		{
			obj = new HSocket(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSocket[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HSocket[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HSocket(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HSocket(string hostName, int port, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(342);
			HalconAPI.StoreS(proc, 0, hostName);
			HalconAPI.StoreI(proc, 1, port);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSocket(string hostName, int port, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(342);
			HalconAPI.StoreS(proc, 0, hostName);
			HalconAPI.StoreI(proc, 1, port);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSocket(int port, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(343);
			HalconAPI.StoreI(proc, 0, port);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSocket(int port, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(343);
			HalconAPI.StoreI(proc, 0, port);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HImage ReceiveImage()
		{
			IntPtr proc = HalconAPI.PreCall(325);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SendImage(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(326);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public HRegion ReceiveRegion()
		{
			IntPtr proc = HalconAPI.PreCall(327);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SendRegion(HRegion region)
		{
			IntPtr proc = HalconAPI.PreCall(328);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, region);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(region);
		}

		public HXLD ReceiveXld()
		{
			IntPtr proc = HalconAPI.PreCall(329);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLD.LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SendXld(HXLD XLD)
		{
			IntPtr proc = HalconAPI.PreCall(330);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, XLD);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(XLD);
		}

		public HTuple ReceiveTuple()
		{
			IntPtr proc = HalconAPI.PreCall(331);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SendTuple(HTuple tuple)
		{
			IntPtr proc = HalconAPI.PreCall(332);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, tuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(tuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SendTuple(string tuple)
		{
			IntPtr proc = HalconAPI.PreCall(332);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, tuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ReceiveData(HTuple format, out HTuple from)
		{
			IntPtr proc = HalconAPI.PreCall(333);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, format);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(format);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out from);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple ReceiveData(string format, out string from)
		{
			IntPtr proc = HalconAPI.PreCall(333);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, format);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HalconAPI.LoadS(proc, 1, err, out from);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SendData(string format, HTuple data, HTuple to)
		{
			IntPtr proc = HalconAPI.PreCall(334);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, format);
			HalconAPI.Store(proc, 2, data);
			HalconAPI.Store(proc, 3, to);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(data);
			HalconAPI.UnpinTuple(to);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SendData(string format, string data, string to)
		{
			IntPtr proc = HalconAPI.PreCall(334);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, format);
			HalconAPI.StoreS(proc, 2, data);
			HalconAPI.StoreS(proc, 3, to);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetSocketParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(335);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetSocketParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(335);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetSocketParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(336);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetSocketParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(336);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string GetNextSocketDataType()
		{
			IntPtr proc = HalconAPI.PreCall(337);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return stringValue;
		}

		public int GetSocketDescriptor()
		{
			IntPtr proc = HalconAPI.PreCall(338);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public static void CloseAllSockets()
		{
			IntPtr proc = HalconAPI.PreCall(339);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public void CloseSocket()
		{
			IntPtr proc = HalconAPI.PreCall(340);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HSocket SocketAcceptConnect(string wait)
		{
			IntPtr proc = HalconAPI.PreCall(341);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, wait);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 0, err, out HSocket obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void OpenSocketConnect(string hostName, int port, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(342);
			HalconAPI.StoreS(proc, 0, hostName);
			HalconAPI.StoreI(proc, 1, port);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenSocketConnect(string hostName, int port, string genParamName, string genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(342);
			HalconAPI.StoreS(proc, 0, hostName);
			HalconAPI.StoreI(proc, 1, port);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreS(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenSocketAccept(int port, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(343);
			HalconAPI.StoreI(proc, 0, port);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenSocketAccept(int port, string genParamName, string genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(343);
			HalconAPI.StoreI(proc, 0, port);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem ReceiveSerializedItem()
		{
			IntPtr proc = HalconAPI.PreCall(403);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SendSerializedItem(HSerializedItem serializedItemHandle)
		{
			IntPtr proc = HalconAPI.PreCall(404);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, serializedItemHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}
	}
}
