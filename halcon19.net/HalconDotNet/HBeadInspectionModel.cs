using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HBeadInspectionModel : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBeadInspectionModel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBeadInspectionModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBeadInspectionModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("bead_inspection_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HBeadInspectionModel obj)
		{
			obj = new HBeadInspectionModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HBeadInspectionModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HBeadInspectionModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HBeadInspectionModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HBeadInspectionModel(HXLD beadContour, HTuple targetThickness, HTuple thicknessTolerance, HTuple positionTolerance, string polarity, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1985);
			HalconAPI.Store(proc, 1, beadContour);
			HalconAPI.Store(proc, 0, targetThickness);
			HalconAPI.Store(proc, 1, thicknessTolerance);
			HalconAPI.Store(proc, 2, positionTolerance);
			HalconAPI.StoreS(proc, 3, polarity);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(targetThickness);
			HalconAPI.UnpinTuple(thicknessTolerance);
			HalconAPI.UnpinTuple(positionTolerance);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(beadContour);
		}

		public HBeadInspectionModel(HXLD beadContour, int targetThickness, int thicknessTolerance, int positionTolerance, string polarity, string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1985);
			HalconAPI.Store(proc, 1, beadContour);
			HalconAPI.StoreI(proc, 0, targetThickness);
			HalconAPI.StoreI(proc, 1, thicknessTolerance);
			HalconAPI.StoreI(proc, 2, positionTolerance);
			HalconAPI.StoreS(proc, 3, polarity);
			HalconAPI.StoreS(proc, 4, genParamName);
			HalconAPI.StoreI(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(beadContour);
		}

		public HTuple GetBeadInspectionParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1981);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetBeadInspectionParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1981);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetBeadInspectionParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1982);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetBeadInspectionParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1982);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HXLD ApplyBeadInspectionModel(HImage image, out HXLD rightContour, out HXLD errorSegment, out HTuple errorType)
		{
			IntPtr proc = HalconAPI.PreCall(1983);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLD.LoadNew(proc, 1, err, out HXLD obj);
			err = HXLD.LoadNew(proc, 2, err, out rightContour);
			err = HXLD.LoadNew(proc, 3, err, out errorSegment);
			err = HTuple.LoadNew(proc, 0, err, out errorType);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void ClearBeadInspectionModel()
		{
			IntPtr proc = HalconAPI.PreCall(1984);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateBeadInspectionModel(HXLD beadContour, HTuple targetThickness, HTuple thicknessTolerance, HTuple positionTolerance, string polarity, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1985);
			HalconAPI.Store(proc, 1, beadContour);
			HalconAPI.Store(proc, 0, targetThickness);
			HalconAPI.Store(proc, 1, thicknessTolerance);
			HalconAPI.Store(proc, 2, positionTolerance);
			HalconAPI.StoreS(proc, 3, polarity);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(targetThickness);
			HalconAPI.UnpinTuple(thicknessTolerance);
			HalconAPI.UnpinTuple(positionTolerance);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(beadContour);
		}

		public void CreateBeadInspectionModel(HXLD beadContour, int targetThickness, int thicknessTolerance, int positionTolerance, string polarity, string genParamName, int genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1985);
			HalconAPI.Store(proc, 1, beadContour);
			HalconAPI.StoreI(proc, 0, targetThickness);
			HalconAPI.StoreI(proc, 1, thicknessTolerance);
			HalconAPI.StoreI(proc, 2, positionTolerance);
			HalconAPI.StoreS(proc, 3, polarity);
			HalconAPI.StoreS(proc, 4, genParamName);
			HalconAPI.StoreI(proc, 5, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(beadContour);
		}
	}
}
