using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HXLDDistTrans : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDDistTrans()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDDistTrans(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDDistTrans(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("xld_dist_trans");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HXLDDistTrans obj)
		{
			obj = new HXLDDistTrans(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HXLDDistTrans[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HXLDDistTrans[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HXLDDistTrans(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HXLDDistTrans(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1353);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDDistTrans(HXLDCont contour, string mode, HTuple maxDistance)
		{
			IntPtr proc = HalconAPI.PreCall(1360);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, maxDistance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maxDistance);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
		}

		public HXLDDistTrans(HXLDCont contour, string mode, double maxDistance)
		{
			IntPtr proc = HalconAPI.PreCall(1360);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, maxDistance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeDistanceTransformXld();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDDistTrans(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeDistanceTransformXld(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeDistanceTransformXld();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HXLDDistTrans Deserialize(Stream stream)
		{
			HXLDDistTrans hXLDDistTrans = new HXLDDistTrans();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hXLDDistTrans.DeserializeDistanceTransformXld(hSerializedItem);
			hSerializedItem.Dispose();
			return hXLDDistTrans;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HXLDDistTrans Clone()
		{
			HSerializedItem hSerializedItem = SerializeDistanceTransformXld();
			HXLDDistTrans hXLDDistTrans = new HXLDDistTrans();
			hXLDDistTrans.DeserializeDistanceTransformXld(hSerializedItem);
			hSerializedItem.Dispose();
			return hXLDDistTrans;
		}

		public void ClearDistanceTransformXld()
		{
			IntPtr proc = HalconAPI.PreCall(1351);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HXLDCont ApplyDistanceTransformXld(HXLDCont contour)
		{
			IntPtr proc = HalconAPI.PreCall(1352);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
			return obj;
		}

		public void ReadDistanceTransformXld(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1353);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DeserializeDistanceTransformXld(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1354);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeDistanceTransformXld()
		{
			IntPtr proc = HalconAPI.PreCall(1355);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void WriteDistanceTransformXld(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1356);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDistanceTransformXldParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1357);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDistanceTransformXldParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1357);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetDistanceTransformXldParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1358);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDistanceTransformXldParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1358);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HXLDCont GetDistanceTransformXldContour()
		{
			IntPtr proc = HalconAPI.PreCall(1359);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void CreateDistanceTransformXld(HXLDCont contour, string mode, HTuple maxDistance)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1360);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, maxDistance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maxDistance);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
		}

		public void CreateDistanceTransformXld(HXLDCont contour, string mode, double maxDistance)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1360);
			HalconAPI.Store(proc, 1, contour);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, maxDistance);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour);
		}
	}
}
