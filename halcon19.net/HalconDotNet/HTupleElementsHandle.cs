namespace HalconDotNet
{
	internal class HTupleElementsHandle : HTupleElementsImplementation
	{
		internal HTupleElementsHandle(HTupleHandle source, int index)
			: base(source, index)
		{
		}

		internal HTupleElementsHandle(HTupleHandle source, int[] indices)
			: base(source, indices)
		{
		}

		public override HHandle[] getH()
		{
			if (indices == null)
			{
				return null;
			}
			HHandle[] array = new HHandle[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				array[i] = source.HArr[indices[i]];
			}
			return array;
		}

		public override int[] getI()
		{
			if (indices == null)
			{
				return null;
			}
			int[] array = new int[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				if (!HalconAPI.IsLegacyHandleMode())
				{
					throw new HTupleAccessException("Implicit access to handle as number is only allowed in legacy handle mode. Use *.H.Handle to get IntPtr value.");
				}
				if (HalconAPI.isPlatform64)
				{
					throw new HTupleAccessException("System.Int32 cannot represent a handle on this platform");
				}
				array[i] = (int)source.HArr[indices[i]].Handle;
				source.HArr[indices[i]].Detach();
			}
			return array;
		}

		public override long[] getL()
		{
			if (indices == null)
			{
				return null;
			}
			long[] array = new long[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				if (!HalconAPI.IsLegacyHandleMode())
				{
					throw new HTupleAccessException("Implicit access to handle as number is only allowed in legacy handle mode. Use *.H.Handle to get IntPtr value.");
				}
				array[i] = (long)source.HArr[indices[i]].Handle;
				source.HArr[indices[i]].Detach();
			}
			return array;
		}

		public override void setH(HHandle[] h)
		{
			if (!IsValidArrayForSetX(h))
			{
				return;
			}
			bool flag = h.Length == 1;
			for (int i = 0; i < indices.Length; i++)
			{
				if (source.HArr[indices[i]] != null)
				{
					source.HArr[indices[i]].Dispose();
				}
				source.HArr[indices[i]] = new HHandle(h[(!flag) ? i : 0]);
			}
		}

		public override object[] getO()
		{
			if (indices == null)
			{
				return null;
			}
			object[] array = new object[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				array[i] = source.HArr[indices[i]];
			}
			return array;
		}

		public override HTupleType getType()
		{
			return HTupleType.HANDLE;
		}
	}
}
