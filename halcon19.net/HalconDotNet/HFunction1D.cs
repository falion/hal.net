using System;

namespace HalconDotNet
{
	public class HFunction1D : HData
	{
		public HFunction1D()
		{
		}

		internal HFunction1D(HData data)
			: base(data)
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, HTupleType type, int err, out HFunction1D obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HFunction1D(new HData(tuple));
			return err;
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFunction1D obj)
		{
			return LoadNew(proc, parIndex, HTupleType.MIXED, err, out obj);
		}

		public HFunction1D(HTuple YValues)
		{
			IntPtr proc = HalconAPI.PreCall(1399);
			HalconAPI.Store(proc, 0, YValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(YValues);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HFunction1D(double YValues)
		{
			IntPtr proc = HalconAPI.PreCall(1399);
			HalconAPI.StoreD(proc, 0, YValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HFunction1D(HTuple XValues, HTuple YValues)
		{
			IntPtr proc = HalconAPI.PreCall(1400);
			HalconAPI.Store(proc, 0, XValues);
			HalconAPI.Store(proc, 1, YValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(XValues);
			HalconAPI.UnpinTuple(YValues);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HFunction1D(double XValues, double YValues)
		{
			IntPtr proc = HalconAPI.PreCall(1400);
			HalconAPI.StoreD(proc, 0, XValues);
			HalconAPI.StoreD(proc, 1, YValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public static HFunction1D operator +(HFunction1D function, double add)
		{
			return function.ScaleYFunct1d(1.0, add);
		}

		public static HFunction1D operator +(double add, HFunction1D function)
		{
			return function.ScaleYFunct1d(1.0, add);
		}

		public static HFunction1D operator -(HFunction1D function, double sub)
		{
			return function.ScaleYFunct1d(1.0, 0.0 - sub);
		}

		public static HFunction1D operator -(HFunction1D function)
		{
			return function.NegateFunct1d();
		}

		public static HFunction1D operator *(HFunction1D function, double factor)
		{
			return function.ScaleYFunct1d(factor, 0.0);
		}

		public static HFunction1D operator *(double factor, HFunction1D function)
		{
			return function.ScaleYFunct1d(factor, 0.0);
		}

		public static HFunction1D operator /(HFunction1D function, double divisor)
		{
			return function.ScaleYFunct1d(1.0 / divisor, 0.0);
		}

		public static HFunction1D operator *(HFunction1D function1, HFunction1D function2)
		{
			return function1.ComposeFunct1d(function2, "constant");
		}

		public static HFunction1D operator !(HFunction1D function)
		{
			return function.InvertFunct1d();
		}

		public void GnuplotPlotFunct1d(HGnuplot gnuplotFileID)
		{
			IntPtr proc = HalconAPI.PreCall(1295);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, gnuplotFileID);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(gnuplotFileID);
		}

		public HFunction1D ComposeFunct1d(HFunction1D function2, string border)
		{
			IntPtr proc = HalconAPI.PreCall(1377);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, function2);
			HalconAPI.StoreS(proc, 2, border);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(function2);
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HFunction1D InvertFunct1d()
		{
			IntPtr proc = HalconAPI.PreCall(1378);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HFunction1D DerivateFunct1d(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1379);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void LocalMinMaxFunct1d(string mode, string interpolation, out HTuple min, out HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(1380);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, mode);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out min);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out max);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple ZeroCrossingsFunct1d()
		{
			IntPtr proc = HalconAPI.PreCall(1381);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HFunction1D ScaleYFunct1d(double mult, double add)
		{
			IntPtr proc = HalconAPI.PreCall(1382);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, mult);
			HalconAPI.StoreD(proc, 2, add);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HFunction1D NegateFunct1d()
		{
			IntPtr proc = HalconAPI.PreCall(1383);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HFunction1D AbsFunct1d()
		{
			IntPtr proc = HalconAPI.PreCall(1384);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetYValueFunct1d(HTuple x, string border)
		{
			IntPtr proc = HalconAPI.PreCall(1385);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, x);
			HalconAPI.StoreS(proc, 2, border);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(x);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double GetYValueFunct1d(double x, string border)
		{
			IntPtr proc = HalconAPI.PreCall(1385);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, x);
			HalconAPI.StoreS(proc, 2, border);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public void GetPairFunct1d(HTuple index, out HTuple x, out HTuple y)
		{
			IntPtr proc = HalconAPI.PreCall(1386);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(index);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out x);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out y);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetPairFunct1d(int index, out double x, out double y)
		{
			IntPtr proc = HalconAPI.PreCall(1386);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, index);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HalconAPI.LoadD(proc, 0, err, out x);
			err = HalconAPI.LoadD(proc, 1, err, out y);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public int NumPointsFunct1d()
		{
			IntPtr proc = HalconAPI.PreCall(1387);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public void YRangeFunct1d(out double YMin, out double YMax)
		{
			IntPtr proc = HalconAPI.PreCall(1388);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HalconAPI.LoadD(proc, 0, err, out YMin);
			err = HalconAPI.LoadD(proc, 1, err, out YMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void XRangeFunct1d(out double XMin, out double XMax)
		{
			IntPtr proc = HalconAPI.PreCall(1389);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HalconAPI.LoadD(proc, 0, err, out XMin);
			err = HalconAPI.LoadD(proc, 1, err, out XMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void Funct1dToPairs(out HTuple XValues, out HTuple YValues)
		{
			IntPtr proc = HalconAPI.PreCall(1390);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HTuple.LoadNew(proc, 0, err, out XValues);
			err = HTuple.LoadNew(proc, 1, err, out YValues);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HFunction1D SampleFunct1d(HTuple XMin, HTuple XMax, HTuple XDist, string border)
		{
			IntPtr proc = HalconAPI.PreCall(1391);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, XMin);
			HalconAPI.Store(proc, 2, XMax);
			HalconAPI.Store(proc, 3, XDist);
			HalconAPI.StoreS(proc, 4, border);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(XMin);
			HalconAPI.UnpinTuple(XMax);
			HalconAPI.UnpinTuple(XDist);
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HFunction1D SampleFunct1d(double XMin, double XMax, double XDist, string border)
		{
			IntPtr proc = HalconAPI.PreCall(1391);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, XMin);
			HalconAPI.StoreD(proc, 2, XMax);
			HalconAPI.StoreD(proc, 3, XDist);
			HalconAPI.StoreS(proc, 4, border);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HFunction1D TransformFunct1d(HTuple paramsVal)
		{
			IntPtr proc = HalconAPI.PreCall(1392);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, paramsVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(paramsVal);
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple MatchFunct1dTrans(HFunction1D function2, string border, HTuple paramsConst, HTuple useParams, out double chiSquare, out HTuple covar)
		{
			IntPtr proc = HalconAPI.PreCall(1393);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, function2);
			HalconAPI.StoreS(proc, 2, border);
			HalconAPI.Store(proc, 3, paramsConst);
			HalconAPI.Store(proc, 4, useParams);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(function2);
			HalconAPI.UnpinTuple(paramsConst);
			HalconAPI.UnpinTuple(useParams);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HalconAPI.LoadD(proc, 1, err, out chiSquare);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out covar);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple DistanceFunct1d(HFunction1D function2, HTuple mode, HTuple sigma)
		{
			IntPtr proc = HalconAPI.PreCall(1394);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, function2);
			HalconAPI.Store(proc, 2, mode);
			HalconAPI.Store(proc, 3, sigma);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(function2);
			HalconAPI.UnpinTuple(mode);
			HalconAPI.UnpinTuple(sigma);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple DistanceFunct1d(HFunction1D function2, string mode, double sigma)
		{
			IntPtr proc = HalconAPI.PreCall(1394);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, function2);
			HalconAPI.StoreS(proc, 2, mode);
			HalconAPI.StoreD(proc, 3, sigma);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.UnpinTuple(function2);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HFunction1D SmoothFunct1dGauss(double sigma)
		{
			IntPtr proc = HalconAPI.PreCall(1395);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, sigma);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public double IntegrateFunct1d(out HTuple negative)
		{
			IntPtr proc = HalconAPI.PreCall(1396);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out negative);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public void ReadFunct1d(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1397);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteFunct1d(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1398);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateFunct1dArray(HTuple YValues)
		{
			IntPtr proc = HalconAPI.PreCall(1399);
			HalconAPI.Store(proc, 0, YValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(YValues);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateFunct1dArray(double YValues)
		{
			IntPtr proc = HalconAPI.PreCall(1399);
			HalconAPI.StoreD(proc, 0, YValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateFunct1dPairs(HTuple XValues, HTuple YValues)
		{
			IntPtr proc = HalconAPI.PreCall(1400);
			HalconAPI.Store(proc, 0, XValues);
			HalconAPI.Store(proc, 1, YValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(XValues);
			HalconAPI.UnpinTuple(YValues);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateFunct1dPairs(double XValues, double YValues)
		{
			IntPtr proc = HalconAPI.PreCall(1400);
			HalconAPI.StoreD(proc, 0, XValues);
			HalconAPI.StoreD(proc, 1, YValues);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HFunction1D SmoothFunct1dMean(int smoothSize, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(1401);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, smoothSize);
			HalconAPI.StoreI(proc, 2, iterations);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			UnpinTuple();
			err = LoadNew(proc, 0, err, out HFunction1D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}
	}
}
