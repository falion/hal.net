using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HXLD : HObject, ISerializable, ICloneable
	{
		public new HXLD this[HTuple index] => SelectObj(index);

		public HXLD()
			: base(HObjectBase.UNDEF, copy: false)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLD(IntPtr key)
			: this(key, copy: true)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLD(IntPtr key, bool copy)
			: base(key, copy)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLD(HObject obj)
			: base(obj)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		private void AssertObjectClass()
		{
			HalconAPI.AssertObjectClass(key, "xld");
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, int err, out HXLD obj)
		{
			obj = new HXLD(HObjectBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeXld();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLD(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeXld(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeXld();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HXLD Deserialize(Stream stream)
		{
			HXLD hXLD = new HXLD();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hXLD.DeserializeXld(hSerializedItem);
			hSerializedItem.Dispose();
			return hXLD;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HXLD Clone()
		{
			HSerializedItem hSerializedItem = SerializeXld();
			HXLD hXLD = new HXLD();
			hXLD.DeserializeXld(hSerializedItem);
			hSerializedItem.Dispose();
			return hXLD;
		}

		public void GetParallelsXld(out HTuple row1, out HTuple col1, out HTuple length1, out HTuple phi1, out HTuple row2, out HTuple col2, out HTuple length2, out HTuple phi2)
		{
			IntPtr proc = HalconAPI.PreCall(41);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row1);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out col1);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out length1);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out phi1);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out row2);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out col2);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out length2);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out phi2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DispXld(HWindow windowHandle)
		{
			IntPtr proc = HalconAPI.PreCall(74);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void ReceiveXld(HSocket socket)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(329);
			HalconAPI.Store(proc, 0, socket);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(socket);
		}

		public void SendXld(HSocket socket)
		{
			IntPtr proc = HalconAPI.PreCall(330);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, socket);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(socket);
		}

		public HXLD ObjDiff(HXLD objectsSub)
		{
			IntPtr proc = HalconAPI.PreCall(573);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsSub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsSub);
			return obj;
		}

		public HImage PaintXld(HImage image, HTuple grayval)
		{
			IntPtr proc = HalconAPI.PreCall(575);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, grayval);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(grayval);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HImage PaintXld(HImage image, double grayval)
		{
			IntPtr proc = HalconAPI.PreCall(575);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreD(proc, 0, grayval);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public new HXLD CopyObj(int index, int numObj)
		{
			IntPtr proc = HalconAPI.PreCall(583);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.StoreI(proc, 1, numObj);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLD ConcatObj(HXLD objects2)
		{
			IntPtr proc = HalconAPI.PreCall(584);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return obj;
		}

		public new HXLD SelectObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLD SelectObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public int CompareObj(HXLD objects2, HTuple epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.Store(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(epsilon);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public int CompareObj(HXLD objects2, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.StoreD(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public int TestEqualObj(HXLD objects2)
		{
			IntPtr proc = HalconAPI.PreCall(591);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLD meshes, int gridSpacing, HTuple rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.Store(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLD meshes, int gridSpacing, string rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.StoreS(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void DeserializeXld(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1632);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeXld()
		{
			IntPtr proc = HalconAPI.PreCall(1633);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple TestClosedXld()
		{
			IntPtr proc = HalconAPI.PreCall(1667);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple MomentsAnyPointsXld(string mode, HTuple area, HTuple centerRow, HTuple centerCol, HTuple p, HTuple q)
		{
			IntPtr proc = HalconAPI.PreCall(1669);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, area);
			HalconAPI.Store(proc, 2, centerRow);
			HalconAPI.Store(proc, 3, centerCol);
			HalconAPI.Store(proc, 4, p);
			HalconAPI.Store(proc, 5, q);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(area);
			HalconAPI.UnpinTuple(centerRow);
			HalconAPI.UnpinTuple(centerCol);
			HalconAPI.UnpinTuple(p);
			HalconAPI.UnpinTuple(q);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsAnyPointsXld(string mode, double area, double centerRow, double centerCol, int p, int q)
		{
			IntPtr proc = HalconAPI.PreCall(1669);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, area);
			HalconAPI.StoreD(proc, 2, centerRow);
			HalconAPI.StoreD(proc, 3, centerCol);
			HalconAPI.StoreI(proc, 4, p);
			HalconAPI.StoreI(proc, 5, q);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple EccentricityPointsXld()
		{
			IntPtr proc = HalconAPI.PreCall(1670);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple EllipticAxisPointsXld(out HTuple rb, out HTuple phi)
		{
			IntPtr proc = HalconAPI.PreCall(1671);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out rb);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double EllipticAxisPointsXld(out double rb, out double phi)
		{
			IntPtr proc = HalconAPI.PreCall(1671);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out rb);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple OrientationPointsXld()
		{
			IntPtr proc = HalconAPI.PreCall(1672);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple MomentsPointsXld(out HTuple m20, out HTuple m02)
		{
			IntPtr proc = HalconAPI.PreCall(1673);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out m20);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out m02);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsPointsXld(out double m20, out double m02)
		{
			IntPtr proc = HalconAPI.PreCall(1673);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out m20);
			err = HalconAPI.LoadD(proc, 2, err, out m02);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple AreaCenterPointsXld(out HTuple row, out HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1674);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double AreaCenterPointsXld(out double row, out double column)
		{
			IntPtr proc = HalconAPI.PreCall(1674);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out row);
			err = HalconAPI.LoadD(proc, 2, err, out column);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple TestSelfIntersectionXld(string closeXLD)
		{
			IntPtr proc = HalconAPI.PreCall(1675);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, closeXLD);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HXLD SelectXldPoint(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLD SelectXldPoint(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple TestXldPoint(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1677);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int TestXldPoint(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(1677);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HXLD SelectShapeXld(HTuple features, string operation, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.Store(proc, 2, min);
			HalconAPI.Store(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLD SelectShapeXld(string features, string operation, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.StoreD(proc, 2, min);
			HalconAPI.StoreD(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple OrientationXld()
		{
			IntPtr proc = HalconAPI.PreCall(1679);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple EccentricityXld(out HTuple bulkiness, out HTuple structureFactor)
		{
			IntPtr proc = HalconAPI.PreCall(1680);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out bulkiness);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out structureFactor);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double EccentricityXld(out double bulkiness, out double structureFactor)
		{
			IntPtr proc = HalconAPI.PreCall(1680);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out bulkiness);
			err = HalconAPI.LoadD(proc, 2, err, out structureFactor);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple CompactnessXld()
		{
			IntPtr proc = HalconAPI.PreCall(1681);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void DiameterXld(out HTuple row1, out HTuple column1, out HTuple row2, out HTuple column2, out HTuple diameter)
		{
			IntPtr proc = HalconAPI.PreCall(1682);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row1);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column1);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out row2);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out column2);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out diameter);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DiameterXld(out double row1, out double column1, out double row2, out double column2, out double diameter)
		{
			IntPtr proc = HalconAPI.PreCall(1682);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row1);
			err = HalconAPI.LoadD(proc, 1, err, out column1);
			err = HalconAPI.LoadD(proc, 2, err, out row2);
			err = HalconAPI.LoadD(proc, 3, err, out column2);
			err = HalconAPI.LoadD(proc, 4, err, out diameter);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple ConvexityXld()
		{
			IntPtr proc = HalconAPI.PreCall(1683);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple CircularityXld()
		{
			IntPtr proc = HalconAPI.PreCall(1684);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple EllipticAxisXld(out HTuple rb, out HTuple phi)
		{
			IntPtr proc = HalconAPI.PreCall(1685);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out rb);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double EllipticAxisXld(out double rb, out double phi)
		{
			IntPtr proc = HalconAPI.PreCall(1685);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out rb);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public void SmallestRectangle2Xld(out HTuple row, out HTuple column, out HTuple phi, out HTuple length1, out HTuple length2)
		{
			IntPtr proc = HalconAPI.PreCall(1686);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out length1);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out length2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SmallestRectangle2Xld(out double row, out double column, out double phi, out double length1, out double length2)
		{
			IntPtr proc = HalconAPI.PreCall(1686);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out length1);
			err = HalconAPI.LoadD(proc, 4, err, out length2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SmallestRectangle1Xld(out HTuple row1, out HTuple column1, out HTuple row2, out HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(1687);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row1);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column1);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out row2);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SmallestRectangle1Xld(out double row1, out double column1, out double row2, out double column2)
		{
			IntPtr proc = HalconAPI.PreCall(1687);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row1);
			err = HalconAPI.LoadD(proc, 1, err, out column1);
			err = HalconAPI.LoadD(proc, 2, err, out row2);
			err = HalconAPI.LoadD(proc, 3, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SmallestCircleXld(out HTuple row, out HTuple column, out HTuple radius)
		{
			IntPtr proc = HalconAPI.PreCall(1688);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out radius);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SmallestCircleXld(out double row, out double column, out double radius)
		{
			IntPtr proc = HalconAPI.PreCall(1688);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out radius);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLD ShapeTransXld(string type)
		{
			IntPtr proc = HalconAPI.PreCall(1689);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple LengthXld()
		{
			IntPtr proc = HalconAPI.PreCall(1690);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple MomentsAnyXld(string mode, HTuple pointOrder, HTuple area, HTuple centerRow, HTuple centerCol, HTuple p, HTuple q)
		{
			IntPtr proc = HalconAPI.PreCall(1691);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, pointOrder);
			HalconAPI.Store(proc, 2, area);
			HalconAPI.Store(proc, 3, centerRow);
			HalconAPI.Store(proc, 4, centerCol);
			HalconAPI.Store(proc, 5, p);
			HalconAPI.Store(proc, 6, q);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(pointOrder);
			HalconAPI.UnpinTuple(area);
			HalconAPI.UnpinTuple(centerRow);
			HalconAPI.UnpinTuple(centerCol);
			HalconAPI.UnpinTuple(p);
			HalconAPI.UnpinTuple(q);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsAnyXld(string mode, string pointOrder, double area, double centerRow, double centerCol, int p, int q)
		{
			IntPtr proc = HalconAPI.PreCall(1691);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreS(proc, 1, pointOrder);
			HalconAPI.StoreD(proc, 2, area);
			HalconAPI.StoreD(proc, 3, centerRow);
			HalconAPI.StoreD(proc, 4, centerCol);
			HalconAPI.StoreI(proc, 5, p);
			HalconAPI.StoreI(proc, 6, q);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple MomentsXld(out HTuple m20, out HTuple m02)
		{
			IntPtr proc = HalconAPI.PreCall(1692);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out m20);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out m02);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double MomentsXld(out double m20, out double m02)
		{
			IntPtr proc = HalconAPI.PreCall(1692);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out m20);
			err = HalconAPI.LoadD(proc, 2, err, out m02);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple AreaCenterXld(out HTuple row, out HTuple column, out HTuple pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(1693);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 3, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double AreaCenterXld(out double row, out double column, out string pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(1693);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out row);
			err = HalconAPI.LoadD(proc, 2, err, out column);
			err = HalconAPI.LoadS(proc, 3, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HPose[] GetRectanglePose(HCamPar cameraParam, HTuple width, HTuple height, string weightingMode, double clippingFactor, out HTuple covPose, out HTuple error)
		{
			IntPtr proc = HalconAPI.PreCall(1908);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 1, width);
			HalconAPI.Store(proc, 2, height);
			HalconAPI.StoreS(proc, 3, weightingMode);
			HalconAPI.StoreD(proc, 4, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(width);
			HalconAPI.UnpinTuple(height);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			HPose[] result = HPose.SplitArray(tuple);
			GC.KeepAlive(this);
			return result;
		}

		public HPose GetRectanglePose(HCamPar cameraParam, double width, double height, string weightingMode, double clippingFactor, out HTuple covPose, out HTuple error)
		{
			IntPtr proc = HalconAPI.PreCall(1908);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.StoreD(proc, 1, width);
			HalconAPI.StoreD(proc, 2, height);
			HalconAPI.StoreS(proc, 3, weightingMode);
			HalconAPI.StoreD(proc, 4, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out covPose);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out error);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetCirclePose(HCamPar cameraParam, HTuple radius, string outputType, out HTuple pose2)
		{
			IntPtr proc = HalconAPI.PreCall(1909);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 1, radius);
			HalconAPI.StoreS(proc, 2, outputType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(radius);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out pose2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetCirclePose(HCamPar cameraParam, double radius, string outputType, out HTuple pose2)
		{
			IntPtr proc = HalconAPI.PreCall(1909);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.StoreD(proc, 1, radius);
			HalconAPI.StoreS(proc, 2, outputType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out pose2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple HeightWidthRatioXld(out HTuple width, out HTuple ratio)
		{
			IntPtr proc = HalconAPI.PreCall(2120);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out width);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out ratio);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double HeightWidthRatioXld(out double width, out double ratio)
		{
			IntPtr proc = HalconAPI.PreCall(2120);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out width);
			err = HalconAPI.LoadD(proc, 2, err, out ratio);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HXLD InsertObj(HXLD objectsInsert, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2121);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsInsert);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsInsert);
			return obj;
		}

		public new HXLD RemoveObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLD RemoveObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLD ReplaceObj(HXLD objectsReplace, HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return obj;
		}

		public HXLD ReplaceObj(HXLD objectsReplace, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLD obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return obj;
		}
	}
}
