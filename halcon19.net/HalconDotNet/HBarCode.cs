using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HBarCode : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBarCode()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBarCode(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBarCode(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("barcode");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HBarCode obj)
		{
			obj = new HBarCode(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HBarCode[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HBarCode[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HBarCode(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HBarCode(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1988);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HBarCode(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2001);
			HalconAPI.Store(proc, 0, genParamName);
			HalconAPI.Store(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HBarCode(string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2001);
			HalconAPI.StoreS(proc, 0, genParamName);
			HalconAPI.StoreD(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeBarCodeModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBarCode(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeBarCodeModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeBarCodeModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HBarCode Deserialize(Stream stream)
		{
			HBarCode hBarCode = new HBarCode();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hBarCode.DeserializeBarCodeModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hBarCode;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HBarCode Clone()
		{
			HSerializedItem hSerializedItem = SerializeBarCodeModel();
			HBarCode hBarCode = new HBarCode();
			hBarCode.DeserializeBarCodeModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hBarCode;
		}

		public void DeserializeBarCodeModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1986);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeBarCodeModel()
		{
			IntPtr proc = HalconAPI.PreCall(1987);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadBarCodeModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1988);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteBarCodeModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1989);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HObject GetBarCodeObject(HTuple candidateHandle, string objectName)
		{
			IntPtr proc = HalconAPI.PreCall(1990);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, candidateHandle);
			HalconAPI.StoreS(proc, 2, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(candidateHandle);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HObject GetBarCodeObject(string candidateHandle, string objectName)
		{
			IntPtr proc = HalconAPI.PreCall(1990);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, candidateHandle);
			HalconAPI.StoreS(proc, 2, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetBarCodeResult(HTuple candidateHandle, string resultName)
		{
			IntPtr proc = HalconAPI.PreCall(1991);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, candidateHandle);
			HalconAPI.StoreS(proc, 2, resultName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(candidateHandle);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetBarCodeResult(string candidateHandle, string resultName)
		{
			IntPtr proc = HalconAPI.PreCall(1991);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, candidateHandle);
			HalconAPI.StoreS(proc, 2, resultName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple DecodeBarCodeRectangle2(HImage image, HTuple codeType, HTuple row, HTuple column, HTuple phi, HTuple length1, HTuple length2)
		{
			IntPtr proc = HalconAPI.PreCall(1992);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, codeType);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.Store(proc, 4, phi);
			HalconAPI.Store(proc, 5, length1);
			HalconAPI.Store(proc, 6, length2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(codeType);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(length1);
			HalconAPI.UnpinTuple(length2);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DecodeBarCodeRectangle2(HImage image, string codeType, double row, double column, double phi, double length1, double length2)
		{
			IntPtr proc = HalconAPI.PreCall(1992);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreS(proc, 1, codeType);
			HalconAPI.StoreD(proc, 2, row);
			HalconAPI.StoreD(proc, 3, column);
			HalconAPI.StoreD(proc, 4, phi);
			HalconAPI.StoreD(proc, 5, length1);
			HalconAPI.StoreD(proc, 6, length2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return stringValue;
		}

		public HRegion FindBarCode(HImage image, HTuple codeType, out HTuple decodedDataStrings)
		{
			IntPtr proc = HalconAPI.PreCall(1993);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, codeType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(codeType);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			err = HTuple.LoadNew(proc, 0, err, out decodedDataStrings);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HRegion FindBarCode(HImage image, string codeType, out string decodedDataStrings)
		{
			IntPtr proc = HalconAPI.PreCall(1993);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreS(proc, 1, codeType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			err = HalconAPI.LoadS(proc, 0, err, out decodedDataStrings);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HTuple QueryBarCodeParams(string properties)
		{
			IntPtr proc = HalconAPI.PreCall(1994);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, properties);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetBarCodeParamSpecific(HTuple codeTypes, HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1995);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, codeTypes);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(codeTypes);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetBarCodeParamSpecific(string codeTypes, string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1995);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, codeTypes);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetBarCodeParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1996);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetBarCodeParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1996);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetBarCodeParamSpecific(HTuple codeTypes, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1997);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, codeTypes);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(codeTypes);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetBarCodeParamSpecific(string codeTypes, string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1997);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, codeTypes);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetBarCodeParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1998);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetBarCodeParam(string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1998);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void ClearBarCodeModel(HBarCode[] barCodeHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(barCodeHandle);
			IntPtr proc = HalconAPI.PreCall(2000);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(barCodeHandle);
		}

		public void ClearBarCodeModel()
		{
			IntPtr proc = HalconAPI.PreCall(2000);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateBarCodeModel(HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2001);
			HalconAPI.Store(proc, 0, genParamName);
			HalconAPI.Store(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateBarCodeModel(string genParamName, double genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2001);
			HalconAPI.StoreS(proc, 0, genParamName);
			HalconAPI.StoreD(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
