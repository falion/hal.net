using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HClassTrainData : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassTrainData()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassTrainData(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassTrainData(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("class_train_data");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassTrainData obj)
		{
			obj = new HClassTrainData(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassTrainData[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HClassTrainData[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HClassTrainData(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HClassTrainData(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1781);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassTrainData(int numDim)
		{
			IntPtr proc = HalconAPI.PreCall(1798);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeClassTrainData();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassTrainData(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeClassTrainData(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeClassTrainData();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HClassTrainData Deserialize(Stream stream)
		{
			HClassTrainData hClassTrainData = new HClassTrainData();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hClassTrainData.DeserializeClassTrainData(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassTrainData;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HClassTrainData Clone()
		{
			HSerializedItem hSerializedItem = SerializeClassTrainData();
			HClassTrainData hClassTrainData = new HClassTrainData();
			hClassTrainData.DeserializeClassTrainData(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassTrainData;
		}

		public void DeserializeClassTrainData(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1779);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeClassTrainData()
		{
			IntPtr proc = HalconAPI.PreCall(1780);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadClassTrainData(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1781);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteClassTrainData(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1782);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HClassTrainData SelectSubFeatureClassTrainData(HTuple subFeatureIndices)
		{
			IntPtr proc = HalconAPI.PreCall(1783);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, subFeatureIndices);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subFeatureIndices);
			err = LoadNew(proc, 0, err, out HClassTrainData obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SetFeatureLengthsClassTrainData(HTuple subFeatureLength, HTuple names)
		{
			IntPtr proc = HalconAPI.PreCall(1784);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, subFeatureLength);
			HalconAPI.Store(proc, 2, names);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(subFeatureLength);
			HalconAPI.UnpinTuple(names);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GetClassTrainDataGmm(HClassGmm GMMHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1785);
			HalconAPI.Store(proc, 0, GMMHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(GMMHandle);
		}

		public void AddClassTrainDataGmm(HClassGmm GMMHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1786);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, GMMHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(GMMHandle);
		}

		public void GetClassTrainDataMlp(HClassMlp MLPHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1787);
			HalconAPI.Store(proc, 0, MLPHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(MLPHandle);
		}

		public void AddClassTrainDataMlp(HClassMlp MLPHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1788);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, MLPHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(MLPHandle);
		}

		public void GetClassTrainDataKnn(HClassKnn KNNHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1789);
			HalconAPI.Store(proc, 0, KNNHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(KNNHandle);
		}

		public void AddClassTrainDataKnn(HClassKnn KNNHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1790);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, KNNHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(KNNHandle);
		}

		public void GetClassTrainDataSvm(HClassSvm SVMHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1791);
			HalconAPI.Store(proc, 0, SVMHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(SVMHandle);
		}

		public void AddClassTrainDataSvm(HClassSvm SVMHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1792);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, SVMHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(SVMHandle);
		}

		public int GetSampleNumClassTrainData()
		{
			IntPtr proc = HalconAPI.PreCall(1793);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple GetSampleClassTrainData(int indexSample, out int classID)
		{
			IntPtr proc = HalconAPI.PreCall(1794);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, indexSample);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HalconAPI.LoadI(proc, 1, err, out classID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void ClearClassTrainData()
		{
			IntPtr proc = HalconAPI.PreCall(1796);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AddSampleClassTrainData(string order, HTuple features, HTuple classID)
		{
			IntPtr proc = HalconAPI.PreCall(1797);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, order);
			HalconAPI.Store(proc, 2, features);
			HalconAPI.Store(proc, 3, classID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(classID);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateClassTrainData(int numDim)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1798);
			HalconAPI.StoreI(proc, 0, numDim);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassMlp SelectFeatureSetMlp(string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1799);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HClassMlp.LoadNew(proc, 0, err, out HClassMlp obj);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HClassMlp SelectFeatureSetMlp(string selectionMethod, string genParamName, double genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1799);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HClassMlp.LoadNew(proc, 0, err, out HClassMlp obj);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HClassSvm SelectFeatureSetSvm(string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1800);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HClassSvm.LoadNew(proc, 0, err, out HClassSvm obj);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HClassSvm SelectFeatureSetSvm(string selectionMethod, string genParamName, double genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1800);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HClassSvm.LoadNew(proc, 0, err, out HClassSvm obj);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HClassGmm SelectFeatureSetGmm(string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1801);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HClassGmm.LoadNew(proc, 0, err, out HClassGmm obj);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HClassGmm SelectFeatureSetGmm(string selectionMethod, string genParamName, double genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1801);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HClassGmm.LoadNew(proc, 0, err, out HClassGmm obj);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HClassKnn SelectFeatureSetKnn(string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1802);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HClassKnn.LoadNew(proc, 0, err, out HClassKnn obj);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HClassKnn SelectFeatureSetKnn(string selectionMethod, string genParamName, double genParamValue, out HTuple selectedFeatureIndices, out HTuple score)
		{
			IntPtr proc = HalconAPI.PreCall(1802);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HClassKnn.LoadNew(proc, 0, err, out HClassKnn obj);
			err = HTuple.LoadNew(proc, 1, err, out selectedFeatureIndices);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}
	}
}
