using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HObjectBase : IDisposable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static readonly IntPtr UNDEF = IntPtr.Zero;

		internal static readonly IntPtr UNDEF2 = new IntPtr(1);

		internal IntPtr key = UNDEF;

		private bool suppressedFinalization;

		[EditorBrowsable(EditorBrowsableState.Never)]
		public IntPtr Key => key;

		internal HObjectBase()
			: this(UNDEF, copy: false)
		{
		}

		internal HObjectBase(IntPtr key, bool copy)
		{
			if (copy && key != UNDEF && key != UNDEF2)
			{
				this.key = HalconAPI.CopyObject(key);
			}
			else
			{
				this.key = ((key == UNDEF2) ? UNDEF : key);
			}
		}

		internal HObjectBase(HObjectBase obj)
			: this(obj.key, copy: true)
		{
			GC.KeepAlive(obj);
		}

		public bool IsInitialized()
		{
			return key != UNDEF;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public IntPtr CopyKey()
		{
			IntPtr result = HalconAPI.CopyObject(key);
			GC.KeepAlive(this);
			return result;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void TransferOwnership(HObjectBase source)
		{
			if (source != this)
			{
				Dispose();
				if (source != null)
				{
					key = source.key;
					source.key = UNDEF;
					suppressedFinalization = false;
					GC.ReRegisterForFinalize(this);
				}
			}
		}

		~HObjectBase()
		{
			try
			{
				Dispose(disposing: false);
			}
			catch (Exception)
			{
			}
		}

		private void Dispose(bool disposing)
		{
			if (key != UNDEF)
			{
				HalconAPI.ClearObject(key);
				key = UNDEF;
			}
			if (disposing)
			{
				GC.SuppressFinalize(this);
				suppressedFinalization = true;
			}
			GC.KeepAlive(this);
		}

		void IDisposable.Dispose()
		{
			Dispose(disposing: true);
		}

		public virtual void Dispose()
		{
			Dispose(disposing: true);
		}

		internal void Store(IntPtr proc, int parIndex)
		{
			HalconAPI.HCkP(proc, HalconAPI.SetInputObject(proc, parIndex, key));
		}

		internal int Load(IntPtr proc, int parIndex, int err)
		{
			if (key != UNDEF)
			{
				throw new HalconException("Undisposed object instance when loading output parameter");
			}
			if (HalconAPI.IsFailure(err))
			{
				return err;
			}
			err = HalconAPI.GetOutputObject(proc, parIndex, out key);
			if (suppressedFinalization)
			{
				suppressedFinalization = false;
				GC.ReRegisterForFinalize(this);
			}
			return err;
		}
	}
}
