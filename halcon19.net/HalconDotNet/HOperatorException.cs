using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HOperatorException : HalconException
	{
		public HOperatorException(int err, string sInfo, Exception inner)
			: base(err, (sInfo == "") ? HalconAPI.GetErrorMessage(err) : sInfo, inner)
		{
		}

		public HOperatorException(int err, string sInfo)
			: this(err, sInfo, null)
		{
		}

		public HOperatorException(int err)
			: this(err, "")
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("GetErrorText is deprecated, please use GetErrorMessage instead.")]
		public new string GetErrorText()
		{
			return HalconAPI.GetErrorMessage(GetErrorCode());
		}

		public new string GetErrorMessage()
		{
			return HalconAPI.GetErrorMessage(GetErrorCode());
		}

		public long GetExtendedErrorCode()
		{
			HOperatorSet.GetExtendedErrorInfo(out HTuple _, out HTuple errorCode, out HTuple _);
			if (errorCode.Length > 0)
			{
				return errorCode[0].L;
			}
			return 0L;
		}

		public string GetExtendedErrorMessage()
		{
			HOperatorSet.GetExtendedErrorInfo(out HTuple _, out HTuple _, out HTuple errorMessage);
			if (errorMessage.Length > 0)
			{
				return errorMessage[0];
			}
			return "";
		}

		public static void throwOperator(int err, string logicalName)
		{
			if (HalconAPI.IsFailure(err))
			{
				throw new HOperatorException(err, HalconAPI.GetErrorMessage(err) + " in operator " + logicalName);
			}
		}

		internal static void throwOperator(int err, int procIndex)
		{
			if (HalconAPI.IsFailure(err))
			{
				string logicalName = HalconAPI.GetLogicalName(procIndex);
				throw new HOperatorException(err, HalconAPI.GetErrorMessage(err) + " in operator " + logicalName);
			}
		}

		public static void throwInfo(int err, string sInfo)
		{
			throw new HOperatorException(err, sInfo + ":\n" + HalconAPI.GetErrorMessage(err) + "\n");
		}
	}
}
