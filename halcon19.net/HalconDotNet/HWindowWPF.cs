using System;
using System.Runtime.InteropServices;
using System.Windows.Input;
using System.Windows.Interop;

namespace HalconDotNet
{
	internal class HWindowWPF : HwndHost
	{
		internal const int WS_CHILD = 1073741824;

		internal const int WS_VISIBLE = 268435456;

		internal const int LBS_NOTIFY = 1;

		internal const int HOST_ID = 2;

		internal const int LISTBOX_ID = 1;

		internal const int WS_VSCROLL = 2097152;

		internal const int WS_BORDER = 8388608;

		private HWindowControlWPF parent;

		private HWindow window = new HWindow();

		private IntPtr hwndHalcon = IntPtr.Zero;

		private int width = 1;

		private int height = 1;

		private int lastMoveX;

		private int lastMoveY;

		private bool delayedInit;

		public HWindow HalconWindow => window;

		public event HWInitEventHandler HWInitEvent;

		public event HWButtonEventHandler HWButtonEvent;

		public event HWMouseEventHandler HWMouseEvent;

		public HWindowWPF(HWindowControlWPF parent)
		{
			this.parent = parent;
		}

		protected override HandleRef BuildWindowCore(HandleRef hwndParent)
		{
			width = (int)parent.Container.ActualWidth;
			height = (int)parent.Container.ActualHeight;
			if (width <= 0 || double.IsNaN(width))
			{
				delayedInit = true;
				width = 1;
			}
			if (height <= 0 || double.IsNaN(height))
			{
				delayedInit = true;
				height = 1;
			}
			base.Width = width;
			base.Height = height;
			IntPtr intPtr = CreateWindowEx(0, "static", "", 1342177280, 0, 0, width, height, hwndParent.Handle, (IntPtr)2, IntPtr.Zero, 0);
			try
			{
				HSystem.SetCheck("~father");
				window.OpenWindow(0, 0, width, height, intPtr, "visible", "");
				hwndHalcon = window.GetOsWindowHandle(out IntPtr _);
			}
			catch (HOperatorException ex)
			{
				int errorCode = ex.GetErrorCode();
				if (errorCode >= 5100 && errorCode < 5200)
				{
					throw ex;
				}
			}
			if (!delayedInit)
			{
				this.HWInitEvent();
			}
			return new HandleRef(this, intPtr);
		}

		protected override void DestroyWindowCore(HandleRef hwnd)
		{
			if (window.IsInitialized())
			{
				window.Dispose();
			}
			hwndHalcon = IntPtr.Zero;
			IntPtr handle = hwnd.Handle;
			if (handle != IntPtr.Zero)
			{
				DestroyWindow(handle);
			}
		}

		public void SetWindowExtents(int width, int height)
		{
			if (window.IsInitialized())
			{
				bool flag = true;
				if (width <= 0 || double.IsNaN(width))
				{
					flag = false;
					width = 1;
				}
				if (height <= 0 || double.IsNaN(height))
				{
					flag = false;
					height = 1;
				}
				if (this.width != width || this.height != height)
				{
					this.width = width;
					this.height = height;
					base.Width = width;
					base.Height = height;
					window.SetWindowExtents(0, 0, width, height);
				}
				if (delayedInit && flag)
				{
					delayedInit = false;
					this.HWInitEvent();
				}
			}
		}

		[DllImport("user32.dll")]
		private static extern IntPtr SetFocus(IntPtr hWnd);

		public void SetNativeFocus()
		{
			if (window.IsInitialized() && hwndHalcon != IntPtr.Zero)
			{
				SetFocus(hwndHalcon);
			}
		}

		protected override IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam, ref bool handled)
		{
			IntPtr result = base.WndProc(hwnd, msg, wparam, lparam, ref handled);
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			if (HalconAPI.isPlatform64)
			{
				if (msg == 522)
				{
					long num4 = wparam.ToInt64() & uint.MaxValue;
					if (num4 > int.MaxValue)
					{
						num4 |= -4294967296L;
					}
					num = (int)(num4 >> 16);
				}
				else
				{
					num = wparam.ToInt32();
				}
				num2 = (int)(lparam.ToInt64() & 0xFFFF);
				num3 = (int)(lparam.ToInt64() >> 16);
			}
			else
			{
				num = wparam.ToInt32() >> 16;
				num2 = (lparam.ToInt32() & 0xFFFF);
				num3 = lparam.ToInt32() >> 16;
			}
			switch (msg)
			{
			case 513:
				this.HWButtonEvent(num2, num3, MouseButton.Left, MouseButtonState.Pressed);
				break;
			case 519:
				this.HWButtonEvent(num2, num3, MouseButton.Middle, MouseButtonState.Pressed);
				break;
			case 516:
				this.HWButtonEvent(num2, num3, MouseButton.Right, MouseButtonState.Pressed);
				break;
			case 523:
				this.HWButtonEvent(num2, num3, ((num & 0x20) == 32) ? MouseButton.XButton1 : MouseButton.XButton2, MouseButtonState.Pressed);
				break;
			case 514:
				this.HWButtonEvent(num2, num3, MouseButton.Left, MouseButtonState.Released);
				break;
			case 520:
				this.HWButtonEvent(num2, num3, MouseButton.Middle, MouseButtonState.Released);
				break;
			case 517:
				this.HWButtonEvent(num2, num3, MouseButton.Right, MouseButtonState.Released);
				break;
			case 524:
				this.HWButtonEvent(num2, num3, ((num & 0x20) == 32) ? MouseButton.XButton1 : MouseButton.XButton2, MouseButtonState.Released);
				break;
			case 512:
				if ((num & 1) != 0)
				{
					this.HWMouseEvent(num2, num3, MouseButton.Left, 0);
				}
				else if ((num & 0x10) != 0)
				{
					this.HWMouseEvent(num2, num3, MouseButton.Middle, 0);
				}
				else if ((num & 2) != 0)
				{
					this.HWMouseEvent(num2, num3, MouseButton.Right, 0);
				}
				else if ((num & 0x20) != 0)
				{
					this.HWMouseEvent(num2, num3, MouseButton.XButton1, 0);
				}
				else if ((num & 0x40) != 0)
				{
					this.HWMouseEvent(num2, num3, MouseButton.XButton2, 0);
				}
				else
				{
					this.HWMouseEvent(num2, num3, null, 0);
				}
				lastMoveX = num2;
				lastMoveY = num3;
				break;
			case 522:
				this.HWMouseEvent(lastMoveX, lastMoveY, null, num);
				break;
			}
			return result;
		}

		[DllImport("user32.dll", CharSet = CharSet.Unicode)]
		internal static extern IntPtr CreateWindowEx(int dwExStyle, string lpszClassName, string lpszWindowName, int style, int x, int y, int width, int height, IntPtr hwndParent, IntPtr hMenu, IntPtr hInst, [MarshalAs(UnmanagedType.AsAny)] object pvParam);

		[DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool DestroyWindow(IntPtr hwnd);
	}
}
