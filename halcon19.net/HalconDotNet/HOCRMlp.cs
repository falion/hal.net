using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HOCRMlp : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRMlp()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRMlp(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRMlp(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("ocr_mlp");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRMlp obj)
		{
			obj = new HOCRMlp(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HOCRMlp[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HOCRMlp[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HOCRMlp(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HOCRMlp(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(694);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCRMlp(int widthCharacter, int heightCharacter, string interpolation, HTuple features, HTuple characters, int numHidden, string preprocessing, int numComponents, int randSeed)
		{
			IntPtr proc = HalconAPI.PreCall(708);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.Store(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.StoreI(proc, 5, numHidden);
			HalconAPI.StoreS(proc, 6, preprocessing);
			HalconAPI.StoreI(proc, 7, numComponents);
			HalconAPI.StoreI(proc, 8, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(characters);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HOCRMlp(int widthCharacter, int heightCharacter, string interpolation, string features, HTuple characters, int numHidden, string preprocessing, int numComponents, int randSeed)
		{
			IntPtr proc = HalconAPI.PreCall(708);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.StoreS(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.StoreI(proc, 5, numHidden);
			HalconAPI.StoreS(proc, 6, preprocessing);
			HalconAPI.StoreI(proc, 7, numComponents);
			HalconAPI.StoreI(proc, 8, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(characters);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeOcrClassMlp();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HOCRMlp(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeOcrClassMlp(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeOcrClassMlp();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HOCRMlp Deserialize(Stream stream)
		{
			HOCRMlp hOCRMlp = new HOCRMlp();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hOCRMlp.DeserializeOcrClassMlp(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRMlp;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HOCRMlp Clone()
		{
			HSerializedItem hSerializedItem = SerializeOcrClassMlp();
			HOCRMlp hOCRMlp = new HOCRMlp();
			hOCRMlp.DeserializeOcrClassMlp(hSerializedItem);
			hSerializedItem.Dispose();
			return hOCRMlp;
		}

		public HTuple SelectFeatureSetTrainfMlpProtected(HTuple trainingFile, HTuple password, HTuple featureList, string selectionMethod, int width, int height, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(661);
			HalconAPI.Store(proc, 0, trainingFile);
			HalconAPI.Store(proc, 1, password);
			HalconAPI.Store(proc, 2, featureList);
			HalconAPI.StoreS(proc, 3, selectionMethod);
			HalconAPI.StoreI(proc, 4, width);
			HalconAPI.StoreI(proc, 5, height);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(password);
			HalconAPI.UnpinTuple(featureList);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple SelectFeatureSetTrainfMlpProtected(string trainingFile, string password, string featureList, string selectionMethod, int width, int height, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(661);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.StoreS(proc, 1, password);
			HalconAPI.StoreS(proc, 2, featureList);
			HalconAPI.StoreS(proc, 3, selectionMethod);
			HalconAPI.StoreI(proc, 4, width);
			HalconAPI.StoreI(proc, 5, height);
			HalconAPI.Store(proc, 6, genParamName);
			HalconAPI.Store(proc, 7, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple SelectFeatureSetTrainfMlp(HTuple trainingFile, HTuple featureList, string selectionMethod, int width, int height, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(662);
			HalconAPI.Store(proc, 0, trainingFile);
			HalconAPI.Store(proc, 1, featureList);
			HalconAPI.StoreS(proc, 2, selectionMethod);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(featureList);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple SelectFeatureSetTrainfMlp(string trainingFile, string featureList, string selectionMethod, int width, int height, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(662);
			HalconAPI.StoreS(proc, 0, trainingFile);
			HalconAPI.StoreS(proc, 1, featureList);
			HalconAPI.StoreS(proc, 2, selectionMethod);
			HalconAPI.StoreI(proc, 3, width);
			HalconAPI.StoreI(proc, 4, height);
			HalconAPI.Store(proc, 5, genParamName);
			HalconAPI.Store(proc, 6, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public static void ClearOcrClassMlp(HOCRMlp[] OCRHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(OCRHandle);
			IntPtr proc = HalconAPI.PreCall(691);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(OCRHandle);
		}

		public void ClearOcrClassMlp()
		{
			IntPtr proc = HalconAPI.PreCall(691);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeOcrClassMlp(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(692);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeOcrClassMlp()
		{
			IntPtr proc = HalconAPI.PreCall(693);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadOcrClassMlp(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(694);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteOcrClassMlp(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(695);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetFeaturesOcrClassMlp(HImage character, string transform)
		{
			IntPtr proc = HalconAPI.PreCall(696);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.StoreS(proc, 1, transform);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			return tuple;
		}

		public HTuple DoOcrWordMlp(HRegion character, HImage image, string expression, int numAlternatives, int numCorrections, out HTuple confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(697);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DoOcrWordMlp(HRegion character, HImage image, string expression, int numAlternatives, int numCorrections, out double confidence, out string word, out double score)
		{
			IntPtr proc = HalconAPI.PreCall(697);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreS(proc, 1, expression);
			HalconAPI.StoreI(proc, 2, numAlternatives);
			HalconAPI.StoreI(proc, 3, numCorrections);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			err = HalconAPI.LoadS(proc, 2, err, out word);
			err = HalconAPI.LoadD(proc, 3, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return stringValue;
		}

		public HTuple DoOcrMultiClassMlp(HRegion character, HImage image, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(698);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DoOcrMultiClassMlp(HRegion character, HImage image, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(698);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return stringValue;
		}

		public HTuple DoOcrSingleClassMlp(HRegion character, HImage image, HTuple num, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(699);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return tuple;
		}

		public string DoOcrSingleClassMlp(HRegion character, HImage image, HTuple num, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(699);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, character);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 1, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(num);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(character);
			GC.KeepAlive(image);
			return stringValue;
		}

		public double TrainfOcrClassMlpProtected(HTuple trainingFile, HTuple password, int maxIterations, double weightTolerance, double errorTolerance, out HTuple errorLog)
		{
			IntPtr proc = HalconAPI.PreCall(700);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, trainingFile);
			HalconAPI.Store(proc, 2, password);
			HalconAPI.StoreI(proc, 3, maxIterations);
			HalconAPI.StoreD(proc, 4, weightTolerance);
			HalconAPI.StoreD(proc, 5, errorTolerance);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			HalconAPI.UnpinTuple(password);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out errorLog);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public double TrainfOcrClassMlpProtected(string trainingFile, string password, int maxIterations, double weightTolerance, double errorTolerance, out HTuple errorLog)
		{
			IntPtr proc = HalconAPI.PreCall(700);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, trainingFile);
			HalconAPI.StoreS(proc, 2, password);
			HalconAPI.StoreI(proc, 3, maxIterations);
			HalconAPI.StoreD(proc, 4, weightTolerance);
			HalconAPI.StoreD(proc, 5, errorTolerance);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out errorLog);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public double TrainfOcrClassMlp(HTuple trainingFile, int maxIterations, double weightTolerance, double errorTolerance, out HTuple errorLog)
		{
			IntPtr proc = HalconAPI.PreCall(701);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, trainingFile);
			HalconAPI.StoreI(proc, 2, maxIterations);
			HalconAPI.StoreD(proc, 3, weightTolerance);
			HalconAPI.StoreD(proc, 4, errorTolerance);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out errorLog);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public double TrainfOcrClassMlp(string trainingFile, int maxIterations, double weightTolerance, double errorTolerance, out HTuple errorLog)
		{
			IntPtr proc = HalconAPI.PreCall(701);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, trainingFile);
			HalconAPI.StoreI(proc, 2, maxIterations);
			HalconAPI.StoreD(proc, 3, weightTolerance);
			HalconAPI.StoreD(proc, 4, errorTolerance);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out errorLog);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple GetPrepInfoOcrClassMlp(HTuple trainingFile, string preprocessing, out HTuple cumInformationCont)
		{
			IntPtr proc = HalconAPI.PreCall(702);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, trainingFile);
			HalconAPI.StoreS(proc, 2, preprocessing);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(trainingFile);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cumInformationCont);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetPrepInfoOcrClassMlp(string trainingFile, string preprocessing, out HTuple cumInformationCont)
		{
			IntPtr proc = HalconAPI.PreCall(702);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, trainingFile);
			HalconAPI.StoreS(proc, 2, preprocessing);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cumInformationCont);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetRejectionParamsOcrClassMlp(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(703);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public string GetRejectionParamsOcrClassMlp(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(703);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return stringValue;
		}

		public void SetRejectionParamsOcrClassMlp(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(704);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetRejectionParamsOcrClassMlp(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(704);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetRegularizationParamsOcrClassMlp(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(705);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetRegularizationParamsOcrClassMlp(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(706);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetRegularizationParamsOcrClassMlp(string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(706);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GetParamsOcrClassMlp(out int widthCharacter, out int heightCharacter, out string interpolation, out HTuple features, out HTuple characters, out int numHidden, out string preprocessing, out int numComponents)
		{
			IntPtr proc = HalconAPI.PreCall(707);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out widthCharacter);
			err = HalconAPI.LoadI(proc, 1, err, out heightCharacter);
			err = HalconAPI.LoadS(proc, 2, err, out interpolation);
			err = HTuple.LoadNew(proc, 3, err, out features);
			err = HTuple.LoadNew(proc, 4, err, out characters);
			err = HalconAPI.LoadI(proc, 5, err, out numHidden);
			err = HalconAPI.LoadS(proc, 6, err, out preprocessing);
			err = HalconAPI.LoadI(proc, 7, err, out numComponents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetParamsOcrClassMlp(out int widthCharacter, out int heightCharacter, out string interpolation, out string features, out HTuple characters, out int numHidden, out string preprocessing, out int numComponents)
		{
			IntPtr proc = HalconAPI.PreCall(707);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out widthCharacter);
			err = HalconAPI.LoadI(proc, 1, err, out heightCharacter);
			err = HalconAPI.LoadS(proc, 2, err, out interpolation);
			err = HalconAPI.LoadS(proc, 3, err, out features);
			err = HTuple.LoadNew(proc, 4, err, out characters);
			err = HalconAPI.LoadI(proc, 5, err, out numHidden);
			err = HalconAPI.LoadS(proc, 6, err, out preprocessing);
			err = HalconAPI.LoadI(proc, 7, err, out numComponents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcrClassMlp(int widthCharacter, int heightCharacter, string interpolation, HTuple features, HTuple characters, int numHidden, string preprocessing, int numComponents, int randSeed)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(708);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.Store(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.StoreI(proc, 5, numHidden);
			HalconAPI.StoreS(proc, 6, preprocessing);
			HalconAPI.StoreI(proc, 7, numComponents);
			HalconAPI.StoreI(proc, 8, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(characters);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateOcrClassMlp(int widthCharacter, int heightCharacter, string interpolation, string features, HTuple characters, int numHidden, string preprocessing, int numComponents, int randSeed)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(708);
			HalconAPI.StoreI(proc, 0, widthCharacter);
			HalconAPI.StoreI(proc, 1, heightCharacter);
			HalconAPI.StoreS(proc, 2, interpolation);
			HalconAPI.StoreS(proc, 3, features);
			HalconAPI.Store(proc, 4, characters);
			HalconAPI.StoreI(proc, 5, numHidden);
			HalconAPI.StoreS(proc, 6, preprocessing);
			HalconAPI.StoreI(proc, 7, numComponents);
			HalconAPI.StoreI(proc, 8, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(characters);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
