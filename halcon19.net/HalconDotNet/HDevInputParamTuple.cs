namespace HalconDotNet
{
	internal class HDevInputParamTuple : HDevInputParam
	{
		protected HTuple mTuple;

		public HDevInputParamTuple(HTuple tuple)
		{
			mTuple = new HTuple(tuple);
		}

		public override HTuple GetCtrlParamTuple()
		{
			return mTuple;
		}

		public override void Dispose()
		{
			if (mTuple != null)
			{
				mTuple.Dispose();
				mTuple = null;
			}
		}
	}
}
