using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HDlClassifier : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifier()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifier(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifier(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("dl_classifier");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlClassifier obj)
		{
			obj = new HDlClassifier(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlClassifier[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDlClassifier[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HDlClassifier(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HDlClassifier(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(2122);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeDlClassifier();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifier(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeDlClassifier(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeDlClassifier();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HDlClassifier Deserialize(Stream stream)
		{
			HDlClassifier hDlClassifier = new HDlClassifier();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hDlClassifier.DeserializeDlClassifier(hSerializedItem);
			hSerializedItem.Dispose();
			return hDlClassifier;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HDlClassifier Clone()
		{
			HSerializedItem hSerializedItem = SerializeDlClassifier();
			HDlClassifier hDlClassifier = new HDlClassifier();
			hDlClassifier.DeserializeDlClassifier(hSerializedItem);
			hSerializedItem.Dispose();
			return hDlClassifier;
		}

		public HDlClassifierResult ApplyDlClassifier(HImage images)
		{
			IntPtr proc = HalconAPI.PreCall(2102);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, images);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HDlClassifierResult.LoadNew(proc, 0, err, out HDlClassifierResult obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(images);
			return obj;
		}

		public static void ClearDlClassifier(HDlClassifier[] DLClassifierHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(DLClassifierHandle);
			IntPtr proc = HalconAPI.PreCall(2103);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(DLClassifierHandle);
		}

		public void ClearDlClassifier()
		{
			IntPtr proc = HalconAPI.PreCall(2103);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeDlClassifier(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2109);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HTuple GetDlClassifierParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2114);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDlClassifierParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(2114);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void ReadDlClassifier(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2122);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSerializedItem SerializeDlClassifier()
		{
			IntPtr proc = HalconAPI.PreCall(2126);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SetDlClassifierParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2128);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDlClassifierParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(2128);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteDlClassifier(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(2132);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
