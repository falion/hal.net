using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HTextureInspectionResult : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionResult()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionResult(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextureInspectionResult(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("texture_inspection_result");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextureInspectionResult obj)
		{
			obj = new HTextureInspectionResult(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextureInspectionResult[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HTextureInspectionResult[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HTextureInspectionResult(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HTextureInspectionResult(HImage image, out HRegion noveltyRegion, HTextureInspectionModel textureInspectionModel)
		{
			IntPtr proc = HalconAPI.PreCall(2044);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			err = HRegion.LoadNew(proc, 1, err, out noveltyRegion);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(textureInspectionModel);
		}

		public static HTuple AddTextureInspectionModelImage(HImage image, HTextureInspectionModel textureInspectionModel)
		{
			IntPtr proc = HalconAPI.PreCall(2043);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(image);
			GC.KeepAlive(textureInspectionModel);
			return tuple;
		}

		public HRegion ApplyTextureInspectionModel(HImage image, HTextureInspectionModel textureInspectionModel)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2044);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(textureInspectionModel);
			return obj;
		}

		public static void ClearTextureInspectionResult(HTextureInspectionResult[] textureInspectionResultID)
		{
			HTuple hTuple = HHandleBase.ConcatArray(textureInspectionResultID);
			IntPtr proc = HalconAPI.PreCall(2048);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(textureInspectionResultID);
		}

		public void ClearTextureInspectionResult()
		{
			IntPtr proc = HalconAPI.PreCall(2048);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static HImage GetTextureInspectionModelImage(HTextureInspectionModel textureInspectionModel)
		{
			IntPtr proc = HalconAPI.PreCall(2075);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(textureInspectionModel);
			return obj;
		}

		public HObject GetTextureInspectionResultObject(HTuple resultName)
		{
			IntPtr proc = HalconAPI.PreCall(2077);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultName);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HObject GetTextureInspectionResultObject(string resultName)
		{
			IntPtr proc = HalconAPI.PreCall(2077);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public static void TrainTextureInspectionModel(HTextureInspectionModel textureInspectionModel)
		{
			IntPtr proc = HalconAPI.PreCall(2099);
			HalconAPI.Store(proc, 0, textureInspectionModel);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(textureInspectionModel);
		}
	}
}
