using System;

namespace HalconDotNet
{
	internal class HTupleElementsMixed : HTupleElementsImplementation
	{
		internal HTupleElementsMixed(HTupleMixed source, int index)
			: base(source, index)
		{
		}

		internal HTupleElementsMixed(HTupleMixed source, int[] indices)
			: base(source, indices)
		{
		}

		public override int[] getI()
		{
			if (indices == null)
			{
				return null;
			}
			switch (getType())
			{
			case HTupleType.INTEGER:
			{
				int[] array2 = new int[indices.Length];
				for (int j = 0; j < indices.Length; j++)
				{
					array2[j] = (int)source.OArr[indices[j]];
				}
				return array2;
			}
			case HTupleType.LONG:
			{
				int[] array = new int[indices.Length];
				for (int i = 0; i < indices.Length; i++)
				{
					array[i] = (int)(long)source.OArr[indices[i]];
				}
				return array;
			}
			case HTupleType.HANDLE:
			{
				int[] array3 = new int[indices.Length];
				for (int k = 0; k < indices.Length; k++)
				{
					if (!HalconAPI.IsLegacyHandleMode())
					{
						throw new HTupleAccessException("Implicit access to handle as number is only allowed in legacy handle mode. Use *.H.Handle to get IntPtr value.");
					}
					if (HalconAPI.isPlatform64)
					{
						throw new HTupleAccessException("System.Int32 cannot represent a handle on this platform");
					}
					array3[k] = (int)((HHandle)source.OArr[indices[k]]).Handle;
					((HHandle)source.OArr[indices[k]]).Detach();
				}
				return array3;
			}
			default:
				throw new HTupleAccessException(source, "Mixed tuple does not contain integer " + ((indices.Length == 1) ? ("value at index " + indices[0]) : "values at given indices"));
			}
		}

		public override long[] getL()
		{
			if (indices == null)
			{
				return null;
			}
			switch (getType())
			{
			case HTupleType.INTEGER:
			{
				long[] array2 = new long[indices.Length];
				for (int j = 0; j < indices.Length; j++)
				{
					array2[j] = (int)source.OArr[indices[j]];
				}
				return array2;
			}
			case HTupleType.LONG:
			{
				long[] array = new long[indices.Length];
				for (int i = 0; i < indices.Length; i++)
				{
					array[i] = (long)source.OArr[indices[i]];
				}
				return array;
			}
			case HTupleType.HANDLE:
			{
				long[] array3 = new long[indices.Length];
				for (int k = 0; k < indices.Length; k++)
				{
					if (!HalconAPI.IsLegacyHandleMode())
					{
						throw new HTupleAccessException("Implicit access to handle as number is only allowed in legacy handle mode. Use *.H.Handle to get IntPtr value.");
					}
					array3[k] = (long)((HHandle)source.OArr[indices[k]]).Handle;
					((HHandle)source.OArr[indices[k]]).Detach();
				}
				return array3;
			}
			default:
				throw new HTupleAccessException(source, "Mixed tuple does not contain integer " + ((indices.Length == 1) ? ("value at index " + indices[0]) : "values at given indices"));
			}
		}

		public override double[] getD()
		{
			if (indices == null)
			{
				return null;
			}
			switch (getType())
			{
			case HTupleType.DOUBLE:
			{
				double[] array2 = new double[indices.Length];
				for (int j = 0; j < indices.Length; j++)
				{
					array2[j] = (double)source.OArr[indices[j]];
				}
				return array2;
			}
			case HTupleType.INTEGER:
			{
				double[] array = new double[indices.Length];
				for (int i = 0; i < indices.Length; i++)
				{
					array[i] = (int)source.OArr[indices[i]];
				}
				return array;
			}
			case HTupleType.LONG:
			{
				double[] array3 = new double[indices.Length];
				for (int k = 0; k < indices.Length; k++)
				{
					array3[k] = (long)source.OArr[indices[k]];
				}
				return array3;
			}
			default:
				throw new HTupleAccessException(source, "Mixed tuple does not contain numeric " + ((indices.Length == 1) ? ("value at index " + indices[0]) : "values at given indices"));
			}
		}

		public override string[] getS()
		{
			if (indices == null)
			{
				return null;
			}
			HTupleType type = getType();
			if (type == HTupleType.STRING)
			{
				string[] array = new string[indices.Length];
				for (int i = 0; i < indices.Length; i++)
				{
					array[i] = (string)source.OArr[indices[i]];
				}
				return array;
			}
			throw new HTupleAccessException(source, "Mixed tuple does not contain string " + ((indices.Length == 1) ? ("value at index " + indices[0]) : "values at given indices"));
		}

		public override HHandle[] getH()
		{
			if (indices == null)
			{
				return null;
			}
			HTupleType type = getType();
			if (type == HTupleType.HANDLE)
			{
				HHandle[] array = new HHandle[indices.Length];
				for (int i = 0; i < indices.Length; i++)
				{
					array[i] = (HHandle)source.OArr[indices[i]];
				}
				return array;
			}
			throw new HTupleAccessException(source, "Mixed tuple does not contain handle " + ((indices.Length == 1) ? ("value at index " + indices[0]) : "values at given indices"));
		}

		public override object[] getO()
		{
			if (indices == null)
			{
				return null;
			}
			object[] array = new object[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				array[i] = source.OArr[indices[i]];
			}
			return array;
		}

		protected void DisposeElement(int index)
		{
			(source.OArr[index] as IDisposable)?.Dispose();
		}

		public override void setI(int[] i)
		{
			if (IsValidArrayForSetX(i))
			{
				bool flag = i.Length == 1;
				for (int j = 0; j < indices.Length; j++)
				{
					DisposeElement(indices[j]);
					source.OArr[indices[j]] = i[(!flag) ? j : 0];
				}
			}
		}

		public override void setL(long[] l)
		{
			if (IsValidArrayForSetX(l))
			{
				bool flag = l.Length == 1;
				for (int i = 0; i < indices.Length; i++)
				{
					DisposeElement(indices[i]);
					source.OArr[indices[i]] = l[(!flag) ? i : 0];
				}
			}
		}

		public override void setD(double[] d)
		{
			if (IsValidArrayForSetX(d))
			{
				bool flag = d.Length == 1;
				for (int i = 0; i < indices.Length; i++)
				{
					DisposeElement(indices[i]);
					source.OArr[indices[i]] = d[(!flag) ? i : 0];
				}
			}
		}

		public override void setS(string[] s)
		{
			if (IsValidArrayForSetX(s))
			{
				bool flag = s.Length == 1;
				for (int i = 0; i < indices.Length; i++)
				{
					DisposeElement(indices[i]);
					source.OArr[indices[i]] = s[(!flag) ? i : 0];
				}
			}
		}

		public override void Dispose()
		{
			GC.SuppressFinalize(this);
			GC.KeepAlive(this);
		}

		public override void setH(HHandle[] h)
		{
			if (IsValidArrayForSetX(h))
			{
				bool flag = h.Length == 1;
				for (int i = 0; i < indices.Length; i++)
				{
					DisposeElement(indices[i]);
					source.OArr[indices[i]] = new HHandle(h[(!flag) ? i : 0]);
				}
			}
		}

		public override void setO(object[] o)
		{
			if (!IsValidArrayForSetX(o))
			{
				return;
			}
			bool flag = o.Length == 1;
			for (int i = 0; i < indices.Length; i++)
			{
				DisposeElement(indices[i]);
				object obj = o[(!flag) ? i : 0];
				if (HTupleImplementation.GetObjectType(obj) == 16)
				{
					obj = new HHandle((HHandle)obj);
				}
				source.OArr[indices[i]] = obj;
			}
		}

		public override HTupleType getType()
		{
			return ((HTupleMixed)source).GetElementType(indices);
		}
	}
}
