using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HClassKnn : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassKnn()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassKnn(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassKnn(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("class_knn");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassKnn obj)
		{
			obj = new HClassKnn(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassKnn[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HClassKnn[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HClassKnn(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HClassKnn(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1809);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassKnn(HTuple numDim)
		{
			IntPtr proc = HalconAPI.PreCall(1816);
			HalconAPI.Store(proc, 0, numDim);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numDim);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeClassKnn();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassKnn(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeClassKnn(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeClassKnn();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HClassKnn Deserialize(Stream stream)
		{
			HClassKnn hClassKnn = new HClassKnn();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hClassKnn.DeserializeClassKnn(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassKnn;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HClassKnn Clone()
		{
			HSerializedItem hSerializedItem = SerializeClassKnn();
			HClassKnn hClassKnn = new HClassKnn();
			hClassKnn.DeserializeClassKnn(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassKnn;
		}

		public HRegion ClassifyImageClassKnn(HImage image, out HImage distanceImage, double rejectionThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(429);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, rejectionThreshold);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			err = HImage.LoadNew(proc, 2, err, out distanceImage);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void AddSamplesImageClassKnn(HImage image, HRegion classRegions)
		{
			IntPtr proc = HalconAPI.PreCall(430);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 2, classRegions);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(classRegions);
		}

		public HClassTrainData GetClassTrainDataKnn()
		{
			IntPtr proc = HalconAPI.PreCall(1789);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HClassTrainData.LoadNew(proc, 0, err, out HClassTrainData obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void AddClassTrainDataKnn(HClassTrainData classTrainDataHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1790);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, classTrainDataHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
		}

		public HTuple SelectFeatureSetKnn(HClassTrainData classTrainDataHandle, string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1802);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return tuple;
		}

		public HTuple SelectFeatureSetKnn(HClassTrainData classTrainDataHandle, string selectionMethod, string genParamName, double genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1802);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return tuple;
		}

		public void ClearClassKnn()
		{
			IntPtr proc = HalconAPI.PreCall(1804);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int GetSampleNumClassKnn()
		{
			IntPtr proc = HalconAPI.PreCall(1805);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple GetSampleClassKnn(int indexSample, out HTuple classID)
		{
			IntPtr proc = HalconAPI.PreCall(1806);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, indexSample);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out classID);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void DeserializeClassKnn(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1807);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeClassKnn()
		{
			IntPtr proc = HalconAPI.PreCall(1808);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadClassKnn(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1809);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteClassKnn(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1810);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetParamsClassKnn(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1811);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetParamsClassKnn(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1812);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ClassifyClassKnn(HTuple features, out HTuple rating)
		{
			IntPtr proc = HalconAPI.PreCall(1813);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out rating);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void TrainClassKnn(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1814);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AddSampleClassKnn(HTuple features, HTuple classID)
		{
			IntPtr proc = HalconAPI.PreCall(1815);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.Store(proc, 2, classID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(classID);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AddSampleClassKnn(double features, int classID)
		{
			IntPtr proc = HalconAPI.PreCall(1815);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, features);
			HalconAPI.StoreI(proc, 2, classID);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateClassKnn(HTuple numDim)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1816);
			HalconAPI.Store(proc, 0, numDim);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numDim);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassLUT CreateClassLutKnn(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1819);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HClassLUT.LoadNew(proc, 0, err, out HClassLUT obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}
	}
}
