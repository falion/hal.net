using System;
using System.ComponentModel;

namespace HalconDotNet
{
	[Serializable]
	public class HXLDCont : HXLD
	{
		public new HXLDCont this[HTuple index] => SelectObj(index);

		public HXLDCont()
			: base(HObjectBase.UNDEF, copy: false)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDCont(IntPtr key)
			: this(key, copy: true)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDCont(IntPtr key, bool copy)
			: base(key, copy)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDCont(HObject obj)
			: base(obj)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		private void AssertObjectClass()
		{
			HalconAPI.AssertObjectClass(key, "xld_cont");
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, int err, out HXLDCont obj)
		{
			obj = new HXLDCont(HObjectBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		public HXLDCont(HRegion regions, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(70);
			HalconAPI.Store(proc, 1, regions);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(regions);
		}

		public HXLDCont(HTuple row, HTuple col)
		{
			IntPtr proc = HalconAPI.PreCall(72);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont UnionCotangentialContoursXld(double fitClippingLength, HTuple fitLength, double maxTangAngle, double maxDist, double maxDistPerp, double maxOverlap, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(0);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, fitClippingLength);
			HalconAPI.Store(proc, 1, fitLength);
			HalconAPI.StoreD(proc, 2, maxTangAngle);
			HalconAPI.StoreD(proc, 3, maxDist);
			HalconAPI.StoreD(proc, 4, maxDistPerp);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(fitLength);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont UnionCotangentialContoursXld(double fitClippingLength, double fitLength, double maxTangAngle, double maxDist, double maxDistPerp, double maxOverlap, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(0);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, fitClippingLength);
			HalconAPI.StoreD(proc, 1, fitLength);
			HalconAPI.StoreD(proc, 2, maxTangAngle);
			HalconAPI.StoreD(proc, 3, maxDist);
			HalconAPI.StoreD(proc, 4, maxDistPerp);
			HalconAPI.StoreD(proc, 5, maxOverlap);
			HalconAPI.StoreS(proc, 6, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont PolarTransContourXldInv(HTuple row, HTuple column, double angleStart, double angleEnd, HTuple radiusStart, HTuple radiusEnd, int widthIn, int heightIn, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.Store(proc, 4, radiusStart);
			HalconAPI.Store(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, widthIn);
			HalconAPI.StoreI(proc, 7, heightIn);
			HalconAPI.StoreI(proc, 8, width);
			HalconAPI.StoreI(proc, 9, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radiusStart);
			HalconAPI.UnpinTuple(radiusEnd);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont PolarTransContourXldInv(double row, double column, double angleStart, double angleEnd, double radiusStart, double radiusEnd, int widthIn, int heightIn, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(1);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.StoreD(proc, 4, radiusStart);
			HalconAPI.StoreD(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, widthIn);
			HalconAPI.StoreI(proc, 7, heightIn);
			HalconAPI.StoreI(proc, 8, width);
			HalconAPI.StoreI(proc, 9, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont PolarTransContourXld(HTuple row, HTuple column, double angleStart, double angleEnd, HTuple radiusStart, HTuple radiusEnd, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(2);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.Store(proc, 4, radiusStart);
			HalconAPI.Store(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radiusStart);
			HalconAPI.UnpinTuple(radiusEnd);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont PolarTransContourXld(double row, double column, double angleStart, double angleEnd, double radiusStart, double radiusEnd, int width, int height)
		{
			IntPtr proc = HalconAPI.PreCall(2);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, angleStart);
			HalconAPI.StoreD(proc, 3, angleEnd);
			HalconAPI.StoreD(proc, 4, radiusStart);
			HalconAPI.StoreD(proc, 5, radiusEnd);
			HalconAPI.StoreI(proc, 6, width);
			HalconAPI.StoreI(proc, 7, height);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void GenContourNurbsXld(HTuple rows, HTuple cols, HTuple knots, HTuple weights, int degree, HTuple maxError, HTuple maxDistance)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(4);
			HalconAPI.Store(proc, 0, rows);
			HalconAPI.Store(proc, 1, cols);
			HalconAPI.Store(proc, 2, knots);
			HalconAPI.Store(proc, 3, weights);
			HalconAPI.StoreI(proc, 4, degree);
			HalconAPI.Store(proc, 5, maxError);
			HalconAPI.Store(proc, 6, maxDistance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(cols);
			HalconAPI.UnpinTuple(knots);
			HalconAPI.UnpinTuple(weights);
			HalconAPI.UnpinTuple(maxError);
			HalconAPI.UnpinTuple(maxDistance);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenContourNurbsXld(HTuple rows, HTuple cols, string knots, string weights, int degree, double maxError, double maxDistance)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(4);
			HalconAPI.Store(proc, 0, rows);
			HalconAPI.Store(proc, 1, cols);
			HalconAPI.StoreS(proc, 2, knots);
			HalconAPI.StoreS(proc, 3, weights);
			HalconAPI.StoreI(proc, 4, degree);
			HalconAPI.StoreD(proc, 5, maxError);
			HalconAPI.StoreD(proc, 6, maxDistance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rows);
			HalconAPI.UnpinTuple(cols);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont Union2ClosedContoursXld(HXLDCont contours2)
		{
			IntPtr proc = HalconAPI.PreCall(6);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, contours2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours2);
			return obj;
		}

		public HXLDCont SymmDifferenceClosedContoursXld(HXLDCont contours2)
		{
			IntPtr proc = HalconAPI.PreCall(8);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, contours2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours2);
			return obj;
		}

		public HXLDCont DifferenceClosedContoursXld(HXLDCont sub)
		{
			IntPtr proc = HalconAPI.PreCall(10);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, sub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(sub);
			return obj;
		}

		public HXLDCont IntersectionClosedContoursXld(HXLDCont contours2)
		{
			IntPtr proc = HalconAPI.PreCall(12);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, contours2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contours2);
			return obj;
		}

		public HXLDCont UnionCocircularContoursXld(HTuple maxArcAngleDiff, HTuple maxArcOverlap, HTuple maxTangentAngle, HTuple maxDist, HTuple maxRadiusDiff, HTuple maxCenterDist, string mergeSmallContours, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(13);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, maxArcAngleDiff);
			HalconAPI.Store(proc, 1, maxArcOverlap);
			HalconAPI.Store(proc, 2, maxTangentAngle);
			HalconAPI.Store(proc, 3, maxDist);
			HalconAPI.Store(proc, 4, maxRadiusDiff);
			HalconAPI.Store(proc, 5, maxCenterDist);
			HalconAPI.StoreS(proc, 6, mergeSmallContours);
			HalconAPI.StoreI(proc, 7, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maxArcAngleDiff);
			HalconAPI.UnpinTuple(maxArcOverlap);
			HalconAPI.UnpinTuple(maxTangentAngle);
			HalconAPI.UnpinTuple(maxDist);
			HalconAPI.UnpinTuple(maxRadiusDiff);
			HalconAPI.UnpinTuple(maxCenterDist);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont UnionCocircularContoursXld(double maxArcAngleDiff, double maxArcOverlap, double maxTangentAngle, double maxDist, double maxRadiusDiff, double maxCenterDist, string mergeSmallContours, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(13);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxArcAngleDiff);
			HalconAPI.StoreD(proc, 1, maxArcOverlap);
			HalconAPI.StoreD(proc, 2, maxTangentAngle);
			HalconAPI.StoreD(proc, 3, maxDist);
			HalconAPI.StoreD(proc, 4, maxRadiusDiff);
			HalconAPI.StoreD(proc, 5, maxCenterDist);
			HalconAPI.StoreS(proc, 6, mergeSmallContours);
			HalconAPI.StoreI(proc, 7, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont CropContoursXld(HTuple row1, HTuple col1, HTuple row2, HTuple col2, string closeContours)
		{
			IntPtr proc = HalconAPI.PreCall(14);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, col1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, col2);
			HalconAPI.StoreS(proc, 4, closeContours);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(col1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(col2);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont CropContoursXld(double row1, double col1, double row2, double col2, string closeContours)
		{
			IntPtr proc = HalconAPI.PreCall(14);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, col1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, col2);
			HalconAPI.StoreS(proc, 4, closeContours);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void GenCrossContourXld(HTuple row, HTuple col, HTuple size, double angle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(15);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.Store(proc, 2, size);
			HalconAPI.StoreD(proc, 3, angle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(size);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCrossContourXld(double row, double col, double size, double angle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(15);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, col);
			HalconAPI.StoreD(proc, 2, size);
			HalconAPI.StoreD(proc, 3, angle);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont SortContoursXld(string sortMode, string order, string rowOrCol)
		{
			IntPtr proc = HalconAPI.PreCall(16);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, sortMode);
			HalconAPI.StoreS(proc, 1, order);
			HalconAPI.StoreS(proc, 2, rowOrCol);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont MergeContLineScanXld(HXLDCont prevConts, out HXLDCont prevMergedConts, int imageHeight, HTuple margin, string mergeBorder, int maxImagesCont)
		{
			IntPtr proc = HalconAPI.PreCall(17);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, prevConts);
			HalconAPI.StoreI(proc, 0, imageHeight);
			HalconAPI.Store(proc, 1, margin);
			HalconAPI.StoreS(proc, 2, mergeBorder);
			HalconAPI.StoreI(proc, 3, maxImagesCont);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(margin);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			err = LoadNew(proc, 2, err, out prevMergedConts);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(prevConts);
			return obj;
		}

		public HXLDCont MergeContLineScanXld(HXLDCont prevConts, out HXLDCont prevMergedConts, int imageHeight, double margin, string mergeBorder, int maxImagesCont)
		{
			IntPtr proc = HalconAPI.PreCall(17);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, prevConts);
			HalconAPI.StoreI(proc, 0, imageHeight);
			HalconAPI.StoreD(proc, 1, margin);
			HalconAPI.StoreS(proc, 2, mergeBorder);
			HalconAPI.StoreI(proc, 3, maxImagesCont);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			err = LoadNew(proc, 2, err, out prevMergedConts);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(prevConts);
			return obj;
		}

		public void ReadContourXldArcInfo(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(20);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteContourXldArcInfo(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(21);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HXLDCont GenParallelContourXld(string mode, HTuple distance)
		{
			IntPtr proc = HalconAPI.PreCall(23);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(distance);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont GenParallelContourXld(string mode, double distance)
		{
			IntPtr proc = HalconAPI.PreCall(23);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void GenRectangle2ContourXld(HTuple row, HTuple column, HTuple phi, HTuple length1, HTuple length2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(24);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, phi);
			HalconAPI.Store(proc, 3, length1);
			HalconAPI.Store(proc, 4, length2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(length1);
			HalconAPI.UnpinTuple(length2);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenRectangle2ContourXld(double row, double column, double phi, double length1, double length2)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(24);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, length1);
			HalconAPI.StoreD(proc, 4, length2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DistRectangle2ContourPointsXld(int clippingEndPoints, double row, double column, double phi, double length1, double length2)
		{
			IntPtr proc = HalconAPI.PreCall(25);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, clippingEndPoints);
			HalconAPI.StoreD(proc, 1, row);
			HalconAPI.StoreD(proc, 2, column);
			HalconAPI.StoreD(proc, 3, phi);
			HalconAPI.StoreD(proc, 4, length1);
			HalconAPI.StoreD(proc, 5, length2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void FitRectangle2ContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int iterations, double clippingFactor, out HTuple row, out HTuple column, out HTuple phi, out HTuple length1, out HTuple length2, out HTuple pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(26);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, iterations);
			HalconAPI.StoreD(proc, 5, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out length1);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out length2);
			err = HTuple.LoadNew(proc, 5, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitRectangle2ContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int iterations, double clippingFactor, out double row, out double column, out double phi, out double length1, out double length2, out string pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(26);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, iterations);
			HalconAPI.StoreD(proc, 5, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out length1);
			err = HalconAPI.LoadD(proc, 4, err, out length2);
			err = HalconAPI.LoadS(proc, 5, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont SegmentContourAttribXld(HTuple attribute, string operation, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(27);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, attribute);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.Store(proc, 2, min);
			HalconAPI.Store(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribute);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont SegmentContourAttribXld(string attribute, string operation, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(27);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, attribute);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.StoreD(proc, 2, min);
			HalconAPI.StoreD(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont SegmentContoursXld(string mode, int smoothCont, double maxLineDist1, double maxLineDist2)
		{
			IntPtr proc = HalconAPI.PreCall(28);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, smoothCont);
			HalconAPI.StoreD(proc, 2, maxLineDist1);
			HalconAPI.StoreD(proc, 3, maxLineDist2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void FitCircleContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int iterations, double clippingFactor, out HTuple row, out HTuple column, out HTuple radius, out HTuple startPhi, out HTuple endPhi, out HTuple pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(29);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, iterations);
			HalconAPI.StoreD(proc, 5, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out radius);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out startPhi);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out endPhi);
			err = HTuple.LoadNew(proc, 5, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitCircleContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int iterations, double clippingFactor, out double row, out double column, out double radius, out double startPhi, out double endPhi, out string pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(29);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, iterations);
			HalconAPI.StoreD(proc, 5, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out radius);
			err = HalconAPI.LoadD(proc, 3, err, out startPhi);
			err = HalconAPI.LoadD(proc, 4, err, out endPhi);
			err = HalconAPI.LoadS(proc, 5, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitLineContourXld(string algorithm, int maxNumPoints, int clippingEndPoints, int iterations, double clippingFactor, out HTuple rowBegin, out HTuple colBegin, out HTuple rowEnd, out HTuple colEnd, out HTuple nr, out HTuple nc, out HTuple dist)
		{
			IntPtr proc = HalconAPI.PreCall(30);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreI(proc, 2, clippingEndPoints);
			HalconAPI.StoreI(proc, 3, iterations);
			HalconAPI.StoreD(proc, 4, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rowBegin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out colBegin);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out rowEnd);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out colEnd);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out nr);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out nc);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out dist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitLineContourXld(string algorithm, int maxNumPoints, int clippingEndPoints, int iterations, double clippingFactor, out double rowBegin, out double colBegin, out double rowEnd, out double colEnd, out double nr, out double nc, out double dist)
		{
			IntPtr proc = HalconAPI.PreCall(30);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreI(proc, 2, clippingEndPoints);
			HalconAPI.StoreI(proc, 3, iterations);
			HalconAPI.StoreD(proc, 4, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out rowBegin);
			err = HalconAPI.LoadD(proc, 1, err, out colBegin);
			err = HalconAPI.LoadD(proc, 2, err, out rowEnd);
			err = HalconAPI.LoadD(proc, 3, err, out colEnd);
			err = HalconAPI.LoadD(proc, 4, err, out nr);
			err = HalconAPI.LoadD(proc, 5, err, out nc);
			err = HalconAPI.LoadD(proc, 6, err, out dist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple DistEllipseContourPointsXld(string distanceMode, int clippingEndPoints, double row, double column, double phi, double radius1, double radius2)
		{
			IntPtr proc = HalconAPI.PreCall(31);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, distanceMode);
			HalconAPI.StoreI(proc, 1, clippingEndPoints);
			HalconAPI.StoreD(proc, 2, row);
			HalconAPI.StoreD(proc, 3, column);
			HalconAPI.StoreD(proc, 4, phi);
			HalconAPI.StoreD(proc, 5, radius1);
			HalconAPI.StoreD(proc, 6, radius2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void DistEllipseContourXld(string mode, int maxNumPoints, int clippingEndPoints, double row, double column, double phi, double radius1, double radius2, out HTuple minDist, out HTuple maxDist, out HTuple avgDist, out HTuple sigmaDist)
		{
			IntPtr proc = HalconAPI.PreCall(32);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreI(proc, 2, clippingEndPoints);
			HalconAPI.StoreD(proc, 3, row);
			HalconAPI.StoreD(proc, 4, column);
			HalconAPI.StoreD(proc, 5, phi);
			HalconAPI.StoreD(proc, 6, radius1);
			HalconAPI.StoreD(proc, 7, radius2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out minDist);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out maxDist);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out avgDist);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out sigmaDist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistEllipseContourXld(string mode, int maxNumPoints, int clippingEndPoints, double row, double column, double phi, double radius1, double radius2, out double minDist, out double maxDist, out double avgDist, out double sigmaDist)
		{
			IntPtr proc = HalconAPI.PreCall(32);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreI(proc, 2, clippingEndPoints);
			HalconAPI.StoreD(proc, 3, row);
			HalconAPI.StoreD(proc, 4, column);
			HalconAPI.StoreD(proc, 5, phi);
			HalconAPI.StoreD(proc, 6, radius1);
			HalconAPI.StoreD(proc, 7, radius2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out minDist);
			err = HalconAPI.LoadD(proc, 1, err, out maxDist);
			err = HalconAPI.LoadD(proc, 2, err, out avgDist);
			err = HalconAPI.LoadD(proc, 3, err, out sigmaDist);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitEllipseContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int vossTabSize, int iterations, double clippingFactor, out HTuple row, out HTuple column, out HTuple phi, out HTuple radius1, out HTuple radius2, out HTuple startPhi, out HTuple endPhi, out HTuple pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(33);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, vossTabSize);
			HalconAPI.StoreI(proc, 5, iterations);
			HalconAPI.StoreD(proc, 6, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out column);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out phi);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out radius1);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out radius2);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out startPhi);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out endPhi);
			err = HTuple.LoadNew(proc, 7, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void FitEllipseContourXld(string algorithm, int maxNumPoints, double maxClosureDist, int clippingEndPoints, int vossTabSize, int iterations, double clippingFactor, out double row, out double column, out double phi, out double radius1, out double radius2, out double startPhi, out double endPhi, out string pointOrder)
		{
			IntPtr proc = HalconAPI.PreCall(33);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, algorithm);
			HalconAPI.StoreI(proc, 1, maxNumPoints);
			HalconAPI.StoreD(proc, 2, maxClosureDist);
			HalconAPI.StoreI(proc, 3, clippingEndPoints);
			HalconAPI.StoreI(proc, 4, vossTabSize);
			HalconAPI.StoreI(proc, 5, iterations);
			HalconAPI.StoreD(proc, 6, clippingFactor);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out row);
			err = HalconAPI.LoadD(proc, 1, err, out column);
			err = HalconAPI.LoadD(proc, 2, err, out phi);
			err = HalconAPI.LoadD(proc, 3, err, out radius1);
			err = HalconAPI.LoadD(proc, 4, err, out radius2);
			err = HalconAPI.LoadD(proc, 5, err, out startPhi);
			err = HalconAPI.LoadD(proc, 6, err, out endPhi);
			err = HalconAPI.LoadS(proc, 7, err, out pointOrder);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCircleContourXld(HTuple row, HTuple column, HTuple radius, HTuple startPhi, HTuple endPhi, HTuple pointOrder, double resolution)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(34);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.Store(proc, 3, startPhi);
			HalconAPI.Store(proc, 4, endPhi);
			HalconAPI.Store(proc, 5, pointOrder);
			HalconAPI.StoreD(proc, 6, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(startPhi);
			HalconAPI.UnpinTuple(endPhi);
			HalconAPI.UnpinTuple(pointOrder);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenCircleContourXld(double row, double column, double radius, double startPhi, double endPhi, string pointOrder, double resolution)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(34);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, startPhi);
			HalconAPI.StoreD(proc, 4, endPhi);
			HalconAPI.StoreS(proc, 5, pointOrder);
			HalconAPI.StoreD(proc, 6, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenEllipseContourXld(HTuple row, HTuple column, HTuple phi, HTuple radius1, HTuple radius2, HTuple startPhi, HTuple endPhi, HTuple pointOrder, double resolution)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(35);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.Store(proc, 2, phi);
			HalconAPI.Store(proc, 3, radius1);
			HalconAPI.Store(proc, 4, radius2);
			HalconAPI.Store(proc, 5, startPhi);
			HalconAPI.Store(proc, 6, endPhi);
			HalconAPI.Store(proc, 7, pointOrder);
			HalconAPI.StoreD(proc, 8, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(phi);
			HalconAPI.UnpinTuple(radius1);
			HalconAPI.UnpinTuple(radius2);
			HalconAPI.UnpinTuple(startPhi);
			HalconAPI.UnpinTuple(endPhi);
			HalconAPI.UnpinTuple(pointOrder);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenEllipseContourXld(double row, double column, double phi, double radius1, double radius2, double startPhi, double endPhi, string pointOrder, double resolution)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(35);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.StoreD(proc, 2, phi);
			HalconAPI.StoreD(proc, 3, radius1);
			HalconAPI.StoreD(proc, 4, radius2);
			HalconAPI.StoreD(proc, 5, startPhi);
			HalconAPI.StoreD(proc, 6, endPhi);
			HalconAPI.StoreS(proc, 7, pointOrder);
			HalconAPI.StoreD(proc, 8, resolution);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont AddNoiseWhiteContourXld(int numRegrPoints, double amp)
		{
			IntPtr proc = HalconAPI.PreCall(36);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numRegrPoints);
			HalconAPI.StoreD(proc, 1, amp);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDPoly GenPolygonsXld(string type, HTuple alpha)
		{
			IntPtr proc = HalconAPI.PreCall(45);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.Store(proc, 1, alpha);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(alpha);
			err = HXLDPoly.LoadNew(proc, 1, err, out HXLDPoly obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDPoly GenPolygonsXld(string type, double alpha)
		{
			IntPtr proc = HalconAPI.PreCall(45);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.StoreD(proc, 1, alpha);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDPoly.LoadNew(proc, 1, err, out HXLDPoly obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ProjectiveTransContourXld(HHomMat2D homMat2D)
		{
			IntPtr proc = HalconAPI.PreCall(47);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont AffineTransContourXld(HHomMat2D homMat2D)
		{
			IntPtr proc = HalconAPI.PreCall(49);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, homMat2D);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(homMat2D);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont CloseContoursXld()
		{
			IntPtr proc = HalconAPI.PreCall(50);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ClipEndPointsContoursXld(string mode, HTuple length)
		{
			IntPtr proc = HalconAPI.PreCall(51);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, length);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(length);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ClipEndPointsContoursXld(string mode, double length)
		{
			IntPtr proc = HalconAPI.PreCall(51);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreD(proc, 1, length);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ClipContoursXld(int row1, int column1, int row2, int column2)
		{
			IntPtr proc = HalconAPI.PreCall(52);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, row1);
			HalconAPI.StoreI(proc, 1, column1);
			HalconAPI.StoreI(proc, 2, row2);
			HalconAPI.StoreI(proc, 3, column2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont LocalMaxContoursXld(HImage image, HTuple minPercent, int minDiff, int distance)
		{
			IntPtr proc = HalconAPI.PreCall(53);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.Store(proc, 0, minPercent);
			HalconAPI.StoreI(proc, 1, minDiff);
			HalconAPI.StoreI(proc, 2, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(minPercent);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HXLDCont LocalMaxContoursXld(HImage image, int minPercent, int minDiff, int distance)
		{
			IntPtr proc = HalconAPI.PreCall(53);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, image);
			HalconAPI.StoreI(proc, 0, minPercent);
			HalconAPI.StoreI(proc, 1, minDiff);
			HalconAPI.StoreI(proc, 2, distance);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HXLDCont UnionStraightContoursHistoXld(out HXLDCont selectedContours, int refLineStartRow, int refLineStartColumn, int refLineEndRow, int refLineEndColumn, int width, int maxWidth, int filterSize, out HTuple histoValues)
		{
			IntPtr proc = HalconAPI.PreCall(54);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, refLineStartRow);
			HalconAPI.StoreI(proc, 1, refLineStartColumn);
			HalconAPI.StoreI(proc, 2, refLineEndRow);
			HalconAPI.StoreI(proc, 3, refLineEndColumn);
			HalconAPI.StoreI(proc, 4, width);
			HalconAPI.StoreI(proc, 5, maxWidth);
			HalconAPI.StoreI(proc, 6, filterSize);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			err = LoadNew(proc, 2, err, out selectedContours);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out histoValues);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont UnionStraightContoursXld(double maxDist, double maxDiff, double percent, string mode, HTuple iterations)
		{
			IntPtr proc = HalconAPI.PreCall(55);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxDist);
			HalconAPI.StoreD(proc, 1, maxDiff);
			HalconAPI.StoreD(proc, 2, percent);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.Store(proc, 4, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(iterations);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont UnionStraightContoursXld(double maxDist, double maxDiff, double percent, string mode, string iterations)
		{
			IntPtr proc = HalconAPI.PreCall(55);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxDist);
			HalconAPI.StoreD(proc, 1, maxDiff);
			HalconAPI.StoreD(proc, 2, percent);
			HalconAPI.StoreS(proc, 3, mode);
			HalconAPI.StoreS(proc, 4, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont UnionCollinearContoursExtXld(double maxDistAbs, double maxDistRel, double maxShift, double maxAngle, double maxOverlap, double maxRegrError, double maxCosts, double weightDist, double weightShift, double weightAngle, double weightLink, double weightRegr, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(56);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxDistAbs);
			HalconAPI.StoreD(proc, 1, maxDistRel);
			HalconAPI.StoreD(proc, 2, maxShift);
			HalconAPI.StoreD(proc, 3, maxAngle);
			HalconAPI.StoreD(proc, 4, maxOverlap);
			HalconAPI.StoreD(proc, 5, maxRegrError);
			HalconAPI.StoreD(proc, 6, maxCosts);
			HalconAPI.StoreD(proc, 7, weightDist);
			HalconAPI.StoreD(proc, 8, weightShift);
			HalconAPI.StoreD(proc, 9, weightAngle);
			HalconAPI.StoreD(proc, 10, weightLink);
			HalconAPI.StoreD(proc, 11, weightRegr);
			HalconAPI.StoreS(proc, 12, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont UnionCollinearContoursXld(double maxDistAbs, double maxDistRel, double maxShift, double maxAngle, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(57);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxDistAbs);
			HalconAPI.StoreD(proc, 1, maxDistRel);
			HalconAPI.StoreD(proc, 2, maxShift);
			HalconAPI.StoreD(proc, 3, maxAngle);
			HalconAPI.StoreS(proc, 4, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont UnionAdjacentContoursXld(double maxDistAbs, double maxDistRel, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(58);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, maxDistAbs);
			HalconAPI.StoreD(proc, 1, maxDistRel);
			HalconAPI.StoreS(proc, 2, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont SelectContoursXld(string feature, double min1, double max1, double min2, double max2)
		{
			IntPtr proc = HalconAPI.PreCall(59);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, feature);
			HalconAPI.StoreD(proc, 1, min1);
			HalconAPI.StoreD(proc, 2, max1);
			HalconAPI.StoreD(proc, 3, min2);
			HalconAPI.StoreD(proc, 4, max2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetRegressParamsXld(out HTuple nx, out HTuple ny, out HTuple dist, out HTuple fpx, out HTuple fpy, out HTuple lpx, out HTuple lpy, out HTuple mean, out HTuple deviation)
		{
			IntPtr proc = HalconAPI.PreCall(60);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out nx);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out ny);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out dist);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out fpx);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out fpy);
			err = HTuple.LoadNew(proc, 6, HTupleType.DOUBLE, err, out lpx);
			err = HTuple.LoadNew(proc, 7, HTupleType.DOUBLE, err, out lpy);
			err = HTuple.LoadNew(proc, 8, HTupleType.DOUBLE, err, out mean);
			err = HTuple.LoadNew(proc, 9, HTupleType.DOUBLE, err, out deviation);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HXLDCont RegressContoursXld(string mode, int iterations)
		{
			IntPtr proc = HalconAPI.PreCall(61);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreI(proc, 1, iterations);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetContourAngleXld(string angleMode, string calcMode, int lookaround)
		{
			IntPtr proc = HalconAPI.PreCall(62);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, angleMode);
			HalconAPI.StoreS(proc, 1, calcMode);
			HalconAPI.StoreI(proc, 2, lookaround);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HXLDCont SmoothContoursXld(int numRegrPoints)
		{
			IntPtr proc = HalconAPI.PreCall(63);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numRegrPoints);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple ContourPointNumXld()
		{
			IntPtr proc = HalconAPI.PreCall(64);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple QueryContourGlobalAttribsXld()
		{
			IntPtr proc = HalconAPI.PreCall(65);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetContourGlobalAttribXld(HTuple name)
		{
			IntPtr proc = HalconAPI.PreCall(66);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, name);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(name);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetContourGlobalAttribXld(string name)
		{
			IntPtr proc = HalconAPI.PreCall(66);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple QueryContourAttribsXld()
		{
			IntPtr proc = HalconAPI.PreCall(67);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetContourAttribXld(string name)
		{
			IntPtr proc = HalconAPI.PreCall(68);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void GetContourXld(out HTuple row, out HTuple col)
		{
			IntPtr proc = HalconAPI.PreCall(69);
			Store(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out row);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out col);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenContourPolygonRoundedXld(HTuple row, HTuple col, HTuple radius, HTuple samplingInterval)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(71);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.Store(proc, 3, samplingInterval);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(radius);
			HalconAPI.UnpinTuple(samplingInterval);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenContourPolygonRoundedXld(HTuple row, HTuple col, HTuple radius, double samplingInterval)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(71);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.Store(proc, 2, radius);
			HalconAPI.StoreD(proc, 3, samplingInterval);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			HalconAPI.UnpinTuple(radius);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GenContourPolygonXld(HTuple row, HTuple col)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(72);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, col);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(col);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HXLDCont ObjDiff(HXLDCont objectsSub)
		{
			IntPtr proc = HalconAPI.PreCall(573);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsSub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsSub);
			return obj;
		}

		public new HXLDCont CopyObj(int index, int numObj)
		{
			IntPtr proc = HalconAPI.PreCall(583);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.StoreI(proc, 1, numObj);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ConcatObj(HXLDCont objects2)
		{
			IntPtr proc = HalconAPI.PreCall(584);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return obj;
		}

		public new HXLDCont SelectObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDCont SelectObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public int CompareObj(HXLDCont objects2, HTuple epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.Store(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(epsilon);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public int CompareObj(HXLDCont objects2, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.StoreD(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public int TestEqualObj(HXLDCont objects2)
		{
			IntPtr proc = HalconAPI.PreCall(591);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public HRegion GenRegionContourXld(string mode)
		{
			IntPtr proc = HalconAPI.PreCall(597);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HShapeModel CreateAnisoShapeModelXld(HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleRMin, double scaleRMax, HTuple scaleRStep, double scaleCMin, double scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(935);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			err = HShapeModel.LoadNew(proc, 0, err, out HShapeModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HShapeModel CreateAnisoShapeModelXld(int numLevels, double angleStart, double angleExtent, double angleStep, double scaleRMin, double scaleRMax, double scaleRStep, double scaleCMin, double scaleCMax, double scaleCStep, string optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(935);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.StoreD(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.StoreD(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HShapeModel.LoadNew(proc, 0, err, out HShapeModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HShapeModel CreateScaledShapeModelXld(HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, double scaleMin, double scaleMax, HTuple scaleStep, HTuple optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(936);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.Store(proc, 6, scaleStep);
			HalconAPI.Store(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleStep);
			HalconAPI.UnpinTuple(optimization);
			err = HShapeModel.LoadNew(proc, 0, err, out HShapeModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HShapeModel CreateScaledShapeModelXld(int numLevels, double angleStart, double angleExtent, double angleStep, double scaleMin, double scaleMax, double scaleStep, string optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(936);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleMin);
			HalconAPI.StoreD(proc, 5, scaleMax);
			HalconAPI.StoreD(proc, 6, scaleStep);
			HalconAPI.StoreS(proc, 7, optimization);
			HalconAPI.StoreS(proc, 8, metric);
			HalconAPI.StoreI(proc, 9, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HShapeModel.LoadNew(proc, 0, err, out HShapeModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HShapeModel CreateShapeModelXld(HTuple numLevels, double angleStart, double angleExtent, HTuple angleStep, HTuple optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(937);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.Store(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(optimization);
			err = HShapeModel.LoadNew(proc, 0, err, out HShapeModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HShapeModel CreateShapeModelXld(int numLevels, double angleStart, double angleExtent, double angleStep, string optimization, string metric, int minContrast)
		{
			IntPtr proc = HalconAPI.PreCall(937);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.StoreD(proc, 1, angleStart);
			HalconAPI.StoreD(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreS(proc, 4, optimization);
			HalconAPI.StoreS(proc, 5, metric);
			HalconAPI.StoreI(proc, 6, minContrast);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HShapeModel.LoadNew(proc, 0, err, out HShapeModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HDeformableModel CreateLocalDeformableModelXld(HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(975);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HDeformableModel.LoadNew(proc, 0, err, out HDeformableModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HDeformableModel CreateLocalDeformableModelXld(int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(975);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HDeformableModel.LoadNew(proc, 0, err, out HDeformableModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HDeformableModel CreatePlanarCalibDeformableModelXld(HCamPar camParam, HPose referencePose, HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(976);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.Store(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.Store(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.Store(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.Store(proc, 11, scaleCStep);
			HalconAPI.Store(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HDeformableModel.LoadNew(proc, 0, err, out HDeformableModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HDeformableModel CreatePlanarCalibDeformableModelXld(HCamPar camParam, HPose referencePose, int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(976);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, camParam);
			HalconAPI.Store(proc, 1, referencePose);
			HalconAPI.StoreI(proc, 2, numLevels);
			HalconAPI.Store(proc, 3, angleStart);
			HalconAPI.Store(proc, 4, angleExtent);
			HalconAPI.StoreD(proc, 5, angleStep);
			HalconAPI.StoreD(proc, 6, scaleRMin);
			HalconAPI.Store(proc, 7, scaleRMax);
			HalconAPI.StoreD(proc, 8, scaleRStep);
			HalconAPI.StoreD(proc, 9, scaleCMin);
			HalconAPI.Store(proc, 10, scaleCMax);
			HalconAPI.StoreD(proc, 11, scaleCStep);
			HalconAPI.StoreS(proc, 12, optimization);
			HalconAPI.StoreS(proc, 13, metric);
			HalconAPI.StoreI(proc, 14, minContrast);
			HalconAPI.Store(proc, 15, genParamName);
			HalconAPI.Store(proc, 16, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParam);
			HalconAPI.UnpinTuple(referencePose);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HDeformableModel.LoadNew(proc, 0, err, out HDeformableModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HDeformableModel CreatePlanarUncalibDeformableModelXld(HTuple numLevels, HTuple angleStart, HTuple angleExtent, HTuple angleStep, double scaleRMin, HTuple scaleRMax, HTuple scaleRStep, double scaleCMin, HTuple scaleCMax, HTuple scaleCStep, HTuple optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(977);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.Store(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.Store(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.Store(proc, 9, scaleCStep);
			HalconAPI.Store(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(numLevels);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(angleStep);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleRStep);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(scaleCStep);
			HalconAPI.UnpinTuple(optimization);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HDeformableModel.LoadNew(proc, 0, err, out HDeformableModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HDeformableModel CreatePlanarUncalibDeformableModelXld(int numLevels, HTuple angleStart, HTuple angleExtent, double angleStep, double scaleRMin, HTuple scaleRMax, double scaleRStep, double scaleCMin, HTuple scaleCMax, double scaleCStep, string optimization, string metric, int minContrast, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(977);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, numLevels);
			HalconAPI.Store(proc, 1, angleStart);
			HalconAPI.Store(proc, 2, angleExtent);
			HalconAPI.StoreD(proc, 3, angleStep);
			HalconAPI.StoreD(proc, 4, scaleRMin);
			HalconAPI.Store(proc, 5, scaleRMax);
			HalconAPI.StoreD(proc, 6, scaleRStep);
			HalconAPI.StoreD(proc, 7, scaleCMin);
			HalconAPI.Store(proc, 8, scaleCMax);
			HalconAPI.StoreD(proc, 9, scaleCStep);
			HalconAPI.StoreS(proc, 10, optimization);
			HalconAPI.StoreS(proc, 11, metric);
			HalconAPI.StoreI(proc, 12, minContrast);
			HalconAPI.Store(proc, 13, genParamName);
			HalconAPI.Store(proc, 14, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(angleStart);
			HalconAPI.UnpinTuple(angleExtent);
			HalconAPI.UnpinTuple(scaleRMax);
			HalconAPI.UnpinTuple(scaleCMax);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HDeformableModel.LoadNew(proc, 0, err, out HDeformableModel obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLDCont meshes, int gridSpacing, HTuple rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.Store(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLDCont meshes, int gridSpacing, string rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.StoreS(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void DrawNurbsInterpMod(HWindow windowHandle, string rotate, string move, string scale, string keepRatio, string edit, int degree, HTuple rowsIn, HTuple colsIn, HTuple tangentsIn, out HTuple controlRows, out HTuple controlCols, out HTuple knots, out HTuple rows, out HTuple cols, out HTuple tangents)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1318);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreS(proc, 5, edit);
			HalconAPI.StoreI(proc, 6, degree);
			HalconAPI.Store(proc, 7, rowsIn);
			HalconAPI.Store(proc, 8, colsIn);
			HalconAPI.Store(proc, 9, tangentsIn);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowsIn);
			HalconAPI.UnpinTuple(colsIn);
			HalconAPI.UnpinTuple(tangentsIn);
			err = Load(proc, 1, err);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out controlRows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out controlCols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out knots);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out tangents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DrawNurbsInterp(HWindow windowHandle, string rotate, string move, string scale, string keepRatio, int degree, out HTuple controlRows, out HTuple controlCols, out HTuple knots, out HTuple rows, out HTuple cols, out HTuple tangents)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1319);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreI(proc, 5, degree);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out controlRows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out controlCols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out knots);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 5, HTupleType.DOUBLE, err, out tangents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DrawNurbsMod(HWindow windowHandle, string rotate, string move, string scale, string keepRatio, string edit, int degree, HTuple rowsIn, HTuple colsIn, HTuple weightsIn, out HTuple rows, out HTuple cols, out HTuple weights)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1320);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreS(proc, 5, edit);
			HalconAPI.StoreI(proc, 6, degree);
			HalconAPI.Store(proc, 7, rowsIn);
			HalconAPI.Store(proc, 8, colsIn);
			HalconAPI.Store(proc, 9, weightsIn);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rowsIn);
			HalconAPI.UnpinTuple(colsIn);
			HalconAPI.UnpinTuple(weightsIn);
			err = Load(proc, 1, err);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out weights);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public void DrawNurbs(HWindow windowHandle, string rotate, string move, string scale, string keepRatio, int degree, out HTuple rows, out HTuple cols, out HTuple weights)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1321);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreI(proc, 5, degree);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out rows);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cols);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out weights);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public HXLDCont DrawXldMod(HWindow windowHandle, string rotate, string move, string scale, string keepRatio, string edit)
		{
			IntPtr proc = HalconAPI.PreCall(1322);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.StoreS(proc, 5, edit);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
			return obj;
		}

		public void DrawXld(HWindow windowHandle, string rotate, string move, string scale, string keepRatio)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1323);
			HalconAPI.Store(proc, 0, windowHandle);
			HalconAPI.StoreS(proc, 1, rotate);
			HalconAPI.StoreS(proc, 2, move);
			HalconAPI.StoreS(proc, 3, scale);
			HalconAPI.StoreS(proc, 4, keepRatio);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(windowHandle);
		}

		public HXLDCont DistanceContoursXld(HXLDCont contourTo, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1361);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, contourTo);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contourTo);
			return obj;
		}

		public HTuple DistanceCcMin(HXLDCont contour2, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(1362);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, contour2);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour2);
			return tuple;
		}

		public void DistanceCc(HXLDCont contour2, string mode, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1363);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, contour2);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour2);
		}

		public void DistanceCc(HXLDCont contour2, string mode, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1363);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, contour2);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour2);
		}

		public void DistanceSc(HTuple row1, HTuple column1, HTuple row2, HTuple column2, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1364);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistanceSc(double row1, double column1, double row2, double column2, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1364);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistanceLc(HTuple row1, HTuple column1, HTuple row2, HTuple column2, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1365);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row1);
			HalconAPI.Store(proc, 1, column1);
			HalconAPI.Store(proc, 2, row2);
			HalconAPI.Store(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row1);
			HalconAPI.UnpinTuple(column1);
			HalconAPI.UnpinTuple(row2);
			HalconAPI.UnpinTuple(column2);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistanceLc(double row1, double column1, double row2, double column2, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1365);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row1);
			HalconAPI.StoreD(proc, 1, column1);
			HalconAPI.StoreD(proc, 2, row2);
			HalconAPI.StoreD(proc, 3, column2);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistancePc(HTuple row, HTuple column, out HTuple distanceMin, out HTuple distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1366);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out distanceMin);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void DistancePc(double row, double column, out double distanceMin, out double distanceMax)
		{
			IntPtr proc = HalconAPI.PreCall(1366);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out distanceMin);
			err = HalconAPI.LoadD(proc, 1, err, out distanceMax);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple ReadContourXldDxf(string fileName, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1636);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 1, err);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public string ReadContourXldDxf(string fileName, string genParamName, double genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1636);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 1, err);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return stringValue;
		}

		public void WriteContourXldDxf(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1637);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public new HXLDCont SelectXldPoint(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDCont SelectXldPoint(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDCont SelectShapeXld(HTuple features, string operation, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.Store(proc, 2, min);
			HalconAPI.Store(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDCont SelectShapeXld(string features, string operation, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.StoreD(proc, 2, min);
			HalconAPI.StoreD(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDCont ShapeTransXld(string type)
		{
			IntPtr proc = HalconAPI.PreCall(1689);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont RadialDistortionSelfCalibration(int width, int height, double inlierThreshold, int randSeed, string distortionModel, string distortionCenter, double principalPointVar, out HCamPar cameraParam)
		{
			IntPtr proc = HalconAPI.PreCall(1904);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, width);
			HalconAPI.StoreI(proc, 1, height);
			HalconAPI.StoreD(proc, 2, inlierThreshold);
			HalconAPI.StoreI(proc, 3, randSeed);
			HalconAPI.StoreS(proc, 4, distortionModel);
			HalconAPI.StoreS(proc, 5, distortionCenter);
			HalconAPI.StoreD(proc, 6, principalPointVar);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			err = HCamPar.LoadNew(proc, 0, err, out cameraParam);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ContourToWorldPlaneXld(HTuple cameraParam, HPose worldPose, HTuple scale)
		{
			IntPtr proc = HalconAPI.PreCall(1915);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.Store(proc, 2, scale);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(worldPose);
			HalconAPI.UnpinTuple(scale);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ContourToWorldPlaneXld(HTuple cameraParam, HPose worldPose, string scale)
		{
			IntPtr proc = HalconAPI.PreCall(1915);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, cameraParam);
			HalconAPI.Store(proc, 1, worldPose);
			HalconAPI.StoreS(proc, 2, scale);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.UnpinTuple(worldPose);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ChangeRadialDistortionContoursXld(HCamPar camParamIn, HCamPar camParamOut)
		{
			IntPtr proc = HalconAPI.PreCall(1922);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, camParamIn);
			HalconAPI.Store(proc, 1, camParamOut);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(camParamIn);
			HalconAPI.UnpinTuple(camParamOut);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple DistanceCcMinPoints(HXLDCont contour2, string mode, out HTuple row1, out HTuple column1, out HTuple row2, out HTuple column2)
		{
			IntPtr proc = HalconAPI.PreCall(2111);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, contour2);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out row1);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out column1);
			err = HTuple.LoadNew(proc, 3, HTupleType.DOUBLE, err, out row2);
			err = HTuple.LoadNew(proc, 4, HTupleType.DOUBLE, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour2);
			return tuple;
		}

		public double DistanceCcMinPoints(HXLDCont contour2, string mode, out double row1, out double column1, out double row2, out double column2)
		{
			IntPtr proc = HalconAPI.PreCall(2111);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, contour2);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out row1);
			err = HalconAPI.LoadD(proc, 2, err, out column1);
			err = HalconAPI.LoadD(proc, 3, err, out row2);
			err = HalconAPI.LoadD(proc, 4, err, out column2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(contour2);
			return doubleValue;
		}

		public HXLDCont InsertObj(HXLDCont objectsInsert, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2121);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsInsert);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsInsert);
			return obj;
		}

		public new HXLDCont RemoveObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDCont RemoveObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDCont ReplaceObj(HXLDCont objectsReplace, HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return obj;
		}

		public HXLDCont ReplaceObj(HXLDCont objectsReplace, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return obj;
		}
	}
}
