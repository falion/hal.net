using System;
using System.Windows.Input;

namespace HalconDotNet
{
	public class HMouseEventArgsWPF : EventArgs
	{
		private readonly double x;

		private readonly double y;

		private readonly double row;

		private readonly double column;

		private readonly int delta;

		private readonly MouseButton? button;

		public double X => x;

		public double Y => y;

		public double Row => row;

		public double Column => column;

		public int Delta => delta;

		public MouseButton? Button => button;

		internal HMouseEventArgsWPF(double x, double y, double row, double column, int delta, MouseButton? button)
		{
			this.x = x;
			this.y = y;
			this.row = row;
			this.column = column;
			this.delta = delta;
			this.button = button;
		}
	}
}
