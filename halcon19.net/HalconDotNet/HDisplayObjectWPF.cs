using System;
using System.Windows;
using System.Windows.Controls;

namespace HalconDotNet
{
	public abstract class HDisplayObjectWPF : FrameworkElement, IDisposable
	{
		protected bool disposed;

		protected HSmartWindowControlWPF ParentHSmartWindowControlWPF => ParentItemsControl as HSmartWindowControlWPF;

		internal ItemsControl ParentItemsControl => ItemsControl.ItemsControlFromItemContainer(this);

		public HDisplayObjectWPF()
		{
		}

		public abstract void Display(HWindow hWindow);

		public void Dispose()
		{
			Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				disposed = true;
			}
		}
	}
}
