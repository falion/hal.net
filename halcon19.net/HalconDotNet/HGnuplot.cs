using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HGnuplot : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HGnuplot()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HGnuplot(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HGnuplot(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("gnuplot");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HGnuplot obj)
		{
			obj = new HGnuplot(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HGnuplot[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HGnuplot[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HGnuplot(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public void GnuplotPlotFunct1d(HFunction1D function)
		{
			IntPtr proc = HalconAPI.PreCall(1295);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, function);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(function);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GnuplotPlotCtrl(HTuple values)
		{
			IntPtr proc = HalconAPI.PreCall(1296);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, values);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(values);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GnuplotPlotImage(HImage image, int samplesX, int samplesY, HTuple viewRotX, HTuple viewRotZ, string hidden3D)
		{
			IntPtr proc = HalconAPI.PreCall(1297);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 1, samplesX);
			HalconAPI.StoreI(proc, 2, samplesY);
			HalconAPI.Store(proc, 3, viewRotX);
			HalconAPI.Store(proc, 4, viewRotZ);
			HalconAPI.StoreS(proc, 5, hidden3D);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(viewRotX);
			HalconAPI.UnpinTuple(viewRotZ);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void GnuplotPlotImage(HImage image, int samplesX, int samplesY, double viewRotX, double viewRotZ, string hidden3D)
		{
			IntPtr proc = HalconAPI.PreCall(1297);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 1, samplesX);
			HalconAPI.StoreI(proc, 2, samplesY);
			HalconAPI.StoreD(proc, 3, viewRotX);
			HalconAPI.StoreD(proc, 4, viewRotZ);
			HalconAPI.StoreS(proc, 5, hidden3D);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void GnuplotClose()
		{
			IntPtr proc = HalconAPI.PreCall(1298);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GnuplotOpenFile(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1299);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GnuplotOpenPipe()
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1300);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
