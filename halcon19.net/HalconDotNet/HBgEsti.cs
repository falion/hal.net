using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HBgEsti : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBgEsti()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBgEsti(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HBgEsti(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("bg_estimation");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HBgEsti obj)
		{
			obj = new HBgEsti(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HBgEsti[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HBgEsti[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HBgEsti(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HBgEsti(HImage initializeImage, double syspar1, double syspar2, string gainMode, double gain1, double gain2, string adaptMode, double minDiff, int statNum, double confidenceC, double timeC)
		{
			IntPtr proc = HalconAPI.PreCall(2008);
			HalconAPI.Store(proc, 1, initializeImage);
			HalconAPI.StoreD(proc, 0, syspar1);
			HalconAPI.StoreD(proc, 1, syspar2);
			HalconAPI.StoreS(proc, 2, gainMode);
			HalconAPI.StoreD(proc, 3, gain1);
			HalconAPI.StoreD(proc, 4, gain2);
			HalconAPI.StoreS(proc, 5, adaptMode);
			HalconAPI.StoreD(proc, 6, minDiff);
			HalconAPI.StoreI(proc, 7, statNum);
			HalconAPI.StoreD(proc, 8, confidenceC);
			HalconAPI.StoreD(proc, 9, timeC);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(initializeImage);
		}

		public void CloseBgEsti()
		{
			IntPtr proc = HalconAPI.PreCall(2002);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HImage GiveBgEsti()
		{
			IntPtr proc = HalconAPI.PreCall(2003);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void UpdateBgEsti(HImage presentImage, HRegion upDateRegion)
		{
			IntPtr proc = HalconAPI.PreCall(2004);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, presentImage);
			HalconAPI.Store(proc, 2, upDateRegion);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(presentImage);
			GC.KeepAlive(upDateRegion);
		}

		public HRegion RunBgEsti(HImage presentImage)
		{
			IntPtr proc = HalconAPI.PreCall(2005);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, presentImage);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(presentImage);
			return obj;
		}

		public double GetBgEstiParams(out double syspar2, out string gainMode, out double gain1, out double gain2, out string adaptMode, out double minDiff, out int statNum, out double confidenceC, out double timeC)
		{
			IntPtr proc = HalconAPI.PreCall(2006);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			HalconAPI.InitOCT(proc, 6);
			HalconAPI.InitOCT(proc, 7);
			HalconAPI.InitOCT(proc, 8);
			HalconAPI.InitOCT(proc, 9);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HalconAPI.LoadD(proc, 1, err, out syspar2);
			err = HalconAPI.LoadS(proc, 2, err, out gainMode);
			err = HalconAPI.LoadD(proc, 3, err, out gain1);
			err = HalconAPI.LoadD(proc, 4, err, out gain2);
			err = HalconAPI.LoadS(proc, 5, err, out adaptMode);
			err = HalconAPI.LoadD(proc, 6, err, out minDiff);
			err = HalconAPI.LoadI(proc, 7, err, out statNum);
			err = HalconAPI.LoadD(proc, 8, err, out confidenceC);
			err = HalconAPI.LoadD(proc, 9, err, out timeC);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public void SetBgEstiParams(double syspar1, double syspar2, string gainMode, double gain1, double gain2, string adaptMode, double minDiff, int statNum, double confidenceC, double timeC)
		{
			IntPtr proc = HalconAPI.PreCall(2007);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, syspar1);
			HalconAPI.StoreD(proc, 2, syspar2);
			HalconAPI.StoreS(proc, 3, gainMode);
			HalconAPI.StoreD(proc, 4, gain1);
			HalconAPI.StoreD(proc, 5, gain2);
			HalconAPI.StoreS(proc, 6, adaptMode);
			HalconAPI.StoreD(proc, 7, minDiff);
			HalconAPI.StoreI(proc, 8, statNum);
			HalconAPI.StoreD(proc, 9, confidenceC);
			HalconAPI.StoreD(proc, 10, timeC);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateBgEsti(HImage initializeImage, double syspar1, double syspar2, string gainMode, double gain1, double gain2, string adaptMode, double minDiff, int statNum, double confidenceC, double timeC)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2008);
			HalconAPI.Store(proc, 1, initializeImage);
			HalconAPI.StoreD(proc, 0, syspar1);
			HalconAPI.StoreD(proc, 1, syspar2);
			HalconAPI.StoreS(proc, 2, gainMode);
			HalconAPI.StoreD(proc, 3, gain1);
			HalconAPI.StoreD(proc, 4, gain2);
			HalconAPI.StoreS(proc, 5, adaptMode);
			HalconAPI.StoreD(proc, 6, minDiff);
			HalconAPI.StoreI(proc, 7, statNum);
			HalconAPI.StoreD(proc, 8, confidenceC);
			HalconAPI.StoreD(proc, 9, timeC);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(initializeImage);
		}
	}
}
