using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms.Design;

namespace HalconDotNet
{
	public class HWindowControlDesigner : ControlDesigner
	{
		private HWindowControl windowControl;

		public Bitmap LayoutBitmap
		{
			get
			{
				return null;
			}
			set
			{
				if (value != null)
				{
					HalconWindowLayoutDialog halconWindowLayoutDialog = new HalconWindowLayoutDialog(value.Size);
					halconWindowLayoutDialog.ShowDialog();
					if (!halconWindowLayoutDialog.resultCancel)
					{
						windowControl.WindowSize = new Size(value.Size.Width * halconWindowLayoutDialog.resultPercent / 100, value.Size.Height * halconWindowLayoutDialog.resultPercent / 100);
						windowControl.ImagePart = new Rectangle(Point.Empty, value.Size);
					}
				}
			}
		}

		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
			windowControl = (HWindowControl)component;
		}

		protected override void PreFilterProperties(IDictionary properties)
		{
			base.PreFilterProperties(properties);
			properties["LayoutBitmap"] = TypeDescriptor.CreateProperty(attributes: new Attribute[3]
			{
				CategoryAttribute.Layout,
				DesignOnlyAttribute.Yes,
				new DescriptionAttribute("This design-time property allows you to configure Size and ImagePart by providing a reference image of the desired size.")
			}, componentType: typeof(HWindowControlDesigner), name: "LayoutBitmap", type: typeof(Bitmap));
			properties.Remove("BorderStyle");
		}
	}
}
