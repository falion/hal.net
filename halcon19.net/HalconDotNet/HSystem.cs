using System;

namespace HalconDotNet
{
	public class HSystem
	{
		public static void WaitSeconds(double seconds)
		{
			IntPtr proc = HalconAPI.PreCall(315);
			HalconAPI.StoreD(proc, 0, seconds);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void SystemCall(string command)
		{
			IntPtr proc = HalconAPI.PreCall(316);
			HalconAPI.StoreS(proc, 0, command);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void SetSystem(HTuple systemParameter, HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(317);
			HalconAPI.Store(proc, 0, systemParameter);
			HalconAPI.Store(proc, 1, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(systemParameter);
			HalconAPI.UnpinTuple(value);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void SetSystem(string systemParameter, string value)
		{
			IntPtr proc = HalconAPI.PreCall(317);
			HalconAPI.StoreS(proc, 0, systemParameter);
			HalconAPI.StoreS(proc, 1, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void SetCheck(HTuple check)
		{
			IntPtr proc = HalconAPI.PreCall(318);
			HalconAPI.Store(proc, 0, check);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(check);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void SetCheck(string check)
		{
			IntPtr proc = HalconAPI.PreCall(318);
			HalconAPI.StoreS(proc, 0, check);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void ResetObjDb(int defaultImageWidth, int defaultImageHeight, int defaultChannels)
		{
			IntPtr proc = HalconAPI.PreCall(319);
			HalconAPI.StoreI(proc, 0, defaultImageWidth);
			HalconAPI.StoreI(proc, 1, defaultImageHeight);
			HalconAPI.StoreI(proc, 2, defaultChannels);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static HTuple GetSystem(HTuple query)
		{
			IntPtr proc = HalconAPI.PreCall(320);
			HalconAPI.Store(proc, 0, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(query);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple GetSystem(string query)
		{
			IntPtr proc = HalconAPI.PreCall(320);
			HalconAPI.StoreS(proc, 0, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple GetCheck()
		{
			IntPtr proc = HalconAPI.PreCall(321);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static string GetErrorText(int errorCode)
		{
			IntPtr proc = HalconAPI.PreCall(322);
			HalconAPI.StoreI(proc, 0, errorCode);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			return stringValue;
		}

		public static double CountSeconds()
		{
			IntPtr proc = HalconAPI.PreCall(323);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			return doubleValue;
		}

		public static int CountRelation(string relationName)
		{
			IntPtr proc = HalconAPI.PreCall(324);
			HalconAPI.StoreS(proc, 0, relationName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			return intValue;
		}

		public static string GetExtendedErrorInfo(out int errorCode, out string errorMessage)
		{
			IntPtr proc = HalconAPI.PreCall(344);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadI(proc, 1, err, out errorCode);
			err = HalconAPI.LoadS(proc, 2, err, out errorMessage);
			HalconAPI.PostCall(proc, err);
			return stringValue;
		}

		public static HTuple GetModules(out int moduleKey)
		{
			IntPtr proc = HalconAPI.PreCall(345);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HalconAPI.LoadI(proc, 1, err, out moduleKey);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple QuerySpy(out HTuple values)
		{
			IntPtr proc = HalconAPI.PreCall(371);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out values);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static void SetSpy(string classVal, HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(372);
			HalconAPI.StoreS(proc, 0, classVal);
			HalconAPI.Store(proc, 1, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(value);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void SetSpy(string classVal, string value)
		{
			IntPtr proc = HalconAPI.PreCall(372);
			HalconAPI.StoreS(proc, 0, classVal);
			HalconAPI.StoreS(proc, 1, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static HTuple GetSpy(string classVal)
		{
			IntPtr proc = HalconAPI.PreCall(373);
			HalconAPI.StoreS(proc, 0, classVal);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static void SetAopInfo(HTuple operatorName, HTuple indexName, HTuple indexValue, string infoName, HTuple infoValue)
		{
			IntPtr proc = HalconAPI.PreCall(566);
			HalconAPI.Store(proc, 0, operatorName);
			HalconAPI.Store(proc, 1, indexName);
			HalconAPI.Store(proc, 2, indexValue);
			HalconAPI.StoreS(proc, 3, infoName);
			HalconAPI.Store(proc, 4, infoValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(operatorName);
			HalconAPI.UnpinTuple(indexName);
			HalconAPI.UnpinTuple(indexValue);
			HalconAPI.UnpinTuple(infoValue);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void SetAopInfo(string operatorName, string indexName, string indexValue, string infoName, int infoValue)
		{
			IntPtr proc = HalconAPI.PreCall(566);
			HalconAPI.StoreS(proc, 0, operatorName);
			HalconAPI.StoreS(proc, 1, indexName);
			HalconAPI.StoreS(proc, 2, indexValue);
			HalconAPI.StoreS(proc, 3, infoName);
			HalconAPI.StoreI(proc, 4, infoValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static HTuple GetAopInfo(HTuple operatorName, HTuple indexName, HTuple indexValue, string infoName)
		{
			IntPtr proc = HalconAPI.PreCall(567);
			HalconAPI.Store(proc, 0, operatorName);
			HalconAPI.Store(proc, 1, indexName);
			HalconAPI.Store(proc, 2, indexValue);
			HalconAPI.StoreS(proc, 3, infoName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(operatorName);
			HalconAPI.UnpinTuple(indexName);
			HalconAPI.UnpinTuple(indexValue);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static string GetAopInfo(string operatorName, HTuple indexName, HTuple indexValue, string infoName)
		{
			IntPtr proc = HalconAPI.PreCall(567);
			HalconAPI.StoreS(proc, 0, operatorName);
			HalconAPI.Store(proc, 1, indexName);
			HalconAPI.Store(proc, 2, indexValue);
			HalconAPI.StoreS(proc, 3, infoName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(indexName);
			HalconAPI.UnpinTuple(indexValue);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			return stringValue;
		}

		public static HTuple QueryAopInfo(HTuple operatorName, HTuple indexName, HTuple indexValue, out HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(568);
			HalconAPI.Store(proc, 0, operatorName);
			HalconAPI.Store(proc, 1, indexName);
			HalconAPI.Store(proc, 2, indexValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(operatorName);
			HalconAPI.UnpinTuple(indexName);
			HalconAPI.UnpinTuple(indexValue);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out value);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple QueryAopInfo(string operatorName, string indexName, string indexValue, out HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(568);
			HalconAPI.StoreS(proc, 0, operatorName);
			HalconAPI.StoreS(proc, 1, indexName);
			HalconAPI.StoreS(proc, 2, indexValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out value);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static void OptimizeAop(HTuple operatorName, HTuple iconicType, HTuple fileName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(569);
			HalconAPI.Store(proc, 0, operatorName);
			HalconAPI.Store(proc, 1, iconicType);
			HalconAPI.Store(proc, 2, fileName);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(operatorName);
			HalconAPI.UnpinTuple(iconicType);
			HalconAPI.UnpinTuple(fileName);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void OptimizeAop(string operatorName, string iconicType, string fileName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(569);
			HalconAPI.StoreS(proc, 0, operatorName);
			HalconAPI.StoreS(proc, 1, iconicType);
			HalconAPI.StoreS(proc, 2, fileName);
			HalconAPI.Store(proc, 3, genParamName);
			HalconAPI.Store(proc, 4, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void WriteAopKnowledge(string fileName, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(570);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void WriteAopKnowledge(string fileName, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(570);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static HTuple ReadAopKnowledge(string fileName, HTuple genParamName, HTuple genParamValue, out HTuple operatorNames)
		{
			IntPtr proc = HalconAPI.PreCall(571);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, err, out operatorNames);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple ReadAopKnowledge(string fileName, string genParamName, string genParamValue, out string operatorNames)
		{
			IntPtr proc = HalconAPI.PreCall(571);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			err = HalconAPI.LoadS(proc, 1, err, out operatorNames);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static void SetWindowType(string windowType)
		{
			IntPtr proc = HalconAPI.PreCall(1173);
			HalconAPI.StoreS(proc, 0, windowType);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static HTuple GetWindowAttr(string attributeName)
		{
			IntPtr proc = HalconAPI.PreCall(1175);
			HalconAPI.StoreS(proc, 0, attributeName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static void SetWindowAttr(string attributeName, HTuple attributeValue)
		{
			IntPtr proc = HalconAPI.PreCall(1176);
			HalconAPI.StoreS(proc, 0, attributeName);
			HalconAPI.Store(proc, 1, attributeValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attributeValue);
			HalconAPI.PostCall(proc, procResult);
		}

		public static void SetWindowAttr(string attributeName, string attributeValue)
		{
			IntPtr proc = HalconAPI.PreCall(1176);
			HalconAPI.StoreS(proc, 0, attributeName);
			HalconAPI.StoreS(proc, 1, attributeValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}

		public static HTuple QueryWindowType()
		{
			IntPtr proc = HalconAPI.PreCall(1177);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static int GetCurrentHthreadId()
		{
			IntPtr proc = HalconAPI.PreCall(2152);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			return intValue;
		}

		public static HTuple GetSystemInfo(HTuple query)
		{
			IntPtr proc = HalconAPI.PreCall(2160);
			HalconAPI.Store(proc, 0, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(query);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static HTuple GetSystemInfo(string query)
		{
			IntPtr proc = HalconAPI.PreCall(2160);
			HalconAPI.StoreS(proc, 0, query);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			return tuple;
		}

		public static void InterruptOperator(int HThreadID, string mode)
		{
			IntPtr proc = HalconAPI.PreCall(2161);
			HalconAPI.StoreI(proc, 0, HThreadID);
			HalconAPI.StoreS(proc, 1, mode);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
		}
	}
}
