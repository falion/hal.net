namespace HalconDotNet
{
	internal class HTupleElementsDouble : HTupleElementsImplementation
	{
		internal HTupleElementsDouble(HTupleDouble source, int index)
			: base(source, index)
		{
		}

		internal HTupleElementsDouble(HTupleDouble source, int[] indices)
			: base(source, indices)
		{
		}

		public override double[] getD()
		{
			if (indices == null)
			{
				return null;
			}
			double[] array = new double[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				array[i] = source.DArr[indices[i]];
			}
			return array;
		}

		public override void setD(double[] d)
		{
			if (IsValidArrayForSetX(d))
			{
				bool flag = d.Length == 1;
				for (int i = 0; i < indices.Length; i++)
				{
					source.DArr[indices[i]] = d[(!flag) ? i : 0];
				}
			}
		}

		public override object[] getO()
		{
			if (indices == null)
			{
				return null;
			}
			object[] array = new object[indices.Length];
			for (int i = 0; i < indices.Length; i++)
			{
				array[i] = source.DArr[indices[i]];
			}
			return array;
		}

		public override HTupleType getType()
		{
			return HTupleType.DOUBLE;
		}
	}
}
