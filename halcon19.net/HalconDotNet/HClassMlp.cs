using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HClassMlp : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassMlp()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassMlp(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassMlp(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("class_mlp");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassMlp obj)
		{
			obj = new HClassMlp(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HClassMlp[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HClassMlp[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HClassMlp(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HClassMlp(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1867);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HClassMlp(int numInput, int numHidden, int numOutput, string outputFunction, string preprocessing, int numComponents, int randSeed)
		{
			IntPtr proc = HalconAPI.PreCall(1883);
			HalconAPI.StoreI(proc, 0, numInput);
			HalconAPI.StoreI(proc, 1, numHidden);
			HalconAPI.StoreI(proc, 2, numOutput);
			HalconAPI.StoreS(proc, 3, outputFunction);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeClassMlp();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HClassMlp(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeClassMlp(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeClassMlp();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HClassMlp Deserialize(Stream stream)
		{
			HClassMlp hClassMlp = new HClassMlp();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hClassMlp.DeserializeClassMlp(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassMlp;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HClassMlp Clone()
		{
			HSerializedItem hSerializedItem = SerializeClassMlp();
			HClassMlp hClassMlp = new HClassMlp();
			hClassMlp.DeserializeClassMlp(hSerializedItem);
			hSerializedItem.Dispose();
			return hClassMlp;
		}

		public HRegion ClassifyImageClassMlp(HImage image, double rejectionThreshold)
		{
			IntPtr proc = HalconAPI.PreCall(435);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreD(proc, 1, rejectionThreshold);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HRegion.LoadNew(proc, 1, err, out HRegion obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void AddSamplesImageClassMlp(HImage image, HRegion classRegions)
		{
			IntPtr proc = HalconAPI.PreCall(436);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 2, classRegions);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			GC.KeepAlive(classRegions);
		}

		public HClassTrainData GetClassTrainDataMlp()
		{
			IntPtr proc = HalconAPI.PreCall(1787);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HClassTrainData.LoadNew(proc, 0, err, out HClassTrainData obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void AddClassTrainDataMlp(HClassTrainData classTrainDataHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1788);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, classTrainDataHandle);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
		}

		public HTuple SelectFeatureSetMlp(HClassTrainData classTrainDataHandle, string selectionMethod, HTuple genParamName, HTuple genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1799);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.Store(proc, 2, genParamName);
			HalconAPI.Store(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return tuple;
		}

		public HTuple SelectFeatureSetMlp(HClassTrainData classTrainDataHandle, string selectionMethod, string genParamName, double genParamValue, out HTuple score)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1799);
			HalconAPI.Store(proc, 0, classTrainDataHandle);
			HalconAPI.StoreS(proc, 1, selectionMethod);
			HalconAPI.StoreS(proc, 2, genParamName);
			HalconAPI.StoreD(proc, 3, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			err = HTuple.LoadNew(proc, 1, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 2, HTupleType.DOUBLE, err, out score);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classTrainDataHandle);
			return tuple;
		}

		public HClassLUT CreateClassLutMlp(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1822);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HClassLUT.LoadNew(proc, 0, err, out HClassLUT obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public static void ClearClassMlp(HClassMlp[] MLPHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(MLPHandle);
			IntPtr proc = HalconAPI.PreCall(1863);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(MLPHandle);
		}

		public void ClearClassMlp()
		{
			IntPtr proc = HalconAPI.PreCall(1863);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void ClearSamplesClassMlp(HClassMlp[] MLPHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(MLPHandle);
			IntPtr proc = HalconAPI.PreCall(1864);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(MLPHandle);
		}

		public void ClearSamplesClassMlp()
		{
			IntPtr proc = HalconAPI.PreCall(1864);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeClassMlp(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1865);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeClassMlp()
		{
			IntPtr proc = HalconAPI.PreCall(1866);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadClassMlp(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1867);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteClassMlp(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1868);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ReadSamplesClassMlp(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1869);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void WriteSamplesClassMlp(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1870);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple ClassifyClassMlp(HTuple features, HTuple num, out HTuple confidence)
		{
			IntPtr proc = HalconAPI.PreCall(1871);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.Store(proc, 2, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(num);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int ClassifyClassMlp(HTuple features, HTuple num, out double confidence)
		{
			IntPtr proc = HalconAPI.PreCall(1871);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.Store(proc, 2, num);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(num);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadD(proc, 1, err, out confidence);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple EvaluateClassMlp(HTuple features)
		{
			IntPtr proc = HalconAPI.PreCall(1872);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double TrainClassMlp(int maxIterations, double weightTolerance, double errorTolerance, out HTuple errorLog)
		{
			IntPtr proc = HalconAPI.PreCall(1873);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, maxIterations);
			HalconAPI.StoreD(proc, 2, weightTolerance);
			HalconAPI.StoreD(proc, 3, errorTolerance);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out errorLog);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HTuple GetPrepInfoClassMlp(string preprocessing, out HTuple cumInformationCont)
		{
			IntPtr proc = HalconAPI.PreCall(1874);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, preprocessing);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out cumInformationCont);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public int GetSampleNumClassMlp()
		{
			IntPtr proc = HalconAPI.PreCall(1875);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple GetSampleClassMlp(int indexSample, out HTuple target)
		{
			IntPtr proc = HalconAPI.PreCall(1876);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, indexSample);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.DOUBLE, err, out target);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetRejectionParamsClassMlp(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1877);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetRejectionParamsClassMlp(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1877);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetRejectionParamsClassMlp(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1878);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetRejectionParamsClassMlp(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1878);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AddSampleClassMlp(HTuple features, HTuple target)
		{
			IntPtr proc = HalconAPI.PreCall(1879);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.Store(proc, 2, target);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(target);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void AddSampleClassMlp(HTuple features, int target)
		{
			IntPtr proc = HalconAPI.PreCall(1879);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, features);
			HalconAPI.StoreI(proc, 2, target);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetRegularizationParamsClassMlp(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1880);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetRegularizationParamsClassMlp(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1881);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetRegularizationParamsClassMlp(string genParamName, double genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1881);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreD(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int GetParamsClassMlp(out int numHidden, out int numOutput, out string outputFunction, out string preprocessing, out int numComponents)
		{
			IntPtr proc = HalconAPI.PreCall(1882);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 4);
			HalconAPI.InitOCT(proc, 5);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			err = HalconAPI.LoadI(proc, 1, err, out numHidden);
			err = HalconAPI.LoadI(proc, 2, err, out numOutput);
			err = HalconAPI.LoadS(proc, 3, err, out outputFunction);
			err = HalconAPI.LoadS(proc, 4, err, out preprocessing);
			err = HalconAPI.LoadI(proc, 5, err, out numComponents);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public void CreateClassMlp(int numInput, int numHidden, int numOutput, string outputFunction, string preprocessing, int numComponents, int randSeed)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1883);
			HalconAPI.StoreI(proc, 0, numInput);
			HalconAPI.StoreI(proc, 1, numHidden);
			HalconAPI.StoreI(proc, 2, numOutput);
			HalconAPI.StoreS(proc, 3, outputFunction);
			HalconAPI.StoreS(proc, 4, preprocessing);
			HalconAPI.StoreI(proc, 5, numComponents);
			HalconAPI.StoreI(proc, 6, randSeed);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
