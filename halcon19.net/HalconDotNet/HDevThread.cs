using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace HalconDotNet
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class HDevThread : IDisposable
	{
		public delegate void ProcCallback(HDevThread devThread);

		private IntPtr mThreadHandle;

		private bool mDirectCall;

		private HDevInputParam[] mParamsInput;

		private HDevOutputParam[] mParamsOutput;

		private GCHandle mSelfRef;

		private HalconAPI.HDevThreadInternalCallback mInternalDelegate;

		private ProcCallback mExternalDelegate;

		internal IntPtr InternalCallback(IntPtr dummy)
		{
			if (mExternalDelegate != null)
			{
				mExternalDelegate(this);
			}
			dummy = IntPtr.Zero;
			return dummy;
		}

		internal static void HCkHLib(int err)
		{
			if (HalconAPI.IsFailure(err))
			{
				throw new HalconException(err, "");
			}
		}

		public HDevThread(HDevThreadContext context, ProcCallback proc, int numIn, int numOut)
		{
			HCkHLib(HalconAPI.HXCreateHThread(context.Handle, out mThreadHandle));
			GC.KeepAlive(context);
			mParamsInput = new HDevInputParam[numIn];
			mParamsOutput = new HDevOutputParam[numOut];
			mInternalDelegate = InternalCallback;
			mExternalDelegate = proc;
			mSelfRef = GCHandle.Alloc(this);
		}

		public bool IsDirectCall()
		{
			return mDirectCall;
		}

		public void Start()
		{
			mDirectCall = false;
			HCkHLib(HalconAPI.HXStartHThreadDotNet(mThreadHandle, mInternalDelegate, IntPtr.Zero, out IntPtr _));
		}

		public void ParStart(out HTuple parHandle)
		{
			mDirectCall = false;
			HCkHLib(HalconAPI.HXStartHThreadDotNet(mThreadHandle, mInternalDelegate, IntPtr.Zero, out IntPtr threadId));
			parHandle = new HTuple(threadId);
		}

		public void CallProc()
		{
			mDirectCall = true;
			HCkHLib(HalconAPI.HXPrepareDirectCall(mThreadHandle));
			if (mExternalDelegate != null)
			{
				mExternalDelegate(this);
			}
		}

		public static void ParJoin(HTuple par_handle)
		{
			for (int i = 0; i < par_handle.Length; i++)
			{
				HCkHLib(HalconAPI.HXJoinHThread(par_handle[i].IP));
			}
		}

		public void Exit()
		{
			HCkHLib(HalconAPI.HXExitHThread(mThreadHandle));
			mSelfRef.Free();
		}

		public void Dispose()
		{
			for (int i = 0; i < mParamsInput.Length; i++)
			{
				mParamsInput[i].Dispose();
			}
			if (mThreadHandle != IntPtr.Zero)
			{
				mThreadHandle = IntPtr.Zero;
			}
		}

		public void SetInputIconicParamObject(int parIndex, HObject obj)
		{
			mParamsInput[parIndex] = new HDevInputParamObject(obj);
		}

		public void SetInputIconicParamVector(int parIndex, HObjectVector vector)
		{
			mParamsInput[parIndex] = new HDevInputParamVector(vector);
		}

		public void SetInputCtrlParamTuple(int parIndex, HTuple tuple)
		{
			mParamsInput[parIndex] = new HDevInputParamTuple(tuple);
		}

		public void SetInputCtrlParamVector(int parIndex, HTupleVector vector)
		{
			mParamsInput[parIndex] = new HDevInputParamVector(vector);
		}

		public HObject GetInputIconicParamObject(int parIndex)
		{
			return mParamsInput[parIndex].GetIconicParamObject();
		}

		public HObjectVector GetInputIconicParamVector(int parIndex)
		{
			return mParamsInput[parIndex].GetIconicParamVector();
		}

		public HTuple GetInputCtrlParamTuple(int parIndex)
		{
			return mParamsInput[parIndex].GetCtrlParamTuple();
		}

		public HTupleVector GetInputCtrlParamVector(int parIndex)
		{
			return mParamsInput[parIndex].GetCtrlParamVector();
		}

		public void BindOutputIconicParamObject(int parIndex, bool global, HObject obj)
		{
			mParamsOutput[parIndex] = new HDevOutputParamObject(obj, global);
		}

		public void BindOutputIconicParamVector(int parIndex, bool global, HObjectVector vector, HTuple index)
		{
			mParamsOutput[parIndex] = new HDevOutputParamVector(vector, index, global);
		}

		public void BindOutputIconicParamVector(int parIndex, bool global, HObjectVector vector)
		{
			BindOutputIconicParamVector(parIndex, global, vector, new HTuple());
		}

		public void BindOutputCtrlParamTuple(int parIndex, bool global, HTuple tuple)
		{
			mParamsOutput[parIndex] = new HDevOutputParamTuple(tuple, global);
		}

		public void BindOutputCtrlParamVector(int parIndex, bool global, HTupleVector vector, HTuple index)
		{
			mParamsOutput[parIndex] = new HDevOutputParamVector(vector, index, global);
		}

		public void BindOutputCtrlParamVector(int parIndex, bool global, HTupleVector vector)
		{
			BindOutputCtrlParamVector(parIndex, global, vector, new HTuple());
		}

		public void StoreOutputIconicParamObject(int parIndex, HObject obj)
		{
			HDevOutputParam hDevOutputParam = mParamsOutput[parIndex];
			using (HDevParamGuard hDevParamGuard = new HDevParamGuard(mThreadHandle, hDevOutputParam.IsGlobal()))
			{
				if (hDevParamGuard.IsAvailable())
				{
					hDevOutputParam.StoreIconicParamObject(obj);
				}
			}
		}

		public void StoreOutputIconicParamVector(int parIndex, HObjectVector vector)
		{
			HDevOutputParam hDevOutputParam = mParamsOutput[parIndex];
			using (HDevParamGuard hDevParamGuard = new HDevParamGuard(mThreadHandle, hDevOutputParam.IsGlobal()))
			{
				if (hDevParamGuard.IsAvailable())
				{
					hDevOutputParam.StoreIconicParamVector(vector);
				}
			}
		}

		public void StoreOutputCtrlParamTuple(int parIndex, HTuple tuple)
		{
			HDevOutputParam hDevOutputParam = mParamsOutput[parIndex];
			using (HDevParamGuard hDevParamGuard = new HDevParamGuard(mThreadHandle, hDevOutputParam.IsGlobal()))
			{
				if (hDevParamGuard.IsAvailable())
				{
					hDevOutputParam.StoreCtrlParamTuple(tuple);
				}
			}
		}

		public void StoreOutputCtrlParamVector(int parIndex, HTupleVector vector)
		{
			HDevOutputParam hDevOutputParam = mParamsOutput[parIndex];
			using (HDevParamGuard hDevParamGuard = new HDevParamGuard(mThreadHandle, hDevOutputParam.IsGlobal()))
			{
				if (hDevParamGuard.IsAvailable())
				{
					hDevOutputParam.StoreCtrlParamVector(vector);
				}
			}
		}
	}
}
