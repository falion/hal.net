namespace HalconDotNet
{
	internal class HDevOutputParam
	{
		protected bool mGlobal;

		private static HalconException NI()
		{
			return new HalconException("Unexpected parameter type in exported parallelization code");
		}

		public HDevOutputParam(bool global)
		{
			mGlobal = global;
		}

		public bool IsGlobal()
		{
			return mGlobal;
		}

		public virtual void StoreIconicParamObject(HObject obj)
		{
			throw NI();
		}

		public virtual void StoreIconicParamVector(HObjectVector vector)
		{
			throw NI();
		}

		public virtual void StoreCtrlParamTuple(HTuple tuple)
		{
			throw NI();
		}

		public virtual void StoreCtrlParamVector(HTupleVector vector)
		{
			throw NI();
		}
	}
}
