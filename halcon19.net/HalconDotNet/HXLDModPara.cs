using System;
using System.ComponentModel;

namespace HalconDotNet
{
	[Serializable]
	public class HXLDModPara : HXLD
	{
		public new HXLDModPara this[HTuple index] => SelectObj(index);

		public HXLDModPara()
			: base(HObjectBase.UNDEF, copy: false)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDModPara(IntPtr key)
			: this(key, copy: true)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDModPara(IntPtr key, bool copy)
			: base(key, copy)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HXLDModPara(HObject obj)
			: base(obj)
		{
			AssertObjectClass();
			GC.KeepAlive(this);
		}

		private void AssertObjectClass()
		{
			HalconAPI.AssertObjectClass(key, "xld_mod_para");
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public static int LoadNew(IntPtr proc, int parIndex, int err, out HXLDModPara obj)
		{
			obj = new HXLDModPara(HObjectBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		public HXLDPoly CombineRoadsXld(HXLDPoly edgePolygons, HXLDExtPara extParallels, HXLDPoly centerLines, HTuple maxAngleParallel, HTuple maxAngleColinear, HTuple maxDistanceParallel, HTuple maxDistanceColinear)
		{
			IntPtr proc = HalconAPI.PreCall(37);
			Store(proc, 2);
			HalconAPI.Store(proc, 1, edgePolygons);
			HalconAPI.Store(proc, 3, extParallels);
			HalconAPI.Store(proc, 4, centerLines);
			HalconAPI.Store(proc, 0, maxAngleParallel);
			HalconAPI.Store(proc, 1, maxAngleColinear);
			HalconAPI.Store(proc, 2, maxDistanceParallel);
			HalconAPI.Store(proc, 3, maxDistanceColinear);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(maxAngleParallel);
			HalconAPI.UnpinTuple(maxAngleColinear);
			HalconAPI.UnpinTuple(maxDistanceParallel);
			HalconAPI.UnpinTuple(maxDistanceColinear);
			err = HXLDPoly.LoadNew(proc, 1, err, out HXLDPoly obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(edgePolygons);
			GC.KeepAlive(extParallels);
			GC.KeepAlive(centerLines);
			return obj;
		}

		public HXLDPoly CombineRoadsXld(HXLDPoly edgePolygons, HXLDExtPara extParallels, HXLDPoly centerLines, double maxAngleParallel, double maxAngleColinear, double maxDistanceParallel, double maxDistanceColinear)
		{
			IntPtr proc = HalconAPI.PreCall(37);
			Store(proc, 2);
			HalconAPI.Store(proc, 1, edgePolygons);
			HalconAPI.Store(proc, 3, extParallels);
			HalconAPI.Store(proc, 4, centerLines);
			HalconAPI.StoreD(proc, 0, maxAngleParallel);
			HalconAPI.StoreD(proc, 1, maxAngleColinear);
			HalconAPI.StoreD(proc, 2, maxDistanceParallel);
			HalconAPI.StoreD(proc, 3, maxDistanceColinear);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDPoly.LoadNew(proc, 1, err, out HXLDPoly obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(edgePolygons);
			GC.KeepAlive(extParallels);
			GC.KeepAlive(centerLines);
			return obj;
		}

		public HXLDModPara ObjDiff(HXLDModPara objectsSub)
		{
			IntPtr proc = HalconAPI.PreCall(573);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsSub);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsSub);
			return obj;
		}

		public new HXLDModPara CopyObj(int index, int numObj)
		{
			IntPtr proc = HalconAPI.PreCall(583);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.StoreI(proc, 1, numObj);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDModPara ConcatObj(HXLDModPara objects2)
		{
			IntPtr proc = HalconAPI.PreCall(584);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return obj;
		}

		public new HXLDModPara SelectObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDModPara SelectObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(587);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public int CompareObj(HXLDModPara objects2, HTuple epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.Store(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(epsilon);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public int CompareObj(HXLDModPara objects2, double epsilon)
		{
			IntPtr proc = HalconAPI.PreCall(588);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.StoreD(proc, 0, epsilon);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public int TestEqualObj(HXLDModPara objects2)
		{
			IntPtr proc = HalconAPI.PreCall(591);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objects2);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objects2);
			return intValue;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLDModPara meshes, int gridSpacing, HTuple rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.Store(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(rotation);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HImage GenGridRectificationMap(HImage image, out HXLDModPara meshes, int gridSpacing, string rotation, HTuple row, HTuple column, string mapType)
		{
			IntPtr proc = HalconAPI.PreCall(1159);
			Store(proc, 2);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 0, gridSpacing);
			HalconAPI.StoreS(proc, 1, rotation);
			HalconAPI.Store(proc, 2, row);
			HalconAPI.Store(proc, 3, column);
			HalconAPI.StoreS(proc, 4, mapType);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = LoadNew(proc, 2, err, out meshes);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public new HXLDModPara SelectXldPoint(HTuple row, HTuple column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, row);
			HalconAPI.Store(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDModPara SelectXldPoint(double row, double column)
		{
			IntPtr proc = HalconAPI.PreCall(1676);
			Store(proc, 1);
			HalconAPI.StoreD(proc, 0, row);
			HalconAPI.StoreD(proc, 1, column);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDModPara SelectShapeXld(HTuple features, string operation, HTuple min, HTuple max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.Store(proc, 2, min);
			HalconAPI.Store(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(features);
			HalconAPI.UnpinTuple(min);
			HalconAPI.UnpinTuple(max);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDModPara SelectShapeXld(string features, string operation, double min, double max)
		{
			IntPtr proc = HalconAPI.PreCall(1678);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, features);
			HalconAPI.StoreS(proc, 1, operation);
			HalconAPI.StoreD(proc, 2, min);
			HalconAPI.StoreD(proc, 3, max);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDModPara ShapeTransXld(string type)
		{
			IntPtr proc = HalconAPI.PreCall(1689);
			Store(proc, 1);
			HalconAPI.StoreS(proc, 0, type);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDModPara InsertObj(HXLDModPara objectsInsert, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2121);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsInsert);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsInsert);
			return obj;
		}

		public new HXLDModPara RemoveObj(HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public new HXLDModPara RemoveObj(int index)
		{
			IntPtr proc = HalconAPI.PreCall(2124);
			Store(proc, 1);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HXLDModPara ReplaceObj(HXLDModPara objectsReplace, HTuple index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.Store(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return obj;
		}

		public HXLDModPara ReplaceObj(HXLDModPara objectsReplace, int index)
		{
			IntPtr proc = HalconAPI.PreCall(2125);
			Store(proc, 1);
			HalconAPI.Store(proc, 2, objectsReplace);
			HalconAPI.StoreI(proc, 0, index);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = LoadNew(proc, 1, err, out HXLDModPara obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(objectsReplace);
			return obj;
		}
	}
}
