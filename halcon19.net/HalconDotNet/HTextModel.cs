using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HTextModel : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HTextModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("text_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextModel obj)
		{
			obj = new HTextModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HTextModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HTextModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HTextModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HTextModel(string mode, HTuple OCRClassifier)
		{
			IntPtr proc = HalconAPI.PreCall(422);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, OCRClassifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(OCRClassifier);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTextModel(string mode, string OCRClassifier)
		{
			IntPtr proc = HalconAPI.PreCall(422);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreS(proc, 1, OCRClassifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTextModel()
		{
			IntPtr proc = HalconAPI.PreCall(423);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTextResult FindText(HImage image)
		{
			IntPtr proc = HalconAPI.PreCall(417);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTextResult.LoadNew(proc, 0, err, out HTextResult obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HTuple GetTextModelParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(418);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetTextModelParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(419);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetTextModelParam(string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(419);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreI(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public static void ClearTextModel(HTextModel[] textModel)
		{
			HTuple hTuple = HHandleBase.ConcatArray(textModel);
			IntPtr proc = HalconAPI.PreCall(421);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(textModel);
		}

		public void ClearTextModel()
		{
			IntPtr proc = HalconAPI.PreCall(421);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateTextModelReader(string mode, HTuple OCRClassifier)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(422);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.Store(proc, 1, OCRClassifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(OCRClassifier);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateTextModelReader(string mode, string OCRClassifier)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(422);
			HalconAPI.StoreS(proc, 0, mode);
			HalconAPI.StoreS(proc, 1, OCRClassifier);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateTextModel()
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(423);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
