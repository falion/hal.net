using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HLexicon : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HLexicon()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HLexicon(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HLexicon(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("lexicon");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HLexicon obj)
		{
			obj = new HLexicon(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HLexicon[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HLexicon[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HLexicon(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HLexicon(string name, string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(670);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.StoreS(proc, 1, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HLexicon(string name, HTuple words)
		{
			IntPtr proc = HalconAPI.PreCall(671);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.Store(proc, 1, words);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(words);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ClearLexicon()
		{
			IntPtr proc = HalconAPI.PreCall(666);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public string SuggestLexicon(string word, out int numCorrections)
		{
			IntPtr proc = HalconAPI.PreCall(667);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, word);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			err = HalconAPI.LoadI(proc, 1, err, out numCorrections);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return stringValue;
		}

		public int LookupLexicon(string word)
		{
			IntPtr proc = HalconAPI.PreCall(668);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, word);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple InspectLexicon()
		{
			IntPtr proc = HalconAPI.PreCall(669);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void ImportLexicon(string name, string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(670);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.StoreS(proc, 1, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateLexicon(string name, HTuple words)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(671);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.Store(proc, 1, words);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(words);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
