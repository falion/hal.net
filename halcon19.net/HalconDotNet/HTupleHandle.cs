using System;

namespace HalconDotNet
{
	internal class HTupleHandle : HTupleImplementation
	{
		protected HHandle[] h;

		public override HHandle[] HArr
		{
			get
			{
				return h;
			}
			set
			{
				SetArray(value, copy: false);
			}
		}

		public override HTupleType Type => HTupleType.HANDLE;

		protected override Array CreateArray(int size)
		{
			HHandle[] array = new HHandle[size];
			for (int i = 0; i < size; i++)
			{
				array[i] = new HHandle();
			}
			return array;
		}

		protected override void NotifyArrayUpdate()
		{
			h = (HHandle[])data;
		}

		public HTupleHandle(HHandle h)
		{
			SetArray(new HHandle[1]
			{
				new HHandle(h)
			}, copy: false);
		}

		public HTupleHandle(HHandle[] h, bool copy)
		{
			if (copy)
			{
				HHandle[] array = new HHandle[h.Length];
				for (int i = 0; i < h.Length; i++)
				{
					array[i] = new HHandle(h[i]);
				}
				SetArray(array, copy: false);
			}
			else
			{
				SetArray(h, copy: false);
			}
		}

		public override HTupleElements GetElement(int index, HTuple parent)
		{
			return new HTupleElements(parent, this, index);
		}

		public override HTupleElements GetElements(int[] indices, HTuple parent)
		{
			if (indices == null || indices.Length == 0)
			{
				return new HTupleElements();
			}
			return new HTupleElements(parent, this, indices);
		}

		public override void SetElements(int[] indices, HTupleElements elements)
		{
			HTupleElementsHandle hTupleElementsHandle = new HTupleElementsHandle(this, indices);
			hTupleElementsHandle.setH(elements.HArr);
		}

		public override void Dispose()
		{
			for (int i = 0; i < base.Length; i++)
			{
				if (h[i] != null)
				{
					h[i].Dispose();
				}
			}
		}

		public override HHandle[] ToHArr()
		{
			HHandle[] array = new HHandle[iLength];
			CopyToHArr(array, 0);
			return array;
		}

		public override object[] ToOArr()
		{
			object[] array = new object[iLength];
			CopyToOArr(array, 0);
			return array;
		}

		public override int CopyToHArr(HHandle[] dst, int offset)
		{
			for (int i = 0; i < iLength; i++)
			{
				dst[i + offset] = new HHandle(h[i]);
			}
			return iLength;
		}

		public override int CopyToOArr(object[] dst, int offset)
		{
			for (int i = 0; i < iLength; i++)
			{
				dst[i + offset] = new HHandle(h[i]);
			}
			return iLength;
		}

		public override int CopyFrom(HTupleImplementation impl, int offset)
		{
			return impl.CopyToHArr(h, offset);
		}

		protected override void StoreData(IntPtr proc, IntPtr tuple)
		{
			for (int i = 0; i < iLength; i++)
			{
				HalconAPI.HCkP(proc, HalconAPI.SetH(tuple, i, h[i]));
			}
		}

		public static int Load(IntPtr tuple, out HTupleHandle data)
		{
			int num = 2;
			HalconAPI.GetTupleLength(tuple, out int length);
			HHandle[] array = new HHandle[length];
			for (int i = 0; i < length; i++)
			{
				if (!HalconAPI.IsFailure(num))
				{
					num = HalconAPI.GetH(tuple, i, out array[i]);
				}
			}
			data = new HTupleHandle(array, copy: false);
			return num;
		}
	}
}
