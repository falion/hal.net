using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HFeatureSet : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFeatureSet()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFeatureSet(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFeatureSet(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("feature_set");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFeatureSet obj)
		{
			obj = new HFeatureSet(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFeatureSet[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HFeatureSet[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HFeatureSet(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HFeatureSet(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1888);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ReadSampset(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1888);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void LearnSampsetBox(HClassBox classifHandle, string outfile, int NSamples, double stopError, int errorN)
		{
			IntPtr proc = HalconAPI.PreCall(1890);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, classifHandle);
			HalconAPI.StoreS(proc, 2, outfile);
			HalconAPI.StoreI(proc, 3, NSamples);
			HalconAPI.StoreD(proc, 4, stopError);
			HalconAPI.StoreI(proc, 5, errorN);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(classifHandle);
		}

		public void ClearSampset()
		{
			IntPtr proc = HalconAPI.PreCall(1893);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public double TestSampsetBox(HClassBox classifHandle)
		{
			IntPtr proc = HalconAPI.PreCall(1897);
			Store(proc, 1);
			HalconAPI.Store(proc, 0, classifHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(classifHandle);
			return doubleValue;
		}
	}
}
