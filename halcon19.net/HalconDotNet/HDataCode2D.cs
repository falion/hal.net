using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HDataCode2D : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDataCode2D()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDataCode2D(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDataCode2D(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("datacode_2d");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDataCode2D obj)
		{
			obj = new HDataCode2D(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDataCode2D[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDataCode2D[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HDataCode2D(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HDataCode2D(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1774);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDataCode2D(string symbolType, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1778);
			HalconAPI.StoreS(proc, 0, symbolType);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HDataCode2D(string symbolType, string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1778);
			HalconAPI.StoreS(proc, 0, symbolType);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeDataCode2dModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDataCode2D(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeDataCode2dModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeDataCode2dModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HDataCode2D Deserialize(Stream stream)
		{
			HDataCode2D hDataCode2D = new HDataCode2D();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hDataCode2D.DeserializeDataCode2dModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDataCode2D;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HDataCode2D Clone()
		{
			HSerializedItem hSerializedItem = SerializeDataCode2dModel();
			HDataCode2D hDataCode2D = new HDataCode2D();
			hDataCode2D.DeserializeDataCode2dModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hDataCode2D;
		}

		public HObject GetDataCode2dObjects(HTuple candidateHandle, string objectName)
		{
			IntPtr proc = HalconAPI.PreCall(1766);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, candidateHandle);
			HalconAPI.StoreS(proc, 2, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(candidateHandle);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HObject GetDataCode2dObjects(int candidateHandle, string objectName)
		{
			IntPtr proc = HalconAPI.PreCall(1766);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, candidateHandle);
			HalconAPI.StoreS(proc, 2, objectName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HObject.LoadNew(proc, 1, err, out HObject obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HTuple GetDataCode2dResults(HTuple candidateHandle, HTuple resultNames)
		{
			IntPtr proc = HalconAPI.PreCall(1767);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, candidateHandle);
			HalconAPI.Store(proc, 2, resultNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(candidateHandle);
			HalconAPI.UnpinTuple(resultNames);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDataCode2dResults(string candidateHandle, string resultNames)
		{
			IntPtr proc = HalconAPI.PreCall(1767);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, candidateHandle);
			HalconAPI.StoreS(proc, 2, resultNames);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HXLDCont FindDataCode2d(HImage image, HTuple genParamName, HTuple genParamValue, out HTuple resultHandles, out HTuple decodedDataStrings)
		{
			IntPtr proc = HalconAPI.PreCall(1768);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out resultHandles);
			err = HTuple.LoadNew(proc, 1, err, out decodedDataStrings);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public HXLDCont FindDataCode2d(HImage image, string genParamName, int genParamValue, out int resultHandles, out string decodedDataStrings)
		{
			IntPtr proc = HalconAPI.PreCall(1768);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreI(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			err = HalconAPI.LoadI(proc, 0, err, out resultHandles);
			err = HalconAPI.LoadS(proc, 1, err, out decodedDataStrings);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
			return obj;
		}

		public void SetDataCode2dParam(HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1769);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetDataCode2dParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1769);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetDataCode2dParam(HTuple genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1770);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDataCode2dParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(1770);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple QueryDataCode2dParams(string queryName)
		{
			IntPtr proc = HalconAPI.PreCall(1771);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, queryName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void DeserializeDataCode2dModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1772);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeDataCode2dModel()
		{
			IntPtr proc = HalconAPI.PreCall(1773);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadDataCode2dModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1774);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteDataCode2dModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1775);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ClearDataCode2dModel()
		{
			IntPtr proc = HalconAPI.PreCall(1777);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateDataCode2dModel(string symbolType, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1778);
			HalconAPI.StoreS(proc, 0, symbolType);
			HalconAPI.Store(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateDataCode2dModel(string symbolType, string genParamName, string genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1778);
			HalconAPI.StoreS(proc, 0, symbolType);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
