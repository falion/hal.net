using System;
using System.ComponentModel;

namespace HalconDotNet
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class HDevWindowStack : IDisposable
	{
		~HDevWindowStack()
		{
			try
			{
				Dispose(disposing: false);
			}
			catch (Exception)
			{
			}
		}

		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				GC.SuppressFinalize(this);
			}
			GC.KeepAlive(this);
		}

		void IDisposable.Dispose()
		{
			Dispose(disposing: true);
		}

		public virtual void Dispose()
		{
			Dispose(disposing: true);
		}

		public static void Push(HTuple win_handle)
		{
			int num;
			if (win_handle.Type == HTupleType.HANDLE)
			{
				HHandle h = win_handle.H;
				num = HalconAPI.HWindowStackPush(h.Handle);
				GC.KeepAlive(h);
			}
			else
			{
				num = HalconAPI.HWindowStackPush(win_handle.IP);
			}
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::Push");
			}
		}

		public static HTuple Pop()
		{
			int num = HalconAPI.HWindowStackGetActive(out IntPtr win_handle);
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::Pop");
			}
			HTuple result;
			if (HalconAPI.IsLegacyHandleMode())
			{
				result = win_handle;
			}
			else
			{
				using (HHandle h = new HHandle(win_handle))
				{
					result = new HTuple(h);
				}
			}
			num = HalconAPI.HWindowStackPop();
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::Pop");
			}
			return result;
		}

		public static HTuple GetActive()
		{
			IntPtr win_handle;
			int num = HalconAPI.HWindowStackGetActive(out win_handle);
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::GetActive");
			}
			if (HalconAPI.IsLegacyHandleMode())
			{
				return win_handle;
			}
			using (HHandle h = new HHandle(win_handle))
			{
				return new HTuple(h);
			}
		}

		public static void SetActive(HTuple win_handle)
		{
			int num;
			if (win_handle.Type == HTupleType.HANDLE)
			{
				HHandle h = win_handle.H;
				num = HalconAPI.HWindowStackSetActive(h.Handle);
				GC.KeepAlive(h);
			}
			else
			{
				num = HalconAPI.HWindowStackSetActive(win_handle.IP);
			}
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::SetActive");
			}
		}

		public static bool IsOpen()
		{
			bool is_open;
			int num = HalconAPI.HWindowStackIsOpen(out is_open);
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::IsOpen");
			}
			return is_open;
		}

		public static void CloseAll()
		{
			int num = HalconAPI.HWindowStackCloseAll();
			if (num != 2)
			{
				throw new HalconException(num, "HDevWindowStack::CloseAll");
			}
		}
	}
}
