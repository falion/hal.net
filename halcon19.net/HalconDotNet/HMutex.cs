using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HMutex : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMutex()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMutex(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HMutex(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("mutex");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMutex obj)
		{
			obj = new HMutex(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HMutex[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HMutex[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HMutex(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HMutex(HTuple attribName, HTuple attribValue)
		{
			IntPtr proc = HalconAPI.PreCall(564);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HMutex(string attribName, string attribValue)
		{
			IntPtr proc = HalconAPI.PreCall(564);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void ClearMutex()
		{
			IntPtr proc = HalconAPI.PreCall(560);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void UnlockMutex()
		{
			IntPtr proc = HalconAPI.PreCall(561);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public int TryLockMutex()
		{
			IntPtr proc = HalconAPI.PreCall(562);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public void LockMutex()
		{
			IntPtr proc = HalconAPI.PreCall(563);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateMutex(HTuple attribName, HTuple attribValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(564);
			HalconAPI.Store(proc, 0, attribName);
			HalconAPI.Store(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(attribName);
			HalconAPI.UnpinTuple(attribValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void CreateMutex(string attribName, string attribValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(564);
			HalconAPI.StoreS(proc, 0, attribName);
			HalconAPI.StoreS(proc, 1, attribValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
