namespace HalconDotNet
{
	internal class HDevOutputParamTuple : HDevOutputParam
	{
		protected HTuple mTuple;

		public HDevOutputParamTuple(HTuple tuple, bool global)
			: base(global)
		{
			mTuple = tuple;
		}

		public override void StoreCtrlParamTuple(HTuple tuple)
		{
			mTuple.TransferOwnership(tuple);
		}
	}
}
