using System.Windows.Input;

namespace HalconDotNet
{
	internal delegate void HWMouseEventHandler(int x, int y, MouseButton? button, int delta);
}
