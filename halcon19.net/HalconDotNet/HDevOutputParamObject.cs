namespace HalconDotNet
{
	internal class HDevOutputParamObject : HDevOutputParam
	{
		protected HObject mObject;

		public HDevOutputParamObject(HObject obj, bool global)
			: base(global)
		{
			mObject = obj;
		}

		public override void StoreIconicParamObject(HObject obj)
		{
			mObject.TransferOwnership(obj);
		}
	}
}
