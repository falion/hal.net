using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HHandle : HHandleBase, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HHandle()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HHandle(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HHandle(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HHandle obj)
		{
			obj = new HHandle(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HHandle[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HHandle[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HHandle(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeHandle();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HHandle(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeHandle(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public virtual void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeHandle();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public static HHandle Deserialize(Stream stream)
		{
			HHandle hHandle = new HHandle();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hHandle.DeserializeHandle(hSerializedItem);
			hSerializedItem.Dispose();
			return hHandle;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public HHandle Clone()
		{
			HSerializedItem hSerializedItem = SerializeHandle();
			HHandle hHandle = new HHandle();
			hHandle.DeserializeHandle(hSerializedItem);
			hSerializedItem.Dispose();
			return hHandle;
		}

		public void ClearHandle()
		{
			IntPtr proc = HalconAPI.PreCall(2134);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeHandle(HSerializedItem serializedItem)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2135);
			HalconAPI.Store(proc, 0, serializedItem);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItem);
		}

		public HSerializedItem SerializeHandle()
		{
			IntPtr proc = HalconAPI.PreCall(2138);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public int TupleIsSerializable()
		{
			IntPtr proc = HalconAPI.PreCall(2141);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadI(proc, 0, err, out int intValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intValue;
		}

		public HTuple TupleIsValidHandle()
		{
			IntPtr proc = HalconAPI.PreCall(2143);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public string TupleSemType()
		{
			IntPtr proc = HalconAPI.PreCall(2144);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadS(proc, 0, err, out string stringValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return stringValue;
		}
	}
}
