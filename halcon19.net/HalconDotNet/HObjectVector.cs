using System.ComponentModel;

namespace HalconDotNet
{
	public class HObjectVector : HVector
	{
		private HObject mObject;

		public HObject O
		{
			get
			{
				AssertDimension(0);
				return mObject;
			}
			set
			{
				AssertDimension(0);
				if (value == null || !value.IsInitialized())
				{
					throw new HVectorAccessException("Uninitialized object not allowed in vector");
				}
				mObject.Dispose();
				mObject = new HObject(value);
			}
		}

		public new HObjectVector this[int index]
		{
			get
			{
				return (HObjectVector)base[index];
			}
			set
			{
				base[index] = value;
			}
		}

		public HObjectVector(int dimension)
			: base(dimension)
		{
			mObject = ((dimension <= 0) ? GenEmptyObj() : null);
		}

		public HObjectVector(HObject obj)
			: base(0)
		{
			if (obj == null || !obj.IsInitialized())
			{
				throw new HVectorAccessException("Uninitialized object not allowed in vector");
			}
			mObject = new HObject(obj);
		}

		public HObjectVector(HObjectVector vector)
			: base(vector)
		{
			if (mDimension <= 0)
			{
				mObject = new HObject(vector.mObject);
			}
		}

		private static HObject GenEmptyObj()
		{
			HObject hObject = new HObject();
			hObject.GenEmptyObj();
			return hObject;
		}

		protected override HVector GetDefaultElement()
		{
			return new HObjectVector(mDimension - 1);
		}

		public new HObjectVector At(int index)
		{
			return (HObjectVector)base.At(index);
		}

		protected override bool EqualsImpl(HVector vector)
		{
			if (mDimension >= 1)
			{
				return base.EqualsImpl(vector);
			}
			return ((HObjectVector)vector).O.TestEqualObj(O) != 0;
		}

		public bool VectorEqual(HObjectVector vector)
		{
			return EqualsImpl(vector);
		}

		public HObjectVector Concat(HObjectVector vector)
		{
			return (HObjectVector)ConcatImpl(vector, append: false, clone: true);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HObjectVector Concat(HObjectVector vector, bool clone)
		{
			return (HObjectVector)ConcatImpl(vector, false, clone);
		}

		public HObjectVector Append(HObjectVector vector)
		{
			return (HObjectVector)ConcatImpl(vector, append: true, clone: true);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HObjectVector Append(HObjectVector vector, bool clone)
		{
			return (HObjectVector)ConcatImpl(vector, true, clone);
		}

		public HObjectVector Insert(int index, HObjectVector vector)
		{
			InsertImpl(index, vector, clone: true);
			return this;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HObjectVector Insert(int index, HObjectVector vector, bool clone)
		{
			InsertImpl(index, vector, clone);
			return this;
		}

		public new HObjectVector Remove(int index)
		{
			RemoveImpl(index);
			return this;
		}

		public new HObjectVector Clear()
		{
			ClearImpl();
			return this;
		}

		public new HObjectVector Clone()
		{
			return (HObjectVector)CloneImpl();
		}

		protected override HVector CloneImpl()
		{
			return new HObjectVector(this);
		}

		protected override void DisposeLeafObject()
		{
			if (mDimension <= 0)
			{
				mObject.Dispose();
			}
		}

		public override string ToString()
		{
			if (mDimension <= 0)
			{
				return mObject.Key.ToString();
			}
			return base.ToString();
		}
	}
}
