using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HCalibData : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCalibData()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCalibData(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCalibData(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("calib_data");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCalibData obj)
		{
			obj = new HCalibData(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HCalibData[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HCalibData[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HCalibData(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HCalibData(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1963);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HCalibData(string calibSetup, int numCameras, int numCalibObjects)
		{
			IntPtr proc = HalconAPI.PreCall(1980);
			HalconAPI.StoreS(proc, 0, calibSetup);
			HalconAPI.StoreI(proc, 1, numCameras);
			HalconAPI.StoreI(proc, 2, numCalibObjects);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeCalibData();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HCalibData(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeCalibData(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeCalibData();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HCalibData Deserialize(Stream stream)
		{
			HCalibData hCalibData = new HCalibData();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hCalibData.DeserializeCalibData(hSerializedItem);
			hSerializedItem.Dispose();
			return hCalibData;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HCalibData Clone()
		{
			HSerializedItem hSerializedItem = SerializeCalibData();
			HCalibData hCalibData = new HCalibData();
			hCalibData.DeserializeCalibData(hSerializedItem);
			hSerializedItem.Dispose();
			return hCalibData;
		}

		public void ClearCalibData()
		{
			IntPtr proc = HalconAPI.PreCall(1960);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeCalibData(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1961);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeCalibData()
		{
			IntPtr proc = HalconAPI.PreCall(1962);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ReadCalibData(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1963);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteCalibData(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(1964);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple CalibrateHandEye()
		{
			IntPtr proc = HalconAPI.PreCall(1965);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.DOUBLE, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public double CalibrateCameras()
		{
			IntPtr proc = HalconAPI.PreCall(1966);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public void RemoveCalibData(string itemType, HTuple itemIdx)
		{
			IntPtr proc = HalconAPI.PreCall(1967);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, itemType);
			HalconAPI.Store(proc, 2, itemIdx);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(itemIdx);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void RemoveCalibData(string itemType, int itemIdx)
		{
			IntPtr proc = HalconAPI.PreCall(1967);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, itemType);
			HalconAPI.StoreI(proc, 2, itemIdx);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCalibData(string itemType, HTuple itemIdx, string dataName, HTuple dataValue)
		{
			IntPtr proc = HalconAPI.PreCall(1968);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, itemType);
			HalconAPI.Store(proc, 2, itemIdx);
			HalconAPI.StoreS(proc, 3, dataName);
			HalconAPI.Store(proc, 4, dataValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(itemIdx);
			HalconAPI.UnpinTuple(dataValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCalibData(string itemType, int itemIdx, string dataName, string dataValue)
		{
			IntPtr proc = HalconAPI.PreCall(1968);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, itemType);
			HalconAPI.StoreI(proc, 2, itemIdx);
			HalconAPI.StoreS(proc, 3, dataName);
			HalconAPI.StoreS(proc, 4, dataValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void FindCalibObject(HImage image, int cameraIdx, int calibObjIdx, int calibObjPoseIdx, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(1969);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, image);
			HalconAPI.StoreI(proc, 1, cameraIdx);
			HalconAPI.StoreI(proc, 2, calibObjIdx);
			HalconAPI.StoreI(proc, 3, calibObjPoseIdx);
			HalconAPI.Store(proc, 4, genParamName);
			HalconAPI.Store(proc, 5, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(image);
		}

		public void RemoveCalibDataObserv(int cameraIdx, int calibObjIdx, int calibObjPoseIdx)
		{
			IntPtr proc = HalconAPI.PreCall(1970);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIdx);
			HalconAPI.StoreI(proc, 2, calibObjIdx);
			HalconAPI.StoreI(proc, 3, calibObjPoseIdx);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HXLDCont GetCalibDataObservContours(string contourName, int cameraIdx, int calibObjIdx, int calibObjPoseIdx)
		{
			IntPtr proc = HalconAPI.PreCall(1971);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, contourName);
			HalconAPI.StoreI(proc, 2, cameraIdx);
			HalconAPI.StoreI(proc, 3, calibObjIdx);
			HalconAPI.StoreI(proc, 4, calibObjPoseIdx);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HXLDCont.LoadNew(proc, 1, err, out HXLDCont obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HPose GetCalibDataObservPose(int cameraIdx, int calibObjIdx, int calibObjPoseIdx)
		{
			IntPtr proc = HalconAPI.PreCall(1972);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIdx);
			HalconAPI.StoreI(proc, 2, calibObjIdx);
			HalconAPI.StoreI(proc, 3, calibObjPoseIdx);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HPose.LoadNew(proc, 0, err, out HPose obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void SetCalibDataObservPose(int cameraIdx, int calibObjIdx, int calibObjPoseIdx, HPose objInCameraPose)
		{
			IntPtr proc = HalconAPI.PreCall(1973);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIdx);
			HalconAPI.StoreI(proc, 2, calibObjIdx);
			HalconAPI.StoreI(proc, 3, calibObjPoseIdx);
			HalconAPI.Store(proc, 4, objInCameraPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(objInCameraPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void GetCalibDataObservPoints(int cameraIdx, int calibObjIdx, int calibObjPoseIdx, out HTuple row, out HTuple column, out HTuple index, out HTuple pose)
		{
			IntPtr proc = HalconAPI.PreCall(1974);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIdx);
			HalconAPI.StoreI(proc, 2, calibObjIdx);
			HalconAPI.StoreI(proc, 3, calibObjPoseIdx);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out row);
			err = HTuple.LoadNew(proc, 1, err, out column);
			err = HTuple.LoadNew(proc, 2, err, out index);
			err = HTuple.LoadNew(proc, 3, err, out pose);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SetCalibDataObservPoints(int cameraIdx, int calibObjIdx, int calibObjPoseIdx, HTuple row, HTuple column, HTuple index, HTuple pose)
		{
			IntPtr proc = HalconAPI.PreCall(1975);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, cameraIdx);
			HalconAPI.StoreI(proc, 2, calibObjIdx);
			HalconAPI.StoreI(proc, 3, calibObjPoseIdx);
			HalconAPI.Store(proc, 4, row);
			HalconAPI.Store(proc, 5, column);
			HalconAPI.Store(proc, 6, index);
			HalconAPI.Store(proc, 7, pose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(row);
			HalconAPI.UnpinTuple(column);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(pose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple QueryCalibDataObservIndices(string itemType, int itemIdx, out HTuple index2)
		{
			IntPtr proc = HalconAPI.PreCall(1976);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, itemType);
			HalconAPI.StoreI(proc, 2, itemIdx);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out HTuple tuple);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out index2);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetCalibData(string itemType, HTuple itemIdx, HTuple dataName)
		{
			IntPtr proc = HalconAPI.PreCall(1977);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, itemType);
			HalconAPI.Store(proc, 2, itemIdx);
			HalconAPI.Store(proc, 3, dataName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(itemIdx);
			HalconAPI.UnpinTuple(dataName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetCalibData(string itemType, int itemIdx, string dataName)
		{
			IntPtr proc = HalconAPI.PreCall(1977);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, itemType);
			HalconAPI.StoreI(proc, 2, itemIdx);
			HalconAPI.StoreS(proc, 3, dataName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetCalibDataCalibObject(int calibObjIdx, HTuple calibObjDescr)
		{
			IntPtr proc = HalconAPI.PreCall(1978);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, calibObjIdx);
			HalconAPI.Store(proc, 2, calibObjDescr);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(calibObjDescr);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCalibDataCalibObject(int calibObjIdx, double calibObjDescr)
		{
			IntPtr proc = HalconAPI.PreCall(1978);
			Store(proc, 0);
			HalconAPI.StoreI(proc, 1, calibObjIdx);
			HalconAPI.StoreD(proc, 2, calibObjDescr);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCalibDataCamParam(HTuple cameraIdx, HTuple cameraType, HCamPar cameraParam)
		{
			IntPtr proc = HalconAPI.PreCall(1979);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.Store(proc, 2, cameraType);
			HalconAPI.Store(proc, 3, cameraParam);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraType);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetCalibDataCamParam(HTuple cameraIdx, string cameraType, HCamPar cameraParam)
		{
			IntPtr proc = HalconAPI.PreCall(1979);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, cameraIdx);
			HalconAPI.StoreS(proc, 2, cameraType);
			HalconAPI.Store(proc, 3, cameraParam);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(cameraIdx);
			HalconAPI.UnpinTuple(cameraParam);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateCalibData(string calibSetup, int numCameras, int numCalibObjects)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(1980);
			HalconAPI.StoreS(proc, 0, calibSetup);
			HalconAPI.StoreI(proc, 1, numCameras);
			HalconAPI.StoreI(proc, 2, numCalibObjects);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}
	}
}
