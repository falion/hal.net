using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HFramegrabber : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFramegrabber()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFramegrabber(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HFramegrabber(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("framegrabber");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFramegrabber obj)
		{
			obj = new HFramegrabber(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HFramegrabber[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HFramegrabber[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HFramegrabber(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HFramegrabber(string name, int horizontalResolution, int verticalResolution, int imageWidth, int imageHeight, int startRow, int startColumn, string field, HTuple bitsPerChannel, HTuple colorSpace, HTuple generic, string externalTrigger, HTuple cameraType, HTuple device, HTuple port, HTuple lineIn)
		{
			IntPtr proc = HalconAPI.PreCall(2037);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.StoreI(proc, 1, horizontalResolution);
			HalconAPI.StoreI(proc, 2, verticalResolution);
			HalconAPI.StoreI(proc, 3, imageWidth);
			HalconAPI.StoreI(proc, 4, imageHeight);
			HalconAPI.StoreI(proc, 5, startRow);
			HalconAPI.StoreI(proc, 6, startColumn);
			HalconAPI.StoreS(proc, 7, field);
			HalconAPI.Store(proc, 8, bitsPerChannel);
			HalconAPI.Store(proc, 9, colorSpace);
			HalconAPI.Store(proc, 10, generic);
			HalconAPI.StoreS(proc, 11, externalTrigger);
			HalconAPI.Store(proc, 12, cameraType);
			HalconAPI.Store(proc, 13, device);
			HalconAPI.Store(proc, 14, port);
			HalconAPI.Store(proc, 15, lineIn);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(bitsPerChannel);
			HalconAPI.UnpinTuple(colorSpace);
			HalconAPI.UnpinTuple(generic);
			HalconAPI.UnpinTuple(cameraType);
			HalconAPI.UnpinTuple(device);
			HalconAPI.UnpinTuple(port);
			HalconAPI.UnpinTuple(lineIn);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HFramegrabber(string name, int horizontalResolution, int verticalResolution, int imageWidth, int imageHeight, int startRow, int startColumn, string field, int bitsPerChannel, string colorSpace, double generic, string externalTrigger, string cameraType, string device, int port, int lineIn)
		{
			IntPtr proc = HalconAPI.PreCall(2037);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.StoreI(proc, 1, horizontalResolution);
			HalconAPI.StoreI(proc, 2, verticalResolution);
			HalconAPI.StoreI(proc, 3, imageWidth);
			HalconAPI.StoreI(proc, 4, imageHeight);
			HalconAPI.StoreI(proc, 5, startRow);
			HalconAPI.StoreI(proc, 6, startColumn);
			HalconAPI.StoreS(proc, 7, field);
			HalconAPI.StoreI(proc, 8, bitsPerChannel);
			HalconAPI.StoreS(proc, 9, colorSpace);
			HalconAPI.StoreD(proc, 10, generic);
			HalconAPI.StoreS(proc, 11, externalTrigger);
			HalconAPI.StoreS(proc, 12, cameraType);
			HalconAPI.StoreS(proc, 13, device);
			HalconAPI.StoreI(proc, 14, port);
			HalconAPI.StoreI(proc, 15, lineIn);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HTuple GetFramegrabberParam(HTuple param)
		{
			IntPtr proc = HalconAPI.PreCall(2025);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, param);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(param);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetFramegrabberParam(string param)
		{
			IntPtr proc = HalconAPI.PreCall(2025);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, param);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void SetFramegrabberParam(HTuple param, HTuple value)
		{
			IntPtr proc = HalconAPI.PreCall(2026);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, param);
			HalconAPI.Store(proc, 2, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(param);
			HalconAPI.UnpinTuple(value);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetFramegrabberParam(string param, string value)
		{
			IntPtr proc = HalconAPI.PreCall(2026);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, param);
			HalconAPI.StoreS(proc, 2, value);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public IntPtr GetFramegrabberCallback(string callbackType, out IntPtr userContext)
		{
			IntPtr proc = HalconAPI.PreCall(2027);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, callbackType);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadIP(proc, 0, err, out IntPtr intPtrValue);
			err = HalconAPI.LoadIP(proc, 1, err, out userContext);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return intPtrValue;
		}

		public void SetFramegrabberCallback(string callbackType, IntPtr callbackFunction, IntPtr userContext)
		{
			IntPtr proc = HalconAPI.PreCall(2028);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, callbackType);
			HalconAPI.StoreIP(proc, 2, callbackFunction);
			HalconAPI.StoreIP(proc, 3, userContext);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HImage GrabDataAsync(out HRegion region, out HXLDCont contours, double maxDelay, out HTuple data)
		{
			IntPtr proc = HalconAPI.PreCall(2029);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, maxDelay);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HRegion.LoadNew(proc, 2, err, out region);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage GrabDataAsync(out HRegion region, out HXLDCont contours, double maxDelay, out string data)
		{
			IntPtr proc = HalconAPI.PreCall(2029);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, maxDelay);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HRegion.LoadNew(proc, 2, err, out region);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HalconAPI.LoadS(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage GrabData(out HRegion region, out HXLDCont contours, out HTuple data)
		{
			IntPtr proc = HalconAPI.PreCall(2030);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HRegion.LoadNew(proc, 2, err, out region);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HTuple.LoadNew(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage GrabData(out HRegion region, out HXLDCont contours, out string data)
		{
			IntPtr proc = HalconAPI.PreCall(2030);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			HalconAPI.InitOCT(proc, 3);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			err = HRegion.LoadNew(proc, 2, err, out region);
			err = HXLDCont.LoadNew(proc, 3, err, out contours);
			err = HalconAPI.LoadS(proc, 0, err, out data);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage GrabImageAsync(double maxDelay)
		{
			IntPtr proc = HalconAPI.PreCall(2031);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, maxDelay);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void GrabImageStart(double maxDelay)
		{
			IntPtr proc = HalconAPI.PreCall(2032);
			Store(proc, 0);
			HalconAPI.StoreD(proc, 1, maxDelay);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HImage GrabImage()
		{
			IntPtr proc = HalconAPI.PreCall(2033);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void CloseFramegrabber()
		{
			IntPtr proc = HalconAPI.PreCall(2036);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void OpenFramegrabber(string name, int horizontalResolution, int verticalResolution, int imageWidth, int imageHeight, int startRow, int startColumn, string field, HTuple bitsPerChannel, HTuple colorSpace, HTuple generic, string externalTrigger, HTuple cameraType, HTuple device, HTuple port, HTuple lineIn)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2037);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.StoreI(proc, 1, horizontalResolution);
			HalconAPI.StoreI(proc, 2, verticalResolution);
			HalconAPI.StoreI(proc, 3, imageWidth);
			HalconAPI.StoreI(proc, 4, imageHeight);
			HalconAPI.StoreI(proc, 5, startRow);
			HalconAPI.StoreI(proc, 6, startColumn);
			HalconAPI.StoreS(proc, 7, field);
			HalconAPI.Store(proc, 8, bitsPerChannel);
			HalconAPI.Store(proc, 9, colorSpace);
			HalconAPI.Store(proc, 10, generic);
			HalconAPI.StoreS(proc, 11, externalTrigger);
			HalconAPI.Store(proc, 12, cameraType);
			HalconAPI.Store(proc, 13, device);
			HalconAPI.Store(proc, 14, port);
			HalconAPI.Store(proc, 15, lineIn);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(bitsPerChannel);
			HalconAPI.UnpinTuple(colorSpace);
			HalconAPI.UnpinTuple(generic);
			HalconAPI.UnpinTuple(cameraType);
			HalconAPI.UnpinTuple(device);
			HalconAPI.UnpinTuple(port);
			HalconAPI.UnpinTuple(lineIn);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void OpenFramegrabber(string name, int horizontalResolution, int verticalResolution, int imageWidth, int imageHeight, int startRow, int startColumn, string field, int bitsPerChannel, string colorSpace, double generic, string externalTrigger, string cameraType, string device, int port, int lineIn)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(2037);
			HalconAPI.StoreS(proc, 0, name);
			HalconAPI.StoreI(proc, 1, horizontalResolution);
			HalconAPI.StoreI(proc, 2, verticalResolution);
			HalconAPI.StoreI(proc, 3, imageWidth);
			HalconAPI.StoreI(proc, 4, imageHeight);
			HalconAPI.StoreI(proc, 5, startRow);
			HalconAPI.StoreI(proc, 6, startColumn);
			HalconAPI.StoreS(proc, 7, field);
			HalconAPI.StoreI(proc, 8, bitsPerChannel);
			HalconAPI.StoreS(proc, 9, colorSpace);
			HalconAPI.StoreD(proc, 10, generic);
			HalconAPI.StoreS(proc, 11, externalTrigger);
			HalconAPI.StoreS(proc, 12, cameraType);
			HalconAPI.StoreS(proc, 13, device);
			HalconAPI.StoreI(proc, 14, port);
			HalconAPI.StoreI(proc, 15, lineIn);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void GetFramegrabberLut(out HTuple imageRed, out HTuple imageGreen, out HTuple imageBlue)
		{
			IntPtr proc = HalconAPI.PreCall(2038);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			HalconAPI.InitOCT(proc, 1);
			HalconAPI.InitOCT(proc, 2);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, HTupleType.INTEGER, err, out imageRed);
			err = HTuple.LoadNew(proc, 1, HTupleType.INTEGER, err, out imageGreen);
			err = HTuple.LoadNew(proc, 2, HTupleType.INTEGER, err, out imageBlue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void SetFramegrabberLut(HTuple imageRed, HTuple imageGreen, HTuple imageBlue)
		{
			IntPtr proc = HalconAPI.PreCall(2039);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, imageRed);
			HalconAPI.Store(proc, 2, imageGreen);
			HalconAPI.Store(proc, 3, imageBlue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(imageRed);
			HalconAPI.UnpinTuple(imageGreen);
			HalconAPI.UnpinTuple(imageBlue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}
	}
}
