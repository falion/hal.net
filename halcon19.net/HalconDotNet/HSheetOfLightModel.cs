using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace HalconDotNet
{
	[Serializable]
	public class HSheetOfLightModel : HHandle, ISerializable, ICloneable
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSheetOfLightModel()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSheetOfLightModel(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSheetOfLightModel(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("sheet_of_light_model");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSheetOfLightModel obj)
		{
			obj = new HSheetOfLightModel(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HSheetOfLightModel[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HSheetOfLightModel[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HSheetOfLightModel(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HSheetOfLightModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(374);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public HSheetOfLightModel(HRegion profileRegion, HTuple genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(391);
			HalconAPI.Store(proc, 1, profileRegion);
			HalconAPI.Store(proc, 0, genParamName);
			HalconAPI.Store(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(profileRegion);
		}

		public HSheetOfLightModel(HRegion profileRegion, string genParamName, int genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(391);
			HalconAPI.Store(proc, 1, profileRegion);
			HalconAPI.StoreS(proc, 0, genParamName);
			HalconAPI.StoreI(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(profileRegion);
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			HSerializedItem hSerializedItem = SerializeSheetOfLightModel();
			byte[] value = hSerializedItem;
			hSerializedItem.Dispose();
			info.AddValue("data", value, typeof(byte[]));
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HSheetOfLightModel(SerializationInfo info, StreamingContext context)
		{
			byte[] data = (byte[])info.GetValue("data", typeof(byte[]));
			HSerializedItem hSerializedItem = new HSerializedItem(data);
			DeserializeSheetOfLightModel(hSerializedItem);
			hSerializedItem.Dispose();
		}

		public new void Serialize(Stream stream)
		{
			HSerializedItem hSerializedItem = SerializeSheetOfLightModel();
			hSerializedItem.Serialize(stream);
			hSerializedItem.Dispose();
		}

		public new static HSheetOfLightModel Deserialize(Stream stream)
		{
			HSheetOfLightModel hSheetOfLightModel = new HSheetOfLightModel();
			HSerializedItem hSerializedItem = HSerializedItem.Deserialize(stream);
			hSheetOfLightModel.DeserializeSheetOfLightModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hSheetOfLightModel;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public new HSheetOfLightModel Clone()
		{
			HSerializedItem hSerializedItem = SerializeSheetOfLightModel();
			HSheetOfLightModel hSheetOfLightModel = new HSheetOfLightModel();
			hSheetOfLightModel.DeserializeSheetOfLightModel(hSerializedItem);
			hSerializedItem.Dispose();
			return hSheetOfLightModel;
		}

		public void ReadSheetOfLightModel(string fileName)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(374);
			HalconAPI.StoreS(proc, 0, fileName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
		}

		public void WriteSheetOfLightModel(string fileName)
		{
			IntPtr proc = HalconAPI.PreCall(375);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, fileName);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void DeserializeSheetOfLightModel(HSerializedItem serializedItemHandle)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(376);
			HalconAPI.Store(proc, 0, serializedItemHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(serializedItemHandle);
		}

		public HSerializedItem SerializeSheetOfLightModel()
		{
			IntPtr proc = HalconAPI.PreCall(377);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HSerializedItem.LoadNew(proc, 0, err, out HSerializedItem obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public double CalibrateSheetOfLight()
		{
			IntPtr proc = HalconAPI.PreCall(379);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HalconAPI.LoadD(proc, 0, err, out double doubleValue);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return doubleValue;
		}

		public HObjectModel3D GetSheetOfLightResultObjectModel3d()
		{
			IntPtr proc = HalconAPI.PreCall(380);
			Store(proc, 0);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HObjectModel3D.LoadNew(proc, 0, err, out HObjectModel3D obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage GetSheetOfLightResult(HTuple resultName)
		{
			IntPtr proc = HalconAPI.PreCall(381);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(resultName);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public HImage GetSheetOfLightResult(string resultName)
		{
			IntPtr proc = HalconAPI.PreCall(381);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, resultName);
			HalconAPI.InitOCT(proc, 1);
			int err = HalconAPI.CallProcedure(proc);
			err = HImage.LoadNew(proc, 1, err, out HImage obj);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return obj;
		}

		public void ApplySheetOfLightCalibration(HImage disparity)
		{
			IntPtr proc = HalconAPI.PreCall(382);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, disparity);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(disparity);
		}

		public void SetProfileSheetOfLight(HImage profileDisparityImage, HTuple movementPoses)
		{
			IntPtr proc = HalconAPI.PreCall(383);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, profileDisparityImage);
			HalconAPI.Store(proc, 1, movementPoses);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(movementPoses);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(profileDisparityImage);
		}

		public void MeasureProfileSheetOfLight(HImage profileImage, HTuple movementPose)
		{
			IntPtr proc = HalconAPI.PreCall(384);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, profileImage);
			HalconAPI.Store(proc, 1, movementPose);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(movementPose);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
			GC.KeepAlive(profileImage);
		}

		public void SetSheetOfLightParam(string genParamName, HTuple genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(385);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.Store(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamValue);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void SetSheetOfLightParam(string genParamName, string genParamValue)
		{
			IntPtr proc = HalconAPI.PreCall(385);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.StoreS(proc, 2, genParamValue);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetSheetOfLightParam(string genParamName)
		{
			IntPtr proc = HalconAPI.PreCall(386);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, genParamName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple QuerySheetOfLightParams(string queryName)
		{
			IntPtr proc = HalconAPI.PreCall(387);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, queryName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public void ResetSheetOfLightModel()
		{
			IntPtr proc = HalconAPI.PreCall(388);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void ClearSheetOfLightModel()
		{
			IntPtr proc = HalconAPI.PreCall(390);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public void CreateSheetOfLightModel(HRegion profileRegion, HTuple genParamName, HTuple genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(391);
			HalconAPI.Store(proc, 1, profileRegion);
			HalconAPI.Store(proc, 0, genParamName);
			HalconAPI.Store(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(genParamName);
			HalconAPI.UnpinTuple(genParamValue);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(profileRegion);
		}

		public void CreateSheetOfLightModel(HRegion profileRegion, string genParamName, int genParamValue)
		{
			Dispose();
			IntPtr proc = HalconAPI.PreCall(391);
			HalconAPI.Store(proc, 1, profileRegion);
			HalconAPI.StoreS(proc, 0, genParamName);
			HalconAPI.StoreI(proc, 1, genParamValue);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(profileRegion);
		}
	}
}
