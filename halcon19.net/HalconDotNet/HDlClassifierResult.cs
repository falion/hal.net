using System;
using System.ComponentModel;

namespace HalconDotNet
{
	public class HDlClassifierResult : HHandle
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifierResult()
			: base(HHandleBase.UNDEF)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifierResult(IntPtr handle)
			: base(handle)
		{
			AssertSemType();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public HDlClassifierResult(HHandle handle)
			: base(handle)
		{
			AssertSemType();
		}

		private void AssertSemType()
		{
			AssertSemType("dl_classifier_result");
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlClassifierResult obj)
		{
			obj = new HDlClassifierResult(HHandleBase.UNDEF);
			return obj.Load(proc, parIndex, err);
		}

		internal static int LoadNew(IntPtr proc, int parIndex, int err, out HDlClassifierResult[] obj)
		{
			err = HTuple.LoadNew(proc, parIndex, err, out HTuple tuple);
			obj = new HDlClassifierResult[tuple.Length];
			for (int i = 0; i < tuple.Length; i++)
			{
				obj[i] = new HDlClassifierResult(HalconAPI.IsLegacyHandleMode() ? tuple[i].IP : ((IntPtr)tuple[i].H));
			}
			tuple.Dispose();
			return err;
		}

		public HDlClassifierResult(HImage images, HDlClassifier DLClassifierHandle)
		{
			IntPtr proc = HalconAPI.PreCall(2102);
			HalconAPI.Store(proc, 1, images);
			HalconAPI.Store(proc, 0, DLClassifierHandle);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = Load(proc, 0, err);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			GC.KeepAlive(images);
			GC.KeepAlive(DLClassifierHandle);
		}

		public static void ClearDlClassifierResult(HDlClassifierResult[] DLClassifierResultHandle)
		{
			HTuple hTuple = HHandleBase.ConcatArray(DLClassifierResultHandle);
			IntPtr proc = HalconAPI.PreCall(2104);
			HalconAPI.Store(proc, 0, hTuple);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(hTuple);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(DLClassifierResultHandle);
		}

		public void ClearDlClassifierResult()
		{
			IntPtr proc = HalconAPI.PreCall(2104);
			Store(proc, 0);
			int procResult = HalconAPI.CallProcedure(proc);
			HalconAPI.PostCall(proc, procResult);
			GC.KeepAlive(this);
		}

		public HTuple GetDlClassifierResult(HTuple index, HTuple genResultName)
		{
			IntPtr proc = HalconAPI.PreCall(2115);
			Store(proc, 0);
			HalconAPI.Store(proc, 1, index);
			HalconAPI.Store(proc, 2, genResultName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			HalconAPI.UnpinTuple(index);
			HalconAPI.UnpinTuple(genResultName);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}

		public HTuple GetDlClassifierResult(string index, string genResultName)
		{
			IntPtr proc = HalconAPI.PreCall(2115);
			Store(proc, 0);
			HalconAPI.StoreS(proc, 1, index);
			HalconAPI.StoreS(proc, 2, genResultName);
			HalconAPI.InitOCT(proc, 0);
			int err = HalconAPI.CallProcedure(proc);
			err = HTuple.LoadNew(proc, 0, err, out HTuple tuple);
			HalconAPI.PostCall(proc, err);
			GC.KeepAlive(this);
			return tuple;
		}
	}
}
