using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyProduct("Connection")]
[assembly: AssemblyTrademark("Connection")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyCompany("falion")]
[assembly: AssemblyCopyright("falion")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: CompilationRelaxations(8)]
[assembly: CLSCompliant(true)]
[assembly: AssemblyTitle("Connection.net")]
[assembly: AssemblyDescription("数据接口")]
[assembly: AssemblyVersion("1.0.2.0")]
